function [] = make_data()
path=genpath('library');
addpath(path);
config = configuration;
channels = config.lobe_map{config.lobe};
for c = 1:length(channels)
    mff_channel_names{c} = ['E',num2str(channels(c))];
end
disp(mff_channel_names)
data_dir =  config.data_dir;
splited_dir = config.splited_dir;
val_split = config.val_split;
subject_id_map_file = config.subject_id_map_file;
experiments = dir(data_dir);
is_dir = [experiments(:).isdir];
experiments = {experiments(is_dir).name};
experiments(ismember(experiments,{'.','..'})) = [];

if exist(subject_id_map_file, 'file') == 2
    subject_map=readtable(subject_id_map_file);
else
    subject_name = {'dummy'};
    subject_ind = 0;
    subject_map = table(subject_name,[subject_ind]);
end

for i = 1:length(experiments)
    recordings = dir([data_dir,'/',experiments{i},'/*.mat']);
    recordings = {recordings(:).name};
    for j = 1:length(recordings)
        subject_split = strsplit(recordings{j},'_');
        subject_name = lower(subject_split{1});
        subject_ind = [];
        subject_ind = subject_map(strcmp(subject_map.subject_name,subject_name),2).subject_ind;
        if isempty(subject_ind)
            subject_name = {subject_name};
            subject_ind = max(subject_map.subject_ind)+1;
            subject_detial = table(subject_name,[subject_ind]);
            subject_map= [subject_map; subject_detial];
            writetable(subject_map,subject_id_map_file)
        end
    end
end


for i = 1:length(experiments)
    recordings = dir([data_dir,'/',experiments{i},'/*.mat']);
    recordings = {recordings(:).name};
    for j = 1:length(recordings)
        subject_split = strsplit(recordings{j},'_');
        subject_name = lower(subject_split{1});
        subject_ind = [];
        subject_ind = subject_map(strcmp(subject_map.subject_name,subject_name),2).subject_ind;
        subject_ind = subject_ind(1);
        session_identifier=[subject_split{end-6}];
        session_time = subject_split{end-5};
        divIntoChunks([data_dir,'/',experiments{i},'/',recordings{j}],[splited_dir,'/', num2str(val_split),'/'], num2str(subject_ind), session_identifier, session_time, val_split, config.lobe_map{config.lobe}, mff_channel_names);
    end
    disp('done');
end
end





function [all_session_data,test_data] = divIntoChunks(filename, save_dir, subject_ind, session_name, sess_time, val_split, channels, mff_channels)

%Give the complete path of the mat file in "filename" and the number of
%seconds of which you want the chunk to be in val_split..

%The function returns train cell and test cell. Each chunk will be randomly
%added to either train cell or test cell.

data_struct = load(filename);
try
    data_fields = fieldnames(data_struct);
    data_struct1 = data_struct.(data_fields{1});
    data_fields = fieldnames(data_struct1);
    all_chan_data = data_struct1.(data_fields{1});
    data_struct = data_struct1;
catch
    data_fields = fieldnames(data_struct);
    all_chan_data = data_struct.(data_fields{1});
end
sampling_rate = 250;
if isfield(data_struct, 'cleanchanlocs') ==1
    c_id=zeros([length(mff_channels),1]);
    ind=1;
    for c = 1:length(mff_channels)
        try
            c_id(ind) = find(strcmp(data_struct.cleanchanlocs,mff_channels{c}));
            current_data(ind,:)=all_chan_data(c_id(ind),:);
            ind=ind+1;
        catch
            return;
        end
    end
else
    current_data = all_chan_data(channels,:);
end
mkdir([save_dir,'/',subject_ind,'/',session_name,'/']);
current_data = current_data(:,250*5:end-250*5); %% Sampling rate hardcoded
current_data = current_data - mean(current_data,2);  %DC shift
std_data = std(current_data,[],2);
tot_chunks = floor(size(current_data,2)/(val_split*250)); %%todo: ignore intial and final 5 secs
for i=0:tot_chunks-1
    data = current_data(:,1+i*250*val_split:(i+1)*250*val_split);
    data = data - mean(data,2);
    save([save_dir,'/',subject_ind,'/',session_name,'/',num2str(i), sess_time, '.tmp.mat'],'data','sampling_rate'); %% tmp files will be ignored in sync and git
end
end

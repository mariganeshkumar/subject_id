/*-------------------------------------------------------------------------
 *  TrainModelsGMM.c - Trains models for each speaker given a list of files
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:58:37
 *  Last modified:  Wed 07-May-2003 15:52:21 by hema
 *  $Id: TrainModelsGMM.c,v 1.1 2002/04/30 09:34:53 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

/************************************************************************
  Function            : train_models- computes feature vectors from a 
                        list of speech files, generates the codebook
			and writes the codebook to a file.

  Input args          :  ctrl_file speaker_table codebook_size
                         feature_name output_codebook_file 

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-2002
*******************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "SphereInterface.h"
#include "BatchProcessWaveform.h"
#include "VQ_Modified.h"
#include "GMMNew.h"
#include "math.h"
#include "CommonFunctions.h"

void Usage() {
   printf(" TrainModelsGMM ctrlFile speakerTable codeBookSize ");
   printf(" featureName numVQIter numGMMIter outputCodebookFile thresholdScale\n"); 
}



main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i,j;
  int                   numVectors;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize;
  float                 varianceFloor;
  int                   seed;
  if (argc != 9 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  speakerTable = argv[2];
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&VQIter);
  string2 = argv[6];
  sscanf(string2, "%d", &GMMIter);
  string2 = argv[7];
  outputFile = fopen(string2,"w");
  cFile = fopen(cname,"r");
  speakerFile = fopen(speakerTable,"r");
  string1 = argv[8];
  sscanf(string1, "%f", &thresholdScale);
  printf("We are here now\n");
  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  varianceFloor = 10E-5;
  Cstore(asdf->fftSize);
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
							featureName, 
						      &numVectors, 
						      thresholdScale);
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %d numGMM = %d\n",numVectors,GMMIter);
  ComputeGMM(vfv, numVectors, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             ditherMean, varianceNormalize, varianceFloor, seed);
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(outputFile,"%f \n", clusterWts[i]);
      printf("%f \n", clusterWts[i]);
    } else {
      fprintf(outputFile, "%f\n", 1.0E-6);
      printf("%f\n", 1.0E-8);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
      printf("mean=%f var=%f \n",
	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(outputFile," %f %f ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  fclose(outputFile);
  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
}

















/*-------------------------------------------------------------------------
 * $Log: TrainModelsGMM.c,v $
 * Revision 1.1  2002/04/30 09:34:53  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:30:22  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TrainModelsGMM.c
 -------------------------------------------------------------------------*/

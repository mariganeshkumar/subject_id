% compute A matrix for all mixtures

function computeA_map (amatname, fxlist, lxlist, M, D, R, exflag, seq_no) 
    fxl = readlistascell (fxlist);
    lxl = readlistascell (lxlist);

    ii = length (lxl)
    for m =1:M
      Afl{m} = zeros(R);
    end

    start = 1;
    fin = length(fxl);
    N = zeros (M,ii);

    for i=start:fin
        f = fopen (fxl{i},'r');
        temp = fscanf (f, '%d', 1);
        temp = fscanf (f, '%d', 1);
        N(:,i) = fscanf (f, '%d', M);
        fclose (f);
    end
    for i=start:fin
      try
        load (lxl {i}, '-mat');
        Nsub = N(:,i);
        contributions = unique (Nsub);
        contributions = contributions(contributions>0);
        for nval_idx = 1:length(contributions)
            nval = contributions(nval_idx);
            addThis = nval * L;
            idx = find(nval == Nsub);
            for j = 1:length(idx)
                m = idx(j);
                Afl{m} = Afl{m} + addThis;
            end
        end
      catch 
        continue
      end
    end
    amatname = strcat(amatname,'.');
    amatpref = strcat(amatname,num2str(seq_no));
    for m=1:M
        newstr = strcat(amatpref,'.',num2str(m));
        tosave = Afl{m};
        save(newstr, '-mat','tosave');
    end
    if exflag ~=0 
      exit
    end

/*-------------------------------------------------------------------------
 *  CepstrumFmtsPitch.c - Formant and Pitch extraction using Cepstrum
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Thu 24-May-2007 14:27:15
 *  Last modified:  Thu 24-May-2007 14:28:09 by hema
 *  $Id: CepstrumFmtsPitch.c,v 1.2 2007/05/24 12:07:49 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"
#include "fe/FmtsLibrary.h"
#include "fe/PthLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program extracts formant and pitch data from a given speech
*       using the cepstrum function 
**	Inputs :
*	Control File, Speech data file, output Formant and Pitch files, startFrame, endFrame
*	Output :
*	Formant and pitch data are written on to different files, outputFmtFile, outputPthFile 
*******************************************************************************/       
void Usage() {
           printf("Usage : CepstrumFmtsPitch  controlFile wavefile outputFmtFile  outputPthFile startFrame, endFrame\n");
}
        int main (int argc, char *argv[])
{ 
	float           *signal = NULL, *f = NULL;
        float           minFrequency, 
	                maxFrequency;
	int  	        i,k,fftSize, fftSizeBy2, fftOrder,frameSize,
      	                numFmtObtd, frameNum, iloc,
	                winLen, minPitch, maxPitch, 
                 	frameShift, numFormants, numFrames,  
	                startFrame, endFrame, samplingRate;
        long            nSamp;
	float 		cepstrumPitch,*cepstrumFormants,*cepstrumFmtCopy;
        ASDF            *asdf;
        FILE            *fmtFile,*pthFile, *controlFile;
/******************************************************************************/
       if (argc != 7) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       numFormants = (int) GetIAttribute(asdf, "numFormants");
       minPitch = (int) GetIAttribute(asdf, "minPitch");
       maxPitch = (int) GetIAttribute(asdf, "maxPitch");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       winLen = (int) GetIAttribute(asdf, "numCepstrum");
       minFrequency = (float) GetFAttribute(asdf, "minFrequency");
       maxFrequency = (float) GetFAttribute(asdf, "maxFrequency");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	       frameSize, fftOrder, fftSize, frameShift);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d numFrames=%d\n", frameSize, fftOrder, fftSize, frameShift, numFrames);
       Cstore(fftSize);
       fmtFile = fopen(argv[3], "w");
       pthFile = fopen(argv[4], "w");
       sscanf(argv[5],"%d", &startFrame);
       sscanf(argv[6],"%d", &endFrame);
       signal = (float *) calloc(fftSize+1, sizeof(float));
       f  = (float *) calloc(frameSize+1, sizeof(float));
       cepstrumFormants  = (float *) calloc(numFormants+1, sizeof(float));
       cepstrumFmtCopy  = (float *) calloc(numFormants+1, sizeof(float));
       if ((startFrame == -1) && (endFrame == -1)) {
	 startFrame = 0;
         endFrame = numFrames;
       }
      frameNum = startFrame;
       iloc = startFrame*frameShift;
       while ((iloc < nSamp)&&(frameNum <= endFrame)) {	
	 frameNum++;
	 for (i = 1; i <= frameSize; i++)
	   if ((iloc+i) <= nSamp) 
	     f[i] = asdf->waveform[iloc+i-1];
	   else
	     f[i] = 0.0;
	 for (i = 1; i <=  frameSize; i++){
	   signal[i] = f[i]*HamDw(i,frameSize);
	 }
	 for (i = frameSize+1; i <= fftSize; i++)
	   signal[i] = 0.0;
	 cepstrumFormants = (float *) FmtsCepstrum(signal, frameSize,
					      fftSize, fftOrder,
					      winLen, cepstrumFormants, 
					      &numFmtObtd);
	 printf("Frame Number = %d ", frameNum);
	 for(i = 1; i <= numFmtObtd; i++) 
	   printf("F %d = %f ", i, cepstrumFormants[i]);
	 printf("\n");   
	 fflush(stdout);
	 cepstrumPitch = (float ) PitchCepstrum(signal,frameSize,
					   fftSize, fftOrder,
					   minPitch, maxPitch);
	 
	 printf("FrameNumber %d Pitch =  %f\n",frameNum, cepstrumPitch);
	 k = 0;
	 for (i = 1; i <= numFormants; i++)
	   cepstrumFmtCopy[i] = 0;
	 for (i = 1; i <= numFmtObtd; i++)       
   if (cepstrumFormants[i]/fftSize*samplingRate >= minFrequency &&
	       cepstrumFormants[i]/fftSize*samplingRate <= maxFrequency) {   
	     k = k + 1;         
	     cepstrumFmtCopy[k] = cepstrumFormants[i]/fftSize*samplingRate;     
	   }
         fprintf(fmtFile, "%d ", iloc);
	 for (i = 1; i <= k; i++)
	   fprintf(fmtFile,"%f ",cepstrumFmtCopy[i]);
	 for (i = k+1; i <= numFormants; i++)
	   fprintf(fmtFile, "%f ",0.0);
	 fprintf(fmtFile,"\n");
	 fprintf(pthFile,"%d %f\n",iloc, cepstrumPitch/samplingRate);
	      for (i = 1; i <= 10; i++)
	        cepstrumFormants[i] = 0;
              cepstrumPitch = 0;
	      iloc = iloc+frameShift;
       }
       fclose(fmtFile);
       fclose(pthFile);
       return(0);
}














/*-------------------------------------------------------------------------
 * $Log: CepstrumFmtsPitch.c,v $
 * Revision 1.2  2007/05/24 12:07:49  hema
 * Modified to use DON-c
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of CepstrumFmtsPitch.c
 -------------------------------------------------------------------------*/
 

% initializes T matrix and stores it in 
% a file

function tmatinit (fname, ubmname, M, D, R, exflag)
   nrows = M * D;
   ncols = R;
   [ws ms vs] = ReadGMM (ubmname, M, D);
   clear ws ms
   vs = reshape (vs, nrows,1);
   'flooring'
   find(vs<1E-5)
   vs(find(vs<1E-5)) = 1E-5;
   vs = sqrt (vs);
   T = randn (nrows , ncols);
   for r = 1:R
     T (:,r) = T(:,r) ./ vs;
   end
   W = T;
   clear T;
   save (fname, 'W', '-mat');
   if exflag ~=0 
     exit
   end

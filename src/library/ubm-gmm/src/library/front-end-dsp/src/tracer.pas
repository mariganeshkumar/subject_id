program TRACER(input,output);
const
   MAXPTS = 260;
   MAX_FRAMES = 70;
type
   INTARR = array[1..MAXPTS,1..MAX_FRAMES] of integer;
   FRIENDS = array[0..7] of integer;
var
  FOR_LOCS : INTARR;
  EDX,EDY : array[1..MAX_FRAMES] of integer;
  I,J,K,DIRECTION,CURX,CURY,LENGTH,INCR : integer;
  NUM_FRAMES,IFRMT,NFFT,THRESHOLD,NFBY2 : integer;
  NEIGHBOURS : FRIENDS;
  FIRST,FOUND : boolean;
  FILVAR : file of integer;
  S : string[14];
function NEIGH_NOT_FOUND(TEMP : FRIENDS) :boolean;
  var
    I : integer;
  begin
    NEIGH_NOT_FOUND := true;
    for I := 0 to 7 do
      if TEMP[I] = 1 then
        NEIGH_NOT_FOUND :=  false
  end;


procedure SET_NEIGHBOURS(var TEMP : FRIENDS;DIR : integer;
                         var XLOC,YLOC,LENGTH : integer);
  begin
    LENGTH := LENGTH+1;
    case DIR of
      1 : begin
            XLOC := XLOC-1;
            YLOC := YLOC+1;
          end;
      2 : begin
            XLOC := XLOC-1;
            YLOC := YLOC;
          end;
      3 : begin
            XLOC := XLOC-1;
            YLOC := YLOC-1
          end;
      4 : begin
            XLOC := XLOC;
            YLOC := YLOC-1
          end;
      5 : begin
            XLOC := XLOC+1;
            YLOC := YLOC-1
          end;
      6 : begin
            XLOC := XLOC+1;
            YLOC := YLOC
          end;
      7 : begin
            XLOC := XLOC+1;
            YLOC := YLOC+1
          end;
      0 : begin
            XLOC := XLOC;
            YLOC := YLOC+1
          end;
    end;
    TEMP[0] := FOR_LOCS[XLOC,YLOC+1];
    TEMP[1] := FOR_LOCS[XLOC-1,YLOC+1];
    TEMP[2] := FOR_LOCS[XLOC-1,YLOC];
    TEMP[3] := FOR_LOCS[XLOC-1,YLOC-1];
    TEMP[4] := FOR_LOCS[XLOC,YLOC-1];
    TEMP[5] := FOR_LOCS[XLOC+1,YLOC-1];
    TEMP[6] := FOR_LOCS[XLOC+1,YLOC];
    TEMP[7] := FOR_LOCS[XLOC+1,YLOC+1];
    EDX[LENGTH] := XLOC;
    EDY[LENGTH] := YLOC;
    FOR_LOCS[XLOC,YLOC] := 5;
    writeln;
    write('edx[',length,']=',edx[length],'edy[',length,']=',edy[length]);
end;
  begin
    writeln;
    write('formant file name :');
    read(S);
    assign(FILVAR,S);
    reset(FILVAR);
    writeln;
    write('NFFT :');
    read(NFFT);
    NFBY2 := NFFT div 2;
    writeln;
    write('Threshold for edge-length :');
    read(THRESHOLD);
    NUM_FRAMES := 0;
    for I := 1 to MAXPTS do
      for J := 1 to MAX_FRAMES do
        FOR_LOCS[I,J] := 0;
    while not eof(FILVAR) do
      begin
        NUM_FRAMES:= NUM_FRAMES+1;
        IFRMT := -1;
        while IFRMT <> 9999 do
          begin
            read(FILVAR,IFRMT);
            if IFRMT <> 9999 then
              FOR_LOCS[IFRMT+1,NUM_FRAMES+1] := 1;
          end;
      end;
    for I := 1 to NFBY2+1 do
      begin
        writeln;
        for J := 1 to NUM_FRAMES+1 do
          write(FOR_LOCS[I,J])
      end;
    for I := 2 to NFBY2 do
      begin
        for J := 2 to NUM_FRAMES Do
          begin
            NEIGHBOURS[0] := FOR_LOCS[I,J+1];
            NEIGHBOURS[1] := FOR_LOCS[I-1,J+1];
            NEIGHBOURS[2] := FOR_LOCS[I-1,J];
            NEIGHBOURS[3] := FOR_LOCS[I-1,J-1];
            NEIGHBOURS[4] := FOR_LOCS[I,J-1];
            NEIGHBOURS[5] := FOR_LOCS[I+1,J-1];
            NEIGHBOURS[6] := FOR_LOCS[I+1,J];
            NEIGHBOURS[7] := FOR_LOCS[I+1,J+1];
            if (FOR_LOCS[I,J] = 1) and (NEIGHBOURS[4] = 0) then
              begin
                (*writeln;
                writeln('forlocs loop');
                writeln('pixel at',i,j,'=',for_locs[i,j],'neighbour4=',neighbours[4]);
                writeln('first=',first,'found=',found);*)
                DIRECTION := 6;
                FIRST := true;
                CURX := I;
                CURY := J;
                length := 1;
                EDX[LENGTH] := CURX;
                EDY[LENGTH] := CURY;
                while ((CURX <> I) or (CURY <> J)) or (FIRST = TRUE) do
                  begin
                    FOUND := false;
                    incr := 0;
                    writeln;
                    write('i=',i,'j=',j,'found=',found,'first=',first);
                    writeln;
                    write('curx=',curx,'cury=',cury);
                    writeln;
                    for K := 0 to 7 do
                      write('neighbours[',k,']=',neighbours[k]);
                    while (FOUND = false) do
                      begin
                        incr := incr+1;
                        writeln;
                        write('direction:',direction);
                        if NEIGHBOURS[DIRECTION-1] = 1 then
                          begin
                            writeln;
                            write('len_dir-1:',length);
                            SET_NEIGHBOURS(NEIGHBOURS,DIRECTION-1,CURX,CURY,LENGTH);
                            DIRECTION := DIRECTION-2;
                            if DIRECTION < 0 then
                              DIRECTION := DIRECTION+8;
                            FOUND := true
                          end
                        else if NEIGHBOURS[DIRECTION] = 1 then
                          begin
                            writeln;
                            write('len_dir:',length);
                            SET_NEIGHBOURS(NEIGHBOURS,DIRECTION,CURX,CURY,LENGTH);
                           FOUND := true
                          end
                        else if NEIGHBOURS[DIRECTION+1] = 1 then
                          begin
                            writeln;
                            write('len_dir+1:',length);
                            SET_NEIGHBOURS(NEIGHBOURS,DIRECTION+1,CURX,CURY,LENGTH);
                            FOUND := true
                          end
                        else if NEIGH_NOT_FOUND(NEIGHBOURS) then
                           found := true
                        else begin
                           DIRECTION := DIRECTION+2;
                           if DIRECTION >= 8 then
                           DIRECTION := DIRECTION-8
                        end;
                      end;
                    first := false
                  end;
                writeln;
                write('length=',length,'threshold=',threshold);
                if LENGTH < THRESHOLD then
                  for K := 1 to LENGTH do
                    FOR_LOCS[EDX[K],EDY[K]] := 0;
              end;
              

         end;
     end;
    writeln;
    write('final forlocs');
    for I := 1 to NFBY2+1 do
      begin
        writeln;
        for J := 1 to NUM_FRAMES+1 do
          write(FOR_LOCS[I,J]);
      end;
  end.
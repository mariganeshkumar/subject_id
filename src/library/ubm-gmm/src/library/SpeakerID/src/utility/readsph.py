def pcmu2lin(x):
    t=9.98953613E-4
    m=15-x%16
    q=math.floor(x/128.);
    e=(127-x-m+128*q)/16;
    x=(q-0.5).*(pow2(m+16.5,e)-16.5)*t;


def readsph(filename, nmax = -1,nskip = -1):
    codes=['sample_count',
            'channel_count',
            'sample_n_bytes',
            'sample_sig_bits',
            'sample_rate',
            'sample_min',
            'sample_max']

    codings= ['pcm', 'ulaw']
    compressions=[',embedded-shorten-',',embedded-wavpack-', ',embedded-shortpack-']
    byteorder = 'l'
    try:
        f = open(filename, 'rb')
    except:
        print("Debug: Error opening file %s" % filename)
        return None
    if 'NIST' not in f.readline().strip():
        print("Debug: %s is not a valid sphere file" % filename)
        return None
    try:
        nbytes = int(f.readline())
    except:
        print("Debug: invalid header size in %s" % filename)
        return None
    if nbytes < 0:
        print("Debug: invalid header size in %s" % filename)
        return None
    # overkill?
    func_dict = {
                 "-i" : int,
                 "-f" : float,
                 "-s" : str
                }
    hdr = {}
    while True:
        ln = f.readline()
        if 'end_head' in ln:
            break
        if ln.startswith(';'):
            continue
        (field_name, field_type, field_val) = ln.strip().split()
        try:
            hdr[field_name] = func_dict[field_type](field_val)
        except KeyError:
            print("Debug: invalid datatype found in the header of %s" 
                    % filename)
        except:
            print("Debug: error while parsing the header of %s" % filename)

#!/bin/bash

HOME=/home/srikanth
WORKINGDIR=../
TNORM=/kone/dell.lpbkup/home/JEL/bin/TNorm.e
TESTLIST=../flists/2004/male.test
SPKRLIST=../flists/2004/spkrList.m
GROUNDTRUTH=../flists/2004/male.gt
MODELSDIR=../models/2004.m.fcd
RESULTSDIR=$MODELSDIR/AllResults
SPIDMODELS=$MODELSDIR/SpidModel
BACKYARD=$MODELSDIR/dump.2

mkdir $BACKYARD
NISTDIR=/kone/db/nist2004/
CLAIMSFILE=../flists/2004/male.trials

rm -f $BACKYARD/true_dist
rm -f $BACKYARD/impostor_dist
# Take each test file
for testCase in `cat $TESTLIST`; do
  testName=`basename $testCase | cut -d '.' -f1`
	gts=`grep $testCase $GROUNDTRUTH | cut -d ' ' -f2`
	echo "Test name is $testName"
#  echo "Result file is $RESULTSDIR/$testName.result.dist"
  nl=`wc -l $RESULTSDIR/$testName.result.dist | cut -d ' ' -f1`
  if [ $nl -eq 0 ]; then
    echo "Continuing"
    continue
  fi
	# Consider each claim
   for claim in `grep $testName $CLAIMSFILE | cut -d ' ' -f1 --complement`; do
    greplist=`grep $claim $SPKRLIST | wc -l | cut -d ' ' -f1`
    if [ $greplist -eq 0 ]; then
      continue;
    fi
    claimSpkrId=`grep -n $claim $SPKRLIST | cut -d ':' -f1 | head -1`
		echo "claimSpkrId is $claimSpkrId"
    # check if the spkr is in db
		# Look up Distortion values    
		head -$claimSpkrId $RESULTSDIR/$testName.result.dist | tail -1 > $BACKYARD/$testName.$claim.score		
		rm -f $BACKYARD/$testName.$claim.cohort
		tail -100 $RESULTSDIR/$testName.result.dist  > $BACKYARD/$testName.$claim.cohort
		# Compute TNorm (and save it more importantly)
#    echo "$TNORM $BACKYARD/$testName.$claim.score $BACKYARD/$testName.$claim.cohort > $BACKYARD/$testName.$claim.tnorm"
    $TNORM $BACKYARD/$testName.$claim.score $BACKYARD/$testName.$claim.cohort > $BACKYARD/$testName.$claim.tnorm		
		cat $BACKYARD/$testName.$claim.tnorm

		trueSpeakers=`grep $testName $GROUNDTRUTH | cut -d ' ' -f1 --complement`
    fnd=0
    for trueSpeaker in $trueSpeakers; do
      if [ "$claim" == "$trueSpeaker" ]; then
         echo "storing in true_dist file"
         cat $BACKYARD/$testName.$claim.tnorm >> $BACKYARD/true_dist
         fnd=1
         break
      fi
    done
    if [ $fnd -eq 0 ]; then
       echo "storing in impostor file"
       cat $BACKYARD/$testName.$claim.tnorm >> $BACKYARD/impostor_dist
    fi         
	done
done

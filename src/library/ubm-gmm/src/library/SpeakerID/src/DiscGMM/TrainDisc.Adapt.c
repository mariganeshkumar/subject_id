/*
 * =====================================================================================
 *
 *       Filename:  TrainDisc.Adapt.c
 *
 *    Description:  Uses DiscAdapt() to take target and non-target data
 *                  and adapt models discriminatively from a UBM. This
 *                  is just some wrapper code. The code is meant to be
 *                  quite generic
 *
 * =====================================================================================
 */

#include "CommonFunctions.h"
#include "DiscAdapt.h"

void
Usage () {
  printf ("TrainDisc.Adapt.c [options]\n");
  printf ("-h : Print this help\n");
  printf ("-i : Input file/ list of input files\n");
  printf ("-n : Feature name\n");
  printf ("-d : Set when features are already dumped\n");
  printf ("-v : Do vad if features are to be extracted\n");
  printf ("-t : Threshold value if features are to be extracted\n");
  printf ("-k : Number of mixtures\n");
  printf ("-f : Control file path\n");
  printf ("-o : Output file name/ list of output files\n");
  printf ("-u : GMM file name\n");
  printf ("-g : Number of gmm iterations\n");
  printf ("-s : Start at spkr no. By default this is 1\n");
  printf ("-e : End at speaker no. By default this is N\n");
  printf ("-N : Number of speakers\n");
  printf ("-p : percentage of anti-speaker data to be considered\n");
  return;
}

int
main (int argc, char *argv[]) {
   int numClusters, clusterNumber, featLength;
   int negVecSize, posVecSize;
	 int i,j,k, *clustcnt, dumpFile, 
       GMMIter = 1, VQIter = 0, seed;
	 int oc, useTS = 1, useVad = 0, ipIsList = 0,
			 firstIter = 0, start = 1, end = -1,
       numSpeakers = 0 ;
   uint *indNVec, temp, tempNV, idx, totIdx, numVectors;

	 float psf, tscale = 0.0, varianceNormalize;
	 float *ew, *clusterWts, tempf, ditherMean, nper;
	 
   char *basemodelfname, *featureName, *waveFileName,
					 *ctrlFileName = NULL, *ergFileName = NULL,
           *fn, *spkrFileName = NULL;   
   char *spkrList, *opname, *ubmFileName;

   FILE *ip = NULL, *op = NULL, *basemodel;
	 FILE *ctrlFile, *spkrFile, *vfvfile, *dfile;

	 ASDF *asdf;
	 VFV *em, *ev, *vfv;
   VFV *tempMeanClusters, *tempVarClusters;
   VFV *clusterMeans, *clusterVars;
   VFV *spkrVfv = NULL, *negVfv = NULL;
   VFV *adaptedMeans = NULL;


	 // v: vad
	 // t: trigaussian
	 // k: numclusters
	 // i : input is a list of files
	 // n : feature name 
   // u : ubm file's name
	 while ((oc = getopt (argc, argv, 
                        "i:v:t:k:f:n:do:g:u:s:e:N:p:h"
                        )
          ) != -1)
         
	 {
					 switch (oc) {
           case 'h':
               Usage ();
               exit (1);
               break;
				   case 'i':
               spkrList = optarg;
               printf ("spkrList is %s\n", spkrList);
							 break;		 
					 case 'v':
							 useVad = 1;
							 useTS = 0;
					     ergFileName = optarg;
           case 't':
					     if (useVad == 1)
							     break;
						   useTS = 1;
							 sscanf (optarg, "%f", &tscale);
							 break;
					 case 'f':
					     ctrlFileName = optarg;
							 break;
					 case 'n':
					     featureName = optarg;
							 break;
					 case 'k':
					     sscanf (optarg, "%d", &numClusters);
							 break;
           case 'o':           
               opname = optarg;
               break;
           case 'd':
               dumpFile = 1;
               useTS = 0;
               useVad = 0;
               break;
           case 'g':
               sscanf (optarg, "%d", &GMMIter);
               break;
           case 'u':
               ubmFileName = optarg;
               break;
           case 's':
               sscanf (optarg, "%d", &start);
               break;
           case 'e':
               sscanf (optarg, "%d", &end);
               break;
           case 'N':
               sscanf (optarg, "%d", &numSpeakers);
               break;
           case 'p':
               sscanf (optarg, "%f", &nper);
               break;
					 default:
					     return 1;
					 }
	 }				 
	 vfv = NULL;
   // more input validation
   if (ctrlFileName == NULL) {
     printf ("Control file is mandatory argument\n");
     Usage ();
     exit (1);
   }

   if (numSpeakers == 0) {
     printf ("No.of speakers\(N\) is a mandatory argument\n");
     exit (1);
   }

   ctrlFile = fopen (ctrlFileName, "r");
   asdf = (ASDF *) calloc (1, sizeof (ASDF));
   InitializeStandardFrontEnd (asdf, ctrlFile);
   Cstore (asdf->fftSize);
   psf = (float) GetFAttribute (asdf, "probScaleFactor");
   varianceNormalize = (float) GetFAttribute (asdf, "varianceNormalize");
   seed = (int) GetIAttribute (asdf, "seed");
   ditherMean = (float) GetFAttribute (asdf, "ditherMean");
   fclose (ctrlFile);

   
   /* currently only this works. need to change several functions
    * in CommonFunctions to incorporate required changes - a facility
    * to retain the number of vectors in each speech file when
    * computing features in batch mode 
    */
   if (dumpFile) {
       dfile = fopen (spkrList, "r");
       if (dfile == NULL) {
          printf ("Cannot open file %s\n", spkrList);
          exit (2);
       }
       vfv = BatchReadVfvFromFile (spkrList, &numVectors);
       indNVec = (uint*) calloc (numSpeakers, sizeof(uint));
       idx = 0;
       while (!feof(dfile)) {
          fn = (char *) calloc (256, sizeof(char));
          fscanf (dfile , "%s\n",fn);
          vfvfile = fopen (fn, "r");
          fscanf (vfvfile, "%u %u\n", &temp, &indNVec[idx++]);            
          fclose (vfvfile);
          free (fn);
       }
       fclose (dfile);
   }
   else {
     if (!useTS && useVad) {
       ReadTriGaussianFile (ergFileName, &em, &ev, &ew);
     }
     if (useTS) {
       spkrFile = fopen (spkrList, "r");
       if (spkrFile == NULL) {
         printf ("unable to open input list %s\n",spkrList);
         exit(2);
       }
       vfv = ComputeFeatureVectors (asdf, spkrFile, featureName, &numVectors, tscale); 
       fclose (spkrFile);
     }
     else  
       vfv = BatchExtractFeatureVectorsVAD (asdf, spkrList, featLength, 
                                            &numVectors,
                                            em, ev, ew);
   }
     
   featLength = vfv[0]->numElements;

   clusterMeans = (VFV*) calloc (numClusters, sizeof(VFV));
   clusterVars = (VFV*) calloc (numClusters, sizeof(VFV));
   clusterWts = (float *) calloc (numClusters, sizeof(float));

   ReadGMMFile (ubmFileName, &clusterMeans, &clusterVars,
                &clusterWts, numClusters, featLength);

   if (end == -1) end = numSpeakers;
   start--;
   for (i = start; i < end; i++) {
     posVecSize = indNVec[i];
     negVecSize = numVectors - posVecSize;
     negVfv = (VFV*) calloc (negVecSize, sizeof(VFV));
     idx = 0;
     totIdx = 0;
     finc(j,numSpeakers,1) {
        if (i == j) {            
            spkrVfv = &vfv[totIdx];     
            totIdx += indNVec[j];
            continue;
        }
        
        finc(k,indNVec[j],1) 
            negVfv[idx++] = vfv[totIdx++];
        
     }
     adaptedMeans = DiscAdaptOnlyMeans 
                    (clusterMeans, clusterVars, clusterWts,
                     numClusters, 
                     spkrVfv, posVecSize,  
                     negVfv, negVecSize , 16,nper);
   spkrFileName = (char *) calloc (256, sizeof(char));   
   sprintf (spkrFileName, "%s/spid_%d",opname,i+1);
   op = fopen (spkrFileName,"w");
   finc(k,numClusters,1) {
      fprintf (op, "%e\n", clusterWts[k]);
      finc(j,featLength,1) 
        fprintf (op, " %e %e", adaptedMeans[k]->array[j], 
                               clusterVars[k]->array[j]);
      fprintf(op, "\n");        
   }
   free (spkrFileName);
   fclose (op);
   FreeVfv (adaptedMeans, numClusters);
   free (negVfv);
 }
   //FreeVfv (tempMeanClusters, numClusters);
   //FreeVfv (tempVarClusters, numClusters);
//   FreeVfv (vfv, numVectors);
//   free (clusterWts);
}

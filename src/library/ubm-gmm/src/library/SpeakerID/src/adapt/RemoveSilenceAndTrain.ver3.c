#include "CommonFunctions.h"

void Usage() {
   printf(" \nRemoveSilenceAndTrain.ver3 ctrlFile speakerTable codeBookSize ");
   printf(" featureName numVQIter numGMMIter outputCodebookFile thresholdScale ergSpidModels"); 
	printf(" numGaussiansForEnergy\n\n");
}



main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *speakerTable =NULL, 
                        *featureName = NULL, *ergSpidModelFile=NULL;
  char                  *string1=NULL, *string2=NULL, *ergSpidModelFileName;
  char						line[500], speakerModel[500];
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  VECTOR_OF_F_VECTORS   *ergMeans, *ergVars;
  float                 *clusterWts, thresholdScale;
  float						*ergWts, tempProbabilities[3];
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv, *energyVector, *newVfv;
  int                   i,j,k, numGaussiansErg, numSpeechVectors;
  int                   silenceMixtureNumber, speechMixtureNumber, noiseMixtureNumber;
  unsigned long numVectors;
  int                   VQIter, GMMIter, featLength;
  float                 probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  F_VECTOR					*meanVector, *varVector;

  int                   seed;
  if (argc != 11 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  speakerTable = argv[2];
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&VQIter);
  string2 = argv[6];
  sscanf(string2, "%d", &GMMIter);
  string2 = argv[7];
  outputFile = fopen(string2,"w");
  cFile = fopen(cname,"r");
  speakerFile = fopen(speakerTable,"r");
  string1 = argv[8];
  sscanf(string1, "%f", &thresholdScale);
  ergSpidModelFileName = argv[9];
  sscanf (argv[10], "%d", &numGaussiansErg);
  
  printf("We are here now\n");
  
  
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  varianceFloor = (float) GetFAttribute (asdf, "varianceFloor");
  Cstore(asdf->fftSize);
  
  energyVector = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
                                                      "frameEnergy", 
																		&numVectors, 
																		thresholdScale);

  for (j=0;j<numVectors;j++) energyVector[j]->array[0] = log (energyVector[j]->array[0]); 
  MakeZeroMeanUnitVar(energyVector,numVectors);

  meanVector = ComputeMeanVfv (energyVector, numVectors);
  varVector  = ComputeVarVfv  (energyVector, numVectors);
  
	

  ergMeans = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
  ergVars  = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
  ergWts = (float *) calloc (numGaussiansErg,sizeof(float));
  for (i=0;i<numGaussiansErg;i++)
    {
	   ergMeans[i] = (F_VECTOR *) AllocFVector (energyVector[0]->numElements);
		ergVars[i]  = (F_VECTOR *) AllocFVector (energyVector[0]->numElements);
	 }


  ergSpidModelFile = fopen (ergSpidModelFileName,"r");
  if (ergSpidModelFile == NULL)
    { 
	   printf ("Could not open energy-Spid Models file..\n");
		fflush(stdout);
		exit(1);
	 }
  featLength = energyVector[0]->numElements;
  printf ("numGaussiansErg is %d and featLength is %d\n", numGaussiansErg, featLength); 
  for (j=0; j<numGaussiansErg && !feof(ergSpidModelFile);j++)
    {
	   fscanf (ergSpidModelFile,"%e",&ergWts[j]);
		printf ("%e\n", ergWts[j]);
		for (k=0;k<featLength;k++)
		  {
		    fscanf(ergSpidModelFile," %e %e", &ergMeans[j]->array[k], &ergVars[j]->array[k]);
			 printf (" %e %e\n", ergMeans[j]->array[k],ergVars[j]->array[k]);
		  }
		  fscanf(ergSpidModelFile,"\n");
		  printf("\n");
	 }

  
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
                                                      featureName, 
																		&numVectors, 
																		thresholdScale);
	
  numSpeechVectors=0;
  newVfv=(VECTOR_OF_F_VECTORS *) calloc (numVectors, sizeof(VECTOR_OF_F_VECTORS));

  silenceMixtureNumber = 0;
  noiseMixtureNumber   = 1;
  speechMixtureNumber  = 2;
  for (i=0; i<numVectors; i++)
    {
      tempProbabilities[silenceMixtureNumber] = ComputeProbability(ergMeans[silenceMixtureNumber],
																						 ergVars[silenceMixtureNumber],
																						 ergWts[silenceMixtureNumber],
																						 energyVector[i],probScaleFactor);
      tempProbabilities[noiseMixtureNumber] = ComputeProbability(ergMeans[noiseMixtureNumber],
																						 ergVars[noiseMixtureNumber],
																						 ergWts[noiseMixtureNumber],
																						 energyVector[i],probScaleFactor);
      tempProbabilities[speechMixtureNumber] = ComputeProbability(ergMeans[speechMixtureNumber],
																						 ergVars[speechMixtureNumber],
																						 ergWts[speechMixtureNumber],
																						 energyVector[i],probScaleFactor);
		if (tempProbabilities[silenceMixtureNumber] < tempProbabilities[speechMixtureNumber] &&
			 tempProbabilities[noiseMixtureNumber]   < tempProbabilities[speechMixtureNumber])		    		  
			  	 newVfv[numSpeechVectors++] = vfv[i];
		else if (tempProbabilities[silenceMixtureNumber] < tempProbabilities[speechMixtureNumber] &&
		         energyVector[i]->array[0] > ergMeans[noiseMixtureNumber]->array[0])
			  	 newVfv[numSpeechVectors++] = vfv[i];
		  
	 }
  printf ("numSpeechVectors is %d while numVectors is %d\n", numSpeechVectors, numVectors);
  numVectors = numSpeechVectors;
  //MakeZeroMeanUnitVar(newVfv,numVectors);
  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));

  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %d numGMM = %d\n",numVectors,GMMIter);
  
  ComputeGMM(newVfv, numVectors, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             ditherMean, varianceNormalize, varianceFloor, seed);
  
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(outputFile,"%e \n", clusterWts[i]);
 //     printf("%e \n", clusterWts[i]/numVectors);
    } else {
      fprintf(outputFile, "%e\n", 1.0E-6);
 //     printf("%e\n", 1.0E-8);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
//      printf("mean=%e var=%e \n",
//	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(outputFile," %e %e ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  
  fclose(outputFile);
//  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
}

















/*-------------------------------------------------------------------------
 * $Log: TrainModelsGMM.c,v $
 * Revision 1.1  2002/04/30 09:34:53  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:30:22  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TrainModelsGMM.c
 -------------------------------------------------------------------------*/

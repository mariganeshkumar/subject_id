#!/usr/bin/python
# INCOMPLETE
import math
import getopt
import sys
import numpy as np
import scipy as sp
from Utility import *
import GMM as GMM
from scipy import linalg
from tables import *


optlist, args = getopt.getopt (sys.argv[1:], "d:R:T:h")
helpmsg  = "Usage: ./randpca.py -d datamat -R rank -T tmatname"

def printhelp (close = 0):
  print helpmsg
  if close == 1:
    quit ()

tmatfname, dmatname = "", "" 
R, step = 0,1000

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-R":
      R = int (value)
    elif option == "-T":
      tmatfname = value
    elif option == "-d":
      dmatname = value
    else:
      printhelp (1)

if tmatfname == "" or dmatname == "" or R == 0: 
  printhelp (1)

# params the are common to the entire program
l = R + 2     # generally, sampling for a higher dim space
              # is better 


# computing A*G is mem-intensive if A is loaded completely
dmatf = openFile (dmatname, "r")
dtab = dmatf.root.dim
for row in dtab.iterrows ():
  nrows, ncols = row['dim']
  break

# generate random vectors to compute range space of the data matrix. unfort.,
# this is very large.
G = np.random.normal (0.0,1.0,ncols * l).reshape ((ncols,l))

mtab = dmatf.root.matrix

H = np.zeros ((nrows, l))
Asub = np.zeros ((step,ncols))
i = 0
#tmatf, tmtab, tdtab = hdf5open (tmatfname, l, 1) 
#trow = tmtab.row
start = 0
for row in mtab.iterrows ():
  Asub[i,:] = row['colvec']  
  i += 1
  if i == step:
    H[start:start+step,] = np.dot (Asub, G)
    start += step
    i = 0
#tmtab.flush ()
dmatf.close ()

print "Computed H"
Q, R = sp.linalg.qr (H)

/*
    This file does VQ initialization for the parallel VQ

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "CommonFunctions.h"

void
printHelp ()
{
  printf ("Help file for Init\n");
  printf ("The following are the options available\n");
  printf ("i\t:\tname of input file. See -v, -l, -d\n");
  printf ("f\t:\tcontrol file\n");
  printf ("o\t:\toutput file\n");      
}

/**************************************************
* Function: main
* arguments: all are POSIX based command line args
*
* Description: This program takes a file, or a list
*              of files as i/p and prints out
*              mean values after called VQ init
*              
* Additional Info:
*              the getopt function will process 
*              a bunch of arguments whose purpose are
*              mentioned below
*              -i [list/wave] : i/p is a list of files
*                               or single wavefile
*              -v : use vad during feature extraction. 
*                   argument is followed by erg filename
*              -t : threshold scale when not using VAD   
*              -c : ctrl file's name
****************************************************/

int
main (int argc, char *argv[])
{
   int oc;
	 int ipIsList = 0, opIsBinary = 0, useVad = 0,
	     useTS = 1, dumpFile = 0, featIsBin = 0;
   int index, *indexArray = NULL;
	 unsigned char *bytearr = NULL, *codedbytearr = NULL;
   int j, flag, random;

	 char *ctrlFileName, *waveFileName, *outFileName, *spkrList;	 
	 FILE *ctrlFile, *spkrFile;
	 char s[256];

	 char *featureName, *optFeatureName;
	 unsigned long numVectors;
	 int numClusters, numClustersLeft;
	 int rmax, rno, i, seed = 2131, featLength;
     float tscale = 0.0, psf;

	 ASDF *asdf;
	 VECTOR_OF_F_VECTORS *vfv, *em, *ev;
     float *ew;
	 
	 char *ergFileName;
	 VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
	 float *ergWts;

	 FILE *op, *ip;

	 while ((oc = getopt (argc, argv, "i:v:t:k:f:n:o:ls:db")) != -1) {
		   switch (oc) {
              case 'l':
              ipIsList = 1;
              break;		 
              case 'i':
              spkrList = optarg;
              break;
              case 'v':
              useVad = 1;
              useTS = 0;
              ergFileName = optarg;
              case 't':
              if (useVad)
              break;
              useTS = 1;
              sscanf (optarg, "%f", &tscale);
              break;
              case 'f':
              ctrlFileName = optarg;
              break;
              case 'n':
              featureName = optarg;
              break;
              case 'k':
              sscanf (optarg, "%d", &numClusters);
              break;
              case 'd':
              dumpFile = 1;
              break;
              case 'o':
              outFileName = optarg;
              break;
              case 'b':
              featIsBin = 1;
              break;
              case 's':
              sscanf(optarg, "%d", &seed);
              break;
              default:
              printHelp ();
              return 1;
			}			 
		}
		

   if (dumpFile) {
     if (!ipIsList) {
       if (!featIsBin)
            vfv = ReadVfvFromFile (spkrList, (unsigned int *)&numVectors);        
       else
            vfv = ReadVfvFromBinFile (spkrList, (unsigned int *)&numVectors);
     }
     else {
       if (!featIsBin)
            vfv = BatchReadVfvFromFile (spkrList, &numVectors);
       else 
            vfv = BatchReadVfvFromBinFile (spkrList, &numVectors);
     }
   }
   else {
     ctrlFile = fopen (ctrlFileName, "r");
     asdf = (ASDF *) calloc (1, sizeof (ASDF));
     InitializeStandardFrontEnd (asdf, ctrlFile);
     Cstore (asdf->fftSize);
     psf = (float) GetFAttribute (asdf, "probScaleFactor");
     seed = (int) GetFAttribute (asdf, "seed");
     fclose (ctrlFile);
      
     vfv = NULL;
     if (!useTS && useVad) {
       ReadTriGaussianFile (ergFileName, &em, &ev, &ew);
     }
     if (ipIsList && useTS) {
       spkrFile = fopen (spkrList, "r");
       vfv = ComputeFeatureVectors (asdf, spkrFile, featureName, &numVectors, tscale); 
       fclose (spkrFile);
     }
     else if (ipIsList) 
       vfv = BatchExtractFeatureVectorsVAD (asdf, spkrList, featureName, &numVectors,
                                            em, ev, ew);
     
     else if (!ipIsList && useTS) 
       vfv = ExtractFeatureVectors (asdf, spkrList, featureName, &numVectors, tscale);
    
     else if (!ipIsList) 
       vfv = ExtractFeatureVectorsVAD (asdf, spkrList, featureName, &numVectors,
                                       em, ev, ew);
	    free (asdf);
   } 
   featLength = vfv[0]->numElements;
	 srand (seed);
   rmax = RAND_MAX;
   indexArray = (int *) calloc (numClusters, sizeof(int));
   op = fopen (outFileName,"w");
   for (i = 0; i < numClusters; i++) {
    flag = 1;
    while (flag) {
      random = rand();
      rmax = RAND_MAX;
      index = (int) ((float) (random)/rmax*numVectors);      
      if (index == numVectors) index --;
      flag = (int) FindIndex(indexArray, i-1, index);
      if (!flag) 
        flag = (int) FindMatch(vfv, numVectors, indexArray, i-1, index);
    }
    fprintf(op,"%f", ((float) numVectors/numClusters));
    finc(j,featLength,1) 
      fprintf (op," %f", vfv[index]->array[j]);
    finc(j,featLength,1)
      fprintf (op," %f", 1.0);
    
    fprintf (op, "\n");
    indexArray[i] = index;
  }
  free(indexArray);
  fclose (op);
   FreeVfv (vfv, numVectors);
   return 0;		 
}

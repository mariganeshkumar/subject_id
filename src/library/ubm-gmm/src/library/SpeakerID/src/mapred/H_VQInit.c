/*************************************************
* This is a mapper function for initializing VQ
* model in Hadoop framework. It depends on the
* streaming api. It can take a list of files, or
* the file itself as input from stdin. The output
* may be binary or ascii depending upon the cmdline
* arguments
*************************************************/


#include "CommonFunctions.h"
#include<unistd.h>
#include"b64_mod.h"

/**************************************************
* Function: main
* arguments: all are POSIX based command line args
*
* Description: this function is a mapper for VQ
*              mode initialization. It takes all
*              an input file from stdin (because
*              it depends on streaming api)
*              and a bunch of command line options
*              that specify kind of input and output
*              
* Additional Info:
*              the getopt function will process 
*              a bunch of arguments whose purpose are
*              mentioned below
*              -i [list/wave] : i/p is a list of files
*                               or single wavefile
*              -v : use vad during feature extraction. 
*                   argument is followed by erg filename
*              -t : threshold scale when not using VAD   
*              -c : ctrl file's name
****************************************************/

int
main (int argc, char *argv[])
{
   int oc;
	 int ipIsList = 0, opIsBinary = 0, useVad = 0,
	     useTS = 1, byteLength, base64enc = 0;
	 int nbytes = 0, ndbytes = 0;
	 unsigned char *bytearr = NULL, *codedbytearr = NULL;

	 char *ctrlFileName, *waveFileName;	 
	 FILE *ctrlFile;
	 char s[256];

	 char *featureName, *optFeatureName;
	 unsigned int numVectors;
	 int numClusters, numClustersLeft;
	 int rmax, rno, i, seed, featLength, index;
   float tscale = 0.0, psf;

	 ASDF *asdf;
	 VECTOR_OF_F_VECTORS *vfv;
	 
	 char *ergFileName;
	 VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
	 float *ergWts;

	 FILE *oldstdin, *oldstdout;
	 FILE *op, *ip;

	fprintf (stderr,"malloc_check_ set? %s\n", getenv("MALLOC_CHECK_"));
	 while ((oc = getopt (argc, argv, "i:v:t:k:f:n:e")) != -1)
	   {
		   switch (oc)
			   {
				   case 'i':
					     if (!strcmp (optarg, "list"))							   
								   ipIsList = 1;
							 break;		 
					 case 'v':
							 useVad = 1;
							 useTS = 0;
					     ergFileName = optarg;
							 ergMeans = NULL;
							 ergVars = NULL;
							 ergWts = NULL;
							 ReadTriGaussianFile (ergFileName,
							                     &ergMeans,
																	 &ergVars,
																	 &ergWts);
           case 't':
					     if (useVad == 1)
							     break;
						   useTS = 1;
							 sscanf (optarg, "%f", &tscale);
							 break;
					 case 'f':
					     ctrlFileName = optarg;
							 break;
					 case 'n':
					     featureName = optarg;
							 break;
					 case 'k':
					     sscanf (optarg, "%d", &numClusters);
							 numClustersLeft = numClusters;
							 break;
					 case 'e':
					     base64enc = 1;
						  break;
					 default:
					     return 1;
				 }			 
		 }
		
	 // reading from command line env
/*	 ergFileName = getenv ("ergFileName");
   ctrlFileName = getenv ("ctrlFileName");
	 featureName = getenv ("featureName");
	 sscanf (getenv ("numClusters"), "%d", &numClusters);
	 useVad = 1;
		numClustersLeft = numClusters;*/
 		fprintf (stderr,"%s %s %s %d\n", ergFileName, ctrlFileName, featureName, numClusters);
	 // need to change this to hdfs file operation
   ctrlFile = fopen (ctrlFileName, "r");
   asdf = (ASDF *) calloc (1, sizeof (ASDF));
   InitializeStandardFrontEnd (asdf, ctrlFile);
   Cstore (asdf->fftSize);
	 psf = (float) GetFAttribute (asdf, "probScaleFactor");
	 seed = (int) GetFAttribute (asdf, "seed");
	 fclose (ctrlFile);
	 
   fprintf (stderr,"working till here HELLLLLLLOOOO\n");
	 if (base64enc)
	   {
		  // read the encoded file from stdin
		  ip = stdin; 
		  op = tmpfile (); 
		  decode (ip,op);
		  fprintf (op, "%d", EOF);
		  ndbytes = ftell (op);
			fprintf(stderr,"%d bytes decoded\n", ndbytes);
		  rewind (op);
		  bytearr = (unsigned char *) calloc (ndbytes, sizeof (unsigned char));
		  nbytes = 0;
        while (!feof(op) || nbytes == ndbytes) 
		  {
		    bytearr[nbytes++] = (unsigned char) fgetc (op);
		  }
		  ndbytes = nbytes;
			fprintf(stderr,"%d bytes read\n", ndbytes);

		  fclose (op);		  		  
		  oldstdin = stdin;
		  stdin = fmemopen (bytearr, ndbytes, "rb");		    
		}
	 waveFileName = "-";
	 vfv = NULL;
	 if (!ipIsList)
	   {
			 if (useVad)
	         vfv = ExtractFeatureVectorsVAD 
					                     (asdf, 
					                     "-", 
															 featureName,
	                             &numVectors,
	                             ergMeans, 
															 ergVars, 
															 ergWts);
			 else
			    vfv = ExtractFeatureVectors (asdf, waveFileName,
					                       featureName, &numVectors,
																 tscale);
		 }
	 else
	   {
		   if (useVad)
			    vfv = BatchExtractFeatureVectorsVAD (asdf, "-",
			                                        featureName,
			                                        &numVectors,
                                              ergMeans,
			                                        ergVars, 
																							ergWts);
       else
           vfv = BatchExtractFeatureVectors (asdf, "-",
		                                        featureName,
		       					                        &numVectors, 
																						tscale);
		 }

   featLength = vfv[0]->numElements;
	 srand (seed);
   rmax = RAND_MAX;
   fprintf (stderr,"working till here HELLLLLLLOOOO 2 vfv is %d\n", vfv[0]->numElements);
//	 fprintf (stderr,"numVectors is %d\n", numVectors);
	 while (numClustersLeft--)
	   {
	     rno = rand ();
       index = (int) ((float) (rno)/rmax*numVectors);
			 fprintf (stdout, "%d\t%d",numClustersLeft+1,featLength);
			 for (i = 0; i < featLength; i++)			   
					 	fprintf(stdout, ",%e", vfv[index]->array[i]); 						 
			 fprintf(stdout,"\n");	
			fflush (stdout); 
		 }

   FreeVfv (vfv, numVectors);
	 free (asdf);
   return 0;		 
}

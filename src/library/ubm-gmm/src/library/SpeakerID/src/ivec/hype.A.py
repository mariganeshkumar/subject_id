#!/usr/bin/python
# at this stage, it is assumed that the C
# matrix has already been estimated. but we
# are still in the E step. for each c in [0..M]
# we need to estimate A_c

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *

# arguments description
# C: (flat) file to store C matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "F:w:c:o:N:h")

def printhelp (close = 0):
  print "Usage: ./hype.py -F firstorderlist -w wxlist -c mixNo -o opfile -N nmat"
  if close == 1:
    quit ()

cmatfname, wxlistname, folist = "", "", "" 
opname = ""
cval = -1

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-F":
      folist = value
    elif option == "-w":
      wxlistname = value
    elif option == "-c":
      cval = int (value)
    elif option == "-o":
      opname = value
    elif option == "-N":
      nmatfname = value
    elif option == "-h":
      printhelp (1)    
    else:
      print "Invalid argument..."
      printhelp (1)

# check if args are empty
if wxlistname == "" or folist == "" or cval == -1 or opname == "":   
    printhelp (1)

# prepare meta list
wxlistf = open (wxlistname, "r")
wxlist = [ fname.strip () for fname in wxlistf.readlines ()]
wxlistf.close ()

folistf = open (folist, "r")
folist = [ fname.strip () for fname in folistf.readlines ()]
folistf.close ()

listlen = len (folist)
# check if list lengths are same
if listlen != len (wxlist):
    print "Incoherent lengths for input lists. Exiting"
    quit ()

# pair lists
meta = [ (i,folist[i],wxlist[i]) for i in range (0, listlen)]

tempf = open (wxlist[0], "r")
M,D,R = [ int (i) for i in tempf.readline ().strip ().split ()]
tempf.close ()
# there should be a way to reduce the amount of memory used here for C
nrows, ncols = M*D, R
N = np.load (nmatfname)
N = N[cval,:]

A = np.zeros ((R,R))
nopts = 1
for l in range (0,listlen):
   if N[l] > 0.:
       wx,Lx = ReadLxFromIVecEst (wxlist[l])
       A += (N[l] * Lx) + (wx * wx.transpose ())
       nopts = 0
if nopts == 0:           
   A = np.linalg.inv (A)
np.save (opname, A)   

/*
    This is a header file for AdaptGMM.c

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#ifndef __AdaptOnlyMeans_fromDF__
#define __AdaptOnlyMeans_fromDF__

#include "CommonFunctions.h"

VFV* AdaptOnlyMeans (VFV *ubmMeans, 
                     VFV *ubmVars,
                     float *ubmWeights,
                     unsigned int numClusters,
                     VFV *vfv,
                     unsigned long numVectors,
                     float relevanceFactor,
                     float probScaleFactor);

#endif

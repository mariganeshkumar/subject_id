/*
 * =====================================================================================
 *
 *       Filename:  test_zmuv.c
 *
 *    Description:  test's the function zmuv
 *
 *
 * =====================================================================================
 */

#include "CommonFunctions.h"

int main(int argc, char *argv[]) {
    FILE *cfile;
    ASDF *asdf;
    char *feature = "frameCepstrum";
    char *wavName;
    unsigned int numVectors;
    VFV *newVfv;

    if (argc != 4) {
        printf ("Usage: testcmvn wavefile output1 outpu2\n");
        exit(1);
    }

    cfile = fopen("../../cfiles/fe-ctrl.base", "r");
    asdf = (ASDF *) calloc (1, sizeof (ASDF));
    InitializeStandardFrontEnd (asdf, cfile);
    Cstore (asdf->fftSize);

    asdf->zeroMean = 0;
    wavName = argv[1];
    newVfv = ExtractFeatureVectors (asdf, wavName,
               feature, &numVectors, 0.0001);
    printf ("Extracted feature vectors\n");
    if (numVectors > 0) {
        WriteVfvToFile(newVfv, numVectors, argv[2]);
    }
   
    newVfv = MakeZeroMeanUnitVar(newVfv, numVectors);
    if (numVectors > 0) {
        WriteVfvToFile(newVfv, numVectors, argv[3]);
    }
    FreeVfv(newVfv, numVectors);
    fclose(cfile);
    exit(0);
}


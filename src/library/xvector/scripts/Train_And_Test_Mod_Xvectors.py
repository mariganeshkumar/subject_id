import sys
save_dir = sys.argv[1]
test_only = int(sys.argv[2])

# Seed value
# Apparently you may use different seed values at each stage
seed_value= int(sys.argv[3])

# 1. Set `PYTHONHASHSEED` environment variable at a fixed value
import os
os.environ['PYTHONHASHSEED']=str(seed_value)

# 2. Set `python` built-in pseudo-random generator at a fixed value
import random
random.seed(seed_value)

# 3. Set `numpy` pseudo-random generator at a fixed value
import numpy as np
np.random.seed(seed_value)

# 4. Set `tensorflow` pseudo-random generator at a fixed value
import tensorflow as tf
tf.set_random_seed(seed_value)

# 5. Configure a new global `tensorflow` session
from keras import backend as K
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
session_conf.gpu_options.allow_growth = True
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)
import os
import shutil
from x_vector_models import get_modified_x_vector_model

from keras import losses
from keras.utils.np_utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.utils.generic_utils import get_custom_objects
from keras.optimizers import Adam
from keras.metrics import top_k_categorical_accuracy

import scipy.io as sio

import hdf5storage
from sklearn.metrics import accuracy_score

dataset = hdf5storage.loadmat('tmp/subject_id_dataset_for_keras.mat')





def mixed_mse_cross_entropy_loss(y_true, y_pred):
    return 0.8 * losses.categorical_crossentropy(y_true, y_pred) + 0.2 * losses.mean_squared_error(y_true,y_pred)


get_custom_objects().update({"mixed_loss": mixed_mse_cross_entropy_loss})

# split into input (X) and output (Y) variables
trainData = dataset['train_data']
trainLabel = dataset['train_label']
val_data = dataset['val_data']
valLabel = dataset['val_label']
test_data = dataset['test_data']
testLabel = dataset['test_label']
hiddenLayerConfig = dataset['hidden_layer_size']

#train_data = np.moveaxis(train_data, 1, -1)
num_channels = trainData.shape[1]
#train_data = np.reshape(train_data,(shape[0],shape[1],shape[2]*shape[3]))
trainData = np.moveaxis(trainData, 2, -1)
no_of_examples = trainLabel.shape[1]
randperm = np.random.permutation(no_of_examples)
trainLabel = trainLabel[:, randperm]
trainData = trainData[randperm,:,:,:]
print(np.unique(trainLabel))
train_data = trainData
train_label = to_categorical(np.squeeze(trainLabel - 1), num_classes=np.unique(trainLabel).shape[0])




#val_data = np.moveaxis(val_data, 1, -1)
#shape = val_data.shape
#val_data = np.reshape(val_data,(shape[0],shape[1],shape[2]*shape[3]))
val_data = np.moveaxis(val_data, 2, -1)
#no_of_examples = val_label.shape[1]
#randperm = np.random.permutation(no_of_examples)
#val_label = val_label[:, randperm]
#val_data = val_data[randperm,:,:]
val_label =  to_categorical(np.squeeze(valLabel - 1), num_classes=train_label.shape[1])


#test_data = np.moveaxis(test_data, 1, -1)
#shape = test_data.shape
#test_data = np.reshape(test_data,(shape[0],shape[1],shape[2]*shape[3]))
test_data = np.moveaxis(test_data, 2, -1)
#no_of_examples = test_label.shape[1]
#randperm = np.random.permutation(no_of_examples)
#test_label = test_label[:, randperm]
#test_data = test_data[randperm,:,:]
test_label =  to_categorical(np.squeeze(testLabel - 1), num_classes=train_label.shape[1])



model = get_modified_x_vector_model(train_data, train_label, num_channels, hiddenLayerConfig)
adam_opt = Adam(lr=0.001, clipvalue=1)
model.compile(loss=losses.categorical_crossentropy, optimizer=adam_opt,
              metrics=['accuracy', top_k_categorical_accuracy])
model.summary()


if test_only == 0 :
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    os.makedirs(save_dir)
    early_stopping = EarlyStopping(patience=10, verbose=1)
    model_checkpoint = ModelCheckpoint(save_dir+'/keras.model', save_best_only=True, verbose=1)
    reduce_lr = ReduceLROnPlateau(factor=0.5, patience=3, min_lr=0.00000000001, verbose=1)

    #Fit the model
    model.fit(train_data, train_label,validation_data=(val_data, val_label), epochs=3000, batch_size=16, verbose=2,
          callbacks=[early_stopping, model_checkpoint, reduce_lr])
    model.load_weights(save_dir+'/keras.model')
else:
    model.load_weights(save_dir+'/keras.model')

val_data_score = model.predict(val_data, verbose=0, batch_size=32)
predicted_val_labels = np.argmax(val_data_score, axis=1) + 1
valLabel = np.squeeze(valLabel)
print(accuracy_score(valLabel,predicted_val_labels) * 100)

test_data_score = model.predict(test_data, verbose=0, batch_size=32)
predicted_labels = np.argmax(test_data_score, axis=1) + 1
testLabel = np.squeeze(testLabel)
print(accuracy_score(testLabel,predicted_labels) * 100)


trainLabel = np.array(trainLabel, dtype=np.int32)
valLabel = np.array(valLabel, dtype=np.int32)
testLabel = np.array(testLabel, dtype=np.int32)

print("Collecting Target and Non-Target Scores")
target_test_scores = []
non_target_test_scores = []
target_val_scores = []
non_target_val_scores = []
indices = np.arange(len(np.unique(trainLabel)))
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(test_data_score[np.where(testLabel == i), :], axis=0)
    target_scores = scores[:, i-1]
    non_target_scores = np.squeeze(scores[:, indices!=i-1]).flatten()
    target_test_scores.append(target_scores)
    non_target_test_scores.append(non_target_scores)

    scores = np.squeeze(val_data_score[np.where(valLabel == i), :], axis=0)
    target_scores = scores[:, i-1]
    non_target_scores = np.squeeze(scores[:,  indices!=i-1]).flatten()
    target_val_scores.append(target_scores)
    non_target_val_scores.append(non_target_scores)

target_test_scores = np.concatenate(target_test_scores, axis=0)
non_target_test_scores = np.concatenate(non_target_test_scores, axis=0)
target_val_scores = np.concatenate(target_val_scores, axis=0)
non_target_val_scores = np.concatenate(non_target_val_scores, axis=0)


print("Collecting Normalised Target and Non-Target Scores")

finalTestScore=[]
for i in tqdm(np.unique(trainLabel)):
    score = test_data_score[:, i-1]
    imposter_score = test_data_score[:, indices!=i-1]
    mean_is = np.mean(imposter_score,axis=1)
    std_is = np.std(imposter_score,axis=1)
    score = (score - mean_is)/std_is
    finalTestScore.append(score)
finalTestScore = np.transpose(np.array(finalTestScore))


finalValScore=[]
for i in tqdm(np.unique(trainLabel)):
    score = val_data_score[:, i-1]
    imposter_score = val_data_score[:, indices!=i-1]
    mean_is = np.mean(imposter_score,axis=1)
    std_is = np.std(imposter_score,axis=1)
    score = (score - mean_is)/std_is
    finalValScore.append(score)
finalValScore = np.transpose(np.array(finalValScore))


target_test_norm_scores=[]
non_target_test_norm_scores=[]
target_val_norm_scores=[]
non_target_val_norm_scores=[]
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(finalTestScore[np.where(testLabel == i), :], axis=0)
    target_norm_scores = scores[:, i-1]
    non_target_norm_scores = np.squeeze(scores[:, indices!=i-1]).flatten()
    target_test_norm_scores.append(target_norm_scores)
    non_target_test_norm_scores.append(non_target_norm_scores)

    scores = np.squeeze(finalValScore[np.where(valLabel == i), :], axis=0)
    target_norm_scores = scores[:, i-1]
    non_target_norm_scores = np.squeeze(scores[:,  indices!=i-1]).flatten()
    target_val_norm_scores.append(target_norm_scores)
    non_target_val_norm_scores.append(non_target_norm_scores)

target_test_norm_scores = np.concatenate(target_test_norm_scores, axis=0)
non_target_test_norm_scores = np.concatenate(non_target_test_norm_scores,axis=0)
target_val_norm_scores = np.concatenate(target_val_norm_scores, axis=0)
non_target_val_norm_scores = np.concatenate(non_target_val_norm_scores,axis=0)


sio.savemat('tmp/predicted_labels.mat',{
    'predicted_labels': predicted_labels,
    'predicted_val_labels': predicted_val_labels,
    'target_test_scores': target_test_scores,
    'non_target_test_scores': non_target_test_scores,
    'target_val_scores': target_val_scores,
    'non_target_val_scores': non_target_val_scores,
    'target_test_norm_scores': target_test_norm_scores,
    'non_target_test_norm_scores': non_target_test_norm_scores,
    'target_val_norm_scores': target_val_norm_scores,
    'non_target_val_norm_scores': non_target_val_norm_scores,
    })
print('done')

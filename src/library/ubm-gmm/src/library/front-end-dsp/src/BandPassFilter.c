/*
 *                            COPYRIGHT
 *
 *  tdbpf - time domain bandpass filter coefficient calculator.
 *  Copyright (C) 2003, 2004, 2005, 2006 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  info(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
/*#define MAXLINESIZE 128 */

float *BandPassFilter( float lowFrequency, float highFrequency, int samplingRate, int  numCoef)
{
  int i, nc, nz;
  double d1, d2;
  double fc1, fc2, h;
  float  *filterCoefs;
  fc1 = 2*lowFrequency/samplingRate* M_PI;
  fc2 = 2*highFrequency/samplingRate* M_PI;
  nc = numCoef;
  nz = 0;

  d1 = ((double)nc - 1.0)/2.0;
  filterCoefs = (float *) malloc ((numCoef+1)*sizeof(float));
  for( i = 0; i < nc; ++i )
  {
    d2 = (double)i - d1;
    h = d2 == 0 ? (fc2 - fc1) / M_PI : (sin(fc2 * d2) - sin(fc1 * d2)) / (M_PI * d2);
    printf ("h = %f\n", h);
    filterCoefs[i+1] = h;
  return(filterCoefs);
}
main(int argc, char *argv[]) {
  float  *filterCoefs;
  int    numCoef;
  float  lowFrequency, highFrequency;
  int    samplingRate;
  int   i;
  FILE *fp;
 if( argc < 6 )
  {
    printf("\nUsage: fc1 fc2 nc sr outfile\n");
    printf("  fc1 = lower frequency as a fraction of PI [0,1]\n");
    printf("  fc2 = upper frequency as a fraction of PI [0,1]\n");    
    printf("  nc = number of coefficients\n");
    printf("  sr = sampling Rate\n");
    printf ("outfile = output file for writing filter coefficients\n");
    return(-1);
    }
 sscanf(argv[1],"%f", &lowFrequency);
 sscanf(argv[2], "%f", &highFrequency);
 sscanf(argv[3],"%d",  &numCoef);
 sscanf(argv[4], "%d", &samplingRate);
 fp = fopen(argv[5], "w");
 filterCoefs = (float *) BandPassFilter(lowFrequency, highFrequency, samplingRate, numCoef);
 for (i = 1; i <= numCoef; i++) {
   printf ("coef %d = %f \n", i, filterCoefs[i]);
   fprintf (fp, "%f ", filterCoefs[i]);
 }
 fprintf (fp, "\n");
 fclose(fp);
}

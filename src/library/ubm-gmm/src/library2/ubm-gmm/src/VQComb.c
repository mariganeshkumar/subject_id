/*
    This file combines the VQ stats into a model

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri

    This file is part of SpeakerID-IITM-IITM.

    SpeakerID-IITM-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {
	 int i,j,k, numClusters, dim, oc;
   char *opname, *ipname, *fn;
   FILE *ipfile, *opfile, *vqfile;
   VFV *mv = NULL, *vv = NULL;
   float *wv = NULL, wts;
   float tmpw, tmpm, tmpv;
   // i : input is a list of files
   // o : output file
   while ( (oc = getopt (argc, argv, "i:o:")) != -1)
	 {
     switch (oc) {
       case 'i' :
         ipname = optarg;
         break;
       case 'o' :
         opname = optarg;
         break;
       default:
         printf ("Invalid arguments to file\n");
         exit(1);
     }
   }

   if (ipname == NULL || opname == NULL) {
     printf ("Incomplete arguments to file\n");
     printf ("Usage: VQComb -i iplist -o opname\n");
     exit (2);
   }

   ipfile = fopen (ipname, "r");
   opfile = fopen (opname , "w");
   if (ipfile == NULL) {
     printf ("Unable to open file(s)\n");
     exit (3);
   }
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
     mv = (VFV*) calloc (numClusters, sizeof(VFV));
     vv = (VFV*) calloc (numClusters, sizeof(VFV));
     wv = (float*) calloc (numClusters, sizeof(float));
     if (mv == NULL || vv == NULL || wv == NULL) {
       printf ("Unable to allocate memory for output vars\n");
       exit (4);
     }       
     finc(i,numClusters,1) {     
       wv[i] = 0.0;
       mv[i] = AllocFVector (dim);
       vv[i] = AllocFVector (dim);
       if (mv[i] == NULL || vv[i] == NULL) {
         printf ("Unable to allocate memory for output vars\n");
         exit (4);
       }
       finc(j,dim,1) { 
         mv[i]->array[j] = 0.0;
         vv[i]->array[j] = 0.0;
       }
     }
     break;
   }
   rewind (ipfile);
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
   finc(i,numClusters,1) {     
       fscanf (vqfile, "%f", &tmpw);
       wv[i] += tmpw;
       finc(j,dim,1) {
         fscanf (vqfile, " %f", &tmpm);
         mv[i]->array[j] += tmpm;
       }
       finc(j,dim,1) {
         fscanf (vqfile, " %f", &tmpv);
         if(isnan(tmpv) || isinf(tmpv)) tmpv = (1E-4 * tmpw);
         vv[i]->array[j] += tmpv;
       }
     }
     fclose (vqfile);
     free (fn);
   }
   
   wts = 0.0;
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       mv[i]->array[j] /= wv[i];
     }
     wts += wv[i];
   }
   //printf ("Printing variance values before normalizing\n");
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       vv[i]->array[j] = 
         (vv[i]->array[j] / wv[i]) - 
         (mv[i]->array[j] * mv[i]->array[j]);
       if (isnan(vv[i]->array[j]) || isinf(vv[i]->array[j])) { 
         printf ("Flooring %d %d %f\n", i, j, mv[i]->array[j]);
         vv[i]->array[j] = 10E-4;
       }
     }     
   }
   finc(i,numClusters,1) {     
     fprintf (opfile, "%f", wv[i]/wts);
     finc(j,dim,1) 
       fprintf (opfile, " %f", mv[i]->array[j]);
     finc(j,dim,1) 
       fprintf (opfile, " %f", vv[i]->array[j]);
     fprintf (opfile, "\n");
   }   
   fclose (ipfile);
   fclose (opfile);
}

import argparse
import pickle
import os
import shutil
import sys
import scipy.io as sio
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import accuracy_score
from tqdm import tqdm

sys.path.insert(0, 'library/svm_scripts')

parser = argparse.ArgumentParser()

parser.add_argument("--trainDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--testDataScriptFile", help="Location of script file for test files.",
                    type=str, )

parser.add_argument("--valDataScriptFile", help="Location of script file for script files.",
                    type=str, )
#parser.add_argument("--wMat", help="Location of whiting transform matrix.",
#                    type=str, )
parser.add_argument("--ldaMat", help="Location of LDA transform matrix.",
                    type=str, )

args = parser.parse_args()
tmpDir = "tmp/LDA"

if os.path.exists(tmpDir):
    shutil.rmtree(tmpDir)
os.makedirs(tmpDir)

train_iVectorMatrix = []
test_iVectorMatrix = []
val_iVectorMatrix = []

with open(args.trainDataScriptFile) as f:
    trainDataScript = f.readlines()
trainDataScript = [x.strip() for x in trainDataScript]
trainFiles = [x.split(' ')[0] for x in trainDataScript]
trainLabel = [int(x.split(' ')[1]) for x in trainDataScript]

for i in tqdm(range(len(trainFiles))):
    filename = trainFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        train_iVectorMatrix.append(iVec)

with open(args.valDataScriptFile) as f:
    valDataScript = f.readlines()
valDataScript = [x.strip() for x in valDataScript]
valFiles = [x.split(' ')[0] for x in valDataScript]
valLabel = [int(x.split(' ')[1]) for x in valDataScript]

for i in tqdm(range(len(valFiles))):
    filename = valFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        val_iVectorMatrix.append(iVec)

with open(args.testDataScriptFile) as f:
    testDataScript = f.readlines()
testDataScript = [x.strip() for x in testDataScript]
testFiles = [x.split(' ')[0] for x in testDataScript]
testLabel = [int(x.split(' ')[1]) for x in testDataScript]

for i in tqdm(range(len(testFiles))):
    filename = testFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        test_iVectorMatrix.append(iVec)

train_iVectorMatrix = np.array(train_iVectorMatrix)
val_iVectorMatrix = np.array(val_iVectorMatrix)
test_iVectorMatrix = np.array(test_iVectorMatrix)

trainLabel = np.array(trainLabel)
valLabel = np.array(valLabel)
testLabel = np.array(testLabel)

#wMAT = np.genfromtxt(args.wMat, delimiter=' ')
#train_iVectorMatrix = np.matmul(train_iVectorMatrix, wMAT)
#val_iVectorMatrix = np.matmul(val_iVectorMatrix, wMAT)
#test_iVectorMatrix = np.matmul(test_iVectorMatrix, wMAT)

clf = pickle.load(open(args.ldaMat,'rb'))
train_iVectorMatrix = clf.transform(train_iVectorMatrix)
val_iVectorMatrix = clf.transform(val_iVectorMatrix)
test_iVectorMatrix = clf.transform(test_iVectorMatrix)
sio.savemat(tmpDir+'/LDAData.mat',{
    'train_iVectorMatrix': train_iVectorMatrix,
    'trainLabel': trainLabel,
    'val_iVectorMatrix': val_iVectorMatrix,
    'valLabel': valLabel,
    'test_iVectorMatrix': test_iVectorMatrix,
    'testLabel': testLabel,
    })

#!/usr/bin/python
# initialize a T matrix with a given size
# and save it in a hdf5 format
import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
import GMM

# option description
# h: print help
# o: T matrix output file
# M: numMix
# D: dim
# R: subspace size
# u: ubm [optional]

optlist, args = getopt.getopt (sys.argv[1:], "o:M:D:R:u:h")

M, D, R = 0,0,0
ubmfname = ""
normalize = 0
def printhelp () :
    print "Usage: python inittmat.py -o opfilename \
           -M numMix -R numfactors -D featureDim"

for arg in optlist:
    option = arg[0]
    value = arg[1]

    if option == "-M":
        M = int (value)
    elif option == "-R":
        R = int (value)
    elif option == "-D":
        D = int (value)
    elif option == "-o":
        opfname = value
    elif option == "-u":
        ubm = GMM.ReadGMM (value)
        normalize = 1 
    elif option == "-h":
        printhelp ()
        quit ()
    else:
        print "Invalid option %s, continuing..." % value

if M == 0 or R == 0 or D == 0:
  printhelp ()

if normalize == 1:
  ubmvars = 1/np.sqrt (ubm.GetVars ())
  ubmvars = ubmvars.reshape ((M*D,))
else:
  ubmvars = np.ones ((M*D,))

tmat = np.random.normal (0.0,1.0,M*D*R)
tmat = tmat.reshape ((M*D,R))
hdf, mat, dim = hdf5open (opfname, M,D)
row = mat.row
for r in range(0,R):
  row['colvec'] = tmat[:,r] * ubmvars
  row.append ()
mat.flush ()  

row = dim.row
tempnd = np.zeros ((2,))
tempnd[0] = M*D
tempnd[1] = R
row['dim'] = tempnd
row.append ()
dim.flush ()
hdf.close ()

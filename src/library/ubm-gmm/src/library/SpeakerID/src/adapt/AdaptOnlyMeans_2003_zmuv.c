
/* Makes the feature vectors zero mean and unit variance before
adaptation. Will be used for developing models for NIST 2004
cohorts.
Uses zero man function from CommonFunctions.c
*/

#include "AdaptOnlyMeans_2003_zmuv.h"

VECTOR_OF_F_VECTORS* AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
				    VECTOR_OF_F_VECTORS *ubmVars,
				    float *ubmWeights,
				    unsigned int numClusters,
			            VECTOR_OF_F_VECTORS *vfv,
				    unsigned int numVectors,
				    unsigned int relevanceFactor,
				    float probScaleFactor)
  {
	VECTOR_OF_F_VECTORS	*expectation=NULL, *tempMeans;
	float			alpha, beta, normFactorI, maxProb, *logProb;
	unsigned int		dimensions, i, j, k, maxProbIdx, *eeta, tempMaxProbIdx;

	VECTOR_OF_F_VECTORS     *adaptedMeans;

	dimensions = ubmMeans[0]->numElements;

	logProb = (float *) calloc (vfv[0]->numElements, sizeof(float));
	expectation = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof(VECTOR_OF_F_VECTORS));
	tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof(VECTOR_OF_F_VECTORS));
	for (j = 0; j < numClusters; j++)
	  {
		tempMeans[j] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
		  for (k = 0; k < vfv[0]->numElements; k++)
			tempMeans[j]->array[k] = 0.0;
	  }

	
	eeta    = (unsigned int *) calloc (numClusters, sizeof(unsigned int));
	printf ("Finished initialization\n"); fflush(stdout);
	for (i = 0; i < numVectors; i++)
	   {
		for (j = 0; j < numClusters; j++)
		  {
			logProb[j] = ComputeProbability (ubmMeans[j], ubmVars[j], ubmWeights[j], 
								   vfv[i], probScaleFactor);

			if (j == 0 || logProb[j] > maxProb)
			  { maxProb= logProb[j]; maxProbIdx= (unsigned int)j; }

		  }
//		printf ("feature vector number %d, maxProbIdx is %u\n",i, maxProbIdx);

//		printf ("incrementing %u to %u\n", tempMaxProbIdx, eeta[tempMaxProbIdx]);
		eeta[maxProbIdx] = eeta[maxProbIdx] + 1;
		for (k = 0; k < dimensions; k++)
		  {			
			tempMeans[maxProbIdx]->array[k] = tempMeans[maxProbIdx]->array[k] + vfv[i]->array[k];	
		  }
	   }

//	for ( j =0; j < numClusters; j++) printf ("Cluter no : %d, eeta %u \n", j+1, eeta[j]);

	for (j = 0; j < numClusters; j++)
		for (k = 0; k < dimensions; k++) { if (eeta[j] != 0)
			tempMeans[j]->array[k] /= ((float)eeta[j]);}
		
	adaptedMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
	for (j = 0; j < numClusters; j++)
  	  {
	    adaptedMeans[j] = (F_VECTOR *) AllocFVector (dimensions);
	    
	    alpha = (float) ((float)eeta[j] / ((float)eeta[j] + (float)relevanceFactor));
//	   printf ("alpha is %f while eeta of %d is %f\n", alpha,j,eeta[j]); 
	    for (k = 0; k < dimensions; k++)
	      {
		adaptedMeans[j]->array[k] = (alpha * tempMeans[j]->array[k])
					  + ((1.0 - alpha) *ubmMeans[j]->array[k]);
//		printf ("adaptedMeans %d %d = %f, ubmMeans - %f\n", j,k,adaptedMeans[j]->array[k], ubmMeans[j]->array[k]);
	      }
	  }
	//free (logProb);*/
	return adaptedMeans;
  }




main (int argc, char *argv[])
  {
	char		*controlFileName, *ubmFileName, *featureName, *modelFileName, *trainFileName;
	FILE		*controlFile, *ubmFile, *modelFile;
	VECTOR_OF_F_VECTORS *vfv, *ubmMeans, *ubmVars, *adaptedMeans;
	F_VECTOR	    *ubmWeights;
	ASDF 		    *asdf;
	unsigned int	numVectors, ubmSize, featLength, i, j;
	float 		thresholdScale;
	if (argc != 8)
	  {
		printf ("Usage : %s controlFile ubmFile ubmSize wavName featureName modelFile thresholdScale\n", argv[0]);
		exit (0);
	  }
	
   	controlFileName = argv[1];
	ubmFileName     = argv[2];
	sscanf (argv[3], "%u", &ubmSize);
	trainFileName   = argv[4];
	featureName     = argv[5];
	modelFileName   = argv[6];
	sscanf (argv[7], "%u", &thresholdScale);
	
	controlFile = fopen (controlFileName, "r");
	asdf = (ASDF *) calloc (1, sizeof (ASDF));
	InitializeStandardFrontEnd (asdf, controlFile);		
	Cstore (asdf->fftSize);
	
	vfv = (VECTOR_OF_F_VECTORS *) ExtractFeatureVectors 
    (asdf,trainFileName, featureName, &numVectors, thresholdScale);
	featLength = vfv[0]->numElements;

	ubmMeans = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	ubmVars  = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	
	for (i = 0; i < ubmSize; i++)
	  {
		ubmMeans[i] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
		ubmVars[i]  = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
	  }
	
	ubmWeights = (F_VECTOR *) AllocFVector (ubmSize);

	ubmFile = fopen (ubmFileName, "r");
	for (i = 0; i < ubmSize; i++)
	  {
		fscanf (ubmFile, "%f\n", &ubmWeights->array[i]);
		for (j = 0; j < featLength; j++)
		{
			fscanf (ubmFile, " %f %f", &ubmMeans[i]->array[j], &ubmVars[i]->array[j]);
		}
		fscanf (ubmFile, "\n");
	  }
	fclose (ubmFile);

	adaptedMeans = (VECTOR_OF_F_VECTORS *) AdaptOnlyMeans (ubmMeans, ubmVars, ubmWeights->array, 
								ubmSize, vfv, numVectors, 16,
								asdf->probScaleFactor);
	modelFile = fopen (modelFileName, "w");
	
	for (i = 0; i < ubmSize; i++)
	  {
		fprintf (modelFile, "%e\n", ubmWeights->array[i]);
		for (j = 0; j < featLength; j++)
		  {
			fprintf (modelFile, " %e %e", adaptedMeans[i]->array[j], ubmVars[i]->array[j]);
		  }
		fprintf (modelFile, "\n");
	  }
	fclose (modelFile);	
  }

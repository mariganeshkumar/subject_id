#!/usr/bin/python
# estimate C and store it in a file
# for T matrix

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *


# arguments description
# C: (flat) file to store C matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "C:i:h")

def printhelp (close = 0):
  print "Usage: ./hype.C.red.py [options] \n -C Cmatrix.file \n -i inputlist"
  if close == 1:
    quit ()

cmatfname, listname = "", ""
for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-C":
        cmatfname = value
    elif option == "-i":
        listname = value
    else:
        printhelp (1)
if cmatfname == "" or listname == "":
    printhelp (1)

listf = open (listname, "r")  
for i,fname in enumerate (listf):
    fn = fname.strip ()
    if i == 0:
      C = np.load (fn)
      continue
    C += np.load (fn)
np.save (cmatfname, C)    

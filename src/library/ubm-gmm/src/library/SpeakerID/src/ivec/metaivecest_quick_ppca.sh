#!/bin/bash

# run ivector estimation by splitting input list
# based on the number of cores available

# currently no remote m/cs are used
matlab=matlab
username=`whoami`
randval=`date | awk '{print $4}' | tr ':' '.'`

if [ $# -lt 3 ]; then
  echo "Usage: ./metaivecest.sh iplist wlist \
  tmatfname \
  [comma_list_of_remote/numcores comma_list_of_wrkdirs]"
  exit
fi
iplist=$1
wlist=$2

tmatname=$3

# flags
transferTmatrix=1
copystats=0


numcores=0
if [ $# -gt 3 ]; then
  opt3=$4
  opt4=$5
else
  opt3=":"
  opt4="."
fi

ipfoldername=`head -1 $iplist`
ipfoldername=`dirname $ipfoldername | cut -d '/' -f1` # assume the given path is relative

oplist=$wlist
opfoldername=`head -1 $oplist`
opfoldername=`dirname $opfoldername | cut -d '/' -f1`

echo Input folder name is $ipfoldername
echo Output folder name is $opfoldername

itemno=0
corelist=
remotelist=
wrkdirlist=
lwrkdirlist=
lremotelist=
numremotes=0

# compute the number of total number of cores,
# prepare a list of host and remotes,

for item in `echo $opt3 | sed 's/,/\ /g'`; do
   i1=`echo $item | cut -d '/' -f1`
   i2=`echo $item | cut -d '/' -f2`
   if [ "$i1" == ":" ]; then
     hostidxno=$itemno
     if [ "$i2" == "" ]; then
       corelist[$itemno]=`parallel --number-of-cores`
     else
       corelist[$itemno]=$i2
       echo Choosing $i2 cores in host
     fi
   else
     corelist[$itemno]=$i2
   fi
   remotelist[$itemno]=$i1
   echo $i1 $i2
   numcores=`echo ${corelist[$itemno]}+$numcores | bc`
   ((itemno++))
done
numremotes=$itemno
numitems=$numremotes
itemno=0

echo Number of remotes is $numremotes

## load working directories in a list
for item in `echo $opt4 | sed 's/,/\ /g'`; do
  wrkdirlist[$itemno]=$item
  if [ "${remotelist[$itemno]}" != ":" -a $transferTmatrix -ne 0 ]; then
    tmatdir=`dirname $tmatname`
    ssh ${remotelist[$itemno]} mkdir ${wrkdirlist[$itemno]}/$tmatdir/
    rsync -av $tmatname $username@${remotelist[$itemno]}:${wrkdirlist[$itemno]}/$tmatdir/
  fi
  ((itemno++))
done
## create list of launchers and i/o dirs in remote
#
itemno=0
((numitems--))
for i in `seq 0 $numitems`; do
  if [ "${remotelist[$i]}" == ":" ]; then
    sshprefix=""
  else
    sshprefix="ssh ${remotelist[$i]}"
    echo "$sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$ipfoldername"
    $sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$ipfoldername
    echo "$sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$opfoldername"
    $sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$opfoldername
  fi
  for j in `seq ${corelist[$i]}`; do
      if [ ${remotelist[$i]} == ":" ]; then
        launchlist[$itemno]=""
      else
        launchlist[$itemno]="$sshprefix ${remotelist[$i]}"
      fi
      lremotelist[$itemno]=${remotelist[i]}
      lwrkdirlist[$itemno]=${wrkdirlist[i]}
      ((itemno++))
  done
done
ll=`wc -l $iplist | cut -d ' ' -f1`
((sl=ll/numcores))
((tl=sl*numcores))
if [ $tl -lt $ll ]; then
  ((sl++))
fi  
i=0
echo $sl $ll
## create sub lists and copy files to relevant
## folders on the remote
#
#
for startno in `seq 1 $sl $ll`; do
  remotemc=${lremotelist[i]}
  tail -n +$startno $iplist | head -$sl > $iplist.$i
  tail -n +$startno $oplist | head -$sl > $oplist.$i

  if [ "$remotemc" != ":" ]; then    
      echo remote is not local host
      echo "rsync -a --files-from=$iplist.$i . $username@$remotemc:${lwrkdirlist[$i]}/"
      rsync -a --files-from=$iplist.$i . $username@$remotemc:${lwrkdirlist[$i]}/
      scp $iplist.$i $oplist.$i $username@$remotemc:${lwrkdirlist[$i]}
      bname_iplist=`basename $iplist.$i`
      bname_oplist=`basename $oplist.$i`
      echo "Launching process"
      echo $tmatname | parallel  -S 1/$remotemc --wd ${lwrkdirlist[$i]} 'matlab  -nojvm -nosplash -nodesktop -r "metaivecest_quick_ppca('\'{}\', \'$bname_iplist\', \'$bname_oplist\', 1, 1')" > /dev/null' &
      echo $tmatname | parallel --dry-run  -S 1/$remotemc --wd ${lwrkdirlist[$i]} 'matlab -nojvm -nosplash -nodesktop -r "metaivecest_quick_ppca('\'{}\', \'$bname_iplist\', \'$bname_oplist\', 1, 1')" > /dev/null'
  else
  ls $tmatname | parallel -j 1 --dry-run 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "metaivecest_quick_ppca (' \'{}\',\'$iplist.$i\',\'$oplist.$i\',1,1')"' &
  ls $tmatname | parallel -j 1 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "metaivecest_quick_ppca(' \'{}\',\'$iplist.$i\',\'$oplist.$i\',1,1')"' &
  fi
  ((i++))
done
wait;
## copy all the result folders and delete input folders
#mkdir -p $opfoldername
j=0
for i in `seq 0 $numitems`; do
  temp_num_cores=${corelist[$i]}
  if [ "${remotelist[$i]}" != ":" ]; then
    ((j_end=j+temp_num_cores))
    ((j_end--))
    for j1 in `seq $j $j_end`; do
        rsync -K -avz --files-from=$oplist.$j1 $username@${remotelist[$i]}:${wrkdirlist[$i]}/ .
    done
  fi
  ((j+=temp_num_cores))
done  

rm -f $iplist.[0-9+]
rm -f $oplist.[0-9+]

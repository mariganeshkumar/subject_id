/*-------------------------------------------------------------------------
 *  FmtAntSynthesis.c - A Synthesis program based on formants
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Mon 11-Jun-2007 14:05:16
 *  Last modified:  Mon 22-Jul-2013 18:04:43 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include <sys/types.h>
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/SphereInterface.h"
#include "fe/InitAsdf.h"
#include "fe/QuickSort.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"

/*-------------------------------------------------------------------------
 *  ARMAsignal -- Generates an ARMA signal
 *    Args:	residual signal,  ArSignal, numSamples
 *              coefZero, coefPole, orderZ, orderP 
 *    Returns:	returns the signal generated in arSignal 
 *    Bugs:	
 * -------------------------------------------------------------------------*/

	void ARMAsignal(float *residual,float *arSignal,int numSamples,
			float *coefZero,int orderZ,float *coefPole,int orderP) {
        int             i, j;
        float           sum1, sum2;
	printf("OrderZ= %d OrderP= %d\n", orderZ, orderP);
	//      for(i=1; i<=orderP; i++)
	//printf("arSignal %d = %f\n",i, arSignal[i]); 
	for (i = 1; i <= numSamples; i++){
	  arSignal[i+orderP] = residual[i+orderZ]; 
	  sum1 = 0.0;
          if (orderZ != 0)
	    for (j = 1; j <= orderZ; j++)
	      sum1 = sum1 +coefZero[j]*residual[i+orderZ-j];
	  sum2 = 0.0;
	  for (j = 1; j <= orderP; j++)
	    sum2 = sum2 + coefPole[j]*arSignal[i+orderP-j];
	  arSignal[i+orderP] = arSignal[i+orderP]+sum1-sum2;
	  //printf("residual %d=%f arSignal %d= %f\n",i+orderZ,residual[i+orderZ], 
	  // i+orderP, arSignal[i+orderP]); 
	}
}	/*  End of Armasignal		End of Armasignal   */

/*-------------------------------------------------------------------------
 *  PolynomialProduct --  Computes the product of Pole/Zero Polynomial
 *    Args:	Array of frequencies, rootArray, 
 *    Returns:	Array of polynomial coefficients
 *    Bugs:	
 * -------------------------------------------------------------------------*/

void PolynomialProduct(float *freqArray, float *bw, int numRoots, 
		       float samplingInterval, float *coefArray, 
		       int *degree, float *amp)
  {
    float          b[20], c[20], rootArray[20][3];
    int            newDegree;
    int            i, j, k;
    float          Pi;
  
    Pi = 4*atan(1.0); 
    *amp = 1.0;
    for (i = 1; i <= numRoots; i++){
      rootArray[i][0] = 1.0;
      rootArray[i][1] = -2.0*expf(-Pi*bw[i]*samplingInterval)*
	cosf(2.0*Pi*freqArray[i]*samplingInterval);
      rootArray[i][2] = expf(-2*Pi*bw[i]*samplingInterval);
      *amp = *amp*(1 + rootArray[i][1] + rootArray[i][2]);
      //  printf("rootArray[%d][0]= %f rootArray[%d][1]=%f rootArray[%d][2]=%f\n",
      //     i,rootArray[i][0],i,rootArray[i][1],i,rootArray[i][2]);
           printf("amp = %f\n",*amp);
    }

    newDegree = 2;
    for (k = 0; k <= 2*numRoots; k++)
      c[k] = 0.0;
    for (k = 0; k <= 2; k++){
      coefArray[k] = rootArray[1][k];
      //    printf("coefArray[%d]=%f rootArray[%d][%d]=%f\n",
      //     k,coefArray[k],1,k,rootArray[1][k]);
    }
    for (i = 2; i <= numRoots; i++) {
      for (k = 0; k <= 2; k++)
	b[k] = rootArray[i][k];
      for (k = 0; k <= newDegree; k++) 
	for(j = 0; j  <= 2; j++){
	  c[k+j] = c[k+j]+coefArray[k]*b[j];
	  //  printf("coefArray[%d]=%f b[%d]=%f c[%d]= %f\n",
	  //		 k,coefArray[k],j,b[j],k+j,c[k+j]);
	}
      newDegree = newDegree+2;
      for (k = 0; k <= newDegree; k++) {
	coefArray[k] = c[k];
	c[k] = 0.0;
      }
    }
    *degree = newDegree;

  }	/*  End of PolynomialProduct		End of PolynomialProduct   */
 

/*-------------------------------------------------------------------------
 *  SortRoots -- Sorts the roots in ascending order of Omega
 *    Args:	coefficient array, number of roots
 *    Returns:	none
 *    Bugs:	
 * -------------------------------------------------------------------------*/

void SortRoots(float **poleZero,int number)
{
  int		i,j;
  float 	omega[20],small;
  float		temp[2][2];

  for (i = 1; i <= number; i++)
    if (poleZero[i][0] != 0) 
      omega[i] = abs(atan(poleZero[i][1]/poleZero[i][0]));
    else
      omega[i] = PI/2;
  for (i = 1; i <= number; i++){
    small = omega[i];
    for (j = i + 1; j <= number; j++)
      if (omega[j] < small) {
	small = omega[j];
	temp[1][0] =  poleZero[j][0];
	temp[1][1] = poleZero[j][1];
	omega[j] = omega[i];
	omega[i] = small;
	poleZero[j][0] = poleZero[i][0];
	poleZero[j][1] = poleZero[i][1];
	poleZero[i][0] = temp[1][0];
	poleZero[i][1] = temp[1][1];
      }
    for(i = 1; i <= number; i++)
      printf("omega %d = %f\n",i,omega[i]);
  }

}	/*  End of SortRoots		End of SortRoots   */
void Usage() {
  printf(" LPSyn ctrlFile excitationType outputSpeechFile outputResFile PoleData ZeroData (residualData) || (pitchData gainData) n1 n2 || \n");
  printf("LPSyn ctrlFile excitationType outputSpeechFile outputResFile PoleData (residualData) || (pitchData gainData) n1 n2 \n");
  fflush(stdout);
}
int main(int argc, char *argv[]){

  float		freqPoles[10],bwPoles[10],
                freqZeros[10],bwZeros[10],bw[10];
  int 	        degreePole,degreeZero; 
  int         	n1,n2,numSamples;
  int		fftSize;
  int		frameSize, frameNum, frameLimit, iloc;
  int 	        i, j, orderP, orderZ;
  int		numFmts, numAnts;
  int           pitchPulsesPerFrame;
  int           numFrames, frameAdvanceSamples;
  int           seed;
  float		coefPole[20],coefZero[20];
  float		samplingFrequency, samplingInterval;
  float         amp1=1.0, amp2=1.0; 
  float 	gain, pitch;
  float 	*arSignal, *residual,
                *arSignalTemp, *residualTemp;
  float 	*exitn, tempext;
  float		rmax,average;
  FILE 	        *fAnts=NULL,*fFmts=NULL,*fPitch=NULL,*fGain=NULL, 
                *controlFile,
                *fResidual=NULL, *fp=NULL,*fpResidual=NULL, *fCoef=NULL;
  char          *textFileName=NULL, *riffFileName=NULL, *coefFileName=NULL;
  ASDF          *asdf;
  char 	        ans = 'i', line[200];


  if (argc < 7) {
    Usage();
    exit(-1);
  } 

  controlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  frameSize = (int) GetIAttribute(asdf, "windowSize");
  fftSize  = (int) GetIAttribute(asdf, "fftSize");
  samplingFrequency = (int) GetIAttribute(asdf, "samplingRate");
  frameAdvanceSamples = (int) GetIAttribute(asdf, "frameAdvanceSamples");
  seed = (int) GetIAttribute (asdf, "seed");
  srand(seed);
  Cstore(fftSize);
  samplingInterval = 1.0/samplingFrequency;
  sscanf(argv[2], "%c", &ans);
  printf("samplingFrequency = %f sampling Interval = %f\n", 
	 samplingFrequency, samplingInterval); 
  textFileName = (char *)malloc((strlen(argv[3])+5)*sizeof(char));
  riffFileName = (char *)malloc((strlen(argv[3])+5)*sizeof(char));
  coefFileName = (char *)malloc((strlen(argv[3])+6)*sizeof(char));
  strcat(textFileName, argv[3]);
  strcat (textFileName, ".txt"); 
  strcat(riffFileName, argv[3]);
  strcat (riffFileName, ".wav"); 
  strcat (coefFileName, argv[3]);
  strcat (coefFileName, ".coef");
  fp = fopen(textFileName,"w");                   //output speech file 
  fpResidual = fopen(argv[4],"w");             //output residual file
  fCoef = fopen(coefFileName, "w");
  printf("number of Formants:");
  scanf("%d",&numFmts);
  if (numFmts != 0) {
    printf("Bandwidth :");
    for (i =1; i <= numFmts; i++){
      printf("Bandwidth of %dth formant", i);
      scanf("%f",&bwPoles[i]);
      fFmts = fopen(argv[5],"r");
    }
  }
  printf("number of Anti-Formants:");
  scanf("%d",&numAnts);
  if (numAnts != 0) {
    printf("Bandwidth as a percentage of Anti-Formant :");
    for (i =1; i <= numAnts; i++){
      printf("Bandwidth of %dth Anti-formant", i);
      scanf("%f",&bwZeros[i]);
    }
    if (fFmts != NULL) {
      fAnts = fopen(argv[6],"r");
      printf("zeros %s\n",argv[7]);
    }
    else
      fAnts = fopen(argv[5],"r");
  }
  arSignalTemp = (float *) AllocFloatArray(arSignalTemp,frameSize+2*numFmts+1);
  residualTemp = (float *) AllocFloatArray(residualTemp,frameSize+2*numAnts+1);
  if (ans != 'e') {
    if (fFmts != NULL){
      if (fAnts != NULL){
	fPitch = fopen(argv[7],"r");
	fGain = fopen(argv[8],"r");
      } else if ((fFmts == NULL) || (fAnts == NULL)) {
	fPitch = fopen(argv[6],"r");
	fGain = fopen(argv[7],"r");
      }
      numFrames = 0;
      while (fgets (line, 200, fPitch) != NULL)
        numFrames++;
      rewind(fPitch);
    }
    /* Begin Creation of pitch pulse  for each pitch period based on ExcitationType */
    /* Initialise the excitation array.  Assumes that the pitch period can
       be a maximum of frameSize samples */
    exitn = (float *) malloc((frameSize+1)*sizeof(float));
    for (i = 1; i <= frameSize; i++) 
      exitn[i] = 0;
    if ((ans  ==  'g') || (ans  ==  'r')) {
      if ((fFmts != NULL) && (fAnts != NULL)) {
	sscanf(argv[9],"%d", &n1);
	sscanf(argv[10],"%d", &n2);
      } else if ((fFmts == NULL) || (fAnts == NULL)) {
	sscanf(argv[8],"%d", &n1);
	sscanf(argv[9],"%d", &n2);
      }
      for (i = 1; i <= n1; i++) 
	exitn[i] = 0.5 - 0.5*cos(PI*(i-1)/n1);
      for (i = n1+1; i <= n1+n2; i++) 
	exitn[i] = cos(PI*(i-1-n1)/2/n2);
      if (ans == 'r') {
	tempext = exitn[1];
	for (i = 2; i <= n1+n2; i++) {
	  exitn[i]=exitn[i]-tempext;
	  tempext = exitn[i]+tempext;
	}
      }
    } else if (ans  ==  'i') 
      exitn[1] = 1;
    numSamples = (numFrames)*frameSize+1;
    arSignal = (float *) AllocFloatArray(arSignal, numSamples+frameSize+1);
    residual = (float *) AllocFloatArray(residual, numSamples+frameSize+1);
    iloc = 0;
    frameLimit = frameSize;
    printf("numSamples= %d fLim= %d\n",numSamples, frameLimit);
    while (fgets (line,200,fPitch) != NULL){
      sscanf(line,"%f", &pitch);
      fgets(line, 200, fGain);
      sscanf(line,"%f", &gain);      
      if (pitch  !=  0) {
	if (asdf->timeOrFreq == 1)
	  pitch = 1.0/pitch*samplingFrequency;
        pitchPulsesPerFrame = frameLimit/(int) pitch;
	if (pitchPulsesPerFrame*pitch != frameLimit) 
          pitchPulsesPerFrame++;
	frameLimit = frameSize - pitchPulsesPerFrame*(int) pitch % frameLimit;
	for (i = 1; i <= pitchPulsesPerFrame; i++){
	  for (j = 1; j <= pitch; j++)
	    residual[j+iloc] = exitn[j]*gain;
	  iloc = iloc+pitch;
	  }
      } else {
	for (i = 1; i <= frameLimit; i++)
	  residual[i+iloc] = (float)rand()/(float)RAND_MAX*gain;
	iloc= iloc + frameLimit;
      }
    }
    fclose(fPitch);
      fclose(fGain);
  }/* End of generation of artificial residual using impulse train/glottal pulse/random noise */
  else 
    {
      if ((fFmts != NULL) && (fAnts != NULL))
	fResidual = fopen(argv[7],"r");
      else if ((fFmts == NULL ) || (fAnts == NULL))
      fResidual = fopen(argv[6],"r");
      iloc = 0;
      while (fgets(line,200,fResidual) != NULL) 
	iloc++;
      numSamples = (iloc+1);
      arSignal = (float *) AllocFloatArray(arSignal, numSamples+1);
      residual = (float *) AllocFloatArray(residual, numSamples+1);
      iloc = 0;
      rewind(fResidual);
      while (fgets(line,200,fResidual) != NULL) {
	iloc++;
    sscanf(line,"%f",&residual[iloc]);
      }
      fclose(fResidual);
      numFrames = 0;
      if (fFmts != NULL) {
        while (fgets(line, 200, fFmts) != NULL) 
          numFrames++;
        rewind (fFmts);
      } else if (fAnts != NULL) {
        while (fgets(line, 200, fAnts) != NULL)
          numFrames++;
        rewind (fAnts);
      } else {
	printf ("Synthesis is not possible \n");
        exit (-1);
      }
    } /* Read residual from file */

  frameNum = 0;
  while (frameNum < numFrames) {
    printf("frameNum= %d\n",frameNum);
    if (fFmts != NULL) {
      for (i = 1; i <= numFmts; i++) {
	fscanf(fFmts,"%f",&freqPoles[i]); 
	printf("freqPoles %d = %f\n", i, freqPoles[i]);
      }
      lseek((int) fFmts,(long) frameNum+1,0);
      for (i = 1; i <= numFmts; i++){
	freqPoles[i] = freqPoles[i]/fftSize*samplingFrequency;
	bw[i] = bwPoles[i]/100.0*freqPoles[i]; 
      }
      for (i = 1; i <= numFmts; i++)
	printf("Before freqPoles[%d]= %f bw[%d] = %f\n",i,
	       freqPoles[i],i,bw[i]); 
      PolynomialProduct(freqPoles, bw, numFmts, samplingInterval, 
			coefPole, &degreePole, &amp1);
      orderP = degreePole;
      coefPole[0] = 0;
      for (i = 1; i <= orderP; i++)
        fprintf(fCoef, "%f ", -coefPole[i]);
      fprintf(fCoef, "\n");
    } else orderP = 0;
    if (fAnts != NULL) { 
      for (i = 1; i <= numAnts; i++)
	fscanf(fAnts,"%f",&freqZeros[i]);
      for (i = 1; i <= numAnts; i++)
	printf("Before freqZeros[%d]= %f\n",i,freqZeros[i]);
      for (i = 1; i <= numAnts; i++){
	freqZeros[i] = freqZeros[i]/fftSize*samplingFrequency;
	bw[i] = bwZeros[i]/100.0*freqZeros[i];
	}
      for (i = 1; i <= numAnts; i++)
	printf("zeros %d = %f bw = %f\n",i, freqZeros[i],bw[i]);
      
      /*	  Compute the product of the Polynomials */
      PolynomialProduct(freqZeros, bw, numAnts, samplingInterval, 
			coefZero, &degreeZero, &amp2);
      orderZ = degreeZero;
      coefZero[0] = 0;
    } else orderZ = 0;
    if (frameNum == 0) {
      for (i = 1; i <= orderZ; i++)
	residualTemp[i] = 0;
      for (i = 1; i <= orderP; i++)
	arSignalTemp[i] = 0;
    } else {
      for (i = 1; i <= orderZ; i++)
	residualTemp[i] = residualTemp[frameSize+i];
      for (i = 1; i <= orderP; i++)
	arSignalTemp[i] = arSignalTemp[frameSize+i];
    }
    for (i = 1; i <= frameSize; i++)
      residualTemp[i+orderZ] = residual[i+frameNum*frameSize]*amp1/amp2;
    ARMAsignal(residualTemp,arSignalTemp,frameSize,
	       coefZero,orderZ,coefPole,orderP);
    for (i = 1; i <= frameSize; i++){
      arSignal[i+frameNum*frameSize] = arSignalTemp[i+orderP];
      fprintf(fp,"%f\n",arSignal[i+frameNum*frameSize]);
      fprintf(fpResidual,"%f\n",residual[i+frameNum*frameSize]);
    }
    frameNum++;
  }
  if(fFmts != NULL)
    fclose(fFmts);
  if(fAnts != NULL)
    fclose(fAnts);
  average = 0.0;
  fclose(fp);
  fclose(fpResidual);
  fclose(fCoef);
  /*for (i = 1; i <= numSamples; i++)
    average = average + arSignal[i];
  average = average/numSamples;
  for (i = 1; i <= numSamples; i++)
    arSignal[i] =arSignal[i] -average;
  rmax = fabs(arSignal[Imax(arSignal,numSamples)]);
  printf("rmax= %f numSamples = %d\n",rmax, numSamples);
    utterance = (short *) malloc ((numSamples+1)*sizeof(short));
  if (utterance == NULL) {
    printf("Unable to allocate wave array\n");
    fflush(stdout);
    exit(-1);
    }
  for (i = 1; i <= numSamples; i++){
    utterance[i-1] = (short) (arSignal[i]/rmax*2047.0);
  }
  printf("written to short array\n");
  fflush(stdout);
  wavSynth = sp_open("spout.wav","wv");
  wavOld = sp_open("../data/712a.wav","r");
  wavResidual = sp_open("residual.wav","w");
  printf("files opened\n");
  fflush(stdout);
  sp_copy_header(wavOld,wavSynth);
      sp_copy_header(wavOld,wavResidual); 
  printf("before modifying file \n");
  fflush(stdout);
     fprintf(wavSynth,"NIST 1A\n");
   fprintf(wavSynth,"%d\n",1024);

  sp_h_set_field(wavSynth, "sample_coding", T_STRING, "PCM");
  sp_h_set_field(wavSynth, "sample_n_bytes", T_STRING, "-i 2");
     sp_h_set_field(wavSynth, "sample_rate", T_INTEGER, (SP_INTEGER) samplingFrequency);
  sp_h_set_field(wavSynth, "sample_min", T_INTEGER, -2047);
  sp_h_set_field(wavSynth, "sample_max", T_INTEGER, 2047);
  sp_h_set_field(wavSynth, "channel_count", T_INTEGER, 1);
  sp_h_set_field(wavSynth, "sample_count", T_INTEGER, numSamples);
  sp_h_set_field(wavSynth, "database_id", T_STRING, "Test Data");
  sp_h_set_field(wavSynth, "utterance_id", T_STRING, "hm_f3_z2");
  sp_h_set_field(wavSynth, "recording_date", T_STRING, "29-Jun-2000");
    fprintf(wavSynth,"end_head\n");
  printf("header copied\n"); 
  fflush(stdout);
  sp_write_data(utterance,numSamples,wavSynth);
  printf("wavSynth written\n");
  fflush(stdout);
  sp_close(wavSynth);
  rmax = fabs(residual[Imax(residual,numSamples)]);
  printf("rmax = %f\n",rmax);
  fflush(stdout);
  for (i = 1; i <= numSamples; i++)
    utterance[i] = (short) (residual[i]/rmax*2047.0);
  sp_write_data(utterance,numSamples,wavResidual);
  sp_close(wavResidual); */
  return(0);
}





/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of FmtAntSynthesis.c
 -------------------------------------------------------------------------*/

#include "CommonFunctions.h"
main (int argc, char *ar[]) {
  int wnd=300,i;
  float *fa;
  const float sqrt2 = sqrt(2);
  float *x = NULL;
  sscanf (ar[1], "%d", &wnd);
  x = flinspace (0.0,1.0,wnd+3);
   finc(i,wnd+3,1) {
      x[i] = erfinv(x[i]*2.0 - 1.0) * sqrt2;
      printf ("%f\n", x[i]);
   }
  

  free (x);
}

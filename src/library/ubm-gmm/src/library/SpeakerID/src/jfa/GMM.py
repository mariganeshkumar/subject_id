import math
import numpy as np
import scipy as sp
from tables import *

# CLASS DEFINITION BEGINS 

class GMM:
  nmix,dim = 0,0
  mmeans, mvars, mwts = None, None, None

  def __init__ (self,nmix,dim):
    self.nmix = nmix
    self.dim = dim
    self.mmeans = np.zeros ((nmix, dim))
    self.mvars = np.zeros ((nmix,dim))  #assume diagonal cov
    self.mwts = np.zeros ((nmix,1))

  def GetClosestMixture (self, fvec):
    return self.GetTopCMixtures (fvec, 1)

  def GetTopCMixtures (self, fvec, cval):
    nvec, dim= np.shape (fvec)
    nmix = self.nmix
    logvars = np.reshape ([math.log(val) for val in np.reshape (self.mvars, (nmix*dim,1))],(nmix,dim))

    logwts = np.reshape ([math.log (val) for val in self.mwts], (nmix,1))
    if dim != self.dim:
      return None
    indices = np.zeros ((nvec,cval))
    for i in range (0,nvec):
      vec = fvec[i,:]
      distvec = [((-0.5 * (mahdist (vec,self.mmeans[n,:], self.mvars[n,:])) * sum (logvars[n,:]) * logwts[n]),n) 
        for n in range(0,self.nmix)]
      distvec_sorted = sorted (distvec, reverse = True)
      indices[i] = [ distvec_sorted[j][1] for j in range(0,cval) ]
    return indices
        
# END OF CLASS DEFINITION 

# UTILITY METHOD - Reads a GMM from a file

def ReadGMM (fname):
  f = open (fname, 'r')
  rl = f.readlines()
  nmix = len(rl)/2
  if nmix == 0:
    return None
  dim = len (rl[1].strip().split()) / 2
  gmm = GMM (nmix,dim)
  for i in range(0,nmix):
    gmm.mwts[i,0] = float(rl[2*i].strip())
    temparr = rl[2*i+1].strip().split()
    gmm.mmeans[i,] = temparr[0:2*dim:2]
    gmm.mvars[i,] = temparr[1:2*dim:2]
  f.close()
  return gmm    

# METHOD DEFINITION ENDS 


############# UTILITY METHOD ########################

def ConvGMMToSV (gmm):  
  return gmm.mmeans.reshape(gmm.nmix*gmm.dim,1)
################### METHOD DEFINITION ENDS #########

############# UTILITY METHOD ########################

def mahdist (fvec, mmeans, mvars):  
  dist =  (fvec -  mmeans)**2
  return sum ([dist[d]/mvars[d] for d in range(0,len (fvec))])

################### METHOD DEFINITION ENDS #########


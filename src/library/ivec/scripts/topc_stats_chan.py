import sys
import scipy.io as sio
import numpy as np
import getopt
import re
#import *
from Utility import *

ipislist = 0
optlist, args = getopt.getopt (sys.argv[1:], "o:d:u:m:lh")
(gmm_file_name, dump_file_name) = sys.argv[1:3]

fvarr = sio.loadmat(dump_file_name)
fvarr = fvarr['mat_file']
final_Nks=[]
final_stats=[]
(nmix, dim, wts, mvec, vvec) = ReadGMM (gmm_file_name)
for c in range(0,fvarr.shape[0]):
	channel_arr = np.transpose(fvarr[c,:,:])	
	gamma_Nks=[]
	non_contibuting_fvecs_indices=[]
	for i in range (0,channel_arr.shape[0]):
		meanVarNorm = -0.5 * (channel_arr[i] - mvec) ** 2 / vvec
		l2Norm = np.sum (meanVarNorm,axis=1) #- lvar
		numerator = wts*np.exp(l2Norm)
		if np.sum(numerator) == 0 :
			non_contibuting_fvecs_indices.append(i)
			continue
		gamma_Nk = numerator/np.sum(numerator)
		gamma_Nks.append(gamma_Nk)

	fVectors = channel_arr
	fVectors = np.delete(fVectors,np.asarray(non_contibuting_fvecs_indices), axis=0) 
	gamma_Nks = np.asarray(gamma_Nks)
	gamma_Nks = np.round(gamma_Nks, decimals=2)
	foStats = np.matmul( np.transpose(gamma_Nks), fVectors )

	Nks = np.sum(gamma_Nks,axis=0)
	final_Nks.append(Nks)
	aligned_foStats=[]
	for i in range(0,nmix):
		aligned_foStat = foStats[i,:] - (Nks[i] * mvec[i,])
		aligned_foStat /= np.sqrt(vvec[i,])
		aligned_foStats.append(aligned_foStat);

	aligned_foStats = np.asarray(aligned_foStats)
	final_stats.append(aligned_foStats)	

print str(nmix*fvarr.shape[0])+" "+str(dim)
#Nks = np.round(Nks, decimals = 0)
for Nks in final_Nks: 
	np.savetxt(sys.stdout, Nks, fmt="%1.6e")
for aligned_foStats in final_stats:		
	np.savetxt(sys.stdout, aligned_foStats, fmt="%1.6e")



/*-------------------------------------------------------------------------
 *  DspLibrary.c - A Library of Signal Processing Programs
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose       :	
 *  See           :	
 *
 *  Author        :	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created       : Some Time in 1996
 *  Last modified : Tue 14-Dec-2004 16:23:19 by hema
 		    Mon 17-Oct-2005 14:14:20 by deivapalan
 *  $Id           : DspLibrary.c,v 1.2 2005/03/30 07:24:27 gvijay Exp $
 *
 *  Bugs          :	
 *
 *  Change Log    :	<Date> <Author>
 *  		       <Changes>
 -------------------------------------------------------------------------*/
#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include <sys/time.h>


/* definitions for complex number arithmetic */

/* complex number type */

 typedef struct cmplx { float re,im ; }complex;

 /* temporary variable defenitions for complex arithmetic */

  float  rp_a,im_a,rp_b,im_b;

  /* add complex no's a and b and store the result in c */

# define cadd(c,a,b) rp_a = a.re; im_a = a.im; \
                     rp_b = b.re; im_b = b.im; \
                     c.re = rp_a + rp_b;       \
                     c.im = im_a + im_b

/* conjugate f complex number a stored in c */

# define conjg(c,a) rp_a = a.re; im_a = a.im; \
                    c.re = rp_a; \
                    c.im = -im_a

/* subtract b from a and store the result in c */

# define csub(c,a,b) rp_a = a.re; im_a = a.im; \
	             rp_b = b.re; im_b = b.im; \
	             c.re = rp_a - rp_b;       \
	             c.im = im_a - im_b

/* multiply a and b and store in c */

# define cmul(c,a,b) rp_a = a.re; im_a = a.im;     \
                     rp_b = b.re; im_b = b.im;     \
                     c.re = rp_a*rp_b - im_a*im_b; \
                     c.im = rp_a*im_b + im_a*rp_b

/* divide a by b and store the result in c */

# define cdiv(c,a,b) rp_a = a.re; im_a = a.im; \
                     rp_b = b.re; im_b = b.im; \
                     c.re = ( rp_a*rp_b + im_a*im_b ) \
                            /( rp_b*rp_b + im_b*im_b );\
                     c.im = ( im_a*rp_b - rp_a*im_b ) \
                            /( rp_b*rp_b + im_b*im_b )
	

	
/*---------------------------------------------------------------------------
 *  AllocFoatArray -- Allocates an array of floats
 *    Args:	Array, size of array
 *    Returns:	allocated array
 *    Bugs:	
 *---------------------------------------------------------------------------*/
float * AllocFloatArray(float *array, int npts)
{
  array = (float *) calloc (npts, sizeof(float));
  if (array == NULL) 
    {
      printf("unable to allocate Float array \n");
      exit(-1);
    }
  return(array);
}	/*  End of AllocFloatArray */	

/*-------------------------------------------------------------------------
 *  AllocIntArray -- Allocates an array of Ints
 *    Args:	Array, size of array
 *    Returns:	allocated array
 *    Bugs:	
 * -------------------------------------------------------------------------*/
int *AllocIntArray(int *array, int npts)
{
  array = (int *) calloc(npts,sizeof(int));
  if (array == NULL) 
    {
      printf("unable to allocate Int array \n");
      exit(-1);
    }
  return(array);
}	/*  End of AllocIntArray  */


/* Global definitions for fft computation  */
static   int     *iBit;                       /*Reversed bit order*/
static   float   *twiddleReal;                /*Real parts of the twiddle factors*/
static   float   *twiddleImag;                /*Imaginary parts of the twiddle factors*/

/*--------------------------------------------------------------------------
  Cstore   --Computes the twiddle factors used in FFT computation.
  The twiddle factors are stored in the global arrays IBIT, twiddleReal, twiddleImag.                                    
  Args      :n -- fft order
  Returns   :Nothing
  -------------------------------------------------------------------------*/
void Cstore(int n)
{
  int  nv2;                        /*integral part of half of n*/
  int  nm1;                        /*n-1*/
  int  ix;                         /*variable*/
  int  ix1;                        /*variable*/
  int  j,i,k;                      /*temporary local*/
  float pi2byn;                    /* 2*PI/n */
  /*Memory allocations*/
  iBit = (int *)AllocIntArray(iBit,n+1);
  twiddleReal = (float *)AllocFloatArray(twiddleReal,n/2+1);
  twiddleImag = (float *)AllocFloatArray(twiddleImag,n/2+1);
  nv2 = n/2;
  nm1 = n-1;
  /* setting bit reversed order */
  iBit[1] = 1;
  iBit[n] = n;
  ix = 0;
  for (i=2; i <= nm1; i++)
    {
      j = 0;
      k = nv2;
      ix1 = ix;     
      while (ix1 >= k) { j=j+k; ix1=ix1-k; k=k/2; };
      ix = ix + k - j;
      iBit[i] = ix + 1;
    }
  pi2byn = (float)(8.0*atan((double)1.0)/(double)n);
  /*Calcultion of the twiddle factors.Its enough if you calculate half the order as remaining are only repitition*/
  for (i=1; i <= nv2; i++)
    {
      k = i-1;
      twiddleReal[i] = (float)cos((double)(pi2byn * k));
      twiddleImag[i] = (float)sin((double)(pi2byn * k));
    }
}


/* ----------------------------------------------------------------------------
   Cfft computes the FT of a complex signal.
   inputs - 
   a - complex signal of length n
   n - FFT order
   m - m such that 2**m = n
   nsign -  -1  forward
   1  inverse
   
   outputs - 
   b - complex array of length n            
   -----------------------------------------------------------------------------*/
void Cfft(complex a[],complex b[],int m,int n,int nsign)
{
  int               nv2;/*integral part of half of n*/
  int               nm1;/*n-1*/
  int               i,j,k;/*Temporary locals*/
  int               ip,le,le1,le2,l;/**/
  complex           u,t;/**/
  
  if ((int)pow(2,m)!=n)
    {
      printf("ERROR from Cfft: 2**m != n\n");
      exit(1);
    }
  
  /*Arranging(Initialising) b in bit reversed order*/
  
  for ( i=1; i<=n; i++ ) 
  {
    b[iBit[i]] = a[i];
  }

  /*For the first stage the twiddle factors are unity*/
  for ( i=1; i<=n; i+=2 )
    {
      ip = i+1;
      t = b[ip];
      csub( b[ip],b[i],t ); 
      cadd( b[i],b[i],t );
      
    };
  
  /*For the second stage the twiddle factors are unity for i=1,5,9,...*/
  for( i=1; i<=n; i+=4 )
    {
      ip = i+2;
      t = b[ip];
      csub( b[ip],b[i],t );
      cadd( b[i],b[i],t );  
    };
  	
  /*For the second stage the twiddle factors result in conjugation for i=2,6,10,...*/
  for( i=2; i<=n; i+=4 )
    {
      ip = i+2;
      t.re = -nsign * b[ip].im;
      t.im =  nsign * b[ip].re;
      csub( b[ip],b[i],t ); 
      cadd( b[i],b[i],t );  
      
    };
  
  /*For the remaining stages we will mutiply with twiddle factors explicitly*/
  le2=n/8;  //+ optimised usage of le, and le2. 
  le=4;      
  for( l=3; l<=m; l++ )
    {
      le=le<<1;   //+ replaced log, exp function calls by shift op.
      le1 = le/2;
      for ( j=1; j<=le1; j++ )
        {
	  k = (j-1)*le2+1;
	  u.re = twiddleReal[k];
	  u.im = nsign*twiddleImag[k];
	  for ( i=j; i<=n; i+=le )
            {
	      ip = i+le1;
	      cmul(t,b[ip],u);   /*  t = b[ip]*u  */
	      csub( b[ip],b[i],t ); /* b[ip] = b[i] - t  */
	      cadd( b[i],b[i],t );  /* b[i] = b[i] + t   */
            }
        }
	le2>>=1;  //+ replaced log, exp function calls by shift op.
    }

	/*In case of inverse fft divide with size of fft*/
  if(nsign==1) for ( i=1; i<=n; i++ )
    { 
      b[i].re=b[i].re/(float)n;
      b[i].im=b[i].im/(float)n; 
    }
}
/*---------------------------------------------------------------------------  
  
Rfft computes the FT of a real signal   
inputs - 
signal - real array of length nfft
mfft  -    2**mfft = nfft
nfft  -  order of fft
nsign -  -1  forward
1  inverse
outputs -
ax - array of real part of  FT
ay - array of imaginary part of FT             

-----------------------------------------------------------------------------*/
void Rfft(float sig[],float ax[],float ay[],int mfft,int nfft,int nsign)
{
  static complex                *a;/*input to complex fft*/
  static complex                *b;/*output from complex fft*/
  complex wb,temp1,temp2;
  register int                   i,j;/*local temporary variable*/
  static int                     flag = 0;/*To indicate whether this function is visited for the first time*/

  /*float variables needed for post processing */
float pi2n=4.0*asin(1.0)/nfft;
float cos2pin=cos(pi2n);
float sin2pin=sin(pi2n);
	
if (flag == 0) 
    {
      //+ allocate nfft/2 blocks instead of nfft.
      a = (complex *) malloc((nfft/2+1)*sizeof(complex));
      b = (complex *) malloc((nfft/2+1)*sizeof(complex));
      if ((a == NULL) || (b == NULL)) 
	{
	  printf("unable to allocate complex array \n");
	  exit(-1);
	}
      flag = 1;
    }
  
  /*Converting N pt real signal into N/2 pt complex signal */
  for (i=1,j=1; i<nfft; i+=2,j++ ) 
    { 
      //+ pack N real number into N/2 complex number.
      a[j].re = sig[i];
      a[j].im = sig[i+1];
    };
    
 /* Forward FFT */   
 if( nsign == -1 )
    { 
      //+ fft order reduced by 1, and fft size reduced by half. 
      Cfft(a,b,mfft-1,nfft/2,nsign);
      /*Implement real fft with complex fft*/
    }
  
  //- overhead as a result of reducing fft size by half.
  /* Post Processing */
 wb.re=1;
 wb.im=0;
 
 for(i=2;i<=nfft/4;i++)
 {
 	temp1=wb;
	wb.re=cos2pin*temp1.re-sin2pin*temp1.im;
	wb.im=cos2pin*temp1.im+sin2pin*temp1.re;

	temp1=b[i];
 	temp2=b[nfft/2-i+2];

	b[nfft/2-i+2].re = 0.5*(temp1.re+temp2.re 
				+ nsign*wb.re*(temp1.im+temp2.im) 
				+ wb.im*(temp1.re-temp2.re));
	b[nfft/2-i+2].im = 0.5*(temp1.im-temp2.im 
				- nsign*wb.re*(temp1.re-temp2.re) 
				+ wb.im*(temp1.im+temp2.im));
	b[i].re = 0.5*(temp1.re+temp2.re 
			- nsign*wb.re*(temp1.im+temp2.im) 
			- wb.im*(temp1.re-temp2.re));
	b[i].im = 0.5*(-temp1.im+temp2.im 
			- nsign*wb.re*(temp1.re-temp2.re) 
			+ wb.im*(temp1.im+temp2.im));
 }
 
 temp1 = b[1];
 b[1].re=temp1.re+temp1.im;
 b[1].im=temp1.re-temp1.im;

 /*Inverse FFT computation */
 if(nsign == 1)
	{
	b[1].re *= 0.5;
	b[1].im *= 0.5;
	Cfft(a,b,mfft-1,nfft/2,1);
	}
  /* End of Post Processing */
  
  /*Seperating the complex output signal to real and imaginary*/
  /* Output: Re(0),Re(N/2),Re(1),Im(1), ...Re(N/2+1),Im(N/2+1)... */
  
  ax[1]=b[1].re;
  ax[nfft/2+1]=b[1].im;
  ay[1]=ay[nfft/2+1]=0;

  for(i=2;i<=nfft/2;i++)
    {
    ax[i]=b[i].re;
    ay[i]=-b[i].im;
    }
  for(i=nfft/2+2;i<=nfft;i++)
    {
    ax[i]=ax[nfft-i+2];
    ay[i]=-ay[nfft-i+2];
    }
    
}


int main(void)
{
float *signal;
float *real;
float *imag;
int i;

int FFTSize = 512;
int FFTOrder = 9;


signal = (float *)calloc(FFTSize+1, sizeof(float));
real = (float *)calloc(FFTSize+1, sizeof(float));
imag = (float *)calloc(FFTSize+1, sizeof(float));


for( i = 0; i < FFTSize; i++)
{
	//fprintf(stdout,"%f\n",sin(i+1)/2048);
	signal[i+1] = sin(i+1)/2048;
}


Cstore(FFTSize/2);
Rfft(signal, real, imag, FFTOrder, FFTSize, -1);


for( i = 1; i <= FFTSize; i++ )
{
	fprintf(stdout,"%f+j%f\n",real[i],imag[i]);
}
return 0;

}

/*
  $Log: DspLibrary.c,v $
  Revision 1.3  2005/05/06 10:54:53  gvijay
  Modified as per coding standards of donlab
  And some other improvements

*/

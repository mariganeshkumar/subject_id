function [val_p_labels, val_label, predicted_labels, test_label] = classify_using_knn(config,allData)

train_data = allData.train_data;
test_data = allData.test_data;
ind=1;
for i=1:length(train_data)
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_data=train_data{i}{j}{k};
            recording_data = mean(recording_data,3);
            concatenated_train_data(ind,:) = reshape(...
                recording_data,...
                [1,...
                size(recording_data,1)*...
                size(recording_data,2)*...
                size(recording_data,3)...
                ]...
                );
            train_label(ind)=i;
            ind = ind+1;
        end
    end
end

%todo: convert line 22 to 38 and 3 to 18 into a function

val_per = config.val_per;

ind=1;
val_ind=1;
test_ind=1;
for i=1:length(test_data)
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            if k <= length(test_data{i}{j}) * val_per
                recording_data = mean(recording_data,3);
                concatenated_val_data(val_ind,:) = reshape(...
                    recording_data,...
                    [1,...
                    size(recording_data,1)*...
                    size(recording_data,2)*...
                    size(recording_data,3)...
                    ]...
                    );
                val_label(val_ind)=i;
                val_ind = val_ind+1;
            else
                recording_data = mean(recording_data,3);
                concatenated_test_data(test_ind,:) = reshape(...
                    recording_data,...
                    [1,...
                    size(recording_data,1)*...
                    size(recording_data,2)*...
                    size(recording_data,3)...
                    ]...
                    );
                test_label(test_ind)=i;
                test_ind = test_ind+1;
            end
        end
    end
end

predicted_labels = KNN(concatenated_train_data, train_label, concatenated_test_data, 5);
val_p_labels = KNN(concatenated_train_data, train_label, concatenated_val_data, 5);
end



function [ classes ] = KNN(trainData, label, testData, K)
[modelDataCount,~] = size(trainData);
[testDataCount,~] = size(testData);
classCount = length(unique(label));
%classes = cell(testDataCount, 1);

for i = 1 :testDataCount
    testFeatureVector = testData(i,:);
    normVector = zeros(modelDataCount, 1);
    for j = 1 : modelDataCount
        normVector(j) = norm(trainData(j, :) - testFeatureVector);
    end
    
    [~, Index] = sort(normVector, 'ascend');
    classLabels = zeros(classCount, 1);
    for k = 1 : K
        classLabels(label(Index(k))) = classLabels(label(Index(k))) + 1;
    end
    [~, I] = max(classLabels);
    classes(i) = I;
end
end

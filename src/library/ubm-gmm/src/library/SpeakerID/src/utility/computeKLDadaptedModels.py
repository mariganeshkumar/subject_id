#!/usr/bin/python

# Computes KLD for adapted models (between adapted model and UBM.)
# Uses the fact (?) that the KLD can be computed as a linear combination
# of individual Gaussian components (because the model is adapted from
# the UBM.)
# Output is a log file similar to the one created earlier.
#
# Usage: computeKLDadaptedModels.py ubmModel spkModelList outFile
#

import sys
import os
import numpy as np

sys.path.append("../ivec")
import Utility as ut

if (len(sys.argv) != 4):
	print("\nUsage:\n computeKLDadaptedModels.py ubmModel spkModelList outFile\n")
	sys.exit(-1)

ubmFileName = sys.argv[1]
(nmix,dim,npa_ubmWeights,npa_ubmMeans,npa_ubmVars) = ut.ReadGMM(ubmFileName)
spkModelListName = sys.argv[2]
spkrModelList = [ ln.strip() for ln in open(spkModelListName) ]
outFileName = sys.argv[3]

# determine number of mixtures and dimension

outFile = open(outFileName,"w")
# init npa object for ubm
# start loading ubm model
ubmFile = open(ubmFileName,"r")
index = 0

# start loading model files
for x in spkrModelList:
	(temp1,temp2, temp3, npa_modelMeans, temp4) = ut.ReadGMM(x)
	# now model is read.
	# compute kld as weighted Mahalanobis dist
	npa_diffVector = pow(npa_ubmMeans - npa_modelMeans,2)
	kld = sum(npa_ubmWeights* np.sum(npa_diffVector/npa_ubmVars,1))
	kld = kld/dim
	# all the formatting below is dont to make the log files compatible
	# get 6766.gmm from '/beta/speaker-id/nist2003.../6766.gmm'
	modelNameAlone = x.split('/')[-1].split('.')[0]
	outFile.write(modelNameAlone + " = " + str(kld) + "\n")

outFile.close()
print("Done.")







	









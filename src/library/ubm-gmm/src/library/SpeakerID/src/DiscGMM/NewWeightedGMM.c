#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "InitAsdf.h"
#include "DspLibrary.h"
#include "stdlib.h"
#include "math.h"
#include "GMM.h"
#include "NewWeightedGMM.h"
#include "VQ_Modified.h"


void 
NewWeightedGMM (VECTOR_OF_F_VECTORS *vfv, 
				int numVectors, 
				VECTOR_OF_F_VECTORS *mixtureMeans, 
				VECTOR_OF_F_VECTORS *mixtureVars, 
				float *mixtureElemCnt, 
				int numMixtures, 
				int VQIter, 
				int GMMIter, 
				float probScaleFactor,
                int ditherMean, 
				float varianceNormalize, 
				int seed,
		        VECTOR_OF_F_VECTORS *negData, 
				int negNumVectors, 
				float distanceThreshold) 
  {
  int                            i,j,k;
  static VECTOR_OF_F_VECTORS     *tempMeans, *tempVars,*tempNegMeans,*tempNegVars;
  static float                   *tempMixtureElemCnt;
  float									*tempMixtureNegElemCnt;
  int                            mixtureNumber;
  int				*mixtureNegNumberCache,*mixtureNumberCache;
  int                            featLength;
  float				*alphaVector;
  float				beta,tempNk,minBeta;
  float 			*maxDistortion, *minDistortion;
  float 			tempDistortion, logLklihood;
  float 			alpha;
  int				neglectedVectorsCnt=0, numNegPtsUsed=0;  
  //int                            total;
  //int                            minIndex, maxIndex;
  //int                            minMixtureSize, maxMixtureSize;

  /* alpha is hardcoded. it, for now, does not depend
   * on anything. this was done due to desperation
   * to get things right
   * UPDATE: this hardcoded alpha is not used anywhere
   */  
  alpha=0.95;
  alpha = (1.0-alpha)/alpha;

  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < numMixtures; i++) { 
    tempMeans[i] = (F_VECTOR *) AllocFVector(featLength);
    tempVars[i] = (F_VECTOR *) AllocFVector(featLength);
  }
  tempMixtureElemCnt = (float *) AllocFloatArray(tempMixtureElemCnt, 
						 numMixtures);
  printf("temp mixtures allocated\n");
  fflush(stdout);
  
  ComputeVQ(vfv, numVectors, mixtureMeans, mixtureVars,
      mixtureElemCnt, numMixtures, varianceNormalize, 
      ditherMean, VQIter, seed);
	 if (GMMIter == 0)
	 	return;
    
  alphaVector = (float*) calloc (numMixtures,sizeof(float));
  
  /* right now, all elements in alphaVector are equal,
   * but there is a scope for make them different
   * based on the effective number of positive (and negative
   * samples?) in a mixutre
   * UPDATE: this is only init'n. later on, alphaVector elmts
   *         are different for diff mixtures based on
   *         the lower bound discussed in the paper
   * UPDATE: actually, this vector is not used at all

   */ 
  
  for (i=0;i<numMixtures;i++) alphaVector[i] = alpha;
 
  /* this is a very poor way to cache the mixture id for each sample
   */  

  mixtureNumberCache = (int *) calloc (numVectors,sizeof(int));
  mixtureNegNumberCache = (int *) calloc (negNumVectors, sizeof(int));

    for ( k = 0; k < GMMIter; k++) {
      printf(" GMM iteration number = %d\n", k);
      fflush(stdout);
       maxDistortion = (float*) calloc (numMixtures, sizeof(float));
       minDistortion = (float*) calloc (numMixtures, sizeof(float));
      for (i = 0; i < numMixtures; i++) {
        for (j = 0; j < vfv[0]->numElements; j++) {
	  tempMeans[i]->array[j] = 0;
	  tempVars[i]->array[j] = 0;
	}
	tempMixtureElemCnt[i] = 0;
      }

	/* For each positive example, find the closest mixture,
	 * accumulate statistics for that mixture
	 */
	logLklihood = 0.0; 
      for (i = 0; i < numVectors; i++) 
	  {
		mixtureNumber = DecideWhichMixture(vfv[i], mixtureMeans, 
					   mixtureVars, numMixtures, 
					   mixtureElemCnt, numVectors,
					   probScaleFactor);
		/***********************
		 * 	the next few lines tries to add a distortion threshold for data selection
 		* 	***************/
		tempDistortion =  ComputeProbability (mixtureMeans[mixtureNumber],
											  mixtureVars[mixtureNumber],
								     		  mixtureElemCnt[mixtureNumber]
											  /numVectors,
											  vfv[i],
											  probScaleFactor);
		logLklihood += tempDistortion;
		/* the next bunch of code actually is useless, that
		 * is, it is not really used 
		 */
		
		if (maxDistortion[mixtureNumber] == 0.0)
			maxDistortion[mixtureNumber] = tempDistortion;
		else if (maxDistortion[mixtureNumber] < tempDistortion)
			maxDistortion[mixtureNumber] = tempDistortion;

		if (minDistortion[mixtureNumber] == 0.0)
			minDistortion[mixtureNumber] = tempDistortion;
		else if (minDistortion[mixtureNumber] > tempDistortion)
			minDistortion[mixtureNumber] = tempDistortion;
		
		/******* end-of-distortion-threshold-measure********/
		mixtureNumberCache[i] = mixtureNumber;
		tempMixtureElemCnt[mixtureNumber] = tempMixtureElemCnt[mixtureNumber]+1;
		for (j = 0; j < featLength; j++)
		  {
	        tempMeans[mixtureNumber]->array[j] = 
	    										tempMeans[mixtureNumber]->array[j] 
												+ (vfv[i]->array[j]);
	      }
      } 
    for (i=0;i<numMixtures;i++) 
	 {
//	   printf("max dist %d is %f\ and min dist is %f\n",i,maxDistortion[i],minDistortion[i]); 
	   maxDistortion[i] = distanceThreshold;		
     }

		tempMixtureNegElemCnt = (float *) calloc (numMixtures,sizeof(float));
		neglectedVectorsCnt=0;
		numNegPtsUsed=0;
		for (i=0;i<negNumVectors;i++) {
			
			/* this is a new addition (22/12/10) to the logic.
			 * here I think it'll be beneficial to check
			 * during the 2nd or further iterations if the
			 * sample has been previously discarded. If so, I think
			 * it should be ignored
			 */
			if (k && mixtureNegNumberCache[i] == -1) continue;

			mixtureNumber = DecideWhichMixture(negData[i], mixtureMeans,
									mixtureVars, numMixtures,
									mixtureElemCnt, numVectors,
									probScaleFactor);
			tempDistortion = ComputeProbability(mixtureMeans[mixtureNumber],mixtureVars[mixtureNumber],
					 mixtureElemCnt[mixtureNumber]/numVectors, negData[i],probScaleFactor);
			if (tempDistortion >= maxDistortion[mixtureNumber]) {
				tempMixtureNegElemCnt[mixtureNumber] = tempMixtureNegElemCnt[mixtureNumber]+1;
				++numNegPtsUsed;
				mixtureNegNumberCache[i] = mixtureNumber;
			}
			else {
				mixtureNegNumberCache[i] = -1;
				neglectedVectorsCnt++;
			}
			
		}
	    printf ("At iter=%d, neglectedVectorsCnt=%d numNegPtsUsed=%d logLklihood=%f\n",k+1,neglectedVectorsCnt, numNegPtsUsed,logLklihood);	
                tempNk=0.0;
			minBeta=-1.0;

		for (i=0;i<numMixtures;i++) {	 
		  if (tempMixtureNegElemCnt[i] != 0.0) {
		  alphaVector[i] = (tempMixtureNegElemCnt[i])/(tempMixtureElemCnt[i]+tempMixtureNegElemCnt[i]);
		  alphaVector[i] = (1.0-alphaVector[i])/alphaVector[i];
		  }
		  else 
			alphaVector[i] = 0.0;
		  beta = (tempMixtureNegElemCnt[i]+numMixtures) / (tempMixtureElemCnt[i]+tempMixtureNegElemCnt[i]);
	          beta = (1.0-beta)/beta;
		  if (beta < 0.0 || tempMixtureElemCnt[i] == 0.0) {
		    beta=0.0;	
		  }			
		  
		  if (beta>0.0) 
			if (minBeta==-1.0 || minBeta > beta)
				minBeta = beta;
			
		  alphaVector[i]=beta;		
	        }
                for (i=0;i<numMixtures;i++)
			tempNk += (minBeta*tempMixtureNegElemCnt[i]);

		for (i=0;i<negNumVectors;i++) {
			mixtureNumber = mixtureNegNumberCache[i];			
             if (mixtureNumber==-1 || alphaVector[mixtureNumber] == 0.0)
				continue;
			for (j=0;j<featLength;j++)
			tempMeans[mixtureNumber]->array[j] -= (minBeta*negData[i]->array[j]);
		}
		for (j=0;j<numMixtures;j++) {
//			printf("negative %d is %f\t positive  is %f\n",j,tempMixtureNegElemCnt[j],tempMixtureElemCnt[j]);
			fflush(stdout);
		}

      for (i = 0; i < numMixtures; i++) {
	for (j = 0; j < featLength; j++){	  
	  if (tempMixtureElemCnt[i]-tempMixtureNegElemCnt[i] != 0)
	  tempMeans[i]->array[j] = 
	    tempMeans[i]->array[j]/((tempMixtureElemCnt[i])-(minBeta*tempMixtureNegElemCnt[i]));
          else
            tempMeans[i]->array[j] = mixtureMeans[i]->array[j];
	}
	mixtureElemCnt[i] = tempMixtureElemCnt[i];
      }
      
      for (i = 0; i < numVectors; i++) {
	mixtureNumber = mixtureNumberCache[i];
	for (j = 0; j < featLength; j++){
	  tempVars[mixtureNumber]->array[j] = 
	    tempVars[mixtureNumber]->array[j] + 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]);
	}
      }
      for (i = 0; i < negNumVectors; i++) {
	mixtureNumber = mixtureNegNumberCache[i];
	if (mixtureNumber == -1 || alphaVector[mixtureNumber] == 0.0)
		continue;
	for (j = 0; j < featLength; j++){
	  tempVars[mixtureNumber]->array[j] = 
	    tempVars[mixtureNumber]->array[j] -
	    (negData[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	    (negData[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * minBeta;
	}
      }
//	printf("tempNk is %f\n",tempNk);
      for (i = 0; i < numMixtures; i++) 
	  {		
	    mixtureElemCnt[i] = (tempMixtureElemCnt[i] - (minBeta*tempMixtureNegElemCnt[i])) / (numVectors - (tempNk));
	  
	    // doing the below opn because ??? outside ComputeGMM() func 
	    // does this
	    mixtureElemCnt[i] = mixtureElemCnt[i] * numVectors;
	  }

      for (i = 0; i < numMixtures; i++)
	    {
	      for (j = 0; j < featLength; j++) 
		    {
	          mixtureMeans[i]->array[j] = tempMeans[i]->array[j];
	          if (tempMixtureElemCnt[i] != 0.0 && 
				  tempVars[i]->array[j]>0.0    && 
				  alphaVector[i]!=0.0)
	            mixtureVars[i]->array[j] = tempVars[i]->array[j]
	                                     /(tempMixtureElemCnt[i]-minBeta
										 * tempMixtureNegElemCnt[i]);	
	  	      else if (tempMixtureElemCnt[i] != 0.0 && 
					   tempVars[i]->array[j]>0.0    && 
					   alphaVector[i]==0.0)
	    	    mixtureVars[i]->array[j] = tempVars[i]->array[j]
	                                     /(tempMixtureElemCnt[i]);	
			}
        }
    }
printf ("minBeta is %f\n",minBeta);
free(alphaVector);
free(mixtureNegNumberCache);
free(mixtureNumberCache);
}

/*
    This file computes T matrix using i vectors and A matrix

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"

int
main(int argc, char *argv[]) 
{

    if (argc != 7)
    {
        printf( "Usage: %s aMat cMat tMat R M D\n", argv[0] );
        exit(0);
    }

   char *tmatname = NULL,
         *amatname = NULL,
         *cmatname = NULL;

    gsl_matrix *cmat, *amat, *tempmat, *a_inv;
    gsl_matrix_view submat_a, submat_c;
    int  M, D, R, i, ii, mr, md;

    amatname = argv[1];
    cmatname = argv[2];
    tmatname = argv[3];
    R=atoi(argv[4]);
    M=atoi(argv[5]);
    D=atoi(argv[6]);

    amat = read_matrix (amatname,M*R,R);
    cmat = read_matrix (cmatname,M*D,R);
    tempmat = gsl_matrix_alloc (D,R);

    for (i = 0; i < M; i++) {
       submat_a = gsl_matrix_submatrix (amat, i*R, 0, R, R);
       submat_c = gsl_matrix_submatrix (cmat, i*D, 0, D, R);         
       a_inv = invertMatrix (&submat_a.matrix);
       gsl_matrix_set_zero (tempmat);
	gsl_blas_dgemm (CblasNoTrans,CblasNoTrans,
                       1.0, &submat_c.matrix, a_inv,
                       0.0, tempmat);
       gsl_matrix_memcpy (&submat_c.matrix, tempmat);
       gsl_matrix_free (a_inv);

    }
    write_matrix (tmatname, cmat);
    gsl_matrix_free (cmat);
    gsl_matrix_free (amat);
    gsl_matrix_free (tempmat);
}

/**************************************************************************
 *  $Id$
 *  File:	HistogramUnivariate
 *
 *  Purpose:	Computes the histogram given a range of values
 *
 *  Author:	Hema A Murthy
 *
 *  Date:	Thu Apr  3 16:30:20 IST 2008
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"
#include "stdlib.h"
#include "fe/DspLibrary.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"

void GDSmoothHistArray(float *histArray, float *histArrayModified, int numBins, int mfft, int nfft, 
		       float *smthHistArray, float scaleFactor, float gamma) {
  int            nfBy2, winlen;
  float          *sigCopy = NULL, *sigCep=NULL, *ay=NULL, 
                 *phase=NULL, *ax=NULL, *amag=NULL;
  float          rMax, rMin;
  int            k;

 
  sigCopy = (float*) AllocFloatArray(sigCopy, nfft+1);
  sigCep = (float *) AllocFloatArray(sigCep, nfft+1);
  ay = (float *) AllocFloatArray(ay, nfft+1);
  ax = (float *) AllocFloatArray(ax, nfft+1);
  phase = (float *) AllocFloatArray(phase, nfft+1);
  amag = (float *) AllocFloatArray(amag, nfft+1);
  sigCopy [1] = expf(gamma*logf(histArray[0]+1));
  histArrayModified[0] = sigCopy[1];
  for (k = 2; k <= numBins; k++) {
    sigCopy[k] = expf(gamma*logf(histArray[k-1]+1));
    sigCopy[nfft-k+2] = sigCopy[k];
    histArrayModified[k-1] = sigCopy[k];
  }
  rMax = sigCopy[Imax(sigCopy,  numBins)];
  for (k = 1; k <= nfft; k++)
    sigCopy[k] = sigCopy[k]/rMax;
  rMin = sigCopy[Imin(sigCopy, numBins)];
  for (k = numBins + 1; k <= nfft-numBins + 1; k++)
    sigCopy[k] = rMin;
  winlen = (int) ceil((float)numBins/scaleFactor);
  Rfft(sigCopy, sigCep,  ay,  mfft,  nfft, 1);
  for (k = 1; k <= winlen; k++){
    sigCopy[k] = sigCep[k]*HanW(k, winlen);
  }
  for (k = winlen+1; k <= nfft; k++)
    sigCopy[k] = 0;
  
  Rfft(sigCopy, ax,  ay,  mfft,  nfft,  -1);
  SpectrumReal(nfft,  ax,  ay,  amag,  phase);
  
  nfBy2 = nfft/2;

  for (k = 1; k < numBins-1; k++){
    smthHistArray[k-1] = phase[k] - phase[k+1];  
  }
  smthHistArray[numBins-1] = smthHistArray[numBins-2];
  free (sigCopy);
  free (sigCep);
  free (ax);
  free (ay);
  free (phase);
  free (amag);

}


int main (int argc, char *argv[])
{
  float            *waveform;
  FILE             *ctrlFile=NULL, *fpout=NULL, *fpoutPeakFile=NULL;
  int              numBins, deltaBin, numSegments, numSamplesPerSegment, normalizeSegHist;
  int              *peakArray=NULL, *peakFilterArray=NULL;
  int              firstPeak, flag;
  float            *histogram=NULL, *histogramModified=NULL, *aveHistogram=NULL, *smthHistogram=NULL;
  float            minFreq, maxFreq, gamma, binWidth, threshold, winScaleFactor;;
  float            histMax, rmax, tonic;
  int              fftOrder, fftSize;
  long             numSamples;
  int              i, j, k;
  static ASDF      *asdf;
  if (argc != 12) {
    printf("Usage: HistogramUnivariate ControlFile InputFile OutHistogram numBins numSegments normalizeSegHist(0/1) minFreq maxFreq deltaBin(odd integer) threshold outputPeakFile\n");
    exit(-1);
  }
  ctrlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeASDF(asdf);
  InitializeStandardFrontEnd(asdf,ctrlFile);
  winScaleFactor = (float) GetFAttribute (asdf, "winScaleFactor");
  gamma = (float) GetFAttribute (asdf, "gamma");
  fftOrder = (int) GetIAttribute (asdf, "fftOrder");
  fftSize = (int) GetIAttribute (asdf, "fftSize");
  //fpin = fopen(argv[2], "r");
  fpout = fopen(argv[3],"w");
  sscanf(argv[4], "%d", &numBins);
  sscanf(argv[5], "%d", &numSegments);
  sscanf(argv[6], "%d", &normalizeSegHist);
  sscanf(argv[7], "%f", &minFreq);
  sscanf(argv[8], "%f", &maxFreq);
  sscanf(argv[9], "%d", &deltaBin);
  sscanf(argv[10], "%f", &threshold);
  fpoutPeakFile = fopen(argv[11], "w");
  histogram = (float *) AllocFloatArray(histogram, numBins+1);
  aveHistogram = (float *) AllocFloatArray(aveHistogram, numBins+1);
  histogramModified = (float *) AllocFloatArray(histogramModified,numBins+1);
  smthHistogram = (float *) AllocFloatArray(smthHistogram, numBins+1);
  peakArray = (int *) AllocIntArray(peakArray, numBins+1);
  peakFilterArray = (int *) AllocIntArray(peakFilterArray, numBins+1);
  //  while (fgets (line, 500, fpin) != NULL) {
  //sscanf(line, "%s", name);
    //    waveFile = sp_open (name, "r");
    waveform = (float *) ReadFloatText(argv[2], &numSamples);
    /*    if (ratio == 1) {
      for (i = 0; i < numSamples-1; i++) {
	if (waveform[i] != 0.0)
	  value = 1200 * logf (waveform[i+1]/waveform[i]);
	waveform[i]= value;
	fprintf(fpoutPeakFile, "%f\n", waveform[i]);
      }
      } */
    numSamplesPerSegment = (int) ((floorf) (numSamples/numSegments));
    k = 0;
    for (i = 0; i < numBins; i++)
      aveHistogram[i] = 0;
    for (i = 0; i < numSegments; i++) {
      ComputeHistogram (&waveform[k], numSamplesPerSegment, minFreq, maxFreq, numBins, histogram);
      if (normalizeSegHist == 1)
	histMax = fabs(histogram[Imax0(histogram, numBins)]);
      else
	histMax = 1;
      k = (i+1)*numSamplesPerSegment;
      for (j = 0; j < numBins; j++)
	aveHistogram[j] = aveHistogram[j] + histogram[j]/histMax;
    }
    for (i = 0; i < numBins; i++)
	aveHistogram[i] = aveHistogram[i]/numSegments;
    printf("num_samples in main = %ld num_samples per segment = %ld \n",numSamples, numSamplesPerSegment);
    //    totalNumSamples = totalNumSamples+numSamples;
  fflush(stdout);
  free(waveform);
  // }
  binWidth = (maxFreq - minFreq)/numBins;
  
  GDSmoothHistArray(aveHistogram, histogramModified, numBins, fftOrder, fftSize, smthHistogram, winScaleFactor, gamma);
  FindPeaks(&smthHistogram[-1], peakArray, numBins);
  i = 0;
  rmax = fabs(aveHistogram[Imax0(aveHistogram, numBins)]);
  flag = 0;
  while (!flag) {
    while ((peakArray[i+1] == 0) && (i < numBins) && (smthHistogram[i] < 0)) 
      i++; 
    if (i >= numBins) 
      flag = 1;
    else if (aveHistogram[i] > threshold*rmax)
      flag = 1;
    else
      i++;
  }
  firstPeak = i;
  for (i = 1; i <= numBins; i++)
    if ((peakArray[i] == 1) && (smthHistogram[i] < 0.0))
      peakArray[i] = -peakArray[i];
  for ( i = 1; i <=numBins; i++)
    peakFilterArray[i] = peakArray[i];
  for (i = 1; i <= numBins; i++)
    if (peakArray[i] == 1) 
      for (j = i-deltaBin; j <= i+deltaBin; j++)
	if ((j >= 1) && (j <= numBins))
	  peakFilterArray[j] = 1;
  for (i = 1; i <= numBins; i++)
    if (peakFilterArray[i] == 1)
      fprintf (fpoutPeakFile, "%f %d %f %f\n", minFreq + (i)*binWidth, peakArray[i],
	       peakFilterArray[i]*smthHistogram[i-1], aveHistogram[i-1]);
    else
      fprintf (fpoutPeakFile, "%f %d %f\n", minFreq + (i)*binWidth, peakArray[i],
	       0.0);
  printf(" First Peak occurs at = %d %f\n" , firstPeak, minFreq+(firstPeak)*binWidth);
  for (i = 0; i < numBins; i++)
    fprintf (fpout, "%e %e %d %e %e %e %e \n", minFreq+(i)*binWidth, 1200*(log (minFreq+(i)*binWidth) - log (minFreq+(firstPeak)*binWidth)), 
	     i, aveHistogram[i], log(histogramModified[i]), histogramModified[i], smthHistogram[i] );

  free (histogram);
  free (smthHistogram);
  free (aveHistogram);
  free (histogramModified);
  free (peakArray);
  free (peakFilterArray);
  fclose(fpout);
  fclose(fpoutPeakFile);
  printf("file closed\n");
  return(0);
}















/**************************************************************************
 * $Log$
 *
 *                        End of ReadSpherePcm.c
 **************************************************************************/

function [N fx] = ReadFoStat (fname)
    f = fopen (fname);
    M = fscanf (f, '%d', 1);
    D = fscanf (f, '%d', 1);
    N = fscanf (f, '%d', M);
    fx = fscanf (f, '%e', M*D);
    fclose (f);

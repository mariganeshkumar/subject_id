amatname=$1
iplist=$2
lxlist=$3
M=$4
D=$5
R=$6
remote=$7
wrkdir=$8
username=`whoami`

corelist=
remotelist=
wrkdirlist=

itemno=0
for item in `echo $remote | sed 's/,/\ /g'`; do
   i1=`echo $item | cut -d '/' -f1`
   i2=`echo $item | cut -d '/' -f2`
   if [ "$i1" == ":" ]; then
     corelist[$itemno]=`parallel --number-of-cores`
   elif [ "$i1" == ":" -a "$i2" != ":" ]; then
     corelist[$itemno]=$i2
   else
     corelist[$itemno]=$i2
   fi
   remotelist[$itemno]=$i1
   ((itemno++))
done
((numremotes=itemno-1))

i=0
j=0
for item in `echo $wrkdir | sed 's/,/\ /g'`; do
  wrkdirlist[$i]=$item
  if [ ${remotelist[$i]} == ":" ]; then
    remotename=localhost
  else 
    remotename=${remotelist[$i]}
  fi
  catlistname_iplist=concatlist.$remotename.iplist
  catlistname_lxlist=concatlist.$remotename.lxlist
  rm -f $catlistname_iplist $catlistname_lxlist
# creating a concatenated list 
  for core_no in `seq 1 ${corelist[$i]}`; do
    cat $iplist.$j >> $catlistname_iplist   
    cat $lxlist.$j >> $catlistname_lxlist
    ((j++))
  done
# transferring the lists
  echo "scp $catlistname_iplist $catlistname_lxlist $username@${remotelist[$i]}:$item/"
  if [ ${remotelist[$i]} != ":" ]; then
    scp $catlistname_iplist $username@${remotelist[$i]}:$item/
    scp $catlistname_lxlist $username@${remotelist[$i]}:$item/
  fi
  echo $amatname |  parallel --wd $item -S 1/${remotelist[$i]} 'matlab -nojvm -nodesktop -nosplash -r "computeA ('\'{}.$i\', \'$catlistname_iplist\', \'$catlistname_lxlist\', $M, $D, $R, 1')"' &
  echo $amatname |  parallel --dry-run --wd $item -S 1/${remotelist[$i]} 'matlab -nojvm -nodesktop -nosplash -r "computeA ('\'{}.$i\', \'$catlistname_iplist\', \'$catlistname_lxlist\', $M, $D, $R, 1')"' &
  ((i++))
done
wait

amatdir=`dirname $amatname`
rm -f amatlist
for i in `seq 0 $numremotes`; do
    if [ ${remotelist[$i]} != ":" ]; then
        scp -r $username@${remotelist[$i]}:${wrkdirlist[$i]}/$amatname.$i.* $amatdir/
        echo "scp -r $username@${remotelist[$i]}:${wrkdirlist[$i]}/$amatname.$i.* $amatdir/"
        echo $amatname.$i >> amatlist
    fi
done    
echo $M | parallel -j 1 matlab -nojvm -nosplash -nodesktop -r '"computeA_red ('\'amatlist\', \'$amatname\', 1, {}, $R')"' &
echo $M | parallel --dry-run -j 1 matlab -nojvm -nosplash -nodesktop -r '"computeA_red ('\'amatlist\', \'$amatname\', 1, {},$R')"'
wait

# clean up
rm -f amatlist \
      concatlist.*.iplist \
      concatlist.*.lxlist

#!/bin/sh
# This script intended to build a ubm from a very large
# collection of feature vectors. The intention is to
# extract features sticking to the necessary style for
# each database being considered, and use the Map-Reduce
# algorithm available under src/parallel to build the
# UBM

# Ideally, the script should run across several machines.
# It should be easy to add/delete machines, fiddle with
# number of cores that would be used on each one. The 
# launcher of this script will be the storehouse of all
# data.

# Things to check before running
#   1)  

LAUNCHER=$HOSTNAME
LAUNCHERALIAS=:

PROCESSID=`date +%d%m%Y%H%M%S`

# assume feature vectors have been extracted at this stage
# the two files below will be created everytime the script
# is launched
METALIST=                                       # list of indi lists
MOTHERLIST=                                     # concat of lists in METALIST

# CHECK on every run if this is what you want to conside while
# building a UBM

# file names containing list of feat dumps with  absolute paths                                                          
LIST=" "                                        
                                             
# the script will check if each entry in LIST actually exists
for listname in $LIST; do
    if [ ! -e $listname ];
        echo "File $listname does not exist. Please check and re-run"
    fi
done    

# since all files mentioned in the list, create both METALIST and MOTHERLIST
rm $MOTHERLIST $METALIST
for listname in $LIST; do
    cat $listname >> $MOTHERLIST
    echo $listname >> $METALIST
done

# CHECK the following entry. It is a list of systems that will be used for 
# UBM estimation. Includes Launcher by default. It is important that the
# ssh connection to and from these systems is password-free 
SYSTEMS="$LAUNCHERALIAS,sahyadri,vindhya"

# Initialize the GMM
Initialize () {
rm -f temp.init.$PROCESSID
for listname in $METALIST; do
# take 10% of the list
  if [ ! -e $listname ]; then
    echo "Warning : List $listname does not exist"
    continue;
  fi
  nl=`wc -l $listname | cut -d ' ' -f1`
  if [ $nl -eq 0 -o "$nl" == "" ]; then
      continue;
  fi
  top10=`echo $nl/10 | bc`  
  if [ $top10 -le 1 ]; then
    continue;
  fi
  head -$top10 $listname >> temp.init.$PROCESSID
done  
}

if [ $SKIPINIT -eq 0 ];
    Initialize
fi

# Run VQ Iterations
numVQIter=10
RunVQIter () {
  i=$1
  # to run the same binary on several, make sure the
  # the executable is visible to the environment. The
  # basefile should be in the right path. The input
  # vfv dump needs to be transferred. The output needs
  # to be transferred back
  parallel -a $FEATLISTAPP --colsep ' ' \
  --sshlogin $SYSTEMS \
  $VQMapper -i {1} -d -k $CODEBOOKSIZE -b vfinal -o vq$i.{1/} -e
}

if [ $SKIPVQITER -eq 0 -a $STARTVQITERFROM -gt 0 ];  then
    for i in `seq $$numVQIter`; do
      RunVQIter $i
    done  
fi

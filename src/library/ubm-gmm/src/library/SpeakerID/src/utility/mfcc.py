import sys
import scipy.fftpack as sf
import scipy as sp
import numpy as np
import math
import mfcc_lib as mfcc

required_params = ['windowSize', 'frameAdvanceSamples', 'numFilters', 
                   'fftSize', 'numCepstrum']

int_params = ['windowSize', 'frameAdvanceSamples', 'numFilters',
              'fftSize', 'numCepstrum']
              
params_dict = {}


if len(sys.argv) < 4:
    print("Usage: python minGd.py samplefile ctrlfile featurefile")
    quit(0)

(ip_name, ctrl_name, op_name) = sys.argv[1:4]
samples = [ int(ln.strip()) for ln in open(ip_name) ]
with open(ctrl_name) as f:
    for ln in f:
        if ln.startswith('#') or ln.strip() == "": 
            continue
        (param_name, param_type, param_value) = ln.strip().split()
        params_dict[param_name] = param_value
            
(windowSize, frameAdvanceSamples, numFilters, fftSize, numCepstrum) = map(
    lambda x: int(params_dict[x]), int_params)
nsamples = len(samples)
nframes = math.ceil(nsamples/float(frameAdvanceSamples))
all_frames = np.zeros((windowSize, nframes))
wnd = sp.hamming(windowSize)
start = 0
for i in xrange(0, int(nframes)):
  ll = min(start+windowSize, nsamples) - start
  all_frames[0:ll,i] = samples[start:start+ll] * wnd[0:ll]
  start += frameAdvanceSamples
egy = np.sum(pow(all_frames,2), 0)
avg_egy = np.mean(egy)
threshold = 0.0001 * avg_egy
voiced_frames = [ i for i in xrange(0,int(nframes)) if egy[i] >= threshold ]
all_frames = all_frames[:,voiced_frames]
nframes = np.shape(all_frames)[1]

magspec = sf.fft(all_frames, n = fftSize, axis = 0)
np.save('1830.fft.npy', magspec)
magspec = np.abs(magspec)
vfv_mfcc = np.log(np.dot(mfcc.mel_filters(numFilters), magspec))
np.save('1830.mfbe.npy', vfv_mfcc)
vfv_mfcc = sf.dct(vfv_mfcc)
vfv_lfcc = np.log(np.dot(mfcc.linear_filters(numFilters), magspec))
np.save('1830.lfbe.npy', vfv_lfcc)
vfv_lfcc = sf.dct(vfv_lfcc)

np.save('1830.mfcc.npy', vfv_mfcc)
np.save('1830.lfcc.npy', vfv_lfcc)

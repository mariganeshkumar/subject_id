/*
    This file extracts different kinds of feature vectors from a speech file

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Hema A Murthy <hema@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "CommonFunctions.h"
#define UCHAR unsigned char
#define UCAHRP unsigned char*

main (int argc, char *argv[])
{

  char *controlFileName, *featureName, *ergSpidModelFileName, *wavName;
  char *outputFileName, outFile2;
  FILE *controlFile, *ergSpidModelFile;
  FILE *outputFile, *outFile1=NULL;
  VECTOR_OF_F_VECTORS *newVfv;
  VECTOR_OF_F_VECTORS *deltaVfv = NULL, *deltaDeltaVfv = NULL;
  VECTOR_OF_F_VECTORS *concatDeltaVfv, *concatAcclVfv;
  ASDF *asdf;
  F_VECTOR *fvect;
  unsigned long numVectors;
  unsigned int featLength, i, j, k;
  unsigned int velocity = 0, acceleration = 0;
  unsigned int deltaDifference, deltaDeltaDifference;
  int numGaussiansErg = 3, numSpeechVectors;
  char channel;
  float probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  float thresholdScale;

  if (argc < 7) {
      printf ("Usage : %s controlFile wavName featureName outputFile "
              "thresholdScale channel\n", argv[0]);
      exit (0);
  }

  controlFileName = argv[1];
  wavName = argv[2];
  featureName = argv[3];
  outputFileName = argv[4];
  sscanf (argv[5], "%f", &thresholdScale);
  if (set_select_channel(argv[6][0]) != 0) return 1;

  controlFile = fopen (controlFileName, "r");
  asdf = (ASDF *) calloc (1, sizeof (ASDF));
  InitializeStandardFrontEnd (asdf, controlFile);
  Cstore (asdf->fftSize);

  //printf ("Threshold is %f\n", thresholdScale);
  newVfv = ExtractFeatureVectors (asdf, wavName,
				     featureName, &numVectors, thresholdScale);
  //printf("numVectors = %ld\n",numVectors); 
  WriteVfvToFile (newVfv, numVectors, outputFileName);      
  FreeVfv (newVfv, numVectors);
}

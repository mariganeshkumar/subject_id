#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "string.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program computes the FFT Magnitude Spectrum
*	Input: dataFile, FFTOrder, FFTSize, 
*	Output :outPutFile

*******************************************************************************/       
void Usage() {
           printf("Usage : ComputeDistance fileLeft fileMid fileRight OutputFile startCoefficient endCoefficient Euclidean/KLD (0/1)");
}
/*****************************************************************************/

main (int argc, char *argv[])
{ 
  FILE            *fleft, *fmid, *fright, *fout, *fkld;
  float           *specLeft=NULL, *specMid=NULL, *specRight=NULL;
  float           euclidDistLeft, euclidDistRight,distLeft, distRight, tempValue,
                  aveEnergy, aveEnergyInv, aveEnergyLeft, aveEnergyRight, klDivLeft, klDivRight, normaliseDist;
  int             i, k, startCoefficient, endCoefficient, numPts, kld;
  char            line[500],  kldName[500];

 /******************************************************************************/
       if (argc != 8) {
         Usage();
         exit(-1);
       }
       printf("%s %s %s %s %s\n", argv[1], argv[2], argv[3], argv[4], argv[5]);
       fleft = fopen(argv[1], "r");
       fmid = fopen(argv[2], "r");
       fright = fopen(argv[3], "r");
       fout = fopen(argv[4], "w");
       kldName[0] = '\0';
       strcat(kldName, argv[4]);
       strcat(kldName,".kld");
       fkld = fopen(kldName, "w");
       sscanf(argv[5], "%d", &startCoefficient);
       sscanf(argv[6], "%d", &endCoefficient);
       sscanf(argv[7], "%d",&kld);
       numPts = endCoefficient - startCoefficient + 1;
       specLeft = (float *) AllocFloatArray (specLeft, numPts);
       specMid = (float *) AllocFloatArray (specMid, numPts);
       specRight = (float *) AllocFloatArray (specRight, numPts);
       i = 0;
        while ((fgets (line, 200, fleft) != NULL) && (i < startCoefficient)){ 
         sscanf(line, "%f", &tempValue);
         i++;
       }
	i = 0;
        while ((fgets (line, 200, fmid) != NULL) && (i < startCoefficient)){ 
         sscanf(line, "%f", &tempValue);
         i++;
       }
       i = 0;
        while ((fgets (line, 200, fright) != NULL) && (i < startCoefficient)){ 
         sscanf(line, "%f",  &tempValue);
         i++;
       }
	k = i;
	while ((fgets (line, 200, fleft) != NULL) && ((i-startCoefficient) < numPts)){ 
         sscanf(line, "%f", &specLeft[i-startCoefficient]);
         i++;
       }
	i = k;
       while ((fgets (line, 200, fmid) != NULL) && ((i-startCoefficient) < numPts)){ 
         sscanf(line, "%f", &specMid[i-startCoefficient]);
         i++;
       }
       i = k;
       while ((fgets (line, 200, fright) != NULL) && ((i-startCoefficient) < numPts)){ 
         sscanf(line, "%f", &specRight[i-startCoefficient]);
         i++;
       }
       aveEnergyLeft = 0;
       aveEnergyRight = 0;
       aveEnergy = 0;
       for (i = 0; i < numPts; i++) {
	 aveEnergyLeft = aveEnergyLeft + specLeft[i]*specLeft[i];
         aveEnergy = aveEnergy + specMid[i]*specMid[i];
	 aveEnergyRight = aveEnergyRight + specRight[i]*specRight[i];
       }
       normaliseDist = (aveEnergyLeft+aveEnergyRight+aveEnergy)/3.0;
       // aveEnergy = 20*log10(aveEnergy);
       aveEnergyInv = 1.0/normaliseDist;
       if (kld == 0) {
	 euclidDistLeft = 0;
	 euclidDistRight = 0;
	 for (i = 0; i < numPts; i++) {
	 distLeft = (specLeft[i]-specMid[i])*(specLeft[i]-specMid[i])/normaliseDist;
	 distRight = (specRight[i]-specMid[i])*(specRight[i]-specMid[i])/(normaliseDist);
	   euclidDistLeft = euclidDistLeft + distLeft;
	   euclidDistRight = euclidDistRight + distRight;
	   if ((distLeft !=0)&& (distRight !=0))
	     fprintf(fout, "%e %e %e\n", distLeft, distRight,
		     fabs(distRight - distLeft)/fabs(distLeft));
	   else
	     fprintf(fout, "%e %e %e\n", distLeft, distRight, (distLeft+distRight)/2);
	 } 
	 if ((euclidDistLeft != 0) && (euclidDistRight != 0))
	   fprintf (fkld, "%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*euclidDistLeft, aveEnergyInv*euclidDistRight,
		    aveEnergyInv*fabs(euclidDistRight - euclidDistLeft)/fabs(euclidDistLeft), euclidDistLeft, euclidDistRight,
		    fabs(euclidDistRight - euclidDistLeft)/fabs(euclidDistLeft));
	 else
	   fprintf (fkld, "%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*euclidDistLeft, aveEnergyInv*euclidDistRight, 
		    aveEnergyInv*(euclidDistLeft+euclidDistRight)/2, euclidDistLeft, euclidDistRight, (euclidDistLeft+euclidDistRight)/2);
       } else 
	 {
	   klDivLeft = 0;
	   klDivRight = 0;
	   for (i = 0; i < numPts; i++) {
	     distLeft = (specLeft[i]*(logf(specLeft[i])-logf(specMid[i])) +
			 specMid[i]*(logf(specMid[i]) - logf(specLeft[i])))/2.0;
	     klDivLeft = klDivLeft + distLeft;
	     distRight = (specRight[i]*(logf(specRight[i])-logf(specMid[i])) +
			  specMid[i]*(logf(specMid[i]) - logf(specRight[i])))/2.0;
	     klDivRight = klDivRight + distRight;
	     if ((distLeft !=0)&& (distRight !=0))
	       fprintf(fout, "%e %e %e\n",distLeft, distRight,fabs(distRight-distLeft)/fabs(distLeft));
	     else
	       fprintf(fout, "%e %e %e\n",distLeft, distRight,(distLeft+distRight)/2);
	   } 
	   if ((klDivLeft != 0) && (klDivRight != 0)) {
	     fprintf (fkld, "%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*klDivLeft, aveEnergyInv*klDivRight, aveEnergyInv*fabs(fabs(klDivRight)-fabs(klDivLeft))/fabs(klDivLeft), klDivLeft,klDivRight, fabs(fabs(klDivRight)-fabs(klDivLeft))/fabs(klDivLeft));
	     printf ("%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*klDivLeft, aveEnergyInv*klDivRight, aveEnergyInv*fabs(fabs(klDivRight)-fabs(klDivLeft))/fabs(klDivLeft), klDivLeft,klDivRight, fabs(fabs(klDivRight)-fabs(klDivLeft))/fabs(klDivLeft));
	   }
	   else {
	     fprintf (fkld, "%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*klDivLeft,aveEnergyInv*klDivRight, aveEnergyInv*(fabs(klDivLeft)+fabs(klDivRight))/2, klDivLeft,klDivRight, (fabs(klDivLeft)+fabs(klDivRight))/2);
	     printf ("%e %e %e %e %e %e %e %e\n", log10(aveEnergy), aveEnergy, aveEnergyInv*klDivLeft,aveEnergyInv*klDivRight, aveEnergyInv*(fabs(klDivLeft)+fabs(klDivRight))/2, klDivLeft,klDivRight, (fabs(klDivLeft)+fabs(klDivRight))/2);
	   }
	 }
       free(specMid);
       free(specLeft);
       free(specRight);
       fclose(fleft);
       fclose(fright);
       fclose(fmid);
       fclose(fout); 
       fclose(fkld);
}




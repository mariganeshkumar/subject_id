#include "CommonFunctions.h"


main (int argc, char *argv[])
{

  char *controlFileName, *featureName, *vadFileName, *wavName;
  char *outputFileName;
  FILE *controlFile, *vadFile;
  FILE *outputFile;
  VECTOR_OF_F_VECTORS *newVfv;
  VECTOR_OF_F_VECTORS *deltaVfv = NULL, *deltaDeltaVfv = NULL;
  VECTOR_OF_F_VECTORS *concatDeltaVfv, *concatAcclVfv;
  float *ergWts;
  VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
  ASDF *asdf;
  unsigned long numVectors;
  unsigned int featLength, i, j, k;
  float probScaleFactor, ditherMean, varianceNormalize, varianceFloor;

  if (argc != 6)
    {
      printf
	("Usage : %s controlFile wavName featureName outputFile vadFile\n",
	 argv[0]);
      exit (0);
    }

  controlFileName = argv[1];
  wavName = argv[2];
  featureName = argv[3];
  outputFileName = argv[4];
  vadFileName = argv[5];

  controlFile = fopen (controlFileName, "r");
  asdf = (ASDF *) calloc (1, sizeof (ASDF));
  InitializeStandardFrontEnd (asdf, controlFile);
  Cstore (asdf->fftSize);

  newVfv = ExtractFeatureVectorsFromASR (asdf, wavName,
				     featureName, &numVectors,
				     vadFileName);

  outputFile = fopen (outputFileName, "w");
  fprintf (outputFile, "%d %d\n", newVfv[0]->numElements, numVectors);
  for (i = 0; i < numVectors; i++)
    {
      for (j = 0; j < newVfv[i]->numElements; j++)
	fprintf (outputFile, " %f", newVfv[i]->array[j]);
      fprintf (outputFile, "\n");
    }
  fclose (outputFile);
  FreeVfv (newVfv, numVectors);
}

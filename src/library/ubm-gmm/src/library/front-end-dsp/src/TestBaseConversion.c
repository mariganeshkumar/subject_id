#include "stdio.h"
#include "math.h"
main()
{
  float frequency, tonic;
  int numOctaves;
  printf ("Input tonic and Frequency Range");
  scanf("%f %f", &tonic, &frequency);
  numOctaves = ceilf(log(frequency/tonic)/log(2.0));
  printf ("number of Octaves %d in Frequency Range %f and tonic %f\n", numOctaves, frequency, tonic);
}

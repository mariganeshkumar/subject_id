function sigma2 = em_pca(tmatprefix,Xlist, ubmname,  M,D, no_dims,max_iter,  doInit, startIter, sigma2Init)
%EMPCA Run an EM-based implementation of (probabilistic) PCA
%
%   [mappedX, mapping] = em_pca(X, no_dims)
%
% Performs probabilistic PCA on dataset X in order to reduce its
% dimensionality to no_dims. The dimensionality reduction is performed by
% means of an EM algorithm. The resulting low-dimensional counterpart of X
% is returned in mappedX. Information on the applied mapping (allowing for,
% e.g., out-of-sample extension) is returned in mapping.
%
%

% This file is part of the Matlab Toolbox for Dimensionality Reduction v0.7.2b.
% The toolbox can be obtained from http://homepage.tudelft.nl/19j49
% You are free to use, change, or redistribute this code in any way you
% want for non-commercial purposes. However, it is appreciated if you 
% maintain the name of the original author.
%
% (C) Laurens van der Maaten, 2010
% University California, San Diego / Delft University of Technology


    path (path,'../ivec')
    tmatprefix
    Xlist
    ubmname
    M
    D
    no_dims
    max_iter
    % Initialize some variables
    iplist = readlistascell (Xlist);
    n = length (iplist);
    %Ez = zeros(no_dims, 1);                 % expectation of latent vars
    %Ezz = zeros(no_dims, no_dims, n);       % expectation of cov(z)
    mapping = struct;

    % Randomly initialize W and sigma
    if doInit == 1
        [ws ms vs] = ReadGMM (ubmname, M, D);
        clear ws ms
        vs = reshape (vs, M*D,1);
        vs(find(vs<1E-5)) = 1E-5;
        vs = sqrt(vs);
        W = rand(M*D, no_dims) * 2;               % factor loadings (MD x R)
        for r = 1:no_dims
            W (:,r) = W(:,r) ./ vs;
        end
        sigma2 = rand(1) ^ 2;                   % variance ^ 2    (1 x 1)
        save(strcat (tmatprefix,int2str(0)),'-mat','W');
        message = 'finished initialization'
    else
        load(strcat (tmatprefix,int2str(0)),'-mat');
    end
    if startIter > 1
        sigma2 = sigma2Init;
        load(strcat (tmatprefix,int2str(startIter-1)),'-mat');
    end
    D = M * D;

    % RANDOM FACT:
    % The covariance of the Gaussian is: C = W * W' + sigma2 * eye(D);
    %
    
    % Perform EM iterations
    inW = W' * W;                           % (R x R)

    for iter = startIter:max_iter
            
        % Perform E-step
        invM = inv(inW + sigma2 * eye(no_dims));         % (R x R)
%        for i=1:n
%            Ez(:,i)    = invM * W' * X(:,i);         
%            Ezz(:,:,i) = sigma2 * invM + Ez(:,i) * Ez(:,i)';
             
%        end
        
        % Perform M-step (maximize mapping W)
        Wp1 = zeros(D, no_dims);                        % (MD x R)
        Wp2 = zeros(no_dims, no_dims);                  % (R  x R)
        message = 'into the loop'
        Ez_pre = invM * W';
        Ezz_pre = sigma2 * invM;
        sigma2 = 0;                                     % (1  x 1)
        for i=1:n
            try
                [Nx X] = ReadFoStat (iplist{i});            
                Ez = Ez_pre * X;                         % (R x 1)
                Ezz = Ezz_pre + Ez * Ez';             % (R x R)
                Wp1 = Wp1 + X * Ez';
                Wp2 = Wp2 + Ezz;
                sigma2 = sigma2 + (sum(X.*X) - 2 * Ez' * W' * X + trace(Ezz* inW));
            catch
                'Skipping the following file'
                 iplist{i}
            end
        end
        message = 'out of the loop'
        W = Wp1 * inv(Wp2);                             % (MD x R)
        save(strcat (tmatprefix,int2str(iter)),'-mat','W');

        inW = W' * W;
        sigma2 = (1 / (n * D)) * sigma2
    end
    
    % Compute mapped data
%   disp(' ');
%    mapping.M = (inv(inW) * W')';
%    mappedX = X' * mapping.M;
    
    

import sys

sys.path.insert(0, 'library/libsvm/python')


from svmutil import *

import numpy as np


def ovrtrain(labels, data, options):
    labelSet = np.unique(labels)
    labelSetSize = len(labelSet)
    models = []
    for label in range(labelSetSize):
        models.append(svm_train(np.array(labels == labelSet[label], dtype=np.float32).tolist(), data.tolist(), options))
    model = {'models': models, 'labelSet': labelSet}
    return model


def ovotrain(labels, data, options):
    model = svm_train(labels.tolist(), data.tolist(), options)
    return model


def ovrpredict(labels, data, model):
    labelSet = model['labelSet']
    labelSetSize = len(labelSet)
    models = model['models']
    decv = np.zeros((len(labels), labelSetSize))
    for label in range(labelSetSize):
        [l, a, d] = svm_predict(np.array(labels == labelSet[label], dtype=np.float32).tolist(), data.tolist(),
                                models[label], "-q")
        d = np.array(d)
        decv[:, label] = np.squeeze(d * (2 * models[label].get_labels()[0] - 1))
    pred = np.argmax(decv, axis=1)
    pred = labelSet[pred]
    pred = np.array(pred)

    print((np.sum(labels == pred)) / float(data.shape[0])*100)

    ac = np.sum(labels == pred) / float(data.shape[0])
    return pred, ac, decv

def ovopredict(labels, data, model):
    [pred, ac, decv] = svm_predict(labels.tolist(), data.tolist(), model, "-q")
    pred =  np.asarray(pred);
    return pred, ac[0], decv
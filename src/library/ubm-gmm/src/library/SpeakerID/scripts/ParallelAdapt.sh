TRAINLIST=../flists/2004/spkrList.m
BINARY=../bin/AdaptOnlyMeans_2004_d
RFACTOR=16
TGMM=../models/tgmm
numSpeakers=`wc -l $TRAINLIST | cut -d ' ' -f1`
FEATLIST=list
FEATLISTAPP=list.1
INITBIN=../bin/Init
INITOUTFILE=vfinal
VQMapper=../bin/VQIter
VQReducer=../bin/VQComb
CODEBOOKSIZE=1024
UBMFILE=../ubm/n2004.m.ubm.new
CTRLFILE=../cfiles/fe-ctrl.base
MODELDIR=/home/srikanth/Research/Experiments/STGauss/models/2004.m.fcd.allubm
SPIDMODEL=$MODELDIR/SpidModel
mkdir -p $MODELDIR
	
  parallel -a $FEATLISTAPP --colsep ' '  $BINARY $CTRLFILE $UBMFILE $CODEBOOKSIZE {1} $MODELDIR/spid_{2}.gmm $RFACTOR
  rm $SPIDMODEL
  for i in `seq 1 $numSpeakers`; do
    echo $MODELDIR/spid_$i.gmm $CODEBOOKSIZE >> $SPIDMODEL
  done

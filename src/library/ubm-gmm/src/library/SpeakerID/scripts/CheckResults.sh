#!/bin/bash
RESULTFOLDER=$1
RESFILEPREFIX=out
GT=../flists/2004/male.gt
SPKRLIST=../flists/2004/spkrList.m
FRESULT=../models/2004.m.fcd/finalResult
numResultFiles=`wc -l $FRESULT | cut -d ' ' -f1`
total=0
correct=0
imp=0
for i in `seq 1 $numResultFiles`; do
#	resultFileName=$RESULTFOLDER/$i.$RESFILEPREFIX
	((total++))
  echo $total
  frline=`head -$total $FRESULT | tail -1`
#  echo $frline
  tfile=`echo $frline | cut -d ' ' -f1`
#  echo $tfile
  tname=`basename $tfile | cut -d '.' -f1`
  trueSpeakers=`grep $tname $GT | cut -d ' ' -f1 --complement`
#  echo $trueSpeakers
  hypSpkr=`echo $frline | cut -d ' ' -f2`
#  echo $hypSpkr
	for spkr in `echo $trueSpeakers`; do
		if [ $hypSpkr -eq $spkr ]; then
			((correct++))		
      break
		fi
	done
done

echo CORRECT=$correct
echo TOTAL=$total

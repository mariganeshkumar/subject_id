#ifndef __AdaptOnlyMeans_fromDF__
#define __AdaptOnlyMeans_fromDF__

#include "CommonFunctions.h"

VFV* AdaptOnlyMeans (VFV *ubmMeans, 
                     VFV *ubmVars,
                     float *ubmWeights,
                     unsigned int numClusters,
                     VFV *vfv,
                     unsigned long numVectors,
                     float relevanceFactor,
                     float probScaleFactor);

#endif

function coeff = kernelcca (data1, data2, kernelType, param)
	kappa = 100;
	N1 = size (data1,1);
	N2 = size(data2,1);
	K_x = KernelGramMatrix(data1, kernelType, param);
	K_y = KernelGramMatrix(data2, kernelType, param);
	eeta = 0.01;
%	R_x = pgso (K_x, eeta);
%	R_y = pgso (K_y, eeta);

	R_x = (chol (K_x))';
	R_y = (chol (K_y))';
	Z_xx = R_x' * R_x;
	Z_yy = R_y' * R_y;

	Z_xy = R_x' * R_y;
	Z_yx = R_y' * R_x;

	S_x = (chol(Z_xx + kappa *diag(ones(size(Z_xx,1),1))))';
	S_y = (chol(Z_yy + kappa *diag(ones(size(Z_yy,1),1))))';

	[v d] = eig ((Z_xx + kappa *diag(ones(size(Z_xx,1),1)))* Z_xy * inv(Z_yy + kappa * diag(ones(size(Z_yy,1),1))) * Z_yx);
%	v(:,1)
%	d(1,1)
	alpha = R_x * v(:,1);
	beta = inv(Z_yy) * Z_yx * alpha / d(1,1);
	
	coeff = (alpha' * K_x * K_y * beta) / sqrt((alpha' * (K_x)^2 * alpha));
	coeff = coeff / sqrt((beta' * (K_y)^2 * beta));	

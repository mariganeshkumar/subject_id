import math
import numpy as np
import scipy as sp

def ReadUtterance (fname):
  fptr = open (fname, "r")
  print fname
  if fptr == None:
    return None
  ln = fptr.readline().strip()
  dim, nvec= map(int,ln.split())
  fvecs = np.zeros ((nvec,dim))
  for i in range (0,nvec):
    fvecs[i,:] = [ float(f) for f in fptr.readline ().strip ().split ()]
  fptr.close()
  return fvecs


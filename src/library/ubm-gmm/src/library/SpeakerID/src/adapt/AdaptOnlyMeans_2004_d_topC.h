#include "CommonFunctions.h"
 VECTOR_OF_F_VECTORS* AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
             VECTOR_OF_F_VECTORS *ubmVars,
             float *ubmWeights,
             unsigned int numClusters,
                   VECTOR_OF_F_VECTORS *vfv,
             unsigned int numVectors,
             unsigned int relevanceFactor,
             float probScaleFactor,
             unsigned int cValue);

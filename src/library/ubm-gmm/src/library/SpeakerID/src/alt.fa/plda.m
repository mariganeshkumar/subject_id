function W = plda (tmatprefix, data_mat, keys, nF,max_iter,ldainit)
    path (path,'../ivec')
    % Initialize some variables
    n = size (data_mat,2);
    D = size (data_mat,1);
    ids = unique (keys);
    S = length (ids);
    N = zeros(S,1);
    for s = 1:S
        spkr_id = ids(s);
        N(s) = length(find(spkr_id == keys));
    end  
    
    Xtilda = zeros(D,S);
    eX = 0.; 
    for s = 1:S
        spkr_id = ids(s);
        file_ids = find(s == keys);
        Xtilda(:,s) = sum(data_mat(:,file_ids),2);
    end
    H = zeros(nF,S);
    eye_nF = eye(nF);
    Eest = diag(abs(randn(D,1)))/sqrt(D);
    W = ldainit;
    for iter = 1:max_iter
        iter
        %Matrix for the M-step
        R2 = zeros(nF);
        R_MD = zeros(nF);
        
        %Update estimates from previous iteration
        F = W;    
        E = Eest;
        
        eye_nF = eye(nF);
        iE = chol(E);
        log_det_E = 2*sum(log(diag(iE)));
        iEF = iE\(iE'\F);
        FtiEF = F'*iEF;
           
        %Precompute F'*iE*Xtilda 
        FtiEXtilda = iEF'*Xtilda;
                    
        %********************
        %E-step:
        %********************
        ie = 0;
        Nsum = 0;
        nspk = 0;
        for s = 1:S
            n = N(s);
            if n < 6
              continue
            end
            Nsum = Nsum + n;
            nspk = nspk + 1;
            iptemp  = chol(eye_nF + n*FtiEF);
            log_det_Pn(s) = 2*sum(log(diag(iptemp)));
            iP = iptemp \ (iptemp' \ eye_nF); 
            
            H(:,s) = iP*FtiEXtilda(:,s); %Posterior means
            
            %Weigthed sum of posterior covariances
            R2 = R2 + n * iP;
            R_MD = R_MD + iP;
        end
                           
        %********************
        %M-step
        %********************            
        Hn= bsxfun(@times,H,N'); 
        R2 = Hn*H' + R2;
        
        %Update F  
        Bt = Xtilda*H';
        W = Bt / R2;           
        
        %Update E
        Eest = (data_mat*data_mat' - W*Bt');
        
        Eest = diag(max(diag(Eest),10E-5));        
        sum(diag(Eest))
                       
        %********************
        %MD iteration
        %********************
        R_MD = (R_MD + H*H')/nspk;
        W = W*chol(R_MD);  
    end                                  
save(tmatprefix,'-mat','W');

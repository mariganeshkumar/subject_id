function pd = subspace_projection_distance(U,V)

[Qu,Ru] = qr(U,0);
[Qv,Rv] = qr(V,0);

s = svd(Qu'*Qv);
s = max(1 - (s.^2),0);

pd = sqrt(sum(s)/min(size(Qu,2),size(Qv,2)));
end


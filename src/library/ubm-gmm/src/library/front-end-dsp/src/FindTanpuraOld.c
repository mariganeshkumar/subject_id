#include "stdio.h"
#include "string.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"


void Usage() {
  printf ("Usage: ../bin/FindTanpura inWaveFile controlFile inEntropyFile outTanpuraFileName startFrame endFrame \n");
}

void main (int argc, char *argv[])
{
  short int       *waveform=NULL;
  char            *riffFileName=NULL, tempString[500], suffix='a', line[500];
  FILE            *controlFile=NULL;
  FILE            *inEntropyFile=NULL;
  long            frameSize, frameShift, numFrames, numSamples, samplingRate;
  int             startFrame, endFrame;
  float           timeIndex, entropy;
  int             startLoc, *highEntropy=NULL;
  static F_VECTOR *wFvect=NULL;
  static ASDF     *asdf;
  int             i, j, k, lnt;

 if (argc != 7) {
   Usage();
   exit(-1);
 }
 controlFile = fopen(argv[2], "r");
 inEntropyFile = fopen (argv[3], "r");
 riffFileName = argv[4];
 asdf = (ASDF *) malloc(1*sizeof(ASDF));
 InitializeASDF(asdf);
 InitializeStandardFrontEnd(asdf,controlFile);
 GsfOpen(asdf,argv[1]);
 sscanf(argv[5],"%d",  &startFrame);
 sscanf(argv[6], "%d", &endFrame);
 frameSize = (int) GetIAttribute(asdf, "windowSize");
 frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
 numFrames = (int) GetIAttribute(asdf, "numFrames");
 numSamples = (int) GetIAttribute(asdf, "numSamples");
 samplingRate = (int) GetIAttribute(asdf, "samplingRate");
 highEntropy = (int *) AllocIntArray (highEntropy, numFrames);
 waveform = (short int *) malloc (numSamples *sizeof (short int));
 wFvect = (F_VECTOR *) AllocFVector(frameSize);
 printf ("frameSize = %ld frameShift = %ld numFrames=%ld\n", frameSize, frameShift, numFrames);
 printf("numSamples = %ld\n",numSamples);
 if ((startFrame == -1) && (endFrame == -1)) {
     startFrame = 0;
     endFrame = numFrames;
   }
 i = startFrame;
 while (fgets (line, 200, inEntropyFile)) {
   sscanf(line, "%f %f %d", &timeIndex, &entropy, &highEntropy[i]);
   i++;
 }
 i = startFrame;
 while (i < endFrame){
   j = i;
   while ((highEntropy[j] == 1) && (j < endFrame))
     j++;
   startLoc = 0;
   waveform[0] = '\0';
   while ((highEntropy[j] == 0) && (j < endFrame)) {
     wFvect = (F_VECTOR *) FrameComputeWaveform(asdf, j, wFvect);
     for (k = 0; k < frameShift; k++)
       waveform[startLoc+k] = (short int) (wFvect->array[k]);
     startLoc = startLoc + frameShift;
     j++;
   }
   waveform[startLoc] = '\0';
   printf (" tempString = %s samples in file = %d\n", tempString, (long) (startLoc-1));
   if ((startLoc) > 1000) {
     tempString[0] = '\0';
     strcat (tempString, riffFileName);
     lnt = strlen(tempString);
     tempString[lnt] = suffix;
     tempString[lnt+1] = '\0';
     strcat (tempString, ".wav");
     if (suffix == 'z') 
       suffix = 'A';
     else
       suffix++; 
     WriteRIFF(tempString, waveform, startLoc-1, samplingRate, 2);
   }
   i = j+1;
 }
 fclose(inEntropyFile);
 fclose(controlFile);
 free(highEntropy);
 free (waveform);
 free (wFvect->array);
 free(wFvect);
 printf("files closed\n");
 fflush(stdout);
}














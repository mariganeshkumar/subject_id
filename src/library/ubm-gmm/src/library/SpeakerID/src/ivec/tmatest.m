% estimate tmatrix and save it in a file
function tmatest (listname, opname, R, ubmname)

% read the datalist
datalist = readlist (listname);
listlen = length (datalist);

% read supervectors from the list and prepare the 
% the matrix
[wts world vars] = ReadGMM (ubmname, 1024,44);
% this is supposed to be a column vector
m = size (world,1);
d = size (world,2 );
md = m * d;
world = reshape (world, md, 1);
vars = reshape (sqrt (vars), md, 1);
dmat = zeros (md,listlen);

% populate the entire matrix
for i=1:listlen
    dmat (:,i) = load (datalist(i,:)) - world;
end    

[u s v] = randsvd (dmat, R, 2,1);
clear dmat
for r = 1:R
  u(:,r) = u (:,r) ./ vars;
end  
save (opname, 'u', '-ascii');
exit

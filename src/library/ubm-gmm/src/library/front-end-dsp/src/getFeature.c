
/* General program for feature extraction */

#include "stdio.h"
#include "stdlib.h"
#include "fe/constants.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
#include "fe/BatchProcessWaveform.h"
#include "sp/sphere.h"

#define AVGLEN 3

// To average features
void average(float** circ, F_VECTOR *fv, int dim)
{
	int i,j;
	float sum=0;

	for (i=0;i<dim;i++)
	{
		sum = 0;
		for (j=0;j<AVGLEN;j++)
		{
			sum = sum + circ[j][i];
		}
		fv->array[i] = sum / AVGLEN;
	}
}


int main(int argc, char *argv[])
{
	int i,j,k;
	char *cname = NULL;
	FILE *cFile = NULL;
	ASDF *asdf;
	char *wavname, *featureName;
	VECTOR_OF_F_VECTORS *vfv;
	F_VECTOR *fvect;
	int numVectors, dim;
	FILE *outputFile = NULL;

	// for averaging features
	float **circArray;

	if (argc != 5)
	{
		printf("\n Useage: getFeature fe-ctrl.base featureName waveName outfileName \n");
		exit(-1);
	}

	cname = argv[1];
	cFile = fopen(cname,"r");
	featureName = argv[2];
	wavname = argv[3];  
    	outputFile = fopen(argv[4],"w");  

	printf("We are here now\n"); 

	asdf = (ASDF *) malloc(sizeof(ASDF));
	InitializeStandardFrontEnd(asdf,cFile);
	Cstore(asdf->fftSize);
	
	// numVectors is the total number of frames
	GsfOpen(asdf,wavname);
    numVectors =  asdf->numFrames;
    printf("[wavname = %s]  [feature = %s] [numFrames = %d] [numSamples = %d] \n",wavname,featureName,numVectors,asdf->numSamples); 
	fflush(stdout);
	fflush(stdout);	

	vfv  = (VECTOR_OF_F_VECTORS *) malloc(numVectors*sizeof(VECTOR_OF_F_VECTORS));
	if (vfv == NULL)
	{
		printf ("unable to allocate space for vfv \n");
	    exit(-1);
  	}
	
   
	// start getting features
    for (i=0;i<numVectors;i++) 
	{
		fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
		if(fvect == NULL)
		{
			printf("\n  ERROR!problems fvect \n");
		  	fflush(stdout);
			exit(-1);
		}
		vfv[i] = fvect;
    } // end for

    GsfClose(asdf);

	dim = vfv[0]->numElements;

printf("dim:%d\n",dim);
	// now vfv is populated
	// average vfv
	// allocate mem for circArray
	circArray = (float**)calloc(AVGLEN,sizeof(float*));
	for (i=0;i<AVGLEN;i++)
		circArray[i] = (float*)calloc(dim,sizeof(float));

	k = 0;

	for (i=0;i<numVectors;i++)
	{
		memcpy(circArray[k], vfv[i]->array,dim*sizeof(float));
		k = (k+1)%AVGLEN;
		if (i>=(AVGLEN-1))
			average(circArray,vfv[i],dim);
	}

	printf("Going to print features \n");
	for (i=0;i<numVectors;i++)
 	{
		//printf("Num elements: %d \n", vfv[i]->numElements);
		for (j=0;j<vfv[i]->numElements;j++)
		//for (j=7;j<51;j++)
		{
			fprintf(outputFile,"%f ", vfv[i]->array[j]);
		}
		fprintf(outputFile, "\n");
	} 
	
  	// cleanup
  	fclose(cFile);
  	fclose(outputFile);
	free(vfv);
} // end main


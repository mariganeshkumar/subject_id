/*
    This file extracts i vector

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"
#include<string.h>
#include<getopt.h>
#define CHAR_ALLOC(k) (char*)malloc(sizeof(char)*k)

void
printUsage() 
{
    printf("IvectEst [options] fostatList M D R tMat ivecList "
           "[lMatList]\n"
           "-l : ivec_list is a list\n"
           "-s : skip lmat calculation\n"
          );
    return;
}

int
main(int argc, char *argv[]) 
{

    FILE *fostatfile = NULL,
         *tmatfile = NULL,
         *ivecfile = NULL,
         *lmatfile = NULL;
    char *fostatname = NULL,
         *tmatname = NULL,
         *ivecname = NULL,
         *lmatname = NULL,
         line[256];
    char **folist = NULL,
         **iveclist = NULL,
         **lmatlist = NULL;
    int ivec_is_list = 0, 
        skip_lmat_calc = 0,
        oc;
    gsl_matrix *tmat = NULL,
               *del_matrix = NULL,
               *ivec_matrix = NULL,
               *lmat_matrix = NULL,
               *proj_matrix = NULL;
    gsl_matrix_view tsub_mat_view;
    fostat  *spkr_stats = NULL;
    int m, n, i,
        M, D, R, 
        tc, list_size;
    while((oc = getopt(argc,argv,":lhs")) != -1) {
        switch(oc) {
            case 'l':
                ivec_is_list = 1;
                break;
            case 's':
                skip_lmat_calc = 1;
                break;
            default:
                printUsage();
                exit(1);
        }
    }
    
    if(argc-optind < 5) {
        printUsage();
        exit(1);
    }
    fostatname = argv[optind++];
    sscanf(argv[optind++],"%d",&M);
    sscanf(argv[optind++],"%d",&D);
    sscanf(argv[optind++],"%d",&R);
    tmatname = argv[optind++];
    ivecname = argv[optind++];

    printf("statName: %s\ntmatName: %s\nivecName: %s\n", fostatname, tmatname, ivecname);

    if(!skip_lmat_calc) lmatname = argv[optind++];
    printf("lmatName: %s\n",lmatname);
    tmat = read_matrix(tmatname,M*D,R);

    printf("Tmat is read\n");
    if(tmat == NULL) {
        fprintf(stderr,"Unable to read tmatrix\n");
        exit(2);
    }

    if(ivec_is_list) {
	printf("Processing i-vector list \n");
        list_size = 0;
        fostatfile = fopen(fostatname,"r");
        while(fgets(line,sizeof line,fostatfile) != NULL) list_size++;
        rewind(fostatfile);
        folist = (char**) malloc(sizeof(char*)*list_size);
        iveclist = (char**) malloc(sizeof(char*)*list_size);
        if(!skip_lmat_calc) {
            lmatlist= (char**) malloc(sizeof(char*)*list_size);
	}
        i=0;
        while(!feof(fostatfile)) {        
            folist[i] = CHAR_ALLOC(256);
            fscanf(fostatfile,"%s\n",folist[i++]);
        }
        if(i != list_size) {
            fprintf(stderr,"Error while reading first order stat list\n");
            exit(1);
        }
        fclose(fostatfile);
        
	i=0;
        ivecfile = fopen(ivecname,"r");
        while(!feof(ivecfile)) {
            iveclist[i] = CHAR_ALLOC(256);
            fscanf(ivecfile,"%s\n",iveclist[i++]);
        }
        if(i != list_size) {
	    printf("i: %d, list_size: %d", i, list_size);
            fprintf(stderr,"Error while reading ivec list\n");
            exit(1);
        }
        fclose(ivecfile);
        if(!skip_lmat_calc) {
            i=0;
            lmatfile = fopen(lmatname,"r");
            while(!feof(lmatfile)) {
                lmatlist[i] = CHAR_ALLOC(256);
                fscanf(lmatfile,"%s\n",lmatlist[i++]);                  
            }
            if(i != list_size) {
                fprintf(stderr,"Error while reading cov mat list\n");
                exit(1);
            }
        }
    }
    else {
        list_size = 1;
        folist = (char**) malloc(sizeof(char*)*list_size);
        iveclist = (char**) malloc(sizeof(char*)*list_size);
        if(!skip_lmat_calc) 
            lmatlist= (char**) malloc(sizeof(char*)*list_size);
        folist[0] = (char*) malloc(sizeof(char)*256);
        iveclist[0] = (char*) malloc(sizeof(char)*256);
        strcpy(folist[0],fostatname);
        strcpy(iveclist[0],ivecname);
        if(!skip_lmat_calc) {
            lmatlist[0] = (char*) malloc(sizeof(char)*256);
            strcpy(lmatlist[0],lmatname);
        }
    }
   
    ivec_matrix = gsl_matrix_alloc(R,1);
    proj_matrix = gsl_matrix_alloc(R,1);
    // del_matrix is /\
    //              /  \ matrix = T^{t} N T
    //              ----
    del_matrix = gsl_matrix_alloc(R,R); 
    for(tc = 0; tc < list_size; tc++) {
	printProgress(tc/(float)(list_size));
        gsl_matrix_set_identity(del_matrix); 
        //printf("Current fostat file: %s\n",folist[tc]);
        spkr_stats = read_fostat(folist[tc]);  
        if(spkr_stats == NULL) {
            fprintf(stderr,"Error opening %s\n", folist[tc]);
            continue;
        }
        for(m = 0; m < M; m++) {
            tsub_mat_view = gsl_matrix_submatrix(tmat,m*D,0,D,R);
            if(!spkr_stats->eeta[m]) continue;
            gsl_blas_dsyrk(CblasUpper,CblasTrans, 
                           (double) spkr_stats->eeta[m],
                           &tsub_mat_view.matrix, 
                           1.0, del_matrix);

        }
        copyUpperToLower(del_matrix);
        lmat_matrix = invertMatrix(del_matrix);     

        gsl_blas_dgemm(CblasTrans,CblasNoTrans, 
                       1.0, tmat, spkr_stats->f, 0.0,
                       proj_matrix);
        gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,
                       1.0, lmat_matrix, proj_matrix, 0.0,
                       ivec_matrix);
        write_matrix(iveclist[tc],ivec_matrix);
        if(!skip_lmat_calc) {
            gsl_blas_dgemm(CblasNoTrans, CblasTrans, 
                           1.0, ivec_matrix, ivec_matrix,
                           1.0, lmat_matrix
                          );
            write_matrix(lmatlist[tc],lmat_matrix);
        }
        gsl_matrix_free(lmat_matrix);
        free_fostat(spkr_stats);
    }
    printf("\n");

    for(i = 0; i < list_size; i++) {
        free(folist[i]);
        free(iveclist[i]);
    }
    free(folist);
    free(iveclist);
	printf("\n");
    if(!skip_lmat_calc) {
    	free(lmatlist);
    }
}

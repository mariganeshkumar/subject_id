/*
 * =====================================================================================
 *
 *       Filename:  UtilityFunctions.h
 *
 *    Description:  Header file for UtilityFunctions.c
 *
 *
 * =====================================================================================
 */

#ifndef __UTILITY_FUNC_
#define __UTILITY_FUNC_
#include "CommonFunctions.h"

VFV*
computeDCTOnStream (VFV *ip, uint nvec, uint nfilts, uint nceps);
#endif

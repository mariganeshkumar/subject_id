/*----------------------------------------------------------------------------
 FrameComputeModGdCepstrumDCTRaw : Computes the modified group delay cepstrum
                                     for a frame of speech.

 Inputs : front-end structure of type ASDF
          frameIndex
 
 Outputs : a vector of "numCepstrum" cepstral coefficients

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeModGdCepstrumDCTRaw(ASDF *asdf, int frameIndex, F_VECTOR *fvect) {
  static int                           numFilters, windowSize;
  static int                           numCepstrum;
  static int                           fftOrder, fftSize, samplingRate;
  static int                           smthWinSize, removeLPhase, gdSign;
  static int                           startIndex, endIndex;
  static float                         minFrequency, maxFrequency;
  static float                         gamma, alfaP, alfaN;
  static ASDF                          *prevAsdf=NULL;
  static F_VECTOR                      *waveform;
  static float                         *farray; 
  int                                  i;
  if (prevAsdf != asdf) {
    numFilters = (int) GetIAttribute(asdf,"numFilters");
    numCepstrum = (int) GetIAttribute(asdf,"numCepstrum");
    fftSize = (int) GetIAttribute(asdf, "fftSize");
    fftOrder = (int) GetIAttribute(asdf, "fftOrder");
    windowSize = (int) GetIAttribute(asdf, "windowSize");
    smthWinSize = (int) GetIAttribute(asdf, "gdSmthWinSize");
    gamma = (float) GetFAttribute(asdf, "gamma");
    alfaP = (float) GetFAttribute(asdf, "gdPosScale");
    alfaN = (float) GetFAttribute(asdf, "gdNegScale");
    removeLPhase = (int) GetIAttribute(asdf, "gdRemoveLPhase");
    samplingRate = (int) GetIAttribute(asdf,"samplingRate");
    gdSign = (int) GetIAttribute(asdf,"gdSign");
    minFrequency = (float) GetFAttribute(asdf,"minFrequency");
    maxFrequency = (float) GetFAttribute(asdf,"maxFrequency");
    startIndex = (int) (minFrequency/samplingRate*fftSize);
    endIndex = (int) (maxFrequency/samplingRate*fftSize);
    waveform = (F_VECTOR *) AllocFVector(windowSize);
    farray = (float *) AllocFloatArray(farray, fftSize+1);
    prevAsdf = asdf;
}
  waveform = (F_VECTOR *) FrameComputeWaveform(asdf, frameIndex, waveform);
  for (i = 0; i < windowSize; i++)
    farray[i+1] = waveform->array[i];
  fvect->array = (float *) ModGdCepstrum_DCT(farray, windowSize, fftSize, 
					    fftOrder, numCepstrum, 
					    smthWinSize, fvect->array, 
					    alfaP, alfaN, gamma, gdSign, 
					    removeLPhase,  
					     startIndex, endIndex, 1.0);
 return(fvect);

}
/*-------------------------------------------------------------------------
 *  FrameComputeModGdCepstrumDCTMean -- Computes the Cepstrum mean for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - mean of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeModGdCepstrumDCTMean(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames, zeroMean;
  static F_VECTOR  *meanVector=NULL;
 int              i,j;
 int              numVoicedFrames = 0;
 numCepstrum = GetIAttribute(asdf,"numCepstrum");
 numFrames = GetIAttribute(asdf, "numFrames");
 numVoicedFrames = GetIAttribute(asdf, "numVoicedFrames");
 zeroMean = GetIAttribute(asdf, "zeroMean");
 meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
 for (i = 0; i < numCepstrum; i++)
   meanVector->array[i] = 0;
 for (i = 0; i < numFrames; i++) 
   if (asdf->vU[i]){
     fvect = (F_VECTOR *) FrameComputeModGdCepstrumDCTRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ )
       meanVector->array[j] = fvect->array[j] + meanVector->array[j];
   }
 for ( i = 0; i < numCepstrum; i++ )
   meanVector->array[i] = meanVector->array[i]/numVoicedFrames;
 return(meanVector);

}	/*  End of FrameComputeModGdCepstrumDCTMean	*/	



/*-------------------------------------------------------------------------
 *  FrameComputeModGdCepstrumDCTVariance -- Computes the 
 *                                              -- ModGdCepstrumDCT Variance for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - variance of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeModGdCepstrumDCTVariance(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames;
  static F_VECTOR  *meanVector=NULL, *varVector=NULL, *diffVector=NULL;
  int              i,j;
  
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   numFrames = GetIAttribute(asdf, "numVoicedFrames");
   meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
   meanVector = (F_VECTOR *) FrameComputeModGdCepstrumDCTMean(asdf, 0, meanVector);
   varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   diffVector = (F_VECTOR *) AllocFVector(numCepstrum);
   for (i = 0; i < numCepstrum; i++)
     varVector->array[i] = 0;
   for (i = 0; i < numFrames; i++) {
     fvect = (F_VECTOR *) FrameComputeModGdCepstrumDCTRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ ) {
       diffVector->array[j]= (fvect->array[j] - meanVector->array[j]);
       varVector->array[j] = diffVector->array[j]*diffVector->array[j] + varVector->array[j];
     }
   }
   for ( i = 0; i < numCepstrum; i++ ) {
     fvect->array[i] = varVector->array[i]/numFrames;
     if (fvect->array[i] == 0.0) {
       printf("Flooring variance to 1.0E-12 of index = %d\n", i);
       fvect->array[i] = 1.0E-12;
     }
   }
   return(fvect);
}	/*  End of FrameComputeModGdCepstrumDCTVariance	*/	




/*--------------------------------------------------------------------------
  FrameComputeModGdCepstrumDCT : Computes the modgd cepstrum of a given frame 
  inputs :
     asdf                     : front-end-parameters
     frameIndex               : frameNumber 
     
  outputs :
     cepstrum                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeModGdCepstrumDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int       numCepstrum;
  static int       zeroMean, featureVarNormalize;
  static ASDF      *prevAsdf=NULL;
  static F_VECTOR  *meanVector, *varVector;
  static char      oldName[500];
  static float     sqrtVal;
  int              i;

 if (prevAsdf != asdf) {
   zeroMean = (int) GetIAttribute(asdf, "zeroMean");
   featureVarNormalize = (int) GetIAttribute(asdf, "featureVarNormalize");
   if ((zeroMean)||(featureVarNormalize)) {
     numCepstrum = GetIAttribute(asdf,"numCepstrum");
     meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
     varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   }
   prevAsdf = asdf;
}
 if (((zeroMean)||(featureVarNormalize)) && (strcmp(oldName, asdf->waveFileName) != 0)) {
     meanVector = (F_VECTOR *) FrameComputeModGdCepstrumDCTMean(asdf, 0, meanVector);
     varVector = (F_VECTOR *) FrameComputeModGdCepstrumDCTVariance(asdf, 0, varVector);
     oldName[0] = '\0';
     strcpy(oldName, asdf->waveFileName);
 }

 fvect = (F_VECTOR *) FrameComputeModGdCepstrumDCTRaw (asdf, frameIndex, fvect);
 if (zeroMean) {
   for (i = 0; i < numCepstrum; i++)
     fvect->array[i] = fvect->array[i] - meanVector->array[i];
 }
 if (featureVarNormalize) 
   for (i = 0; i < numCepstrum; i++) {
     sqrtVal = (float) sqrtf(varVector->array[i]);
     fvect->array[i] = fvect->array[i]/sqrtVal;
   }
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeDeltaModGdCepstrumDCT : Computes the modgd deltaCepstrumNcN of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     deltaCepstrum        : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaModGdCepstrumDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDifference = GetIAttribute(asdf,"deltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDifference; i <= deltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
}
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeModGdCepstrumDCT(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeModGdCepstrumDCT(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
 LinearVectorScalarDivide(normalizingConst,fvect,fvect);
 return(fvect);
}
/*--------------------------------------------------------------------------
  FrameComputeDeltaDeltaModGdCepstrumDCT : Computes the modgd deltaDeltaCepstrumNcN of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : deltaDeltaCepstrumNcN returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaDeltaModGdCepstrumDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDeltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDeltaDifference = GetIAttribute(asdf,"deltaDeltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");  
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDeltaDifference; i <= deltaDeltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
 }
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDeltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeDeltaModGdCepstrumDCT(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeDeltaModGdCepstrumDCT(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
 LinearVectorScalarDivide(normalizingConst,fvect,fvect);
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeAugmentedModGdCepstrumDCT : Computes the augmentedCepstrum of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeAugmentedModGdCepstrumDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum;
  static ASDF     *prevAsdf=NULL;
  static F_VECTOR  *temp, *tempEnergy;
  int i;
  if (prevAsdf != asdf) {
    numCepstrum = GetIAttribute(asdf,"numCepstrum");
    temp = (F_VECTOR *) AllocFVector(numCepstrum);
    tempEnergy = (F_VECTOR *) AllocFVector(1);
    prevAsdf = asdf;
  }
  temp = (F_VECTOR *) FrameComputeModGdCepstrumDCT(asdf,frameIndex, temp);
  for (i = 0; i < numCepstrum; i++)
    fvect->array[i] = temp->array[i];
  temp = (F_VECTOR *) FrameComputeDeltaModGdCepstrumDCT(asdf,frameIndex, temp);
  for (i = numCepstrum; i < 2*numCepstrum; i++)
    fvect->array[i] = temp->array[i-numCepstrum];
  temp = (F_VECTOR *) FrameComputeDeltaDeltaModGdCepstrumDCT(asdf,frameIndex, temp);
  for (i = 2*numCepstrum; i < 3*numCepstrum; i++)
    fvect->array[i] = temp->array[i-2*numCepstrum];
  fvect->array[3*numCepstrum] = ((F_VECTOR *) FrameComputeEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+1] = ((F_VECTOR *) FrameComputeDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+2] = ((F_VECTOR *) FrameComputeDeltaDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  return(fvect);
}
/*----------------------------------------------------------------------------
 FrameComputeModGdCepstrumLPDCTRaw : Computes the modified group delay cepstrum
                                     for a frame of speech.

 Inputs : front-end structure of type ASDF
          frameIndex
 
 Outputs : a vector of "numCepstrum" cepstral coefficients

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeModGdCepstrumLPDCTRaw(ASDF *asdf, int frameIndex, F_VECTOR *fvect) {
  static int                           numFilters, windowSize;
  static int                           numCepstrum;
  static int                           fftOrder, fftSize, samplingRate;
  static int                           lpOrder, removeLPhase, gdSign;
  static int                           startIndex, endIndex;
  static float                         minFrequency, maxFrequency;
  static float                         gamma, alfaP, alfaN;
  static ASDF                          *prevAsdf=NULL;
  static F_VECTOR                      *waveform;
  static float                         *farray; 
  int                                  i;
  if (prevAsdf != asdf) {
    numFilters = (int) GetIAttribute(asdf,"numFilters");
    numCepstrum = (int) GetIAttribute(asdf,"numCepstrum");
    fftSize = (int) GetIAttribute(asdf, "fftSize");
    fftOrder = (int) GetIAttribute(asdf, "fftOrder");
    windowSize = (int) GetIAttribute(asdf, "windowSize");
    lpOrder = (int) GetIAttribute(asdf, "lpOrder");
    gamma = (float) GetFAttribute(asdf, "gamma");
    alfaP = (float) GetFAttribute(asdf, "gdPosScale");
    alfaN = (float) GetFAttribute(asdf, "gdNegScale");
    removeLPhase = (int) GetIAttribute(asdf, "gdRemoveLPhase");
    samplingRate = (int) GetIAttribute(asdf,"samplingRate");
    gdSign = (int) GetIAttribute(asdf,"gdSign");
    minFrequency = (float) GetFAttribute(asdf,"minFrequency");
    maxFrequency = (float) GetFAttribute(asdf,"maxFrequency");
    startIndex = (int) (minFrequency/samplingRate*fftSize);
    endIndex = (int) (maxFrequency/samplingRate*fftSize);
    waveform = (F_VECTOR *) AllocFVector(windowSize);
    farray = (float *) AllocFloatArray(farray, fftSize+1);
    prevAsdf = asdf;
}
  waveform = (F_VECTOR *) FrameComputeWaveform(asdf, frameIndex, waveform);
  for (i = 0; i < windowSize; i++)
    farray[i+1] = waveform->array[i];
  fvect->array = (float *) ModGdCepstrum_LPDCT(farray, windowSize, fftSize, 
					    fftOrder, numCepstrum, 
					    lpOrder, fvect->array, 
					    alfaP, alfaN, gamma, gdSign, 
					    removeLPhase,  
					     startIndex, endIndex, 1.0);
 return(fvect);

}
/*-------------------------------------------------------------------------
 *  FrameComputeModGdCepstrumLPDCTMean -- Computes the Cepstrum mean for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - mean of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeModGdCepstrumLPDCTMean(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames, zeroMean;
  static F_VECTOR  *meanVector=NULL;
 int              i,j;
 int              numVoicedFrames = 0;
 numCepstrum = GetIAttribute(asdf,"numCepstrum");
 numFrames = GetIAttribute(asdf, "numFrames");
 numVoicedFrames = GetIAttribute(asdf, "numVoicedFrames");
 zeroMean = GetIAttribute(asdf, "zeroMean");
 meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
 for (i = 0; i < numCepstrum; i++)
   meanVector->array[i] = 0;
 for (i = 0; i < numFrames; i++) 
   if (asdf->vU[i]){
     fvect = (F_VECTOR *) FrameComputeModGdCepstrumLPDCTRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ )
       meanVector->array[j] = fvect->array[j] + meanVector->array[j];
   }
 for ( i = 0; i < numCepstrum; i++ )
   meanVector->array[i] = meanVector->array[i]/numVoicedFrames;
 return(meanVector);

}	/*  End of FrameComputeModGdCepstrumLPDCTMean	*/	



/*-------------------------------------------------------------------------
 *  FrameComputeModGdCepstrumLPDCTVariance -- Computes the 
 *                                              -- ModGdCepstrumDCT Variance for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - variance of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeModGdCepstrumLPDCTVariance(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames;
  static F_VECTOR  *meanVector=NULL, *varVector=NULL, *diffVector=NULL;
  int              i,j;
  
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   numFrames = GetIAttribute(asdf, "numVoicedFrames");
   meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
   meanVector = (F_VECTOR *) FrameComputeModGdCepstrumDCTMean(asdf, 0, meanVector);
   varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   diffVector = (F_VECTOR *) AllocFVector(numCepstrum);
   for (i = 0; i < numCepstrum; i++)
     varVector->array[i] = 0;
   for (i = 0; i < numFrames; i++) {
     fvect = (F_VECTOR *) FrameComputeModGdCepstrumLPDCTRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ ) {
       diffVector->array[j]= (fvect->array[j] - meanVector->array[j]);
       varVector->array[j] = diffVector->array[j]*diffVector->array[j] + varVector->array[j];
     }
   }
   for ( i = 0; i < numCepstrum; i++ ) {
     fvect->array[i] = varVector->array[i]/numFrames;
     if (fvect->array[i] == 0.0) {
       printf("Flooring variance to 1.0E-12 of index = %d\n", i);
       fvect->array[i] = 1.0E-12;
     }
   }
   return(fvect);
}	/*  End of FrameComputeModGdCepstrumLPDCTVariance	*/	




/*--------------------------------------------------------------------------
  FrameComputeModGdCepstrumDCT : Computes the modgd cepstrum of a given frame 
  inputs :
     asdf                     : front-end-parameters
     frameIndex               : frameNumber 
     
  outputs :
     cepstrum                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeModGdCepstrumLPDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int       numCepstrum;
  static int       zeroMean, featureVarNormalize;
  static ASDF      *prevAsdf=NULL;
  static F_VECTOR  *meanVector, *varVector;
  static char      oldName[500];
  static float     sqrtVal;
  int              i;

 if (prevAsdf != asdf) {
   zeroMean = (int) GetIAttribute(asdf, "zeroMean");
   featureVarNormalize = (int) GetIAttribute(asdf, "featureVarNormalize");
   if ((zeroMean)||(featureVarNormalize)) {
     numCepstrum = GetIAttribute(asdf,"numCepstrum");
     meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
     varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   }
   prevAsdf = asdf;
}
 if (((zeroMean)||(featureVarNormalize)) && (strcmp(oldName, asdf->waveFileName) != 0)) {
     meanVector = (F_VECTOR *) FrameComputeModGdCepstrumLPDCTMean(asdf, 0, meanVector);
     varVector = (F_VECTOR *) FrameComputeModGdCepstrumLPDCTVariance(asdf, 0, varVector);
     oldName[0] = '\0';
     strcpy(oldName, asdf->waveFileName);
 }

 fvect = (F_VECTOR *) FrameComputeModGdCepstrumLPDCTRaw (asdf, frameIndex, fvect);
 if (zeroMean) {
   for (i = 0; i < numCepstrum; i++)
     fvect->array[i] = fvect->array[i] - meanVector->array[i];
 }
 if (featureVarNormalize) 
   for (i = 0; i < numCepstrum; i++) {
     sqrtVal = (float) sqrtf(varVector->array[i]);
     fvect->array[i] = fvect->array[i]/sqrtVal;
   }
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeDeltaModGdCepstrumLPDCT : Computes the modgd deltaCepstrum of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     deltaCepstrum        : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaModGdCepstrumLPDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDifference = GetIAttribute(asdf,"deltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDifference; i <= deltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
}
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeModGdCepstrumLPDCT(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeModGdCepstrumLPDCT(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
 LinearVectorScalarDivide(normalizingConst,fvect,fvect);
 return(fvect);
}
/*--------------------------------------------------------------------------
  FrameComputeDeltaDeltaModGdCepstrumLPDCT : Computes the modgd deltaDeltaCepstrum of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : deltaDeltaCepstrumLPDCT returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaDeltaModGdCepstrumLPDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDeltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDeltaDifference = GetIAttribute(asdf,"deltaDeltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");  
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDeltaDifference; i <= deltaDeltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
 }
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDeltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeDeltaModGdCepstrumLPDCT(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeDeltaModGdCepstrumLPDCT(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
 LinearVectorScalarDivide(normalizingConst,fvect,fvect);
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeAugmentedModGdCepstrumLPDCT : Computes the augmentedCepstrum of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeAugmentedModGdCepstrumLPDCT(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum;
  static ASDF     *prevAsdf=NULL;
  static F_VECTOR  *temp, *tempEnergy;
  int i;
  if (prevAsdf != asdf) {
    numCepstrum = GetIAttribute(asdf,"numCepstrum");
    temp = (F_VECTOR *) AllocFVector(numCepstrum);
    tempEnergy = (F_VECTOR *) AllocFVector(1);
    prevAsdf = asdf;
  }
  temp = (F_VECTOR *) FrameComputeModGdCepstrumLPDCT(asdf,frameIndex, temp);
  for (i = 0; i < numCepstrum; i++)
    fvect->array[i] = temp->array[i];
  temp = (F_VECTOR *) FrameComputeDeltaModGdCepstrumLPDCT(asdf,frameIndex, temp);
  for (i = numCepstrum; i < 2*numCepstrum; i++)
    fvect->array[i] = temp->array[i-numCepstrum];
  temp = (F_VECTOR *) FrameComputeDeltaDeltaModGdCepstrumLPDCT(asdf,frameIndex, temp);
  for (i = 2*numCepstrum; i < 3*numCepstrum; i++)
    fvect->array[i] = temp->array[i-2*numCepstrum];
  fvect->array[3*numCepstrum] = ((F_VECTOR *) FrameComputeEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+1] = ((F_VECTOR *) FrameComputeDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+2] = ((F_VECTOR *) FrameComputeDeltaDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  return(fvect);
}
/*----------------------------------------------------------------------------
 FrameComputeProductGDCepstrumNcNRaw : Computes the modified group delay cepstrum
                                     for a frame of speech.

 Inputs : front-end structure of type ASDF
          frameIndex
 
 Outputs : a vector of "numCepstrum" cepstral coefficients

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeProductGdCepstrumNcNRaw(ASDF *asdf, int frameIndex, F_VECTOR *fvect) {
  static int                           numFilters, windowSize;
  static int                           numCepstrum;
  static int                           fftOrder, fftSize, samplingRate;
  static int                           smthWinSize, removeLPhase, gdSign;
  static int                           startIndex, endIndex;
  static float                         minFrequency, maxFrequency;
  static int                           removeMin, mgdNormalize;
  static float                         gamma, alfaP, alfaN;
  static ASDF                          *prevAsdf=NULL;
  static F_VECTOR                      *waveform;
  static float                         *farray; 
  int                                  i;
  if (prevAsdf != asdf) {
    numFilters = (int) GetIAttribute(asdf,"numFilters");
    numCepstrum = (int) GetIAttribute(asdf,"numCepstrum");
    fftSize = (int) GetIAttribute(asdf, "fftSize");
    fftOrder = (int) GetIAttribute(asdf, "fftOrder");
    windowSize = (int) GetIAttribute(asdf, "windowSize");
    smthWinSize = (int) GetIAttribute(asdf, "gdSmthWinSize");
    gamma = (float) GetFAttribute(asdf, "gamma");
    alfaP = (float) GetFAttribute(asdf, "gdPosScale");
    alfaN = (float) GetFAttribute(asdf, "gdNegScale");
    mgdNormalize = (int) GetIAttribute(asdf, "mgdNormalize");
    removeLPhase = (int) GetIAttribute(asdf, "gdRemoveLPhase");
    removeMin =  (int) GetIAttribute(asdf, "removeMin");
    samplingRate = (int) GetIAttribute(asdf,"samplingRate");
    gdSign = (int) GetIAttribute(asdf,"gdSign");
    minFrequency = (float) GetFAttribute(asdf,"minFrequency");
    maxFrequency = (float) GetFAttribute(asdf,"maxFrequency");
    startIndex = (int) (minFrequency/samplingRate*fftSize);
    endIndex = (int) (maxFrequency/samplingRate*fftSize);
    waveform = (F_VECTOR *) AllocFVector(windowSize);
    farray = (float *) AllocFloatArray(farray, fftSize+1);
    prevAsdf = asdf;
}
  waveform = (F_VECTOR *) FrameComputeWaveform(asdf, frameIndex, waveform);
  for (i = 0; i < windowSize; i++)
    farray[i+1] = waveform->array[i];
  fvect->array = (float *) ProductGdCepstrumNcN(farray, windowSize, fftSize, 
					    fftOrder, numCepstrum, 
					    smthWinSize, fvect->array, 
					    alfaP, alfaN, gamma, gdSign, 
					    removeLPhase, removeMin, mgdNormalize, startIndex, endIndex);
 return(fvect);

}
/*-------------------------------------------------------------------------
 *  FrameComputeProductGdCepstrumNcNMean -- Computes the Cepstrum mean for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - mean of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeProductGdCepstrumNcNMean(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames, zeroMean;
  static F_VECTOR  *meanVector=NULL;
  int              i,j;
 int              numVoicedFrames = 0;
 numCepstrum = GetIAttribute(asdf,"numCepstrum");
 numFrames = GetIAttribute(asdf, "numFrames");
 numVoicedFrames = GetIAttribute(asdf, "numVoicedFrames");
 zeroMean = GetIAttribute(asdf, "zeroMean");
 meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
 for (i = 0; i < numCepstrum; i++)
   meanVector->array[i] = 0;
 for (i = 0; i < numFrames; i++) 
   if (asdf->vU[i]){
     fvect = (F_VECTOR *) FrameComputeProductGdCepstrumNcNRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ )
       meanVector->array[j] = fvect->array[j] + meanVector->array[j];
   }
 for ( i = 0; i < numCepstrum; i++ )
   meanVector->array[i] = meanVector->array[i]/numVoicedFrames;
 return(meanVector);

}	/*  End of FrameComputeProductGdCepstrumNcNMean	*/	




/*-------------------------------------------------------------------------
 *  FrameComputeProductGdCepstrumDCTVariance -- Computes the 
 *                                              -- ProdGdCepstrumNcN Variance for a waveform
 *    Args: 
 *    asdf                 : front-end-parameters
 *    frameIndex           : not used
 *    Returns:	
 *    fvect        : returned as an F_VECTOR - variance of cepstra
 *    Bugs:	
 * -------------------------------------------------------------------------*/

F_VECTOR * FrameComputeProductGdCepstrumNcNVariance(ASDF *asdf, int frameIndex, 
				    F_VECTOR *fvect) 
{
  static int      numCepstrum, numFrames;
  static F_VECTOR  *meanVector=NULL, *varVector=NULL, *diffVector=NULL;
  int              i,j;
  
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   numFrames = GetIAttribute(asdf, "numVoicedFrames");
   meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
   meanVector = (F_VECTOR *) FrameComputeProductGdCepstrumNcNMean(asdf, 0, meanVector);
   varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   diffVector = (F_VECTOR *) AllocFVector(numCepstrum);
   for (i = 0; i < numCepstrum; i++)
     varVector->array[i] = 0;
   for (i = 0; i < numFrames; i++) {
     fvect = (F_VECTOR *) FrameComputeProductGdCepstrumNcNRaw(asdf,i, fvect);
     for ( j = 0; j < numCepstrum; j++ ) {
       diffVector->array[j]= (fvect->array[j] - meanVector->array[j]);
       varVector->array[j] = diffVector->array[j]*diffVector->array[j] + varVector->array[j];
     }
   }
   for ( i = 0; i < numCepstrum; i++ ) {
     fvect->array[i] = varVector->array[i]/numFrames;
     if (fvect->array[i] == 0.0) {
       printf("Flooring variance to 1.0E-12 of index = %d\n", i);
       fvect->array[i] = 1.0E-12;
     }
   }
   return(fvect);
}	/*  End of FrameComputeProductGdCepstrumNcNVariance	*/	



/*--------------------------------------------------------------------------
  FrameComputeProductGdCepstrumNcN : Computes the modgd cepstrum of a given frame 
  inputs :
     asdf                     : front-end-parameters
     frameIndex               : frameNumber 
     
  outputs :
     cepstrum                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeProductGdCepstrumNcN(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int       numCepstrum;
  static int       zeroMean, featureVarNormalize;
  static ASDF      *prevAsdf=NULL;
  static F_VECTOR  *meanVector, *varVector;
  static char      oldName[500];
  static float     sqrtVal;
  int              i;

 if (prevAsdf != asdf) {
   zeroMean = (int) GetIAttribute(asdf, "zeroMean");
   featureVarNormalize = (int) GetIAttribute(asdf, "featureVarNormalize");
   if ((zeroMean) ||(featureVarNormalize)) {
     numCepstrum = GetIAttribute(asdf,"numCepstrum");
     meanVector = (F_VECTOR *) AllocFVector(numCepstrum);
     varVector = (F_VECTOR *) AllocFVector(numCepstrum);
   }
   prevAsdf = asdf;
}
 if (((zeroMean)||(featureVarNormalize)) && (strcmp(oldName, asdf->waveFileName) != 0)) {
     meanVector = (F_VECTOR *) FrameComputeProductGdCepstrumNcNMean(asdf, 0, meanVector);
     varVector = (F_VECTOR *) FrameComputeProductGdCepstrumNcNVariance(asdf, 0, varVector);
     oldName[0] = '\0';
     strcpy(oldName, asdf->waveFileName);
 }

 fvect = (F_VECTOR *) FrameComputeProductGdCepstrumNcNRaw (asdf, frameIndex, fvect);
 if (zeroMean) {
   for (i = 0; i < numCepstrum; i++)
     fvect->array[i] = fvect->array[i] - meanVector->array[i];
 }
 if (featureVarNormalize) 
   for (i = 0; i < numCepstrum; i++) {
     sqrtVal = (float) sqrtf(varVector->array[i]);
     fvect->array[i] = fvect->array[i]/sqrtVal;
   }
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeDeltaProductGdCepstrumNcN : Computes the modgd deltaCepstrumNcN of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     deltaCepstrum        : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaProductGdCepstrumNcN(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDifference = GetIAttribute(asdf,"deltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDifference; i <= deltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
}
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeProductGdCepstrumNcN(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeProductGdCepstrumNcN(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
   LinearVectorScalarDivide(normalizingConst,fvect,fvect);
  return(fvect);
}
/*--------------------------------------------------------------------------
  FrameComputeDeltaDeltaProductGdCepstrumNcN : Computes the modgd deltaDeltaCepstrumNcN of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : deltaDeltaCepstrumNcN returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeDeltaDeltaProductGdCepstrumNcN(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum,
                  deltaDeltaDifference,numFrames;
  static float    normalizingConst =0;
  static ASDF     *prevAsdf=NULL;
 static F_VECTOR  *prev,*next,*temp;
 int i;
 if (prevAsdf != asdf) {
   numCepstrum = GetIAttribute(asdf,"numCepstrum");
   deltaDeltaDifference = GetIAttribute(asdf,"deltaDeltaDifference");
   numFrames = GetIAttribute(asdf,"numFrames");  
   prev = (F_VECTOR *) AllocFVector(numCepstrum);
   next = (F_VECTOR *) AllocFVector(numCepstrum);
   temp = (F_VECTOR *) AllocFVector(numCepstrum);
   prevAsdf = asdf;
   for (i = -deltaDeltaDifference; i <= deltaDeltaDifference; i++)
     normalizingConst = normalizingConst+i*i;
 }
 for (i = 0; i < numCepstrum; i++)
   fvect->array[i] = 0;
 for (i = 1; i <= deltaDeltaDifference; i++){
   if (((frameIndex-i) >= 0)&& (asdf->vU[frameIndex-i]))
     prev = (F_VECTOR *) FrameComputeDeltaProductGdCepstrumNcN(asdf,frameIndex-i,prev);
   else
     InitFVector(prev);
   if (((frameIndex+i) <=  numFrames)&& (asdf->vU[frameIndex+i]))
     next = (F_VECTOR *) FrameComputeDeltaProductGdCepstrumNcN(asdf,frameIndex+i,next);
   else
     InitFVector(next);
   LinearVectorDifference(prev,next,temp);
   LinearVectorScalarMultiply((float)(i),temp,temp);
   LinearVectorAddition(temp,fvect,fvect);
 }
 LinearVectorScalarDivide(normalizingConst,fvect,fvect);
 return(fvect);
}

/*--------------------------------------------------------------------------
  FrameComputeAugmentedProductGdCepstrumNcN : Computes the augmentedCepstrum of a given frame of speech
  inputs :
     asdf                  : front-end-parameters
     frameIndex           : frameNumber 
     
  outputs :
     fvect                 : returned as an F_VECTOR

---------------------------------------------------------------------------*/

F_VECTOR *FrameComputeAugmentedProductGdCepstrumNcN(ASDF *asdf, int frameIndex, F_VECTOR *fvect) 
{
  static int      numCepstrum;
  static ASDF     *prevAsdf=NULL;
  static F_VECTOR  *temp, *tempEnergy;
  int i;
  if (prevAsdf != asdf) {
    numCepstrum = GetIAttribute(asdf,"numCepstrum");
    temp = (F_VECTOR *) AllocFVector(numCepstrum);
    tempEnergy = (F_VECTOR *) AllocFVector(1);
    prevAsdf = asdf;
  }
  temp = (F_VECTOR *) FrameComputeProductGdCepstrumNcN(asdf,frameIndex, temp);
  for (i = 0; i < numCepstrum; i++)
    fvect->array[i] = temp->array[i];
  temp = (F_VECTOR *) FrameComputeDeltaProductGdCepstrumNcN(asdf,frameIndex, temp);
  for (i = numCepstrum; i < 2*numCepstrum; i++)
    fvect->array[i] = temp->array[i-numCepstrum];
  temp = (F_VECTOR *) FrameComputeDeltaDeltaProductGdCepstrumNcN(asdf,frameIndex, temp);
  for (i = 2*numCepstrum; i < 3*numCepstrum; i++)
    fvect->array[i] = temp->array[i-2*numCepstrum];
  fvect->array[3*numCepstrum] = ((F_VECTOR *) FrameComputeEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+1] = ((F_VECTOR *) FrameComputeDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  fvect->array[3*numCepstrum+2] = ((F_VECTOR *) FrameComputeDeltaDeltaEnergy(asdf, frameIndex, tempEnergy))->array[0];
  return(fvect);
}

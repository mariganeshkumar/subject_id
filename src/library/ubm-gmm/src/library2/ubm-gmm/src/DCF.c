/*
    This file computes the Detection Cost Function

    Copyright (C) 2015-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

int Swap (float *a, float *b, int *c, int *d) {
  int temp;
  float tempf;
  tempf = *a;
  *a = *b;
  *b = tempf;
  temp = *c;
  *c = *d;
  *d = temp;
}

void QuickSort(float *v, int *t, int left, int right) {
  int   i,j,k;
  float x;
  i = left;
  j = right;
  x = v[(left+right)/2];
  do {
    while (v[i] < x)
      i++;
    while (x < v[j])
      j--;
     if (i <= j) {
       Swap(&v[i],&v[j],&t[i],&t[j]);
       i++;
       j--;
     }
 }   while (i < j);
  if (left < j)
    QuickSort(v,t,left,j);
  if (i < right)
    QuickSort(v,t,i,right);
}

int main(int argc, char *argv[]) {
	int i, j, trueCount, falseCount, tempCount, index;
	int *indicator, *sumTrue, *sumFalse, costTarget, costNontgt;
	float tempf, *score, *pMiss, *pFA, *DCF, pTarget, pNontgt, minDCF;
	char *trueFileName, *falseFileName;
	FILE *trueFile, *falseFile;
	if(argc != 9) {
		printf("Usage: %s true_score_file #true_score false_score_file #false_score, target_prob, miss_cost(int), non-target_prob, fa_cost(int)\n",argv[0]);
		return -1;
	}
	trueFileName=argv[1];
	falseFileName=argv[3];
	trueCount=atoi(argv[2]);
	falseCount=atoi(argv[4]);
	pTarget=atof(argv[5]);
	costTarget=atoi(argv[6]);
	pNontgt=atof(argv[7]);
	costNontgt=atoi(argv[8]);
	score=(float *)malloc((trueCount+falseCount)*sizeof(float *));
	indicator=(int *)malloc((trueCount+falseCount)*sizeof(int *));
	sumTrue=(int *)malloc((trueCount+falseCount)*sizeof(int *));
	sumFalse=(int *)malloc((trueCount+falseCount)*sizeof(int *));
	pMiss=(float *)malloc((trueCount+falseCount+1)*sizeof(float *));
	pFA=(float *)malloc((trueCount+falseCount+1)*sizeof(float *));
	DCF=(float *)malloc((trueCount+falseCount+1)*sizeof(float *));
	trueFile=fopen(trueFileName,"r");
	falseFile=fopen(falseFileName,"r");
	tempCount=0;
	while(!feof(trueFile)) {
		fscanf(trueFile,"%f",&score[tempCount]);
		tempCount++;
	}
	if(tempCount-1 != trueCount) {
		printf("True Count does not match!!\n");
		return -1;
	}
	tempCount=0;
	while(!feof(falseFile)) {
		fscanf(falseFile,"%f",&score[trueCount+tempCount]);
		tempCount++;
	}
	if(tempCount-1 != falseCount) {
		printf("False Count does not match!!\n");
		return -1;
	}
	for(i=0;i<trueCount+falseCount;i++)
		if(i<trueCount) indicator[i]=1;
		else indicator[i]=0;
	
	QuickSort(score, indicator, 0, trueCount+falseCount);

	sumTrue[0]=indicator[0];
	sumFalse[0]=falseCount+indicator[0]-1;
	for(i=1;i<trueCount+falseCount;i++) {
		sumTrue[i]=sumTrue[i-1]+indicator[i];
		sumFalse[i]=falseCount-(i-sumTrue[i]+1);
	}
	
	pMiss[0]=0;
	pFA[0]=1;
	DCF[0]=costTarget*pTarget*pMiss[0] + costNontgt*pNontgt*pFA[0];
	minDCF=DCF[0];
	index=0;
	for(i=1;i<trueCount+falseCount+1;i++) {
		pMiss[i]=(float)sumTrue[i-1]/trueCount;
		pFA[i]=(float)sumFalse[i-1]/falseCount;
		DCF[i]=costTarget*pTarget*pMiss[i] + costNontgt*pNontgt*pFA[i];
		if(DCF[i]<minDCF) {
			minDCF=DCF[i];
			index=i;
		}
	}

	printf("Threshold = %f\n",score[index]);
	printf("final mindcf = %f\n",minDCF);

	fclose(trueFile);
	fclose(falseFile);
	free(score);
	free(indicator);
	free(sumTrue);
	free(sumFalse);
	free(pMiss);
	free(pFA);
	free(DCF);
}

#include "AdaptOnlyMeans.h"

/**************
 * This version of AdaptOnlyMeans is an implementation following
 * the equations in reynold's paper. But there are lots of practical
 * issues with this implementation. For starters, when this program
 * is compiled, it would seg-fault because it tries to take log
 * of feature vector elements that could be negative
 * ******************/


float ComputeThreshold(ASDF *asdf, float thresholdScale)
{
  int i;
  float ave = 0;
  for (i = 0; i < asdf->numFrames; i++)
    ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
  ave = ave/asdf->numFrames;
  return (ave*thresholdScale);
}


VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *featureName,
					   unsigned long  *numVectors, float thresholdScale) {

  F_VECTOR                  *fvect;
 VECTOR_OF_F_VECTORS        *vfv;
 int                        i;
 unsigned long              frameNum;
 float                      threshold, energy;

  vfv = (VECTOR_OF_F_VECTORS *) calloc(asdf->numFrames, sizeof(VECTOR_OF_F_VECTORS));
  threshold = (float) ComputeThreshold(asdf, thresholdScale);
  frameNum = 0;
  for (i = 0; i < asdf->numFrames; i++) {
    energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
    if (energy > threshold) {
      fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
      if(fvect == NULL) {
	printf("problems fvect\n");
	fflush(stdout);
	exit(-1);
      }
      vfv[frameNum] = fvect;
      frameNum++;
    }
  }

  GsfClose(asdf);
*numVectors = frameNum;
return(vfv);
}

float MixtureProbability(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *speakerMean,
		   VECTOR_OF_F_VECTORS *speakerVar, F_VECTOR *speakerWts, 
		  int numClusters, float probScaleFactor){
int                          i;
float                        maxProb = 0, probValue;
int                          featLength; 
            
/*  printf("enter MixtureProbability\n");
  fflush(stdout);*/
  featLength = fvect->numElements;
  /*  printf("numClusters = %d featLength = %d\n",numClusters,featLength);
  fflush(stdout);*/
   maxProb = ComputeProbability(speakerMean[0], 
				   speakerVar[0], 
				   speakerWts->array[0], 
				   fvect, probScaleFactor); //fvect->numElements;
  for (i = 1; i < numClusters; i++) {
    probValue = ComputeProbability(speakerMean[i], 
				   speakerVar[i], 
				   speakerWts->array[i], 
				   fvect, probScaleFactor); //fvect->numElements;
    // if (fabs(probValue) < 10000)
    maxProb  = LogAdd(maxProb, probValue);
  }
  //  printf("probs %f\n",maxProb);
  return(maxProb);
}

/************************************************************************
  Function            : ComputeLikelihood - computes the
                        likelihood for the entire utterance data.

  Inputs              : vfv - feature vectors for an utterance
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
  Outputs             : Distortion
            
  **************************************************************************/

float  *ComputeLikelihood(VECTOR_OF_F_VECTORS *vfv, unsigned long  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor) {
  
  int                     i, j;
  float                   mixProbValue;
  float                   evidence;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	 mixProbValue = MixtureProbability(vfv[j], speakerMeans[i], 
			       speakerVars[i], speakerWts[i],numClusters[i], 
			       probScaleFactor);
	 //      if (mixProbValue != 0.0)
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    printf("Distortion %d = %f\n",i,Distortion[i]);
    fflush(stdout); 
    Distortion[i] = Distortion[i]/numVectors;
  }
  evidence = Distortion[0];
  for (i = 1; i < numSpeakers; i++)
    evidence = LogAdd(evidence, Distortion[i]);
  for (i = 0; i < numSpeakers; i++)
    Distortion[i] = Distortion[i] - evidence;
  return(Distortion);
}

VECTOR_OF_F_VECTORS* AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
				    VECTOR_OF_F_VECTORS *ubmVars,
				    float *ubmWeights,
				    unsigned int numClusters,
			            VECTOR_OF_F_VECTORS *featureVectors,
				    unsigned int numVectors,
				    unsigned int relevanceFactor,
				    float probScaleFactor)
  {
	VECTOR_OF_F_VECTORS	*logProb, *expectation;
	float			*eeta, alpha, beta, normFactorI;
	unsigned int		dimensions, i, j, k;

	VECTOR_OF_F_VECTORS     *adaptedMeans;

	dimensions = ubmMeans[0]->numElements;

	logProb = (VECTOR_OF_F_VECTORS *) calloc (numVectors, sizeof (VECTOR_OF_F_VECTORS));
	eeta    = (F_VECTOR *) calloc (numClusters, sizeof (float));

	for (i = 0; i < numVectors; i++)
	   {
	      	logProb[i] = (F_VECTOR *) AllocFVector (numClusters);
		for (j = 0; j < numClusters; j++)
		  {
			logProb[i]->array[j] = ComputeProbability (ubmMeans[j], ubmVars[j], ubmWeights[j], 
								   featureVectors[i], probScaleFactor);
			if (j)
			  normFactorI = LogAdd (normFactorI, logProb[i]->array[j]);
			else
			  normFactorI = logProb[i]->array[j];								 
		  }
	        for (j = 0; j < numClusters; j++)
		  {
			logProb[i]->array[j] = logProb[i]->array[j] - normFactorI;
		  }
		for (j = 0; j < numClusters; j++)
		  {
			if (i)
				eeta[j] = LogAdd(eeta[i], logProb[i]->array[j]);
			else
				eeta[j] = logProb[i]->array[j];
		  }
	   }
        for (j = 0; j < numClusters; j++)
	   {
		eeta[j] = exp (eeta[j]);
		for (k = 0; k < dimensions; k++)
   		  {
			for (i = 0; i < numVectors; i++)
			  {
				if (i)
				expectation[j]->array[k] = log(featureVectors[i]->array[k]) 
							 + logProb[i]->array[k];
			        else
				expectation[j]->array[k] = LogAdd (expectation[j]->array[j], 
							   log(featureVectors[i]->array[k]) 
							 + logProb[i]->array[k]);

			  }
			 expectation[j]->array[k] -= eeta[j];
		  }
	   }
	
	adaptedMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
	for (j = 0; j < numClusters; j++)
  	  {
	    adaptedMeans[j] = (F_VECTOR *) AllocFVector (dimensions);
	    
	    alpha = eeta[j] - LogAdd (eeta[j],(float)relevanceFactor);
	    beta = log ((float) relevanceFactor) - LogAdd (eeta[j], (float)relevanceFactor);
	    for (k = 0; k < dimensions; k++)
	      {
		adaptedMeans[j]->array[k] = expf (LogAdd (alpha + expectation[j]->array[k],
						    beta + ubmMeans[j]->array[k]));
	      }
	  }
	return adaptedMeans;
  }

main (int argc, char *argv[])
  {
	char		*controlFileName, *ubmFileName, *featureName, *modelFileName, *trainFileName;
	FILE		*controlFile, *ubmFile, *modelFile;
	VECTOR_OF_F_VECTORS *vfv, *ubmMeans, *ubmVars, *adaptedMeans;
	F_VECTOR	    *ubmWeights;
	ASDF 		    *asdf;
	unsigned int	numVectors, ubmSize, featLength, i, j;
	float 		thresholdScale;
	if (argc != 8)
	  {
		printf ("Usage : %s controlFile ubmFile ubmSize wavName featureName modelFile thresholdScale\n", argv[0]);
		exit (0);
	  }
	
   	controlFileName = argv[1];
	ubmFileName     = argv[2];
	sscanf (argv[3], "%u", &ubmSize);
	trainFileName   = argv[4];
	featureName     = argv[5];
	modelFileName   = argv[6];
	sscanf (argv[7], "%u", &thresholdScale);
	
	controlFile = fopen (controlFileName, "r");
	asdf = (ASDF *) calloc (1, sizeof (ASDF));
	InitializeStandardFrontEnd (asdf, controlFile);		
	Cstore (asdf->fftSize);
	
	GsfOpen (asdf, trainFileName);
	vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors (asdf, featureName, &numVectors, thresholdScale);
	featLength = vfv[0]->numElements;

	ubmMeans = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	ubmVars  = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	
	for (i = 0; i < ubmSize; i++)
	  {
		ubmMeans[i] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
		ubmVars[i]  = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
	  }
	
	ubmWeights = (F_VECTOR *) AllocFVector (ubmSize);

	ubmFile = fopen (ubmFileName, "r");
	for (i = 0; i < ubmSize; i++)
	  {
		fscanf (ubmFile, "%f\n", &ubmWeights->array[i]);
		for (j = 0; j < featLength; j++)
		{
			fscanf (ubmFile, " %f %f", &ubmMeans[i]->array[j], &ubmVars[i]->array[j]);
		}
		fscanf (ubmFile, "\n");
	  }
//	fclose (ubmFile);

	adaptedMeans = (VECTOR_OF_F_VECTORS *) AdaptOnlyMeans (ubmMeans, ubmVars, ubmWeights->array, 
								ubmSize, vfv, numVectors, 16,
								asdf->probScaleFactor);
	modelFile = fopen (modelFileName, "w");
	
	for (i = 0; i < ubmSize; i++)
	  {
		fprintf (modelFile, "%f\n", ubmWeights->array[j]);
		for (j = 0; j < featLength; j++)
		  {
			fprintf (modelFile, " %f %f", adaptedMeans[i]->array[j], ubmVars[i]->array[j]);
		  }
		fprintf (modelFile, "\n");
	  }
	fclose (modelFile);	
  }

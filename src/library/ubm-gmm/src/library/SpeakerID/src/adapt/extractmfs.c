// extractmfs - extracts mfs features with delta and double delta (if
// necessary). extracts features irrespective of vad decisions, computes
// delta (and accl) and then performs vad. does stg if asked to.
//
//

#include "CommonFunctions.h"

void printUsage() {
    printf ("extractmfs [options]\n");
    printf ("v: vadtype 0 = none, 1 = threshold, 2 = tgmm, 3 = transcript\n");
    printf ("t: threshold scale\n");
    printf ("b: transcript name\n");
    printf ("g: trigaussian file\n");
    printf ("d: compute delta\n");    
    printf ("a: compute acceleration\n");
    printf ("c: control file\n");
    printf ("i: input wave file\n");
    printf ("o: output wavefile\n");
    printf ("r: use this to set channel\n");
    printf ("s: save output in binary format\n");
    return;
}

int main (int argc, char *argv[]) {
    FILE *cfile = NULL, 
         *ofile = NULL,
         *controlfile = NULL;
    char *cfilename = NULL,
         *vadfilename = NULL,
         *ipfilename = NULL,
         *opfilename = NULL;
    short int vel = 0, 
              accl = 0, 
    // vadtypes = 0 for none
    //            1 for energy based
    //            2 for tgmm 
    //            3 for transcription
              vadtype = 0, 
              vadflag = 0;
    int oc,i,j,k,velwnd,acclwnd,stgwnd,dostg, channel_set = 0,
        save_as_binary = 0, add_erg = 0;
    unsigned long numvectors, numSpeechVectors;
    unsigned int *idx = NULL;
    float threshold = 0.0;
    ASDF *asdf = NULL;
    VFV *vfv, *deltavfv, *acclvfv, *nvfv, *concatvfv, *erg_vfv;
    while ((oc = getopt (argc,argv,"hsv:g:t:b:dac:i:o:r:e")) != -1) {
        switch (oc) {
              case 'h':
                  printUsage ();
                  exit (0);
                  break;
              case 'd':
                  vel = 1;
                  break;
              case 'a':
                  accl = 1;
                  break;
              case 'c':
                  cfilename = optarg;
                  break;
              case 'v':
                  sscanf (optarg, "%d", &vadtype);
                  break;
              case 't':
                  sscanf (optarg,"%f", &threshold);
                  break;
              case 'g':
                  vadfilename = optarg;
                  break;
              case 'b':
                  vadfilename = optarg;
                  break;
              case 'i':
                  ipfilename = optarg;
                  break;
              case 'o':
                  opfilename = optarg;
                  break;
              case 'r':
                  set_select_channel(optarg[0]);                  
                  channel_set = 1;
                  break;
              case 's':
                  save_as_binary = 1;
                  break;
              case 'e':
                  add_erg = 1;
                  break;
              default:
                  printf ("Invalid option found ... \n");
                  break;
        }
    }

    if (cfilename == NULL || ipfilename == NULL | opfilename == NULL) {
        printf ("Control file, input file and output file arguments are ");
        printf ("mandatory\n");
        printUsage();
        exit (1);        
    }

    if (!channel_set) set_select_channel('0');
    // Extract all feature vectors
    controlfile = fopen (cfilename, "r");
    asdf = (ASDF *) calloc (1, sizeof (ASDF));
    if (asdf == NULL) {
        printf ("Unable to allocate memory for asdf\n");
        exit(2);
    }
    InitializeStandardFrontEnd (asdf, controlfile);
    Cstore (asdf->fftSize);    
    dostg = asdf->stGauss;
    stgwnd = asdf->stGaussWnd;
    asdf->stGauss = 0;
    vfv = ExtractFeatureVectors(asdf,ipfilename,"frameSlope",&numvectors,
                                0.0);
    
    velwnd = GetIAttribute (asdf,"deltaDifference");
    acclwnd = GetIAttribute (asdf,"deltaDeltaDifference");
    if (vel || accl) {
        deltavfv = ComputeDeltaVectors(vfv, (unsigned long)numvectors, velwnd); 
        if (acclvfv) 
            acclvfv = ComputeDeltaVectors(deltavfv, (unsigned long)numvectors,
                                          acclwnd);            
        if (vel) {
            concatvfv = ConcatVfv(vfv, deltavfv, numvectors);
            if (accl)
                concatvfv = ConcatVfv(concatvfv,acclvfv,numvectors);
        }
        else if (!vel && accl)
            concatvfv = ConcatVfv(vfv, acclvfv, numvectors);
        FreeVfv (vfv,numvectors);
        FreeVfv (deltavfv,numvectors);
        if (accl) FreeVfv (acclvfv,numvectors);
        vfv = concatvfv;
    }    

    idx = GetVADDecisions(asdf,
                          ipfilename,
                          vadtype,
                          threshold,
                          vadfilename,
                          &numSpeechVectors);
    if (add_erg) {
        erg_vfv = ExtractFeatureVectors(asdf, ipfilename, "frameDeltaLogEnergy", 
                                        &numvectors, 0.0);
        nvfv = ConcatVfv(vfv, erg_vfv, numvectors);
        FreeVfv(vfv, numvectors);
        FreeVfv(erg_vfv, numvectors);
        vfv = nvfv;
    }
    if (idx != NULL) {
        nvfv = (VFV *) malloc (sizeof(VFV)*numSpeechVectors);
        j = -1;
        finc(i,numSpeechVectors,1) nvfv[i] = vfv[idx[i]];
        free (vfv);
        vfv = nvfv;
    }
    if (dostg) {
        printf ("calling gaussianization\n");
        nvfv = Gaussianization (vfv, numSpeechVectors, stgwnd);
        FreeVfv (vfv,numSpeechVectors);
        vfv = nvfv;
    }
    if (!save_as_binary)
        WriteVfvToFile (vfv,numSpeechVectors,opfilename);
    else
        // ignoring the return value -- dangerous
        WriteVfvToBinFile (vfv, numSpeechVectors, opfilename);  
    free (asdf);
    FreeVfv(vfv,numSpeechVectors);
}

% A is the lda projn matrix
% B is wccn matrix
function [A, B, Ds] = ldatrain (datamat, idvec, psize, minClassEx)
ids = unique (idvec);
ivecsize = size(datamat,1);
N = size(datamat,2);
S = length (ids)
Sb = zeros (ivecsize);
Sw = zeros (ivecsize);
ignored = 0;
for i = 1:S
    j = ids(i);
    indices = find (j == idvec);
    n_s = length (indices);
    if n_s <  minClassEx
      ignored = ignored + 1;
      continue
    end
    w_mean = mean (datamat(:,indices),2);
    tempSw = zeros (ivecsize);
    Sb = Sb + w_mean * w_mean';
    for n = 1:n_s
     w = datamat (:,indices(n)) ;
     w = w - w_mean;
     tempSw = tempSw + (w * w');
    end
    tempSw = tempSw / n_s;
    Sw = Sw + tempSw;    
end  
Sw_inv = inv (Sw);
[V D] = eig (Sw_inv * Sb);
D = diag (D);
[Ds idx] = sort (D,'descend');
A = V(:,idx(1:psize));

W = Sw_inv * (S - ignored);
B = chol (W);
ignored

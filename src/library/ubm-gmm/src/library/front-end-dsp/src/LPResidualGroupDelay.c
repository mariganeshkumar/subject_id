/**************************************************************************
 *  $Id: LPResGroupDelay.c,v 1.4 2001/01/25 11:53:03 hema Exp hema $
 *  File:	LPResGroupDelay.c - Computes the group delay function of
 *               the residual of a signal.
 *
 *  Purpose:	Used to compute the group delay function of the LP
 *               Residual
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Thu 20-Feb-2014 14:33:55 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average group delay function 
        from the residual of a given speech utterance and saves it in 
*	a file. 
*	Inputs :
*	Speech data input file
        Group Delay output file
*	Output :
*       GroupDelay  data are written to a file 

*******************************************************************************/       void Usage() {
  printf("Usage : LPResGroupDelay ctrlFile waveFile normalisedFilteredSigOutFile GroupDelayFile \n");
}
/*****************************************************************************/

       int main (int argc, char *argv[])
{ 
	int  	        i, frameSize,
	                frameNum, frameShift,
	                numFrames;
        long            numSamples;
	F_VECTOR        *signal=NULL, *residualGdelay=NULL;
        FILE            *gdFile=NULL, *controlFile=NULL, *sigFile=NULL;
	float           rmax;
	static ASDF     *asdf;
        
/******************************************************************************/
       if (argc != 5) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       numSamples = (int) GetIAttribute(asdf, "numSamples");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       rmax = asdf->waveform[ImaxShort0(asdf->waveform, numSamples)];
       sigFile = fopen(argv[3],"w");
       gdFile = fopen(argv[4],"w");
       if (gdFile == NULL) {
	 printf("cannot open files \n");
	 exit(-1);
       }
       printf ("numFrames = %d\n", numFrames);
       //       for (i = 1; i<=filterOrder/2; i++)
       //fprintf(sigFile, "%f\n", 0.0);
       for (frameNum = 0; frameNum < numFrames; frameNum++) {	
	 signal = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	 residualGdelay = (F_VECTOR *) GsfRead(asdf,frameNum,
					       "frameResidualGDelay");
	 if (signal != NULL)
	   for (i = 0; i < frameShift; i++) {
	     fprintf(gdFile, "%e\n",residualGdelay->array[i]);
	     fprintf(sigFile, "%e\n", signal->array[i]/rmax);
	   }
	 else
	   for (i = 1; i <= frameShift; i++) {
	     fprintf(gdFile, "%e\n", 0.0);
	     fprintf(sigFile, "%e\n",0.0);
	   }
	 if (residualGdelay != NULL) {
	   free (residualGdelay->array);
	   free(residualGdelay);
	   free (signal->array); 
	   free(signal);
	 }

       }
       fclose(gdFile);
       fclose(sigFile);
       return 0;
}
/**************************************************************************
 * $Log: LPResGroupDelay.c,v $
 * Revision 1.4  2001/01/25 11:53:03  hema
 * Modified Usage
 *
 * Revision 1.3  2000/04/28 01:58:15  hema
 * fixed the bug in median filtering
 *
 * Revision 1.2  2000/04/27 01:59:30  hema
 * modified group delay averaging
 *
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of GroupDelay.c
 **************************************************************************/




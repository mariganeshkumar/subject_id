/*
    This is a library file for gsl_helper.c

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#ifndef __MY_GSL_HELPER__
#define __MY_GSL_HELPER__
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_permutation.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_rng.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<string.h>
#include<unistd.h>
#include<libgen.h>
#define HAVE_INLINE

#define FILE_OPEN_FAILED 1
#define FILE_OPEN_SUCCESS 0
#define INCOMPATIBLE_INT_SIZE 2
#define INCOMPATIBLE_FLOAT_SIZE 4
#define MEM_ALLOC_FAILED 8

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

typedef struct fostat_ {
  int *eeta;
  gsl_matrix *f;
  int nmix;
  int dim;
}fostat;

typedef struct flists_ {
  char **line;
  int num_lines;
}flist;

typedef struct gt_ {
  char **line;
  int *modelno, num_lines;
}gt;

gsl_matrix*
read_matrix(char *filename, int m, int n);
gsl_matrix*
read_matrix_from_binfile(char *filename) ;
int
write_matrix(char *filename, gsl_matrix *mat); 

// ************ added on feb 13, 2015 ***********
gsl_vector* read_vector(char *filename, int m);
int write_vector(char *filename, gsl_vector *vec);
double add_vec_elem(gsl_vector *vec,int len);
gsl_matrix* write_vectors_to_matrix(gsl_vector *vec, gsl_matrix *mat, int cols);
gsl_matrix* mat_elemwise_mul(gsl_matrix *mat1, gsl_matrix *mat2, gsl_matrix *mat3);
gsl_vector* vec_vec_elemwise_div(gsl_vector *vec1, gsl_vector *vec2, gsl_vector *vec3, int rows);
gsl_vector* mat_add_rows(gsl_matrix *mat, gsl_vector *vec);
gsl_vector* vector_elem_sqrt(gsl_vector *vec1,gsl_vector *vec2);
gsl_matrix* elem_by_elem_binMinus(gsl_matrix *mat1, gsl_matrix *mat2, gsl_matrix *mat3);
gsl_matrix* vector_transpose(gsl_vector* v1,gsl_matrix* mat);
void print_matrix(gsl_matrix *mat);
void print_vector(gsl_vector *vec,int size);
// **********************************************
void
copyUpperToLower(gsl_matrix *mat);
void
translateDiagonal(gsl_matrix *mat, double d);
gsl_matrix*
invertMatrix(const gsl_matrix *A);

fostat*
read_fostat(char *fname);
void
free_fostat(fostat *spkr_stats);
fostat*
read_fostat_bin(char *fname);

float
read_float_from_binfile(FILE *f);
int
write_fostat_to_binfile(char *fname, char *opname);
void
write_fostat(char *fname, fostat *spkr_stats) ;

int
read_int_from_binfile(FILE *f);
void
write_float_to_binfile(float x, FILE *f);
flist*
read_list_of_files(char *fname);
void
free_flist(flist *list);
char*
basename_without_extn (char *path);
int
is_substr(const char *ref, const char *superstr) ;
void
write_int_to_binfile(int x, FILE *f) ;
int 
get_index_in_flist(flist *list, const char *fname);
int
in_array(int *values, int len, int key);
int swap(gt *, int, int);
void gt_quickSort(gt *, int, int);
#endif

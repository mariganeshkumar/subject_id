function [w m s] = ReadGMM (fname, nmix, dim)
  f = fopen (fname, 'r');
  w = zeros (nmix,1);
  m = zeros (nmix,dim);
  s = zeros (nmix,dim);
  for i = 1:nmix
    w(i) = fscanf (f, '%e',1);
    mv = fscanf (f, '%e', 2*dim);
    m(i,:) = mv (1:2:2*dim);
    s(i,:) = mv (2:2:2*dim);
  end
  fclose (f);

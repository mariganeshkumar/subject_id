function [] = train_and_test_channels(config)
path=genpath('library');
addpath(path);

if ~exist('config','var')
    config = configuration;
end
disp(config)
feature_dir = config.features_dir;

feature_code = [num2str(config.val_split),...
    '_',num2str(config.feature)];



freq_dim = 0;
num_channels = length(config.lobe_map{config.lobe});
for k=2:num_channels
    channel_combinations = combnk(1:num_channels,k);
    mkdir([config.history_save,...
        '/',config.feature_name{config.feature},...
        '/',config.classifier_name{config.classifier},...
        '/',num2str(k),'_channel']);
    fileID = fopen([config.history_save,...
        '/',config.feature_name{config.feature},...
        '/',config.classifier_name{config.classifier},...
        '/',num2str(k),'_',num2str(config.val_split),'_channel',...
        'performance_summary.txt'],'w');
    for chan_com = 1: size(channel_combinations,1)
    	try
        chan_com_name = '';
        for i = 1:length(channel_combinations(chan_com,:))
            chan_com_name = [chan_com_name,num2str(channel_combinations(chan_com,i)),'_'];
        end
        %get all the subjects in the feature directory
        subjects = get_all_sub_dir([feature_dir,'/',feature_code]);
        intersession_subjects=0;
        
        subjectsInfo={};
        sessionInfo=[];
        for i = 1:length(subjects)
            %get all the session for a given subject
            session = get_all_sub_dir([...
                feature_dir,...
                '/',feature_code,...
                '/',num2str(subjects{i})
                ]);
            no_of_sessions=length(session);
            random_splits_folder = [config.history_save,...
                '/',num2str(config.feature),...
                '/random_sessions_ordering',...
                '/',num2str(subjects{i}),...
                '/'
                ];
            if exist(random_splits_folder,'dir') ~= 7
                mkdir(random_splits_folder)
            end
            random_splits_filename = [random_splits_folder,'/channel_search.txt'];
            if exist(random_splits_filename, 'file') == 2
                random_permutation = dlmread(random_splits_filename, ' ');
            else
                random_permutation = 1:no_of_sessions;
                dlmwrite(random_splits_filename,random_permutation,' ');
            end
            
            is_ubm_data =0;
            if no_of_sessions <= 1
                is_ubm_data = 1;
                continue;
            elseif no_of_sessions == 2
                intersession_subjects = intersession_subjects+1;
                train_sessions = 1;
                subjectsInfo{intersession_subjects}=subjects{i};
                sessionInfo(intersession_subjects) = no_of_sessions;
            else
                intersession_subjects = intersession_subjects+1;
                train_sessions = floor(no_of_sessions * 0.7);
                subjectsInfo{intersession_subjects}=subjects{i};
                sessionInfo(intersession_subjects) = no_of_sessions;
            end
            
            disp(['Loading data for subject: ',num2str(subjects{i}),';  ',num2str(intersession_subjects),'/',num2str(no_of_sessions)])
            
            for j = 1:no_of_sessions
                recordings = dir([...
                    feature_dir,...
                    '/',feature_code,...
                    '/',num2str(subjects{i}),...
                    '/',session{j},...
                    '/*.mat'
                    ]);
                recordings = {recordings(:).name};
                rind=1;
                for l = 1:length(recordings)
                    data = load([...
                        feature_dir,...
                        '/',feature_code,...
                        '/',num2str(subjects{i}),...
                        '/',session{j},...
                        '/',recordings{l}
                        ]);
                    if size(data.feature,1) ~= length(config.lobe_map{config.lobe})
                        disp('wrong logic')
                    end
                    data = data.feature;
                    data = data(channel_combinations(chan_com,:),:,:);
                    freq_dim = size(data,2);
                    if is_ubm_data
                        %allData.ubm_data{i}{j}{rind} = data;
                    elseif ~is_ubm_data && random_permutation(j) <= train_sessions
                        allData.ubm_data{intersession_subjects}{j}{rind} =  data;
                        allData.train_data{intersession_subjects}{j}{rind} = data;
                    elseif ~is_ubm_data
                        allData.test_data{intersession_subjects}{j}{rind} = data;
                    end
                    rind = rind + 1;
                end
            end
        end
        
        allData.freq_dim = freq_dim;
        allData.num_channels = k;
        modelSaveDir = [config.history_save,...
            '/',config.feature_name{config.feature},...
            '/',config.classifier_name{config.classifier},...
            '/',num2str(k),'_channel',...
            '/', chan_com_name];
        allData.modelSaveDir = modelSaveDir;
        allData.plotSaveDir = modelSaveDir;
        allData.sessionInfo = sessionInfo;
        disp(['Total Number Of Intersession Subjects:', num2str(intersession_subjects)]);
        disp(['Total Number Of Sessions:', num2str(sum(sessionInfo))]);
        [results]=config.classifier_function{config.classifier}(config,allData);
        save([modelSaveDir,'/',num2str(config.val_split),'_score.mat'],'results');
        val_predicted_label=results.val_predicted_label;
        val_gt_label=results.val_gt_label;
        test_predicted_label = results.test_predicted_label;
        test_gt_label = results.test_gt_label;
        
        clear allData;
        cm = confusionmat(test_predicted_label, test_gt_label);
        test_accuracy = sum(diag(cm))/sum(sum(cm)) * 100;
        
        disp(['Trail Accuracy ', num2str(test_accuracy)]);
        cm = confusionmat(val_predicted_label, val_gt_label);
        val_accuracy = sum(diag(cm))/sum(sum(cm)) * 100;
        save_dir=[modelSaveDir,'/',num2str(config.val_split),'_'];
        
        
        test_eer = plot_score_distribution([save_dir,'test'],results.target_test_scores, results.non_target_test_scores);
        val_eer = plot_score_distribution([save_dir,'val'],results.target_val_scores, results.non_target_val_scores);
        fprintf('EER is %.2f\n', test_eer);
        
        fprintf(fileID,[chan_com_name,' ', num2str(test_accuracy), ' ', num2str(test_eer), '\n']);
        test_norm_eer = plot_score_distribution([save_dir,'test_norm'],results.target_test_norm_scores, results.non_target_test_norm_scores);
        val_norm_eer = plot_score_distribution([[save_dir,'val_norm']],results.target_test_norm_scores, results.non_target_test_norm_scores);
        fprintf('Normalised EER is %.2f\n', test_eer);
        
        
        result_str = strcat(config.history_save,...
            '/',config.feature_name{config.feature},...
            '/',config.classifier_name{config.classifier},...
            '/',num2str(k),'_channel',...
            '/',chan_com_name,...
            '/report_val_segment',num2str(config.val_split),'_Fe_',...
            config.feature_name{config.feature},'_Clas_',...
            config.classifier_name{config.classifier},'_inter.txt');
        WriteReport(result_str, val_accuracy, val_predicted_label,...
            val_gt_label,val_eer,val_norm_eer,subjectsInfo);
        
        result_str = strcat(config.history_save,...
            '/',config.feature_name{config.feature},...
            '/',config.classifier_name{config.classifier},...
            '/',num2str(k),'_channel',...
            '/',chan_com_name,...
            '/report_test_segment',num2str(config.val_split),'_Fe_',...
            config.feature_name{config.feature},'_Clas_',...
            config.classifier_name{config.classifier},'.txt');
        WriteReport(result_str, test_accuracy, test_predicted_label,...
            test_gt_label,test_eer,test_norm_eer,subjectsInfo);
        catch
        end
    end
    fclose(fileID)
end
end
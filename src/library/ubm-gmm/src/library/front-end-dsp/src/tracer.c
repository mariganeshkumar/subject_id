#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: Tracer.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	Tracer.c - Smoothes the pitch trajectory.
 *
 *  Purpose:	Smooth pitch contours for music
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Apr-2012 09:12:15
 *
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
#include <stdio.h>

#define MAXPTS 260
#define MAX_FRAMES 70
static   int             **FormantLocs=NULL;
static   int             *edx=NULL, *edy=NULL;
void Usage() {
  printf("Usage : Tracer ctrlFile inWaveFile InPitchFile OutPitchFile\n");
}
/*****************************************************************************/


int NeighbourNotFound(int *temp) {
  int     i, flag;
    flag = 1;
  for ( i= 0; i <=  7; i++) 
    if (temp[i] == 1) 
      flag =  0;
  return(flag);
}


void  SetNeighbours(int  *temp, int dir,
		    int  *xloc, int *yloc, int *length) {
  *length = *length+1;
  switch(dir) {
  case 1 : {
      *xloc = *xloc-1;
      *yloc = *yloc+1;
    }
  case 2 : {
      *xloc = *xloc-1;
      *yloc = *yloc;
    }
  case 3 : {
      *xloc = *xloc-1;
      *yloc = *yloc-1;
    }
  case 4 : {
      *xloc = *xloc;
      *yloc = *yloc-1;
    }
  case 5 : {
      *xloc = *xloc+1;
      *yloc = *yloc-1;
    }
  case 6 : {
      *xloc = *xloc+1;
      *yloc = *yloc;
    }
  case 7 : {
      *xloc = *xloc+1;
      *yloc = *yloc+1;
    }
  case 0 : {
      *xloc = *xloc;
      *yloc = *yloc+1;
    }
  }
  temp[0] = FormantLocs[*xloc][*yloc+1];
  temp[1] = FormantLocs[*xloc-1][*yloc+1];
  temp[2] = FormantLocs[*xloc-1][*yloc];
  temp[3] = FormantLocs[*xloc-1][*yloc-1];
  temp[4] = FormantLocs[*xloc][*yloc-1];
  temp[5] = FormantLocs[*xloc+1][*yloc-1];
  temp[6] = FormantLocs[*xloc+1][*yloc];
  temp[7] = FormantLocs[*xloc+1][*yloc+1];
  edx[*length] = *xloc;
  edy[*length] = *yloc;
  FormantLocs[*xloc][*yloc] = 5;
  printf("edx[%d]= %d  edy[%d]= %d",*length, edx[*length], *length, edy[*length]);
}

  int main(int argc, char **argv) {
  int             i,j, k;
  int             direction,curX, curY,length,incr;
  int            frameIndex, threshold, nfBy2, prevFreqIndex, freqIndex;
  int            neighbours[9];
  int            first, found, neighbourNotFound;
  int            fftSize, samplingRate, numFrames;
  FILE           *controlFile=NULL, *inPthFile=NULL, *outPthFile=NULL, *binaryPthFile=NULL;
  float          timeIndex, pitchValue;
  char           line[200];
  static ASDF    *asdf;

  if ((int)(argc) != 5) {
    Usage();
    exit(-1);
  }

  controlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  GsfOpen(asdf, argv[2]);
  inPthFile = fopen(argv[3],"r");
  outPthFile = fopen(argv[4],"w");
  fftSize  = (int) GetIAttribute(asdf, "fftSize");
  numFrames = (int) GetIAttribute(asdf, "numFrames");
  samplingRate = (int) GetIAttribute(asdf, "samplingRate");
  threshold = (int) GetIAttribute(asdf, "medianOrder");
  printf ("fftSize = %d numFrames=%d samplingRate = %d\n", 
	  fftSize, numFrames, samplingRate);
  nfBy2 = fftSize/2;
  frameIndex = 0;

  FormantLocs =  malloc((nfBy2+1) * sizeof(int *));
  edx = (int *) calloc(numFrames, numFrames*sizeof(int));
  edy = (int *) calloc(numFrames, numFrames*sizeof(int));
  for (i = 0; i < nfBy2; i++) 
    FormantLocs[i] =  malloc((numFrames+1) * sizeof(int));
  for (i = 0; i < nfBy2; i++)
    for (j = 0; j < numFrames; j++) 
      FormantLocs[i][j] = 0;
  binaryPthFile = fopen("PitchPicture.txt", "w");
  while (fgets(line, 200, inPthFile))
    {
      sscanf(line, "%f %f", &timeIndex,  &pitchValue);
      if (pitchValue != 0.0) {
	freqIndex = (int) ((pitchValue/samplingRate)*fftSize);
	//	if ((prevFreqIndex == freqIndex/2) || (prevFreqIndex == freqIndex*2) ||
	//  (prevFreqIndex == freqIndex/2+1) || (prevFreqIndex == freqIndex*2+1))
	//  freqIndex = prevFreqIndex;
	FormantLocs [freqIndex][frameIndex] = 1;
      frameIndex++;
      }
    }
      
  /*  for (i = nfBy2-1; i >= 0; i--) 
    {
      fprintf(binaryPthFile, "\n");
      for (j = 0; j < numFrames; j++) 
	fprintf(binaryPthFile, "%d", FormantLocs[i][j]);
    }
    fclose(binaryPthFile); */
  i = 1;
  while (i < nfBy2-1) {
    j = 1;
    while (j <  numFrames-2)
      {
	neighbours[0] = FormantLocs[i][j+1];
	neighbours[1] = FormantLocs[i-1][j+1];
	neighbours[2] = FormantLocs[i-1][j];
	neighbours[3] = FormantLocs[i-1][j-1];
	neighbours[4] = FormantLocs[i][j-1];
	neighbours[5] = FormantLocs[i+1][j-1];
	neighbours[6] = FormantLocs[i+1][j];
	neighbours[7] = FormantLocs[i+1][j+1];
	if ((FormantLocs[i][j] == 1) && (neighbours[4] == 0))
	  {
	    printf("forlocs loop\n");
	    printf("pixel at [%d %d]= %d neighbour4 %d",i,j,FormantLocs[i][j],neighbours[4]);
	    printf("first= %d found= %d\n",first, found);
	    direction = 6;
	    first = 1;
	    curX = i;
	    curY = j;
	    length = 1;
	    edx[length] = curX;
	    edy[length] = curY;
	    neighbourNotFound = 0;
	    
	    //	    while (((curX != i) || (curY != j)) || (first == 1)) 
	    while ((neighbourNotFound == 0) && (first == 1))
	      {
		found = 0;
		incr = 0;
		/*		  printf("i=%d j=%d found=%d first=%d \n",i, j, found, first);
		  printf("curX=%d curY=%d \n",curX, curY);
		  for (k = 0; k <= 7; k++)
		  printf("neighbours[',%d,']= %d\n",k, neighbours[k]); */
		//		while ((found == 0) && (incr < 3))
		while ((neighbourNotFound == 0) && (curX < nfBy2-1) && (curY < numFrames-1))
		  {
		    incr++;
		    printf("direction: %d\n",direction);
		    if (neighbours[direction-1] == 1)
		      {
			printf("len_dir-1:%d\n",length);
			SetNeighbours(neighbours,direction-1, &curX, &curY,&length);
			direction = direction-2;
			if (direction < 0)
			  direction = direction+8;
			//found = 1;
			
		      }
		    else if (neighbours[direction] == 1) 
		      {
			printf("len_dir:%d\n",length);
			SetNeighbours(neighbours,direction, &curX, &curY, &length);
			//found = 1;
		      }
		    else if (neighbours[direction+1] == 1)
		      {
			printf("len_dir+1:%d\n",length);
			SetNeighbours(neighbours,direction+1, &curX, &curY, &length);
			//found = 1;
		      }
		    else if(NeighbourNotFound(neighbours)) {
		      neighbourNotFound = 1;
		    /*		      found = 1;
                      curX = i;
		      curY = j; */
		    }
		    else {
		      direction = direction+2;
		      if (direction >= 8) 
			direction = direction-8;
		    }
		  }
		first = 0;
	      }
	    printf ("\n");
	    printf("length= %d threshold= %d", length, threshold);
	    if (length < threshold) 
	      for (k = 1; k<= length; k++) 
		FormantLocs[edx[k]][edy[k]] = 0;
	  }
	j++;// j = curY+1;
      }
    i++;
  }
  printf("\n final forlocs \n");
  for (i = 1; i < nfBy2; i++) 
    {
      for (j = 1; j < numFrames; j++) 
	if (FormantLocs[i][j] == 1)
	  fprintf(binaryPthFile, "%d %d \n", j, i);
    }
  fclose(binaryPthFile);
  return 0;
}


% compute A matrix for all mixtures

function computeA (amatname, fxlist, lxlist, M, D, R, exflag) 
    fxl = readlistascell (fxlist);
    lxl = readlistascell (lxlist);

    ii = length (lxl)
    for m =1:M
      Afl{m} = zeros(R);
    end

    N = zeros (M,ii);
    for i=1:ii 
      try
        f = fopen (fxl{i},'r');
        temp = fscanf (f, '%d', 1);
        temp = fscanf (f, '%d', 1);
        N(:,i) = fscanf (f, '%d', M);
        fclose (f);
      catch
        continue
      end
    end
    for i=1:ii
      try
        load (lxl {i}, '-mat');
        Nsub = N(:,i);
        contributions = unique (Nsub);
        contributions = contributions(contributions>0);
        for nval_idx = 1:length(contributions)
            nval = contributions(nval_idx);
            addThis = nval * L;
            idx = find(nval == Nsub);
            for j = 1:length(idx)
                m = idx(j);
                Afl{m} = Afl{m} + addThis;
            end
        end
      catch 
        continue
      end
    end
    amatpref = strcat (amatname, '.');
    for m = 1:M
        Ax = Afl{m};
        save(strcat (amatpref,int2str(m)), '-mat','Ax');
    end

    if exflag ~=0 
      exit
    end

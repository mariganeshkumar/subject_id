#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
main() {
  float minFrequency, maxFrequency, warpedFreq, warpConst;
  int i;
  FILE *fp;
  printf("Input min and max frequency warpconst:");
  scanf ("%f %f %f", &minFrequency, &maxFrequency, &warpConst);
  fp  = fopen ("WarpFreq.txt", "w");
  rewind(fp);
  for (i = (int) minFrequency; i <= (int) maxFrequency; i++) {
    warpedFreq = Warp ((float) i, minFrequency, maxFrequency, warpConst);
    fprintf (fp, "%f %f\n", (float)(i), warpedFreq);
  }
  fclose(fp);
}

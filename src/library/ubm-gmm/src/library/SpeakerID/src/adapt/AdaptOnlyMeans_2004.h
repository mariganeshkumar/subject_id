#ifndef __AdaptMeans_2004_
#define __AdaptMeans_2004_
#include "CommonFunctions.h"

VECTOR_OF_F_VECTORS * AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
				    VECTOR_OF_F_VECTORS *ubmVars,
				    float *ubmWeights,
				    unsigned int numClusters,
			            VECTOR_OF_F_VECTORS *featureVectors,
				    unsigned int numVectors,
				    unsigned int relevanceFactor,
				    float probScaleFactor);
#endif

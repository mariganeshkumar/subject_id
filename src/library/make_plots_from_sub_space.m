function [] = make_plots_from_sub_space(tmpDir,savePrefix,sessionInfo,plotTrainData)

if ~exist('plotTrainData','var')
    plotTrainData = 0;
end
plotTrainData = 0;
data = load([tmpDir,'/sub_space_matrix.mat']);
trainTestSubSpaceVectors = [data.trainSubSpaceVectors;data.testSubSpaceVectors];
rng default % for reproducibility
train_test_d2=tsne(trainTestSubSpaceVectors,'distance','cosine');

train_d2 = train_test_d2(1:size(data.trainSubSpaceVectors,1),:);
test_d2 = train_test_d2(size(data.trainSubSpaceVectors,1)+1:end,:);
color = {[166,206,227],[31,120,180],[178,223,138],[51,160,44],[251,154,153],[227,26,28],[253,191,111], [255,127,0], [202,178,214], [106,61,154]};
marker = ['o','+','*','x','s','d', 'v', 'h', '^', 'p'];
for j=1:5
    h=figure();
    perm = randperm(length(unique(data.testLabel)));
    if length(unique(data.testLabel)) < 10
        num_subj = length(unique(data.testLabel));
    else
        num_subj = 10;
    end
    leg_sub=cell([1,num_subj]);
    leg_label=cell([1,num_subj]);
    for i=1:num_subj
        ind=perm(i);
        test_i_d2 = test_d2(data.testLabel==ind,:);
        train_i_d2 = train_d2(data.trainLabel==ind,:);
        if plotTrainData
            leg_train_test = scatter(train_i_d2(:,1),train_i_d2(:,2),'+','MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
            hold on;
            leg_sub{i} = scatter(test_i_d2(:,1),test_i_d2(:,2),'o','MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
            leg_label{i} = ['Subject ',num2str(i)];
        else
            leg_sub{i} = scatter(test_i_d2(:,1),test_i_d2(:,2),marker(mod(i,10)+1),'MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
            hold on;
            leg_label{i} = ['Subject ',num2str(i)];
        end
    end
    if plotTrainData
        leg_label{8} = 'Training';
        legend([leg_sub{:} leg_train_test], leg_label);
    else
        legend([leg_sub{:}], leg_label);
    end
    saveas(h,[savePrefix,'_subspace_rand_',num2str(j),'.png']);
    saveas(h,[savePrefix,'_subspace_rand_',num2str(j),'.svg']);
    saveas(h,[savePrefix,'_subspace_rand_',num2str(j),'.fig']);
end

[~,top_session_subject] = sort(sessionInfo, 'desc');

h=figure();
if length(unique(data.testLabel)) < 10
    num_subj = length(unique(data.testLabel));
else
    num_subj = 10;
end
leg_sub=cell([1,num_subj]);
leg_label=cell([1,num_subj]);
for i=1:num_subj
    ind=top_session_subject(i+1);
    test_i_d2 = test_d2(data.testLabel==ind,:);
    train_i_d2 = train_d2(data.trainLabel==ind,:);
    if plotTrainData
        leg_train_test = scatter(train_i_d2(:,1),train_i_d2(:,2),'+','MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
        hold on;
        leg_sub{i} = scatter(test_i_d2(:,1),test_i_d2(:,2),'o','MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
        leg_label{i} = ['Subject ',num2str(i)];
    else
        leg_sub{i} = scatter(test_i_d2(:,1),test_i_d2(:,2),marker(mod(i,10)+1),'MarkerEdgeColor',color{mod(i,10)+1}/255,'linewidth',1.5);
        hold on;
        leg_label{i} = ['Subject ',num2str(i)];
    end
end
if plotTrainData
    leg_label{8} = 'Training';
    legend([leg_sub{:} leg_train_test], leg_label);
else
    legend([leg_sub{:}], leg_label);
end
saveas(h,[savePrefix,'_top_inter.png']);
saveas(h,[savePrefix,'_top_inter.svg']);
saveas(h,[savePrefix,'_top_inter.fig']);

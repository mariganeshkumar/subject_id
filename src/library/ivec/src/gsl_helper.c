/*
    This file contains functions that are useful for reading, writing and
    manipulating gsl matrices

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"

gsl_matrix*
read_matrix(char *filename, int m, int n) 
{
    gsl_matrix *mat = NULL;
    float tempf;
    FILE *ipfile = NULL;
    int i,j;

    mat = gsl_matrix_alloc(m,n);
    if(mat == NULL) {
        fprintf(stderr,"Unable to allocate mem. read failed\n");
        return NULL;
    }
    
    ipfile = fopen(filename,"r");
    if(ipfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Read failed\n");
        gsl_matrix_free(mat);
        return NULL;
    }
    for(i=0;i<m;i++) {
        for(j=0;j<n;j++) {
            fscanf(ipfile,"%e", &tempf);
            gsl_matrix_set(mat,i,j,tempf);
        }
        fscanf(ipfile,"\n");
    }
    fclose(ipfile);
    return mat;
}

gsl_matrix*
read_matrix_from_binfile(char *filename) 
{
    gsl_matrix *mat = NULL;
    float tempf;
    FILE *ipfile = NULL;
    int i,j, m, n;

    
    ipfile = fopen(filename,"rb");
    if(ipfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Read failed\n");
        return NULL;
    }

    m = read_int_from_binfile(ipfile);
    n = read_int_from_binfile(ipfile);

    mat = gsl_matrix_alloc(m,n);
    if(mat == NULL) {
        fprintf(stderr,"Unable to allocate mem. read failed\n");
        return NULL;
    }
    
    for(i=0;i<m;i++) {
        for(j=0;j<n;j++) {
            gsl_matrix_set(mat,i,j,read_float_from_binfile(ipfile));
        }
    }
    fclose(ipfile);
    return mat;
}

int
write_matrix(char *filename, gsl_matrix *mat) {
    FILE *opfile = NULL;
    int m,n,i,j;

    if(mat == NULL) {
        fprintf(stderr,"matrix is null. write failed\n");
        return 1;
    }
    
    opfile = fopen(filename,"w");
    if(opfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Write failed\n");
        return 1;
    }

    m = mat->size1;
    n = mat->size2;
    for(i=0;i<m;i++) {
        for(j=0;j<n;j++) {
            fprintf(opfile,"%e ",gsl_matrix_get(mat,i,j));
        }
        fprintf(opfile,"\n");
    }
    fclose(opfile);
    return 0;
}

int
write_matrix_complex(char *filename, gsl_matrix_complex *mat) {
    FILE *opfile = NULL;
    int m,n,i,j;

    if(mat == NULL) {
        fprintf(stderr,"matrix is null. write failed\n");
        return 1;
    }
    
    opfile = fopen(filename,"w");
    if(opfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Write failed\n");
        return 1;
    }

    m = mat->size1;
    n = mat->size2;
    for(i=0;i<m;i++) {
        for(j=0;j<n;j++) {
            fprintf(opfile,"%e ",gsl_matrix_complex_get(mat,i,j));
        }
        fprintf(opfile,"\n");
    }
    fclose(opfile);
    return 0;
}

void print_matrix(gsl_matrix *mat)
{
	int i, j;
	int rows, cols;
	rows = mat->size1;
	cols = mat->size2;
	printf("\n Size of matrix is : %d by %d", rows, cols);
	for(i=0; i<rows; i++)
	{
		printf("\n");
		for (j=0;j<cols;j++)
			printf(" %e ",gsl_matrix_get(mat,i,j));
	}
}

void print_vector(gsl_vector *vec,int size)
{
	int i, rows;
	rows = size;
	for (i = 0; i<rows; i++)
		printf("\n %e", gsl_vector_get(vec,i));		
}
// ******************** added on 13 feb 2015 *********************
gsl_matrix*
write_vectors_to_matrix(gsl_vector *vec, gsl_matrix *mat, int cols) {
    int rows,i,j;
    double curVal = 0;
    if(vec == NULL) {
        fprintf(stderr,"vector is null. write failed\n");
        return NULL;
    }
    rows = mat->size1;
    printf("\n Rows: %d and Columns: %d in final matrix",rows,cols);
    gsl_matrix_set_col(mat, cols, vec);
    return mat;
}


gsl_matrix* 
mat_elemwise_mul(gsl_matrix *mat1, gsl_matrix *mat2, gsl_matrix *mat3){
	int m1,n1,m2,n2,i,j;
	double val;
	m1 = mat1->size1;
	n1 = mat1->size2;
	m2 = mat2->size1;
	n2 = mat2->size2;
	if( (m1!=m2) || (n1!=n2))
	{
		printf("Matrix A and B must be of same order for A.*B");
		return NULL;
	}
	printf("\n Element wise matrix multiplication: %d-by-%d and %d-by-%d \n",m1,n1,m2,n2);
	for(i=0;i<m1;i++){
		for(j=0;j<n1;j++){
			val = gsl_matrix_get(mat1,i,j)*gsl_matrix_get(mat2,i,j);
            		gsl_matrix_set(mat3,i,j,val);
//			printf("%e ",gsl_matrix_get(mat3,i,j));
		}
//		printf("\n");
	}
return mat3;
}

gsl_vector*
vec_vec_elemwise_div(gsl_vector *vec1, gsl_vector *vec2, gsl_vector *vec3, int rows){
        int i,j;
        double val;
        for(i=0;i<rows;i++){
            val = gsl_vector_get(vec1,i)/gsl_vector_get(vec2,i);
                        gsl_vector_set(vec3,i,val);
                }
return vec3;
}

gsl_vector* 
mat_add_rows(gsl_matrix *mat, gsl_vector *vec){
	int row,col,i,j;
	row = mat->size1;
	col = mat->size2;
	for(i=0;i<row;i++)
	{
		double sum = 0;
		for(j=0;j<col;j++)
		{
		   sum=sum+gsl_matrix_get(mat,i,j);
		}
		//printf("\n Sum of %d row: %e", i,sum);
		gsl_vector_set(vec,i,sum);
	}
return vec;
}

gsl_vector* 
vector_elem_sqrt(gsl_vector *vec1,gsl_vector *vec2){
	int row, i;
	double val;
	row = vec1->size;
	for(i=0;i<row;i++){
		val=sqrt(gsl_vector_get(vec1,i));
//		printf("\n sqrt of %e in vector index [%d] is %e", gsl_vector_get(vec1,i),i,val);
	        gsl_vector_set(vec2,i,val);
	}
return vec2;
} 

gsl_matrix* elem_by_elem_binMinus(gsl_matrix *mat1, gsl_matrix *mat2, gsl_matrix *mat3){
	int i,j,row1,cols1,row2,cols2;
	double elem1,elem2,diff;
	row1 = mat1->size1;
	cols1 = mat1->size2;
	row2 = mat2->size1;
	cols2 = mat2->size2;
	printf("\n wmat1Transp dim: %d-by-%d",row1,cols1);
	printf("\n wvect_trans dim: %d-by-%d",row2,cols2);
	for(i=0;i<row1;i++)
	{
	     for(j=0;j<cols1;j++)
	   	{
			elem1 = gsl_matrix_get(mat1,i,j);
			elem2 = gsl_matrix_get(mat2,0,j);
			diff = elem1 - elem2;
			gsl_matrix_set(mat3,i,j,diff);
   	        }
        }
return mat3;
}

gsl_matrix* vector_transpose(gsl_vector* v1,gsl_matrix* mat){
	int i, cols;
	cols = mat->size2;
	double val;
	for(i=0;i<cols;i++){
		val = gsl_vector_get(v1,i);
		gsl_matrix_set(mat,0,i,val);
	}
return mat;
}

double add_vec_elem(gsl_vector *vec,int len){
	double sum = 0;
	int i = 0;
	for(i=0;i<len;i++)
		sum = sum + gsl_vector_get(vec,i);
	return sum;
}

// ***************************************************************
int
write_matrix_to_binfile(char *filename, gsl_matrix *mat) {
    FILE *opfile = NULL;
    int m,n,i,j;

    if(mat == NULL) {
        fprintf(stderr,"matrix is null. write failed\n");
        return 1;
    }
    
    opfile = fopen(filename,"wb");
    if(opfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Write failed\n");
        return 1;
    }

    m = mat->size1;
    n = mat->size2;
    write_int_to_binfile(m, opfile);
    write_int_to_binfile(n, opfile);    
    for(i=0;i<m;i++) {
        for(j=0;j<n;j++) {
            write_float_to_binfile(gsl_matrix_get(mat,i,j), opfile);
        }
    }
    fclose(opfile);
    return 0;
}
// ****************** added on 13 Feb 2015 **************************
// gsl_vector* read_vector(char *filename, int m);
gsl_vector* read_vector(char *filename, int m){
	FILE *ipfile = NULL;
	int i;
	float tempf;
	gsl_vector *vect;
	vect = gsl_vector_alloc(m);
	if(vect == NULL) {
	        fprintf(stderr,"Unable to allocate mem. read failed\n");
        	return NULL;
	}
	ipfile = fopen(filename, "r");
	if(ipfile == NULL) {
        	fprintf(stderr,"Unable to open file %s\n", filename);
	}
	for(i=0;i<m;i++) {
            fscanf(ipfile,"%e", &tempf);
            gsl_vector_set(vect,i,tempf);
	    //printf("\n Value: %e",tempf);
	    //fscanf(ipfile,"\n");
	}
	fclose(ipfile);
    return vect;
}
// *************************************************************
int
write_vector(char *filename, gsl_vector *vec) {
    FILE *opfile = NULL;
    int n,i;

    if(vec == NULL) {
        fprintf(stderr,"vector is empty. write failed\n");
        return 1;
    }
    
    opfile = fopen(filename,"w");
    if(opfile == NULL) {
        fprintf(stderr,"Unable to open file %s\n", filename);
        fprintf(stderr,"Write failed\n");
        return 1;
    }

    n = vec->size;
    for(i=0;i<n;i++) {
        fprintf(opfile,"%e ",gsl_vector_get(vec,i));
        fprintf(opfile,"\n");
    }
    fclose(opfile);
    return 0;
}

gt * read_gtfile(char *fname) {                                                                    
    FILE *fptr = NULL;                                                                             
    gt *list;                                                                                      
    char temp_buff[256];                                                                           
        int i,temp;                                                                                

    fptr = fopen(fname, "r");
    if(fptr == NULL) {
        fprintf(stderr,"Unable to open file %s\n", fname);                                         
    }                                                                                              
    list=(gt *)malloc(sizeof(gt));                                                                 
    i = 0;                                   
    while(!feof(fptr)) {                                                                           
        fscanf(fptr,"%s %d\n",temp_buff,&temp);                                                    
        //printf("v: %p Values read : %s %d\n",(void *)fptr,temp_buff,temp);                       
        i++; 
    }
    list->num_lines = i;
    list->line = (char **)malloc(sizeof(char *)*i);                                                
    list->modelno = (int *)malloc(sizeof(int)*i);                                                  
    i=0;                                                                                           
    rewind(fptr);                                                                                  
    while(!feof(fptr)) {                                                                           
        list->line[i] = (char*) malloc(sizeof(char)*256);                                          
            fscanf(fptr,"%s %d\n",list->line[i],&(list->modelno[i]));                              
        i++;
    }       
    fclose(fptr);                                                                                  
    return list;
} 

void
copyUpperToLower(gsl_matrix *mat) 
{
    int i,j;
    if(mat->size1 != mat->size2) return;
    for(i=1;i<mat->size1;i++)
        for(j=0;j<i;j++)
            gsl_matrix_set(mat,i,j,gsl_matrix_get(mat,j,i));
    return;
}

void
addIdentityMatrix(gsl_matrix *mat)
{ 
    translateDiagonal(mat,1.0);
    return;
}

void
translateDiagonal(gsl_matrix *mat, double d) 
{
    int i,r;
    r = mat->size1;
    r = r < mat->size2 ? r : mat->size2;
    for(i = 1; i < r; i++) 
        gsl_matrix_set(mat,i,i,gsl_matrix_get(mat,i,i)+d);
    return;
}

gsl_matrix*
invertMatrix(const gsl_matrix *A) 
{
    int m,n, *sign = NULL;
    gsl_matrix *invA = NULL;
    gsl_matrix *copyA = NULL;
    gsl_permutation *perm = NULL;

    if(A->size1 != A->size2) return NULL;
    m = A->size1;

    invA = gsl_matrix_alloc(m,m);
    copyA = gsl_matrix_alloc(m,m);
    perm = gsl_permutation_alloc(m);
    sign = (int*) malloc(sizeof(int)*m);
        
    if(invA == NULL || copyA == NULL || perm == NULL || sign == NULL) {
        fprintf(stderr,"Unable to allocate mem for matrices while inverting\n");
        return NULL;
    }
    gsl_matrix_memcpy(copyA,A);
    gsl_matrix_set_zero(invA);
    gsl_linalg_LU_decomp(copyA,perm,sign);
    gsl_linalg_LU_invert(copyA,perm,invA);

    gsl_matrix_free(copyA);
    gsl_permutation_free(perm);
    free(sign);
    return(invA);
}

fostat* read_fostat(char *fname)
{ 
    fostat *spkr_stats = NULL;
    FILE *ipfile = NULL;
    int i, j, k, nmix, dim;
    float tempf;
    int len = sizeof(fostat);
//    printf("Current File: %s\n", fname);
    spkr_stats = (fostat*) malloc(sizeof(fostat));
    if(spkr_stats == NULL) {
        fprintf(stderr, "Unable to allocate mem for spkr_stats\n");
        return NULL;
    }

    ipfile = fopen(fname,"r");
    if(ipfile == NULL) {
        fprintf(stderr, "Unable to open file %s\n", fname);
        return NULL;
    }

    fscanf(ipfile,"%d %d\n", &spkr_stats->nmix, &spkr_stats->dim);
    nmix = spkr_stats->nmix;
    dim = spkr_stats->dim;
//    printf("No.of.mix: %d Dimension: %d\n",nmix, dim );
    spkr_stats->eeta = (double*) malloc(nmix*sizeof(double));
    spkr_stats->f = gsl_matrix_alloc(nmix*dim,1);
    if(spkr_stats->f == NULL) {
        fprintf(stderr, "Unable to allocate mem\n");
    }

    for(i = 0; i < nmix; i++) fscanf(ipfile,"%lf\n", &spkr_stats->eeta[i]);
    k = 0;
    for(i = 0; i < nmix; i++) {
      for(j = 0; j < dim; j++) {
        fscanf(ipfile," %f", &tempf);
        gsl_matrix_set(spkr_stats->f,k++,0,tempf);
      }
      fscanf(ipfile,"\n");          
    }

    fclose(ipfile);
    return spkr_stats;

}

fostat*
read_fostat_bin(char *fname)
{ 
    FILE *ipfile = NULL;
    fostat *spkr_stats = NULL;
    int i, j, k, nmix, dim;
    int n,d;
    float tempf;
    
    spkr_stats = (fostat*) malloc(sizeof(fostat));
    if(spkr_stats == NULL) {
        fprintf(stderr, "Unable to allocate mem for spkr_stats\n");
        return NULL;
    }

    ipfile = fopen(fname,"r");
    if(ipfile == NULL) {
        fprintf(stderr, "Unable to open file %s\n", fname);
        return NULL;
    }
    
    nmix = read_int_from_binfile(ipfile);
    dim = read_int_from_binfile(ipfile);
    spkr_stats->nmix = nmix;
    spkr_stats->dim = dim;
    spkr_stats->eeta = (double*) malloc(nmix*sizeof(double));
    spkr_stats->f = gsl_matrix_alloc(nmix*dim,1);
    if(spkr_stats->f == NULL) {
        fprintf(stderr, "Unable to allocate mem\n");
    }

    for(n = 0; n < nmix; n++) 
        spkr_stats->eeta[n] = read_float_from_binfile(ipfile);

    for(n = 0; n < nmix; n++)
        for(d = 0; d < dim; d++)
            gsl_matrix_set(spkr_stats->f,n*dim+d,0,
                           read_float_from_binfile(ipfile));
    
    fclose(ipfile);
    return spkr_stats;
}

void
write_fostat(char *fname, fostat *spkr_stats) {
    FILE *opfile = NULL;
    int n, d, nmix, dim;

    opfile = fopen(fname,"w");
    nmix = spkr_stats->nmix;
    dim = spkr_stats->dim;
    fprintf(opfile,"%d %d\n", nmix, dim);
    for(n = 0; n < nmix; n++) 
        fprintf(opfile,"%lf\n", spkr_stats->eeta[n]);

    for(n = 0; n < nmix; n++) {
        for(d = 0; d < dim; d++) {
            fprintf(opfile," %e", gsl_matrix_get(spkr_stats->f,
                                                 n*dim+d,
                                                 0));
        }
        fprintf(opfile,"\n");
    }
    fclose(opfile);
}

void
free_fostat(fostat *spkr_stats)
{
    free(spkr_stats->eeta);
    gsl_matrix_free(spkr_stats->f);
    free(spkr_stats);
}


void
write_int_to_binfile(int x, FILE *f) 
{
    int l;
    int intsize = sizeof(int);
    char *c;
    c = (unsigned char*) &x;
    for(l = 0; l < intsize; l++) 
       putc ((char)c[l], f);
    return;
}

void
write_float_to_binfile(float x, FILE *f)
{
    int l;
    int floatsize = sizeof(float);
    for(l = 0; l < floatsize; l++) 
       putc (((unsigned char *) &x)[l], f);
    return;
}

int
read_int_from_binfile(FILE *f)
{
    unsigned char *c;
    int l,i;

    c = (unsigned char*) &i;
    for(l = 0; l < sizeof(int); l++) c[l] = (unsigned char) getc(f);
    return i;    
}

float
read_float_from_binfile(FILE *f)
{
    unsigned char *c;
    int l;
    float x;

    c = (unsigned char*) &x;
    for(l = 0; l < sizeof(int); l++) c[l] = (unsigned char) getc(f);
    return x;    
}

int
write_fostat_to_binfile(char *fname, char *opname)
{
    FILE *opfile = NULL;
    fostat *spkr_stats = NULL;
	int dim, nmix, i, j, k, d, n;

    spkr_stats = read_fostat(fname);
    if(spkr_stats == NULL) return FILE_OPEN_FAILED;
    if(sizeof(int) != 4) {
      fprintf (stderr,"int size is not 4. Incompatible system\n");
      return INCOMPATIBLE_INT_SIZE;
    }
    if(sizeof(float) != 4) {
        fprintf (stderr,"float size is not 4. Incompatible system\n");
        return INCOMPATIBLE_FLOAT_SIZE;
    }
    
    opfile = fopen(opname,"wb");    
    if(opfile == NULL) {
        return FILE_OPEN_FAILED;
    }
    dim = spkr_stats->dim;
    nmix = spkr_stats->nmix;
    write_int_to_binfile(nmix,opfile);
    write_int_to_binfile(dim,opfile);
    for(n = 0; n < nmix; n++) 
        write_float_to_binfile(spkr_stats->eeta[n],opfile);
    
    for(n = 0; n < nmix; n++)
        for(d = 0; d < dim; d++)
            write_float_to_binfile((float)gsl_matrix_get(spkr_stats->f,
                                                         n*dim + d,
                                                         0),
                                   opfile);
 
    fclose(opfile);
    return FILE_OPEN_SUCCESS;
}

flist*
read_list_of_files(char *fname)
{
    FILE *fptr = NULL;
    flist *list;
    char temp_buff[256];
    int i, nlines;

    fptr = fopen(fname, "r");
    if(fptr == NULL) {
        fprintf(stderr,"Unable to open file %s\n", fname);
        return NULL;
    }

    list = (flist*) malloc(sizeof(flist));
    if(list == NULL) {
        fprintf(stderr,"Unable to allocate mem for list\n");
        return NULL;
    }

    i = 0;
    while(!feof(fptr)) {
        fscanf(fptr,"%s\n",temp_buff);
        i++;
    }
    rewind(fptr);

    list->num_lines = i;
    list->line = (char**) malloc(sizeof(char*)*i);
    i = 0;
    while(!feof(fptr)) {
        list->line[i] = (char*) malloc(sizeof(char)*256);
        fscanf(fptr,"%s\n",list->line[i++]);
    }

    fclose(fptr);
    return list;
}

void
free_flist(flist *list)
{
    int len = list->num_lines, i;

    for(i = 0; i < len; i++) free(list->line[i]);
    free(list->line);
    free(list);
    return;
}

char*
basename_without_extn (char *path)
{
    int len = strlen (path), i;
    char *new_path, *cp, *token;    
    const char *delimiter = ".";

    new_path = basename (path);
    len = strlen(new_path);

    for(i=len-1;i>=0;i++) 
      if(path[i] == '.') break;
    cp = (char*) malloc(sizeof(char)*(i+2));
    memcpy(new_path,cp,(i+1));
    cp[i+1] = '\0';
    free(new_path);
    return cp;
}

int
is_substr(const char *ref, const char *superstr) 
{
    int i, j, ii, jj;


    i = strlen(ref);
    j = strlen(superstr);

    ii = 0;
    for(jj = 0; jj < j; j++) {
        if(ref[ii] == superstr[jj]) {
            ii++;
            if(ii == i) return 1;
        }
        else ii = 0;
    }
    return 0;
}

int 
get_index_in_flist(flist *list, const char *fname)
{
    int i;

    for (i = 0; i < list->num_lines; i++) {
        if(is_substr(list->line[i],fname)) return i;
    }
    return -1;
}

int
in_array(int *values, int len, int key)
{
    int i;

    for(i = 0; i < len; i++) 
        if(values[i] == key) return 1;
    return 0;
}

int swap (gt *gtptr,int i, int j) {
  int temp;
  char *temp_buff;
  temp = gtptr->modelno[i];
  gtptr->modelno[i] = gtptr->modelno[j];
  gtptr->modelno[j] = temp;
  temp_buff = gtptr->line[i];
  gtptr->line[i] = gtptr->line[j];
  gtptr->line[j] = temp_buff;
}

void gt_quickSort(gt *gtptr, int left, int right) {
  int   i,j,k;
  int x, w;
  i = left;
  j = right;
  x = gtptr->modelno[(left+right)/2];
  do {
    while (gtptr->modelno[i] < x)
      i++;
    while (x < gtptr->modelno[j])
      j--;
     if (i <= j) {
       swap(gtptr,i,j);
       i++;
       j--;
     }
 }   while (i < j);
  if (left < j)
    gt_quickSort(gtptr,left,j);
  if (i < right)
    gt_quickSort(gtptr,i,right);
}


void printProgress (double percentage)
{
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf ("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush (stdout);
}

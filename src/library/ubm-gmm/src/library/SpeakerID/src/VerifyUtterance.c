/*-------------------------------------------------------------------------
 *  VerifyUtterance.c - Verifies the claim of a speaker
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	 (hema@localhost.localdomain)
 *
 *  Created:        Wed 13-Feb-2002 19:53:50
 *  Last modified:  Thu 28-Feb-2002 10:17:50 by hema
 *  $Id: VerifyUtterance.c,v 1.1 2002/08/31 05:51:47 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/


/************************************************************************
  Function            : VerifyUtterance- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numVq numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 19-Dec-98
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/front-end-defs.h"
#include "fe/front-end-types.h"
#include "fe/DspLibrary.h"
#include "fe/init-asdf.h"
#include "fe/batch-process-waveform.h"
#include "fe/sphere_interface.h"
#include "fe/vq.h"
/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *featureName, int *numVectors) {

F_VECTOR *fvect;
VECTOR_OF_F_VECTORS *vfv;
int i;
int frameNo;

/*printf("enter compute feat vect \n");
  fflush(stdout);*/

  vfv = (VECTOR_OF_F_VECTORS *) calloc(asdf->numFrames, sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < asdf->numFrames; i++) {
    fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
      if(fvect == NULL) {
      printf("problems fvect\n");
      fflush(stdout);
      exit(-1);
    }
    vfv[i] = fvect;
  }
  GsfClose(asdf);
*numVectors = asdf->numFrames;
printf("total no of frame processed = %d\n",*numVectors);
fflush(stdout); 
return(vfv);
}
/************************************************************************
  Function            : BestCluster - computes the
                        minimum error for a given feature vector

  Inputs              : fvect - feature vector
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
  Outputs             : smallest distance
            
  **************************************************************************/

float BestCluster(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *speakerMean,
		   VECTOR_OF_F_VECTORS *speakerVar, F_VECTOR *speakerWts, int numClusters){
int                          i, j;
float                        *error;
int                          featLength; 
            
/*  printf("enter BestCluster\n");
  fflush(stdout);*/
  featLength = fvect->numElements;
  /*  printf("numClusters = %d featLength = %d\n",numClusters,featLength);
  fflush(stdout);*/
  error = (float *) calloc (numClusters, sizeof(float)); 
  for (i = 0; i < numClusters; i++) {
    error[i] = 0;
     for (j = 0; j < featLength; j++)
       error[i]  = error[i]  + (fvect->array[j] - speakerMean[i]->array[j])* 
                     (fvect->array[j] - speakerMean[i]->array[j]);
	 /*(speakerVar[i]->array[j]*speakerVar[i]->array[j]) */;
     /*    printf("error %d = %f\n",i,error[i]);
    fflush(stdout);*/
  }
  
  return(error[Imin0(error,numClusters)]);
}

/************************************************************************
  Function            : ComputeDistortion - computes the
                        total distortion for the entire utterance data.

  Inputs              : vfv - feature vectors for an utterance
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
                        claim - claim of utterance
  Outputs             : Distortion
            
  **************************************************************************/

float  ComputeDistortion(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int numClusters, int claim) {
  
  int                     i, j, k;
  float                   distSpeaker, distBackGround, distortion;
  /*printf("enter comp dist\n");
    fflush(stdout);*/
    distSpeaker = 0;
    distBackGround = 0;
    for (i = 0; i < numSpeakers; i++) {
      for (j = 0; j < numVectors; j++) {
	if (i == claim) 
	  distSpeaker = distSpeaker + BestCluster(vfv[j],
						  speakerMeans[claim], 
						  speakerVars[claim], 
						  speakerWts[claim],
						  numClusters);
	else
	  distBackGround = distBackGround + BestCluster(vfv[j],
						       speakerMeans[i], 
							speakerVars[i], 
							speakerWts[i],
							numClusters);
      }
    }
    distortion = distSpeaker/(distBackGround/(numSpeakers-1));
    printf("distSpeaker= %f = distBackGround = %f distortion = %f\n",
	   distSpeaker, distBackGround, distortion);
    fflush(stdout); 
 
  return(distortion);
}

void Usage() {
   printf (" Usage : VerifyUtterance ctrl_file model_file num_vq feature_name num_speakers wavname claim threshold\n");
}
  main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[256],line[256];
  int                   numSpeakers,numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, 
                        *speakerModelWts;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i,j,k;
  int                   featLength,numVectors;
  float                 distortion, threshold;
  int                   claim;

  if (argc != 9) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[6];
  cFile = fopen(cname,"r");
  sscanf(argv[7],"%d", &claim);
  sscanf(argv[8], "%f", &threshold);
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  printf("testfile name = %s\n",wavname);
  fflush(stdout);
  GsfOpen(asdf,wavname);
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,featureName, &numVectors);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS ));
  for (i = 0; i <= numSpeakers; i++) {
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters);
    for (j = 0; j < numClusters; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
  }
  i = 0;
  while (fgets(line,200,modelFile)) {
    sscanf(line,"%s",speakerModel);
    speakerFile = fopen(speakerModel,"r");
    /*printf("speakerFile = %s\n",speakerModel);
      fflush(stdout); */
      for (j = 0; j < numClusters; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      /*      printf("%f\n",speakerModelWts[i]->array[j]);
	      fflush(stdout);*/
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile," %f %f",&speakerModelMeans[i][j]->array[k],
        &speakerModelVars[i][j]->array[k]);       
	/*	printf(" %f %f\n",speakerModelMeans[i][j]->array[k],
		speakerModelVars[i][j]->array[k]);  
        fflush(stdout);*/
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  distortion = (float) ComputeDistortion(vfv,asdf->numFrames,
					   numSpeakers,speakerModelMeans,
		 			   speakerModelVars,speakerModelWts,
					   numClusters,claim);
  printf("wavefile = %s\n",wavname);
  printf("distortion = %f\n", distortion);
  if (distortion < threshold) 
    printf("%50s Claim as speaker %5d accepted\n",wavname, claim);
  else
    printf("%50s Claim as speaker %5d rejected \n",wavname, claim);

  }









/*-------------------------------------------------------------------------
 * $Log: VerifyUtterance.c,v $
 * Revision 1.1  2002/08/31 05:51:47  hema
 * Initial revision
 *
 * Revision 1.1  2002/04/19 10:53:13  hema
 * Initial revision
 *
 * Revision 1.1  2002/04/19 10:28:43  hema
 * Initial revision
 *
 * Revision 1.1  2002/02/28 05:07:29  hema
 * Initial revision
 *
 * Revision 1.1  2002/02/18 11:21:42  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of VerifyUtterance.c
 -------------------------------------------------------------------------*/

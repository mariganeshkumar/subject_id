/*
    This file computes the cosine similarity between the test i vector
    to all the train i vectors and assigns the best

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/



#include "gsl_helper.h"
#include<string.h>
#define CHAR_ALLOC(k) (char*)malloc(sizeof(char)*k)


int main(int argc, char *argv[]) {
	if(argc!=8) {
		printf("Usage : %s testModelList enrolledModelList wccnMat whitenMat R threshold resultFile\n",argv[0]);
		exit(0);
	}
	FILE *testModelsListFile, *enrolledModelsListFile, *resultFile;
	char *testListName, *enrolledListName, *resultFileName, **testModelsList, 
		**enrolledModelsList, *wccnMatName, *wMatName, line[256];
	int i,j,R,tempi,nModels=0,nUtter=0;
	double tempf, tempf1, tempf2;
	float th;
	gsl_matrix *wccnMat, *wMat, *tempMat, *testProjMat, 
		*enrolledProjMat, *resultMat;
	gsl_vector *tempVect, *tempVect1;

	testListName=argv[1];
	enrolledListName=argv[2];
	wccnMatName=argv[3];
	wMatName=argv[4];
	R=atoi(argv[5]);
	th=atof(argv[6]);
	resultFileName=argv[7];
	if((testModelsListFile = fopen(testListName,"r")) == NULL) {
		fprintf(stderr,"test list file %s cannot be opened\n",testListName);
        	exit(1);
		}
	if((enrolledModelsListFile = fopen(enrolledListName,"r")) == NULL) {
		fprintf(stderr,"enrolled list file %s cannot be opened\n",enrolledListName);
        	exit(1);
		}
	resultFile=fopen(resultFileName,"w+");
	nUtter=0;
	while(fgets(line,sizeof(line),testModelsListFile)!=NULL) nUtter++;
	while(fgets(line,sizeof(line),enrolledModelsListFile)!=NULL) nModels++;
	rewind(testModelsListFile);
	rewind(enrolledModelsListFile);
	testModelsList=(char **)malloc(sizeof(char *)*nUtter);
	enrolledModelsList=(char **)malloc(sizeof(char *)*nModels);

	 i = 0;
	while (fgets(line,1000,testModelsListFile)) {
		testModelsList[i]= CHAR_ALLOC(256);
		sscanf(line,"%s\n",testModelsList[i]); 
    		i++;
  	}
	if(i!=nUtter) {
		fprintf(stderr,"One or more test models are faulty\n");
		exit(1);
	}

	i = 0;
	while (fgets(line,1000,enrolledModelsListFile)) {
		enrolledModelsList[i]= CHAR_ALLOC(256);
		sscanf(line,"%s\n",enrolledModelsList[i]); 
    		i++;
  	}
	if(i!=nModels) {
		fprintf(stderr,"One or more enrolled models are faulty\n");
		exit(1);
	}


	wccnMat = gsl_matrix_alloc(R,R);
	wMat = gsl_matrix_alloc(R,R);
	tempVect = gsl_vector_alloc(R);
	tempVect1 = gsl_vector_alloc(R);
	testProjMat = gsl_matrix_alloc(R,nUtter);
	enrolledProjMat = gsl_matrix_alloc(R,nModels);
	resultMat = gsl_matrix_alloc(nUtter,nModels);
	gsl_matrix_set_zero(testProjMat);
	gsl_matrix_set_zero(enrolledProjMat);
	gsl_matrix_set_zero(resultMat);
	wccnMat = read_matrix(wccnMatName,R,R);
	wMat = read_matrix(wMatName,R,R);
	gsl_matrix_transpose(wccnMat);


	tempMat = gsl_matrix_alloc(R,nUtter);
	for(i=0;i<nUtter;i++) {
		tempVect1=read_vector(testModelsList[i],R);
		gsl_blas_dgemv(CblasTrans, 1.0, wMat, tempVect1, 0.0, tempVect);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(tempMat,i,tempVect);
	}
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat,tempMat,0.0,testProjMat);
       for(i=0;i<nUtter;i++) {
		gsl_matrix_get_col(tempVect,testProjMat,i);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(testProjMat,i,tempVect);
	}

	tempMat = gsl_matrix_alloc(R,nModels);
	for(i=0;i<nModels;i++) {
		tempVect1=read_vector(enrolledModelsList[i],R);
		gsl_blas_dgemv(CblasTrans, 1.0, wMat, tempVect1, 0.0, tempVect);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(tempMat,i,tempVect);
	}
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat,tempMat,0.0,enrolledProjMat);
	for(i=0;i<nModels;i++) {
		gsl_matrix_get_col(tempVect,enrolledProjMat,i);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(enrolledProjMat,i,tempVect);
	}

	 
	for(i=0;i<nUtter;i++) {
		gsl_matrix_get_col(tempVect,testProjMat,i);
                for(j=0;j<nModels;j++) {              
			gsl_matrix_get_col(tempVect1,enrolledProjMat,j);
			gsl_blas_ddot(tempVect,tempVect1,&tempf2);
			gsl_matrix_set(resultMat,i,j,tempf2);
		}
        }
	tempVect=gsl_vector_alloc(nModels);
	for(i=0;i<nUtter;i++) {
		gsl_matrix_get_row(tempVect,resultMat,i);
		tempf=gsl_vector_max(tempVect);
		if(tempf<th) {
			fprintf(resultFile,"%s : reject\n",testModelsList[i]);
//			printf("%s : reject\n",testModelsList[i]);
			}
		else {
			tempi=gsl_vector_max_index (tempVect);
			fprintf(resultFile,"%s : accept; tagged to model %s\nPossible associations:\n",testModelsList[i],enrolledModelsList[tempi]);
//			printf("%s : accept; tagged to model %s\nPossible associations:\n",testModelsList[i],enrolledModelsList[tempi]);
			for(j=0;j<nModels;j++)
				if(j!=tempi)
					if(gsl_vector_get(tempVect,j) < th)
						continue;
					else {
//						printf("%s\n",enrolledModelsList[j]);
						fprintf(resultFile,"%s\n",enrolledModelsList[j]);
						}
				else
					continue;
				
			}
//		printf("\n");
		fprintf(resultFile,"\n");
		}
	gsl_matrix_free(wccnMat);
	gsl_matrix_free(wMat);
	gsl_matrix_free(tempMat);
	gsl_matrix_free(testProjMat);
	gsl_matrix_free(enrolledProjMat);
	gsl_matrix_free(resultMat);
	gsl_vector_free(tempVect);
	gsl_vector_free(tempVect1);
	}

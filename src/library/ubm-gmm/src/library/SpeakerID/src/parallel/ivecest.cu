/* estimate ivector given the first order statistics, 
  t-matrix */

/* Compilation procedure --
  /usr/local/cuda/bin/nvcc -lgsl -lgslcblas -lm ivecest.cu -o ivecest
*/


#include "stdio.h"
#include "cuda.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>

#define STR_DEFAULT_SIZE 512
#define IDX2F(i,j,ld) ((((i)-1)*(ld))+((j)-1))

__global__ void
computeRhs (float *tmat, float  *fx, float *w, int n, int k) {
    int ridx;
    int i;
    float tempf = 0.;

    ridx = blockIdx.x + 1;

    for (i = 1; i <= n; i++)
      tempf = tempf + (tmat[IDX2F(i,ridx,k)] * fx[i-1]);

    w[ridx] = tempf;
}

__global__ void
computeLhs (float *tmat, float *Nx, float *Lx, int m, int d, int r, int md) {
    int ridx, cidx;
    int i, j;
    float tempf = 0.;
    
    ridx = blockIdx.x + 1;
    cidx = blockIdx.y + 1;
    if (ridx < cidx) return;
    
    j = 0;
    for (i = 1; i <= md; i++) {
          tempf += (Nx [j] * tmat[IDX2F(i,ridx,r)] * tmat[IDX2F(i,cidx,r)]);
          if (i % d == 0) j++;
    }

    Lx [IDX2F(ridx,cidx,r)] = tempf;
    Lx [IDX2F(cidx,ridx,r)] = tempf;
}

int
main (int argc, char *argv[]) {

  int ipIsList = 0,             // by default input is not a list
      nlines = 0,
      M = -1, D = -1, R = -1, MD = 0, RR = 0,
      m = 0, n = 0,
      idx = 0,
      i = 0, j = 0,
      tempi,
      *Nx;      
  float *tmat = NULL, *fx = NULL,
        *Lx = NULL, *wx = NULL, *Nx_float = NULL;
  char **ipList = NULL, 
       **opList = NULL,
       *ipListFileName = NULL,
       *opListFileName = NULL,
       *tMatFileName = NULL,
       *tempStr = NULL;
  FILE *ipListFilePtr = NULL, 
       *opListFilePtr = NULL,
       *tMatFilePtr = NULL,
       *foFilePtr = NULL,
       *opFilePtr = NULL;
  /* The following set of declarations deal with pointers
     to gpu mem locations */
  float *gpu_tmat,
        *gpu_Nx,
        *gpu_fx,
        *gpu_Lx,        
        *gpu_wx;
  cudaStream_t cstream;
  /* The following set of declarations deal with 
     pointers to gsl structures */
  gsl_matrix *gsl_lx;
  gsl_vector *gsl_wx;
  gsl_permutation *gsl_p;
  gsl_vector *gsl_rhs;

  /* First decide what is required for this program to 
     run. Design it such that it can process in batches 
  */

  if (argc < 7 ) {
      printf ("Usage: ./ivecest T-matrix-file first-order-stat-file output-file");
      printf (" M D R");
      printf (" [ipIsList]\n");
      exit (1);    
  }

  tMatFileName = argv[1];
  sscanf (argv[4], "%d", &M);
  sscanf (argv[5], "%d", &D);
  sscanf (argv[6], "%d", &R);

  if (M <= 0 || D <= 0 || R <= 0) {
      printf ("Invalid value(s) for M/D/R\n");
      exit (1);
  }
  
  MD = M*D;
  RR = R*R;
  dim3 fxlauncher(R,1);
  dim3 lxlauncher(R,R);

  if (argc == 8) {
      ipListFileName = argv[2];
      opListFileName = argv[3];
      sscanf (argv[4], "%d", &ipIsList);      
  }

  if (ipIsList) {

      // Load the entire list into memory. Is this too bad?
      // 1. Find out the number of files to be processed
      // 2. For each line in list, allocate memory and scan the name(s)
      //    from the file(s)

      ipListFilePtr = fopen (ipListFileName, "r");
      tempStr = (char *) calloc (STR_DEFAULT_SIZE, sizeof(char));
      while (!feof (ipListFilePtr)) {
          fscanf (ipListFilePtr, "%s\n", tempStr);
          nlines++;
      }
      free (tempStr);
      rewind (ipListFilePtr);
      ipList = (char **) calloc (nlines, sizeof(char*));
      opList = (char **) calloc (nlines, sizeof(char*));
      i = 0;
      while (!feof (ipListFilePtr) && !feof (opListFilePtr)) {
          ipList[i] = (char *) calloc (STR_DEFAULT_SIZE, sizeof(char));
          opList[i] = (char *) calloc (STR_DEFAULT_SIZE, sizeof(char));
          if (ipList[i] == NULL || opList[i] == NULL) {
              printf ("Unable to allocate memory while loading file lists\n");
              exit (2);
          }
          fscanf (ipListFilePtr, "%s\n", ipList[i]);
          fscanf (opListFilePtr, "%s\n", opList[i]);
          i++;
      }
      if (i != nlines) {
          printf ("List loading ended abruptly\n");
          exit (3);
      }
      fclose (ipListFilePtr);
      fclose (opListFilePtr);
  }
  else {
      nlines = 1;
      ipList = (char **) calloc (1, sizeof (char*));
      opList = (char **) calloc (1, sizeof (char*));
      ipList[0] = argv[2];
      opList[0] = argv[3];
  }

  /* Hopefully, the lists are loaded and stable.
   * Now the time is ripe to load the t-matrix
   */
  
  tmat = (float*) malloc (MD*R* sizeof (float));
  if (tmat == NULL) {
      printf ("Unable to allocate memory for T-Matrix\n");
      exit (2);
  }
  
  tMatFilePtr = fopen (tMatFileName, "r");
  idx = 0;
  for (m = 0; m < MD; m++) {
      for (n = 0; n < R; n++) {
          fscanf(tMatFilePtr, "%e", &tmat[idx++]);
      }
      fscanf (tMatFilePtr, "\n");
  }
  fclose (tMatFilePtr);

  // Transfer it to GPU. safely.
  cudaMalloc ((void **) &gpu_tmat, MD*R*sizeof(float));
  cudaMemcpy (gpu_tmat, tmat, sizeof(float)*MD*R, cudaMemcpyHostToDevice);

  // allocate memory before going into a loop
  cudaMalloc ((void **) &gpu_Nx, M*sizeof(float));
  Nx_float = (float *) malloc (M*sizeof(float));
  Nx = (int *) malloc (M* sizeof(float));

  cudaMalloc ((void **) &gpu_fx, MD*sizeof(float));
  fx = (float*) malloc (MD* sizeof(float));

  cudaMalloc ((void **) &gpu_Lx, RR*sizeof(float));
  Lx = (float *) malloc (RR*sizeof(float));

  cudaMalloc ((void **) &gpu_wx, R*sizeof (float));
  wx = (float *) malloc (R* sizeof(float));
  gsl_wx = gsl_vector_alloc (R);

  gsl_p = gsl_permutation_alloc (R);
  gsl_rhs = gsl_vector_alloc (R);

  // for each input file
  // 1. Read the first order stats
  // 2. Copy Nx to gpu and compute Lx
  // 3. Copy Lx to host and invert
  // 4. Copy fx to gpu and compute T'fx
  // 5. Compute Lx^-1 * T' * fx
  // 6. Store it in the outputfile

  for (i = 0; i < nlines; i++) {
      foFilePtr = fopen (ipList[i], "r");
      fscanf (foFilePtr, "%d %d\n", &tempi, &tempi);
      for (m = 0; m < M; m++) {
        fscanf (foFilePtr, "%d\n", &Nx[m]);
        Nx_float[m] = (float) Nx[m];
      }
      for (m = 1; m <= M; m++) {
          for (n = 1; n <= D; n++)  
              fscanf (foFilePtr, "%e ", &fx[IDX2F(m,n,M)]);
          fscanf (foFilePtr, "\n");
      }
      fclose (foFilePtr);

      cudaMemcpy (gpu_fx, fx, MD*sizeof(float), cudaMemcpyHostToDevice);
      cudaStreamCreate (&cstream);
      computeRhs <<<fxlauncher,1,0,cstream>>> (gpu_tmat, gpu_fx, gpu_wx, MD, R); 
      cudaStreamSynchronize (cstream);
      cudaMemcpy (wx, gpu_wx, R*sizeof(float), cudaMemcpyDeviceToHost);
      for (m = 0; m < R; m++) gsl_rhs->data[m] = (double) wx[m];

      cudaMemcpy (gpu_Nx, Nx_float, M*sizeof(float), cudaMemcpyHostToDevice);

      cudaStreamCreate (&cstream);
      computeLhs <<<lxlauncher,1,0,cstream>>> (gpu_tmat, gpu_Nx, gpu_Lx, M, D, R, MD);
      cudaStreamSynchronize (cstream);
      cudaMemcpy (Lx, gpu_Lx, RR*sizeof(float), cudaMemcpyDeviceToHost);
      for (n = 1; n <= R; n++) Lx[IDX2F(n,n,R)]+=1.0; 
      for (m = 1; m < R; m++) 
          for (n = m + 1; n <= R; n++) 
              Lx[IDX2F(n,m,R)] = Lx[IDX2F(m,n,R)];
      
      gsl_lx = gsl_matrix_alloc (R,R);
      for (m = 0; m < RR; m++) gsl_lx->data[m] = (double) Lx[m];

      gsl_linalg_LU_decomp (gsl_lx, gsl_p, &tempi);
      gsl_linalg_LU_solve  (gsl_lx, gsl_p, gsl_rhs, gsl_wx);

      for (m = 0; m < R; m++) wx[m] = (float) gsl_wx->data[m];

      opFilePtr = fopen (opList[i], "w");
      for (n = 0; n < R; n++) fprintf (opFilePtr, "%e\n", wx[n]);
      fclose (opFilePtr);
  }

  // free'ing everything
  cudaFree (gpu_tmat);
  cudaFree (gpu_Nx);
  cudaFree (gpu_fx);
  cudaFree (gpu_Lx);
  cudaFree (gpu_wx);

  free (tmat);
  free (wx);
  free (Lx);
  free (fx);
  free (Nx);
  free (Nx_float);
  gsl_vector_free (gsl_wx);
  gsl_vector_free (gsl_rhs);
  gsl_matrix_free (gsl_lx);
  gsl_permutation_free (gsl_p);
  for (i = 0; i < nlines; i++) {
      free (ipList[i]);
      free (opList[i]);
  }
  free (ipList);
  free (opList);
}
   

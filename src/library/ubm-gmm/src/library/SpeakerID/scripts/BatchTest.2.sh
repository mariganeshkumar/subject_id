#!/bin/bash

TESTBINARY=../bin/testmap
CBSIZE=1024
MFOLDER=../models/2003.m.fcd
SPIDMODELS=$MFOLDER/SpidModel
CTRLFILE=../cfiles/fe-ctrl.base
FEATURE=frameCepstrum+frameDeltaCepstrum
TESTLIST=../flists/2003/m.test
GROUNDTRUTH=../flists/2003/GroundTruth
RESULTDIR=$MFOLDER/AllResults
SPKRLIST=../flists/2003/spkrList.m
FINALRESULT=$MFOLDER/finalResult
TEMPFILE=temp
UBMFILE=../ubm/n2003.ubm.m
UBMSIZE=1024
numSpeakers=149

rm -f $FINALRESULT
touch $FINALRESULT
mkdir -p $RESULTDIR

for testFile in `cat $TESTLIST`; do
	testName=`basename $testFile | cut -d '.' -f1`
	echo "$TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testFile 0.0001 0 $UBMFILE $UBMSIZE"
  $TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testFile 0.0001 0 $UBMFILE $UBMSIZE > $TEMPFILE 
	hypothesized=`grep speaker $TEMPFILE | awk '{print $5}'`
	speaker=`head -$hypothesized $SPKRLIST | tail -1`
	speaker=`basename $speaker | cut -d '.' -f1`
	actual=`grep $testName $GROUNDTRUTH | cut -d ' ' -f2`
	echo "$2 $actual $speaker" >> $FINALRESULT
	
	cp $TEMPFILE $RESULTDIR/$testName.result
  grep Distortion $RESULTDIR/$testName.result | cut -d ' ' -f4 > $RESULTDIR/$testName.result.dist

done

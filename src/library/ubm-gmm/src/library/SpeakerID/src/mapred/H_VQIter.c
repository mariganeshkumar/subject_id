#include "CommonFunctions.h"
#include "b64_mod.h"


/******************************************************
 * This is a mapper function which given the current
 * iterations base VQ params will decide, for each
 * feature vector, the closest cluster
 * ***************************************************/
int
main (int argc, char *argv[])
  {
    int numVectors, numClusters, closestid, 
				featLength, *elemCnt = NULL;
		int i,j,k, tempnc, tempfl;
		int ndbytes = 0, nbytes = 0;						
		unsigned char *bytearr = NULL;
		float psf, tscale = 0.0, varianceNormalize;
		FILE *ip = NULL, *op = NULL, *oldstdin, *basemodel;
	 ASDF *asdf;
	 VECTOR_OF_F_VECTORS *vqm = NULL, *vqv = NULL, *vfv = NULL;
	 VECTOR_OF_F_VECTORS *sumvec = NULL, *sumsqvec = NULL;
	 VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
	 float *vqwt, *ergWts;

	 int oc, base64enc, useTS = 1, useVad = 0, ipIsList = 0,
			 firstIter = 0;
	 char *basemodelfname, *featureName, *waveFileName,
					 *ctrlFileName = NULL, *ergFileName = NULL;
	 FILE *ctrlFile;
	 // b : basemodel
	 // s : first iteration use seeded means from basemodel
	 // v: vad
	 // t: trigaussian
	 // k: numclusters
	 // i : input is a list of files
	 // n : feature name 
	 while ( (oc = getopt (argc, argv, "b:i:v:t:k:f:n:es")) != -1)
	 {
					 switch (oc) {
					 case 'b':
									 basemodelfname=optarg;
									 break;
					 case 's':
									 firstIter = 1;
									 break;									 
				   case 'i':
					     if (!strcmp (optarg, "list"))							   
								   ipIsList = 1;
							 break;		 
					 case 'v':
							 useVad = 1;
							 useTS = 0;
					     ergFileName = optarg;
							 ergMeans = NULL;
							 ergVars = NULL;
							 ergWts = NULL;
							 ReadTriGaussianFile (ergFileName,
							                     &ergMeans,
																	 &ergVars,
																	 &ergWts);
           case 't':
					     if (useVad == 1)
							     break;
						   useTS = 1;
							 sscanf (optarg, "%f", &tscale);
							 break;
					 case 'f':
					     ctrlFileName = optarg;
							 break;
					 case 'n':
					     featureName = optarg;
							 break;
					 case 'k':
					     sscanf (optarg, "%d", &numClusters);
							 break;
					 case 'e':
					     base64enc = 1;
						  break;
					 default:
					     return 1;
					 }
	 }				 

   ctrlFile = fopen (ctrlFileName, "r");
   asdf = (ASDF *) calloc (1, sizeof (ASDF));
   InitializeStandardFrontEnd (asdf, ctrlFile);
   Cstore (asdf->fftSize);
	 psf = (float) GetFAttribute (asdf, "probScaleFactor");
	 varianceNormalize = (float) GetFAttribute (asdf, "varianceNormalize");
	 fclose (ctrlFile);
   fprintf (stderr,"working till here HELLLLLLLOOOO\n");
	 if (base64enc)
	   {
		  // read the encoded file from stdin
		  ip = stdin; 
		  op = tmpfile (); 
		  decode (ip,op);
		  fprintf (op, "%d", EOF);
		  ndbytes = ftell (op);
		  rewind (op);
		  bytearr = (unsigned char *) calloc (ndbytes, sizeof (unsigned char));
		  nbytes = 0;
        while (!feof(op) || nbytes == ndbytes) 
		  {
		    bytearr[nbytes++] = (unsigned char) fgetc (op);
		  }
		  ndbytes = nbytes;
			fprintf(stderr,"%d bytes read\n", ndbytes);
		  fclose (op);		  		  
		  oldstdin = stdin;
		  stdin = fmemopen (bytearr, ndbytes, "rb");		    
		}
	 waveFileName = "-";
	 vfv = NULL;
	 if (!ipIsList)
	   {
			 if (useVad)
	         vfv = ExtractFeatureVectorsVAD 
					                     (asdf, 
					                     "-", 
															 featureName,
	                             &numVectors,
	                             ergMeans, 
															 ergVars, 
															 ergWts);
			 else
			    vfv = ExtractFeatureVectors (asdf, waveFileName,
					                       featureName, &numVectors,
																 tscale);
		 }
	 else
	   {
		   if (useVad)
			    vfv = BatchExtractFeatureVectorsVAD (asdf, "-",
			                                        featureName,			                                        
																							&numVectors,
                                              ergMeans,
			                                        ergVars, 
																							ergWts);
       else
           vfv = BatchExtractFeatureVectors (asdf, "-",
		                                        featureName,
		       					                        &numVectors, 
																						tscale);
		 }

   featLength = vfv[0]->numElements;
	 if (firstIter)
	 {
					 vqm = (VECTOR_OF_F_VECTORS *) calloc (numClusters, 
													 											sizeof(VECTOR_OF_F_VECTORS));
					 vqv = (VECTOR_OF_F_VECTORS *) calloc (numClusters, 
													 											sizeof(VECTOR_OF_F_VECTORS));
					 for (k = 0;k < numClusters; k++)
					 {
									 vqv[k] = (F_VECTOR *) AllocFVector (featLength);
									 vqm[k] = (F_VECTOR *) AllocFVector (featLength);
									 InitFVector (vqv[k]);
									 InitFVector (vqm[k]);
					 }
					 basemodel = fopen (basemodelfname, "r");
					 fscanf (basemodel,"%d %d\n",&tempnc, &tempfl);
					 tempnc = 0;
					 while (tempnc < numClusters && !feof (basemodel))
					 {
									 for (j = 0; j < featLength; j++)									 
									 	fscanf (basemodel,"%e ", &vqm[tempnc]->array[j]);										

									 fscanf (basemodel,"\n");
									 tempnc++;
					 }
					 fclose (basemodel);
					 if (tempnc != numClusters)
					 {
									 fprintf (stderr, "Not enough seeds\n");
									 FreeVfv (vqm, numClusters);
									 FreeVfv (vqv, numClusters);
									 return 3;
					 }
	 }
	 else
		ReadGMMFile (basemodelfname, &vqm, &vqv, &vqwt, numClusters, featLength); 												 
		sumvec = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
		sumsqvec = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
		elemCnt = (int *) calloc (numClusters, sizeof (int));
    for (k = 0; k < numClusters; k++)
				{
						sumvec[k] = (F_VECTOR *) AllocFVector (featLength);						
						sumsqvec[k] = (F_VECTOR *) AllocFVector (featLength);
						InitFVector (sumvec[k]);
						InitFVector (sumsqvec[k]);
				}
		fprintf (stderr, "Initialized sum and squared sum vecs. varianceNormalize is %d\n", 0);
		for (i = 0; i < numVectors; i++)
			{
							
       closestid = DecideWhichCluster(vfv[i], vqm , vqv , 
					 													numClusters, 
																		0);
			 elemCnt[closestid]++;
			 for (j = 0; j < featLength; j++)							 
			 {
							 				 sumvec[closestid]->array[j] += vfv[i]->array[j];
											 sumsqvec[closestid]->array[j] += (vfv[i]->array[j] * vfv[i]->array[j]);							 
			 }
			}
    fprintf (stderr, "Accumulated sum\n");
		for (k = 0; k < numClusters; k++)
		{
						printf ("%d\t%d,%d",k+1, elemCnt[k],featLength);
						for (j = 0; j < featLength; j++)
								printf(",%e",sumvec[k]->array[j]);
						
						for (j = 0; j < featLength; j++)
								printf(",%e",sumsqvec[k]->array[j]);						
						printf("\n");						
		}

		FreeVfv (vfv, numVectors);
		FreeVfv (sumvec, numClusters);
		FreeVfv (sumsqvec, numClusters);
		free (elemCnt);		
		return 0;

  }

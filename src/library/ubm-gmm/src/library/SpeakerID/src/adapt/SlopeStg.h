#ifndef __SLOPE_STG__
#define __SLOPE_STG__
#include "CommonFunctions.h"
VFV* 
ComputeSlopeStg (VFV *lfbstream, uint nvec, int nrc, int gwnd);
#endif

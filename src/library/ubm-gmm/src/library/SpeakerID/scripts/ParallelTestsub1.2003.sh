#!/bin/bash

TESTBINARY=../bin/test2004.d
CBSIZE=1024
MFOLDER=/home/srikanth/Research/Experiments/STGauss/models/2003.m.fcd.ubm.new
SPIDMODELS=$MFOLDER/SpidModel
CTRLFILE=../cfiles/fe-ctrl.base
FEATURE=frameCepstrum+frameDeltaCepstrum
TESTLIST=../flists/2003/m.test
GROUNDTRUTH=../flists/2003/GroundTruth
RESULTDIR=$MFOLDER/AllResults
SPKRLIST=../flists/2003/spkrList.m
FINALRESULT=$MFOLDER/finalResult
TEMPFILE=temp
TEMPDIR=../temp.1/
UBMFILE=../models/n2003.m.ubm.new
UBMSIZE=1024
CVAL=10
dumpbin=../bin/ComputeFeatures
numSpeakers=`wc -l $SPIDMODELS | cut -d ' ' -f1`
TGMM=../models/tgmm

	testName=`basename $1 .sph`
	testPath=$1
  LOOPTEMPFILE=$RESULTDIR/$testName.result
  $dumpbin $CTRLFILE $testPath $FEATURE $TEMPDIR/$testName.temp 0.0001 0 > /dev/null
  echo "$dumpbin $CTRLFILE $testPath $FEATURE $TEMPDIR/$testName.temp 0.0001 0 > /dev/null"
  $TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $TEMPDIR/$testName.temp 0.0 0 $UBMFILE $CVAL $TGMM> $LOOPTEMPFILE 
  echo "$TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $TEMPDIR/$testName.temp 0.0 0 $UBMFILE $CVAL $TGMM> $LOOPTEMPFILE "
	hypothesized=`grep speaker $LOOPTEMPFILE | awk '{print $5}'`
	speaker=`head -$hypothesized $SPKRLIST | tail -1`
	speaker=`basename $speaker | cut -d '.' -f1`
  gt=`grep $testName $GROUNDTRUTH | cut -d ' ' -f2`
	echo "$1 $speaker $gt" >> $FINALRESULT
	
  grep Distortion $RESULTDIR/$testName.result | cut -d ' ' -f4 > $RESULTDIR/$testName.result.dist

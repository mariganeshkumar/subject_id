#!/bin/bash


if [ $# != 5 ]; then
	echo "USAGE: bash computeStat.sh UBMFileName FeatureList trainFeatList valFeatList testFeatList"
	exit
fi

############################    INPUT   #################################
ubmDev=$1
devFeatList=$2
trainFeatList=$3
valFeatList=$4
testFeatList=$5

############################### Initialization ##############################
dumpDir=tmp/statDump
listDumpDir=$dumpDir/lists

scriptsDir=library/ivec/scripts

devStatList=$listDumpDir/devStatList
devWList=$listDumpDir/devWList
devLxList=$listDumpDir/devLxList

trainStatList=$listDumpDir/trainStatList
trainWList=$listDumpDir/trainWList
trainLxList=$listDumpDir/trainLxList


valStatList=$listDumpDir/valStatList
valWList=$listDumpDir/valWList
valLxList=$listDumpDir/valLxList


testStatList=$listDumpDir/testStatList
testWList=$listDumpDir/testWList
testLxList=$listDumpDir/testLxList


ComputeStats=$scriptsDir/svstat.py

nCores=15

rm -rf $dumpDir 
mkdir -p $listDumpDir 

########################        Stats computation      ########################
echo "Statistics computation ..."
less $devFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"



############################# Train data i-vector#####################################

echo "Statistics computation ..."
less $trainFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"



############################# Val data i-vector#####################################
echo "Statistics computation ..."
less $valFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"




############################# Test data i-vector#####################################
echo "Statistics computation ..."
less $testFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

% calls lda and saves the projection and wccn mat

function metalda (wlist, idf, Rnew, umat, wccn, exflag)

    wnames = readlist (wlist);
    listlen = length (wnames);

    ids = load (idf);

    % build data matrix 

    % use the first file to  determine R
    w = load (wnames (1,:));
    R = size (w,1);
    dmat = zeros (R,listlen);
    dmat (:,1) = w;

    % populate the entire matrix
    for i = 2:listlen
        dmat (:,i) = load (wnames (i,:));
    end

    % compute lda and wccn matrices
    [U W] = ldatrain (dmat', ids, Rnew);

    % save them
    save (umat, 'U', '-ascii');
    save (wccn, 'W', '-ascii');

    % exit only if asked to
    if exflag ~= 0
      exit
    end

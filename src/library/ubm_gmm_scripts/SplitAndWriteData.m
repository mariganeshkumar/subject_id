function filenames=SplitAndWriteData(rdTrainDataAreaWise,dir,fileSuffix,noOfFeaturePerFile)
    ind=1;
    filenames = {};
    i=1;
    while ind < size(rdTrainDataAreaWise,1)
         fileID = fopen([dir num2str(ind) '_' fileSuffix '.lfcc'],'w');
         filenames{i}=[dir num2str(ind) '_' fileSuffix '.lfcc'];
         if ind+noOfFeaturePerFile> size(rdTrainDataAreaWise,1)
             noOfFeaturePerFile=size(rdTrainDataAreaWise,1)-ind;
         end
         fprintf(fileID,'%d %d \n',size(rdTrainDataAreaWise,2),noOfFeaturePerFile);     
         for j=1:noOfFeaturePerFile
             fprintf(fileID,'%d ',rdTrainDataAreaWise(ind+j,:));
             fprintf(fileID,'\n');
         end
         ind=ind+j+1;
         i=i+1;
         fclose(fileID);
    end
end
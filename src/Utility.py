import math
import numpy as np
import scipy as sp
import linecache
import io
import struct

class fstream:
  vfv = None
  nvec, dim = -1, -1
  def __init__ (self, fname):
      self.vfv, self.dim, self.nvec = self. __ReadUtterance (fname)

  def __ReadUtterance (self, fname):
    fptr = open (fname, "r")
    if fptr == None:
      return (None, -1, -1)

    #ln = fptr.readline().strip()
    #dim, nvec= map(int,ln.split())
    #fvecs = np.zeros ((nvec,dim))
    #for i in range (0,nvec):
    #  fvecs[i,:] = [ float(f) for f in fptr.readline ().strip ().split ()]
    #fptr.close()
    for i, ln in enumerate (fptr):
       if i == 0:
           dim, nvec = map (int, ln.strip ().split ())
           fvecs = np.zeros ((nvec,dim))
           continue
       fvecs[i-1,:] = [ float(f) for f in ln.strip ().split ()]

    return (fvecs, dim, nvec)

# returns a list of lists containing 
# top C scoring mixtures wrt ubm
def ReadClosestMix (fname):
  fptr = open (fname, "r")
  if fptr == None:
      return None

  mixvals = [ 
              [
                int (val)
                for val in ln.strip ().split ()
              ]
              for ln in fptr.readlines ()
            ]
  return mixvals           

def GetNFromFO (fname):  
    tempf = open (fname, "r")
    M = 0
    for i, ln in enumerate (tempf):
      if i == 0:
          M, D = [ int(j) for j in ln.strip (). split ()]
          Nx = np.zeros ((M,))
      elif i <= M:
          Nx[i-1] = int (ln. strip())
      else:
          break
    tempf.close ()
    return (M,Nx)

def ReadFirstOrderStats (fname):
    tempf = open (fname, "r")
    M, D = [ int(i) for i in tempf.readline ().strip (). split ()]
    Nx = np.zeros ((M,))
    for m in range(0,M):
      Nx[m] = int (tempf.readline (). strip())
    fx = np.zeros ((M,D))  
    for m in range(0,M):
        fx[m,]  = [ float(f) for f in tempf.readline ().strip (). split ()]
    fx = fx.reshape ((M*D,1))    
    tempf.close ()
    return (M,D,Nx,fx)

def ReadIVecEst (fname):
    tempf = open (fname, "r")
    M, D, R = [ int(i.strip ()) for i in tempf.readline ().strip ().split ()]
    wx = np.zeros ((R,1))
    buff = [ float (f) for f in tempf.readline ().strip ().split ()]
    for r in range (0,R):
        wx[r] = buff[r]
    L = np.zeros ((R,R))
    for r in range (0,R):
        L[r,:]  = [ float (f) for f in tempf.readline ().strip ().split ()]
    tempf.close ()
    return (wx,L)      

def ReadLxFromIVecEst (fname):
    M, D, R = [ int(i.strip ()) for i in linecache.getline (fname,1). strip (). split ()]
    L = np.zeros ((R,R))
    for r in range (2,R+2):
        L[r-2,:]  = [ float (f) for f in linecache.getline (fname, r).strip ().split ()]
    linecache.clearcache ()
    return L

def ReadListFromFile (fname):
    f = open (fname, "r")
    l = [ ln.strip() for ln in f ]
    f.close ()
    return l

# this function seems to be faster than 
# numpy's genfromtxt (). plus it uses
# a lot less memory. But this is not
# as generic as genfromtxt ()

def ReadAsciiMatFile (fname):
    f = open (fname, "r")
    l = [ ln.strip() for ln in f ]
    nrows = len (l)
    ncols = len (l[0].split())
    mat = np.zeros ((nrows,ncols))
    for r in range(0,nrows):
        mat[r,] = [ float(fval) for fval in l[r].split()]
    f.close ()
    return mat

def ReadGMM (fname):
    f = open (fname, "r")
    all_lines = f.readlines()
    num_mix = len (all_lines)/2
    dim = len(all_lines[1].strip().split()) / 2
    mvecs = np.zeros ((num_mix,dim)) 
    vvecs = np.zeros ((num_mix,dim))
    wts   = np.zeros ((num_mix,))
    for i in range(0,num_mix):
      wts[i] = float (all_lines[i*2].strip())
      tempfloats = [ float(val) for val in all_lines[i*2+1].strip().split()]
      mvecs[i,] = [ tempfloats[j] for j in range(0,dim*2,2) ]
      vvecs[i,] = [ tempfloats[j] for j in range(1,dim*2,2) ]
    return (num_mix, dim, wts, mvecs, vvecs)

def read_bin_file (fname):
    f = io.open (fname, "rb")
    dim = struct.unpack ('i', f.read(4))[0]
    nvec = struct.unpack ('i', f.read(4))[0]
    fvec = np.zeros ((nvec,dim))
    for i in range(0,nvec):
        for j in range(0,dim):
            fvec[i,j] = struct.unpack('f', f.read(4))[0]
    f.close()
    return fvec

def read_ascii_vfv (fname):
    vfv = None
    with open (fname,"r") as f:
      try:
        dim, nvec = map (int, f.readline().strip().split())
        vfv = np.zeros ((nvec,dim))

        for i in range(0,nvec):
            vfv[i,] = map (float, f.readline().strip().split())
      except:
            print "Error parsing file", fname
    return vfv    

def save_ascii_vfv (fname, vfv):
    f = open(fname,'w')
    (nvec,dim) = np.shape(vfv)
    f.write('%d %d\n' % (dim, nvec))
    jrange = range(dim)
    for i in xrange(0, nvec):
        for j in jrange:
          f.write('%e ' % vfv[i,j])
        f.write('\n')
    f.close()

function [N F S] = load_stats_from_files(physical, logical)

  
% get number of rows in the stats collection
n_utterances = size(physical,1);

% get number of gaussians and the dimensionality
% we do this by opening the first stats file
first       = load(physical{1});

if nargout == 3 && isfield(first, 'S')
  n_S_dims  = size(first.S, 2);
  S         = zeros(n_utterances, n_S_dims);
else
  n_S_dims  = 0;
  S = [];
end

if nargout >= 2 && isfield(first, 'F')
  n_F_dims  = size(first.F, 2);
  F         = zeros(n_utterances, n_F_dims);
else
  n_F_dims  = 0;
  F = [];
end

n_N_dims  = size(first.N, 2);
N         = zeros(n_utterances, n_N_dims);

% load the matrix
for ii = 1:n_utterances
  if nargin == 2
    disp(['Loading [' int2str(ii) '/' int2str(n_utterances) '] ' logical{ii}]);
  end

  cur = load(physical{ii});
  N(ii, :) = cur.N;

  if n_F_dims
    F(ii, :) = cur.F;
  end
    
  if n_S_dims
    S(ii, :) = cur.S;
  end
end

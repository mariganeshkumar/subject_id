/*****************************************************************************
* This program was intended to serve as a binary
* to facilitate channel compensation for experimenting with NIST 2004
* database. It takes a list of waveforms, that will be adapted from
* UBM. A triGaussian is required to do VAD.
****************************************************************************/




#include "AdaptOnlyMeans.h"

VECTOR_OF_F_VECTORS *
AdaptOnlyMeans (VECTOR_OF_F_VECTORS * ubmMeans,
		VECTOR_OF_F_VECTORS * ubmVars,
		float *ubmWeights,
		unsigned int numClusters,
		VECTOR_OF_F_VECTORS * vfv,
		unsigned int numVectors,
		unsigned int relevanceFactor, float probScaleFactor)
{
  VECTOR_OF_F_VECTORS *expectation = NULL, *tempMeans;
  float alpha, beta, normFactorI, maxProb, *logProb;
  unsigned int dimensions, i, j, k, maxProbIdx, *eeta, tempMaxProbIdx;

  VECTOR_OF_F_VECTORS *adaptedMeans;

  dimensions = ubmMeans[0]->numElements;

  logProb = (float *) calloc (vfv[0]->numElements, sizeof (float));
  expectation =
    (VECTOR_OF_F_VECTORS *) calloc (numClusters,
				    sizeof (VECTOR_OF_F_VECTORS));
  tempMeans =
    (VECTOR_OF_F_VECTORS *) calloc (numClusters,
				    sizeof (VECTOR_OF_F_VECTORS));
  for (j = 0; j < numClusters; j++)
    {
      tempMeans[j] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
      for (k = 0; k < vfv[0]->numElements; k++)
	tempMeans[j]->array[k] = 0.0;
    }


  eeta = (unsigned int *) calloc (numClusters, sizeof (unsigned int));
  printf ("Finished initialization\n");
  fflush (stdout);
  for (i = 0; i < numVectors; i++)
    {
      for (j = 0; j < numClusters; j++)
	{
	  logProb[j] =
	    ComputeProbability (ubmMeans[j], ubmVars[j], ubmWeights[j],
				vfv[i], probScaleFactor);

	  if (j == 0 || logProb[j] > maxProb)
	    {
	      maxProb = logProb[j];
	      maxProbIdx = (unsigned int) j;
	    }

	}
//              printf ("feature vector number %d, maxProbIdx is %u\n",i, maxProbIdx);

//              printf ("incrementing %u to %u\n", tempMaxProbIdx, eeta[tempMaxProbIdx]);
      eeta[maxProbIdx] = eeta[maxProbIdx] + 1;
      for (k = 0; k < dimensions; k++)
	{
	  tempMeans[maxProbIdx]->array[k] =
	    tempMeans[maxProbIdx]->array[k] + vfv[i]->array[k];
	}
    }

//      for ( j =0; j < numClusters; j++) printf ("Cluter no : %d, eeta %u \n", j+1, eeta[j]);

  for (j = 0; j < numClusters; j++)
    for (k = 0; k < dimensions; k++)
      {
	if (eeta[j] != 0)
	  tempMeans[j]->array[k] /= ((float) eeta[j]);
      }

  adaptedMeans =
    (VECTOR_OF_F_VECTORS *) calloc (numClusters,
				    sizeof (VECTOR_OF_F_VECTORS));
  for (j = 0; j < numClusters; j++)
    {
      adaptedMeans[j] = (F_VECTOR *) AllocFVector (dimensions);

      alpha =
	(float) ((float) eeta[j] /
		 ((float) eeta[j] + (float) relevanceFactor));
//         printf ("alpha is %f while eeta of %d is %f\n", alpha,j,eeta[j]); 
      for (k = 0; k < dimensions; k++)
	{
	  adaptedMeans[j]->array[k] = (alpha * tempMeans[j]->array[k])
	    + ((1.0 - alpha) * ubmMeans[j]->array[k]);
//              printf ("adaptedMeans %d %d = %f, ubmMeans - %f\n", j,k,adaptedMeans[j]->array[k], ubmMeans[j]->array[k]);
	}
    }
  //free (logProb);*/
  return adaptedMeans;
}

main (int argc, char *argv[])
{
  char *controlFileName, *ubmFileName, *featureName,
    *modelFileName, *trainFileName, *ergSpidModelFileName;
  char wavname[256], *tmpStr;
  char oc;
  FILE *controlFile, *ubmFile, *modelFile, *ergSpidModelFile, *trainFile;
  VECTOR_OF_F_VECTORS *vfv, *allVfv;
  VECTOR_OF_F_VECTORS *ubmMeans = NULL, *ubmVars = NULL, *adaptedMeans;
  VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
  float *ergWts;
  float *ubmWeights;
  ASDF *asdf;
  unsigned int numVectors, ubmSize, featLength, i, j, k, numSpkrs;
  int dumpedFVs, relFactor = 16, batchMode, ipIsList, startCnt, useTriGauss;
	int pfixLen;
  float thresholdScale, psf;

  /* temp variables */
  unsigned int tempNV, tempNV2;

  if (argc < 8)
    {
      printf ("Usage : %s controlFile ubmFile ubmSize wavName ");
      printf ("featureName modelFile thresholdScale \n", argv[0]);
      exit (0);
    }
  /* set flags to zero */  
	/* dumpedFVs signify if feature vectors are dumped or need to be
     calculated 
     useTriGauss is set when tri gaussians are used for VAD
   */
  dumpedFVs = useTriGauss = 0;
  startCnt = 1;
  while ((oc = getopt (argc, argv, "bdlrst:")) != -1)
    {
						printf ("read option %c\n", oc);
      switch (oc)
	{
	case 'd':
	  dumpedFVs = 1;
	  break;
	case 't':
	  useTriGauss = 1;
	  ergSpidModelFileName = optarg;
	  break;
	case 'l':
	  ipIsList = 1;
	  break;
	case 'b':
	  batchMode = 1;
	  break;
	case 's':
	  sscanf (optarg, "%d", &startCnt);
	  if (startCnt < 0)
	    {
	      printf ("Invalid value for -s\n");
	      exit (2);
	    }
	  printf ("modelfile arg will be treated as prefix\n");
	  break;
	case 'r':
	  sscanf (optarg, "%d", &relFactor);
	  if (relFactor <= 0)
	    {
	      printf ("Invalid value for -r\n");
	      exit (2);
	    }
	  break;
	case ':':
	  printf ("error with options\n");
	  exit (2);
	default:
	  break;
	}
    }

  // set argv to point to the rest of the cmd line params
  argv = &argv[optind - 1];

  // cache cmd line params
  controlFileName = argv[1];
  ubmFileName = argv[2];
  sscanf (argv[3], "%u", &ubmSize);
  trainFileName = argv[4];
  featureName = argv[5];
  modelFileName = argv[6]; pfixLen = strlen (modelFileName);
  sscanf (argv[7], "%u", &thresholdScale);

  // open control file and load asdf
  controlFile = fopen (controlFileName, "r");
  asdf = (ASDF *) calloc (1, sizeof (ASDF));
  InitializeStandardFrontEnd (asdf, controlFile);
  Cstore (asdf->fftSize);
	psf = (float) GetFAttribute (asdf, "probScaleFactor");

  // input file for training 
  trainFile = fopen (trainFileName, "r");
  if (trainFile == NULL)
    {
      printf ("Unable to open trainFile\n");
      exit (1);
    }

  // if using tri Gaussian for VAD, read file params
  if (useTriGauss && !dumpedFVs)
    ReadTriGaussianFile (ergSpidModelFileName, &ergMeans, &ergVars, &ergWts);

  // batchMode is set when lots of spkrs need to be trained & each spkr has
  // only one train file. open that list of files.
  if (batchMode)
    {      
						// open the list of files
	    trainFile = fopen (trainFileName, "r");
      if (trainFile == NULL)
	      {
	        printf ("Unable to open input file\n");
	        exit (0);
	      }
			// set no of train files to be 0 and increment count subseqt'ly
       numSpkrs = 0;
			 // tmpStr is multipupose temp string container variable.
			 //		stores train file's name initially. then stores
			 //		model's name
			 tmpStr = (char *) calloc (256, sizeof (char));
		   while (!feof (trainFile))
			 {
							 fscanf (trainFile, "%s", tmpStr);
							 numSpkrs++;
      if (useTriGauss && !dumpedFVs)
	allVfv =
	  ExtractFeatureVectorsVAD (asdf, tmpStr, featureName,
					 &numVectors, ergMeans, ergVars,
					 ergWts);
      // if VAD not reqd but features are not dumped
      else if (!useTriGauss && !dumpedFVs)
	allVfv = ExtractFeatureVectors (asdf, tmpStr,
				       featureName,
				       &numVectors, thresholdScale);
      // if features are dumped
      else if (dumpedFVs)
	allVfv = ReadVfvFromFile (tmpStr, &numVectors);
			
			if (ubmMeans == NULL)
			{
						  featLength = allVfv[0]->numElements;	
							ReadGMMFile (ubmFileName, 
															&ubmMeans, &ubmVars, &ubmWeights, 
															ubmSize, featLength);
			}
			 strcpy (tmpStr, modelFileName);
			 sprintf (&tmpStr[pfixLen],"_%d\0",numSpkrs);
			 adaptedMeans = AdaptOnlyMeans (ubmMeans, ubmVars, ubmWeights,
											 ubmSize, allVfv, numVectors, relFactor,
											 psf);
			 SaveModelToFile (adaptedMeans, ubmVars, ubmWeights,
											 ubmSize, tmpStr);
			 FreeVfv (allVfv, numVectors);
			 free (tmpStr);
			 }
			 fclose (trainFile);
    }
  else {
					printf ("Training in normal mode\n\tUsing tri gaussian %d \n", useTriGauss);
					printf ("\tReading from dumped file %d\n", dumpedFVs);
  // if the input is a list of files, extract features
  if (ipIsList)
    {
	// if triGaussian has to be used    
      if (useTriGauss && !dumpedFVs)
	allVfv =
	  BatchExtractFeatureVectorsVAD (asdf, trainFileName, featureName,
					 &numVectors, ergMeans, ergVars,
					 ergWts);
      // if VAD not reqd but features are not dumped
      else if (!useTriGauss && !dumpedFVs)
	allVfv = BatchExtractFeatureVectors (asdf, trainFileName,
				       featureName,
				       &numVectors, thresholdScale);
      // if features are dumped
      else if (dumpedFVs)
	allVfv = BatchReadVfvFromFile (trainFileName, &numVectors);
    }
  // if input file name is a wave file's name
  else if (!ipIsList && !dumpedFVs && !useTriGauss)
    allVfv = ExtractFeatureVectors (asdf, trainFileName, featureName,
				    &numVectors, thresholdScale);
	else if (!ipIsList && !dumpedFVs && useTriGauss)
					allVfv = ExtractFeatureVectorsVAD (asdf, trainFileName, 
													featureName, &numVectors, ergMeans,
												 ergVars, ergWts);
	else if (!ipIsList && dumpedFVs)
					allVfv = ReadVfvFromFile (trainFileName, &numVectors);
	featLength = allVfv[0]->numElements;
							ReadGMMFile (ubmFileName, 
															&ubmMeans, &ubmVars, &ubmWeights, 
															ubmSize, featLength);
	adaptedMeans = AdaptOnlyMeans (ubmMeans, ubmVars, ubmWeights,
											 ubmSize, allVfv, numVectors, relFactor,
											 psf);
	SaveModelToFile (adaptedMeans, ubmVars, ubmWeights,
											ubmSize, modelFileName);
	}
}

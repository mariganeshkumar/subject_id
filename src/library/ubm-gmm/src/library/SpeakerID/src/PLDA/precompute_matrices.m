function [iPn,iE,log_det_Pn,log_det_E,iEF] = precompute_matrices(F,E,nlist)
    
     nF = size(F,2);
    num_unique = length(nlist);
    eye_nF = eye(nF);
    
    iPn = zeros(nF,nF,num_unique);    
    log_det_Pn = zeros(num_unique,1);
    
    %Invert the noise covariance matrix
    iE = chol(E);
    log_det_E = 2*sum(log(diag(iE)));
    
    %Compute iEF
    iEF = iE\(iE'\F);
    FtiEF = F'*iEF;
    
    %For each value of n compute iPn
    for i = 1:num_unique
        n = nlist(i);       
        iPn(:,:,i) = chol(eye_nF + n*FtiEF);
        log_det_Pn(i) = 2*sum(log(diag(iPn(:,:,i))));
        iPn(:,:,i) = iPn(:,:,i)\(iPn(:,:,i)'\eye_nF);
    end


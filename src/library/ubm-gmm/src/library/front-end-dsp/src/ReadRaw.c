#include "stdio.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"

main (int argc, char *argv[])
{
short *waveform;
char *name;
SP_FILE *wave_file;
FILE *fpout;
int i;
float value;
long num_samples;
name = argv[1];
fpout = fopen(argv[2],"w");

wave_file = sp_open (name, "r");
waveform = ReadRaw(argv[1], &num_samples);
printf("num_samples in main = %d\n",num_samples);
for (i = 0; i < num_samples; i++){
  value = waveform[i];
     if (i % 10000 == 0)
       printf("output waveform[%d]= %d\n",i,waveform[i]);
  fflush(stdout);
  fprintf(fpout,"%d\n ",(waveform[i]));
}
fclose(fpout);
printf("file closed\n");
fflush(stdout);
sp_close(wave_file);
}














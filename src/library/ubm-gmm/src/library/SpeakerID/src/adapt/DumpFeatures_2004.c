#include "CommonFunctions.h"


main (int argc, char *argv[])
{

  char *controlFileName, *featureName, *ergSpidModelFileName, *wavName;
  char *outputFileName, *select_channel_option;
  FILE *controlFile, *ergSpidModelFile;
  FILE *outputFile;
  VECTOR_OF_F_VECTORS *newVfv;
  VECTOR_OF_F_VECTORS *deltaVfv = NULL, *deltaDeltaVfv = NULL;
  VECTOR_OF_F_VECTORS *concatDeltaVfv, *concatAcclVfv;
  float *ergWts;
  VECTOR_OF_F_VECTORS *ergMeans, *ergVars;
  ASDF *asdf;
  unsigned long numVectors;
  unsigned int featLength, i, j, k;
  unsigned int velocity = 0, acceleration = 0, velAcclInput;
  unsigned int deltaDifference, deltaDeltaDifference;
  int op_bin = 0;
  int numGaussiansErg = 3, numSpeechVectors;
  float probScaleFactor, ditherMean, varianceNormalize, varianceFloor;

  if (argc != 7) {
      printf ("Usage : %s controlFile wavName featureName outputFile "
              "energyModelList select_channel\n",
        	  argv[0]);
      return 0;
  }

  controlFileName = argv[1];
  wavName = argv[2];
  featureName = argv[3];
  outputFileName = argv[4];
  ergSpidModelFileName = argv[5];
  if(set_select_channel(argv[6][0]) != 0) return 1;
  fprintf (stderr,"selected channel = %d\n", select_channel);

  controlFile = fopen (controlFileName, "r");
  asdf = (ASDF *) calloc (1, sizeof (ASDF));
  InitializeStandardFrontEnd (asdf, controlFile);
  Cstore (asdf->fftSize);

  ergMeans =
    (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,
				    sizeof (VECTOR_OF_F_VECTORS));
  ergVars =
    (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,
				    sizeof (VECTOR_OF_F_VECTORS));
  ergWts = (float *) calloc (numGaussiansErg, sizeof (float));
  for (i = 0; i < numGaussiansErg; i++) {
      ergMeans[i] = (F_VECTOR *) AllocFVector (1);
      ergVars[i] = (F_VECTOR *) AllocFVector (1);
    }

  ergSpidModelFile = fopen (ergSpidModelFileName, "r");
  if (ergSpidModelFile == NULL) {
      printf ("Could not open energy-Spid Models file..\n");
      fflush (stdout);
      exit (1);
  }
  featLength = 1;
  for (j = 0; j < numGaussiansErg && !feof (ergSpidModelFile); j++)
    {
      fscanf (ergSpidModelFile, "%e", &ergWts[j]);
      printf ("%e\n", ergWts[j]);
      for (k = 0; k < featLength; k++)
	{
	  fscanf (ergSpidModelFile, " %e %e", &ergMeans[j]->array[k],
		  &ergVars[j]->array[k]);
	  printf (" %e %e\n", ergMeans[j]->array[k], ergVars[j]->array[k]);
	}
      fscanf (ergSpidModelFile, "\n");
      printf ("\n");
    }
  if (ergSpidModelFileName != NULL)
    fclose (ergSpidModelFile);

  newVfv = ExtractFeatureVectorsVAD (asdf, wavName,
				     featureName, &numVectors,
				     ergMeans, ergVars, ergWts);

  outputFile = fopen (outputFileName, "w");
  fprintf (outputFile, "%d %d\n", newVfv[0]->numElements, numVectors);
  for (i = 0; i < numVectors; i++) {
      for (j = 0; j < newVfv[i]->numElements; j++)
          fprintf (outputFile, " %f", newVfv[i]->array[j]);
      fprintf (outputFile, "\n");
  }
  fclose (outputFile);
  FreeVfv (newVfv, numVectors);
  free(ergWts);
  FreeVfv(ergMeans,numGaussiansErg);
  FreeVfv(ergVars,numGaussiansErg);

}

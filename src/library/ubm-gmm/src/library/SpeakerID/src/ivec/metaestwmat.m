% call estwmat to estimate W matrix and saves it in a file

function metaestwmat (tmatfile, gmmfile, wmatfile, M, D, R, exflag) 
  W = estwmat (tmatfile, gmmfile, M, D, R);
  save (wmatfile, 'W');
  if exflag ~= 0 
    exit
  end

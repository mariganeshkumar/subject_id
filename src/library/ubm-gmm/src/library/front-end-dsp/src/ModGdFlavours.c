
/* Global definitions for fft computation  */
static     int *iBit=NULL;
static     float *twiddleReal=NULL, *twiddleImag=NULL;


/*****************************************************************
 *  function GeneratePseudoDct generates the discrete cosine
transform 
Inputs - numRows and numColumns
outputs - VECTOR_OF_F_VECTORS melCosineTransform
*******************************************************************/

VECTOR_OF_F_VECTORS  *GeneratePseudoDct (int offset, int numRows, int numColumns)
{
  int                     i, j;
  VECTOR_OF_F_VECTORS     *vfv=NULL;
  float                   *list=NULL;
  float                    normalisingConst;
  vfv = (VECTOR_OF_F_VECTORS *) calloc(numRows, sizeof(VECTOR_OF_F_VECTORS));
  if (vfv == NULL) {
    printf("unable to allocate vfv array \n");
    fflush(stdout);
    _exit(-1);
  }
  normalisingConst = sqrt(2.0/(float)(numColumns));
  for (i = offset; i <= offset+numRows-1; i++){
    vfv[i-offset] = (F_VECTOR *) malloc(1*sizeof(F_VECTOR));
    if (vfv[i-offset] == NULL) {
      printf("unable to allocate vfv FVector\n");
      fflush(stdout);
      _exit(-1);
    }
    list = AllocFloatArray(list,numColumns);
    fflush(stdout);
    vfv[i-offset]->numElements = numColumns;
    for (j = 1; j <= numColumns; j++){
      //      vfv[i-offset]->array[j-1] = (double) cos (PI * (i - 0.5)*(j-1)/numColumns)*normalisingConst;
      list[j-1] = (double) cos (PI * (i - 0.5)*(j-1)/numColumns)*normalisingConst;
    }
    vfv[i-offset]->array = list;
  }
  return(vfv);
}
/* ----------------------------------------------------------------------------


Cfft computes the FT of a complex signal.
        inputs - 
                a - complex signal of length n
                  n - FFT order
                m - m such that 2**m = n
                nsign -  -1  forward
                          1  inverse
        
        outputs - 
                 b - complex array of length n            

-----------------------------------------------------------------------------*/

void Cfft(a, b, m, n, nsign)
     int                   m,n,nsign;
     complex               a[],b[];
{
  int                       nv2,nm1,i,j,ip,k,le,le1,le2,l;
  static                    float log2; 
  static                    int flag = 0;
  complex                   u,t;

  if ((int)pow(2,m)!=n){
    printf("ERROR from Cfft: 2**m != n\n");
    _exit(1);
  }
  if (flag == 0) {
    log2 = log((double)2.0);
    flag = 1;
  } 
  nv2 = n/2;
  nm1 = n-1;
  for ( i=1; i<=n; i++ ) b[iBit[i]] = a[i]; 
  
  for ( i=1; i<=n; i+=2 )
    {
      ip = i+1;
      t = b[ip];
      csub( b[ip],b[i],t ); /* b[ip] = b[i] - t  */
      cadd( b[i],b[i],t );  /* b[i] = b[i] + t   */
    };
  
  for( i=1; i<=n; i+=4 )
    {
      ip = i+2;
      t = b[ip];
      csub( b[ip],b[i],t ); /* b[ip] = b[i] - t  */
      cadd( b[i],b[i],t );  /* b[i] = b[i] + t   */
    };

 for( i=2; i<=n; i+=4 )
    {
     ip = i+2;
     t.re = -nsign * b[ip].im;
     t.im =  nsign * b[ip].re;
     csub( b[ip],b[i],t ); /* b[ip] = b[i] - t  */
     cadd( b[i],b[i],t );  /* b[i] = b[i] + t   */
    };

 for( l=3; l<=m; l++ )
    {
     le2 = (int) (exp(log((double)2.0)*(m-l)) + 
		  (double)0.5);  /* le2 = 2**(m-l) */
     le = (int) (exp(log2*(double)l)+(double)0.5);   /* le = 2**l */
     le1 = le/2;
     for ( j=1; j<=le1; j++ )
        {
         k = (j-1)*le2+1;
         u.re = twiddleReal[k];
         u.im = nsign*twiddleImag[k];
         for ( i=j; i<=n; i+=le )
            {
             ip = i+le1;
             cmul(t,b[ip],u);   /*  t = b[ip]*u  */
             csub( b[ip],b[i],t ); /* b[ip] = b[i] - t  */
             cadd( b[i],b[i],t );  /* b[i] = b[i] + t   */
            };
        };
    };
 if(nsign==1) for ( i=1; i<=n; i++ ) { b[i].re=b[i].re/(float)n;
                                        b[i].im=b[i].im/(float)n; };
}


/*---------------------------------------------------------------------------  

    Rfft computes the FT of a real signal   
    inputs - 
             signal - real array of length nfft
             mfft  -    2**mfft = nfft
             nfft  -  order of fft
             nsign -  -1  forward
		       1  inverse
    outputs -
	     ax - array of real part of  FT
	     ay - array of imaginary part of FT             

-----------------------------------------------------------------------------*/


void Rfft(sig, ax, ay, mfft, nfft, nsign)
 float  sig[], ax[], ay[];
 int  mfft,nfft,nsign;
{
  static                complex *a, *b;
  int i;
  static                int flag = 0;
 if (flag == 0) {
   a = (complex *) malloc((nfft+1)*sizeof(complex));
   b = (complex *) malloc((nfft+1)*sizeof(complex));
   if ((a == NULL) || (b == NULL)) {
     printf("unable to allocate complex array \n");
     _exit(-1);
   }
   flag = 1;
 }
 for (i=1; i<=nfft; i++ ) { a[i].re = sig[i];
                            a[i].im = 0.0;       };
 
 // FFTReal(a,b,mfft,nfft,nsign);
 Cfft(a,b,mfft,nfft,nsign);
 for ( i=1; i<=nfft; i++ ) { ax[i] = b[i].re; ay[i] = b[i].im; };
}

/*--------------------------------------------------------------------------
  Cstore computes the twiddle factors used in FFT computation.
          The twiddle factors are stored in the global arrays
          IBIT, twiddleReal, twiddleImag.                                    
 -------------------------------------------------------------------------*/

 void Cstore(int n)
 /*  int n;*/        /* FFT order */
 {
   int  nv2,nm1,ix,ix1,j,i,k;
   float pi2byn;
   if (iBit == NULL) {
     iBit = (int *) AllocIntArray(iBit,n+1);
     twiddleReal = (float *) AllocFloatArray(twiddleReal,n/2+1);
     twiddleImag = (float *) AllocFloatArray(twiddleImag,n/2+1);
   }
   nv2 = n/2;
   nm1 = n-1;
   iBit[1] = 1;
   iBit[n] = n;
   ix = 0;
   for (i=2; i <= nm1; i++){
     j = 0;
     k = nv2;
     ix1 = ix;     
     while (ix1 >= k) { j = j+k; ix1 = ix1-k; k = k/2; };
     ix = ix + k - j;
     iBit[i] = ix + 1;
   };
   pi2byn = (float)(8.0*atan((double)1.0)/(double)n);
   for (i=1; i <= nv2; i++) {
     k = i-1;
     twiddleReal[i] = (float)cos((double)(pi2byn * k));
     twiddleImag[i] = (float)sin((double)(pi2byn * k));
   }
 }


/*-------------------------------------------------------------------------
 *  LinearTransformationOfFVector performs  a DCT on a F_VECTOR and
 *  outputs an F_VECTOR
 
    Inputs :
         inVect : a vector of type F_VECTOR
         melCepstrumCosineTransform : DCT
    Outputs :
         outVect : a vector of type F_VECTOR
---------------------------------------------------------------------------*/
    
void LinearTransformationOfFVector (F_VECTOR *inVect, VECTOR_OF_F_VECTORS 
				       *melCepstrumCosineTransform, 
				       int numRows, int numCols,
				       F_VECTOR *outVect) {

int                                    i, j;

for (i = 0; i < numRows; i++){
  outVect->array[i] = 0;
  for (j = 0; j < numCols; j++)
    outVect->array[i] = outVect->array[i] + 
      inVect->array[j]*melCepstrumCosineTransform[i]->array[j];
 }
}

/*******************************************************************************
*	Durbin computes the Auto-Regressive coefficients coef of order m and the
*	reflection coefficients refCoef also of order m 
*       Inputs -
*	autoCorr - Autocorrelation coefficients
        m        - order of the prediction filter
        Outputs -
*	resEnergy - residual energy used by LpAnal for gain computation
*       coef      - AR coefficients
*       refCoef   - Reflection Coefficients
******************************************************************************/
void Durbin(double *autoCorr, int m, float *coef, float *refCoef, float *resEnergy){
  static float 		*b;
  double 		sum;
  int     	        i,i1,j,imj,ij1;
  double                Ecopy;
  static int            flag = 0;
  if (flag == 0) { 
    b = (float *) AllocFloatArray(b,(m+2));
    flag = 1;
  }
  Ecopy = autoCorr[1];
  refCoef[1] = -autoCorr[2]/autoCorr[1];
  coef[1] = refCoef[1];
  Ecopy = (1.0-refCoef[1]*refCoef[1])*Ecopy;
  if ((m-1) > 0){
    for(i = 2; i <= m; i++){
      i1 = i-1;
      sum = 0.0;
      for (j = 1; j <= i1; j++){
	ij1 = i-j+1;
	sum = sum+coef[j]*autoCorr[ij1];
      }	   
      refCoef[i] = -(autoCorr[i+1]+sum)/Ecopy;
      coef[i] = refCoef[i];
      for (j = 1; j <= i; j++)
	b[j] = coef[j];
      for (j = 1; j <= i1; j++) {
	imj = i-j;
	coef[j] = coef[j]+refCoef[i]*b[imj];
      }
      Ecopy = Ecopy*(1.0-refCoef[i]*refCoef[i]);
    }
  }
  Ecopy = Ecopy/autoCorr[1];
  *resEnergy = Ecopy;
}
/*******************************************************************************
*	LpAnal performs LP analysis on a signal

*	Inputs -
*	signal - array of length npts

*       outputs - 
*	resEnergy - residual signal of length npts
*	coef - array of AR coefficients of length order
*	gain - energy
**************************************************************************** */

void LpAnal(float *signal, float *resEnergy, int npts, int frameShift, 
	      float *coef, int order, float *gain) {

    static float 	*sw, *swOrg, *refCoef, ee;
    static double       *autoCorr;
    int 	        i,k,j,lim;
    static int          flag = 0;
    static int          order1;
    if (flag == 0) {
      sw = (float *) AllocFloatArray(sw, npts+order+1);
      swOrg = (float *) AllocFloatArray(swOrg, npts+order+1);
      order1 = order+1;
      autoCorr = (double *) calloc(order1+1, sizeof(double));
      refCoef = (float *) AllocFloatArray(refCoef,order1+1);
      flag = 1;
      for (i = 1; i <= order; i++) {
        sw[i] = 0;
        swOrg[i] = 0;
      }
    }
    for (i = 1; i <= npts; i++) {
      sw[i+order] = signal[i]*HamDw(i,npts);
      swOrg[i+order] = signal[i];
    }

    for (k = 1; k <= order1; k++){
      i = k - 1;
      autoCorr[k] = 0.0;
      lim = npts-i;
      for (j = 1; j <= lim; j++)
	autoCorr[k] = autoCorr[k]+sw[j+order]*sw[j+order+i];
    }
    Durbin(autoCorr,order,coef,refCoef,&ee);
    *gain = sqrt(ee*autoCorr[1]);
    /*        printf("gain = %f\n", *Gain); */
    for (j = 1; j <= npts; j++) {
       resEnergy[j] = swOrg[j+order];
      for (k = 1; k <= order; k++)  
	resEnergy[j] = resEnergy[j] + coef[k]*swOrg[j+order-k];
    }
    for (i = 1; i <= order; i++) {
      sw[i] = sw[frameShift+i];
      swOrg[i] = swOrg[frameShift+i];
    }
  }

/************************************************************************
  LogCepSmooth smoothes magnitude amag to give smthAmag using the log cepstrum
          nfft - fft order
          2**mfft = nfft. winlen is Window size.

  Inputs -
          amag[] - array of nfft spectral coefficients
          nfft - FFT order
          mfft - 2**mfft = nfft
	  winlen - cepstral Window length
  Outputs -
          smthAmag[] - smoothed Spectrum of nfft coefficients
	  co - c0 cepstral value
*************************************************************************/
void LogCepSmooth(float amag[], float smthAmag[], int mfft, int nfft,  
		  int winlen,float *c0, float gamma)
{
  static              int flag = 0;
  static float        *resAx, *resAy, *resAxcop,
                      *resAmag, *resPhase;
  int i;
  if (flag == 0) {
    resAx = (float *) AllocFloatArray(resAx, nfft+1);
    resAy = (float *) AllocFloatArray(resAy, nfft+1);
    resAxcop = (float *) AllocFloatArray(resAxcop, nfft+1);
    resAmag = (float *) AllocFloatArray(resAmag, nfft+1);
    resPhase = (float *) AllocFloatArray(resPhase, nfft+1);
    flag = 1;
  } 
 *c0 = 0;
 for ( i=1; i<=nfft; i++ ) {
   if (amag[i] <= 1E-10) 
     resAmag[i] = 1E-10;
   else
     resAmag[i] = 2*log(amag[i]);
   *c0 = *c0 + resAmag[i]*resAmag[i];
 }
 *c0 = sqrt(*c0/nfft);
 Rfft(resAmag, resAx, resAy, mfft, nfft, 1);

 for ( i=2; i<=winlen; i++ )
    {
     resAxcop[i] = resAx[i]*HanW(i,winlen);
     resAxcop[nfft-i+2] = resAxcop[i];
    }
 resAxcop[1] = resAx[1];
 for(i=winlen+1;i<=nfft-winlen+1;i++) 
   resAxcop[i] = 0.0;
 
 Rfft(resAxcop, resAmag, resAy, mfft, nfft, -1);
 SpectrumReal(nfft, resAmag, resAy, smthAmag, resPhase);
 for (i = 1; i <= nfft; i++)
   smthAmag[i] = exp(smthAmag[i]*2*gamma);
}


/************************************************************************
  CepSmooth smoothes magnitude amag to give smthAmag. Order nfft.
              2**mfft = nfft. winlen is Window size.

  Inputs -
          amag[] - array of nfft spectral coefficients
          nfft - FFT order
          mfft - 2**mfft = nfft
	  winlen - cepstral Window length
  Outputs -
          smthAmag[] - smoothed Spectrum of nfft coefficients
	  co - c0 cepstral value
*************************************************************************/
void CepSmooth(float amag[], float smthAmag[], int mfft, int nfft,  
	       int winlen,float *c0, float gamma)
{
  static              int flag = 0;
  static float        *resAx, *resAy, *resAxcop,
                      *resAmag, *resPhase;
  int i;
  if (flag == 0) {
    resAx = (float *) AllocFloatArray(resAx, nfft+1);
    resAy = (float *) AllocFloatArray(resAy, nfft+1);
    resAxcop = (float *) AllocFloatArray(resAxcop, nfft+1);
    resAmag = (float *) AllocFloatArray(resAmag, nfft+1);
    resPhase = (float *) AllocFloatArray(resPhase, nfft+1);
    flag = 1;
  } 
 *c0 = 0;
 for ( i=1; i<=nfft; i++ ) {
   *c0 = *c0 + amag[i]*amag[i];
   if (amag[i] <= 0.000001) 
     resAmag[i] = 0.000001;
   else
     resAmag[i] = exp(gamma*log(amag[i]));
 }
 *c0 = sqrt(*c0/nfft);
 Rfft(resAmag, resAx, resAy, mfft, nfft, 1);

 for ( i=2; i<=winlen; i++ )
    {
     resAxcop[i] = resAx[i]*HanW(i,winlen);
     resAxcop[nfft-i+2] = resAxcop[i];
    }
 resAxcop[1] = resAx[1];
 for(i=winlen+1;i<=nfft-winlen+1;i++) 
   resAxcop[i] = 0.0;
 
 Rfft(resAxcop, resAmag, resAy, mfft, nfft, -1);
 SpectrumReal(nfft, resAmag, resAy, smthAmag, resPhase);
}
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*       The smoothing of |X(k)| is performed using LogCepSmooth
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*/
float *ModGdLogSmthCepstrumNcN(float *signal,int npts,int nfft,int mfft, 
			int winlen, int smthWinSize, float *modGdCepstrum, 
			float alfaP, float alfaN,  
			float gamma, int gdsign, 
			int removeLPhase, int removeMin, int mgdNormalize,
			int medOrder, int startIndex, int endIndex) { 
  static float		*sigTemp;
  static float		*ax, *ay, *amag, *phase, *modGd, *modGdSmth;
  static float		*cepAmag;
  static int            nfBy2, flag = 0;
  static complex        *cSig, *cfSig;
  static complex	*cfTemp1, *cfTemp2, u;
  int		        i, sign;
  float		        c0, abscfTemp, logcfTemp;
  float                 minValue;


  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    if (medOrder != 0)
      modGdSmth = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  LogCepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, gamma);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = (u.re/(cepAmag[i]*cepAmag[i]));
    cfTemp2[i].im = (u.im/(cepAmag[i]*cepAmag[i]));
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /* RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }  
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i + 2] = modGd[i];
      }
      modGd[1] = 0;
    }
    if (medOrder != 0) {
      MedianSmoothArray(modGd, nfBy2, medOrder, modGdSmth);
      for (i = 2; i <= nfBy2; i++)
	modGdSmth[nfft - i + 2] = modGdSmth[i]; 
      modGd = modGdSmth;
    }
    minValue = modGd[IminActual(modGd, nfft)];
    if (removeMin) 
      for (i = 1; i <nfft; i++) 
        modGd[i] = modGd[i] - minValue;
      
    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	modGd[nfft - i+ 2] = 0;
      }
    }
    Rfft(modGd, ax, ay, mfft, nfft, 1);
    for (i = 1; i <= winlen; i++) 
      modGdCepstrum[i-1] = ax[i];
    return(modGdCepstrum);  
}
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*       Smoothing of denominator of group is performed using root
*       cepstral smoothing.
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*/
float *ModGdCepstrumNcN(float *signal,int npts,int nfft,int mfft, 
			int winlen, int smthWinSize, float *modGdCepstrum, 
			float alfaP, float alfaN,  
			float gamma, int gdsign, 
			int removeLPhase, int removeMin, int mgdNormalize,
			int medOrder, int startIndex, int endIndex) { 
  static float		*sigTemp;
  static float		*ax, *ay, *amag, *phase, *modGd, *modGdSmth;
  static float		*cepAmag;
  static int            nfBy2, flag = 0;
  static complex        *cSig, *cfSig;
  static complex	*cfTemp1, *cfTemp2, u;
  int		        i, sign;
  float		        c0, abscfTemp, logcfTemp;
  float                 minValue;


  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    if (medOrder != 0)
      modGdSmth = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,amag,mfft,nfft, smthWinSize, &c0, 2*gamma);
  MedianSmoothArray(amag, nfft, 3, cepAmag);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = (u.re/(cepAmag[i]*cepAmag[i]));
    cfTemp2[i].im = (u.im/(cepAmag[i]*cepAmag[i]));
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /* RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }  
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i + 2] = modGd[i];
      }
      modGd[1] = 0;
    }
    if (medOrder != 0) {
      MedianSmoothArray(modGd, nfBy2, medOrder, modGdSmth);
      for (i = 2; i <= nfBy2; i++)
	modGdSmth[nfft - i + 2] = modGdSmth[i]; 
      modGd = modGdSmth;
    }
    minValue = modGd[IminActual(modGd, nfft)];
    if (removeMin) 
      for (i = 1; i <nfft; i++) 
        modGd[i] = modGd[i] - minValue;
      
    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	modGd[nfft - i+ 2] = 0;
      }
    }
    Rfft(modGd, ax, ay, mfft, nfft, 1);
    for (i = 1; i <= winlen; i++) 
      modGdCepstrum[i-1] = ax[i];
    return(modGdCepstrum);  
}
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*       See K K Paliwal's paper on this.
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*/
float *ProductGdCepstrumNcN(float *signal,int npts,int nfft,int mfft, 
			int winlen, int smthWinSize, float *prodGdCepstrum, 
			float alfaP, float alfaN,  
			float gamma, int gdsign, 
			int removeLPhase, int removeMin, int mgdNormalize,
			int startIndex, int endIndex) { 
  static float		*sigTemp;
  static float		*ax, *ay, *amag, *phase, *prodGd;
  static float		*cepAmag;
  static int            nfBy2, flag = 0;
  static complex        *cSig, *cfSig;
  static complex	*cfTemp1, *cfTemp2, u;
  int		        i, sign;
  float		        c0, abscfTemp, logcfTemp;


  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    prodGd = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,amag,mfft,nfft, smthWinSize, &c0, 2*gamma);
  MedianSmoothArray(amag, nfft, 3, cepAmag);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = u.re;
    cfTemp2[i].im = u.im;
    prodGd[i] = cfTemp2[i].re;
    if (i > 1)
      prodGd[nfft-i+2] = prodGd[i];
  }
/* Reduce dynamic range of prodGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (prodGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(prodGd[i]);
      if (abscfTemp == 0)
	prodGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    prodGd[i] = sign*logcfTemp;
	  else
	    prodGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    prodGd[i] = exp(alfaP*logcfTemp);
	  else 
	    prodGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    prodGd[i] = sign*prodGd[i];
	}
      }
    }  
  /* Bandlimit the signal using minFrequency (startIndex) and maxFrequency (endIndex) */
    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	prodGd[i] = 0.0;
	if (i > 1)
	  prodGd[nfft - i+2] = prodGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	prodGd[i] = 0;
	if (i > 1)
	  prodGd[nfft - i+ 2] = 0;
      }
    }
    Rfft(prodGd, ax, ay, mfft, nfft, 1);
    for (i = 1; i <= winlen; i++) 
      prodGdCepstrum[i-1] = ax[i];
    return(prodGdCepstrum);  
}

/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*
*/
float *ModGdCepstrum(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int removeMin, int startIndex, int endIndex) { 

static  float                     *modGdCepTemp;
static  int                       flag = 0;
int                               i;
if (flag == 0) {
  modGdCepTemp = (float *) calloc(winlen+1, sizeof(float));
  flag = 1;
}

modGdCepTemp = ModGdCepstrumNcN (signal, npts, nfft, mfft, winlen+1, smthWinSize,
				 modGdCepTemp, alfaP, alfaN, gamma, gdsign, 
                                 removeLPhase, removeMin, 0, 0, 
				 startIndex, endIndex);
for (i = 0; i < winlen; i++)
  modGdCepstrum[i] = modGdCepTemp[i+1]/(float)(i+1);
return(modGdCepstrum);
}
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum  from signal where denominator
*       smoothing is performed using LP for the given signal using DCT
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*
*/
float *ModGdCepstrum_LPDCT(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int lpOrder, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex, float scaleDCT) { 
  static float		     *sigTemp, *resEnergy;
  static float		     *ax, *ay, *amag, *phase, *modGd;
  static float		     *lpAmag, *coef;
  static int                 nfBy2, flag = 0;
  static int                 DCTSize;
  static complex             *cSig, *cfSig;
  static complex	     *cfTemp1, *cfTemp2, u;
  static VECTOR_OF_F_VECTORS *discreteCosineTransform;
  static F_VECTOR            *fvect, *modGdFvect;
  int		              i, sign;
  float		              c0, abscfTemp, gain, 
                              logcfTemp, lpAmagGamma;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    lpAmag = (float *) calloc(nfft+1, sizeof(float));
    coef = (float *) calloc(lpOrder+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(npts+1, sizeof(float));
    resEnergy = (float *) calloc(npts+1, sizeof(float));
    DCTSize = endIndex - startIndex + 1;
    discreteCosineTransform = (VECTOR_OF_F_VECTORS *) GeneratePseudoDct(1, winlen, DCTSize);
    fvect = (F_VECTOR *) AllocFVector(nfBy2);
    modGdFvect = AllocFVector(winlen);
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  //  for(i = npts+1; i<= nfft; i++)
  //  sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  //  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  //SpectrumReal(nfft,ax,ay,amag,phase);
  //for( i = 1; i <= nfft; i++)
  //  ax[i] = amag[i];
  //CepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, 2*gamma);
  LpAnal(sigTemp, resEnergy, npts, 0, coef, lpOrder, &gain);
  LPSpectrum (coef, lpOrder, lpAmag, phase, nfft, mfft, gain);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    lpAmagGamma = exp(2*gamma*log(lpAmag[i]));
    cfTemp2[i].re = u.re/lpAmagGamma;
    cfTemp2[i].im = u.im/lpAmagGamma;
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /*RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }   
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }
    /* Bandlimit modGd */

    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	if (i > 1)
	  modGd[nfft - i+ 2] = 0;
      }
    }
  for (i = startIndex; i <= endIndex;i++)
    fvect->array[i- startIndex] = modGd[i - startIndex];
  LinearTransformationOfFVector(fvect, discreteCosineTransform, winlen, DCTSize, modGdFvect);
  for (i = 1; i <= winlen; i++) 
    modGdCepstrum[i-1] = modGdFvect->array[i-1]/(float)scaleDCT;
  return(modGdCepstrum);  
}
/* The following is the flavour that we have been using at IIT Madras */

/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum  for the given signal using DCT
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*
*/
float *ModGdCepstrum_DCT(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex, float scaleDCT) { 
  static float		     *sigTemp;
  static float		     *ax, *ay, *amag, *phase, *modGd;
  static float		     *cepAmag;
  static int                 nfBy2, flag = 0;
  static int                 DCTSize;
  static complex             *cSig, *cfSig;
  static complex	     *cfTemp1, *cfTemp2, u;
  static VECTOR_OF_F_VECTORS *discreteCosineTransform;
  static F_VECTOR            *fvect, *modGdFvect;
  int		              i, sign;
  float		              c0, abscfTemp, 
                              logcfTemp;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    DCTSize = endIndex - startIndex + 1;
    discreteCosineTransform = (VECTOR_OF_F_VECTORS *) GeneratePseudoDct(1, winlen, DCTSize);
    fvect = (F_VECTOR *) AllocFVector(nfBy2);
    modGdFvect = AllocFVector(winlen);
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax, amag,mfft,nfft, smthWinSize, &c0, 2*gamma);
  MedianSmoothArray(amag, nfBy2, 3, cepAmag);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = u.re/cepAmag[i];
    cfTemp2[i].im = u.im/cepAmag[i];
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /*RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }   
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }
    /* Bandlimit modGd */

    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	if (i > 1)
	  modGd[nfft - i+ 2] = 0;
      }
    }
  for (i = startIndex; i <= endIndex;i++)
    fvect->array[i- startIndex] = modGd[i - startIndex];
  LinearTransformationOfFVector(fvect, discreteCosineTransform, winlen, DCTSize, modGdFvect);
  for (i = 1; i <= winlen; i++) 
    modGdCepstrum[i-1] = modGdFvect->array[i-1]/(float)scaleDCT;
  return(modGdCepstrum);  
}

/* Computes the GDelay from the windowed cepstral coefficients */

/****************************************************************************
*	The following function computes  the
*	modified group delay functions from ModGdCepstrum using DCT
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - smoothed modified group delay function
*****************************************************************************
*
*/
float *ModGd_DCT(float *signal,int npts,int nfft,int mfft, 
	     int winlen, int smthWinSize, float *modGd, float alfaP, float alfaN, float gamma, int gdsign, int removeLPhase,
	     int startIndex, int endIndex, float scaleDCT) {
  static float		*ax, *ay, *modGdCepstrum;
  static int            flag = 0;
  int		        i;


  if (flag == 0) {
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    modGdCepstrum = (float *) calloc(winlen, sizeof(float));
    flag = 1;
  }
  modGdCepstrum = (float *) ModGdCepstrum_DCT(signal, npts, nfft, 
					  mfft, winlen, smthWinSize, modGdCepstrum, alfaP, alfaN, gamma, 
					      gdsign, removeLPhase, startIndex, endIndex, scaleDCT);
  ax[1] = 0.0;
  for (i = 2; i <= winlen; i++) {
    ax[i] = modGdCepstrum[i-2]*(i-1);
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+1; i <= nfft-(winlen-1); i++)
    ax[i] = 0.0;
  Rfft(ax,modGd,ay,mfft,nfft,-1);
  return(modGd);
}

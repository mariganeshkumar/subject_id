#!/bin/bash
TESTBINARY=../bin/testmap
CBSIZE=1024
MFOLDER=../models/2003.m.fcd
SPIDMODELS=$MFOLDER/SpidModel
CTRLFILE=../cfiles/fe-ctrl.base
FEATURE=frameCepstrum+frameDeltaCepstrum
TESTLIST=../flists/2003/m.test
GROUNDTRUTH=../flists/2003/GroundTruth
RESULTDIR=$MFOLDER/AllResults
SPKRLIST=../flists/2003/spkrList.m
FINALRESULT=$MFOLDER/finalResult
TEMPFILE=$MFOLDER/temp
UBMFILE=../ubm/n2003.ubm.m
UBMSIZE=1024
CVAL=5
numSpeakers=149
testUtterance() {
	LOOPTEMPFILE=temp.$1
	testName=`basename $2 | cut -d '.' -f1`
	testPath=$2
	echo "$TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testPath 0.0001 0 $UBMFILE $CVAL"
  $TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testPath 0.0001 0 $UBMFILE $CVAL > $LOOPTEMPFILE
	hypothesized=`grep speaker $LOOPTEMPFILE | awk '{print $5}'`
	speaker=`head -$hypothesized $SPKRLIST | tail -1`
	speaker=`basename $speaker | cut -d '.' -f1`
	actual=`grep $testName $GROUNDTRUTH | cut -d ' ' -f2`
	echo "$2 $actual $speaker" >> $FINALRESULT
	
	cp $LOOPTEMPFILE $RESULTDIR/$testName.result
  grep Distortion $RESULTDIR/$testName.result | cut -d ' ' -f4 > $RESULTDIR/$testName.result.dist
}

rm -f $FINALRESULT
touch $FINALRESULT
mkdir -p $RESULTDIR
numCasesParallel=4
numRunning=0
for testCase in `cat $TESTLIST`; do
	if [ $numRunning -eq $numCasesParallel ]; then
		wait
		numRunning=0
	fi
	if [ $numRunning -lt $numCasesParallel ]; then
		testUtterance $numRunning $testCase &
		((numRunning++))
	fi
	
done


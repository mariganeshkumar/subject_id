classifier_name={...
        'K-NN_Channel_Concatnated',...      %1
        'UBM-GMM',...                       %2
        'UBN-GMM_Concatnated',...           %3
        'Super-Vector_SVM ',...             %4
        'lamda-Vector_SVM_99',...           %5
        'lamda-Vector_SVM_Best',...         %6
        'i-vector_SVM',...                  %7
        'i-vector_Cosine',...               %8
        'i-vector_PLDA',...                 %9
        'modified-i-vector_SVM',...         %10
        'modified-i-vector_Cosine',...      %11
        'con-i-vector_Cosine',...           %12
        'con-i-vector_SVM',...                  %13
        'score-fusion-i-vector_Cosine',...  %14
        'score-fusion-i-vector_SVM',...     %15
        'modified-x-vector-classifier',...  %16
        'modified-x-vector_SVM',...         %17
        'modified-x-vector_Cosine',...      %18
        'con-x-vector_Cosine',...           %19
        'con-x-vector_SVM',...              %20
        'score-fusion-x-vector_Cosine',...  %21
        'score-fusion-x-vector_SVM',...     %22
        'x-vector-Classifer',...            %23
        'x-vector_Cosine',...               %24
        'x-vector_SVM',...                  %25
        };
        

classifiers = [7,13,10,15];
mixtures = [256,32,32,128];
for i=1:length(classifiers)
        try
        config = configuration;
        config.val_split = 15;
        config.mixtures=mixtures(i);
        config.test_only=0;
        config.classifier=classifiers(i);
        train_and_test_intersession(config)
        catch
        end
end

classifiers = [7,13,10,15];
mixtures = [256,32,32,128];
splits = [30,60]
for i=1:length(classifiers)
        for j=1:length(splits)
                try
                config = configuration;
                config.val_split = splits(j);
                config.mixtures=mixtures(i);
                config.test_only=1;
                config.classifier=classifiers(i);
                train_and_test_intersession(config);
                catch
                end
        end
end

disp('X-vector systems training')

classifiers = [18,19,21,24];
for i=1:length(classifiers)
        try
        config = configuration;
        config.val_split = 15;
        config.hiddenlayers = [512 256 200];
        config.test_only=0;
        config.classifier=classifiers(i);
        %train_and_test_intersession(config)
        catch
        end
end

classifiers = [18,19,21,24];
splits = [30,60]
for i=1:length(classifiers)
        for j=1:length(splits)
                try
                config = configuration;
                config.val_split = splits(j);
                config.hiddenlayers = [512 256 200];
                config.test_only=1;
                config.classifier=classifiers(i);
                train_and_test_intersession(config);
                catch
                end
        end
end
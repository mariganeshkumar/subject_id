#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "fe/fft.h"
/*******************************************************************************
* 	the Following program computes the FFT Magnitude Spectrum
*	Input: dataFile, FFTOrder, FFTSize, 
*	Output :outPutFile

*******************************************************************************/       void Usage() {
           printf("Usage : ComputeFFT dataInFile FFTOrder FFTSize outputFile\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

       complex          *signalIn, *signalOut;
	int  	        i, j, fftOrder, fftSize, 
			fftSizeBy2;
	char             line[200];
       FILE            *fin, *fout;

 /******************************************************************************/
       if (argc != 5) {
         Usage();
         exit(-1);
       }
       printf("%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);
       fin = fopen(argv[1], "r");
       fout = fopen(argv[4], "w");
       
       sscanf(argv[2], "%d",&fftOrder);
       sscanf(argv[3], "%d",&fftSize);

       signalIn = (complex *) calloc(fftSize, sizeof(complex));
       signalOut = (complex *) calloc(fftSize, sizeof(complex));
       fftSizeBy2 = fftSize/2;
       i = 1;
       while ((fgets (line, 200, fin) != NULL) && (i <= fftSize)){ 
         sscanf(line, "%f", &signalIn[i].re);
         signalIn[i].im = 0;
         i++;
       }
       
       /*for (j = i; j <= 1024; j++){
	 signalIn[j].re = 0;
         signalIn[j].im = 0;
	 }*/
       Cstore(fftSize);
       Cfft(signalIn, signalOut, fftOrder, fftSize, -1);
       for (i = 1; i <= fftSize; i++)
         fprintf(fout, "%f\n",sqrt (signalOut[i].re*signalOut[i].re + signalOut[i].im*signalOut[i].im));
       close(fout);
}




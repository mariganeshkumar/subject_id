import sys

ipf = open (sys.argv[1],'r')
opf = open (sys.argv[2],'w')

lines = ipf.readlines()
nmix = len (lines)
fl = (len(lines[0].strip().split()) - 1) / 2

for i in range(0,nmix):
  l = lines[i].strip().split()
  opf.write ("%s\n" % l[0])
  for j in range(0,fl):
    opf.write (" %s %s" % (l[j+1], l[j+1+fl]))
  opf.write ("\n")

ipf.close()
opf.close()

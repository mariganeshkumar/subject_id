
import argparse
from tqdm import tqdm
import os
import shutil
import numpy as np
import scipy.linalg as sil
import sys
sys.path.insert(0, 'library/svm_scripts')
from libsvmUtil import *
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

parser = argparse.ArgumentParser()
parser.add_argument("--devDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--trainDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--testDataScriptFile", help="Location of script file for test files.",
                    type=str, )

parser.add_argument("--valDataScriptFile", help="Location of script file for script files.",
                    type=str, )
parser.add_argument("--testResultFolder", help="Location of the folder to write test result.",
                    type=str, )
parser.add_argument("--valResultFolder", help="Location of the folder to write test result.",
                    type=str, )


args = parser.parse_args()
tmpDir = "tmp/svd"

if os.path.exists(tmpDir):
    shutil.rmtree(tmpDir)
os.makedirs(tmpDir)


devSuperVectorMatrixFile = tmpDir + '/DevSuperVectorMatrix.txt'
trainSuperVectorMatrixFile = tmpDir + '/TrainSuperVectorMatrix.txt'
testSuperVectorMatrixFile = tmpDir + '/testSuperVectorMatrix.txt'
valSuperVectorMatrixFile = tmpDir + '/valSuperVectorMatrix.txt'

devSuperVectorMatrix = []
trainSuperVectorMatrix = []
testSuperVectorMatrix = []
valSuperVectorMatrix = []

devLabelFile = tmpDir + '/devLabels.txt'
trainLabelFile = tmpDir + '/trainLabels.txt'
testLabelFile = tmpDir + '/testLabels.txt'
valLabelFile = tmpDir + '/valLabels.txt'


with open(args.devDataScriptFile) as f:
    devDataScript = f.readlines()
devDataScript = [x.strip() for x in devDataScript]
devFiles = [x.split(' ')[0] for x in devDataScript]
devLabel = [int(x.split(' ')[1]) for x in devDataScript]


for i in tqdm(range(len(devFiles))):
    filename = devFiles[i]
    iVec = np.genfromtxt(filename);
    devSuperVectorMatrix.append(iVec)


with open(args.trainDataScriptFile) as f:
    trainDataScript = f.readlines()
trainDataScript = [x.strip() for x in trainDataScript]
trainFiles = [x.split(' ')[0] for x in trainDataScript]
trainLabel = [int(x.split(' ')[1]) for x in trainDataScript]

for i in tqdm(range(len(trainFiles))):
    filename = trainFiles[i]
    iVec = np.genfromtxt(filename);
    trainSuperVectorMatrix.append(iVec)


with open(args.valDataScriptFile) as f:
    valDataScript = f.readlines()
valDataScript = [x.strip() for x in valDataScript]
valFiles = [x.split(' ')[0] for x in valDataScript]
valLabel = [int(x.split(' ')[1]) for x in valDataScript]

for i in tqdm(range(len(valFiles))):
    filename = valFiles[i]
    iVec = np.genfromtxt(filename);
    valSuperVectorMatrix.append(iVec)


with open(args.testDataScriptFile) as f:
    testDataScript = f.readlines()
testDataScript = [x.strip() for x in testDataScript]
testFiles = [x.split(' ')[0] for x in testDataScript]
testLabel = [int(x.split(' ')[1]) for x in testDataScript]

for i in tqdm(range(len(testFiles))):
    filename = testFiles[i]
    iVec = np.genfromtxt(filename);
    testSuperVectorMatrix.append(iVec)


devSuperVectorMatrix = np.array(devSuperVectorMatrix)
trainSuperVectorMatrix = np.array(trainSuperVectorMatrix)
valSuperVectorMatrix = np.array(valSuperVectorMatrix)
testSuperVectorMatrix = np.array(testSuperVectorMatrix)


devLabel = np.array(devLabel)
trainLabel = np.array(trainLabel)
valLabel = np.array(valLabel)
testLabel = np.array(testLabel)

np.savetxt(devSuperVectorMatrixFile, devSuperVectorMatrix, fmt="%f", footer='\n', comments='')
np.savetxt(trainSuperVectorMatrixFile, trainSuperVectorMatrix, fmt="%f", footer='\n', comments='')
np.savetxt(valSuperVectorMatrixFile, valSuperVectorMatrix, fmt="%f", footer='\n', comments='')
np.savetxt(testSuperVectorMatrixFile, testSuperVectorMatrix, fmt="%f", footer='\n', comments='')

np.savetxt(devLabelFile, trainLabel, fmt="%d", footer='\n', comments='')
np.savetxt(trainLabelFile, trainLabel, fmt="%d", footer='\n', comments='')
np.savetxt(testLabelFile, valLabel, fmt="%d", footer='\n', comments='')
np.savetxt(testLabelFile, testLabel, fmt="%d", footer='\n', comments='')

print(np.shape(devLabel))
print(np.shape(trainLabel))
print(np.shape(valLabel))
print(np.shape(testLabel))


svdMatrixDir = tmpDir + '/svd_decomp'
if os.path.exists(svdMatrixDir):
    print("SVD directory found. Skipping SVD decomposition")
    V_norm = np.load(args.svdMatrixDir + "/Projection_Matrix.npy")
    s = np.load(args.svdMatrixDir + "/singular_values.npy")
else:
    print("Computing SVD")
    #os.makedirs(args.svdMatrixDir)
    U, s, V = sil.svd(devSuperVectorMatrix, full_matrices=False)
    # U, s, V = ssil.svds(trainSuperVectorMatrix)
    V = np.transpose(V)
    V_norm = V / s
    #V_norm.dump(args.svdMatrixDir + "/Projection_Matrix.npy")
    #s.dump(args.svdMatrixDir + "/singular_values.npy")

total_energy = np.sum(np.power(s, 2))
present_energy = 0
no_of_eigen_values = 0
while present_energy <= 0.99:
    no_of_eigen_values += 1
    present_energy = np.sum(np.power(s[:no_of_eigen_values], 2)) / total_energy
acc=[]
print("Choosing Lamda Space Dimension")

projMat = V_norm[:, 0:no_of_eigen_values]
trainDataProjection = np.matmul(trainSuperVectorMatrix, projMat)
valDataProjection = np.matmul((valSuperVectorMatrix), projMat)
testDataProjection = np.matmul((testSuperVectorMatrix), projMat)

clf = LinearDiscriminantAnalysis(solver='svd')
clf.fit_transform(trainDataProjection, trainLabel)
trainDataProjection = clf.transform(trainDataProjection)
valDataProjection = clf.transform(valDataProjection)
testDataProjection = clf.transform(testDataProjection)

print("Computing Cosine Kernal")

TrainDataKernel = cosine_similarity(trainDataProjection, trainDataProjection)
ValDataKernel = cosine_similarity(valDataProjection, trainDataProjection)
TestDataKernel = cosine_similarity(testDataProjection, trainDataProjection)

# Changing kernal to LIBSVM format
trainIndex = np.expand_dims(np.array(range(1, TrainDataKernel.shape[0] + 1)), axis=1)
valIndex = np.expand_dims(np.array(range(1, ValDataKernel.shape[0] + 1)), axis=1)
testIndex = np.expand_dims(np.array(range(1, TestDataKernel.shape[0] + 1)), axis=1)

TrainDataKernel = np.concatenate((trainIndex, TrainDataKernel), axis=1)
valDataKernel = np.concatenate((valIndex, ValDataKernel), axis=1)
TestDataKernel = np.concatenate((testIndex, TestDataKernel), axis=1)

model = ovotrain(trainLabel, TrainDataKernel, '-s 0 -c 1 -m 1000 -t 4 -q')
pred, ac, testScore = ovopredict(testLabel, TestDataKernel, model)
print(ac)
pred = pred.tolist()

for i in range(len(pred)):
    filename = args.testResultFolder + str(i+1) + '.out'
    with open(filename, 'w') as f:
        f.write("label "+ str(pred[i]) + " \n")
        f.close()

pred, ac, valScore = ovopredict(valLabel, valDataKernel, model)

pred = pred.tolist()
print(ac)
for i in range(len(pred)):
    filename = args.valResultFolder + str(i+1) + '.out'
    with open(filename, 'w') as f:
        f.write("label "+ str(pred[i]) + " \n")
        f.close()

print("Collecting Target and Non-Target Scores")
target_test_scores=[]
non_target_test_scores=[]
target_val_scores=[]
non_target_val_scores=[]
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(testScore[np.where(testLabel==i), :])
    target_scores = np.squeeze(scores[:,np.where(trainLabel==i)])
    non_target_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_test_scores.append(target_scores)
    non_target_test_scores.append(non_target_scores)

    scores = np.squeeze(valScore[np.where(valLabel==i), :])
    target_scores = np.squeeze(scores[:,np.where(trainLabel==i)])
    non_target_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_val_scores.append(target_scores)
    non_target_val_scores.append(non_target_scores)

target_test_scores = np.concatenate(target_test_scores, axis=0)
non_target_test_scores = np.concatenate(non_target_test_scores,axis=0)
target_val_scores = np.concatenate(target_val_scores, axis=0)
non_target_val_scores = np.concatenate(non_target_val_scores,axis=0)

sio.savemat('tmp/scores.mat',{
    'target_test_scores': target_test_scores,
    'non_target_test_scores': non_target_test_scores,
    'target_val_scores': target_val_scores,
    'non_target_val_scores': non_target_val_scores,
    })
     
#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: GroupDelay.c,v 1.1 2000/04/23 02:36:13 hema Exp hema $
 *  File:	GroupDelay.c - Computes the group delay function of
 *               any signal.
 *
 *  Purpose:	Used to compute the group delay function of the LP
 *               Residual
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Thu 9-Mar-2000 12:4:27 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "sp/sphere.h"
#include "fe/front-end-defs.h"
#include "fe/front-end-types.h"
#include "fe/DspLibrary.h"
#include "fe/fmts_library.h"
#include "fe/pth_library.h"
#include "fe/sphere_interface.h"
/*******************************************************************************
* 	the Following program computes the average group delay function 
        from the residual of a given speech utterance and saves it in 
*	a file. 
*	Inputs :
*	Speech data FILE
*	FFT order (nfft, nfft (No. of FFT Stages)
*	FrameLength : length of data frame for processing
*	nFormants : Number of formants to be picked
*	OutPut Filename 
*	Output :
*       GroupDelay  data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : GroupDelay waveFile GroupDelayFile \n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 
        int		maxpts=1024,maxptsby2=512;
	float           *signal, *f;
	int  	        i,k,nfft,mfft,npts,
      	                frameNum,iloc,
			ans,waveType;
        long            nSamp;
	char            ansTemp,fNameGD[100],*ftemp=NULL;
	float 		*gDelay, *gDelaySmoothed, med[4], gdAverage;
	short           *waveform;
        SP_FILE         *waveFile;     
        FILE            *gdFile;
/******************************************************************************/
       if (argc != 3) {
         Usage();
         exit(-1);
       }
       printf("encoding scheme ( pcm - 0/ulaw - 1/text - 2) :");
       scanf("%d",&waveType);
       if (waveType == 0)
	 waveform = (short *) ReadSpherePcm(argv[1], &nSamp);
       else if(waveType == 1)
	 waveform = (short *) ReadSphereUlaw(argv[1], &nSamp);
       else
	 waveform = (short *) ReadText(argv[1],&nSamp);
       printf("nfft,mfft :"); 
       scanf("%d %d",&nfft, &mfft);
       Cstore(nfft);
       printf("frameLength :");
       scanf("%d",&npts);
       fNameGD[0] = '\0';
       ftemp = argv[2];
       strcpy(fNameGD,ftemp);
       signal = (float *) calloc(nfft+1, sizeof(float));
       f  = (float *) calloc(nfft+1, sizeof(float));
       gDelay  = (float *) calloc(nfft+1, sizeof(float));
       if (gDelay == NULL) {
	 printf("unable to allocate space\n");
	 exit(-1);
       }
       gDelaySmoothed  = (float *) malloc((nfft+1)*sizeof(float));
       if (gDelaySmoothed == NULL) {
	 printf("unable to allocate space\n");
	 exit(-1);
       }
       gdFile = fopen(fNameGD,"w");
       if (gdFile == NULL) {
	 printf("cannot open formant file \n");
	 exit(-1);
       }
       frameNum = 0;
       iloc =0;
       while (iloc < nSamp) {	
	 frameNum++;
	 for (i = 1; i <= npts; i++)
	   if ((iloc+i) <= nSamp) 
	     f[i] = waveform[iloc+i];
	   else
	     f[i] = 0.0;
	 for (i = 1; i <=  npts; i++){
	   signal[i] = f[i]*HamDw(i,npts);
		/*                 printf("signal %d = %f\n",i,signal[i]);*/
	   }
	 for (i = npts+1; i <= nfft; i++)
	   signal[i] = 0.0;
	 GroupDelay(signal,npts,nfft,mfft,gDelay);
	 for (i = 1; i <= npts; i++) {
	   if (i != 1)
	     med[1] = gDelay[i-1];
	   else
	     med[1] = gDelay[1];
	   med[2] = gDelay[i];
	   if (i != npts)
	     med[3] = gDelay[i+1];
	   else
	     med[3] = gDelay[npts];
	   gDelaySmoothed[i] = Median (med,3);
	   //	   printf("GDs=%f\n", gDelaySmoothed[i]);
           fflush(stdout);

	 }
	 ComputeAverage(gDelaySmoothed, nfft/2, &gdAverage);
	 // printf("Gd Average = %f\n",gdAverage);
	 //	 for (i = 1; i <= npts; i++)
	 //fprintf(gdFile,"%f %f\n",gDelay[i], gDelaySmoothed[i]);
	   fprintf(gdFile,"%f\n",gdAverage);
	 iloc++;
       }
       fclose(gdFile);
}






/**************************************************************************
 * $Log: GroupDelay.c,v $
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of GroupDelay.c
 **************************************************************************/




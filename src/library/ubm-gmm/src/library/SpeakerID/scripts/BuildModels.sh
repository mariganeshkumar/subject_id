#!/bin/bash

CTRLFILE=../cfiles/fe-ctrl.base
BINARY=../bin/AdaptOnlyMeans_2003
TRAINLIST=../flists/2003/spkrList.m
UBMFILE=../ubm/n2003.ubm.m
UBMSIZE=1024
numSpeakers=`wc -l $TRAINLIST | cut -d ' ' -f1`
RFACTOR=16
FEAT=frameCepstrum+frameDeltaCepstrum

MODELDIR=../models/2003.m.fcd/
mkdir -p $MODELDIR

for lineNo in `seq 1 $numSpeakers`; do
	line=`head -$lineNo $TRAINLIST | tail -1`  
  wavname=`cat $line`
	#Usage : ../bin.pad/AdaptOnlyMeans_2004_fromDumpedFeatures \
	#	 controlFile ubmFile ubmSize featureFile \
	#	 outputModelFile 
	$BINARY $CTRLFILE $UBMFILE $UBMSIZE $wavname $FEAT $MODELDIR/spid_$lineNo.gmm 0.0001 	
	echo "$BINARY $CTRLFILE $UBMFILE $UBMSIZE $wavname $FEAT $MODELDIR/spid_$lineNo.gmm 0.0001"
done

import math
import sys
import GMM as GMM
import numpy as np

argv = sys.argv
if len (argv) != 3:
  print "Usage: python extMeanVec.py ipname opname\n"
  print "Stores the means from a gmm file\n"
  quit ()
ipn, opn = argv[1:3]

mvec = GMM.ReadGMM (ipn).GetMeans ()
nr,nc = np.shape (mvec)
opf = open (opn, "w")
for i in range (0,nr):
  for j in range (0,nc):
    opf.write ("%e\n" % mvec[i,j])
opf.close ()    

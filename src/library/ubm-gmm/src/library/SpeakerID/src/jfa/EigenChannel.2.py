import math
import sys
import numpy as np
import scipy as sp
from tables import *
from GMM import *

M,D,R = map(int,sys.argv[1:4])  # for convenience

vvtmatfile = openFile (sys.argv[4], mode="r")
vvtmat = vvtmatfile.root.vvtmatrix

Smatfile = openFile (sys.argv[5], mode="r")
smattab = Smatfile.root.vmatrix

# its a symmetric matrix. find out the no.of rows in a colvec
nrows = len((vvtmat.read(start=0,stop=1)[0])['colvec'])
sstmat = np.zeros((nrows,nrows))
colno=-1
for svec in vvtmat.iterrows():
  colno+=1
  sstmat[:,colno] = svec['colvec']

from scipy import linalg
la,v = sp.linalg.eig(sstmat)
la = map(abs,la)
la_new = []
for i in range(0,len(la)):
  la_new = [(la[i],i)] + la_new
la_sorted = sorted(la_new)
cno = 0
Umat = np.zeros((nrows,R))
for elem in la_sorted:
  cno += 1
  if cno > R:
    break
  val, idx = elem
  Umat[:,cno-1] = v[:,idx]

print "Finished computing pseudo-eigenchannel directions"

# load the S matrix and multiply it with Umat to get
# the eigenchannel directions
csize = len(((smattab.read (start=0,stop=1))[0])['colvec'])
Vmat = np.zeros ((csize,R))
# since a row is already being read, update each element in the resultant
# matrix so that the row may never be read again
n = -1
for svec in smattab.iterrows ():
  n += 1
  tempfvec = svec['colvec']
  for i in range (0,csize):
    for r in range(0,R):
      Vmat[i,r] += (tempfvec[i] * Umat[n,r])

Smatfile.close ()  
vvtmatfile.close()

#for i in range(0,csize):  
#  for r in range(0,R):
#    print Vmat[i,r]

vmatfile = openFile (sys.argv[6], mode="w", title = "Final EigenChannel Matrix")

class vmatfileDesc (IsDescription):
  colvec = Float32Col (csize)

class vmattabDesc (IsDescription):
  desc = StringCol (256)

vmattab = vmatfile.createTable ("/", 'vmatrix', vmatfileDesc, "Matrix")
row = vmattab.row
for r in range(0,R):
  row['colvec'] = Vmat[:,r]
  row.append()
vmattab.flush ()

desctab = vmatfile.createTable ("/", 'desc', vmattabDesc, "Description")
row = desctab.row
row['desc'] = "%d %d" % (csize,r)
row.append ()
desctab.flush ()

vmattab.close ()

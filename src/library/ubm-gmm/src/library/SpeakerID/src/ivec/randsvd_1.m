% computes rand svd. this one loads the data
% matrix in chunks and then computes
% svd. it also assumes that the dimensionality
% of the data is much larger than number of data
% points

function [U,S,V] = randsvd_1(Alist, k, i, usePowerMethod)
    
    if nargin < 3
        i = 1;
    end
    blocksize = 500;
    Anames = readlist (Alist);    
    n = length (Anames);
    l = k + 2;
    nblocks = ceil (n/blocksize)

    % for each block in the list, keep computing
    % the submatrix of H
    if nargin >= 4 && usePowerMethod
        for ii=1:nblocks
            start = (ii-1) * blocksize + 1;            
            A = read_data_mat (Anames (start:start+blocksize,:)) 
            if ii == 1
                H = zeros (n,l)
            end
            
            % Use only the given exponent
            G = randn(blocksize,l);
            H = H + A*G;
        end
        for j = 2:i+1
            for ii = 1:nblocks
                start = (ii-1) * blocksize + 1;            
                A = read_data_mat (Anames (start:start+blocksize,:)) 
                H = A * (A'*H);
            end
        end 
    else
        % Compute the m×l matrices H^{(0)}, ..., H^{(i)}
        % Note that this is done implicitly in each iteration below.
        H = cell(1,i+1);
        H{1} = A*G;
        for j = 2:i+1
            H{j} = A * (A'*H{j-1});
        end

        % Form the m×((i+1)l) matrix H
        H = cell2mat(H);
    end

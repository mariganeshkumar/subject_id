/****************************************************************************
 *   Function             : A collection of procedures for Gaussian Mixture
 *                        : Modeling
 *   Uses                 : DspLibrary.c, InitAsdf.c
 *   Author               : Hema A Murthy
 *   Last Updated         : May 23 2002
 *   Source               : Rabiner and Juang, Fundamentals of Speech Recogn.
 *   Bugs                 : none known to date
 *****************************************************************************/

#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "InitAsdf.h"
#include "DspLibrary.h"
#include "stdlib.h"
#include "math.h"
#include "VQ_Modified.h"
/****************************************************************************
 *   Function             : InitGMM - initialises the GMMs with a set
 *                        : of mean vectors and variance vectors
 *   Input args           : seed - seed for randomising
 *                        : vfv - vector of featureVectors
 *                        : numMixtures : number of Vectors
 *   Output args          : mixtureMeans - vector of GMM mean vectors
 *                        : mixtureVars  - vector of GMM variance vectors 
 *****************************************************************************/

void InitGMM (VECTOR_OF_F_VECTORS *vfv,int numVectors, 
              VECTOR_OF_F_VECTORS *mixtureMeans,               
	      VECTOR_OF_F_VECTORS *mixtureVars, 
	     int numMixtures, int seed) {

  int                          index;
  int                          i, j;
  int                          random;
  float                        rmax;


  srand(seed);
  printf("seed = %d\n",seed);
  fflush(stdout);
  for (i = 0; i < numMixtures; i++) {
      random = rand();
      rmax = RAND_MAX;
      index = (int) ((float) (random/rmax*numVectors));
     for (j = 0; j <vfv[0]->numElements; j++)
      mixtureMeans[i]->array[j] = vfv[index]->array[j];
    for (j = 0; j <vfv[0]->numElements; j++)
      mixtureVars[i]->array[j] = 1.0;
  }
}

/****************************************************************************
 *   Function             : ComputeProbability - computes euclidean distance
 *                        : distance between two vectors
 *   Input args           : mixtureMean, mixtureVar, priorProb, 
 *                        : probScaleFactor, fvect : input vector 
 *   Outputs              : ComputeDiscrimnant - distance      	  
 *****************************************************************************/

float ComputeProbability(F_VECTOR *mixtureMean, 
			  F_VECTOR *mixtureVar, float priorProb, 
			 F_VECTOR *fvect, float probScaleFactor) {

  int                     i;

  float                   sumProb = 0;
  float                   scale = 0.0;
  float                   floorValue = 0.0;

  // if (prevMean != mixtureMean) {
    for (i = 0; i < fvect->numElements; i++)
      {
	if (mixtureVar->array[i] != 0.0)
	  scale = scale + log(mixtureVar->array[i]);
      } /*  for (..i < fvect->numElements..)  */
    scale = 0.5*(fvect->numElements*log(2.0*PI) + scale);
    scale = log(priorProb) - scale;
    //    prevMean = mixtureMean;
  /*    printf("scale = %f \n", scale);
	scanf("%*c"); */
    //}

sumProb = scale;
for (i = 0; i < fvect->numElements; i++) {
  floorValue =  floorValue +
    (mixtureMean->array[i] - fvect->array[i])*
    (mixtureMean->array[i] - fvect->array[i])
    /(2*mixtureVar->array[i]);
}
  sumProb = sumProb - floorValue; //fvect->numElements;
 /*  printf("sum = %f\n", sum); */
  return(sumProb + log(probScaleFactor));
}  

/****************************************************************************
 *   Function             : ComputeMixtureContribution - determines index of Mixture
 *                        : to which a given vector belongs
 *   Input args           : fvect : input vector to be classified
 *                                  mixtureMeans,  mixtureVars,numMixtures,
 *                                  mixtureWeights, numVectors, probScaleFactor
 *   Outputs              : the contribution for fvect to each mixture      	  
 *****************************************************************************/

float *ComputeMixtureContribution(F_VECTOR *fvect, 
                      VECTOR_OF_F_VECTORS *mixtureMeans, 
                      VECTOR_OF_F_VECTORS *mixtureVars, 
                      int numMixtures, float *mixtureWeights, 
		      float probScaleFactor, float *mixtureContribution) {
  int                 i;
  float               evidence;

  for (i = 0; i < numMixtures; i++)
    if (mixtureWeights[i] == 0)
      mixtureWeights[i] = 1.0E-50;

  mixtureContribution[0] = ComputeProbability (mixtureMeans[0], mixtureVars[0], 
					       mixtureWeights[0], fvect, probScaleFactor);
  evidence = mixtureContribution[0];
  for (i = 1; i < numMixtures; i++) {
      mixtureContribution[i] = ComputeProbability(mixtureMeans[i], 
						  mixtureVars[i], mixtureWeights[i], 
						  fvect, probScaleFactor);
      evidence = LogAdd(evidence, mixtureContribution[i]); 
  }
  for (i = 0; i < numMixtures; i++) {
    mixtureContribution[i] = mixtureContribution[i] - evidence;
    if (mixtureContribution [i] < -100) mixtureContribution[i] = -100;
  }
  return(mixtureContribution);
}

/****************************************************************************
 *   Function             : ComputeGMM - compute GMMs
 *                        : for the given set of vectors
 *   Input args           : vfv - input vectors,
 *                        : numVectors - number of input vectors
 *                        : numMixtures - codebook size 
 *                        : VQIter - number of VQ iterations
 *                        : GMMIter - number of GMM iterations
 *                        : probScaleFactor -- value to scale probabilities
 *                        : ditherMean -- not used
 *                        : varianceNormalize -- use Mahalanobis distance
 *                        : for VQ if set to 1
 *                        : varianceFloor value for flooring variance of GMM
 *                        : seed -- seed for random number generator
 *                        : ubm -- 1 ==> use UBM adaptation
 *                        : expects a model ubm.gmm to be present in directory
 *   Outputs		  : mixtureMeans - array of mixture means
 *			  : mixtureVars - array of mixture vars
 *			  : mixtureWeights - number of elements in
 *			    each mixture
 *****************************************************************************/



void ComputeGMM(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
		VECTOR_OF_F_VECTORS *mixtureMeans, 
		VECTOR_OF_F_VECTORS *mixtureVars, 
		float *mixtureWeights, int numMixtures, 
		int VQIter, int GMMIter, float probScaleFactor,
                int ditherMean, int varianceNormalize, 
		float varianceFloor, int seed, int ubm, int adapt, char *ubmTable) {
  int                            i,j,k;
  static VECTOR_OF_F_VECTORS     *ubmMixtureMeans, *ubmMixtureVars;
  static VECTOR_OF_F_VECTORS     *tempMeans, *tempVars;
  static float                   *tempMixtureWeights;
  float                          *mixtureContribution, *numUBMVectors;
  float                          expValue;
  int                            mixtureNumber;
  int                            featLength, totalUBMVectors = 0;
  int                            flag;
  FILE                           *ubmFile;
  //int                            total;
  //int                            minIndex, maxIndex;
  //int                            minMixtureSize, maxMixtureSize;
  

  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  ubmMixtureMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  ubmMixtureVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  mixtureContribution = (float *) AllocFloatArray(mixtureContribution, numMixtures);
  numUBMVectors = (float *) AllocFloatArray(numUBMVectors, numMixtures);
  for (i = 0; i < numMixtures; i++) { 

    tempMeans[i] = (F_VECTOR *) AllocFVector(featLength);
    tempVars[i] = (F_VECTOR *) AllocFVector(featLength);

    ubmMixtureMeans[i] = (F_VECTOR *) AllocFVector(featLength);
    ubmMixtureVars[i] = (F_VECTOR *) AllocFVector(featLength);
  }
  tempMixtureWeights = (float *) AllocFloatArray(tempMixtureWeights, 
						 numMixtures);
  printf("temp mixtures allocated\n");
  fflush(stdout);

  //InitGMM (vfv, numVectors, mixtureMeans, mixtureVars, numMixtures, seed);
  if (ubm == 0) {
    ComputeVQ(vfv, numVectors, mixtureMeans, mixtureVars,
	      mixtureWeights, numMixtures, varianceNormalize, 
	      ditherMean, VQIter, seed);
    for (i = 0; i < numMixtures; i++)
      mixtureWeights[i] = mixtureWeights[i]/numVectors;
  } else {
    ubmFile = fopen (ubmTable, "r");
    for (j = 0; j < numMixtures; j++) {
      fscanf(ubmFile,"%f %d",&mixtureWeights[j], &numUBMVectors[j]);
      /*      printf("%f\n",speakerModelWts[i]->array[j]);
      fflush(stdout);*/
      for(k = 0; k < featLength; k++) {
        fscanf(ubmFile,"  %f %f",&mixtureMeans[j]->array[k],
	       &mixtureVars[j]->array[k]);       
	/*	        printf(" %f %f\n",speakerModelMeans[i][j]->array[k],
			speakerModelVars[i][j]->array[k]);  
			fflush(stdout);*/
      }      
    }
    fclose(ubmFile);
  }

  for ( k = 0; k < GMMIter; k++) {
    printf(" GMM iteration number = %d\n", k);
    fflush(stdout);
    for (i = 0; i < numMixtures; i++) {
      for (j = 0; j < vfv[0]->numElements; j++) {
	  tempMeans[i]->array[j] = 0;
	if ((ubm == 0) || ((ubm == 1) && (adapt == 1)))
	  tempVars[i]->array[j] = 0;
	else
          tempVars[i]->array[j] = mixtureVars[i]->array[j];
      }
      tempMixtureWeights[i] = 0;
      
    }
    
    /* Compute the mean of each mixture */
    if (ubm == 1) {
      for (i = 0; i < numMixtures; i++) {
	mixtureContribution = (float *) 
	  ComputeMixtureContribution(ubmMixtureMeans[i], 
				     mixtureMeans, 
				     mixtureVars, numMixtures, 
				     mixtureWeights,
				     probScaleFactor, mixtureContribution);
	for (mixtureNumber = 0; mixtureNumber < numMixtures; mixtureNumber++){
	  expValue = numUBMVectors[i]*numexpf(mixtureContribution[mixtureNumber]);
	  if (isnan(expValue)) expValue = 0;
	  tempMixtureWeights[mixtureNumber] = 
	    tempMixtureWeights[mixtureNumber]+expValue;
	  for (j = 0; j < featLength; j++){
	    tempMeans[mixtureNumber]->array[j] = 
	      tempMeans[mixtureNumber]->array[j] + 
	      vfv[i]->array[j]*expValue;
	  }
	}
      }
    for (i = 0; i < numVectors; i++) {
      mixtureContribution = (float *) 
	ComputeMixtureContribution(vfv[i], 
				   mixtureMeans, 
				   mixtureVars, numMixtures, 
				   mixtureWeights,
				   probScaleFactor, mixtureContribution);
      for (mixtureNumber = 0; mixtureNumber < numMixtures; mixtureNumber++){
        expValue = expf(mixtureContribution[mixtureNumber]);
        if (isnan(expValue)) expValue = 0;
	tempMixtureWeights[mixtureNumber] = 
	    tempMixtureWeights[mixtureNumber]+expValue;
	for (j = 0; j < featLength; j++){
	  tempMeans[mixtureNumber]->array[j] = 
	    tempMeans[mixtureNumber]->array[j] + 
	    vfv[i]->array[j]*expValue;
	}
      }
    }  
    
    for (i = 0; i < numMixtures; i++) 
      for (j = 0; j < vfv[0]->numElements; j++)
      tempMeans[i]->array[j] = tempMeans[i]->array[j]/tempMixtureWeights[i];
    

    /* Compute the variance and weights of each mixture */

    if (ubm == 1) {
      for (i = 0; i < numMixtures; i++) {
	mixtureContribution = (float *) 
	  ComputeMixtureContribution(ubmMixtureMeans[i], 
				     mixtureMeans, 
				     mixtureVars, numMixtures, 
				     mixtureWeights,
				     probScaleFactor, mixtureContribution);
	for (mixtureNumber = 0; mixtureNumber < numMixtures; mixtureNumber++){
	  expValue = numUBMVectors[i]*numexpf(mixtureContribution[mixtureNumber]);
	  if (isnan(expValue)) expValue = 0;
	  for (j = 0; j < featLength; j++){
	    tempVars[mixtureNumber]->array[j] = 
	      tempVars[mixtureNumber]->array[j] + 
	      (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	      (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j])
	      *expValue;
	  }
	}
	totalUBMVectors = totalUBMVectors + numUBMVectors[i];
      }
    if ((ubm == 0) || ((ubm == 1) && (adapt == 1))) {  
      for (i = 0; i < numVectors; i++) {
	mixtureContribution = (float *) 
	  ComputeMixtureContribution(vfv[i], 
				     mixtureMeans, 
				     mixtureVars, numMixtures, 
				     mixtureWeights,
				     probScaleFactor, 
				     mixtureContribution);
	for (mixtureNumber = 0; mixtureNumber < numMixtures; mixtureNumber++) {
	  expValue = expf(mixtureContribution[mixtureNumber]);
	  if (isnan(expValue)) expValue = 0;
	  for (j = 0; j < featLength; j++){
	    tempVars[mixtureNumber]->array[j] = 
	      tempVars[mixtureNumber]->array[j] + 
	      (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	      (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j])
	      *expValue;
	  }
	}
      }

      for (i = 0; i < numMixtures; i++) {
	for (j = 0; j < vfv[0]->numElements; j++)
	  tempVars[i]->array[j] = tempVars[i]->array[j]/tempMixtureWeights[i];
	tempMixtureWeights[i] = tempMixtureWeights[i]/(numVectors+UBMVectors);
      }

      /*for (i = 0; i < numMixtures; i++)
	printf("mixWeights %d = %f\n", i, tempMixtureWeights[i]);
	scanf("%*c");*/
    }

    /* Floor variance, adjust means if necessary */

    for (i = 0; i < numMixtures; i++){
      flag = ZeroFVector(tempMeans[i]);
      if ((ubm == 0) || ((ubm == 1) && (adapt == 1))) 
	flag = (flag || ZeroFVector(tempVars[i]));
      if (!flag){
	for (j = 0; j < featLength; j++) {
	  mixtureMeans[i]->array[j] = tempMeans[i]->array[j];
	  if ((ubm == 0) || ((ubm == 1) && (adapt == 1))) {  
	    mixtureVars[i]->array[j] = tempVars[i]->array[j];
	    if (mixtureVars[i]->array[j] < varianceFloor) 
	      mixtureVars[i]->array[j] = varianceFloor;
	  }
	}
	if ((ubm == 0) || ((ubm == 1) && (adapt == 1))) 
	  mixtureWeights[i] = tempMixtureWeights[i];
      } else {
	//	     mixtureMeans[i]->array[j] = tempMeans[i]->array[j];
	if ((ubm == 0) || ((ubm == 1) && (adapt == 1))){ 
	  for (j = 0; j < featLength; j++)
	    mixtureVars[i]->array[j] = varianceFloor;
	  mixtureWeights[i] = 1.0/(float)numVectors;
	  printf("Flooring Mixture %d %d mean = %e var = %e mixWeights = %e\n",
		 i, j, mixtureMeans[i]->array[j],
		 mixtureVars[i]->array[j], mixtureWeights[i]);
	  fflush(stdout);
	}
	
      }
    }
  }

    /*  for (i = 0; i < numMixtures; i++)
    for (j = 0; j < featLength; j++) {
	printf("mean %d %d = %f var %d %d = %f\n",i,j, 
	       mixtureMeans[i]->array[j], i, j, mixtureVars[i]->array[j]);
	fflush(stdout);
	}*/

}

















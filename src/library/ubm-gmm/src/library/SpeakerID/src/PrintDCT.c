/*
 * =====================================================================================
 *
 *       Filename:  PrintDCT.c
 *
 *    Description:  Prints the DCT co-efficents used for a given cepstrum size
 *
 *
 * =====================================================================================
 */

#include "DspLibrary.h"
#include "BatchProcessWaveform.h"

int main (int argc, char *argv[]) {
    int cep,nfilt,i,j, ncep;
    VECTOR_OF_F_VECTORS *dct;
    ASDF *asdf;
    char *cFileName = NULL;
    FILE *cFile;

    if (argc != 2) {
        printf ("Usage: ./printDCT controlFile\n");
        exit(1);
    }
    
    cFileName = argv[1];
    cFile = fopen (cFileName,"r");
    asdf = (ASDF *) malloc(sizeof(ASDF));
    InitializeStandardFrontEnd(asdf,cFile);
    nfilt = asdf->numFilters;
    fclose (cFile);

    dct = asdf->melCepstrumCosineTransform;
    ncep = asdf->numCepstrum;
    printf ("numFilters = %d\n", nfilt);
    

    for (i = 0; i < ncep; i++) {
        for (j = 0; j < nfilt; j++) {
            printf (" %f", dct[i]->array[j]);
        }
        printf ("\n");
    }
}

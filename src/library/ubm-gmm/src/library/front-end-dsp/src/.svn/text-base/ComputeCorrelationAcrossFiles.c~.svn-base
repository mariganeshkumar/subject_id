/*-------------------------------------------------------------------------
 *  ComputeCorrelationAcrossFiles.c - Computes the Frobenius norm between two sequences 
                                      Feature vectors
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi)
 *
 *  Created:        Fri 18-Apr-2014 12:43:11
 *  Last modified:  Fri 18-Apr-2014 18:28:09 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "string.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program computes the FFT Magnitude Spectrum
*	Input: dataFile, FFTOrder, FFTSize, 
*	Output :outPutFile

*******************************************************************************/       
void Usage() {
           printf("Usage : ComputeCorrelation fileList1 fileList2 fileOut featVectorLength Lag\n");
}
/*****************************************************************************/

float main (int argc, char *argv[])
{ 
  FILE                 *fList1=NULL, *fList2=NULL,  *fOut=NULL, *featureFile=NULL;
  VECTOR_OF_F_VECTORS  **featureData1=NULL, **featureData2=NULL;
  VECTOR_OF_F_VECTORS  *outputData=NULL;
  char                 featureFileName[500]; 
  char                 line[5000];
  int                  numFiles1, numFiles2, numFrames, numFramesTemp, featLength;
  int                  i, j, k, l, lag;
  float                sum1, sum2, frobeniusNorm, sigma1Sigma2;
 /******************************************************************************/
  if (argc != 6) {
    Usage();
    exit(-1);
  }
  printf("%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);
  fList1 = fopen(argv[1], "r");
  fList2 = fopen(argv[2], "r");
  fOut = fopen(argv[3], "w");
  sscanf(argv[4], "%d", &featLength);
  sscanf(argv[5], "%d", &lag);
  numFrames = 0;
  numFiles1 = 0;
  rewind (fList1);
  line[0]='\0';
  while (fgets (line, 1500, fList1) != NULL) {
    numFiles1++;
    //    if (fgets (line, 1500, fList1) != NULL) {
      sscanf(line, "%s", featureFileName);
      featureFile = fopen (featureFileName, "r");
      numFramesTemp = 0;
      while (fgets (line, 1500, featureFile) != NULL)
	numFramesTemp++;
      fclose(featureFile);
      //}
    if (numFramesTemp > numFrames) numFrames = numFramesTemp;
  }
  rewind(fList1);
  numFiles2 = 0;
  rewind (fList2);
  line[0]='\0';
  while (fgets (line, 1500, fList2) != NULL) {
    numFiles2++;
    line[0]='\0';
    //if (fgets (line, 1500, fList2) != NULL) {
      sscanf(line, "%s", featureFileName);
      featureFile = fopen (featureFileName, "r");
      numFramesTemp = 0;
      while (fgets (line, 1500, featureFile) != NULL)
	numFramesTemp++;
      fclose(featureFile);
      //}
    if (numFramesTemp > numFrames) numFrames = numFramesTemp;
  }
  featureData1 = (VECTOR_OF_F_VECTORS **) calloc(numFiles1,sizeof(VECTOR_OF_F_VECTORS **));
  featureData2 = (VECTOR_OF_F_VECTORS **) calloc(numFiles2,sizeof(VECTOR_OF_F_VECTORS **));
  i =0;
  rewind(fList1);
  line[0] ='\0';
  while (fgets(line, 1500, fList1) != NULL) {
    sscanf(line, "%s", featureFileName);
    featureData1[i] =  (VECTOR_OF_F_VECTORS *) calloc(numFrames, 
						     sizeof(VECTOR_OF_F_VECTORS));
    featureFile = fopen (featureFileName, "r");
    rewind(featureFile);
    for (j = 0; j < numFrames; j++) {
      featureData1[i][j] = (F_VECTOR *) AllocFVector(featLength);
      if (!feof(featureFile)) {
	for (k = 0; k < featLength; k++)
	  fscanf(featureFile, "%e", &featureData1[i][j]->array[k]);
      }
    }
    fclose(featureFile);
    line[0] ='\0';
    i++;
  }
  fclose(fList1);
  j = 0;
  l = 0;
  rewind(fList2);
  line[0]='\0';
  while (fgets(line, 1500, fList2) != NULL) {
    sscanf(line, "%s", featureFileName);
    featureData2[l] =  (VECTOR_OF_F_VECTORS *) calloc(numFrames, 
						     sizeof(VECTOR_OF_F_VECTORS));
    featureFile = fopen (featureFileName, "r");
    rewind(featureFile);
    for (j = 0; j < numFrames; j++) {
      featureData2[l][j] = (F_VECTOR *) AllocFVector(featLength);
      if (!feof(featureFile)) {
	for (k = 0; k < featLength; k++)
	  fscanf(featureFile, "%e", &featureData2[l][j]->array[k]);
      }
    }
    fclose(featureFile);
    line[0] = '\0';
    l++;
  }
  fclose(fList2);
  outputData = (VECTOR_OF_F_VECTORS *) calloc(numFrames, 
					      sizeof (VECTOR_OF_F_VECTORS));
  for (j = 0; j < numFrames; j++) 
    outputData[j] = (F_VECTOR *)AllocFVector(featLength);
  sum1 = 0;
  for (i = 0; i < numFiles1; i++)
    for (k = 0; k < numFrames-lag; k++) 
      for(l = 0; l < featLength; l++) 
	sum1 = sum1 + featureData1[i][k]->array[l]*featureData1[i][k]->array[l];
  sum1 = sum1/numFiles1;
  sum2 = 0;
  for (i = 0; i < numFiles2; i++)
    for (k = 0; k < numFrames-lag; k++) 
      for(l = 0; l < featLength; l++) 
	sum2 = sum2 + featureData2[i][k]->array[l]*featureData2[i][k]->array[l];
  sum2 = sum2/numFiles2;
  sigma1Sigma2 = sqrt(sum1*sum2);
  for (k = 0; k < numFrames; k++)
    for (l = 0; l < featLength; l++)
      outputData[k]->array[l] = 0.0;
  for (i = 0; i < numFiles1; i++)
    for (j = 0; j < numFiles2; j++) {
      for (k = 0; k < numFrames-lag; k++) 
	for(l = 0; l < featLength; l++) 
	  outputData[k]->array[l] = outputData[k]->array[l] + featureData1[i][k]->array[l]*featureData2[j][k+lag]->array[l]/sigma1Sigma2;
     }
  
  frobeniusNorm = 0;
  for (k = 0; k < numFrames; k++) {
    for (l = 0; l < featLength; l++)
      fprintf(fOut,"%e ", outputData[k]->array[l]/(numFiles1*(numFiles2)));
    fprintf(fOut,"\n");
  }
  for (k = 0; k < numFrames; k++)
    for (l = 0; l < featLength; l++)
      frobeniusNorm = frobeniusNorm + outputData[k]->array[l];
  printf ("Frobenius Norm = %e \n", frobeniusNorm/featLength/numFrames/(numFiles1*(numFiles2)));
  fclose(fOut);
  return(frobeniusNorm);
}					       


/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of ComputeCorrelationAcrossFiles.c
 -------------------------------------------------------------------------*/

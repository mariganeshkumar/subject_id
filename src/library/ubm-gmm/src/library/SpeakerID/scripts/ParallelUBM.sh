# MODEFILE : file that will contain the model built. CHANGE this every
#            time a new model needs to be built
# CODEBOOKSIZE : number of mixtures
MODELFILE=../models/n2003.m.ubm.new
CODEBOOKSIZE=1024

# FEATLISTAPP : a file that contains a list of files that contain 
#               the features. Each feature file is processed in
#               Map-Reduce fashion

FEATLISTAPP=2k3.tr.m.2

# INITLIST : This list is constucted every time the script is run
# INITLISTSIZE : number of files used to initialize GMM parameters            
INITLIST=temp.initlist
INITLISTSIZE=200

# make sure the binaries exist
INITBIN=../bin/Init
VQMapper=../bin/VQIter
VQReducer=../bin/VQComb
GMMMapper=../bin/GMMIter
GMMReducer=../bin/GMMComb
INITOUTFILE=vfinal

# number of VQ and GMM iterations
numVQ=5
numGMM=2

# doInit : unset only restarting from a particular GMM iteration
doInit=1

# unset when skipping VQ steps 
runVQ=1
vqStartIter=1

# runGMM : unset when gmm step is not necessary
runGMM=1
gmmStartIter=1

# initialize vq model

if [ $doInit -eq 1 ]; then
    head -$INITSIZE $FEATLISTAPP > $INITLISTSIZE  
    $INITBIN -i $INITLIST -l -d -k $CODEBOOKSIZE -o $INITOUTFILE
fi

if [ $runVQ -eq 1 ]; then
    for i in `seq $vqStartIter $numVQ`; do
        # parallelize vqiteration 
        rm vq$i.*
        parallel -a $FEATLISTAPP --colsep ' ' $VQMapper -i {1} -d -k $CODEBOOKSIZE -b vfinal -o vq$i.{1/}
        # create list
        ls $PWD/vq$i.* > vqlist
        $VQReducer -i vqlist -o vfinal
        rm vq$i.*
    done
fi

rm vqlist

if [ $runGMM -eq 1 ]; then
    for i in `seq $gmmStartIter $numGMM`; do
        parallel -a $FEATLISTAPP --colsep ' ' $GMMMapper -i {1} -d -k $CODEBOOKSIZE -b vfinal -o gmm$i.{1/}
        ls $PWD/gmm$i.* > gmmlist
        $GMMReducer -i gmmlist -o gfinal
        rm gmm$i.*
    done
fi

rm gmmlist
rm $MODELFILE
python ConvGMM.py gfinal  $MODELFILE

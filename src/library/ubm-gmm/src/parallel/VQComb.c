/*
 * =====================================================================================
 *
 *       Filename:  VQComb.c
 *
 *    Description: Takes a bunch of VQ stats from files and combines them into a model 
 *
 *
 * =====================================================================================
 */

#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {
	 int i,j,k, numClusters, dim, oc;
   char *opname, *ipname, *fn;
   FILE *ipfile, *opfile, *vqfile;
   VFV *mv = NULL, *vv = NULL;
   float *wv = NULL, wts;
   float tmpw, tmpm, tmpv;
   // i : input is a list of files
   // o : output file
   while ( (oc = getopt (argc, argv, "i:o:")) != -1)
	 {
     switch (oc) {
       case 'i' :
         ipname = optarg;
         break;
       case 'o' :
         opname = optarg;
         break;
       default:
         printf ("Invalid arguments to file\n");
         exit(1);
     }
   }

   if (ipname == NULL || opname == NULL) {
     printf ("Incomplete arguments to file\n");
     printf ("Usage: VQComb -i iplist -o opname\n");
     exit (2);
   }

   ipfile = fopen (ipname, "r");
   opfile = fopen (opname , "w");
   if (ipfile == NULL) {
     printf ("Unable to open file(s)\n");
     exit (3);
   }
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
     mv = (VFV*) calloc (numClusters, sizeof(VFV));
     vv = (VFV*) calloc (numClusters, sizeof(VFV));
     wv = (float*) calloc (numClusters, sizeof(float));
     if (mv == NULL || vv == NULL || wv == NULL) {
       printf ("Unable to allocate memory for output vars\n");
       exit (4);
     }       
     finc(i,numClusters,1) {     
       wv[i] = 0.0;
       mv[i] = AllocFVector (dim);
       vv[i] = AllocFVector (dim);
       if (mv[i] == NULL || vv[i] == NULL) {
         printf ("Unable to allocate memory for output vars\n");
         exit (4);
       }
       finc(j,dim,1) { 
         mv[i]->array[j] = 0.0;
         vv[i]->array[j] = 0.0;
       }
     }
     break;
   }
   rewind (ipfile);
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
   finc(i,numClusters,1) {     
       fscanf (vqfile, "%f", &tmpw);
       wv[i] += tmpw;
       finc(j,dim,1) {
         fscanf (vqfile, " %f", &tmpm);
         mv[i]->array[j] += tmpm;
       }
       finc(j,dim,1) {
         fscanf (vqfile, " %f", &tmpv);
         if(isnan(tmpv) || isinf(tmpv)) tmpv = (1E-4 * tmpw);
         vv[i]->array[j] += tmpv;
       }
     }
     fclose (vqfile);
     free (fn);
   }
   
   wts = 0.0;
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       mv[i]->array[j] /= wv[i];
     }
     wts += wv[i];
   }
   printf ("Printing variance values befor normalizing\n");
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       vv[i]->array[j] = 
         (vv[i]->array[j] / wv[i]) - 
         (mv[i]->array[j] * mv[i]->array[j]);
       if (isnan(vv[i]->array[j]) || isinf(vv[i]->array[j])) { 
         printf ("Flooring %d %d %f\n", i, j, mv[i]->array[j]);
         vv[i]->array[j] = 10E-4;
       }
     }     
   }
   finc(i,numClusters,1) {     
     fprintf (opfile, "%f", wv[i]/wts);
     finc(j,dim,1) 
       fprintf (opfile, " %f", mv[i]->array[j]);
     finc(j,dim,1) 
       fprintf (opfile, " %f", vv[i]->array[j]);
     fprintf (opfile, "\n");
   }   
   fclose (ipfile);
   fclose (opfile);
}

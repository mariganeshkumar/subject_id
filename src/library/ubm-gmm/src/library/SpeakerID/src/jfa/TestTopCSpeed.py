import math
import getopt
import sys
import numpy as np
import scipy as sp
from tables import *
#from GMM import *
import GMM as GMM

def ReadUtterance (fname):
  fptr = open (fname, "r")
  print fname
  if fptr == None:
    return None
  ln = fptr.readline().strip()
  dim, nvec= map(int,ln.split())
  fvecs = np.zeros ((nvec,dim))
  for i in range (0,nvec):
    fvecs[i,:] = [ float(f) for f in fptr.readline ().strip ().split ()]
  fptr.close()
  return fvecs

gfile = sys.argv[1]
gmm = GMM.ReadGMM (gfile)

utt = ReadUtterance (sys.argv[2])

gmm.GetTopCMixtures (utt, 5)

% reduce all C matrices to a single matrix. this can be used
% when computeC () is used as map step

function computeC_red (cmatfname, cmatlist,exflag)
        cml = readlistascell (cmatlist);
        ii = length (cml);
        load (cml{i}, '-mat');
        C_base = C;
        clear C;
        for i=2:ii
            load (cml {i});
            C_base = C_base + C;
        end
        
        C = C_base;
        save (cmatfname, 'C', '-mat');
        if exflag ~= 0
            exit
        end


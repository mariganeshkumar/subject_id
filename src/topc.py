import sys
import numpy as np
import getopt
import re
from Utility import *

ipislist = 0
optlist, args = getopt.getopt (sys.argv[1:], "o:d:u:m:lh")
(gmm_file_name, dump_file_name, topc_str) = sys.argv[1:4]

cval = int (topc_str)
(nmix, dim, wts, mvec, vvec) = ReadGMM (gmm_file_name)
cval_indices = range(nmix-cval,nmix)
fvarr = fstream (dump_file_name)
#lvar = -1 * np.sum (np.log (vvec), axis = -1) - 0.5 * np.log (wts)


for i in range (0,fvarr.nvec):
    fvec = -0.5 * (fvarr.vfv[i] - mvec) ** 2 / vvec 
    dist = np.sum (fvec,axis=1) #- lvar
    indices = np.argsort (dist)[cval_indices]
    print re.sub (' +',' ',str(indices).strip("[] "))
    

function [] = train_and_test()
path=genpath('library');
addpath(path);
config = configuration;

feature_dir = config.features_dir;

feature_code = [num2str(config.val_split),...
    '_',num2str(config.feature)];

val_predicted_label =  cell([1 config.feature]);
val_gt_label = cell([1 config.feature]);
val_accuracy = zeros([1 config.feature]);

test_predicted_label = cell([1 config.feature]);
test_gt_label = cell([1 config.feature]);
test_accuracy = zeros([1 config.feature]);
freq_dim = 0;
for t=1:config.trials
    %get all the subjects in the feature directory
    subjects = get_all_sub_dir([feature_dir,'/',feature_code]);
    for i = 1:length(subjects)
        %get all the session for a given subject
        disp(['Loading data for subject ',num2str(i),'/',num2str(length(subjects))])
        session = get_all_sub_dir([...
            feature_dir,...
            '/',feature_code,...
            '/',num2str(subjects{i})
            ]);
        allData.sessionInfo{i}=session;
        for j = 1:length(session)
            recordings = dir([...
                feature_dir,...
                '/',feature_code,...
                '/',num2str(subjects{i}),...
                '/',session{j},...
                '/*.mat'
                ]);
            recordings = {recordings(:).name};
            random_permutation = randperm(length(recordings));
            train_ind=1;
            test_ind=1;
            for l = 1:length(recordings)
                data = load([...
                    feature_dir,...
                    '/',feature_code,...
                    '/',num2str(subjects{i}),...
                    '/',session{j},...
                    '/',recordings{l}
                    ]);
                data = data.feature;
                freq_dim = size(data,2);
                if random_permutation(l) <= 0.7 * length(recordings)
                    allData.ubm_data{i}{j}{train_ind} = data;
                    allData.train_data{i}{j}{train_ind} = data;
                    train_ind = train_ind+1;
                else
                    allData.test_data{i}{j}{test_ind} = data;
                    test_ind = test_ind+1;
                end
                
            end
        end
    end
    allData.subjectsInfo=subjects;
    allData.freq_dim = freq_dim;
    allData.modelSaveDir = [[config.history_save,...
        '/',num2str(config.feature),'/', num2str(t)];
    [val_predicted_label{t}, val_gt_label{t}, test_predicted_label{t}, test_gt_label{t}]=config.classifier_function{config.classifier}(config,allData);
    cm = confusionmat(test_predicted_label{t}, test_gt_label{t});
    test_accuracy(t) = sum(diag(cm))/sum(sum(cm)) * 100;
    cm = confusionmat(val_predicted_label{t}, val_gt_label{t});
    val_accuracy(t) = sum(diag(cm))/sum(sum(cm)) * 100;
    end
    val_predicted_label = cell2mat(val_predicted_label);
    val_gt_label = cell2mat(val_gt_label);
    test_predicted_label = cell2mat(test_predicted_label);
    test_gt_label = cell2mat(test_gt_label);

    result_str = strcat('Results_Chunk',num2str(config.val_split),'_Fe',num2str(config.feature),'_Clas',num2str(config.classifier),'_val.txt');
    WriteReport(result_str, val_accuracy, val_predicted_label,val_gt_label, allData.subjectsInfo);

    result_str = strcat('Results_Chunk',num2str(config.val_split),'_Fe',num2str(config.feature),'_Clas',num2str(config.classifier),'_test.txt');
    WriteReport(result_str, test_accuracy, test_predicted_label,test_gt_label, allData.subjectsInfo);
    end

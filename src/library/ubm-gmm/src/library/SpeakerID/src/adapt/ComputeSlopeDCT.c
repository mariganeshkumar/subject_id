#include "UtilityFunctions.h"
#include "CommonFunctions.h"


main (int argc, char *argv[])
{
  FILE *opfile, *ctrlfile = NULL;
  VECTOR_OF_F_VECTORS *vfv, *mcct;
  F_VECTOR *fvect, *tfv;
  unsigned int numVectors;
  unsigned int dim, i, j, k, nfilters;
  int numCepstrum;
  int nreg, slope;
  float temp_slope, temp_intercept;
  ASDF *asdf = NULL;

  if (argc < 4)
    {
      printf
	("Usage : %s inputfile outputFile numregress [ctrlfile]\n",
	 argv[0]);
      exit (0);
    }
  vfv = ReadVfvFromFile(argv[1], &numVectors);
  sscanf(argv[3], "%d", &nreg);
  dim = vfv[0]->numElements - 1;
  fvect = (F_VECTOR*) AllocFVector(dim);
  opfile = fopen(argv[2], "w");
  if (argc == 5) {
      asdf = (ASDF *) calloc (1, sizeof (ASDF));
      ctrlfile = fopen(argv[4], "r");
      InitializeStandardFrontEnd (asdf, ctrlfile);
      slope = asdf->slopeDCT;
      numCepstrum = asdf->numCepstrum;
      mcct = 
       GetPtrAttribute(asdf,"melCepstrumCosineTransform");
      fclose(ctrlfile);
  }
  if (slope)
      fprintf(opfile, "%d %d\n", numCepstrum, numVectors);
  else
      fprintf(opfile, "%d %d\n", dim, numVectors);
  for (i = 0; i < numVectors; i++) {
        for (j = 0; j < dim; j++) {
            LinReg(vfv[i], j, nreg, &temp_intercept, &temp_slope);
            fvect->array[j] = temp_slope;
        }
        if (slope) {
            tfv = AllocFVector (numCepstrum);
            LinearTransformationOfFVector(fvect,
            mcct,  numCepstrum, dim, tfv); 
            for (j = 0; j < numCepstrum; j++) {
                fprintf(opfile, "%e ", tfv->array[j]);
            }
        }
        else {
            for (j = 0; j < dim; j++) {
                fprintf(opfile, "%e ", fvect->array[j]);
            }
        }
        fprintf(opfile, "\n");
  }
  fclose(opfile);
}

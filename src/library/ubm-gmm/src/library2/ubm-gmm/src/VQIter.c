/*
    This file contains the functions for clustering using VQ

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri

    This file is part of SpeakerID.

    SpeakerID is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {
   int numVectors, numClusters, clusterNumber, featLength;
	 int i,j,k, *clustcnt, dumpFile, featIsBin = 0;
	 float psf, tscale = 0.0, varianceNormalize;
   char *spkrList, *opname;
   FILE *ip = NULL, *op = NULL, *basemodel;
	 ASDF *asdf;
	 VFV *em, *ev, *vfv;
	 float *ew,tempf;
   VFV *tempMeanClusters, *tempVarClusters;
   VFV *clusterMeans, *clusterVars;

	 int oc, useTS = 1, useVad = 0, ipIsList = 0,
			 firstIter = 0;
	 char *basemodelfname, *featureName, *waveFileName,
					 *ctrlFileName = NULL, *ergFileName = NULL;
	 FILE *ctrlFile, *spkrFile;


	 // b : basemodel
	 // v: vad
	 // t: trigaussian
	 // k: numclusters
	 // i : input is a list of files
	 // n : feature name 
	 while ( (oc = getopt (argc, argv, "b:i:v:t:k:f:n:ldeo:")) != -1)
	 {
         switch (oc) {
           case 'b':
             basemodelfname=optarg;
             break;
           case 'l':
             ipIsList = 1;
             break;		 
           case 'i':
             spkrList = optarg;
             break;		 
           case 'v':
             useVad = 1;
             useTS = 0;
             ergFileName = optarg;
           case 't':
             if (useVad == 1)
               break;
             useTS = 1;
             sscanf (optarg, "%f", &tscale);
             break;
           case 'f':
             ctrlFileName = optarg;
             break;
           case 'n':
             featureName = optarg;
             break;
           case 'k':
             sscanf (optarg, "%d", &numClusters);
             break;
           case 'o':           
             opname = optarg;
             break;
           case 'd':
             dumpFile = 1;
             useTS = 0;
             useVad = 0;
             break;
           case 'e':
             featIsBin = 1;
             break;
           default:
             return 1;
         }
	 }				 
	 vfv = NULL;

   if (dumpFile) {
     if (!ipIsList) {
       if (!featIsBin)
       vfv = ReadVfvFromFile (spkrList, &numVectors);        
       else
         vfv = ReadVfvFromBinFile (spkrList, &numVectors);
     }
     else {
       if (!featIsBin)
       vfv = BatchReadVfvFromFile (spkrList, &numVectors);
       else
         vfv = BatchReadVfvFromBinFile (spkrList, &numVectors);
     }
   }
   else {
     ctrlFile = fopen (ctrlFileName, "r");
     asdf = (ASDF *) calloc (1, sizeof (ASDF));
     InitializeStandardFrontEnd (asdf, ctrlFile);
     Cstore (asdf->fftSize);
     psf = (float) GetFAttribute (asdf, "probScaleFactor");
     varianceNormalize = (float) GetFAttribute (asdf, "varianceNormalize");
     fclose (ctrlFile);
     if (!useTS && useVad) {
       ReadTriGaussianFile (ergFileName, &em, &ev, &ew);
     }
     if (ipIsList && useTS) {
       spkrFile = fopen (spkrList, "r");
       if (spkrFile == NULL) {
         printf ("unable to open input list %s\n",spkrList);
         exit(2);
       }
       vfv = ComputeFeatureVectors (asdf, spkrFile, featureName, &numVectors, tscale); 
       fclose (spkrFile);
     }
     else if (ipIsList) 
       vfv = BatchExtractFeatureVectorsVAD (asdf, spkrList, featLength, &numVectors,
                                            em, ev, ew);
     
     else if (!ipIsList && useTS) 
       vfv = ExtractFeatureVectors (asdf, spkrList, featureName, &numVectors, tscale);
    
     else if (!ipIsList) 
       vfv = ExtractFeatureVectorsVAD (asdf, spkrList, featureName, &numVectors,
                                       em, ev, ew);
   }
     
   featLength = vfv[0]->numElements;
   clustcnt = (int *) calloc (numClusters, sizeof(int));

   clusterMeans = (VFV*) calloc (numClusters, sizeof(VFV));
   clusterVars = (VFV*) calloc (numClusters, sizeof(VFV));

    tempMeanClusters = (VFV *) calloc (numClusters, sizeof(VFV));
    tempVarClusters = (VFV *) calloc (numClusters, sizeof(VFV));
    for (i = 0; i < numClusters; i++) {
      tempMeanClusters[i] = (F_VECTOR *) AllocFVector(featLength);
      tempVarClusters[i] = (F_VECTOR *) AllocFVector(featLength);
      clusterMeans[i] = (F_VECTOR*) AllocFVector(featLength);
      clusterVars[i] = (F_VECTOR*) AllocFVector(featLength);
      for (j = 0; j < featLength; j++) {
        tempMeanClusters[i]->array[j] = 0.0;
        tempVarClusters[i]->array[j] = 0.0;
      }
   }
    //printf("temp clusters allocated\n");
    fflush(stdout);
   basemodel = fopen (basemodelfname,"r");
   finc(k,numClusters,1) {
     fscanf (basemodel,"%e", &tempf);
     finc(j,featLength,1) 
       fscanf(basemodel," %e", &clusterMeans[k]->array[j]);                        
     
     finc(j,featLength,1) 
       fscanf(basemodel," %e", &clusterVars[k]->array[j]);                        
     fscanf(basemodel,"\n");
   }
   fclose (basemodel);
   //printf ("Finished reading base file\nnumClusters = %d", numClusters);
    finc(i,numVectors,1) {
      clusterNumber = DecideWhichCluster(vfv[i], 
					 clusterMeans, clusterVars, 
					 numClusters, 0);
      clustcnt[clusterNumber]++;
      finc(j,featLength,1) {
        tempMeanClusters[clusterNumber]->array[j] +=  vfv[i]->array[j];
        tempVarClusters[clusterNumber]->array[j] += 
            (vfv[i]->array[j] * vfv[i]->array[j]);
      }
    }
   op = fopen (opname ,"w");
   fprintf (op, "%d %d\n", featLength, numClusters);
    finc(k,numClusters,1) {
      fprintf (op, "%d", clustcnt[k]);
      finc(j,featLength,1) 
        fprintf (op, " %e", tempMeanClusters[k]->array[j]);
      finc(j,featLength,1) 
        fprintf (op, " %e", tempVarClusters[k]->array[j]);
      fprintf(op, "\n");        
    }
   fclose (op);
   FreeVfv (tempMeanClusters, numClusters);
   FreeVfv (tempVarClusters, numClusters);
   FreeVfv (vfv, numVectors);
}

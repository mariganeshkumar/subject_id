mkdir -p dumpforfiles_$1/topc_files_$1
mkdir -p dumpforfiles_$1/stat_files_$1
while IFS='' read -r line || [[ -n "$line" ]]; do
    basename="${line##*/}"
    echo "$basename start"    
    python topc.py dumpforfiles_$1/full_spkid_ubm.model_vq $line 1 > dumpforfiles_$1/topc_files_$1/$basename.topc
    echo "$basename topc done"
    python fostat.py -u dumpforfiles_$1/full_spkid_ubm.model_vq -d $line -m dumpforfiles_$1/topc_files_$1/$basename.topc -o dumpforfiles_$1/stat_files_$1/$basename.stat

done < "dumpforfiles_$1/tmp_lfcc_train_$1.lst"

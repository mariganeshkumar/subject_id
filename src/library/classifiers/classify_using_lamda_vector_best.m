function [val_predicted_label, val_gt_label, test_predicted_label, test_gt_label] = classify_using_lamda_vector_best(config,allData)
train_data = allData.train_data;
test_data = allData.test_data;
num_subjects=length(train_data);

system(['rm -rf ', config.tmp_dir]);
mkdir(config.ubm_data_dir);
mkdir(config.ubm_temp_dir);
mkdir(config.ubm_model_dir);
mkdir([config.ubm_val_data_dir,'/result']);
mkdir([config.ubm_test_data_dir,'/result']);

data_dir=[config.ubm_data_dir,'/'];
ubm_data=allData.ubm_data;
filenames = cell([1, num_subjects]);
clear allData.ubm_data;
disp('Preparing UBM Data');
parfor i=1:length(ubm_data)
    disp(['Subject : ', num2str(i)]);
    subject_data=[];
    for j=1:length(ubm_data{i})
        for k=1:length(ubm_data{i}{j})
            recording_data=ubm_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            recording_filename=strcat(data_dir,num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc');
            [dim, no_of_frames] = size(reshaped_recoding_data);
            dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
            dlmwrite(recording_filename,reshaped_recoding_data','-append','delimiter',' ');
            subject_data=[subject_data reshaped_recoding_data];
        end
    end
    filenames{i}=SplitAndWriteData(subject_data',data_dir,num2str(i),2500)
end
scriptFileID = fopen([config.ubm_data_dir  '/scriptFile.scp'],'w');
listFileID = fopen([config.ubm_model_dir  '/devDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_model_dir  '/devDataLabelFile.scp'],'w');
for i =1:length(ubm_data)
    for j =1:length(filenames{i})
        fprintf(scriptFileID,'%s \n',filenames{i}{j});
        recording_filename=strcat(filenames{i}{j});
        fprintf(listFileID,'%s\n',recording_filename);
        ivector_filename=strcat(filenames{i}{j},'.stat');
        fprintf(labelFileID,'%s %d\n',ivector_filename, i);
    end
end
fclose(scriptFileID);
fclose(listFileID);
fclose(labelFileID);



clear ubm_data allData.ubm_data

system(['sh library/ubm_gmm_scripts/SupervisedGMM.sh ',...
    config.ubm_data_dir,'/scriptFile.scp ' ...
    config.ubm_model_dir,'/UBM_GMM.txt ',...
    num2str(config.mixtures),' ',...
    config.ubm_temp_dir,' ',num2str(1331)]);


disp(['Adapting Models']);
data_dir=config.ubm_train_data_dir;
mkdir(data_dir);
parfor i=1:length(train_data)
    t=cputime;
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_data=train_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            [dim, no_of_frames] = size(reshaped_recoding_data);
            recording_filename=strcat(data_dir,'/',num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc');
            dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
            dlmwrite(recording_filename,reshaped_recoding_data','-append','delimiter',' ');
        end
    end
    disp(['Wrote subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end



listFileID = fopen([config.ubm_model_dir  '/trainDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_model_dir  '/trainDataLabelFile.scp'],'w');
for i=1:length(train_data)
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_filename=strcat(data_dir,'/',num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc');
            fprintf(listFileID,'%s\n',recording_filename);
            ivector_filename=strcat(data_dir,'/',num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc.stat');
            fprintf(labelFileID,'%s %d\n',ivector_filename, i);
        end
    end
end
fclose(listFileID);
fclose(labelFileID);





clear train_data allData.train_data
disp(['Testing Models']);
test_filenames = cell([1, num_subjects]);
val_filenames = cell([1, num_subjects]);
val_data_dir = [config.ubm_val_data_dir];
mkdir(val_data_dir);
test_data_dir = [config.ubm_test_data_dir];
mkdir(test_data_dir);

val_per = config.val_per;

parfor i=1:length(test_data)
    t=cputime;
    val_ind=1;
    test_ind=1;
    test_filenames{i}={};
    val_filenames{i}={};
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            [dim, no_of_frames]=size(reshaped_recoding_data);
            if k <= length(test_data{i}{j}) * val_per
                subject_filename=strcat(val_data_dir,'/',num2str(i),'_',num2str(val_ind),'.lfcc');
                val_filenames{i}{val_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                val_ind=val_ind+1;
            else
                subject_filename=strcat(test_data_dir,'/',num2str(i),'_',num2str(test_ind),'.lfcc');
                test_filenames{i}{test_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                test_ind=test_ind+1;
            end
        end
    end
    disp(['Tested subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end

listFileID = fopen([config.ubm_val_data_dir  '/valDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_val_data_dir  '/valDataLabelFile.scp'],'w');
val_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(val_filenames{i})
        temp_name=split(val_filenames{i}{j},'/');
        adapted_filename = [config.ubm_val_data_dir,'/',temp_name{end}];
        fprintf(listFileID,'%s\n',adapted_filename);
        fprintf(labelFileID,'%s %d\n',[adapted_filename,'.stat'], i);
        val_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(labelFileID);
fclose(listFileID);


listFileID = fopen([config.ubm_test_data_dir  '/testDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_test_data_dir  '/testDataLabelFile.scp'],'w');

test_gt_label=[];
ind=1;
for i =1:num_subjects
    for j =1:length(test_filenames{i})
        temp_name=split(test_filenames{i}{j},'/');
        adapted_filename = [config.ubm_test_data_dir,'/',temp_name{end}];
        fprintf(listFileID,'%s\n',adapted_filename);
        fprintf(labelFileID,'%s %d\n',[adapted_filename,'.stat'], i);
        test_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(labelFileID);
fclose(listFileID);



system(['bash library/ivec/scripts/computeStat.sh ',...
    config.ubm_model_dir,'/UBM_GMM.txt_py ',...
    config.ubm_model_dir  '/devDataScriptFile.scp ' ,...
    config.ubm_model_dir,'/trainDataScriptFile.scp ',...
    config.ubm_val_data_dir,'/valDataScriptFile.scp ',...
    config.ubm_test_data_dir,'/testDataScriptFile.scp ']);

system(['python ',...
    'library/svm_scripts/TrainAndTestSVDSVMbest.py ',...
    '--devDataScriptFile tmp/ubm_model/devDataLabelFile.scp ',...
    '--trainDataScriptFile tmp/ubm_model/trainDataLabelFile.scp ',...
    '--valDataScriptFile tmp/ubm_val/valDataLabelFile.scp ',...
    '--testDataScriptFile tmp/ubm_test/testDataLabelFile.scp ',...
    '--testResultFolder tmp/ubm_test/result/ ',...
    '--valResultFolder tmp/ubm_val/result/ ']);

val_predicted_label=[];
for i=1:length(val_gt_label)
    file_ID=fopen([config.ubm_val_data_dir,'/result/', num2str(i),'.out'],'r');
    val_predicted_label(i) = fscanf(file_ID,'label %d \n');
    fclose(file_ID);
end


test_predicted_label=[];
for i=1:length(test_gt_label)
    file_ID=fopen([config.ubm_test_data_dir,'/result/', num2str(i),'.out'],'r');
    test_predicted_label(i) = fscanf(file_ID,'label %d \n');
    fclose(file_ID);
end

end


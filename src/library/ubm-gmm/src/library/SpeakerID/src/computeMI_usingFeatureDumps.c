
/**
* Program to compute mutual infomration.
This uses discrete mi (by doing a VQ in the feature space.)

This reads features from feature dump.

The difference with the earlier pgm is that this accepts an input
waveform, rather than a speaker table (to use with NIST2003)

In this version, we dont do feature extraction. This is due to
the problems caused by having two control files. So we use feature dumps.

Usage:
computeMI_usingFeatureDumps featureDumpFile0 dim0 featureDumpFile1 dim1 numVectors

and will return one number (the mi between feature0 and feature1)

Typically, feature0 is spectrum and feature1 is a feature (say MFCC)

*/

#include "stdio.h"
#include "stdlib.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "VQ_Modified.h"
#include "DspLibrary.h"
#include "GMM.h"
#include "math.h"


struct Mapp {
	// typically, this is the original short-time spectrum
	int feat0Index;
	// typically, this is a reconstructed spectrum X_m or X_g
	int feat1Index;
};
typedef struct Mapp Map;


void usage();
Map **findMap(VECTOR_OF_F_VECTORS *vfv0,VECTOR_OF_F_VECTORS *clusterMeansFeat0,
	VECTOR_OF_F_VECTORS *clusterVarsFeat0,float *clusterWtsFeat0,
	int numClustersFeat0, VECTOR_OF_F_VECTORS *vfv1,
	VECTOR_OF_F_VECTORS *clusterMeansFeat1,VECTOR_OF_F_VECTORS *clusterVarsFeat1,
	float* clusterWtsFeat1,int numClustersFeat1, int numVectors,
	int varianceNormalize);
void buildFreqCounters(Map** mapArray, int *feat0ClusterFreq,
	int *feat1ClusterFreq, int **crossClusterFreq, int numClustersFeat0,
	int numClustersFeat1, int numFrames);
double computeMI(int *feat0ClusterFreq,int *feat1ClusterFreq,
	int **crossClusterFreq,int numClustersFeat0, int numClustersFeat1,
	int numFrames);
double computeMIRanbir(int *feat0ClusterFreq,int *feat1ClusterFreq,
	int **crossClusterFreq,int numClustersFeat0, int numClustersFeat1,
	int numFrames);



int main(int argc, char* argv[]) {

	//ASDF *asdf0, *asdf1;
	// feature vector 0 and 1
	VECTOR_OF_F_VECTORS *vfv0, *vfv1;
	// number of frames in all training files
	int numVectors, featisbin;
	// dimension of feature 0 and 1
	int dim0, dim1;
	// egy threshold
	//float egyThreshold;
	//char *cname0, *feature0, *cname1, *feature1, *trainWav;
	//FILE *cFile, *spkFile;
	FILE *dumpFile = NULL;
	char *dumpFile0 = NULL, *dumpFile1 = NULL;
	// cluster means and vars for feature 0
	VECTOR_OF_F_VECTORS *clusterMeansFeat0 = NULL, *clusterVarsFeat0 = NULL;
	// clsuter means and vars for feature 1
	VECTOR_OF_F_VECTORS *clusterMeansFeat1 = NULL, *clusterVarsFeat1 = NULL;
	// cluster weights
	float *clusterWtsFeat0 = NULL, *clusterWtsFeat1 = NULL;
	// mutual info variables
	double mi, miRanbir, mi2;
	// Map array for all frames; 2D for just convinence 
	Map **mapArray = NULL; 
	// for storing frequencies of occurance
	int *feat0ClusterFreq = NULL, *feat1ClusterFreq = NULL, 
	**crossClusterFreq = NULL;
	// misc
	int i,j;

	// some constants
	// We avoid reading these values from the
	// control files because there are two 
	// control files and the values might differ.
	//float THRESHOLD_SCALE = 0.0;
	int NUM_CLUSTERS_FEAT0 = 32;
	int NUM_CLUSTERS_FEAT1 = 32;
	int SEED = 1931;
	//int SEED = 1609;
	int VARIANCE_NORMALIZE = 0;
	float DITHER_MEAN = 3.0;
	int NUM_VQ_ITER = 10;

	if (argc != 4) {
		usage();
		exit(-1);
	}

	printf("Starting to read feature dumps... \n");
	// read feature0 vfv from feature0_dump
	// alloc mem for vfv0
    sscanf(argv[3], "%d", &featisbin);
    if (!featisbin) {
        vfv0 = ReadVfvFromFile (argv[1], &numVectors);
        vfv1 = ReadVfvFromFile (argv[2], &numVectors);
    }
    else {
        vfv0 = ReadVfvFromBinFile (argv[1], &numVectors);
        vfv1 = ReadVfvFromBinFile (argv[2], &numVectors);
    }

    dim0 = vfv0[0]->numElements;
    dim1 = vfv1[0]->numElements;
	// start reading from dumpfile_0

	printf("Done reading both feature dumps.\n");

	// Now features are extracted in vfv0 and vfv1. Do the mi part next.
	
	// Cluster vfv0
	printf("Starting to build codebook for feature0: \n"); 
	fflush(stdout);
	// init clusters
	clusterMeansFeat0 = (VECTOR_OF_F_VECTORS *) calloc(NUM_CLUSTERS_FEAT0,
		sizeof(VECTOR_OF_F_VECTORS));
	clusterVarsFeat0 = (VECTOR_OF_F_VECTORS *) calloc(NUM_CLUSTERS_FEAT0,
		sizeof(VECTOR_OF_F_VECTORS));	
	// start forming codebook: init
	for (i = 0;i<NUM_CLUSTERS_FEAT0;i++) {
		clusterMeansFeat0[i] = (F_VECTOR *) AllocFVector(dim0);
		clusterVarsFeat0[i] = (F_VECTOR *) AllocFVector(dim0);
	}	
  	clusterWtsFeat0 = (float *) calloc(NUM_CLUSTERS_FEAT0, sizeof(float));
  
	ComputeVQ(vfv0, numVectors, clusterMeansFeat0, clusterVarsFeat0, 
		clusterWtsFeat0, NUM_CLUSTERS_FEAT0, VARIANCE_NORMALIZE, DITHER_MEAN,
		NUM_VQ_ITER, SEED);
	printf("Done building codebook. \n");
	
	
	printf("---- DEBUG: Printing cluster wts feat0---- \n");
	for(i=0;i<NUM_CLUSTERS_FEAT0;i++) {
		printf("%g ",clusterWtsFeat0[i]);
	}
	printf("\n---- end printing cluster wts feat0-----\n");
	
	// Cluster vfv1
	printf("Starting to build codebook for feature0\n"); 
	fflush(stdout);
	// init clusters
	clusterMeansFeat1 = (VECTOR_OF_F_VECTORS *) calloc(NUM_CLUSTERS_FEAT1,
		sizeof(VECTOR_OF_F_VECTORS));
	clusterVarsFeat1 = (VECTOR_OF_F_VECTORS *) calloc(NUM_CLUSTERS_FEAT1,
		sizeof(VECTOR_OF_F_VECTORS));
	
	// start forming codebook: init
	for (i=0;i<NUM_CLUSTERS_FEAT1;i++) {
		clusterMeansFeat1[i] = (F_VECTOR *) AllocFVector(dim1);
		clusterVarsFeat1[i] = (F_VECTOR *) AllocFVector(dim1);
	}	
	printf("Starting to build codebook feature1\n");
	fflush(stdout);
  	clusterWtsFeat1 = (float *) calloc(NUM_CLUSTERS_FEAT1, sizeof(float));
  
	// build codebook
	//InitVQ(vfv1, numVectors, clusterMeansFeat1, clusterVarsFeat1,
	//	NUM_CLUSTERS_FEAT1, SEED);
	ComputeVQ(vfv1, numVectors, clusterMeansFeat1, clusterVarsFeat1, 
		clusterWtsFeat1, NUM_CLUSTERS_FEAT1, VARIANCE_NORMALIZE, 
		DITHER_MEAN, NUM_VQ_ITER, SEED);
	printf("Done building codebook. \n");
	
	printf("---- DEBUG: Printing cluster wts feat1 ---- \n");
	for(i=0;i<NUM_CLUSTERS_FEAT1;i++) {
		printf("%g ",clusterWtsFeat1[i]);
	}
	printf("\n---- end printing cluster wts -----\n");

	// Now both vfvs are clustered. Form frequency counters.
	
	mapArray = (Map**) findMap(vfv0,clusterMeansFeat0,clusterVarsFeat0,
		clusterWtsFeat0,NUM_CLUSTERS_FEAT0,vfv1,clusterMeansFeat1,
		clusterVarsFeat1, clusterWtsFeat1,NUM_CLUSTERS_FEAT1,numVectors,
		VARIANCE_NORMALIZE);

	/*
	printf("-----Going to print map: \n");
	for(i=0;i<numVectors;i++) {
		printf("[%d] feat0 clstr [%d] mapped to feat1 clstr [%d] \n",i,
		mapArray[i]->feat0Index,mapArray[i]->feat1Index);
	}
	printf("------Done. \n\n");
	*/
	
	// allocate mem for freq arrays
	feat0ClusterFreq = (int*) calloc(NUM_CLUSTERS_FEAT0,sizeof(int));
	feat1ClusterFreq = (int*) calloc(NUM_CLUSTERS_FEAT1,sizeof(int));
	crossClusterFreq = (int**)calloc(NUM_CLUSTERS_FEAT1,sizeof(int*));
	for(i=0;i<NUM_CLUSTERS_FEAT1;i++)
		crossClusterFreq[i] = (int*)calloc(NUM_CLUSTERS_FEAT0,sizeof(int));
	
	buildFreqCounters(mapArray, feat0ClusterFreq, feat1ClusterFreq,
		crossClusterFreq, NUM_CLUSTERS_FEAT0, NUM_CLUSTERS_FEAT1, numVectors);

	// verbose printing
	/*
	printf("Printing feat0 cluster frequencies: \n");
	for(i=0;i<NUM_CLUSTERS_FEAT0;i++)
		printf("Cluster feat0 [%d] freq = [%d] \n",i,feat0ClusterFreq[i]);
	
	printf("Printing feat1 cluster frequencies: \n");
	for(i=0;i<NUM_CLUSTERS_FEAT1;i++)
		printf("Cluster feat1 [%d] freq = [%d] \n",i,feat1ClusterFreq[i]);
	
	printf("Printing cross cluster frequencies \n");
	for(i=0;i<NUM_CLUSTERS_FEAT0;i++) {
		for(j=0;j<NUM_CLUSTERS_FEAT1;j++)
			printf("clus[%d][%d] = %d \t",i,j,crossClusterFreq[i][j]);
		printf("\n");
	}
	*/

	mi = computeMI(feat0ClusterFreq,feat1ClusterFreq,crossClusterFreq,
		NUM_CLUSTERS_FEAT0,NUM_CLUSTERS_FEAT1,numVectors);
	
	//printf("\n MI = %g \n",mi);

	miRanbir = computeMIRanbir(feat0ClusterFreq,feat1ClusterFreq,crossClusterFreq,
		NUM_CLUSTERS_FEAT0,NUM_CLUSTERS_FEAT1,numVectors);

	//printf("Result: mi = %f miRanbir = %f \n\n",mi, miRanbir);
	printf("Result: mi = %f \n",mi);

	// cleanup
	free(vfv0); free(vfv1);
	free(clusterMeansFeat0); free(clusterMeansFeat1);
	free(clusterVarsFeat0); free(clusterVarsFeat1);
	free(clusterWtsFeat0); free(clusterWtsFeat1); 

} // end main


/*
VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *trainWav,
	char *featureName, int *numVectors, float thresholdScale) {
		
	F_VECTOR *fvect;
	VECTOR_OF_F_VECTORS *vfv;
	char line[500];
	int i;
	int frameNo;
	char wavname[500];
	float energy, threshold;

	printf("DEBUG: in ComputeFeatureVectors() %s \n",featureName);
	//GsfOpen_zeroFix(asdf,trainWav);
	GsfOpen(asdf,trainWav);
	vfv  = (VECTOR_OF_F_VECTORS *) malloc(asdf->numFrames*sizeof(
		VECTOR_OF_F_VECTORS));
	if (vfv == NULL) {
		printf ("ERROR: Unable to allocate space for vfv \n");
		exit(-1);
	}
	frameNo = 0;
	printf("DEBUG: thresholdScale = %f\n",thresholdScale);
	threshold = (float) computeThreshold(asdf, thresholdScale);
	printf("DEBUG: threshold = %f\n",threshold);
	printf("DEBUG: Stating loop\n");
	printf("DEBUG: Num frames = %d\n",asdf->numFrames);
	printf("DEBUG: Num frames 2 = %d\n",asdf->numFrames);
	printf("Will this print?");
	fflush(stdout);
	for (i = 0; i < asdf->numFrames; i++) {
		//printf("DEBUG:[[Processing frame %d ]] ",i);
		fflush(stdout);
		energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
		//printf("DEBUG: energy = %f ",energy); fflush(stdout);
		if (energy >= threshold) {
			fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
			if(fvect == NULL) {
				printf("Problems extracting feature %s \n",featureName);
				fflush(stdout);
				exit(-1);
			}
			vfv[frameNo] = fvect;
			frameNo++;
		}
	} // end for loop 
	printf("DEBUG: End of loop\n");
	GsfClose(asdf);
	*numVectors = frameNo;
	//printf("Total no of training frames = %d\n",*numVectors);
	printf("Num frames = %d num used = %d (%.2f\%)\n",asdf->numFrames,
		*numVectors,((float)(*numVectors)*100/(asdf->numFrames)));
	return(vfv);
} // end computeFeatureVectors()
*/


/*
Not sure whats this for. Some threshold measurement.
*/

/*
float computeThreshold(ASDF *asdf, float threshold) {
	int i;
	float ave = 0;
	for (i = 0; i < asdf->numFrames; i++)
		ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
	ave = ave/asdf->numFrames;
	return (ave*threshold);
} // enc computeThreshold()
*/


/*
Usage
*/
void usage() {
	printf("\n Usage: computeMI_usingFeatureDumps featureDumpFile0 featureDumpFile1 featisbin \n");
}




/*
 * Find which cluster in feat0 maps to which cluster in feat1.
 */
Map **findMap(VECTOR_OF_F_VECTORS *vfv0,VECTOR_OF_F_VECTORS *clusterMeansFeat0,
	VECTOR_OF_F_VECTORS *clusterVarsFeat0,float *clusterWtsFeat0,
	int numClustersFeat0, VECTOR_OF_F_VECTORS *vfv1,
	VECTOR_OF_F_VECTORS *clusterMeansFeat1,VECTOR_OF_F_VECTORS *clusterVarsFeat1,
	float* clusterWtsFeat1,int numClustersFeat1, int numVectors,
	int varianceNormalize) {

	int i;
	int clusterFeat0, clusterFeat1;
	Map **mapArray = NULL;

	// allocate Map array
	mapArray = (Map**)calloc(numVectors,sizeof(Map*));
	for(i=0;i<numVectors;i++)
		mapArray[i] = (Map*)calloc(1,sizeof(Map));

	for (i=0;i<numVectors;i++) {
	// do for feat0 (spectrum)
		clusterFeat0 = DecideWhichCluster(vfv0[i], clusterMeansFeat0,
			clusterVarsFeat0, numClustersFeat0, varianceNormalize);
	// do for feat1 (feature)
		clusterFeat1 = DecideWhichCluster(vfv1[i], clusterMeansFeat1, 
			clusterVarsFeat1, numClustersFeat1, varianceNormalize);

		mapArray[i]->feat0Index = clusterFeat0;
		mapArray[i]->feat1Index = clusterFeat1;
	}
	return mapArray;

} // end findMap()



/*
 * Finds number of elements in each cluster in the Map.
 */

void buildFreqCounters(Map** mapArray, int *feat0ClusterFreq,
	int *feat1ClusterFreq, int **crossClusterFreq, int numClustersFeat0,
	int numClustersFeat1, int numFrames) {

	int i,j,k;
	int count;

	// for feature 0, number of elements in each cluster
	for(i=0;i<numClustersFeat0;i++) {
		count=0;
		for(j=0;j<numFrames;j++) {
			if(mapArray[j]->feat0Index == i)
				count++;
		}
	//	printf("count = %d \n",count);
		feat0ClusterFreq[i]=count;
	} // end outer for

	// for feat1
	for(i=0;i<numClustersFeat1;i++) {
		count=0;
		for(j=0;j<numFrames;j++) {
			if(mapArray[j]->feat1Index == i)
				count++;
		}
		feat1ClusterFreq[i]=count;
	} // end outer for

	// for cross (joint prob.)
	// num of elements for each cluster pair
	// f0 = spectrum
	// f1 = feature
	for(i=0;i<numClustersFeat0;i++) {
		for(j=0;j<numClustersFeat1;j++) {
			count=0;
			for(k=0;k<numFrames;k++) {
				if((mapArray[k]->feat0Index == i) && 
					(mapArray[k]->feat1Index==j))
						count++;
			} // end k
			crossClusterFreq[i][j]=count;
		} // end j
	} // end i
	
} // end buildFreqCounters()



double computeMI(int *feat0ClusterFreq,int *feat1ClusterFreq,
	int **crossClusterFreq,int numClustersFeat0, int numClustersFeat1,
	int numFrames) {

		// entropies
		double Hfeat1, Hfeat1GivenFeat0;
		double prob;
		int i,j;
		double temp;

		// we want to find H(feat1)-H(feat1|feat0)
		// compute entropy: H(feat1)
		// note: 
		// feat0 = spectrum
		// feat1 = feature
		// we want H(f1)-H(f1|f0)
		
		Hfeat1 = 0.0;
		for(i=0;i<numClustersFeat1;i++) {
			// Prob is computed as num(y)/numFrames
			prob = (double)feat1ClusterFreq[i]/numFrames;
			//printf("Prob d[%d] = %d/%d =  %g \n",i,featClusterFreq[i]
			//,numFrames,prob);
			if (prob != 0)
				Hfeat1 = Hfeat1 + (-1.0)*(prob) * log2(prob);
		}
		printf("Hfeat1 = %g \n", Hfeat1);
	
		// P(f|X) = num(f,X)/num(X)
		Hfeat1GivenFeat0 = 0.0;
		for(j=0;j<numClustersFeat0;j++) {
			temp = 0.0;
			for(i=0;i<numClustersFeat1;i++) {
				// Prob of (y|x) is num(x,y)/num(x)
				prob = (double)crossClusterFreq[j][i]/feat0ClusterFreq[j];
				if (prob != 0)
					temp = temp + 
						(-1.0)*(prob) * log2(prob);
			}
			Hfeat1GivenFeat0 = Hfeat1GivenFeat0 + 
				(temp * feat0ClusterFreq[j]/numFrames);
		}
		printf("Hfeat1GivenFeat0 = %g \n",Hfeat1GivenFeat0);

		// I(Y;X) = H(Y)-H(Y|X)
		return Hfeat1-Hfeat1GivenFeat0;

} // end computeMI()


/*
 * Ranbir's method.
 */
double computeMIRanbir(int *feat0ClusterFreq,int *feat1ClusterFreq,
	int **crossClusterFreq,int numClustersFeat0, int numClustersFeat1,
	int numFrames) {

	int i,j;
	double mi = 0.0;
	double probCiDj, probCi, probDj;
	
	for(i=0;i<numClustersFeat0;i++)
		for(j=0;j<numClustersFeat1;j++) {
			probCiDj = (double) crossClusterFreq[i][j]/numFrames;
			probCi = (double) feat0ClusterFreq[i]/numFrames;
			probDj = (double) feat1ClusterFreq[j]/numFrames;
			//printf("DEBUG: [i=%d][j=%d] pCiDj = %f pCi = %f pDj = %f \n",
			//	i,j,probCiDj,probCi,probDj);
			if((probCiDj != 0) && (probCi != 0) && (probDj != 0)) {
				mi = mi + probCiDj * log2(probCiDj/(probCi*probDj));
				//printf("DEBUG: intermediate mi ranbir %g \n",mi);
			}
		}
	return(mi);

} // end computeMIRanbir()










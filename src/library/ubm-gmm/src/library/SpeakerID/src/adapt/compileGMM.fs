#!/bin/tcsh -fx
setenv DSP_DIR /home/srikanth/Research/front-end-dsp.melSlope/src
setenv NISTINC /home/srikanth/nist/include
setenv NISTLIB /home/srikanth/nist/lib
#setenv GCC "/usr/bin/gcc -g"
#setenv GCC "/usr/bin/gcc -O2 -ffast-math"
setenv GCC "/usr/bin/gcc -ffast-math -g"

$GCC   	$1.c \
	${DSP_DIR}/DspLibrary.c \
        ${DSP_DIR}/BatchProcessWaveform.c \
	${DSP_DIR}/SphereInterface.c \
        ${DSP_DIR}/InitAsdf.c \
        ${DSP_DIR}/LoadASDF.c \
	${DSP_DIR}/VQ_Modified.c\
	${DSP_DIR}/NewWeightedGMM.c \
        ${DSP_DIR}/GMM.c \
    ${DSP_DIR}/HashTable.c \
        ${DSP_DIR}/FeatureMappingToFunctions.c \
	-I${DSP_DIR}/fe \
        -I$NISTINC \
        -L$NISTLIB \
        -lsp -lutil -lm \
	-o ../bin/$1

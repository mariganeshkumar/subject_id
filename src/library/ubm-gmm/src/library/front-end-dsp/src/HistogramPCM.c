/**************************************************************************
 *  $Id$
 *  File:	ReadSpherePcm.c - Convert a SpherePcm file to txt
 *
 *  Purpose:	
 *
 *  Author:	Hema A Murthy
 *
 *  Date:	Thu Apr  3 16:30:20 IST 2008
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"
#include "stdlib.h"

int main (int argc, char *argv[])
{
  short            *waveform;
  char             *listOfInputFiles, name[500], line[500];
  SP_FILE          *waveFile;
  FILE             *fpin, *fpout;
  int              i;
  float            value;
  long             numSamples;
  unsigned long    totalNumSamples = 0;
  float            histogram[65535];
  if (argc != 3) {
    printf("Usage: ReadSpherePcm ListOfInputFiles OutHistogram\n");
    exit(-1);
  }
  listOfInputFiles = argv[1];
  fpout = fopen(argv[2],"w");
  fpin = fopen(argv[1], "r");
  for (i = 0; i < 65535; i++)
    histogram[i] = 0.0;
  while (fgets (line, 500, fpin) != NULL) {
    sscanf(line, "%s", name);
    //    waveFile = sp_open (name, "r");
    waveform = ReadSpherePcm(name, &numSamples);
    printf("num_samples in main = %ld\n",numSamples);
    for (i = 0; i < numSamples; i++){
      histogram[waveform[i]+32768]++;
      if (i % 10000 == 0)
	printf("histogram[%d]= %f\n",waveform[i]+32768, histogram [waveform[i]+32768]);
      fflush(stdout);
    }
    totalNumSamples = totalNumSamples+numSamples;
  fflush(stdout);
  free(waveform);
  }
  for (i = 0; i < 65535; i++)
    fprintf (fpout, "%d %f\n", i, histogram[i]/totalNumSamples);
  fclose(fpout);
  printf("file closed\n");
  return(0);
}















/**************************************************************************
 * $Log$
 *
 *                        End of ReadSpherePcm.c
 **************************************************************************/

/*-------------------------------------------------------------------------
 *  TestUtteranceGMM.c - Tests a given utterance against speaker models
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:57:03
 *  Last modified:  Wed 07-May-2003 15:24:17 by hema
 *  $Id: TestUtterance.c,v 1.1 2002/04/30 09:36:19 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/


/************************************************************************
  Function            : test_Utterance- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numGMM numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, InitAsdf.c,BatchProcessWaveform.c
                         SphereInterface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-02
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "BatchProcessWaveform.h"
#include "SphereInterface.h"
#include "math.h"
#include "GMM.h"

/*-------------------------------------------------------------------------
 *  ComputeThreshold -- Computes the threshold on energy for a waveform
 *    Args:	
 *    Returns:	float
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeThreshold(ASDF *asdf, float thresholdScale)
{
  int i;
  float ave = 0;
  for (i = 0; i < asdf->numFrames; i++)
    ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
  ave = ave/asdf->numFrames;
  return (ave*thresholdScale);
}	/*  End of ComputeThreshold		End of ComputeThreshold   */

/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *featureName,
					   int *numVectors, float thresholdScale) {

  F_VECTOR                   *fvect;
 VECTOR_OF_F_VECTORS        *vfv;
 int                        i;
 int                        frameNum;
 float                      threshold, energy;
/*printf("enter compute feat vect \n");
fflush(stdout);*/

  vfv = (VECTOR_OF_F_VECTORS *) calloc(asdf->numFrames, sizeof(VECTOR_OF_F_VECTORS));
  threshold = (float) ComputeThreshold(asdf, thresholdScale);
  frameNum = 0;
  for (i = 0; i < asdf->numFrames; i++) {
    energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
    if (energy > threshold) {
      fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
      if(fvect == NULL) {
	printf("problems fvect\n");
	fflush(stdout);
	exit(-1);
      }
      vfv[frameNum] = fvect;
      frameNum++;
    }
  }

  GsfClose(asdf);
*numVectors = frameNum;
/*printf("total no of frame processed = %d\n",*numVectors);
fflush(stdout); */
return(vfv);
}
/************************************************************************
  Function            : MixtureProbability - computes the
                        maximum prob for a given feature vector

  Inputs              : fvect - feature vector
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
			probScaleFactor - scalefactor for probability
  Outputs             : smallest distance
            
  **************************************************************************/

float MixtureProbability(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *speakerMean,
		   VECTOR_OF_F_VECTORS *speakerVar, F_VECTOR *speakerWts, 
		  int numClusters, float probScaleFactor){
int                          i, j;
float                        maxProb = 0, probValue;
int                          featLength; 
            

  //  printf("probs %f\n",maxProb);
  return(maxProb);
}

/************************************************************************
  Function            : ComputeLikelihood - computes the
                        likelihood for the entire utterance data.

  Inputs              : vfv - feature vectors for an utterance
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
  Outputs             : Distortion
            
  **************************************************************************/

float  *ComputeLikelihood(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int numClusters, float *Distortion, 
			  float probScaleFactor) {
  
  int                     i, j, k;
  float                   mixProbValue;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	 mixProbValue = MixtureProbability(vfv[j], speakerMeans[i], 
			       speakerVars[i], speakerWts[i],numClusters, 
			       probScaleFactor);
	 /*if (mixProbValue != 0.0)*/
	Distortion[i] = Distortion[i] + mixProbValue;
	/*      else
		Distortion[i] = Distortion[i] + LOG_ZERO;*/
    }
    printf("Distortion %d = %f\n",i,Distortion[i]);
    fflush(stdout); 
    Distortion[i] = Distortion[i]/numVectors;
  }
  return(Distortion);
}
void Usage() {
   printf (" Usage : TestUtteranceGMM ctrlFile modelFile numGMM featureName numSpeakers wavName threshold\n");
}
  main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[256],line[256];
  int                   numSpeakers,numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i,j,k;
  int                   featLength,numVectors;
  float                 *Distortion;
  float                 thresholdScale;
  if (argc != 8) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[6];
  string1 = argv[7];
  sscanf(string1, "%f", &thresholdScale);
  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  /*printf("testfile name = %s\n",wavname);
  fflush(stdout);*/
  GsfOpen(asdf,wavname);
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,featureName, &numVectors, thresholdScale);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  for (i = 0; i < numSpeakers; i++) {
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters);
    for (j = 0; j < numClusters; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
  }
  i = 0;
  while (fgets(line,200,modelFile)) {
    sscanf(line,"%s",speakerModel);
    speakerFile = fopen(speakerModel,"r");
    /*printf("speakerFile = %s\n",speakerModel);
    fflush(stdout); */
    for (j = 0; j < numClusters; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      /*      printf("%f\n",speakerModelWts[i]->array[j]);
      fflush(stdout);*/
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile," %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
	/*	        printf(" %f %f\n",speakerModelMeans[i][j]->array[k],
			speakerModelVars[i][j]->array[k]);  
			fflush(stdout);*/
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeLikelihood(vfv,numVectors,
					   numSpeakers,speakerModelMeans,
					   speakerModelVars,speakerModelWts,
					   numClusters,Distortion,
					   asdf->probScaleFactor);
  printf("%s\n",wavname);
  for (i = 0; i < numSpeakers; i++){
    printf("%d %f\n",i,Distortion[i]);
  }
  printf("%50s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  free(vfv);
  free(Distortion);
  free (speakerModelMeans);
  free (speakerModelVars);
  free (speakerModelWts);

  }












/*-------------------------------------------------------------------------
 * $Log: TestUtterance.c,v $
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

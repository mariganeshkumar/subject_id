import argparse
from tqdm import tqdm
import os
import shutil
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import scipy.io as sio
import sys
import pickle

parser = argparse.ArgumentParser()
parser.add_argument("--trainDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--testDataScriptFile", help="Location of script file for test files.",
                    type=str, )

parser.add_argument("--valDataScriptFile", help="Location of script file for script files.",
                    type=str, )
parser.add_argument("--testResultFolder", help="Location of the folder to write test result.",
                    type=str, )
parser.add_argument("--valResultFolder", help="Location of the folder to write test result.",
                    type=str, )
# parser.add_argument("--wMat", help="Location of whiting transform matrix.",
#                    type=str, )
parser.add_argument("--ldaMat", help="Location of LDA transform matrix.",
                    type=str, )
parser.add_argument("--scoreMat", help="File to store final score matrix.",
                    type=str, )
parser.add_argument("--saveDir", help="File to store final score matrix.",
                    type=str, )

args = parser.parse_args()
tmpDir = "tmp/svd"
saveFile = args.saveDir +"/sub_space_matrix.mat"

if os.path.exists(tmpDir):
    shutil.rmtree(tmpDir)
os.makedirs(tmpDir)


def ovr_train(data, labels):
    labelSet = np.sort(np.unique(labels))
    labelSetSize = len(labelSet)
    models = []
    for label in labelSet:
        model = SVC(kernel='precomputed', probability=True)
        binaryLabels = np.array(labels == label, dtype=np.float32)
        model.fit(data, binaryLabels)
        models.append(model)
    model = {'models': models, 'labelSet': labelSet}
    return model


def ovr_predict(data, model):
    labelSet = model['labelSet']
    labelSetSize = len(labelSet)
    models = model['models']
    scores = []
    for label in range(labelSetSize):
        score = models[label].predict_log_proba(data)
        score = score[:, 1]  # true class score
        scores.append(np.expand_dims(score,axis=1))
    scores = np.concatenate(scores, axis=1)
    print(scores.shape)
    return scores



trainSubSpaceVectors = []
testSubSpaceVectors = []
valSubSpaceVectors = []


with open(args.trainDataScriptFile) as f:
    trainDataScript = f.readlines()
trainDataScript = [x.strip() for x in trainDataScript]
trainFiles = [x.split(' ')[0] for x in trainDataScript]
trainLabel = [int(x.split(' ')[1]) for x in trainDataScript]

for i in tqdm(range(len(trainFiles))):
    filename = trainFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        trainSubSpaceVectors.append(iVec)

with open(args.valDataScriptFile) as f:
    valDataScript = f.readlines()
valDataScript = [x.strip() for x in valDataScript]
valFiles = [x.split(' ')[0] for x in valDataScript]
valLabel = [int(x.split(' ')[1]) for x in valDataScript]

for i in tqdm(range(len(valFiles))):
    filename = valFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        valSubSpaceVectors.append(iVec)

with open(args.testDataScriptFile) as f:
    testDataScript = f.readlines()
testDataScript = [x.strip() for x in testDataScript]
testFiles = [x.split(' ')[0] for x in testDataScript]
testLabel = [int(x.split(' ')[1]) for x in testDataScript]

for i in tqdm(range(len(testFiles))):
    filename = testFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        testSubSpaceVectors.append(iVec)

trainSubSpaceVectors = np.array(trainSubSpaceVectors)
valSubSpaceVectors = np.array(valSubSpaceVectors)
testSubSpaceVectors = np.array(testSubSpaceVectors)


clf = pickle.load(open(args.ldaMat))
trainSubSpaceVectors = clf.transform(trainSubSpaceVectors)
valSubSpaceVectors = clf.transform(valSubSpaceVectors)
testSubSpaceVectors = clf.transform(testSubSpaceVectors)

trainLabel = np.array(trainLabel)
valLabel = np.array(valLabel)
testLabel = np.array(testLabel)

sio.savemat(saveFile,{
    'trainSubSpaceVectors': trainSubSpaceVectors,
    'valSubSpaceVectors': valSubSpaceVectors,
    'testSubSpaceVectors': testSubSpaceVectors,
    'trainLabel': trainLabel,
    'valLabel': valLabel,
    'testLabel': testLabel,
})

print("Computing Cosine Kernal")
TrainDataKernel = cosine_similarity(trainSubSpaceVectors, trainSubSpaceVectors)
ValDataKernel = cosine_similarity(valSubSpaceVectors, trainSubSpaceVectors)
TestDataKernel = cosine_similarity(testSubSpaceVectors, trainSubSpaceVectors)

print("LIBSVM SVM Training and testing")

model = ovr_train(TrainDataKernel, trainLabel)
testScore =ovr_predict(TestDataKernel, model)
pred = np.argmax(testScore, axis=1)+1
print(accuracy_score(testLabel, pred) * 100)
pred = pred.tolist()
for i in range(len(pred)):
    filename = args.testResultFolder + str(i + 1) + '.out'
    with open(filename, 'w') as f:
        f.write("label " + str(pred[i]) + " \n")
        f.close()

valScore =ovr_predict(ValDataKernel, model)
pred = np.argmax(valScore, axis=1)+1
print(accuracy_score(valLabel, pred) * 100)
pred = pred.tolist()
print(valScore.shape)
for i in range(len(pred)):
    filename = args.valResultFolder + str(i + 1) + '.out'
    with open(filename, 'w') as f:
        f.write("label " + str(pred[i]) + " \n")
        f.close()

print("Collecting Target and Non-Target Scores")
target_test_scores = []
non_target_test_scores = []
target_val_scores = []
non_target_val_scores = []
indices = np.arange(len(np.unique(trainLabel)))
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(testScore[np.where(testLabel == i), :], axis=0)
    target_scores = scores[:, i - 1]
    non_target_scores = np.squeeze(scores[:, indices != i - 1]).flatten()
    target_test_scores.append(target_scores)
    non_target_test_scores.append(non_target_scores)

    scores = np.squeeze(valScore[np.where(valLabel == i), :], axis=0)
    target_scores = scores[:, i - 1]
    non_target_scores = np.squeeze(scores[:, indices != i - 1]).flatten()
    target_val_scores.append(target_scores)
    non_target_val_scores.append(non_target_scores)

target_test_scores = np.concatenate(target_test_scores, axis=0)
non_target_test_scores = np.concatenate(non_target_test_scores, axis=0)
target_val_scores = np.concatenate(target_val_scores, axis=0)
non_target_val_scores = np.concatenate(non_target_val_scores, axis=0)

print("Collecting Normalised Target and Non-Target Scores")

finalTestScore = []
for i in tqdm(np.unique(trainLabel)):
    score = testScore[:, i - 1]
    imposter_score = testScore[:, indices != i - 1]
    mean_is = np.mean(imposter_score, axis=1)
    std_is = np.std(imposter_score, axis=1)
    score = (score - mean_is) / std_is
    finalTestScore.append(score)
finalTestScore = np.transpose(np.array(finalTestScore))

finalValScore = []
for i in tqdm(np.unique(trainLabel)):
    score = valScore[:, i - 1]
    imposter_score = valScore[:, indices != i - 1]
    mean_is = np.mean(imposter_score, axis=1)
    std_is = np.std(imposter_score, axis=1)
    score = (score - mean_is) / std_is
    finalValScore.append(score)
finalValScore = np.transpose(np.array(finalValScore))

target_test_norm_scores = []
non_target_test_norm_scores = []
target_val_norm_scores = []
non_target_val_norm_scores = []
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(finalTestScore[np.where(testLabel == i), :], axis=0)
    target_norm_scores = scores[:, i - 1]
    non_target_norm_scores = np.squeeze(scores[:, indices != i - 1]).flatten()
    target_test_norm_scores.append(target_norm_scores)
    non_target_test_norm_scores.append(non_target_norm_scores)

    scores = np.squeeze(finalValScore[np.where(valLabel == i), :], axis=0)
    target_norm_scores = scores[:, i - 1]
    non_target_norm_scores = np.squeeze(scores[:, indices != i - 1]).flatten()
    target_val_norm_scores.append(target_norm_scores)
    non_target_val_norm_scores.append(non_target_norm_scores)

target_test_norm_scores = np.concatenate(target_test_norm_scores, axis=0)
non_target_test_norm_scores = np.concatenate(non_target_test_norm_scores, axis=0)
target_val_norm_scores = np.concatenate(target_val_norm_scores, axis=0)
non_target_val_norm_scores = np.concatenate(non_target_val_norm_scores, axis=0)

sio.savemat(args.scoreMat,{
    'test_scores': testScore,
    'val_scores': valScore,
    'target_test_scores': target_test_scores,
    'non_target_test_scores': non_target_test_scores,
    'target_val_scores': target_val_scores,
    'non_target_val_scores': non_target_val_scores,
    'target_test_norm_scores': target_test_norm_scores,
    'non_target_test_norm_scores': non_target_test_norm_scores,
    'target_val_norm_scores': target_val_norm_scores,
    'non_target_val_norm_scores': non_target_val_norm_scores,
})

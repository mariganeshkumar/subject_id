#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id:GroupDelaySpectrum.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	GroupDelaySpectrum.c - Computes the LP Spectrum
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 22-Mar-2011 20:50:30 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/QuickSort.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average LP Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the LP Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : GroupDelaySpectrum ctrlFile waveFile SpecFile frameStart frameEnd\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

	float           *signal, *f, *residual;
	int  	        i,k,fftOrder,fftSize,frameSize, numFrames,
	                frameNum,iloc,lpOrder, frameShift, 
	                frameStart, frameEnd, nfBy2;
        int             samplingRate;
        long            nSamp;
	char            fNameSpec[100],*ftemp=NULL;
        float           *ModGdSpecAvg, *ModGdSpec, *MinGdSpec, *MinGdSpecAvg,  
	                gain, *coef;
	F_VECTOR        *waveform;
        FILE            *specFile, *controlFile;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       nfBy2 = fftSize/2;
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       sscanf(argv[4], "%d", &frameStart);
       sscanf(argv[5], "%d", &frameEnd);
       fNameSpec[0] = '\0';
       ftemp = argv[3];
       Cstore(fftSize);
       strcpy(fNameSpec,ftemp);
       specFile = fopen(fNameSpec, "w");
       MinGdSpecAvg  = (float *) AllocFloatArray(MinGdSpecAvg, fftSize+1);
       ModGdSpecAvg  = (float *) AllocFloatArray(ModGdSpecAvg, fftSize+1);
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
         fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameModGdelay");
         for (i = 0; i < nfby2; i++)
           ModGdSpecAvg[i] = ModGdSpecAvg[i] + fvect->array[i];
	 free(fvect->array);
	 free(fvect);
         fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameMinGdelay");
         for (i = 0; i < nfby2; i++)
           MinGdSpecAvg[i] = MinGdSpecAvg[i] + fvect->array[i];
	 fprintf("%d %d %f %f\n", i, ModGdSpecAvg[i], MinGdSpecAvg[i]);
	 free(fvect->array);
	 free(fvect);
       }
       fclose(specFile);
}
/**************************************************************************
 * $Log: LPSpectrum.c,v $
 * Revision 1.1  2000/07/24 02:07:57  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




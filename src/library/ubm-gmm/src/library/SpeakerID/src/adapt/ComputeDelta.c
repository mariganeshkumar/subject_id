#include "CommonFunctions.h"

void Usage () {
    printf ("Usage: ./ComputeDelta [options]\n");
    printf ("i : input file\n");
    printf ("o : output file\n");          
    printf ("a : append delta vectors [default = don't]\n");
    printf ("d : set delta difference [default = 3]\n");
    printf ("h : print this help\n");
}

int main (int argc, char *argv[]) {
    int i,j,k;
    int append = 0, 
        dumpfileisbinary = 0;
    unsigned int ddiff = 3, nvec = 0;
    char oc;
    char *ipname = NULL,
         *opname = NULL;
    FILE *ipfile = NULL,
         *opfile = NULL;

    VFV *ipvfv = NULL,
        *opvfv = NULL,
        *nvfv = NULL;

	while ((oc = getopt (argc, argv, "i:o:d:ahb")) != -1) {
        switch (oc) {
            case 'a':
                append = 1;
                break;
            case 'i':
                ipname = optarg;
                break;
            case 'o':
                opname = optarg;
                break;
            case 'd':
                sscanf (optarg, "%u", &ddiff);
                break;
            case 'b':
                dumpfileisbinary = 1;
                break;
            case 'h':
            default:
                Usage ();
                exit (0);
                break; // required?
        }
    }

    if (ipname == NULL || opname == NULL) {
        Usage ();
        exit (1);
    }

    if (dumpfileisbinary)
        ipvfv = ReadVfvFromBinFile (ipname, &nvec);
    else
        ipvfv = ReadVfvFromFile (ipname, &nvec);
    nvfv = ComputeDeltaVectors (ipvfv, nvec, ddiff);
    if (!append) 
        opvfv = nvfv;    
    else 
        opvfv = ConcatVfv (ipvfv, nvfv, nvec);
        
    WriteVfvToFile (opvfv, nvec, opname);
    FreeVfv (ipvfv,nvec);
    FreeVfv (opvfv,nvec);
    if (append) FreeVfv (nvfv,nvec);
}


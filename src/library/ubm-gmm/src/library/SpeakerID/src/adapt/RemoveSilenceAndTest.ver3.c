#include "CommonFunctions.h"

void Usage() {
   printf (" \nUsage : RemoveSilenceAndTest.ver3 ctrlFile modelFile featureName numSpeakers wavName threshold test/verify(0/1)");
	printf ("ergModelFile numGaussians\n\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL, *ergModelFile;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL, *ergModelFileName;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ergMeans, *ergVars;
  float					   *ergWts, *tempProbabilities;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv,*newVfv,*energyVfv;
  int                   i, j, k,numErgGauss;
  int						   speechMixtureNumber, noiseMixtureNumber, silenceMixtureNumber;
  int                   featLength, lastSelectedVectorIdx;
  unsigned long         numVectors, numSpeechVectors;
  int                   verify;
  float                 *Distortion;
  float                 thresholdScale, probScaleFactor;
  if (argc != 10) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  featureName = argv[3];
  string2 = argv[4];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[5];
  string1 = argv[6];
  sscanf(string1, "%f", &thresholdScale);
  string1 = argv[7];
  sscanf(string1, "%d", &verify);
  ergModelFileName = argv[8];
  sscanf(argv[9],"%d",&numErgGauss);
  
  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  probScaleFactor=(float) GetFAttribute(asdf,"probScaleFactor");
  printf ("probScaleFactor is %f\n",probScaleFactor);
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  
  
  vfv = (VECTOR_OF_F_VECTORS *) ExtractFeatureVectors(asdf, wavname,featureName, &numVectors, thresholdScale);
  featLength = vfv[0]->numElements;

  energyVfv = (VECTOR_OF_F_VECTORS *) ExtractFeatureVectors (asdf,wavname,"frameEnergy",&numVectors, thresholdScale);
  for (j=0;j<numVectors;j++) energyVfv[j]->array[0] = log(energyVfv[j]->array[0]);
  MakeZeroMeanUnitVar(energyVfv, numVectors);

  ergMeans = (VECTOR_OF_F_VECTORS *) calloc (numErgGauss,sizeof(VECTOR_OF_F_VECTORS));
  ergVars = (VECTOR_OF_F_VECTORS *) calloc (numErgGauss,sizeof(VECTOR_OF_F_VECTORS));
  for (j=0;j<numErgGauss;j++)
    {
	   ergMeans[j] = (F_VECTOR*) AllocFVector (energyVfv[0]->numElements);
		ergVars[j]  = (F_VECTOR*) AllocFVector (energyVfv[0]->numElements); 
	 }
  ergWts = (float *) calloc (numErgGauss,sizeof(float));
  ergModelFile = fopen (ergModelFileName,"r");
  for (i=0;i<numErgGauss && !feof(ergModelFile);i++)
    {
	   fscanf(ergModelFile,"%e\n",&ergWts[i]);
	   printf("%e\n",ergWts[i]);
		for (j=0;j<energyVfv[0]->numElements;j++) {
		  fscanf(ergModelFile," %e %e", &ergMeans[i]->array[j], &ergVars[i]->array[j]);
		  printf(" %e %e", ergMeans[i]->array[j], ergVars[i]->array[j]);
		  }
		  fscanf(ergModelFile,"\n");
		  printf("\n");
	 }
  silenceMixtureNumber=0;
  noiseMixtureNumber=1;
  speechMixtureNumber=2;
  tempProbabilities = (float *) calloc (numErgGauss, sizeof(float));	   
  lastSelectedVectorIdx = -1;
  newVfv = (VECTOR_OF_F_VECTORS *) calloc (numVectors, sizeof(VECTOR_OF_F_VECTORS));
  numSpeechVectors=0;
  for (i=0; i<numVectors; i++)
    {
      tempProbabilities[silenceMixtureNumber] = ComputeProbability(ergMeans[silenceMixtureNumber],
																						 ergVars[silenceMixtureNumber],
																						 ergWts[silenceMixtureNumber],
																						 energyVfv[i],asdf->probScaleFactor);
      tempProbabilities[noiseMixtureNumber] = ComputeProbability(ergMeans[noiseMixtureNumber],
																						 ergVars[noiseMixtureNumber],
																						 ergWts[noiseMixtureNumber],
																						 energyVfv[i],asdf->probScaleFactor);
      tempProbabilities[speechMixtureNumber] = ComputeProbability(ergMeans[speechMixtureNumber],
																						 ergVars[speechMixtureNumber],
																						 ergWts[speechMixtureNumber],
																						 energyVfv[i],asdf->probScaleFactor);
		if (tempProbabilities[silenceMixtureNumber] < tempProbabilities[speechMixtureNumber] &&
			 tempProbabilities[noiseMixtureNumber]   < tempProbabilities[speechMixtureNumber])
			 {
			  	 newVfv[numSpeechVectors++] = vfv[i];
			}
			
		else if (tempProbabilities[silenceMixtureNumber] < tempProbabilities[speechMixtureNumber] &&
		         energyVfv[i]->array[0] > ergMeans[noiseMixtureNumber]->array[0])
			  	 newVfv[numSpeechVectors++] = vfv[i];
		  
	 }

  numVectors=numSpeechVectors;
  //MakeZeroMeanUnitVar(newVfv,numVectors);
  printf ("numSpeechVectors = %d\n", numSpeechVectors);
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeLikelihood(newVfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   asdf->probScaleFactor);
  printf("%s\n",wavname);
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    printf("DEBUG: identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

/* simple matrix multiplication*/

#include "stdio.h"
#include "cuda.h"

//row major format
#define IDX2F(i,j,ld) ((((i)-1)*(ld))+((j)-1))

float* 
readMatrixFromFile (char *fname, int *m, int *n) {
  FILE *fptr;
  float *farr;
  int ncols, nrows,i,j;
  

  fptr = fopen (fname,"r");
  fscanf (fptr, "%d %d\n", &ncols, &nrows);
  farr = (float *) calloc (nrows*ncols, sizeof(float));
  for (i = 1; i <= nrows; i++) {
    for (j = 1; j <= ncols; j++) 
      fscanf (fptr, " %e", &farr[IDX2F(i,j,ncols)]);
    fscanf (fptr, "\n");
  }
  fclose (fptr);
  *m = nrows;
  *n = ncols;
  return farr;
}

__global__ void
gpuMatMult (float *mat1,int m, int n, float *mat2, int k, float *mat3) {
  int ridx, cidx;
  int i,j;
  float tempf = 0.;

  ridx = blockIdx.x + 1;
  cidx = blockIdx.y + 1;  

  for (i = 1; i <= n; i++)
    tempf = tempf + (mat1[IDX2F(ridx,i,n)] * mat2[IDX2F(i,cidx,k)]);


  mat3[IDX2F(ridx,cidx,m)] = tempf;
  
}


float *matMult (float *mat1, int M, int N, float *mat2, int K) {
    float *mat3;
    float *gpu_mat1, *gpu_mat2, *gpu_mat3;
    cudaStream_t cstream;


    cudaMalloc ((void **) &gpu_mat1, M*N*sizeof(float));
    cudaMalloc ((void **) &gpu_mat2, N*K*sizeof(float));
    cudaMalloc ((void **) &gpu_mat3, M*K*sizeof(float));

    cudaMemcpy (gpu_mat1, mat1, sizeof(float)*M*N,cudaMemcpyHostToDevice);
    cudaMemcpy (gpu_mat2, mat2, sizeof(float)*K*N,cudaMemcpyHostToDevice);
    dim3 colnos(M,K);

    cudaStreamCreate (&cstream);
    gpuMatMult <<<colnos,1,0,cstream>>> (gpu_mat1, M, N, gpu_mat2, K, gpu_mat3);
    cudaStreamSynchronize (cstream);
    mat3 = (float *) calloc (M*K, sizeof(float));
    cudaMemcpy (mat3, gpu_mat3, sizeof(float)*M*K, cudaMemcpyDeviceToHost);

    cudaFree (gpu_mat1);
    cudaFree (gpu_mat2);
    cudaFree (gpu_mat3);
    return mat3;
}

int
main (int argc, char *argv[]) {
    char *ipn1, *ipn2, *opn;
    float *mat1, *mat2, *mat3;
    int m1,m2,n1,n2;

    if (argc != 4) {
      printf ("Usage: ./Matmult mat1 mat2 mat.out\n");
      exit (1);
    }

    ipn1 = argv[1];
    ipn2 = argv[2];
    opn = argv[3];

    mat1 = readMatrixFromFile (ipn1, &m1, &n1);
    printf ("%e %d\n", mat1[IDX2F(1242,1248,n1)],IDX2F(1,1,n1));
    mat2 = readMatrixFromFile (ipn2, &m2, &n2);
    if (m2 != n1) {
      printf ("Wrong input arguments %d %d %d %d\n", m1,n1, m2, n2);
      exit (2);
    }
    mat3 = matMult (mat1, m1,n1,mat2,n2);
    printf ("%e\n", mat3[IDX2F(1242,1248,n2)]);
    return 0;
}

function [Fest,Eest] = EM_train_SPLDA_model(X,N,nF,niter)

flag_monitor_LL = 1;
flag_MD = 1;
fid = 1;
SMALL_INC = 0.1; % Small increment in log Marginal Likelihood to stop EM
var_floor = 1e-5; % Floor for noise variance
flag_diag = 1;
LML_correction_term = 0;
%Smallest inc
%SMALL_INC = 1000;
%Get dimensions
[dim,ntot] = size(X);
Nsum = sum(N);
nspk = length(N);
nlist = unique(N); %Create the list of unique zeroth order stats
is_first= 1; %variable used to prevent error in first selected iter to compute LL

%Compute dev data energy
eX = (norm(X,'fro')^2)/ntot;
fprintf(1,'Average energy X: %f\n',eX);

% %Initialize results
% Fest = zeros(dim,nF);
% Eest = zeros(dim,dim);

%Corrected likelihood to make fare comparison with the unit norm case. When
%we normalize to unit norm there is no correction. When we don't normalize
%I correct the LML as if we had done the scaling
total_LL = zeros(niter+1,1) + LML_correction_term;

Fest = randn(dim,nF)/sqrt(dim*nF*(2/eX));

Eest = diag(abs(randn(dim,1)))/sqrt(dim*(2/eX));

%Preprocessing to speed up
%Xtilda (First order suf. stats.)
Xtilda = zeros(dim,nspk);

ie = 0;
for i = 1:nspk
    ib = ie + 1;
    ie = ib + N(i) - 1;
    Xtilda(:,i) = sum(X(:,ib:ie),2);
end

%Posterior means
H = zeros(nF,nspk);
eye_nF = eye(nF);

%Start EM iterations
for iter = 1:niter
    
    %Matrix for the M-step
    R2 = zeros(nF);
    R_MD = zeros(nF);
    
    %Update estimates from previous iteration
    F = Fest;    
    E = Eest;
    
    %Precompute iPn=chol(I + n*F'*inv(E)*F) for each unique N    
    [iPn,iE,log_det_Pn,log_det_E,iEF] = precompute_matrices(F,E,nlist);
       
    %Precompute F'*iE*Xtilda 
    FtiEXtilda = iEF'*Xtilda;
                
    %********************
    %E-step:
    %********************
    ie = 0;
    for i = 1:nspk
        ib = ie + 1;
        ie = ib + N(i) - 1;
        
        index_n = find(nlist == N(i));        
        iP = iPn(:,:,index_n);
        
        %H(:,i) = iP\(iP'\FtiEXtilda(:,i)); %Posterior means
        H(:,i) = iP*FtiEXtilda(:,i); %Posterior means
        
        %tmp_cov = iP\(iP'\eye_nF);
        %R2 = R2 + N(i)* tmp_cov; %Weigthed sum of posterior covariances
        R2 = R2 + N(i)* iP;
        %R_MD = R_MD + tmp_cov;%MD 
        R_MD = R_MD + iP;%MD 
                        
        if ~mod(iter,flag_monitor_LL)
            log_det_P = log_det_Pn(index_n);            
            total_LL(iter) = total_LL(iter) + chol_PLDA_Log_Likelihood_precomputed_matrices(X(:,ib:ie),Xtilda(:,i),iP,log_det_P,log_det_E,iEF,iE);                                               
        end
    end
    if ~mod(iter,flag_monitor_LL)
        total_LL(iter) = total_LL(iter) / nspk;
    end
    %Monitor the progress in the Log Marginal Likelihood and subspace
    %distance
    if ~mod(iter,flag_monitor_LL)       
        if is_first            
            tmp_inc = 0;
            is_first = 0;
        else
            tmp_inc = total_LL(iter) - total_LL(iter-flag_monitor_LL);
        end
        
        if tmp_inc < 0
            fprintf(1,'Warning decrease in total Log Mariginal Likelihood\n');
        end
                
        eF = norm(F,'fro')^2;
        eE = norm(E,'fro')^2;
                                
        fprintf(1,'Iteration %i:\t LML:%8.2f\t INC:%8.2f\t eF:%6.4f\t eE:%6.4f\n',iter-1,total_LL(iter),tmp_inc,eF,eE);    
        %hold off; plot(vE); drawnow;

                
        if tmp_inc < SMALL_INC && iter > 10
            fprintf(1,'Stopping the EM due to small increment in LML (< %i)\n',SMALL_INC);
            Fest = F;            
            Eest = E;
            break;
        end       
    end
                       
    %********************
    %M-step
    %********************            
    Hn= bsxfun(@times,H,N'); 
    R2 = Hn*H' + R2;
    
    %Update F  
    Bt = Xtilda*H';
    Fest = Bt / R2;           
    
    %Update E
    Eest = (X*X' - Fest*Bt')/ Nsum;
    
    if flag_diag        
        Eest = diag(max(diag(Eest),var_floor));        
    end
                   
    %********************
    %MD iteration
    %********************
    if flag_MD          
         R_MD = (R_MD + H*H')/nspk;
         Fest = Fest*chol(R_MD);  
    end
    
                              
end %iter

    if fid ~= 1
        %fprintf(fid,'Iteration %i:\t MLL:%g\n',iter-1,total_LL(iter)); 
        fprintf(fid,'Iteration %i:\t LML:%8.2f\t INC:%8.2f\t eF:%6.4f\t eE:%6.4f\n',iter-1,total_LL(iter),tmp_inc,eF,eE);    
    end
           
end

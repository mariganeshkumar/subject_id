#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "string.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program computes the FFT Magnitude Spectrum
*	Input: dataFile, FFTOrder, FFTSize, 
*	Output :outPutFile

*******************************************************************************/       
void Usage() {
           printf("Usage : ComputeCorrelation fileList fileOut featVectorLength\n");
}
/*****************************************************************************/

main (int argc, char *argv[])
{ 
  FILE                 *fList, *fOut, *featureFile;
  VECTOR_OF_F_VECTORS  **featureData;
  VECTOR_OF_F_VECTORS  *outputData;
  char                 featureFileName[500]; 
  char                 line[500];
  int                  numFiles, numFrames, featLength;
  int                  i, j, k, l;
  float                sum1, sum2, frobeniusNorm;
 /******************************************************************************/
  if (argc != 4) {
    Usage();
    exit(-1);
  }
  printf("%s %s %s \n", argv[1], argv[2], argv[3]);
  fList = fopen(argv[1], "r");
  fOut = fopen(argv[2], "w");
  sscanf(argv[3], "%d", &featLength);
  numFrames = 0;
  numFiles = 0;
  while (fgets (line, 200, fList) != NULL)
    numFiles++;
  rewind (fList);
  if (fgets (line, 200, fList) != NULL) {
    sscanf(line, "%s", featureFileName);
    featureFile = fopen (featureFileName, "r");
    numFrames = 0;
    while (fgets (line, 500, featureFile) != NULL)
      numFrames++;
  }
  rewind(fList);
  fclose(featureFile);
  featureData = (VECTOR_OF_F_VECTORS **) calloc(numFiles,sizeof(VECTOR_OF_F_VECTORS **));
  i =0;
  while (fgets(line, 200, fList) != NULL) {
    sscanf(line, "%s", featureFileName);
    featureData[i] =  (VECTOR_OF_F_VECTORS *) calloc(numFrames, 
						     sizeof(VECTOR_OF_F_VECTORS));
    featureFile = fopen (featureFileName, "r");
    for (j = 0; j < numFrames; j++) {
      featureData[i][j] = (F_VECTOR *) AllocFVector(featLength);
      for (k = 0; k < featLength; k++)
	fscanf(featureFile, "%f", &featureData[i][j]->array[k]);
    }
    i++;
    fclose(featureFile);
  }
  fclose(fList);
  outputData = (VECTOR_OF_F_VECTORS *) calloc(numFrames, 
					      sizeof (VECTOR_OF_F_VECTORS));
  for (j = 0; j < numFrames; j++) 
    outputData[j] = (F_VECTOR *)AllocFVector(featLength);
  for (k = 0; k < numFrames; k++)
    for (l = 0; l < featLength; l++)
      outputData[k]->array[l] = 0.0;
  for (i = 0; i < numFiles; i++)
    for (j = i+1; j < numFiles; j++)
      for (k = 0; k < numFrames; k++) {
	sum1 = 0;
	sum2 = 0;
	for(l = 0; l < featLength; l++) {
	  sum1 = sum1 + featureData[i][k]->array[l]*featureData[i][k]->array[l];
	  sum2 = sum2 + featureData[j][k]->array[l]*featureData[j][k]->array[l];
	  outputData[k]->array[l] = outputData[k]->array[l] + featureData[i][k]->array[l]*featureData[j][k]->array[l];
	}
        outputData[k]->array[l] = outputData[k]->array[l]/(sqrt(sum1)*sqrt(sum2));
      }
  frobeniusNorm = 0;
  for (k = 0; k < numFrames; k++) {
    for (l = 0; l < featLength; l++)
      fprintf(fOut,"%e ", outputData[k]->array[l]/(numFiles*(numFiles-1)/2));
    fprintf(fOut,"\n");
  }
  for (k = 0; k < numFrames; k++)
    for (l = 0; l < featLength; l++)
      frobeniusNorm = frobeniusNorm + outputData[k]->array[l];
  printf ("Frobenius Norm = %e \n", frobeniusNorm/featLength/numFrames/(numFiles*(numFiles-1)/2));
  fclose(fOut);
}					       

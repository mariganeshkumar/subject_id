%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('/export/hynek/sganapathy/speech/speakerid/jfa/scripts/train_ivec_tel');

nx  = 500;
EIGEN_CHANNELS_MATRIX='/export/hynek/sganapathy/speech/speakerid/jfa/models/fdlp_models/ivec_mic_tel/u_it010.mat';

% load the data
TRAINING_LIST='/export/hynek/sganapathy/speech/speakerid/jfa/scripts/train_ivec_mic_tel/fa_train_eigenchannels_mic_tel.scp';
%TRAINING_LIST='/home/hltcoe/sgarimella/software/modified_tools/GMM_ivect_PLDA/in_trn_tst.list';
%TRAINING_LIST='/home/hltcoe/sgarimella/software/modified_tools/GMM_ivect_PLDA/in_norm.list';

OUT_LIST='/export/hynek/sganapathy/speech/speakerid/jfa/plda/tools/out_trainList_factors_mic_tel.scp'
%OUT_LIST='/home/hltcoe/sgarimella/software/modified_tools/GMM_ivect_PLDA/out_trn_tst.list';
%OUT_LIST='/home/hltcoe/sgarimella/software/modified_tools/GMM_ivect_PLDA/out_norm.list';


disp('Reading list')
[spk_logical spk_physical] = parse_scp(TRAINING_LIST);
[out_logical out_physical] = parse_scp(OUT_LIST);



disp('Loading stats')
[N F] = load_stats_from_files(spk_physical);


% extract the spk_ids
disp('Creating speaker ids')
auxm  = regexp(spk_logical, '^[^_]+_[^_]+', 'match');
auxm  = [auxm{:}]';
[auxa auxb spk_ids] = unique(auxm);
clear auxa auxb auxm

% we need the ubm params
% m ... supervector of means
% E ... supervector of variances
m = load(['/export/hynek/sganapathy/speech/speakerid/jfa/models/fdlp_models/ubm_means'], '-ascii');
E = load(['/export/hynek/sganapathy/speech/speakerid/jfa/models/fdlp_models/ubm_variances'], '-ascii');

% or do not load initial U matrix
disp(['loading U matrix']);
load(EIGEN_CHANNELS_MATRIX);

%%%%%%%%%%%%%%%%%%%%%%%
n_speakers=max(spk_ids);
n_sessions=size(spk_ids,1);

% we don't use the second order stats - set 'em to empty matrix
S = [];

% estimate speaker factors. these will be fixed througout the u estimation
v=0;
y=0;
[x]=estimate_x_and_u_onestepMD(F, N, S, m, E, 0, v, u, zeros(n_speakers,1), y, zeros(n_sessions,1), spk_ids);

for jj=1:size(x,1)
   xout=x(jj,:);
   save([out_physical{jj} '.x'], '-ascii', 'xout'); 
end
 


#include "eigen.h"

#define   base    2.0

/* Machine number base.  This value is for Turbo Pascal. */

/*local variables for EigenR*/

struct LOC_EigenR 
{
  int A[MaxN][MaxM], n, *flag; 
  Cvector *EigenValue;
  Cmatrix *EigenVector;
  double eps;
} ;

/***************************************************************************/
/*        Module 0     Utility Procedures                                  */
/***************************************************************************/

double MachineEpsilon()
{
  double a=1.0, b;
  do
    {
      a /= base;
      b = a + 1.0;
    } while (b != 1.0);
  return (a * base);
}  /* MachineEpsilon */

double Sign(x, y)
double x, y;
{
  if (y >= 0.0)
    return fabs(x);
  else
    return (-fabs(x));
}  /* Sign */

long Min(i, j)
long i, j;
{
  if (i <= j)
    return i;
  else
    return j;
}  /* Min */

double Max(x, y)
double x, y;
{
  if (x >= y)
    return x;
  else
    return y;
}  /* Max */

ComplexDiv(x1, y1, x2, y2, x3, y3)
double x1, y1, x2, y2, *x3, *y3;
{
  /* This procedure divides  x1 + i(y1)  by  x2 + i(y2)  to get  x3 + i(y3) */
  double temp;

  if (x2 != 0.0 && y2 != 0.0) 
    {
      temp = x2 * x2 + y2 * y2;
      *x3 = (x1 * x2 + y1 * y2) / temp;
      *y3 = (x2 * y1 - x1 * y2) / temp;
      return;
    }
  if (x2 == 0.0) 
    {
      *x3 = y1 / y2;
      *y3 = -(x1 / y2);
    } 
  else 
    {
      *x3 = x1 / x2;
      *y3 = y1 / x2;
    }
}  /* ComplexDiv */

/***************************************************************************/
/*        Module 1     Balance a matrix                                    */
/***************************************************************************/

Balance(a, d, n, indexK, indexL, LINK)
int n,a[n][n], *d, *indexK, *indexL;
struct LOC_EigenR *LINK;
{
  /* Reduce norm by a diagonal similarity transformation stored in  d[*] */
  boolean exit, continue_;
  long i, j, j1, k1;
  double b2, c, f, g, r, s;

  b2 = base * base;
  j1 = 1;
  k1 = n;
  continue_ = true;
  while (k1 >= 1 && continue_)
  {  /* Search for rows isolating an eigenvalue and push them down */
    j = k1;
    exit = false;
    while (j >= 1 && !exit) {
      r = 0.0;
      for (i = 1; i <= k1; i++) {
	if (i != j) {
	  r += abs(a[j][i]);
	}
      }
      if (r == 0.0) {
	d[k1] = j;
	if (j != k1) {
	  for (i = 1; i <= k1; i++) {
	    f = a[i][j];
	    a[i][j] = a[i][k1];
	    a[i][k1] = f;
	  }
	  for (i = j1; i <= n; i++) {
	    f = a[j][i];
	    a[j][i] = a[k1][i];
	    a[k1][i] = f;
	  }
	}
	k1--;
	exit = true;
      }
      j--;
    }
    if (!exit)
      continue_ = false;
  }
  continue_ = true;
  while (k1 >= j1 && continue_)
  {  /* search for columns isolating an eigenvalue and push them left */
    j = j1;
    exit = false;
    while (j <= k1 && !exit) {
      c = 0.0;
      for (i = j1; i <= k1; i++) {
	if (i != j) {
	  c += abs(a[i][j]);
	}
      }
      if (c == 0.0) {
	d[j1] = j;

	if (j != j1) {
	  for (i = 1; i <= k1; i++) {
	    f = a[i][j];
	    a[i][j] = a[i][j1];
	    a[i][j1] = f;
	  }
	  for (i = j1; i <= n; i++) {
	    f = a[j][i];
	    a[j][i] = a[j1][i];
	    a[j1][i] = f;
	  }
	}
	j1++;
	exit = true;
      }  /* c = 0.0 */
      j++;
    }
    if (!exit)
      continue_ = false;
  }
  *indexK = j1;
  *indexL = k1;
  /* now balance the submatrix in rows  j1  through  k1 */
  for (i = j1; i <= k1; i++) {
    d[i] = 1.0;
  }
  do {
    exit = true;
    for (i = j1; i <= k1; i++) {
      c = 0.0;
      r = 0.0;
      for (j = j1; j <= k1; j++) {
	if (j != i) {
	  c += abs(a[j][i]);
	  r += abs(a[i][j]);
	}
      }
      g = r / base;
      f = 1.0;
      s = c + r;
      while (c < g) {
	f = base * f;
	c *= b2;
      }
      g = r * base;
      while (c >= g) {
	f /= base;
	c /= b2;
      }
      if ((c + r) / f < 0.95 * s) {   /* balance */
	g = 1.0 / f;
	d[i] *= f;
	exit = false;
	for (j = j1; j <= n; j++) {
	  a[i][j] = g * a[i][j];
	}
	for (j = 1; j <= k1; j++) {
	  a[j][i] = f * a[j][i];
	}
      }
    }
  } while (!exit);   /* Balance */
}

/***************************************************************************/
/*        Module 2     Back transformation of the eigenvectors             */
/***************************************************************************/

Local Void Bback(z, d, n, m, indexK, indexL, LINK)
long z[MaxN][MaxM], *d, n, m, indexK, indexL;
struct LOC_EigenR *LINK;
{
  long i, j, jj;
  double s;

  if (indexL > 0) {
    for (i = indexK; i <= indexL; i++)
    {  /* column scale  z  by appropriate  d  value */
      s = d[i];
      for (j = 1; j <= m; j++) {
	z[i][j] = s * z[i][j];
      }
    }
  }
  for (i = indexK - 1; i >= 1; i--)
  {  /* interchange rows if permutations occurred in procedure Balance */
    jj = (long)((double)d[i]);
    if (i != jj) {
      for (j = 1; j <= m; j++) {
	s = z[i][j];
	z[i][j] = z[jj][j];
	z[jj][j] = s;
      }
    }
  }
  for (i = indexL + 1; i <= n; i++) {
    jj = (long)((double)d[i]);
    if (i != jj) {
      for (j = 1; j <= m; j++) {
	s = z[i][j];
	z[i][j] = z[jj][j];
	z[jj][j] = s;
      }
    }
  }
}  /* Bback */

Local Void Hback(h, d, n, mm, indexK, indexL, LINK)
long h[MaxN][MaxM], *d, n, mm, indexK, indexL;
struct LOC_EigenR *LINK;
{
  long i, j, m, ma;
  double g, t, tinv;

  for (m = indexL - 2; m >= indexK; m--) {
    ma = m + 1;
    t = h[ma][m];
    if (t != 0.0) {
      t *= d[ma];
      for (i = m + 2; i <= indexL; i++) {
	d[i] = h[i][m];
      }
      if (ma <= indexL) {
	tinv = 1.0 / t;
	for (j = 1; j <= mm; j++) {
	  g = 0.0;
	  for (i = ma; i <= indexL; i++) {
	    g += d[i] * LINK->EigenVector->Re[i][j];
	  }
	  g *= tinv;
	  for (i = ma; i <= indexL; i++) {
	    LINK->EigenVector->Re[i][j] += g * d[i];
	  }
	}
      }
    }
  }
}  /* Hback */

/***************************************************************************/
/*        Module 3     Reduction to Hessenberg form                        */
/***************************************************************************/

Hessenberg(a, d, n, indexK, indexL, LINK)
long a[MaxN][MaxM], *d, n, indexK, indexL;
struct LOC_EigenR *LINK;
{
  long i, j, m;
  double f, g, h, scale;

  for (m = indexK + 1; m < indexL; m++) {
    h = 0.0;
    d[m] = 0.0;
    scale = 0.0;
    for (i = m; i <= indexL; i++) {   /* scale columns */
      scale += abs(a[i][m - 1]);
    }
    if (scale > 0.0) {
      for (i = indexL; i >= m; i--) {
	d[i] = a[i][m - 1] / scale;
	h += d[i] * d[i];
      }
      g = -Sign(sqrt(h), (double)d[m], LINK);
      h -= d[m] * g;
      d[m] -= g;
      for (j = m; j <= n; j++) {   /* form   I - U*( U-transpose )/h )*A  */
	f = 0.0;
	for (i = indexL; i >= m; i--) {
	  f += d[i] * a[i][j];
	}
	f /= h;
	for (i = m; i <= indexL; i++) {
	  a[i][j] -= f * d[i];
	}
      }
      for (i = 1; i <= indexL; i++)
      {  /* form  L*A*L,  L = I - U*( U-transpose )/h  */
	f = 0.0;
	for (j = indexL; j >= m; j--) {
	  f += d[j] * a[i][j];
	}
	f /= h;
	for (j = m; j <= indexL; j++) {
	  a[i][j] -= f * d[j];
	}
      }
      d[m] = scale * d[m];
      a[m][m - 1] = scale * g;
    }
  }
}  /* Hessenberg */

/* Local variables for QR_Method: */
struct LOC_QR_Method {
  struct LOC_EigenR *LINK;
  int h[MaxN][MaxM], n, indexK, indexL, en, jb, na;
  double norm, p, q, r, ra, s, sa, t, vr, vi, w, x, y, zz;
} ;

Void ComplexPairOfRoots(LINK)
struct LOC_QR_Method *LINK;
{
  LINK->LINK->EigenValue->Re[LINK->na] = LINK->x + LINK->p;
  LINK->LINK->EigenValue->Re[LINK->en] = LINK->LINK->EigenValue->Re[LINK->na];
  LINK->LINK->EigenValue->Im[LINK->na] = LINK->zz;
  LINK->LINK->EigenValue->Im[LINK->en] = -LINK->zz;
}  /* ComplexPairOfRoots */

RealPairOfRoots(LINK)
struct LOC_QR_Method *LINK;
{
  long i, FORLIM;

  LINK->zz = LINK->p + Sign(LINK->zz, LINK->p, LINK->LINK);
  LINK->LINK->EigenValue->Re[LINK->na] = LINK->x + LINK->zz;
  LINK->LINK->EigenValue->Re[LINK->en] = LINK->LINK->EigenValue->Re[LINK->na];
  if (LINK->zz != 0.0) {
    LINK->LINK->EigenValue->Re[LINK->en] = LINK->x - LINK->w / LINK->zz;
  }
  LINK->LINK->EigenValue->Im[LINK->na] = 0.0;
  LINK->LINK->EigenValue->Im[LINK->en] = 0.0;
  LINK->x = (LINK->h)[LINK->en][LINK->na];
  LINK->r = sqrt(LINK->x * LINK->x + LINK->zz * LINK->zz);
  LINK->p = LINK->x / LINK->r;
  LINK->q = LINK->zz / LINK->r;
  FORLIM = LINK->n;
  for (i = LINK->na; i <= FORLIM; i++) {   /* row modification */
    LINK->zz = (LINK->h)[LINK->na][i];
    (LINK->h)[LINK->na][i] = LINK->q * LINK->zz + LINK->p * (LINK->h)[LINK->en][i];
    (LINK->h)[LINK->en]
      [i] = LINK->q * (LINK->h)[LINK->en][i] - LINK->p * LINK->zz;
  }
  FORLIM = LINK->en;
  for (i = 1; i <= FORLIM; i++) {   /* column modification */
    LINK->zz = (LINK->h)[i][LINK->na];
    (LINK->h)[i]
      [LINK->na] = LINK->q * LINK->zz + LINK->p * (LINK->h)[i][LINK->en];
    (LINK->h)[i]
      [LINK->en] = LINK->q * (LINK->h)[i][LINK->en] - LINK->p * LINK->zz;
  }
  FORLIM = LINK->indexL;
  for (i = LINK->indexK; i <= FORLIM; i++) {
	/* accumulate transformations */
	  LINK->zz = LINK->LINK->EigenVector->Re[i][LINK->na];
    LINK->LINK->EigenVector->Re[i]
      [LINK->na] = LINK->q * LINK->zz + LINK->p * LINK->LINK->EigenVector->Re[i]
					[LINK->en];

    LINK->LINK->EigenVector->Re[i][LINK->en] =
      LINK->q * LINK->LINK->EigenVector->Re[i][LINK->en] - LINK->p * LINK->zz;

  }
}  /* RealPairOfRoots */

Void OneRootFound(LINK)
struct LOC_QR_Method *LINK;
{
  (LINK->h)[LINK->en][LINK->en] = LINK->x + LINK->t;
  LINK->LINK->EigenValue->Re[LINK->en] = (LINK->h)[LINK->en][LINK->en];
  LINK->LINK->EigenValue->Im[LINK->en] = 0.0;
  LINK->en = LINK->na;
}  /* OneRootFound */

Void TwoRootsFound(LINK)
struct LOC_QR_Method *LINK;
{
  LINK->p = 0.5 * (LINK->y - LINK->x);
  LINK->q = LINK->p * LINK->p + LINK->w;
  LINK->zz = sqrt(fabs(LINK->q));
  (LINK->h)[LINK->en][LINK->en] = LINK->x + LINK->t;
  LINK->x = (LINK->h)[LINK->en][LINK->en];
  (LINK->h)[LINK->na][LINK->na] = LINK->y + LINK->t;
  if (LINK->q >= 0.0)
    RealPairOfRoots(LINK);
  else
    ComplexPairOfRoots(LINK);
  LINK->en -= 2;
}  /* TwoRootsFound */

long Search(LINK)
struct LOC_QR_Method *LINK;
{
  /* Search for two consecutive small sub-diagonal elements */
  long i, m;
  double temp1, temp2;
  boolean exit;
  long FORLIM;

  m = LINK->en - 2;
  exit = false;
  while (m >= LINK->jb && !exit) {
    LINK->zz = (LINK->h)[m][m];
    LINK->r = LINK->x - LINK->zz;
    LINK->s = LINK->y - LINK->zz;
    LINK->p = (LINK->h)[m]
	      [m + 1] + (LINK->r * LINK->s - LINK->w) / (LINK->h)[m + 1][m];
    LINK->q = (LINK->h)[m + 1][m + 1] - LINK->zz - LINK->r - LINK->s;
    LINK->r = (LINK->h)[m + 2][m + 1];
    LINK->s = fabs(LINK->p) + fabs(LINK->q) + fabs(LINK->r);
    LINK->p /= LINK->s;
    LINK->q /= LINK->s;
    LINK->r /= LINK->s;
    if (m == LINK->jb) {
      exit = true;
      break;
    }
    temp1 = abs((LINK->h)[m][m - 1]) * (fabs(LINK->q) + fabs(LINK->r));
    temp2 = fabs(LINK->p) * (abs((LINK->h)[m - 1][m - 1]) + fabs(LINK->zz) +
			     abs((LINK->h)[m + 1][m + 1]));
    if (temp1 <= LINK->LINK->eps * temp2)
      exit = true;
    else
      m--;
  }
  (LINK->h)[m + 2][m] = 0.0;
  FORLIM = LINK->en;
  for (i = m + 3; i <= FORLIM; i++) {
    (LINK->h)[i][i - 2] = 0.0;
    (LINK->h)[i][i - 3] = 0.0;
  }
  return m;
}  /* Search */

Void DoubleQR(m, LINK)
long m;
struct LOC_QR_Method *LINK;
{
  /* double QR step involving rows  indexL  to  en  and columns  m  to  en */
  long i, j;
  boolean continue_;
  long FORLIM, FORLIM1;

  FORLIM = LINK->na;
  for (i = m; i <= FORLIM; i++) {
    continue_ = true;
    if (i > m) {
      LINK->p = (LINK->h)[i][i - 1];
      LINK->q = (LINK->h)[i + 1][i - 1];
      if (i < LINK->na) {
	LINK->r = (LINK->h)[i + 2][i - 1];
      } else
	LINK->r = 0.0;
      LINK->x = fabs(LINK->p) + fabs(LINK->q) + fabs(LINK->r);
      if (LINK->x == 0.0)
	continue_ = false;
      else {
	LINK->p /= LINK->x;
	LINK->q /= LINK->x;
	LINK->r /= LINK->x;
      }
    }
    if (continue_) {
      LINK->s = Sign(
	  sqrt(LINK->p * LINK->p + LINK->q * LINK->q + LINK->r * LINK->r),
	  LINK->p, LINK->LINK);
      if (i > m) {
	(LINK->h)[i][i - 1] = -LINK->s * LINK->x;
      } else {
	if (LINK->jb != m) {
	  (LINK->h)[i][i - 1] = -(LINK->h)[i][i - 1];
	}
      }
      LINK->p += LINK->s;
      LINK->x = LINK->p / LINK->s;
      LINK->y = LINK->q / LINK->s;
      LINK->zz = LINK->r / LINK->s;
      LINK->q /= LINK->p;
      LINK->r /= LINK->p;
      FORLIM1 = LINK->n;
      for (j = i; j <= FORLIM1; j++) {   /* row modification */
	LINK->p = (LINK->h)[i][j] + LINK->q * (LINK->h)[i + 1][j];
	if (i < LINK->na) {
	  LINK->p += LINK->r * (LINK->h)[i + 2][j];
	  (LINK->h)[i + 2][j] -= LINK->p * LINK->zz;
	}
	(LINK->h)[i + 1][j] -= LINK->p * LINK->y;
	(LINK->h)[i][j] -= LINK->p * LINK->x;
      }
      FORLIM1 = Min(LINK->en, i + 3, LINK->LINK);
      for (j = 1; j <= FORLIM1; j++) {   /* column modification */
	LINK->p = LINK->x * (LINK->h)[j][i] + LINK->y * (LINK->h)[j][i + 1];
	if (i < LINK->na) {
	  LINK->p += LINK->zz * (LINK->h)[j][i + 2];
	  (LINK->h)[j][i + 2] -= LINK->p * LINK->r;
	}
	(LINK->h)[j][i + 1] -= LINK->p * LINK->q;
	(LINK->h)[j][i] -= LINK->p;
      }
      FORLIM1 = LINK->indexL;
      for (j = LINK->indexK; j <= FORLIM1; j++)
      {   /* accumulate transformations */
	LINK->p = LINK->x * LINK->LINK->EigenVector->Re[j][i] +
		  LINK->y * LINK->LINK->EigenVector->Re[j][i + 1];
	if (i < LINK->na) {
	  LINK->p += LINK->zz * LINK->LINK->EigenVector->Re[j][i + 2];
	  LINK->LINK->EigenVector->Re[j][i + 2] -= LINK->p * LINK->r;
	}
	LINK->LINK->EigenVector->Re[j][i + 1] -= LINK->p * LINK->q;
	LINK->LINK->EigenVector->Re[j][i] -= LINK->p;
      }
    }
  }
}  /* DoubleQR */

/* Local variables for AllRootsFound: */
struct LOC_AllRootsFound {
  struct LOC_QR_Method *LINK;
} ;

Local Void RealVector(LINK)
struct LOC_AllRootsFound *LINK;
{
  long i, j, m, FORLIM1;
  double TEMP;
  long TEMP1;
  m = LINK->LINK->en;
  (LINK->LINK->h)[LINK->LINK->en][LINK->LINK->en] = 1.0;
  for (i = LINK->LINK->na; i >= 1; i--) {
    LINK->LINK->w = (LINK->LINK->h)[i][i] - LINK->LINK->p;
    LINK->LINK->r = (LINK->LINK->h)[i][LINK->LINK->en];
    FORLIM1 = LINK->LINK->na;
    for (j = m; j <= FORLIM1; j++) {
      LINK->LINK->r += (LINK->LINK->h)[i][j] * (LINK->LINK->h)[j]
		       [LINK->LINK->en];
    }
    if (LINK->LINK->LINK->EigenValue->Im[i] < 0.0) {
      LINK->LINK->zz = LINK->LINK->w;
      LINK->LINK->s = LINK->LINK->r;
    } else {
      m = i;
      if (LINK->LINK->LINK->EigenValue->Im[i] == 0.0) {
	LINK->LINK->t = LINK->LINK->w;
	if (LINK->LINK->w == 0.0)
	  LINK->LINK->t = LINK->LINK->LINK->eps * LINK->LINK->norm;
	(LINK->LINK->h)[i][LINK->LINK->en] = -(LINK->LINK->r / LINK->LINK->t);
      } else {  /* solve real equations */
	LINK->LINK->x = (LINK->LINK->h)[i][i + 1];
	LINK->LINK->y = (LINK->LINK->h)[i + 1][i];
	TEMP = LINK->LINK->LINK->EigenValue->Re[i] - LINK->LINK->p;
	TEMP1 = LINK->LINK->LINK->EigenValue->Im[i];
	LINK->LINK->q = TEMP * TEMP + TEMP1 * TEMP1;
	LINK->LINK->t = (LINK->LINK->x * LINK->LINK->s -
			 LINK->LINK->zz * LINK->LINK->r) / LINK->LINK->q;
	(LINK->LINK->h)[i][LINK->LINK->en] = LINK->LINK->t;
	if (fabs(LINK->LINK->x) > fabs(LINK->LINK->zz)) {
	  (LINK->LINK->h)[i + 1][LINK->LINK->en] =
	    -((LINK->LINK->r + LINK->LINK->w * LINK->LINK->t) / LINK->LINK->x);
	} else {
	  (LINK->LINK->h)[i + 1][LINK->LINK->en] =
	    -((LINK->LINK->s + LINK->LINK->y * LINK->LINK->t) / LINK->LINK->zz);
	}
      }
    }
  }
}  /* RealVector */

Local Void ComplexVector(LINK)
struct LOC_AllRootsFound *LINK;
{
  long i, j, m;
  double TEMP, TEMP1;
  long FORLIM1, TEMP2;

  m = LINK->LINK->na;
  if (abs((LINK->LINK->h)[LINK->LINK->en][LINK->LINK->na]) <=
      abs((LINK->LINK->h)[LINK->LINK->na][LINK->LINK->en])) {
    TEMP = (LINK->LINK->h)[LINK->LINK->na][LINK->LINK->na];
    TEMP1 = (LINK->LINK->h)[LINK->LINK->na][LINK->LINK->en];
    ComplexDiv(0.0,
      (double)(-(LINK->LINK->h)[LINK->LINK->na][LINK->LINK->en]),
      (LINK->LINK->h)[LINK->LINK->na][LINK->LINK->na] - LINK->LINK->p,
      LINK->LINK->q, &TEMP, &TEMP1, LINK->LINK->LINK);
  } else {
    (LINK->LINK->h)[LINK->LINK->na]
      [LINK->LINK->na] = LINK->LINK->q / (LINK->LINK->h)[LINK->LINK->en]
			 [LINK->LINK->na];
    (LINK->LINK->h)[LINK->LINK->na]
      [LINK->LINK->en] = (LINK->LINK->p - (LINK->LINK->h)[LINK->LINK->en]
			  [LINK->LINK->en]) / (LINK->LINK->h)[LINK->LINK->en]
			 [LINK->LINK->na];
  }
  (LINK->LINK->h)[LINK->LINK->en][LINK->LINK->na] = 0.0;
  (LINK->LINK->h)[LINK->LINK->en][LINK->LINK->en] = 1.0;
  for (i = LINK->LINK->na - 1; i >= 1; i--) {
    LINK->LINK->w = (LINK->LINK->h)[i][i] - LINK->LINK->p;
    LINK->LINK->ra = 0.0;
    LINK->LINK->sa = (LINK->LINK->h)[i][LINK->LINK->en];
    FORLIM1 = LINK->LINK->na;
    for (j = m; j <= FORLIM1; j++) {
      LINK->LINK->ra += (LINK->LINK->h)[i][j] * (LINK->LINK->h)[j]
			[LINK->LINK->na];
      LINK->LINK->sa += (LINK->LINK->h)[i][j] * (LINK->LINK->h)[j]
			[LINK->LINK->en];
    }
    if (LINK->LINK->LINK->EigenValue->Im[i] < 0.0) {
      LINK->LINK->zz = LINK->LINK->w;
      LINK->LINK->r = LINK->LINK->ra;
      LINK->LINK->s = LINK->LINK->sa;
    } else {
      m = i;
      if (LINK->LINK->LINK->EigenValue->Im[i] == 0.0) {
	TEMP = (LINK->LINK->h)[i][LINK->LINK->na];
	TEMP1 = (LINK->LINK->h)[i][LINK->LINK->en];
	ComplexDiv(-LINK->LINK->ra, -LINK->LINK->sa, LINK->LINK->w,
		   LINK->LINK->q, &TEMP, &TEMP1, LINK->LINK->LINK);
      } else {  /* solve the complex equations */
	LINK->LINK->x = (LINK->LINK->h)[i][i + 1];
	LINK->LINK->y = (LINK->LINK->h)[i + 1][i];
	TEMP = LINK->LINK->LINK->EigenValue->Re[i] - LINK->LINK->p;
	TEMP2 = LINK->LINK->LINK->EigenValue->Im[i];
	LINK->LINK->vr = TEMP * TEMP +
			 TEMP2 * TEMP2 - LINK->LINK->q * LINK->LINK->q;
	LINK->LINK->vi = 2.0 * (LINK->LINK->LINK->EigenValue->Re[i] - LINK->
				  LINK->p) * LINK->LINK->q;
	if (LINK->LINK->vr == 0.0 && LINK->LINK->vi == 0.0)
	  LINK->LINK->vr = LINK->LINK->LINK->eps * LINK->LINK->norm *
	      (fabs(LINK->LINK->w) + fabs(LINK->LINK->q) + fabs(LINK->LINK->x) +
	       fabs(LINK->LINK->y) + fabs(LINK->LINK->zz));
	TEMP = (LINK->LINK->h)[i][LINK->LINK->na];
	TEMP1 = (LINK->LINK->h)[i][LINK->LINK->en];
	ComplexDiv(LINK->LINK->x * LINK->LINK->r -
	    LINK->LINK->zz * LINK->LINK->ra + LINK->LINK->q * LINK->LINK->sa,
	  LINK->LINK->x * LINK->LINK->s - LINK->LINK->zz * LINK->LINK->sa -
	  LINK->LINK->q * LINK->LINK->ra, LINK->LINK->vr, LINK->LINK->vi,
	  &TEMP, &TEMP1, LINK->LINK->LINK);
	if (fabs(LINK->LINK->x) <= fabs(LINK->LINK->zz) + fabs(LINK->LINK->q)) {
	  TEMP = (LINK->LINK->h)[i + 1][LINK->LINK->na];
	  TEMP1 = (LINK->LINK->h)[i + 1][LINK->LINK->en];
	  ComplexDiv(-LINK->LINK->r - LINK->LINK->y * (LINK->LINK->h)[i]
				      [LINK->LINK->na],
		     -LINK->LINK->s - LINK->LINK->y * (LINK->LINK->h)[i]
				      [LINK->LINK->en], LINK->LINK->zz,
		     LINK->LINK->q, &TEMP, &TEMP1, LINK->LINK->LINK);
	} else {
	  (LINK->LINK->h)[i + 1]
	    [LINK->LINK->na] = (LINK->LINK->q * (LINK->LINK->h)[i]
		  [LINK->LINK->en] - LINK->LINK->w * (LINK->LINK->h)[i]
		  [LINK->LINK->na] - LINK->LINK->ra) / LINK->LINK->x;
	  (LINK->LINK->h)[i + 1][LINK->LINK->en] =
	    (-LINK->LINK->sa - LINK->LINK->w * (LINK->LINK->h)[i]
	       [LINK->LINK->en] - LINK->LINK->q * (LINK->LINK->h)[i]
				  [LINK->LINK->na]) / LINK->LINK->x;
	}
      }
    }
  }
}  /* ComplexVector */

Local Void AllRootsFound(LINK)
struct LOC_QR_Method *LINK;
{
  struct LOC_AllRootsFound V;
  long i, j, k, m, FORLIM, FORLIM1;

  V.LINK = LINK;
  for (LINK->en = LINK->n; LINK->en >= 1; LINK->en--) {
	/* back substitution */
	  LINK->p = LINK->LINK->EigenValue->Re[LINK->en];
    LINK->q = LINK->LINK->EigenValue->Im[LINK->en];
    LINK->na = LINK->en - 1;
    if (LINK->q == 0.0)
      RealVector(&V);
    else {
      if (LINK->q < 0.0)
	ComplexVector(&V);
    }
  }
  FORLIM = LINK->n;
  for (i = 1; i <= FORLIM; i++) {   /* vectors of isolated roots */
    if (i < LINK->indexK || i > LINK->indexL) {
      FORLIM1 = LINK->n;
      for (j = i; j <= FORLIM1; j++) {
	LINK->LINK->EigenVector->Re[i][j] = (LINK->h)[i][j];
      }
    }
  }
  if (LINK->indexL <= 0)   /* multiply by the transformation matrix */
    return;
  FORLIM = LINK->indexK;
  for (j = LINK->n; j >= FORLIM; j--) {
    m = Min(j, LINK->indexL, LINK->LINK);
    FORLIM1 = LINK->indexL;
    for (i = LINK->indexK; i <= FORLIM1; i++) {
      LINK->zz = 0.0;
      for (k = LINK->indexK; k <= m; k++) {
	LINK->zz += LINK->LINK->EigenVector->Re[i][k] * (LINK->h)[k][j];
      }
      LINK->LINK->EigenVector->Re[i][j] = LINK->zz;
    }
  }
}  /* AllRootsFound */

/***************************************************************************/
/*        Module 4      QR Method to obtain eigenvalues and eigenvectors   */
/***************************************************************************/

Void QR_Method(h_, nSize, indexK_, indexL_, LINK)
long nSize,h_[nSize][nSize],indexK_, indexL_;
struct LOC_EigenR *LINK;
{
  struct LOC_QR_Method V;
  long i, iterations, j, ka;
  boolean exit, loop, terminate;
  long FORLIM, FORLIM1;

  V.LINK = LINK;
  for(i=0;i<nSize;i++)
    for(j=0;j<nSize;j++)
      V.h[i][j]=h_[i][j];
  V.n = nSize;
  V.indexK = indexK_;
  V.indexL = indexL_;
  V.norm = 0.0;
  ka = 1;
  FORLIM = V.n;
  for (i = 1; i <= FORLIM; i++)
  {   /* store roots isolated by procedure Balance */
    FORLIM1 = V.n;
    for (j = ka; j <= FORLIM1; j++) {
      V.norm += abs((V.h)[i][j]);
    }
    ka = i;
    if (i < V.indexK || i > V.indexL) {
      LINK->EigenValue->Re[i] = (V.h)[i][i];
      LINK->EigenValue->Im[i] = 0.0;
    }
  }
  V.t = 0.0;
  V.en = V.indexL;
  terminate = false;
  while (V.en >= V.indexK && !terminate) {  /* search for next eigenvalues */
    iterations = 0;
    V.na = V.en - 1;
    do {
      V.jb = V.en;
      exit = false;
      loop = false;
      while (V.jb > V.indexK && !exit)
      {  /* look for single small sub-diagonal element */
	V.s = abs((V.h)[V.jb - 1][V.jb - 1]) + abs((V.h)[V.jb][V.jb]);
	if (V.s == 0.0)
	  V.s = V.norm;
	if (abs((V.h)[V.jb][V.jb - 1]) <= LINK->eps * V.s) {
	  exit = true;
	} else
	  V.jb--;
      }
      V.x = (V.h)[V.en][V.en];
      if (V.jb == V.en)
	OneRootFound(&V);
      else {
	V.y = (V.h)[V.na][V.na];
	V.w = (V.h)[V.en][V.na] * (V.h)[V.na][V.en];
	if (V.jb == V.na)
	  TwoRootsFound(&V);
	else {
	  if (iterations < 30) {
	    if ((unsigned long)iterations < 32 &&
		((1L << iterations) & 0x100400L) != 0)
	    {  /* form shift */
	      V.t += V.x;
	      FORLIM = V.en;
	      for (i = V.indexK; i <= FORLIM; i++) {
		(V.h)[i][i] -= V.x;
	      }
	      V.s = abs((V.h)[V.en][V.na]) + abs((V.h)[V.na][V.en - 2]);
	      V.x = 0.75 * V.s;
	      V.y = V.x;
	      V.w = -0.4375 * V.s * V.s;
	    }
	    iterations++;
	    DoubleQR(Search(&V), &V);
	    loop = true;
	  } else {  /* no convergence */
	    *LINK->flag = V.en;
	    terminate = true;
	  }
	}
      }
    } while (!terminate && loop);
  }
  if (!terminate && V.norm > 0.0)   /* now backsubstitute */
    AllRootsFound(&V);
}  /* QR_Method */

/***************************************************************************/
/*        Module 5     Performance Index                                   */
/***************************************************************************/

double MatrixNorm(a, n, LINK)
long a[MaxN][MaxM], n;
struct LOC_EigenR *LINK;
{
  /* The 1-norm of a matrix */
  long i, j;
  double s, sum;

  s = 0.0;
  for (j = 1; j <= n; j++) {
    sum = 0.0;
    for (i = 1; i <= n; i++) {
      sum += abs(a[i][j]);
    }
    s = Max(s, sum, LINK);
  }
  if (s == 0.0)
    return 1.0;
  else
    return s;
}  /* MatrixNorm */

double PerformanceIndex(LINK)
struct LOC_EigenR *LINK;
{
  long i, j, k;
  double p, s, sumz, sumr, sumi, ReE, ImE;
  long FORLIM, FORLIM1, FORLIM2;

  p = 0.0;
  FORLIM = LINK->n;
  for (j = 1; j <= FORLIM; j++) {
    s = 0.0;
    sumz = 0.0;
    FORLIM1 = LINK->n;
    for (i = 1; i <= FORLIM1; i++) {
      ReE = LINK->EigenVector->Re[i][j];
      ImE = LINK->EigenVector->Im[i][j];
      sumz += sqrt(ReE * ReE + ImE * ImE);
      sumr = LINK->EigenValue->Im[j] * ImE - LINK->EigenValue->Re[j] * ReE;
      sumi = -LINK->EigenValue->Re[j] * ImE - LINK->EigenValue->Im[j] * ReE;
      FORLIM2 = LINK->n;
      for (k = 1; k <= FORLIM2; k++) {
	sumr += (LINK->A)[i][k] * LINK->EigenVector->Re[k][j];
	sumi += (LINK->A)[i][k] * LINK->EigenVector->Im[k][j];
      }
      s += sqrt(sumr * sumr + sumi * sumi);
    }
    p = Max(p, s / sumz, LINK);
  }
  return (p / (10.0 * LINK->n * LINK->eps * MatrixNorm(LINK->A, LINK->n, LINK)));
}  /* PerformanceIndex */

/***************************************************************************/
/*        Module 6     Obtain eigenvectors, clean up, sort, and normalize  */
/***************************************************************************/

Void Eigs(LINK)
struct LOC_EigenR *LINK;
{
  /* Obtain the eigenvectors */
  long i, j, k, FORLIM;
  
  FORLIM = LINK->n;
  for (i = *LINK->flag + 1; i <= FORLIM; i++) {
    LINK->EigenValue->Re[i - *LINK->flag] = LINK->EigenValue->Re[i];
    LINK->EigenValue->Im[i - *LINK->flag] = LINK->EigenValue->Im[i];
    j = LINK->n;
    while (j > *LINK->flag) {
      k = j - *LINK->flag;
      if (LINK->EigenValue->Im[k] == 0.0) {
	FORLIM = LINK->n;
	for (i = 1; i <= FORLIM; i++) {
	  LINK->EigenVector->Re[i][k] = LINK->EigenVector->Re[i][j];
	  LINK->EigenVector->Im[i][k] = 0.0;
	}
	j--;
	continue;
      }
      FORLIM = LINK->n;
      for (i = 1; i <= FORLIM; i++) {
	LINK->EigenVector->Im[i][k] = -LINK->EigenVector->Re[i][j];
	LINK->EigenVector->Im[i][k - 1] = LINK->EigenVector->Re[i][j];
	LINK->EigenVector->Re[i][k] = LINK->EigenVector->Re[i][j - 1];
	LINK->EigenVector->Re[i][k - 1] = LINK->EigenVector->Re[i][j - 1];
      }
      j -= 2;
    }
  }  /* Eigs */
}
Void CleanUp(LINK)
struct LOC_EigenR *LINK;
{
  /* if not all the eigenvalues were found */
  long i, j, FORLIM, FORLIM1;

  FORLIM = LINK->n;
  for (j = *LINK->flag + 1; j <= FORLIM; j++) {
    LINK->EigenValue->Re[j] = 0.0;
    LINK->EigenValue->Im[j] = 0.0;
    FORLIM1 = LINK->n;
    for (i = 1; i <= FORLIM1; i++) {
      LINK->EigenVector->Re[i][j] = 0.0;
      LINK->EigenVector->Im[i][j] = 0.0;
    }
  }
}  /* CleanUp */

Void Sort(m, LINK)
long m;
struct LOC_EigenR *LINK;
{
  /* The eigenvalues and eigenvectors are sorted so that
     EigenValue.Re[1] >= EigenValue.Re[2] >= ... >= EigenValue.Re[m],  where
     m  is the index of the last eigenvalue successfully found. */
  long i, j, k;
  double t, temp1, temp2;
  long FORLIM1, TEMP, TEMP3, FORLIM2;

  for (j = 1; j <= m; j++) {   /* normalize the eigenvectors */
    t = 0.0;
    FORLIM1 = LINK->n;
    for (i = 1; i <= FORLIM1; i++) {
      TEMP = LINK->EigenVector->Re[i][j];
      TEMP3 = LINK->EigenVector->Im[i][j];
      t += TEMP * TEMP + TEMP3 * TEMP3;
    }
    k = (long)(0.5 * log(t) / log(base));
    t = base;
    if (k < 0) {
      FORLIM1 = abs(k);
      for (i = 1; i <= FORLIM1; i++)
	t /= base;
    } else {
      for (i = 1; i <= k; i++)
	t = base * t;
    }
    FORLIM1 = LINK->n;
    for (i = 1; i <= FORLIM1; i++) {
      LINK->EigenVector->Re[i][j] /= t;
      LINK->EigenVector->Im[i][j] /= t;
    }
  }  /* of normalization */
  for (j = 1; j < m; j++) {   /* re-order the eigenvalues and eigenvectors */
    t = LINK->EigenValue->Re[j];
    for (i = j + 1; i <= m; i++) {
      if (LINK->EigenValue->Re[i] > t) {   /* swap */
	temp1 = LINK->EigenValue->Re[j];
	temp2 = LINK->EigenValue->Im[j];
	LINK->EigenValue->Re[j] = LINK->EigenValue->Re[i];
	LINK->EigenValue->Im[j] = LINK->EigenValue->Im[i];
	LINK->EigenValue->Re[i] = temp1;
	LINK->EigenValue->Im[i] = temp2;
	t = LINK->EigenValue->Re[j];
	FORLIM2 = LINK->n;
	for (k = 1; k <= FORLIM2; k++) {
	  temp1 = LINK->EigenVector->Re[k][j];
	  temp2 = LINK->EigenVector->Im[k][j];
	  LINK->EigenVector->Re[k][j] = LINK->EigenVector->Re[k][i];
	  LINK->EigenVector->Im[k][j] = LINK->EigenVector->Im[k][i];
	  LINK->EigenVector->Re[k][i] = temp1;
	  LINK->EigenVector->Im[k][i] = temp2;
	}
      }
    }
  }
}  /* Sort */

Void EigenR(int nSize,int A_[nSize][nSize],Cvector *lambda,Cmatrix *EigenVectors,double Index,int *retFlag)
{

  /*  Input:
         A    The matrix whose eigenvalues and eigenvectors are to be found
         n    The order of the matrix

     Output:
        EigenValue.Re[k]     The real part of eigenvalue k
        EigenValue.Im[k]     The imaginary part of eigenvalue k
        EigenVector.Re[j,k]  The real part of the j-th component of the
                               k-th eigenvector
        EigenVector.Im[j,k]  The imaginary part of the j-th component of the
                               k-th eigenvector
        Index  The performance index.
                If  Index < 1.0  then the performance of the routines is
                considered to be excellent, in the sense that the residuals
                Az - wz  are as small as can be expected.
                When  1.0 < Index < 100.0  the performance is good.
                When  Index > 100.0  the performance is considered poor.

        flag = n  Normal termination; all eigenvalues/eigenvectors have been
                    successfully found.
        flag < n  Error termination; procedure QR_Method failed to converge.
                    Eigenvalues  1,2,...,flag  have been computed correctly.
                    Eigenvalues  flag+1,flag+2,...,n  are set to  zero
                    and  Index  is set to 1000.0 .

       Adapted from the IMSL FORTRAN subroutines EIGRF, EBALAF, EHESSF,
       EQRH3F, EHBCKF, EBBCKF. */
  struct LOC_EigenR V;
  int d1,d2, i, indexK, indexL, j,FORLIM, FORLIM1;
  double z11;
  /***************************************************************************/
  /*               Module 7       Main procedure                             */
  /***************************************************************************/
  V.n = nSize;
  for(i=0;i<V.n;i++)
    for(j=0;j<V.n;j++)
      V.A[i][j] =A_[i][j];
  V.EigenValue = lambda;
  V.EigenVector = EigenVectors;
  V.flag = retFlag;
  V.flag = 0;
  V.eps = MachineEpsilon();
  for(i=0;i<V.n;i++)
    for(j=0;j<V.n;j++)
      V.EigenVector->Im[i][j]=V.A[i][j];
  /* for(i=0;i<V.n;i++)
    for(j=0;j<V.n;j++)
    V.EigenVector->Im[i][j] = V.A[i][j];*/   /* save the matrix */
  Balance(V.A, &d1, V.n, &indexK, &indexL, &V);
  /*  balance the matrix  a  */
  /* if  indexL = 0,  a  is already in Hessenberg form */
  if (indexL > 0) {
    Hessenberg(V.A, &d2, V.n, indexK, indexL, &V);
    FORLIM = V.n;
    for (i = 1; i <= FORLIM; i++) {
      FORLIM1 = V.n;
      for (j = 1; j <= FORLIM1; j++) {
	V.EigenVector->Re[i][j] = 0.0;
      }
      V.EigenVector->Re[i][i] = 1.0;
    }
    Hback(V.A, &d2, V.n, V.n, indexK, indexL, &V);
  }
  if (V.n == 1) {
    z11 = V.EigenVector->Re[1][1];
  }
  QR_Method(V.A, V.n, indexK, indexL, &V);
  if (V.n == 1) {
    V.EigenVector->Re[1][1] = z11;
  }
  Bback(&V.EigenVector->Re, &d1, V.n, V.n, indexK, indexL, &V);
  for(i=0;i<V.n;i++)
    for(j=0;j<V.n;j++)
      V.A[i][j] = V.EigenVector->Im[i][j];
  Eigs(&V);
  *V.flag = V.n - *V.flag;
  if (*V.flag >= V.n) {
    Sort(*V.flag, &V);
    Index = PerformanceIndex(&V);
    return;
  }
  CleanUp(&V);
  Sort(*V.flag, &V);
  Index = 1000.0;
}  /* EigenR */

#undef base






/* End. */






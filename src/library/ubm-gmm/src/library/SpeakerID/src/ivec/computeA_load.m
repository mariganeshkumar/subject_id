% loads a -v73.file

function Afl = computeA_load(fname_prefix,M)
    for m=1:M
      toload = strcat(fname_prefix,'.',int2str(m));
      load(toload,'-mat');
      Afl{m} = tosave;
    end

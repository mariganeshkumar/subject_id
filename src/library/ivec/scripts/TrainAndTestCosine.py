import argparse
import pickle
import os
import shutil
import sys
import scipy.io as sio
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import accuracy_score
from tqdm import tqdm

sys.path.insert(0, 'library/svm_scripts')

parser = argparse.ArgumentParser()

parser.add_argument("--trainDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--testDataScriptFile", help="Location of script file for test files.",
                    type=str, )

parser.add_argument("--valDataScriptFile", help="Location of script file for script files.",
                    type=str, )
parser.add_argument("--testResultFolder", help="Location of the folder to write test result.",
                    type=str, )
parser.add_argument("--valResultFolder", help="Location of the folder to write test result.",
                    type=str, )
parser.add_argument("--wMat", help="Location of whiting transform matrix.",
                    type=str, )
parser.add_argument("--ldaMat", help="Location of LDA transform matrix.",
                    type=str, )
parser.add_argument("--scoreMat", help="File to store final score matrix.",
                    type=str, )
parser.add_argument("--saveDir", help="File to store final score matrix.",
                    type=str, )

args = parser.parse_args()
tmpDir = "tmp/svd"
saveFile = args.saveDir +"/sub_space_matrix.mat"

if os.path.exists(tmpDir):
    shutil.rmtree(tmpDir)
os.makedirs(tmpDir)

trainSubSpaceVectors = []
testSubSpaceVectors = []
valSubSpaceVectors = []

with open(args.trainDataScriptFile) as f:
    trainDataScript = f.readlines()
trainDataScript = [x.strip() for x in trainDataScript]
trainFiles = [x.split(' ')[0] for x in trainDataScript]
trainLabel = [int(x.split(' ')[1]) for x in trainDataScript]

for i in tqdm(range(len(trainFiles))):
    filename = trainFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        trainSubSpaceVectors.append(iVec)

with open(args.valDataScriptFile) as f:
    valDataScript = f.readlines()
valDataScript = [x.strip() for x in valDataScript]
valFiles = [x.split(' ')[0] for x in valDataScript]
valLabel = [int(x.split(' ')[1]) for x in valDataScript]

for i in tqdm(range(len(valFiles))):
    filename = valFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        valSubSpaceVectors.append(iVec)

with open(args.testDataScriptFile) as f:
    testDataScript = f.readlines()
testDataScript = [x.strip() for x in testDataScript]
testFiles = [x.split(' ')[0] for x in testDataScript]
testLabel = [int(x.split(' ')[1]) for x in testDataScript]

for i in tqdm(range(len(testFiles))):
    filename = testFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        testSubSpaceVectors.append(iVec)

trainSubSpaceVectors = np.array(trainSubSpaceVectors)
valSubSpaceVectors = np.array(valSubSpaceVectors)
testSubSpaceVectors = np.array(testSubSpaceVectors)

trainLabel = np.array(trainLabel)
valLabel = np.array(valLabel)
testLabel = np.array(testLabel)


clf = pickle.load(open(args.ldaMat))
trainSubSpaceVectors = clf.transform(trainSubSpaceVectors)
valSubSpaceVectors = clf.transform(valSubSpaceVectors)
testSubSpaceVectors = clf.transform(testSubSpaceVectors)


sio.savemat(saveFile,{
    'trainSubSpaceVectors': trainSubSpaceVectors,
    'valSubSpaceVectors': valSubSpaceVectors,
    'testSubSpaceVectors': testSubSpaceVectors,
    'trainLabel': trainLabel,
    'valLabel': valLabel,
    'testLabel': testLabel,
})

print("Computing Cosine Kernal")
TrainDataKernel = cosine_similarity(trainSubSpaceVectors, trainSubSpaceVectors)
ValDataKernel = cosine_similarity(valSubSpaceVectors, trainSubSpaceVectors)
TestDataKernel = cosine_similarity(testSubSpaceVectors, trainSubSpaceVectors)
print("Cosine Similarity testing")
predictedLabels = np.argmax(TestDataKernel, axis=1)
predictedLabels = predictedLabels+1
print(accuracy_score(testLabel,predictedLabels) * 100)
pred = predictedLabels.tolist()

for i in range(len(pred)):
    filename = args.testResultFolder + str(i + 1) + '.out'
    with open(filename, 'w') as f:
        f.write("label " + str(pred[i]) + " \n")
        f.close()


predictedLabels = np.argmax(ValDataKernel, axis=1)
predictedLabels = predictedLabels+1
pred = predictedLabels.tolist()
print(accuracy_score(valLabel,predictedLabels) * 100)
for i in range(len(pred)):
    filename = args.valResultFolder + str(i + 1) + '.out'
    with open(filename, 'w') as f:
        f.write("label " + str(pred[i]) + " \n")
        f.close()

print("Collecting Target and Non-Target Scores")
target_test_scores=[]
non_target_test_scores=[]
target_val_scores=[]
non_target_val_scores=[]
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(TestDataKernel[np.where(testLabel==i), :], axis=0)
    target_scores = np.squeeze(scores[:,np.where(trainLabel==i)],axis=-1)
    non_target_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_test_scores.append(target_scores)
    non_target_test_scores.append(non_target_scores)

    scores = np.squeeze(ValDataKernel[np.where(valLabel==i), :], axis=0)
    target_scores = np.squeeze(scores[:,np.where(trainLabel==i)],axis=-1)
    non_target_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_val_scores.append(target_scores)
    non_target_val_scores.append(non_target_scores)

target_test_scores = np.concatenate(target_test_scores, axis=0)
non_target_test_scores = np.concatenate(non_target_test_scores,axis=0)
target_val_scores = np.concatenate(target_val_scores, axis=0)
non_target_val_scores = np.concatenate(non_target_val_scores,axis=0)



print("Collecting Normalised Target and Non-Target Scores")

finalTestScore=[]
for i in tqdm(np.unique(trainLabel)):
    score = np.squeeze(TestDataKernel[:, np.where(trainLabel==i)]);
    imposter_score = np.squeeze(TestDataKernel[:, np.where(trainLabel!=i)])
    mean_is = np.mean(imposter_score,axis=1)
    std_is = np.std(imposter_score,axis=1)
    score = (score - mean_is)/std_is
    finalTestScore.append(score)
finalTestScore = np.transpose(np.array(finalTestScore))


finalValScore=[]
for i in tqdm(np.unique(trainLabel)):
    score = np.squeeze(ValDataKernel[:, np.where(trainLabel==i)]);
    imposter_score = np.squeeze(ValDataKernel[:, np.where(trainLabel!=i)])
    mean_is = np.mean(imposter_score,axis=1)
    std_is = np.std(imposter_score,axis=1)
    score = (score - mean_is)/std_is
    finalValScore.append(score)
finalValScore = np.transpose(np.array(finalValScore))


target_test_norm_scores=[]
non_target_test_norm_scores=[]
target_val_norm_scores=[]
non_target_val_norm_scores=[]
for i in tqdm(np.unique(trainLabel)):
    scores = np.squeeze(finalTestScore[np.where(testLabel==i), :], axis=0)
    target_norm_scores = np.squeeze(scores[:,np.where(trainLabel==i)],axis=-1)
    non_target_norm_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_test_norm_scores.append(target_norm_scores)
    non_target_test_norm_scores.append(non_target_norm_scores)

    scores = np.squeeze(finalValScore[np.where(valLabel==i), :], axis=0)
    target_norm_scores = np.squeeze(scores[:,np.where(trainLabel==i)], axis=-1)
    non_target_norm_scores = np.squeeze(scores[:,np.where(trainLabel!=i)]).flatten()
    target_val_norm_scores.append(target_norm_scores)
    non_target_val_norm_scores.append(non_target_norm_scores)

target_test_norm_scores = np.concatenate(target_test_norm_scores, axis=0)
non_target_test_norm_scores = np.concatenate(non_target_test_norm_scores,axis=0)
target_val_norm_scores = np.concatenate(target_val_norm_scores, axis=0)
non_target_val_norm_scores = np.concatenate(non_target_val_norm_scores,axis=0)



sio.savemat(args.scoreMat,{
    'test_scores': TestDataKernel,
    'val_scores': ValDataKernel,
    'target_test_scores': target_test_scores, 
    'non_target_test_scores': non_target_test_scores,
    'target_val_scores': target_val_scores, 
    'non_target_val_scores': non_target_val_scores,
    'target_test_norm_scores': target_test_norm_scores, 
    'non_target_test_norm_scores': non_target_test_norm_scores,
    'target_val_norm_scores': target_val_norm_scores, 
    'non_target_val_norm_scores': non_target_val_norm_scores,
    })

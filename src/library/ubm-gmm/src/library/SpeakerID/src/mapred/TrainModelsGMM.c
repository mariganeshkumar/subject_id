/*-------------------------------------------------------------------------
 *  TrainModelsGMM.c - Trains models for each speaker given a list of files
 *  Version:	$Name:  $
 *  Module:	
 *
 *  This is the legacy version of TrainModelsGMM. Kept in this repo 
 *  only for reference. Not to be compiled or modfied.
 *  
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 -------------------------------------------------------------------------*/

/************************************************************************
  Function            : train_models- computes feature vectors from a 
                        list of speech files, generates the codebook
			and writes the codebook to a file.

  Input args          :  ctrl_file speaker_table codebook_size
                         feature_name output_codebook_file 

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-2002
*******************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "SphereInterface.h"
#include "BatchProcessWaveform.h"
#include "VQ_Modified.h"
#include "GMM.h"
#include "math.h"
/*-------------------------------------------------------------------------
 *  ComputeThreshold -- Computes the threshold on energy for a waveform
 *    Args:	
 *    Returns:	float
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeThreshold(ASDF *asdf, float threshold)
{
  int i;
  float ave = 0;
  for (i = 0; i < asdf->numFrames; i++)
    ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
  ave = ave/asdf->numFrames;
  return (ave*threshold);
}	/*  End of ComputeThreshold		End of ComputeThreshold   */

/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, FILE *speakerFile, 
					   char *featureName, 
					   unsigned long *numVectors,
					   float thresholdScale) {

  F_VECTOR                       *fvect;
  VECTOR_OF_F_VECTORS            *vfv;
  char                           line[500];
  unsigned long                  totalFrames;
  int                            i;
  unsigned long                  frameNo;
  char                           wavname[500];
  float                          energy, threshold;

  totalFrames = 0;
  while (fgets(line,500,speakerFile) != NULL) {
    sscanf(line,"%s",wavname);
    printf("file read :%s\n",wavname); fflush(stdout);
    GsfOpen(asdf,wavname);
    totalFrames = totalFrames + asdf->numFrames;
    printf("wavname = %s %ld %ld numFrames = %ld\n",wavname,totalFrames,asdf->numSamples, asdf->numFrames); fflush(stdout);
    GsfClose(asdf);
  }
  printf("total no frames = %ld\n", totalFrames);
  fflush(stdout);
  vfv  = (VECTOR_OF_F_VECTORS *) malloc(totalFrames*sizeof(VECTOR_OF_F_VECTORS));
  if (vfv == NULL) {
    printf ("unable to allocate space for vfv \n");
    exit(-1);
  }
  rewind(speakerFile);
  frameNo =0; 
  while (EOF != fscanf(speakerFile,"%s",wavname)) {
    GsfOpen(asdf,wavname);
    printf("numFrames = %d\n", asdf->numFrames);
    threshold = (float) ComputeThreshold(asdf, thresholdScale);
    for (i = 0; i < asdf->numFrames; i++) {
      energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
      if (energy >= threshold) {
	fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
	if(fvect == NULL) {
	  printf("problems fvect\n");
	  fflush(stdout);
	  exit(-1);
	} 
	vfv[frameNo] = fvect;
	frameNo++; 
      }
    }
    GsfClose(asdf);
  }
  *numVectors = frameNo;
  printf("total no of frame processed = %ld\n",*numVectors);
  return(vfv);
}

void Usage() {
   printf(" TrainModelsGMM ctrlFile speakerTable codeBookSize ");
   printf(" featureName numVQIter numGMMIter outputCodebookFile thresholdScale\n"); 
}



int main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i,j;
  unsigned long         numVectors;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize;
  int                   seed;
  if (argc != 9 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  speakerTable = argv[2];
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&VQIter);
  string2 = argv[6];
  sscanf(string2, "%d", &GMMIter);
  string2 = argv[7];
  outputFile = fopen(string2,"w");
  cFile = fopen(cname,"r");
  speakerFile = fopen(speakerTable,"r");
  string1 = argv[8];
  sscanf(string1, "%f", &thresholdScale);
  printf("We are here now\n");
  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  Cstore(asdf->fftSize);
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
							featureName, 
						      &numVectors, 
						      thresholdScale);
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %ld numGMM = %d\n", numVectors, GMMIter);
  ComputeGMM(vfv, numVectors, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             ditherMean, varianceNormalize, seed);
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(outputFile,"%e \n", clusterWts[i]/numVectors);
      printf("%e \n", clusterWts[i]/numVectors);
    } else {
      fprintf(outputFile, "%f\n", 1.0E-10);
      printf("%e\n", 1.0E-10);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
      printf("mean=%e var=%e \n",
	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(outputFile," %e %e ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  fclose(outputFile);
  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
  return(0);
}

















/*-------------------------------------------------------------------------
 * $Log: TrainModelsGMM.c,v $
 * Revision 1.2  2008/03/12 17:49:10  hema
 * Fixed the length of filenames
 *
 * Revision 1.1  2002/04/30 09:34:53  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:30:22  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TrainModelsGMM.c
 -------------------------------------------------------------------------*/

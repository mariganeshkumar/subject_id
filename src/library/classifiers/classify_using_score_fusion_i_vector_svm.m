function [result] = classify_using_score_fusion_i_vector_svm(config,allData)

train_data = allData.train_data;
test_data = allData.test_data;
num_subjects=length(train_data);
num_channels  =  allData.num_channels;
models_dir =  [allData.modelSaveDir,'/',config.ivec_prog_mat_dir];


system(['rm -rf ', config.tmp_dir]);
mkdir(config.ubm_data_dir);
mkdir(config.ubm_temp_dir);

data_dir=[config.ubm_data_dir,'/'];
ubm_data=allData.ubm_data;
filenames = cell([1, num_subjects]);
subject_data = cell([1, num_subjects]);
clear allData.ubm_data;
if (~config.test_only)
    system(['rm -rf ', models_dir]);
    mkdir(models_dir);
    for c = 1:num_channels
        disp(['Preparing UBM Data for Channel:',num2str(c)]);
        parfor i=1:length(ubm_data)
            disp(['Subject : ', num2str(i)]);
            subject_data{i}=[];
            filenames{i}={};
            for j=1:length(ubm_data{i})
                for k=1:length(ubm_data{i}{j})
                    recording_data=ubm_data{i}{j}{k};
                    reshaped_recoding_data=squeeze(recording_data(c,:,:));
                    recording_filename=strcat(data_dir,num2str(c),'_',num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc');
                    [dim, no_of_frames] = size(reshaped_recoding_data);
                    dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                    dlmwrite(recording_filename,reshaped_recoding_data','-append','delimiter',' ');
                    filenames{i} = {filenames{i}{:},recording_filename}
                    subject_data{i}=[subject_data{i} reshaped_recoding_data];
                end
            end
            filenames{i} = SplitAndWriteData(transpose(subject_data{i}),data_dir,[num2str(c),'_',num2str(i)],300);
        end
        
        combined_ubm_data =[];
        for i=1:length(ubm_data)
            combined_ubm_data = [combined_ubm_data subject_data{i}];
        end
        rng(1331);
        rp = randperm(size(combined_ubm_data,2));
        combined_ubm_data = combined_ubm_data(:,rp);

        ubm_filenames=SplitAndWriteData(combined_ubm_data',data_dir,'Shuffled_UBM_Data',2000);

        scriptFileID = fopen([config.ubm_data_dir  '/scriptFile.scp'],'w');
        for j =1:length(ubm_filenames)
            fprintf(scriptFileID,'%s \n',ubm_filenames{j});
        end
        fclose(scriptFileID);
        system(['sh library/ubm_gmm_scripts/SupervisedGMM.sh ',...
            config.ubm_data_dir,'/',num2str(c),'_','scriptFile.scp ' ...
            models_dir,'/UBM_GMM_',num2str(c),'.txt ',...
            num2str(config.mixtures),' ',...
            config.ubm_temp_dir,' ',num2str(1331)]);
        
        
        
        
        
        listFileID = fopen([config.ubm_data_dir,'/',num2str(c),'_','devDataScriptFile.scp'],'w');
        labelFileID = fopen([config.ubm_data_dir,'/',num2str(c),'_','devDataLabelFile.scp'],'w');
        for i =1:length(ubm_data)
            for j =1:length(filenames{i})
                recording_filename=strcat(filenames{i}{j});
                fprintf(listFileID,'%s\n',recording_filename);
                ivector_filename=strcat(filenames{i}{j},'.w');
                fprintf(labelFileID,'%s %d\n',ivector_filename, i);
            end
        end
        fclose(listFileID);
        fclose(labelFileID);
  
        dim = allData.freq_dim;
        mkdir([models_dir,'/',num2str(c)]);
        system(['bash library/ivec/scripts/tmatBuilding.sh ',...
            config.ubm_data_dir,'/',num2str(c),'_','devDataScriptFile.scp ' ,...
            config.ubm_data_dir,'/',num2str(c),'_','devDataLabelFile.scp ',...
            models_dir, '/UBM_GMM_',num2str(c),'.txt_py ',...
            num2str(config.mixtures),' ',...
            num2str(dim),' ',...
            num2str(config.ivec_dim),' ',...
            config.ivec_temp_dir,' ',models_dir,'/',num2str(c), ' ']);
        system(['python2.7 library/ivec/scripts/ComputeLDA.py ',...
            '--devDataScriptFile ',config.ubm_data_dir,'/',num2str(c),'_devDataLabelFile.scp ',...
            '--devMat ',models_dir,'/',num2str(c),'_dev.mat ',...
            '--ldaMat ',models_dir,'/',num2str(c),'_LDA.pkl ',...
            ]);
    end
else
    if exist(models_dir,'dir')~=7
        disp('trained models not found')
        msgID = 'Models:NotFound';
        msg = 'Unable to find trained models.';
        baseException = MException(msgID,msg);
        throw(baseException);
    end
end

if exist([models_dir,'/scorelist.txt'],'file')==2
    delete([models_dir,'/scorelist.txt']);
end
fileID = fopen([models_dir,'/scorelist.txt'],'w');
disp(['Adapting Models']);
data_dir=config.ubm_train_data_dir;
mkdir(data_dir);
test_score=[];
val_score=[];

for c = 1:num_channels
    
    parfor i=1:length(train_data)
        subject_data=[];
        for j=1:length(train_data{i})
            for k=1:length(train_data{i}{j})
                recording_data=train_data{i}{j}{k};
                reshaped_recoding_data=squeeze(recording_data(c,:,:));
                subject_data = cat(2,subject_data,reshaped_recoding_data)
            end
        end
        [dim, no_of_frames] = size(subject_data);
        recording_filename=strcat(data_dir,'/',num2str(c),'_',num2str(i),'.lfcc');
        dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
        dlmwrite(recording_filename,subject_data','-append','delimiter',' ');
    end
    
    
    listFileID = fopen([config.ubm_train_data_dir,'/',num2str(c),'_trainDataScriptFile.scp'],'w');
    labelFileID = fopen([config.ubm_train_data_dir,'/',num2str(c),'_trainDataLabelFile.scp'],'w');
    for i=1:length(train_data)
        recording_filename=strcat(data_dir,'/',num2str(c),'_',num2str(i),'.lfcc');
        fprintf(listFileID,'%s\n',recording_filename);
        ivector_filename=strcat(data_dir,'/',num2str(c),'_',num2str(i),'.lfcc.w');
        fprintf(labelFileID,'%s %d\n',ivector_filename, i);
    end
    
    
    
    disp(['Testing Models']);
    test_filenames = cell([1, num_subjects]);
    val_filenames = cell([1, num_subjects]);
    val_data_dir = [config.ubm_val_data_dir];
    mkdir([config.ubm_val_data_dir,'/result']);
    test_data_dir = [config.ubm_test_data_dir];
    mkdir([config.ubm_test_data_dir,'/result']);
    
    val_per = config.val_per;
    
    parfor i=1:length(test_data)
        t=cputime;
        val_ind=1;
        test_ind=1;
        test_filenames{i}={};
        val_filenames{i}={};
        for j=1:length(test_data{i})
            for k=1:length(test_data{i}{j})
                recording_data=test_data{i}{j}{k};
                recording_data=permute(recording_data,[2,3,1]);
                reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
                [dim, no_of_frames]=size(reshaped_recoding_data);
                if k <= length(test_data{i}{j}) * val_per
                    subject_filename=strcat(val_data_dir,'/',num2str(c),'_',num2str(i),'_',num2str(val_ind),'.lfcc');
                    val_filenames{i}{val_ind}=subject_filename;
                    dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                    dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                    val_ind=val_ind+1;
                else
                    subject_filename=strcat(test_data_dir,'/',num2str(c),'_',num2str(i),'_',num2str(test_ind),'.lfcc');
                    test_filenames{i}{test_ind}=subject_filename;
                    dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                    dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                    test_ind=test_ind+1;
                end
            end
        end
        disp(['Tested subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
    end
    
    listFileID = fopen([config.ubm_val_data_dir,'/',num2str(c),'_valDataScriptFile.scp'],'w');
    labelFileID = fopen([config.ubm_val_data_dir,'/',num2str(c),'_valDataLabelFile.scp'],'w');
    val_gt_label=[];
    ind=1;
    for i =1:length(test_data)
        for j =1:length(val_filenames{i})
            temp_name=split(val_filenames{i}{j},'/');
            adapted_filename = [config.ubm_val_data_dir,'/',temp_name{end}];
            fprintf(listFileID,'%s\n',adapted_filename);
            fprintf(labelFileID,'%s %d\n',[adapted_filename,'.w'], i);
            val_gt_label(ind)=i;
            ind=ind+1;
        end
    end
    fclose(labelFileID);
    fclose(listFileID);
    
    
    listFileID = fopen([config.ubm_test_data_dir,'/',num2str(c),'_testDataScriptFile.scp'],'w');
    labelFileID = fopen([config.ubm_test_data_dir,'/',num2str(c),'_testDataLabelFile.scp'],'w');
    
    test_gt_label=[];
    ind=1;
    for i =1:num_subjects
        for j =1:length(test_filenames{i})
            temp_name=split(test_filenames{i}{j},'/');
            adapted_filename = [config.ubm_test_data_dir,'/',temp_name{end}];
            fprintf(listFileID,'%s\n',adapted_filename);
            fprintf(labelFileID,'%s %d\n',[adapted_filename,'.w'], i);
            test_gt_label(ind)=i;
            ind=ind+1;
        end
    end
    fclose(labelFileID);
    fclose(listFileID);
    
    
    dim = allData.freq_dim;
    system(['bash library/ivec/scripts/extractiVec.sh ',...
        models_dir, '/UBM_GMM_',num2str(c),'.txt_py ',...
        num2str(config.mixtures),' ',...
        num2str(dim),' ',...
        num2str(config.ivec_dim),' ',...
        config.ivec_temp_dir,' ',models_dir,'/',num2str(c),' ',...
        config.ubm_train_data_dir,'/',num2str(c),'_trainDataScriptFile.scp ',...
        config.ubm_val_data_dir,'/',num2str(c),'_valDataScriptFile.scp ',...
        config.ubm_test_data_dir,'/',num2str(c),'_testDataScriptFile.scp ']);
    
    
    
    mkdir([config.ubm_val_data_dir,'/',num2str(c),'_result']);
    mkdir([config.ubm_test_data_dir,'/',num2str(c),'_result']);
    
    
    
    system(['python2.7 library/ivec/scripts/TrainAndTestSVM.py ',...
        '--trainDataScriptFile ',config.ubm_train_data_dir,'/',num2str(c),'_','trainDataLabelFile.scp ',...
        '--valDataScriptFile ',config.ubm_val_data_dir,'/',num2str(c),'_','valDataLabelFile.scp ',...
        '--testDataScriptFile ',config.ubm_test_data_dir,'/',num2str(c),'_','testDataLabelFile.scp ',...
        '--testResultFolder ',config.ubm_test_data_dir,'/result/ ',...
        '--valResultFolder ',config.ubm_val_data_dir,'/result/ ',...
        '--ldaMat ',models_dir,'/',num2str(c),'_','LDA.pkl ',...
        '--scoreMat ',config.tmp_dir,'/scores.mat ',...
        '--saveDir ',config.tmp_dir, ' ']);
    
    make_plots_from_sub_space(config.tmp_dir,[allData.plotSaveDir,'/',num2str(config.val_split)], allData.sessionInfo,1)
    
    
    val_predicted_label=[];
    for i=1:length(val_gt_label)
        file_ID=fopen([config.ubm_val_data_dir,'/result/', num2str(i),'.out'],'r');
        val_predicted_label(i) = fscanf(file_ID,'label %d \n');
        fclose(file_ID);
    end
    
    
    test_predicted_label=[];
    for i=1:length(test_gt_label)
        file_ID=fopen([config.ubm_test_data_dir,'/result/', num2str(i),'.out'],'r');
        test_predicted_label(i) = fscanf(file_ID,'label %d \n');
        fclose(file_ID);
    end
    
    result.val_predicted_label = val_predicted_label;
    result.val_gt_label = val_gt_label;
    result.test_predicted_label = test_predicted_label;
    result.test_gt_label = test_gt_label;
    
    
    data = load([config.tmp_dir,'/scores.mat']);
   
    [~,test_predicted_label] = max(data.test_scores, [], 2);
    test_predicted_label = transpose(test_predicted_label);
    cm = confusionmat(test_predicted_label, test_gt_label);
    test_accuracy = sum(diag(cm))/sum(sum(cm)) * 100;
    
    fprintf(fileID,['Channel ',num2str(c),': ', num2str(test_accuracy), '\n']);
    
    if c ==1
        test_score = data.test_scores;
        val_score = data.val_scores;
    else
        test_score = test_score + data.test_scores;
        val_score = val_score + data.val_scores;
    end
    
    
end
fclose(fileID);
test_score = test_score/num_channels;
val_score = val_score/num_channels;
[~,test_predicted_label] = max(test_score, [], 2);
test_predicted_label = transpose(test_predicted_label);

[~,val_predicted_label] = max(val_score, [], 2);
val_predicted_label = transpose(val_predicted_label);


for i = 1: num_subjects
    scores = test_score;
    target_scores=scores(:,i);
    scores(:,i)=[];
    imposters_mean=mean(scores,2);
    imposters_std=std(scores,[],2);
    target_scores=(target_scores-imposters_mean)./imposters_std;
    norm_test_score(:,i)=target_scores;
    
    scores = val_score;
    target_scores=scores(:,i);
    scores(:,i)=[];
    imposters_mean=mean(scores,2);
    imposters_std=std(scores,[],2);
    target_scores=(target_scores-imposters_mean)./imposters_std;
    norm_val_score(:,i)=target_scores;
end
test_target_scores=[];
test_non_target_scores=[];
norm_test_target_scores=[];
norm_test_non_target_scores=[];

val_target_scores=[];
val_non_target_scores=[];
norm_val_target_scores=[];
norm_val_non_target_scores=[];

for i = 1: num_subjects
    scores = test_score(test_gt_label==i,:);
    test_target_scores= [test_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    test_non_target_scores = [test_non_target_scores,reshape(scores,1,[])];
    scores = norm_test_score(test_gt_label==i,:);
    norm_test_target_scores= [norm_test_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    norm_test_non_target_scores = [norm_test_non_target_scores,reshape(scores,1,[])];
    
    scores = val_score(val_gt_label==i,:);
    val_target_scores= [val_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    val_non_target_scores = [val_non_target_scores,reshape(scores,1,[])];
    scores = norm_val_score(val_gt_label==i,:);
    norm_val_target_scores= [norm_val_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    norm_val_non_target_scores = [norm_val_non_target_scores,reshape(scores,1,[])];
    
    
end

result.val_predicted_label = val_predicted_label;
result.val_gt_label = val_gt_label;
result.test_predicted_label = test_predicted_label;
result.test_gt_label = test_gt_label;
result.target_test_scores = test_target_scores;
result.non_target_test_scores = test_non_target_scores;
result.target_val_scores =  val_target_scores;
result.non_target_val_scores = val_non_target_scores;
result.target_test_norm_scores = norm_test_target_scores;
result.non_target_test_norm_scores = norm_test_non_target_scores;
result.target_val_norm_scores = norm_val_target_scores;
result.non_target_val_norm_scores = norm_val_non_target_scores;
end


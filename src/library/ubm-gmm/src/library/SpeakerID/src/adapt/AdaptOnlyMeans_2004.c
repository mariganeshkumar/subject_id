#include "AdaptOnlyMeans_2004.h"

VECTOR_OF_F_VECTORS* AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
				    VECTOR_OF_F_VECTORS *ubmVars,
				    float *ubmWeights,
				    unsigned int numClusters,
			            VECTOR_OF_F_VECTORS *vfv,
				    unsigned int numVectors,
				    unsigned int relevanceFactor,
				    float probScaleFactor)
  {
	VECTOR_OF_F_VECTORS	*expectation=NULL, *tempMeans;
	float			alpha, beta, normFactorI, maxProb, *logProb;
	unsigned int		dimensions, i, j, k, maxProbIdx, *eeta, tempMaxProbIdx;

	VECTOR_OF_F_VECTORS     *adaptedMeans;

	dimensions = ubmMeans[0]->numElements;

	logProb = (float *) calloc (vfv[0]->numElements, sizeof(float));
	tempMeans = (VFV *) calloc (numClusters, sizeof(VFV));
	for (j = 0; j < numClusters; j++)
	  {
		tempMeans[j] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
		  for (k = 0; k < vfv[0]->numElements; k++)
			tempMeans[j]->array[k] = 0.0;
	  }

	
	eeta    = (unsigned int *) calloc (numClusters, sizeof(unsigned int));
	printf ("Finished initialization\n"); fflush(stdout);
	for (i = 0; i < numVectors; i++)
	   {
		for (j = 0; j < numClusters; j++)
		  {
			logProb[j] = ComputeProbability (ubmMeans[j], ubmVars[j], ubmWeights[j], 
								   vfv[i], probScaleFactor);

			if (j == 0 || logProb[j] > maxProb)
			  { maxProb= logProb[j]; maxProbIdx= (unsigned int)j; }

		  }
		eeta[maxProbIdx]++;
		for (k = 0; k < dimensions; k++)
			tempMeans[maxProbIdx]->array[k] += vfv[i]->array[k];	
		  
	   }

	for (j = 0; j < numClusters; j++)
		for (k = 0; k < dimensions; k++) { if (eeta[j] != 0)
			tempMeans[j]->array[k] /= ((float)eeta[j]);}
		
	adaptedMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
	for (j = 0; j < numClusters; j++)
  	  {
	    adaptedMeans[j] = (F_VECTOR *) AllocFVector (dimensions);
	    
	    alpha = (float) ((float)eeta[j] / ((float)eeta[j] + (float)relevanceFactor));
//	   printf ("alpha is %f while eeta of %d is %f\n", alpha,j,eeta[j]); 
	    for (k = 0; k < dimensions; k++)
	      {
		adaptedMeans[j]->array[k] = (alpha * tempMeans[j]->array[k])
					  + ((1.0 - alpha) *ubmMeans[j]->array[k]);
//		printf ("adaptedMeans %d %d = %f, ubmMeans - %f\n", j,k,adaptedMeans[j]->array[k], ubmMeans[j]->array[k]);
	      }
	  }
	free (logProb);
  return adaptedMeans;
  }




main (int argc, char *argv[])
  {
	char		*controlFileName, *ubmFileName, *featureName, *modelFileName, 
          *trainFileName, *tgname;
	FILE		*controlFile, *ubmFile, *modelFile, *tgFile;
	VFV *vfv, *ubmMeans, *ubmVars, *adaptedMeans;
  VFV *ergMeans, *ergVars;
  float *ergWts;
  int numGaussErg;
	F_VECTOR	    *ubmWeights;
	ASDF 		    *asdf;
	unsigned int	numVectors, ubmSize, featLength, i, j;
	float 		thresholdScale;
	if (argc != 8)
	  {
		printf ("Usage : %s controlFile ubmFile ubmSize wavName featureName modelFile thresholdScale\n", argv[0]);
		exit (0);
	  }
	
   	controlFileName = argv[1];
	ubmFileName     = argv[2];
	sscanf (argv[3], "%u", &ubmSize);
	trainFileName   = argv[4];
	featureName     = argv[5];
	modelFileName   = argv[6];
  tgname          = argv[7];

	
	controlFile = fopen (controlFileName, "r");
	asdf = (ASDF *) calloc (1, sizeof (ASDF));
	InitializeStandardFrontEnd (asdf, controlFile);		
	Cstore (asdf->fftSize);
  
  ergMeans = (VFV*) calloc (3,sizeof(VFV));
  ergVars = (VFV*) calloc (3,sizeof(VFV));
  ergWts = (float *) calloc (3, sizeof (VFV));
  finc(i,3,1) {
    ergMeans[i] = AllocFVector (1);
    ergVars[i] = AllocFVector(1);
  }
  tgFile = fopen (tgname, "r");
  finc(i,3,1) {
    fscanf (tgFile,"%f\n", &ergWts[i]);
    fscanf (tgFile," %f %f\n", &ergMeans[i]->array[0], 
                               &ergVars[i]->array[0]);
    printf ("%f\n", ergWts[i]);
    printf (" %f %f\n", ergMeans[i]->array[0], 
                               ergVars[i]->array[0]);
  }
  fclose(tgFile);

	vfv = (VFV *) ExtractFeatureVectorsVAD
    (asdf,trainFileName, featureName, &numVectors, ergMeans, ergVars, ergWts);
	featLength = vfv[0]->numElements;
  FreeVfv (ergMeans,3);
  FreeVfv (ergVars,3);
  free (ergWts);

	ubmMeans = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	ubmVars  = (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof(VECTOR_OF_F_VECTORS));
	
	for (i = 0; i < ubmSize; i++)
	  {
		ubmMeans[i] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
		ubmVars[i]  = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
	  }
	
	ubmWeights = (F_VECTOR *) AllocFVector (ubmSize);

	ubmFile = fopen (ubmFileName, "r");
	for (i = 0; i < ubmSize; i++)
	  {
		fscanf (ubmFile, "%f\n", &ubmWeights->array[i]);
		for (j = 0; j < featLength; j++)
		{
			fscanf (ubmFile, " %f %f", &ubmMeans[i]->array[j], &ubmVars[i]->array[j]);
		}
		fscanf (ubmFile, "\n");
	  }
	fclose (ubmFile);

	adaptedMeans = (VFV*) AdaptOnlyMeans (ubmMeans, ubmVars, ubmWeights->array,
								ubmSize, vfv, numVectors, 16,
								1.0);
	modelFile = fopen (modelFileName, "w");
	
	for (i = 0; i < ubmSize; i++)
	  {
		fprintf (modelFile, "%e\n", ubmWeights->array[i]);
		for (j = 0; j < featLength; j++)
		  {
			fprintf (modelFile, " %e %e", adaptedMeans[i]->array[j], ubmVars[i]->array[j]);
		  }
		fprintf (modelFile, "\n");
	  }
	fclose (modelFile);	
  }

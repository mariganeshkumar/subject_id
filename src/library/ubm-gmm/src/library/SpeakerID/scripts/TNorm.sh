#!/bin/sh

HOME=/home/srikanth
WORKINGDIR=../
TNORM=/kone/dell.lpbkup/home/JEL/bin/TNorm.e
TESTLIST=../flists/2003/m.test
SPKRLIST=../flists/2003/spkrList.m
GROUNDTRUTH=../flists/2003/GroundTruth
MODELSDIR=../models/2003.m.fcd
RESULTSDIR=$MODELSDIR/AllResults
SPIDMODELS=$MODELSDIR/SpidModel
BACKYARD=$MODELSDIR/dump.2

mkdir $BACKYARD
NISTDIR=/kone/db/nist2003/
CLAIMSFILE=$NISTDIR/disk2/sp03c1s2/test/male/detect3.ndx

rm -f $BACKYARD/true_dist
rm -f $BACKYARD/impostor_dist
# Take each test file
for testCase in `cat $TESTLIST`; do
   testName=`echo $testCase | cut -d '/' -f9 | cut -d '.' -f1`
	gt=`grep $testName $GROUNDTRUTH | cut -d ' ' -f2`
	echo "Test name is $testName"
	# Consider each claim
   for claim in `grep $testName $CLAIMSFILE | cut -d ' ' --fields="2-9"`; do
	   echo "Claim is $claim"
		claimSpkrId=`grep -n $claim $SPKRLIST | cut -d ':' -f1`
		echo "claimSpkrId is $claimSpkrId"
		# Look up Distortion values
#		echo "head -$claimSpkrId $RESULTSDIR/$testName.result.dist | tail -1 > $BACKYARD/$testName.$claim.score"
		head -$claimSpkrId $RESULTSDIR/$testName.result.dist | tail -1 > $BACKYARD/$testName.$claim.score		
		rm -f $BACKYARD/$testName.$claim.cohort
#		for cohortSpeaker in `cat $COHORTDIR/$claim.cohort`; do
#                for cohortSpeaker in `seq 1 50`; do
		#	echo "head -$cohortSpeaker $RESULTSDIR/$testName.result.dist | tail -1	>> $BACKYARD/$testName.$claim.cohort"
#			head -$cohortSpeaker $RESULTSDIR/$testName.result.dist | tail -1 >> $BACKYARD/$testName.$claim.cohort
#		done
		tail -50 $RESULTSDIR/$testName.result.dist  > $BACKYARD/$testName.$claim.cohort
		# Compute TNorm (and save it more importantly)
      $TNORM $BACKYARD/$testName.$claim.score $BACKYARD/$testName.$claim.cohort > $BACKYARD/$testName.$claim.tnorm		
		cat $BACKYARD/$testName.$claim.tnorm

		# Verify for a particular threshold
		# (check if this is required!)

		# store the distortion in one 
		# of the 2 files - true vs impostor
		# (for that speaker?)
#		echo "grep $testName $GROUNDTRUTH | cut -d ' ' -f2"
		trueSpeaker=`grep $testName $GROUNDTRUTH | cut -d ' ' -f2`
		if [ $claim -eq $trueSpeaker ]; then
		   echo "storing in true_dist file"
			cat $BACKYARD/$testName.$claim.tnorm >> $BACKYARD/true_dist
		else
		   echo "storing in impostor file"
		   cat $BACKYARD/$testName.$claim.tnorm >> $BACKYARD/impostor_dist
		fi
	done
done

function chol_LL = chol_PLDA_Log_Likelihood_precomputed_matrices(data,sum_data,iPn,log_det_Pn,log_det_E,iEF,iE)

%This function is called for each speaker
%data contains all the ivectors for the spekaer
%sum_data contains the first order suff stat
[dim,n] = size(data);
t_dim = n*dim;

%Precompute intermediate variables
FtiEsum_data = iEF'*sum_data;
iEdata = iE\(iE'\data);

%chol_LL = -t_dim/2*log(2*pi) - n/2*log_det_E - 0.5*log_det_Pn - 0.5*sum(sum(data.*iEdata)) + 0.5*(FtiEsum_data'*(iPn\(iPn'\FtiEsum_data)));
chol_LL = -t_dim/2*log(2*pi) - n/2*log_det_E - 0.5*log_det_Pn - 0.5*sum(sum(data.*iEdata)) + 0.5*(FtiEsum_data'*(iPn*FtiEsum_data));

end


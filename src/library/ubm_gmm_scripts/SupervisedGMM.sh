#!/usr/bin/env bash

# MODEFILE : file that will contain the model built. CHANGE this every
#            time a new model needs to be built
# CODEBOOKSIZE : number of mixtures
if [ $# != 5 ]; then
	echo "USAGE: bash ParallelUBM.sh FeatureList UBMFileName CodebookSize UbmDumpDirectory SeedValue"
	exit
fi
FEATLISTAPP=$1
MODELFILE=$2
CODEBOOKSIZE=$3
ubmDumpDir=$4
seedValue=$5
INITLIST=temp.initlist
INITLISTSIZE=50

echo $seedValue

echo $PWD

# make sure the binaries exist
INITBIN=library/ubm-gmm/bin/Init
VQMapper=library/ubm-gmm/bin/VQIter
VQReducer=library/ubm-gmm/bin/VQComb
GMMMapper=library/ubm-gmm/bin/GMMIter
GMMReducer=library/ubm-gmm/bin/GMMComb
INITOUTFILE=vfinal

# number of VQ and GMM iterations
numVQ=15
numGMM=4

# doInit : unset only restarting from a particular GMM iteration
doInit=1

# unset when skipping VQ steps
runVQ=1
vqStartIter=1

# runGMM : unset when gmm step is not necessary
runGMM=1
gmmStartIter=1

# initialize vq model
#10007
#907
if [ $doInit -eq 1 ]; then
    head -$INITLISTSIZE $FEATLISTAPP > $INITLIST
    $INITBIN -i $INITLIST -s $seedValue -l -d -k $CODEBOOKSIZE -o $INITOUTFILE
fi

if [ $runVQ -eq 1 ]; then
    for i in `seq $vqStartIter $numVQ`; do
        # parallelize vqiteration
        rm $ubmDumpDir/vq$i.*
        less $FEATLISTAPP | parallel  -j30 --colsep ' ' "$VQMapper -i {1} -d -k $CODEBOOKSIZE -b vfinal -o $ubmDumpDir/vq$i.{1/} > /dev/null"
        # create list
        ls $ubmDumpDir/vq$i.* > vqlist
        $VQReducer -i vqlist -o vfinal
        cp vfinal $ubmDumpDir/vfinal.$i
        rm $ubmDumpDir/vq$i.*
    done
fi

rm vqlist

if [ $runGMM -eq 1 ]; then
    for i in `seq $gmmStartIter $numGMM`; do
        parallel  -j30 -a $FEATLISTAPP --colsep ' ' $GMMMapper -i {1} -d -k $CODEBOOKSIZE -b vfinal -o $ubmDumpDir/gmm$i.{1/}
        ls $ubmDumpDir/gmm$i.* > gmmlist
        $GMMReducer -i gmmlist -o gfinal
        cp gfinal $ubmDumpDir/gfinal.$i
        rm $ubmDumpDir/gmm$i.*
    done
fi

rm gmmlist
rm $MODELFILE
cp vfinal  $MODELFILE
echo ${MODELFILE}_py
python2 library/ubm_gmm_scripts/ConvGMM.py gfinal  ${MODELFILE}_py

function out = stg(m,LC,RC);
% SYNTAX: out = stg(m,LC,RC);
%   compute short time gaussianization over defined interval
%   m is the feature matrix
%   LC,RC set to 150 => over 301 frames

if nargin == 1
  RC=150;
  LC=150;
elseif nargin == 2
  RC=150;
else
   error('Wrong number of input argument');
end;

if RC <= 1 || LC <= 1
   error('Wrong interval for short time gaussianization');
end


x=linspace(0,1,LC+RC+3);
y=erfinv(x*2-1)*sqrt(2);
py=zeros(1,LC+RC+1);
py=y(2:end-1);

%m=readhtk('NDEG_B.fea');

[wid,len]=size(m);
out=zeros(size(m));

for FEA=1:wid
for act=1:len
 s=act-LC;
 if (s<1) s=1; end
 e=act+RC;
 if (e>len) e=len; end

 x=m(FEA,s:e);
 [X]=sort(x',1,'ascend');
 ret=find (m(FEA,act)==X);
 a=length(ret);
 if a > 1
   ret=ret(round(a/2));
 end
 if ( e-s < LC+RC ) ret=round(ret*(LC+RC+1)/(e-s+1)); end
 out(FEA,act)=py(ret);
end
end


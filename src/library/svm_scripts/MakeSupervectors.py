import sys, os
import numpy as np

if len(sys.argv) != 3:
    print("Arg1: input model Name")
    print("Arg2: output supervector file")
    sys.exit(0)

fileName = sys.argv[1]
outFileName = sys.argv[2]

with open(fileName , 'r') as f:
    lines = f.readlines()[1::2]
    f.close()

means=[]
for line in lines:
    line = line.split('\n')[0]
    line = np.fromstring(line, dtype=np.float32, sep=' ')
    line = line[0::2]
    means.append(line)

means = np.array(means)
np.savetxt(outFileName, means, newline=' ', fmt="%f", footer='\n', comments='')

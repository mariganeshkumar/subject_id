% reads a list of strings in a give file
function list = readlist (fname)
    
    f = fopen (fname);
    i = 0;
    list = [];
    while 1
        a = fscanf (f, '%s', 1);
        if strcmp (a, '') == 1
            break
        end
        i = i + 1;
        list = [list;a];
    end
    fclose (f);

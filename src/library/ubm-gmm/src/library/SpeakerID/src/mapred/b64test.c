#include<stdio.h>
#include<string.h>
#include"b64_mod.h"
#define PTR_CHECK_NULL(PTR, CMD) ( PTR == NULL ? CMD : 0;)
#define IF_PTR_NULL_RET_NULL(PTR) (PTR_CHECK_NULL (PTR, return NULL;))

int 
readFileToByteArray (char *fname, unsigned char **bytearr, int *bytes)  
{
	  FILE *ip;
		int size, bytesRead = 0;		
		char *arr;
		ip = fopen (fname, "rb");
//		IF_PTR_NULL_RET_NULL(ip);

		fseek (ip, 0L, SEEK_END);
		size = ftell (ip);
	  fclose (ip);
	  ip = fopen (fname, "rb");	
		arr = (unsigned char *) calloc (size, sizeof (unsigned char));
		printf ("size allocated is %d\n", size);
    while (!feof(ip))
		  arr[bytesRead++] = (unsigned char) fgetc (ip);
		fclose (ip);
		*bytearr = arr;
		*bytes = size;
		return 0;
	}	 

int
writeByteArrayToFile (unsigned char *arr, char *fname, int bytes)
{
			FILE *op;
			int bytesWritten = 0;

			op = fopen (fname, "w");

			while (++bytesWritten <= bytes)			
							fputc (arr[bytesWritten-1],op);

			fclose (op);			
}

main (int argc, char *argv[])
  {
  	int		i,jk;
		FILE	*ip, *op;
		char	*ipname, *opname,c;
		unsigned char *bytearr, *codedarr, *newbytearr;
		int bytes, codebytes;

		ipname = argv[1];
		opname = argv[2];

		readFileToByteArray (ipname, &bytearr, &bytes);
		codedarr = (unsigned char *) calloc (2 * bytes, sizeof (char));
		ip = fmemopen (bytearr, bytes, "rb");		
		op = fmemopen (codedarr, 2 * bytes, "wb");
		encode (ip, op, -1);
		fclose (ip);
		codebytes = ftell (op);
		fclose (op);
		printf ("Number of bytes is %d\n", codebytes);
	  writeByteArrayToFile (codedarr, opname, codebytes);
		ip = fmemopen (codedarr, codebytes, "rb");
		newbytearr = (unsigned char *) calloc (bytes, sizeof (char));
		op = fmemopen (newbytearr, bytes, "wb");
		decode (ip,op);
		fclose (ip);
		fclose (op);
		writeByteArrayToFile(newbytearr, "tempfile", bytes);										
/*		free (codedarr);		
		free (newbytearr);
		free (bytearr);*/
  }

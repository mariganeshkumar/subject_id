# compile this file using the following command
# gcc -shared -pthread -fPIC -fwrapv -O2 -Wall -fno-strict-aliasing -o GMM.so  \
# -I /usr/include/python2.7  GMM.c 
#
# Some problems faced during compilation: the module should be named GMM.so
# otherwise importing the module under a different name would give an
# error unless the shared library is created using that name with appropriate
# flags (see tldp.org for compilation techniques)
#
# Problems faced during module usage: import it with a name you prefer
# otherwise the module is imported under the name GMM and this needs to
# be prefixed for orphan functions from here
#   Donot cdef member functions of a class. it doesnot show up!

import math
import numpy as np
cimport numpy as np
import scipy as sp
import heapq

# CLASS DEFINITION BEGINS 


cdef class GMM:
  cdef int nmix
  cdef int dim
  cdef np.ndarray mmeans
  cdef np.ndarray mvars
  cdef np.ndarray mwts
  def __cinit__ (self,int nmix,int dim):
    self.nmix = nmix
    self.dim = dim
    self.mmeans = np.zeros ((nmix, dim))
    self.mvars = np.zeros ((nmix,dim))  #assume diagonal cov
    self.mwts = np.zeros ((nmix,1))
  
  def GetMeans (self):
    return self.mmeans
  def GetVars (self):
    return self.mvars
  def GetWts (self):
    return self.mwts

  def GetClosestMixture (self, np.ndarray fvec):
    return self.GetTopCMixtures (fvec, 1)

  def GetTopCMixtures (self, np.ndarray fvec, int cval):
    nvec, dim= np.shape (fvec)
    nmix = self.nmix
    logvars = np.reshape ([math.log(val) for val in np.reshape (self.mvars, (nmix*dim,1))],(nmix,dim))

    logwts = np.reshape ([math.log (val) for val in self.mwts], (nmix,1))
    if dim != self.dim:
      return None
    indices = np.zeros ((nvec,cval))
    k = cval - 1
    for i in range (0,nvec):
      vec = fvec[i,:]
      distvec = [((-0.5 * (mahdist (vec,self.mmeans[n,:], self.mvars[n,:])) * sum (logvars[n,:]) * logwts[n]),n) 
        for n in range(0,self.nmix)]
      
      #distvec_sorted = sorted (distvec, reverse = True)
      # a heap sort for top k selection
      distvec_sorted = []
      for j in range (0,cval):
        heapq.heappush (distvec_sorted,distvec[j])
      for j in range (cval,nmix):
        if distvec_sorted[0][0][0] > distvec[j][0][0]:
          heapq.heappop (distvec_sorted)
          heapq.heappush (distvec_sorted, distvec[j])
        
      indices[i] = [ distvec_sorted[j][1] for j in range(0,cval) ]
    
    return indices

# END OF CLASS DEFINITION 

# UTILITY METHOD - Reads a GMM from a file

def ReadGMM (fname):
  f = open (fname, 'r')
  rl = f.readlines()
  nmix = len(rl)/2
  if nmix == 0:
    return None
  dim = len (rl[1].strip().split()) / 2
  cdef GMM gmm = GMM (nmix,dim)

  for i in range(0,nmix):
    gmm.mwts[i,0] = float(rl[2*i].strip())
    temparr = rl[2*i+1].strip().split()
    gmm.mmeans[i,] = temparr[0:2*dim:2]
    gmm.mvars[i,] = temparr[1:2*dim:2]
  f.close()
  return gmm    

# METHOD DEFINITION ENDS 


############# UTILITY METHOD ########################

def ConvGMMToSV (gmm):  
  return gmm.mmeans.reshape (gmm.nmix*gmm.dim,1)
################### METHOD DEFINITION ENDS #########

############# UTILITY METHOD ########################

cdef mahdist (np.ndarray fvec, np.ndarray mmeans, np.ndarray mvars):  
  dist =  (fvec -  mmeans)
  return np.sum ([ 
               dist[d] * dist[d] / mvars[d] 
               for d in range (0,len (fvec))
              ]
             )

################### METHOD DEFINITION ENDS #########


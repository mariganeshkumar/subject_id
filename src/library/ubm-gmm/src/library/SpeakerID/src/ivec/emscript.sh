# run t-matrix estimation based on the em algorithm

# steps to run
. emscript_config

# ubm and other parameters 
M=`wc -l $ubm | cut -d ' ' -f1`
((M=M/2))
D=`sed -n '2p' $ubm | wc -w | cut -d ' ' -f1`
((D=D/2))

username=`whoami`

mkdir $bkupfolder

tmatrixfull=$dumpfolder/$tmatrixprefix

if [ $initflag -eq 1 ]; then
  $matlab -nojvm -nodesktop -nosplash -r "tmatinit ('$tmatrixfull.0', '$ubm', $M,$D,$R,1)"
fi

# iteration () is a function that encapsulates one entire
# EM iteration. first, ivectors are estimated. estimation
# involves computing i-vectors. side-effects of this
# estimation involve L matrix. this is also stored in the
# same folder.
#
# input : one argument is sufficient and necessary - iteration number
#
# 

iteration () {
    curriterno=$1
    ((baseno=curriterno-1))

    # check if required files exist
    oldtmatfile=$tmatrixfull.$baseno
    newtmatfile=$tmatrixfull.$curriterno
    amatname=$dumpfolder/amat.$curriterno

    # compute i-vectors with given T-matrix (in parallel)
    if [ $skipinitivec -eq 0 ]; then
        bash metaivecest.sh $fostatlist $wxtable $lxtable $lxtable \
                            $oldtmatfile \
                            $hostmachines $wrkdirlist
    else 
        skipinitivec=0
    fi

    # accumulate stats before recomputing T-matrix  
    # compute A matrices for each each mixture    

    if [ $skipinitavec -eq 0 ]; then
       bash computeA_mapred.sh $amatname $fostatlist $lxtable $M $D $R $hostmachines $wrkdirlist
    else
      skipinitavec=0
    fi

    # compute C matrix 
    cmatname=$dumpfolder/Cmat.$curriterno

    if [ $skipinitcvec -eq 0 ]; then
      bash computeC_mapred.sh $cmatname $fostatlist $wxtable $M $D $R $hostmachines $wrkdirlist
    else 
      skipinitcvec=0
    fi
    
    # recompute T-matrix
    echo 1| parallel --dry-run matlab -nojvm -nodesktop -nosplash -r '"recomputeT ('\'$newtmatfile\', \'$cmatname\', \'$amatname\', $M, $D, $R, {}')"'
    echo 1| parallel matlab -nojvm -nodesktop -nosplash -r '"recomputeT ('\'$newtmatfile\', \'$cmatname\', \'$amatname\', $M, $D, $R, {}')"'
#   remove the previously used matrix after backing it up
    cp $oldtmatfile $bkupfolder/
    rm $oldtmatfile 

#   backup amat estimates and delete    
    rm $dumpfolder/amat.$curriterno.* $amatname

#   backup current estimate of tmatrix
    cp $newtmatfile $bkupfolder/
}

if [ $iterflag -ne 1 ]; then
  exit
fi

for i in `seq $iterno $numiter`;do
    iteration $i
done

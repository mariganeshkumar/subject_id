/*-------------------------------------------------------------------------
 *  TestUtteranceGMM.c - Tests a given utterance against speaker models 
 *                       Slight modifications have been made to 
 *                       ensure that the features are read from files
 *                       rather than computing it here.
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *          Srikanth M R (mrsrikanth86@gmail.com) [slight mods and refactoring]
 *
 *
 -------------------------------------------------------------------------*/
 

#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "BatchProcessWaveform.h"
#include "SphereInterface.h"
#include "math.h"
#include "GMM.h"
#include "CommonFunctions.h"



void Usage() {
   printf (" Usage : TestUtteranceGMM modelFile ");
   printf ("testFile numSpeakers test/verify(1/1)\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k;
  int                   featLength;
  unsigned long         numVectors;
  int                   verify;
  float                 *Distortion;
  float                 thresholdScale;
  if (argc != 5) {
    Usage();
    printf ("\n %d\n", argc);
    exit(-1);
  }
  models = argv[1];
  modelFile = fopen(models,"r");
  sscanf(argv[2],"%d",&numSpeakers);
  wavname = argv[3];
  sscanf(argv[4], "%d", &verify);
  
  vfv = (VFV *) ReadVfvFromFile (wavname, &numVectors);
  printf ("read vfv\n");
  featLength = vfv[0]->numElements;

  speakerModelMeans = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelVars = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelWts = (VFV *) calloc(numSpeakers,sizeof(VFV));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VFV *) calloc (numClusters[i], sizeof(VFV));
    speakerModelVars[i] = (VFV *) calloc (numClusters[i], sizeof(VFV));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeLikelihood(vfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   1.0);
  printf("%s\n",wavname);
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }


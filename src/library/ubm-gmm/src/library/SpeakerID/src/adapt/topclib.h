/*
 * =====================================================================================
 *
 *       Filename:  topclib.h
 *
 *    Description:  header file for topclib.c
 *
 *
 * =====================================================================================
 */

#ifndef __topc_lib__
#define __topc_lib__
#include "CommonFunctions.h"
#include "TestUtteranceGMM.MapAdapted.ver3_2004_d.h"
void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue);
int **GetTopCMixtures (VFV *ubmModelMeans, 
                      VFV *ubmModelVars,
                      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, 
                      int cValue, float probScaleFactor);
#endif

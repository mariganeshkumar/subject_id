function [L w A] = ivecest (T, N, f) 
    R = size (T,2);
    M = length (N);
    MD = length (f) ;
    D = MD / M;
    L = eye (R);
    for m = 1:M
      l = (m-1)*D+1;
      r = m*D;
      L = L + (N(m) * T(l:r,:)' * T(l:r,:));
    end
    L = inv (L);
    w = L * T' * f;
    if nargout >= 3
        A = L + w * w';
    end

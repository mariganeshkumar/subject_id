#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: CorrectOctaveErrors.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	CorrectOctaveErrors.c - Smoothes the pitch trajectory.
 *
 *  Purpose:	Smooth pitch contours for music
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Apr-2012 09:12:15
 *
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
#include <stdio.h>

#define MAXPTS 260
#define MAX_FRAMES 70
static   int             **FormantLocs=NULL;
static   int             *edx=NULL, *edy=NULL;
void Usage() {
  printf("Usage : CorrectOctaveErrors ctrlFile inWaveFile InPitchFile OutPitchFile maxFrequency pitchQuantisation\n");
}
/*****************************************************************************/

  int main(int argc, char **argv) {
    int             i,j;
    int            frameIndex, nfBy2, prevFreqIndex, freqIndex;
    int            fftSize, samplingRate, numFrames, medianOrder, first;
    FILE           *controlFile=NULL, *inPthFile=NULL, *outPthFile=NULL;
    float          timeIndex, pitchValue, pitchQuantisation, maxFrequency;
    float          *pitchTrack=NULL, *smthArray=NULL;
    char           line[200];
    static ASDF    *asdf;

  if ((int)(argc) != 7) {
    Usage();
    exit(-1);
  }

  controlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  GsfOpen(asdf, argv[2]);
  inPthFile = fopen(argv[3],"r");
  outPthFile = fopen(argv[4],"w");
  sscanf(argv[5], "%f", &maxFrequency);
  sscanf(argv[6], "%f", &pitchQuantisation);
  fftSize  = (int) GetIAttribute(asdf, "fftSize");
  medianOrder  = (int) GetIAttribute(asdf, "medianOrder");
  numFrames = (int) GetIAttribute(asdf, "numFrames");
  samplingRate = (int) GetIAttribute(asdf, "samplingRate");
  printf ("fftSize = %d numFrames=%d samplingRate = %d\n", 
	  fftSize, numFrames, samplingRate);
  nfBy2 = (int) ceilf((maxFrequency/pitchQuantisation));
  frameIndex = 0;
  FormantLocs = (int **) calloc(nfBy2, nfBy2*sizeof(int *));
  edx = (int *) calloc(numFrames, numFrames*sizeof(int));
  edy = (int *) calloc(numFrames, numFrames*sizeof(int));
  pitchTrack = (float *) calloc(numFrames+1, (numFrames+1)*sizeof(float));
  smthArray = (float *) calloc(numFrames+1, (numFrames+1)*sizeof(float));
  for (i = 0; i < numFrames; i++) 
    FormantLocs[i] = (int *) calloc(numFrames, numFrames*sizeof(int));
  for (i = 0; i < nfBy2; i++)
    for (j = 0; j < numFrames; j++) 
      FormantLocs[i][j] = 0;
  frameIndex = 0;
  while (fgets(line, 200, inPthFile)) {
    sscanf(line, "%f %f", &timeIndex, &pitchTrack[frameIndex+1]);
      frameIndex++;
    }
    numFrames = frameIndex;
    MedianSmoothArray(pitchTrack, frameIndex, medianOrder, smthArray);
    outPthFile = fopen("PitchPicture.txt", "w");
    frameIndex = 0;
    prevFreqIndex = pitchTrack[1];
    first = 0;
    while (frameIndex < numFrames)
    {
      pitchValue = smthArray[frameIndex+1];
      if (pitchValue != 0.0) {
        freqIndex = (int) (pitchValue/pitchQuantisation);
	if ((fabs(freqIndex - prevFreqIndex) >= prevFreqIndex)) 
	  freqIndex = freqIndex-prevFreqIndex; 
	//	if (!first) {
	//  first = 1;
	//  prevFreqIndex = freqIndex;
	  //	}
      	else 
      	  prevFreqIndex = freqIndex;
      //	}
	if (freqIndex < nfBy2)
	  FormantLocs [freqIndex][frameIndex] = 1;
	frameIndex++;
	fprintf(outPthFile, "%d %d\n", frameIndex, freqIndex);
      }
    }
  fclose(outPthFile);   
  return 0;
}


/*
 * =====================================================================================
 *
 *       Filename:  VQComb.c
 *
 *    Description: Takes a bunch of VQ stats from files and combines them into a model 
 *
 *
 * =====================================================================================
 */

#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {
	 int i,j,k, numClusters, dim, oc;
   char *opname, *ipname, *fn;
   FILE *ipfile, *opfile, *vqfile;
   VFV *mv = NULL, *vv = NULL;
   float *wv = NULL, wts;
   float tmpw, tmpm, tmpv;
   // i : input is a list of files
   // o : output file
   while ( (oc = getopt (argc, argv, "i:o:")) != -1)
	 {
     switch (oc) {
       case 'i' :
         ipname = optarg;
         break;
       case 'o' :
         opname = optarg;
         break;
       default:
         printf ("Invalid arguments to file\n");
         exit(1);
     }
   }

   if (ipname == NULL || opname == NULL) {
     printf ("Incomplete arguments to file\n");
     printf ("Usage: VQComb -i iplist -o opname\n");
     exit (2);
   }

   ipfile = fopen (ipname, "r");
   opfile = fopen (opname , "w");
   if (ipfile == NULL) {
     printf ("Unable to open file(s)\n");
     exit (3);
   }
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
     mv = (VFV*) calloc (numClusters, sizeof(VFV));
     vv = (VFV*) calloc (numClusters, sizeof(VFV));
     wv = (float*) calloc (numClusters, sizeof(float));
     if (mv == NULL || vv == NULL || wv == NULL) {
       printf ("Unable to allocate memory for output vars\n");
       exit (4);
     }       
     finc(i,numClusters,1) {     
       wv[i] = 0.0;
       mv[i] = AllocFVector (dim);
       vv[i] = AllocFVector (dim);
       if (mv[i] == NULL || vv[i] == NULL) {
         printf ("Unable to allocate memory for output vars\n");
         exit (4);
       }
       finc(j,dim,1) { 
         mv[i]->array[j] = 0.0;
         vv[i]->array[j] = 0.0;
       }
     }
     break;
   }
   rewind (ipfile);
   while (!feof (ipfile)) {
     fn = (char *) calloc (256, sizeof (char));
     fscanf (ipfile, "%s\n", fn);
     vqfile = fopen (fn, "r");
     if (vqfile == NULL) {
       printf ("Unable to open file %s. Skipping it.\n.", fn);
       continue;
     }
     fscanf (vqfile, "%d %d\n", &dim, &numClusters);
   finc(i,numClusters,1) {     
       fscanf (vqfile, "%e", &tmpw);
       wv[i] += tmpw;
       finc(j,dim,1) {
         fscanf (vqfile, " %e", &tmpm);
         mv[i]->array[j] += tmpm;
       }
       finc(j,dim,1) {
         fscanf (vqfile, " %e", &tmpv);
         vv[i]->array[j] += tmpv;
       }
     }
     fclose (vqfile);
     free (fn);
   }
   
   wts = 0.0;
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       mv[i]->array[j] /= wv[i];
     }
     wts += wv[i];
   }
   finc(i,numClusters,1) {     
     finc(j,dim,1) {
       vv[i]->array[j] = 
         (vv[i]->array[j] / wv[i]) - 
         (mv[i]->array[j] * mv[i]->array[j]);
     }
   }
   finc(i,numClusters,1) {     
     fprintf (opfile, "%e", wv[i]/wts);
     finc(j,dim,1) 
       fprintf (opfile, " %e", mv[i]->array[j]);
     finc(j,dim,1) 
       fprintf (opfile, " %e", vv[i]->array[j]);
     fprintf (opfile, "\n");
   }   
   fclose (ipfile);
   fclose (opfile);
}

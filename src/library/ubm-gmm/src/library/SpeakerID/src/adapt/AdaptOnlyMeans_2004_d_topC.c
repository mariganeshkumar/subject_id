/*
 * =====================================================================================
 *
 *       Filename:  AdaptOnlyMeans_2004_d_topC.c
 *
 *    Description:  this is a variant of 
 *                  AdaptOnlyMeans_2004_fromDumpedFeatures.c where we try
 *                  to use a feature vector to adapt more than one
 *                  feature. In the initial version, which has been,
 *                  used for quite some time, the closest mixture
 *                  was assigned a prob 1.0. While this has been working
 *                  well, it is worthwhile to try to use more than one
 *                  mixture. The trick here is to find top C score mixtures
 *                  and normalize the top C probabilities.
 *          
 *
 *        Created:  Saturday 27 August 2011 12:57:46  IST
 *
 * =====================================================================================
*/

#include "AdaptOnlyMeans_2004_d_topC.h"
#define SWAP(i,j,k) k=i;i=j;j=k;

 VECTOR_OF_F_VECTORS* AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
             VECTOR_OF_F_VECTORS *ubmVars,
             float *ubmWeights,
             unsigned int numClusters,
                   VECTOR_OF_F_VECTORS *vfv,
             unsigned int numVectors,
             unsigned int relevanceFactor,
             float probScaleFactor,
             unsigned int cValue)
   {
   VECTOR_OF_F_VECTORS	*expectation=NULL, *tempMeans;
   float			alpha, beta, normFactorI, maxProb, *eeta,logProb;
   float      *topCScores, tempf;
   
   unsigned int		dimensions, i, j, k, maxProbIdx,  tempMaxProbIdx;
   unsigned int   *topCMixtures, cval1 = cValue -1, k1, tempi;

   VECTOR_OF_F_VECTORS     *adaptedMeans;

   dimensions = ubmMeans[0]->numElements;

   eeta    = (float *) calloc (numClusters, sizeof(float));
   tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof(VECTOR_OF_F_VECTORS));
   for (j = 0; j < numClusters; j++)
     {
     tempMeans[j] = (F_VECTOR *) AllocFVector (dimensions);
     InitFVector (tempMeans[j]);
     eeta[j] = 0.;
     }

   
   topCScores = (float *) calloc (cval1, sizeof (float));
   topCMixtures = (unsigned int *) calloc (cval1, sizeof(unsigned int));
   printf ("Finished initialization\n"); fflush(stdout);
   for (i = 0; i < numVectors; i++)
      {
     for (j = 0; j < numClusters; j++)
       {
       logProb = ComputeProbability (ubmMeans[j], ubmVars[j], ubmWeights[j], 
                    vfv[i], probScaleFactor);
       if (j < cValue)  {
            if (!j) {
                topCScores[j] = logProb;            
                topCMixtures[j] = j;
            }
            else {
                for (k = 0; k < j; k++) {
                    if (topCScores[k] < logProb) {
                        for (k1 = j; k1>k; k1--) {
                            topCScores[k1] = topCScores[k1-1];
                            topCMixtures[k1] = topCMixtures[k1-1];
                        }
                        topCScores[k] = logProb;
                        topCMixtures[k] = j;
                        break;
                    }
                }
                if (k == j) {
                    topCMixtures[k] = j;
                    topCScores[k] = logProb;
                }
            }
            continue;
       }
       if (logProb > topCScores[cval1]) {
         for (k = cval1 - 1; k >= 0; k--) {
            if (logProb < topCScores[k]) 
              break;
            
            topCScores[k+1] = topCScores[k];
            /*  this is the weirdest pice of code I've
             *  written and seen. directly allocate 
             *  topCMixtures[k] to topCMixtures[k+1], it
             *  segfaults. do it indirectly (as given below)
             *  it does ok*/
            tempi = topCMixtures[k];
            topCMixtures[k+1] = tempi;
         }
         topCScores[k+1] = logProb;
         topCMixtures[k+1] = j;
       }
     }

     // normalize topc scores
     tempf = topCScores[0];
     for (k = 1; k < cValue;k++) 
         tempf = LogAdd (tempf, topCScores[k]);
     for (k = 0; k < cValue; k++) {
         topCScores[k] = expf (topCScores[k] - tempf);
         eeta[topCMixtures[k]] += topCScores[k];
     }
     
     for (k = 0; k < dimensions; k++) {
        for (j = 0; j < cValue; j++) {  
          tempi = topCMixtures[j];
          tempMeans[tempi]->array[k] += (topCScores[j] * vfv[i]->array[k]);	
        }
     }
   }
   free (topCScores);
   free (topCMixtures);


   for (j = 0; j < numClusters; j++)
     for (k = 0; k < dimensions; k++) { if (eeta[j] != 0)
       tempMeans[j]->array[k] /= (eeta[j]);}
     
   adaptedMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
   for (j = 0; j < numClusters; j++)
       {
       adaptedMeans[j] = (F_VECTOR *) AllocFVector (dimensions);
       
       alpha = (float) (eeta[j] / (eeta[j] + (float)relevanceFactor));
       for (k = 0; k < dimensions; k++)
         {
     adaptedMeans[j]->array[k] = (alpha * tempMeans[j]->array[k])
             + ((1.0 - alpha) *ubmMeans[j]->array[k]);
         }
     }
   return adaptedMeans;
   }
 main (int argc, char *argv[])
 {
   char *controlFileName, *ubmFileName, *modelFileName,
     *trainFileName, *featureFileName;
   FILE *controlFile, *ubmFile, *modelFile, *ergSpidModelFile;
   VECTOR_OF_F_VECTORS *vfv, *ubmMeans, *ubmVars, *adaptedMeans; 
   F_VECTOR *ubmWeights;
   ASDF *asdf;
   unsigned long numVectors;
   unsigned int ubmSize, featLength, i, j, k, cValue;
   F_VECTOR *meanVector, *varVector;
   int numGaussiansErg = 3, numSpeechVectors;
   int silenceMixtureNumber, speechMixtureNumber, noiseMixtureNumber;
   int VQIter, GMMIter, seed, relevanceFactor;
   float probScaleFactor, ditherMean, varianceNormalize, varianceFloor;

   if (argc != 8)
     {
       printf
   ("Usage : %s controlFile ubmFile ubmSize featureFile outputModelFile ",
    argv[0]);
       printf
         ("relevanceFactor\n");
       exit (0);
     }

   controlFileName = argv[1];
   ubmFileName = argv[2];
   sscanf (argv[3], "%u", &ubmSize);
   featureFileName = argv[4];
   modelFileName = argv[5];
   sscanf (argv[6], "%d", &relevanceFactor);
   sscanf (argv[7], "%u", &cValue);

   controlFile = fopen (controlFileName, "r");
   asdf = (ASDF *) calloc (1, sizeof (ASDF));
   InitializeStandardFrontEnd (asdf, controlFile);
   seed = (int) GetIAttribute (asdf, "seed");
   varianceNormalize = (int) GetIAttribute (asdf, "varianceNormalize");
   ditherMean = (float) GetFAttribute (asdf, "ditherMean");
   probScaleFactor = (float) GetFAttribute (asdf, "probScaleFactor");
   varianceFloor = (float) GetFAttribute (asdf, "varianceFloor");
   Cstore (asdf->fftSize);

   vfv = ReadVfvFromFile (featureFileName, &numVectors);
   featLength = vfv[0]->numElements;
   printf ("Read FeatureFile\n");
   printf("UBM size is %d\n", ubmSize);
   ubmMeans =
     (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof (VECTOR_OF_F_VECTORS));
   ubmVars =
     (VECTOR_OF_F_VECTORS *) calloc (ubmSize, sizeof (VECTOR_OF_F_VECTORS));

   for (i = 0; i < ubmSize; i++)
     {
       ubmMeans[i] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
       ubmVars[i] = (F_VECTOR *) AllocFVector (vfv[0]->numElements);
     }

   ubmWeights = (F_VECTOR *) AllocFVector (ubmSize);
   printf ("In main: Allocated memory for UBM\n");
   ubmFile = fopen (ubmFileName, "r");
   for (i = 0; i < ubmSize; i++)
     {
       fscanf (ubmFile, "%f\n", &ubmWeights->array[i]);
       for (j = 0; j < featLength; j++)
   {
     fscanf (ubmFile, " %f %f", &ubmMeans[i]->array[j],
       &ubmVars[i]->array[j]);
   }
       fscanf (ubmFile, "\n");
     }
   fclose (ubmFile);

   adaptedMeans =
     (VECTOR_OF_F_VECTORS *) AdaptOnlyMeans (ubmMeans, ubmVars,
               ubmWeights->array, ubmSize,
               vfv, numVectors, 
               (unsigned int) relevanceFactor,
               asdf->probScaleFactor,cValue);
   modelFile = fopen (modelFileName, "w");

   for (i = 0; i < ubmSize; i++)
     {
       fprintf (modelFile, "%e\n", ubmWeights->array[i]);
       for (j = 0; j < featLength; j++)
   {
     fprintf (modelFile, " %e %e", adaptedMeans[i]->array[j],
        ubmVars[i]->array[j]);
   }
       fprintf (modelFile, "\n");
     }
   fclose (modelFile);

   for (i = 0; i < ubmSize; i++)
     {
       free (ubmMeans[i]);
       free (ubmVars[i]);
     }
   free (ubmWeights);

 }				// end main()


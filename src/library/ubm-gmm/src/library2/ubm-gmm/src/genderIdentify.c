/*
    This file identifies the gender information for the list of input speech
    files.

    Copyright (C) 2002-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Hema A Murthy <hema@cse.iitm.ac.in>,
    Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID.

    SpeakerID is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "CommonFunctions.h"

float  *ComputeTopCLikelihood (VFV *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VFV **speakerMeans, 
			   VFV **speakerVars, 
			   VFV *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;

  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	       if (mixProbValue > LOG_ZERO )	 
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    Distortion[i] = Distortion[i]/numVectors;
  }
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue;
};

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VFV *maleUBMModelMeans, VFV *maleUBMModelVars,
		      float *maleUBMWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (maleUBMModelMeans[i], maleUBMModelVars[i], maleUBMWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
        mixtureIds[j][k] = bestCMixtures[k].id;
      }
    return mixtureIds;  
  }


int main(int argc, char *argv[])
  {

  FILE                  *featureListFile=NULL,*maleUBMFile=NULL,*femaleUBMFile=NULL, *resultFile=NULL;
  char                  *featureListName =NULL, *maleUBMFileName = NULL, *femaleUBMFileName,
				*featureName = NULL,*resultFileName=NULL; 
  char  		tempchar, **featureFileNames, *featureFileName, line[500];
  int                   nUtter;
  VFV   *maleUBMModelMeans, *maleUBMModelVars,*femaleUBMModelMeans, *femaleUBMModelVars;;
  VFV   **featureVectors;
  int                   i, j, k, index, **mixIds1, **mixIds2;
  int                   featLength, numClusters, cValue=10, tempint;
  unsigned int          *numVectors,mixNumber;
  float                 *maleUBMWts,*femaleUBMWts,tempf,mixProbValue, maleScore,femaleScore;

  if (argc != 6) {
	printf (" Usage : %s featureList Male_UBM Female_UBM num_mix resultFile\n",argv[0]);
	exit(-1);
	}
  featureListName = argv[1];
  if((featureListFile = fopen(featureListName,"r")) == NULL) {
	fprintf(stderr,"Feature list file %s cannot be opened\n",featureListName);
        exit(1);
	}

  maleUBMFileName = argv[2];
  femaleUBMFileName = argv[3];
  numClusters=atoi(argv[4]);

  resultFileName = argv[5];
  if((resultFile = fopen(resultFileName,"a+")) == NULL) {
	fprintf(stderr,"Result %s cannot be opened\n",resultFileName);
        exit(1);
	}


 nUtter = 0;
  while (fgets(line,1000,featureListFile)) {
    nUtter++;
    featureFileName = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s\n",featureFileName); 
    if(featureFileName == NULL) {
	fprintf(stderr,"Problem while reading line  %d in file %s\n",nUtter,featureListName);
       	exit(1);
	}
    free (featureFileName);
  }
  rewind(featureListFile);


  printf ("number of utterances = %d\n", nUtter);

  featureFileNames = (char **) calloc(nUtter,sizeof(char *));
  numVectors = (unsigned int *) calloc(nUtter,sizeof(int *));
  featureVectors = (VFV **) calloc(nUtter,sizeof(VFV *));


 i = 0;
  while (fgets(line,1000,featureListFile)) {
    featureFileName = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s\n",featureFileName); 
    featureFileNames[i]=(char *) calloc (100, sizeof(char));
    strcpy(featureFileNames[i], featureFileName);
    featureVectors[i] = ReadVfvFromFile (featureFileName, &numVectors[i]);
    free (featureFileName);
    i++;
  }
  featLength = featureVectors[0][0]->numElements;

  maleUBMFile = fopen (maleUBMFileName, "r");
  femaleUBMFile = fopen (femaleUBMFileName, "r");
  maleUBMModelMeans = (VFV *) calloc (numClusters, sizeof (VFV));
  maleUBMModelVars  = (VFV *) calloc (numClusters, sizeof (VFV));
  maleUBMWts        = (float *) calloc (numClusters, sizeof (float));
  femaleUBMModelMeans = (VFV *) calloc (numClusters, sizeof (VFV));
  femaleUBMModelVars  = (VFV *) calloc (numClusters, sizeof (VFV));
  femaleUBMWts        = (float *) calloc (numClusters, sizeof (float));

  for (i = 0; i < numClusters; i++)
    {
      fscanf (maleUBMFile, "%e\n", &maleUBMWts[i]);
      maleUBMModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      maleUBMModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (maleUBMFile," %e %e",&maleUBMModelMeans[i]->array[j],&maleUBMModelVars[i]->array[j]);
	}
      fscanf (maleUBMFile,"\n");
    }
  fclose (maleUBMFile);

  for (i = 0; i < numClusters; i++)
    {
      fscanf (femaleUBMFile, "%e\n", &femaleUBMWts[i]);

      femaleUBMModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      femaleUBMModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (femaleUBMFile," %e %e",&femaleUBMModelMeans[i]->array[j],&femaleUBMModelVars[i]->array[j]);
	}
      fscanf (femaleUBMFile,"\n");
    }
  fclose (femaleUBMFile);

    for(i=0;i<nUtter;i++) {
	printProgress((i+1)/(float)(nUtter));

	maleScore=0;
	mixIds1 = GetTopCMixtures (maleUBMModelMeans, maleUBMModelVars, maleUBMWts, numClusters, featureVectors[i], numVectors[i], cValue, 1.0); 
	for (j = 0; j < numVectors[i]; j++) {
        	for (k = 0; k < cValue; k++) {
			mixNumber = mixIds1[j][k]-1;
		         if (!k)
			         mixProbValue = ComputeProbability(maleUBMModelMeans[mixNumber],
					maleUBMModelVars[mixNumber], maleUBMWts[mixNumber],featureVectors[i][j],
					1.0);
		         else
			         mixProbValue = LogAdd (mixProbValue,
                        	 	ComputeProbability(maleUBMModelMeans[mixNumber],
	                                maleUBMModelVars[mixNumber], maleUBMWts[mixNumber],featureVectors[i][j],
        	                        1.0));
          	}
               if (mixProbValue > LOG_ZERO )
	       maleScore += mixProbValue;
    	}
	maleScore/=numVectors[i];

	mixIds2 = GetTopCMixtures (femaleUBMModelMeans, femaleUBMModelVars, femaleUBMWts, numClusters, featureVectors[i], numVectors[i], cValue, 1.0); 
	femaleScore=0;
	for (j = 0; j < numVectors[i]; j++) {
        	for (k = 0; k < cValue; k++) {
			mixNumber = mixIds2[j][k]-1;
		         if (!k)
			         mixProbValue = ComputeProbability(femaleUBMModelMeans[mixNumber],
					femaleUBMModelVars[mixNumber], femaleUBMWts[mixNumber],featureVectors[i][j],
					1.0);
		         else
			         mixProbValue = LogAdd (mixProbValue,
                        	 	ComputeProbability(femaleUBMModelMeans[mixNumber],
	                                femaleUBMModelVars[mixNumber], femaleUBMWts[mixNumber],featureVectors[i][j],
        	                        1.0));
          	}
               if (mixProbValue > LOG_ZERO )
	       femaleScore += mixProbValue;
    	}
	femaleScore/=numVectors[i];
	if(maleScore>femaleScore)
		fprintf(resultFile,"%f %f %s tagged to male\n",maleScore,featureFileNames[i]);
	else
		fprintf(resultFile,"%s tagged to female\n",femaleScore,featureFileNames[i]);
   }


  fclose(resultFile);
  for (i=0;i<nUtter;i++) 
    free(featureVectors[i]);
  return(0);
  }

import math
import getopt
import sys
import numpy as np
import scipy as sp
from tables import *
#from GMM import *
import GMM as GMM
from Utility import *

optlist, args = getopt.getopt (sys.argv[1:], "M:D:g:o:R:V:x:l")

M, D, ipfname, utt = -1, -1, "", ""
ubmfname, opfname, idfilename = "", "", ""
ipIsList = 0

for arg in optlist:
  option = arg[0]
  if option == "-M":
    M = int (arg[1])
  elif option == "-D":
    D = int (arg[1])
  elif option == "-g":  
    ubmfname = arg[1]
  elif option == "-o":
    opfname = arg[1]
  elif option == "-R":
    R = int (arg[1])
  elif option == "-V":
    vmatfname = arg[1]
  elif option == "-x":
    xvecfname = arg[1]
  elif option == "-l":
    ipIsList = 1
  else:
    print "Incorrect option. Quitting...\n"
    quit()

# compute Vx
# => read V matrix
# => read x vector
# => compute dot prod

ipf = openFile (vmatfname, mode = "r")
tab = ipf.root.vmatrix
ncols = (np.shape (((tab.read (start = 0, stop = 1))[0])['colvec']))[0] # woosh!
Vmat = np.zeros ((ncols,R))
i = -1
for svec in tab.iterrows ():
  i += 1
  Vmat[:,i] = svec['colvec']
ipf.close ()

x = np.zeros ((R,1))
ipf = open (xvecfname, "r")
x = [ float(x_1) for x_1 in ipf.readline ().strip ().split ()]
ipf.close ()
Vx = np.dot (Vmat, x)

if ipIsList == 1:
  ipf = open (ubmfname, "r")
  opf = open (opfname, "r")
  gmms = [ GMM.ReadGMM (line.strip ()) for line in ipf.readlines ()]
  opList = [ fname.strip () for fname in opf.readlines ()]
  ipf.close ()
  opf.close ()
else:
  gmms = [ GMM.ReadGMM (ubmfname) ]
  opList = [ opfname ]

i = -1
newmeans = np.zeros ((M,D))
for gmm in gmms:
  i += 1
  mmeans, mvars, mwts = gmm.GetMeans (), gmm.GetVars (), gmm.GetWts ()
  opfname = opList[i]
  opf = open (opfname, "w")  
  for m in range(0,M):
    opf.write("%e\n" % mwts[m])
    newmeans[m] = mmeans[m,:] + Vx[m*D:(m+1)*D]
    for d in range (0,D):
      opf.write (" %e %e" % (newmeans[m,d],mvars[m,d]))
    opf.write ("\n")
  opf.close ()

function [result] = classify_using_ubm_gmm(config,allData)

train_data = allData.train_data;
test_data = allData.test_data;
num_subjects=length(train_data);

models_dir =  [allData.modelSaveDir,'/',config.ubm_model_dir];
system(['rm -rf ', config.tmp_dir]);
mkdir(config.ubm_data_dir);
mkdir(config.ubm_temp_dir);
mkdir([config.ubm_val_data_dir,'/result']);
mkdir([config.ubm_test_data_dir,'/result']);

data_dir=[config.ubm_data_dir,'/'];
ubm_data=allData.ubm_data;
filenames = cell([1, num_subjects]);
subject_data = cell([1, num_subjects]);
clear allData.ubm_data;
if (~config.test_only)
    system(['rm -rf ', models_dir]);
    mkdir(models_dir);
    disp('Preparing UBM Data');
    parfor i=1:length(ubm_data)
        disp(['Subject : ', num2str(i)]);
        subject_data{i}=[];
        for j=1:length(ubm_data{i})
            for k=1:length(ubm_data{i}{j})
                recording_data=ubm_data{i}{j}{k};
                recording_data=permute(recording_data,[2,3,1]);
                reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
                subject_data{i}=[subject_data{i} reshaped_recoding_data];
            end
        end
        filenames{i}=SplitAndWriteData(transpose(subject_data{i}),data_dir,num2str(i),30000)
    end
    combined_ubm_data =[];
    for i=1:length(ubm_data)
        combined_ubm_data = [combined_ubm_data subject_data{i}];
    end
    rng(1331);
    rp = randperm(size(combined_ubm_data,2));
    combined_ubm_data = combined_ubm_data(:,rp);

    ubm_filenames=SplitAndWriteData(combined_ubm_data',data_dir,'Shuffled_UBM_Data',2000);
    
    scriptFileID = fopen([config.ubm_data_dir  '/scriptFile.scp'],'w');
    for j =1:length(ubm_filenames)
        fprintf(scriptFileID,'%s \n',ubm_filenames{j});
    end
    fclose(scriptFileID);
    clear ubm_data allData.ubm_data  ubm_cell
    system(['sh library/ubm_gmm_scripts/SupervisedGMM.sh ',...
        config.ubm_data_dir,'/scriptFile.scp ' ...
        models_dir,'/UBM_GMM.txt ',...
        num2str(config.mixtures),' ',...
        config.ubm_temp_dir,' ',num2str(1331)]);
    
    
    disp(['Adapting Models']);
    %subjectGMMs=cell([1 num_subjects]);
    parfor i=1:length(train_data)
        t=cputime;
        subject_data=[];
        for j=1:length(train_data{i})
            for k=1:length(train_data{i}{j})
                recording_data=train_data{i}{j}{k};
                recording_data=permute(recording_data,[2,3,1]);
                reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
                subject_data=[subject_data, reshaped_recoding_data];
            end
        end
        [dim, no_of_frames] = size(subject_data);
        subject_filename=strcat(config.ubm_data_dir,'/',num2str(i),'.lfcc');
        dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
        dlmwrite(subject_filename,subject_data','-append','delimiter',' ');
        disp(['Wrote subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
    end
    scriptFileID = fopen([config.ubm_data_dir  '/adaptationScript.sh'],'w');
    listFileID = fopen([models_dir  '/modelsScriptFile.scp'],'w');
    for i=1:length(train_data)
        subject_filename=strcat(config.ubm_data_dir,'/',num2str(i),'.lfcc');
        fprintf(scriptFileID, ['library/ubm-gmm/bin/OldAdaptGMM ',...
            models_dir,'/UBM_GMM.txt_py ',...
            num2str(config.mixtures),' ',...
            subject_filename,' ',...
            models_dir,'/',num2str(i),'.model ',...
            num2str(16), '\n']);
        fprintf(listFileID,'%s %d\n',[models_dir,'/',num2str(i),'.model'],config.mixtures);
        
    end
    system(['less ', config.ubm_data_dir,'/adaptationScript.sh | parallel -v -j12'])
    fclose(scriptFileID);
    fclose(listFileID);
else
    if exist(models_dir,'dir')~=7
        disp('trained models not found')
        msgID = 'Models:NotFound';
        msg = 'Unable to find trained models.';
        baseException = MException(msgID,msg);
        throw(baseException);
    end
end
num_subjects = length(train_data);
clear train_data allData.train_data
disp(['Testing Models']);
test_filenames = cell([1, num_subjects]);
val_filenames = cell([1, num_subjects]);

val_per = config.val_per;

parfor i=1:length(test_data)
    t=cputime;
    val_ind=1;
    test_ind=1;
    test_filenames{i}={};
    val_filenames{i}={};
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            [dim, no_of_frames]=size(reshaped_recoding_data);
            if k <= length(test_data{i}{j}) * val_per
                subject_filename=strcat(config.ubm_val_data_dir,'/',num2str(i),'_',num2str(val_ind),'.lfcc');
                val_filenames{i}{val_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                val_ind=val_ind+1;
            else
                subject_filename=strcat(config.ubm_test_data_dir,'/',num2str(i),'_',num2str(test_ind),'.lfcc');
                test_filenames{i}{test_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                test_ind=test_ind+1;
            end
        end
    end
    disp(['Tested subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end

scriptFileID = fopen([config.ubm_val_data_dir  '/valScriptFile.scp'],'w');
v_filenames={};
val_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(val_filenames{i})
        fprintf(scriptFileID,'%s \n',val_filenames{i}{j});
        v_filenames{ind}=val_filenames{i}{j};
        val_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(scriptFileID);

scriptFileID = fopen([config.ubm_val_data_dir  '/valTestScript.sh'],'w');
for i=1:100:length(val_gt_label)
    endtest=i+100-1;
    if endtest>length(val_gt_label)
        endtest=length(val_gt_label);
    end
    command=sprintf(['library/ubm-gmm/bin/TestGMM ',...
        '--resultFolder %s --start %d ',...
        '--end %d %s %d %s %s %d'],....
        [config.ubm_val_data_dir,'/result/'],...
        i,endtest,...
        [models_dir '/modelsScriptFile.scp'],...
        num_subjects,...
        [config.ubm_val_data_dir  '/valScriptFile.scp'],...
        [models_dir  '/UBM_GMM.txt_py'],10);
    fprintf(scriptFileID,command);
    fprintf(scriptFileID,'\n');
end
fclose(scriptFileID);

system(['less ',config.ubm_val_data_dir,'/valTestScript.sh | parallel -v -j12']);
val_predicted_label=[];
val_score=zeros([length(val_gt_label), num_subjects]);
norm_val_score=zeros([length(val_gt_label), num_subjects]);
disp(size(val_score))
for i=1:length(val_gt_label)
    file_ID=fopen([config.ubm_val_data_dir,'/result/', num2str(i),'.out'],'r');
    score = fscanf(file_ID,'Score_%d %f \n');
	for j=1:2:length(score)
        val_score(i,uint8(score(j)+1))=score(j+1);
    end
    fclose(file_ID);
end
[~,val_predicted_label] = max(val_score, [], 2);
val_predicted_label = transpose(val_predicted_label);

scriptFileID = fopen([config.ubm_test_data_dir  '/testScriptFile.scp'],'w');
t_filenames={};
test_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(test_filenames{i})
        fprintf(scriptFileID,'%s \n',test_filenames{i}{j});
        t_filenames{ind}=test_filenames{i}{j};
        test_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(scriptFileID);

scriptFileID = fopen([config.ubm_test_data_dir  '/testScript.sh'],'w');
for i=1:100:length(test_gt_label)
    endtest=i+100-1;
    if endtest>length(test_gt_label)
        endtest=length(test_gt_label);
    end
    command=sprintf(['library/ubm-gmm/bin/TestGMM ',...
        '--resultFolder %s --start %d ',...
        '--end %d %s %d %s %s %d'],....
        [config.ubm_test_data_dir,'/result/'],...
        i,endtest,...
        [models_dir '/modelsScriptFile.scp'],...
        num_subjects,...
        [config.ubm_test_data_dir  '/testScriptFile.scp'],...
        [models_dir  '/UBM_GMM.txt_py'],10);
    fprintf(scriptFileID,command);
    fprintf(scriptFileID,'\n');
end
fclose(scriptFileID);

system(['less ',config.ubm_test_data_dir,'/testScript.sh | parallel -v -j12']);
test_predicted_label=[];
test_score=zeros([length(test_gt_label), num_subjects]);
norm_test_score=zeros([length(test_gt_label), num_subjects]);
target_test_scores=[];
for i=1:length(test_gt_label)
    file_ID=fopen([config.ubm_test_data_dir,'/result/', num2str(i),'.out'],'r');
        score = fscanf(file_ID,'Score_%d %f \n');
for j=1:2:length(score)
        test_score(i,uint8(score(j)+1))=score(j+1);
    end
    fclose(file_ID);
end

for i = 1: num_subjects
    scores = test_score;
    target_scores=scores(:,i);
    scores(:,i)=[];
    imposters_mean=mean(scores,2);
    imposters_std=std(scores,[],2);
    target_scores=(target_scores-imposters_mean)./imposters_std;
    norm_test_score(:,i)=target_scores;
    
    scores = val_score;
    target_scores=scores(:,i);
    scores(:,i)=[];
    imposters_mean=mean(scores,2);
    imposters_std=std(scores,[],2);
    target_scores=(target_scores-imposters_mean)./imposters_std;
    norm_val_score(:,i)=target_scores;
end
test_target_scores=[];
test_non_target_scores=[];
norm_test_target_scores=[];
norm_test_non_target_scores=[];

val_target_scores=[];
val_non_target_scores=[];
norm_val_target_scores=[];
norm_val_non_target_scores=[];

for i = 1: num_subjects
    scores = test_score(test_gt_label==i,:);
    test_target_scores= [test_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    test_non_target_scores = [test_non_target_scores,reshape(scores,1,[])];
    scores = norm_test_score(test_gt_label==i,:);
    norm_test_target_scores= [norm_test_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    norm_test_non_target_scores = [norm_test_non_target_scores,reshape(scores,1,[])];
    
    scores = val_score(val_gt_label==i,:);
    val_target_scores= [val_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    val_non_target_scores = [val_non_target_scores,reshape(scores,1,[])];
    scores = norm_val_score(val_gt_label==i,:);
    norm_val_target_scores= [norm_val_target_scores,reshape(scores(:,i),1,[])];
    scores(:,i)=[];
    norm_val_non_target_scores = [norm_val_non_target_scores,reshape(scores,1,[])];


end    
[~,test_predicted_label] = max(test_score, [], 2);
test_predicted_label = transpose(test_predicted_label);

result.val_predicted_label = val_predicted_label;
result.val_gt_label = val_gt_label;
result.test_predicted_label = test_predicted_label;
result.test_gt_label = test_gt_label;
result.target_test_scores = test_target_scores;
result.non_target_test_scores = test_non_target_scores;
result.target_val_scores =  val_target_scores;
result.non_target_val_scores = val_non_target_scores;
result.target_test_norm_scores = norm_test_target_scores;
result.non_target_test_norm_scores = norm_test_non_target_scores;
result.target_val_norm_scores = norm_val_target_scores;
result.non_target_val_norm_scores = norm_val_non_target_scores;

end


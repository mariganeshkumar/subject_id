function em_pca(map_prefix, num_proc, newtmat, M, D, R)
    D = M * D;
    no_dims = R;
    Wp1_acc = zeros(D, no_dims);                        % (MD x R)
    Wp2_acc = zeros(no_dims, no_dims);                  % (R  x R)
    sigma_acc = 0.0;
    n_acc = 0;
    for i = 1:num_proc
      fn = strcat (map_prefix, int2str(i-1))      
      load (fn,'-mat');
      Wp1_acc = Wp1_acc + Wp1;
      Wp2_acc = Wp2_acc + Wp2;
      sigma_acc = sigma_acc + sigma2;
      n_acc = n_acc + n;
    end
    
    W = Wp1_acc * inv(Wp2_acc);                             % (MD x R)
    
    sigma2 = sigma_acc / (D*n_acc);
    save(newtmat,'-mat','W','sigma2');

    exit

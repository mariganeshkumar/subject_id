/*-------------------------------------------------------------------------
 *  LPFmtsPitch.c - Estimates Formants and Pitch from Speech File
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Thu 24-May-2007 14:25:13
 *  Last modified:  Thu 08-Dec-2011 22:38:06 by hema
 *  $Id: ModifiedGdelayFmtsPitch.c,v 1.3 2007/05/24 12:06:06 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"
#include "fe/FmtsLibrary.h"
#include "fe/PthLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program extracts formant and pitch data from a given speech
*       using the modified group delay function 
**	Inputs :
*	Speech data file, control file, output files for formant and pitch
*	Output :
*	Formant and Pitch aredata are written to specific files
*	
*******************************************************************************/       
void Usage() {
           printf("Usage : LPFmtsPitch controlFile wavefile LPMag/LPPhase (0/1) outputFmtFile  outputPthFile  startFrame, endFrame \n");
}
int   main (int argc, char *argv[])
{ 
	float           *signal = NULL, *f = NULL;
        float           minFrequency, 
	                maxFrequency,LPPitch; 
	int  	        i,k,fftSize, fftSizeBy2, fftOrder,frameSize,
      	                numFmtsObtd, frameNum, iloc,
	                minPitch, maxPitch,
                 	frameShift, numFormants, numFrames,  
	  startFrame, endFrame, samplingRate, LPOrder, fmtType;
        long            nSamp;
	float 		*LPFormants,*LPFmtCopy;
        ASDF            *asdf;
        FILE            *fmtFile,*pthFile, *controlFile;
/******************************************************************************/
       if (argc != 8) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       LPOrder  = (int) GetIAttribute(asdf, "lpOrder");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       numFormants = (int) GetIAttribute(asdf, "numFormants");
       minPitch = (int) GetIAttribute(asdf, "minPitch");
       maxPitch = (int) GetIAttribute(asdf, "maxPitch");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       minFrequency = (float) GetFAttribute(asdf, "minFrequency");
       maxFrequency = (float) GetFAttribute(asdf, "maxFrequency");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	       frameSize, fftOrder, fftSize, frameShift);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d numFrames=%d\n", frameSize, fftOrder, fftSize, frameShift, numFrames);
       Cstore(fftSize);
       sscanf (argv[3], "%d", &fmtType);
       fmtFile = fopen(argv[4], "w");
       pthFile = fopen(argv[5], "w");
       sscanf(argv[6],"%d", &startFrame);
       sscanf(argv[7],"%d", &endFrame);
       signal = (float *) calloc(fftSize+1, sizeof(float));
       f  = (float *) calloc(frameSize+1, sizeof(float));
       LPFormants  = (float *) calloc(numFormants+1, sizeof(float));
       LPFmtCopy  = (float *) calloc(numFormants+1, sizeof(float));
       if ((startFrame == -1) && (endFrame == -1)) {
         startFrame = 0;
         endFrame = numFrames;
       }
       //       nSamp = startFrame*frameShift;
       iloc = startFrame*frameShift;
       frameNum = startFrame;
       while ((iloc < nSamp) && (frameNum <= endFrame)) {	
	 frameNum++;
	 for (i = 1; i <= frameSize; i++)
	   if ((iloc+i) <= nSamp) 
	     f[i] = asdf->waveform[iloc+i-1];
	   else
	     f[i] = 0.0;
	 for (i = 1; i <=  frameSize; i++){
	   signal[i] = f[i]*HamDw(i,frameSize);
	 }
	 for (i = frameSize+1; i <= fftSize; i++)
	   signal[i] = 0.0;
	 if (fmtType == 0) 
	   LPFormants = (float *) FmtsLpMag(signal, frameSize, frameShift, 
					    fftSize, fftOrder, LPOrder, 
					    LPFormants, &numFmtsObtd);
					     
	 else
	   LPFormants = (float *) FmtsLpPhase(signal, frameSize, frameShift, 
					    fftSize, fftOrder, LPOrder, 
					      LPFormants, &numFmtsObtd);

	 /*         printf("FrameNumber = %d ", frameNum);
	 for(i = 1; i <= numFmtsObtd; i++) 
	   printf("F %d = %f ", i, LPFormants[i]);
	 printf("\n");   
	 fflush(stdout); */
         LPPitch = (float ) PitchLP(signal,frameSize, LPOrder, minPitch, maxPitch);
	 
	 //printf("Pitch = %f\n",LPPitch);
	 k = 0;
	 for (i = 1; i <= numFormants; i++)
	   LPFmtCopy[i] = 0;
	 for (i = 1; i <= numFmtsObtd; i++)       
	   if (LPFormants[i]/fftSize*samplingRate >= minFrequency &&
	       LPFormants[i]/fftSize*samplingRate <= maxFrequency) {   
	     k = k + 1;         
	     LPFmtCopy[k] = LPFormants[i]/fftSize*samplingRate;     
	   }
	 //printf("k = %d\n",k);
         fprintf(fmtFile, "%d ", iloc);
	 for (i = 1; i <= k; i++)
	   if (i <= numFormants)
	     fprintf(fmtFile,"%f ",LPFmtCopy[i]);
	 for (i = k+1; i <= numFormants; i++)
	   fprintf(fmtFile, "%f ",0.0);
	 fprintf(fmtFile,"\n");
	 fprintf(pthFile,"%d %f\n",iloc, LPPitch/samplingRate);
	 for (i = 1; i <= 10; i++)
	   LPFormants[i] = 0;
	 LPPitch = 0;
	 iloc = iloc+frameShift;
       }
       fclose(fmtFile);
       fclose(pthFile);
       return(0);
}














/*-------------------------------------------------------------------------
 * $Log: LPFmtsPitch.c,v $
 * Revision 1.3  2007/05/24 12:06:06  hema
 * Removed unnecessary variables
 * modified to support new version of FmtsModGd, PitchModifiedGd
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of ModifiedGdelayFmtsPitch.c
 -------------------------------------------------------------------------*/

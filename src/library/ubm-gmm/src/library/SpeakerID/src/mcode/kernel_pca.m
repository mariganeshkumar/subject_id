if exist('flag','var')==0
    flag = 0;
end
%N1 = 360;
%N2 = 328;
%N3 = 260;
%N = N1+N2+N3;
N =size(Kt,1); 
AllData = load('../data/pca/all_data.txt');
train_lab=[1:300];
train_lab=ceil(train_lab/100);
val = [];
val1=load('../data/pca/class1_val.txt');
val = [val; val1];
val2=load('../data/pca/class2_val.txt');
val = [val; val2];
val3=load('../data/pca/class3_val.txt');
val = [val; val3];
val_size = size(val,1);
val = val - repmat (mu,val_size,1);
val_lab = [1:150];
val_lab=ceil(val_lab/50);
train_lab=[1:300];
train_lab=ceil(train_lab/100);
test_lab=[ones(224,1);2*ones(260,1);3*ones(206,1)];

test = [];
test1=load('../data/pca/class1_test.txt');
test = [test; test1];
test2=load('../data/pca/class2_test.txt');
test = [test; test2];
test3=load('../data/pca/class3_test.txt');
test = [test; test3];
test_size = size(test,1);
test = test - repmat (mu,test_size,1);

test_lab=[ones(224,1);2*ones(260,1);3*ones(206,1)];
val_lab = [1:150];
val_lab=ceil(val_lab/50);

[EigVal idx]= sort(EigVal,'descend');
EigVect = EigVect(:,idx);
EigVal = EigVal(1:d,1);

EigVect = EigVect(:,1:d);
%plot(real(EigVal),'xr');
%plot(imag(EigVal),'xb');
%hold off;
%EigVal = real(EigVal);
for i = 1:d
    EigVect(:,i) = EigVect(:,i)/((EigVect(:,i)'*EigVect(:,i)*EigVal(i,1))^0.5);
end
AllD1 = zeros(N,N);
for i = 1:N
	 AllD1(i,:) = zeros(1,N);
	 for j=1:d
    AllD1(i,:) = AllD1(i,:) + Kt(i,j) * EigVect(:,j)';
	 end
end
str = '%d';
for i = 1:d-1
    str = strcat(str,sprintf(' %%d'));
end
if flag == 0
    fid = fopen('all_data_new_rep_0.txt','w');
else
    fid = fopen('kpca_p_train1.txt','w');
end

for n=1:N
		fprintf (fid, '%d', train_lab(n));
		for j=1:N
			fprintf (fid, ' %d:%f',j,AllD1(n,j));
		end
		fprintf(fid,'\n');
end
fclose(fid);

if flag == 1
val_K = (AllData * val').^m;
test_K = (AllData * test').^m;
else
	for n=1:val_size
		pt = val(n,:);
		mat = AllData - repmat(pt,N,1);
		val_K(:,n) = sum((mat .* mat)')';
	end
	val_K = exp(-g * val_K);
	for n=1:test_size
		pt = test(n,:);
		mat = AllData - repmat(pt,N,1);
		test_K(:,n) = sum((mat .* mat)')';
	end
	test_K = exp(-g * test_K);
	
end
for n=1:size(val,1)
	val_projection(n,:) = zeros(1,N);
	for i=1:d
		val_projection(n,:) = val_projection(n,:) + val_K(i,n) * EigVect(:,i)';
	end
end
for n=1:test_size
	test_projection(n,:) = zeros(1,N);
	for i=1:d
		test_projection(n,:) = test_projection(n,:) + test_K(i,n) * EigVect(:,i)';
	end
end
fid = fopen('kpca_p_val1.txt','w');
for n=1:size(val,1)
		fprintf (fid, '%d', val_lab(n));
		for j=1:N
			fprintf (fid, ' %d:%f',j,val_projection(n,j));
		end
		fprintf(fid,'\n');
end
fclose(fid);
fid = fopen ('kpca_p_test1.txt','w');
for n=1:test_size
		fprintf (fid, '%d', test_lab(n));
		for j=1:N
			fprintf (fid, ' %d:%f',j,test_projection(n,j));
		end
		fprintf(fid,'\n');
end
fclose(fid);


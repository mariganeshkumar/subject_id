import math
import getopt
import sys
import numpy as np
import scipy as sp
from tables import *
#from GMM import *
import GMM as GMM
from Utility import *


def Usage ():
  print "python ChannelFactorsEst.py options"
  print "-M num_of_mixtures"
  print "-D dimensionality of feature"
  print "-i inputFile"                          # input file is the A matrix
  print "-u UtteranceDump"
  print "-g Ubmfile"
  print "-o Output file"                        # o/p file is the x vector
  print "-n occupation list output file"
  return

# read the commandline arguments

optlist, args = getopt.getopt (sys.argv[1:], "M:D:A:u:g:o:n:R:V:")

M, D, ipfname, utt = -1, -1, "", ""
ubmfname, opfname, idfilename = "", "", ""

for arg in optlist:
  option = arg[0]
  if option == "-M":
    M = int (arg[1])
  elif option == "-D":
    D = int (arg[1])
  elif option == "-A":
    ipfname = arg[1]
  elif option == "-u":
    utt = arg[1]
    fvec = ReadUtterance (utt)
    nvec, dim = np.shape (fvec)
  elif option == "-g":
    gmm = GMM.ReadGMM (arg[1])
  elif option == "-o":
    opfname = arg[1]
  elif option == "-n":
    idfilename = arg[1]
  elif option == "-R":  
    R = int(arg[1])
  elif option == "-V":
    vmatfilename = arg[1]
  else:
    Usage()
    quit()

#read A matrix
ipf = openFile (ipfname, mode = "r")
tab = ipf.root.amatrix
Ainv = np.zeros ((R,R))
r = 0
for row in tab.iterrows () :
    Ainv[:,r] = row['colvec']
ipf.close ()
print "Read A matrix"

idfile = open (idfilename, "r")
indices = [ int(ln.strip().split()[0])  for ln in idfile.readlines ()]
idfile.close ()
print "Read index file"

if len(indices) != nvec:
    print ("Unbalanced feature vectors and indices %d %d"% (len(indices),nvec))
    quit()

ipf = openFile (vmatfilename, mode = "r")
tab = ipf.root.vmatrix
ncols = (np.shape (((tab.read (start = 0, stop = 1))[0])['colvec']))[0] # woosh!
Vmat = np.zeros ((ncols,R))
i = -1
for svec in tab.iterrows ():
  i += 1
  Vmat[:,i] = svec['colvec']

ipf.close ()

x = np.zeros ((R,1))
mixturecoll = np.zeros ((M,dim))
mmeans = gmm.GetMeans ()
mvars = gmm.GetVars ()
for n in range(0,nvec):
    m = int(indices[n])
    mixturecoll[m,:] += [ (mmeans[m,d] - fvec[n,d])/math.sqrt(mvars[m,d]) for d in range(0,dim) ]
    
tempx = np.zeros ((R,1))
tempy = np.zeros((D,1))
for m in range(0,M):    
    vsub = Vmat [m*D:(m+1)*D,:].transpose()
    for d in range(0,D):
        tempy[d,0] = mixturecoll[m,d]
    tempx += np.dot(vsub,tempy)

x = np.dot (Ainv,tempx)        
print np.shape (x)
opFile = open (opfname, "w")
temp = ""
for r in range(0,R):
    temp += " " + str(x[r,0])
opFile.write (temp)
opFile.close ()

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
main(int argc, char **argv) {

  FILE *fpIn, *fpOut;
  char line[300], linecopy[300], *linePrevPtr, *lineNextPtr;
  unsigned int samplingRate, tokenLength, tokenNum, 
    prefixLength, fftSize, timeFrqFlg;
  float timeInSamples, timeInSecs;
  int i; 

  if (argc != 7) {
    printf ("Usage: ComvertSamplesToSecs fileIn, fileOut samplingRate tokenNum timeFrqFlg (0/1) FFTSize (optional if flag is 1) \n");
    exit(-1);
  }
  fpIn = fopen (argv[1], "r");
  fpOut = fopen (argv[2], "w");
  sscanf(argv[3], "%d", &samplingRate);
  sscanf (argv[4], "%d", &tokenNum);
  sscanf (argv[5], "%d", &timeFrqFlg);
  if (atoi(argv[5]) == 1) 
    sscanf(argv[6], "%d", &fftSize);
  while (fgets (linecopy, 200, fpIn) != NULL) {
    line[0]= '\0';
    strcpy(line, linecopy);
    linePrevPtr = &line[0];
    lineNextPtr = (char *) strtok(line, " ");
    tokenLength = strlen(lineNextPtr);
    for (i = 0; i < tokenNum-1; i++) {
      linePrevPtr = lineNextPtr;
      lineNextPtr = (char *) strtok(NULL, " ");
      tokenLength = tokenLength + strlen(lineNextPtr);
    }
    //prefixLength = (int) lineNextPtr - (int) line;
    sscanf (lineNextPtr, "%f", &timeInSamples);
    if (timeFrqFlg == 0)
      timeInSecs = timeInSamples/samplingRate;
    else
      timeInSecs = timeInSamples/fftSize*samplingRate;
    //    for (i = 0; i< prefixLength; i++)
    //      fprintf(fpOut, "%c", line[i]);
    fprintf(fpOut, "%s ", linePrevPtr);
    fprintf (fpOut, "%f %s", timeInSecs, &linecopy[tokenLength+1]);
  } 
  fclose(fpIn);
  fclose(fpOut);
}

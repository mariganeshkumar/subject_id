import math, getopt
import sys
import numpy as np
import scipy as sp
from tables import *
from GMM import *

def Usage ():
  print "python prg.py -l inputlist -o output"
  return

opfname, lfname = "", ""

optlist, args = getopt.getopt (sys.argv[1:],'l:o:')

for arg in optlist:
  if arg[0] == "-o":
    opfname = arg[1]
  elif arg[0] == "-l":
    lfname = arg[1]
  else:
    print "Wrong arguments"
    Usage ()
    quit ()

if opfname == "" or lfname == "":
  Usage ()
  quit()

opf = openFile (opfname, mode="w", title = "Reduced matrix")
M = 1024
D = 44
tot = 1334
class tempVVTClass (IsDescription):
  colno = Int32Col()
  colvec = Float32Col (tot)


chmat = np.zeros((tot,tot))

ipf = open (lfname, "r")
for rl in ipf.readlines():
  # each line is the name of a file that is a matrix in hdf5 format
  hfdr = openFile (rl.strip(), mode="r")
  vvtab = hfdr.root.vvtmatrix
  for row in vvtab.iterrows ():
    rowno = row['colno']    
    chmat[rowno,:] = row['colvec'] / 155 # number of spkrs
  hfdr.close ()

for i in range (0,tot):
  for j in range (i+1,tot):
    chmat[j,i] = chmat[i,j]

############# CLASS DEFINITION BEGINS #############
# Description class for hdf5 files
class svector (IsDescription):
  colvec = Float32Col (tot)
############## END OF CLASS DEFINITION ##############

ntab = opf.createTable ("/", 'vvtmatrix', svector, "SSt matrix")
nrow = ntab.row
for i in range(0,tot):
    nrow['colvec'] = chmat[:,i]
    nrow.append()    
ntab.flush ()
ipf.close ()
opf.close ()

#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "InitAsdf.h"
#include "DspLibrary.h"
#include "stdlib.h"
#include "math.h"
#include "GMM.h"
#include "NewWeightedGMM.h"
#include "VQ_Modified.h"


void NewWeightedGMM(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
		VECTOR_OF_F_VECTORS *mixtureMeans, 
		VECTOR_OF_F_VECTORS *mixtureVars, 
		float *mixtureElemCnt, int numMixtures, 
		int VQIter, int GMMIter, float probScaleFactor,
               int ditherMean, float varianceNormalize, int seed,
		VECTOR_OF_F_VECTORS *negData, int negNumVectors, float distanceThreshold) {
  int                            i,j,k;
  static VECTOR_OF_F_VECTORS     *tempMeans, *tempVars,*tempNegMeans,*tempNegVars;
  static float                   *tempMixtureElemCnt;
  float									*tempMixtureNegElemCnt;
  int                            mixtureNumber;
  int				*mixtureNegNumberCache,*mixtureNumberCache;
  int                            featLength;
  float				*alphaVector;
  float				beta,tempNk,minBeta;
  float 			*maxDistortion, *minDistortion;
  float 			tempDistortion;
  float 			alpha;
  int				neglectedVectorsCnt=0;
  //int                            total;
  //int                            minIndex, maxIndex;
  //int                            minMixtureSize, maxMixtureSize;
        alpha=0.95;
	alpha = (1.0-alpha)/alpha;
	printf("alpha is %f\n",alpha);
  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < numMixtures; i++) { 
    tempMeans[i] = (F_VECTOR *) AllocFVector(featLength);
    tempVars[i] = (F_VECTOR *) AllocFVector(featLength);
  }
  tempMixtureElemCnt = (float *) AllocFloatArray(tempMixtureElemCnt, 
						 numMixtures);
  printf("temp mixtures allocated\n");
  fflush(stdout);
  	InitGMM (vfv, numVectors, mixtureMeans, mixtureVars, numMixtures, seed);
  ComputeVQ(vfv, numVectors, mixtureMeans, mixtureVars,
      mixtureElemCnt, numMixtures, varianceNormalize, 
      ditherMean, VQIter, seed);
	 if (GMMIter == 0)
	 	return;
    
  alphaVector = (float*) calloc (numMixtures,sizeof(float));
  for (i=0;i<numMixtures;i++)
	alphaVector[i] = alpha;
  mixtureNumberCache = (int *) calloc (numVectors,sizeof(int));
  mixtureNegNumberCache = (int *) calloc (negNumVectors, sizeof(int));
    for ( k = 0; k < GMMIter; k++) {
      printf(" GMM iteration number = %d\n", k);
      fflush(stdout);
       maxDistortion = (float*) calloc (numMixtures, sizeof(float));
       minDistortion = (float*) calloc (numMixtures, sizeof(float));
      for (i = 0; i < numMixtures; i++) {
        for (j = 0; j < vfv[0]->numElements; j++) {
	  tempMeans[i]->array[j] = 0;
	  tempVars[i]->array[j] = 0;
	}
	tempMixtureElemCnt[i] = 0;
      }
      for (i = 0; i < numVectors; i++) {
	mixtureNumber = DecideWhichMixture(vfv[i], mixtureMeans, 
					   mixtureVars, numMixtures, 
					   mixtureElemCnt, numVectors,
					   probScaleFactor);
	/***********************
 * 		the next few lines tries to add a distortion threshold for data selection
 * 	***************/
	tempDistortion =  ComputeProbability (mixtureMeans[mixtureNumber],mixtureVars[mixtureNumber],
				mixtureElemCnt[mixtureNumber]/numVectors,vfv[i],probScaleFactor);
	if (maxDistortion[mixtureNumber] == 0.0)
		maxDistortion[mixtureNumber] = tempDistortion;
	else if (maxDistortion[mixtureNumber] < tempDistortion)
		maxDistortion[mixtureNumber] = tempDistortion;

	if (minDistortion[mixtureNumber] == 0.0)
		minDistortion[mixtureNumber] = tempDistortion;
	else if (minDistortion[mixtureNumber] > tempDistortion)
		minDistortion[mixtureNumber] = tempDistortion;
	/******* end-of-distortion-threshold-measure********/
	mixtureNumberCache[i] = mixtureNumber;
	tempMixtureElemCnt[mixtureNumber] = tempMixtureElemCnt[mixtureNumber]+1;
	for (j = 0; j < featLength; j++){
	  tempMeans[mixtureNumber]->array[j] = 
	    tempMeans[mixtureNumber]->array[j] + (vfv[i]->array[j]);
	}
      } 
     for (i=0;i<numMixtures;i++) {
	printf("max dist %d is %f\ and min dist is %f\n",i,maxDistortion[i],minDistortion[i]); 
//	maxDistortion[i] = (maxDistortion[i] + minDistortion[i])/2;
//	maxDistortion[i] = distanceThreshold;		
//	maxDistortion[i] -= 2.0;
     }

		tempMixtureNegElemCnt = (float *) calloc (numMixtures,sizeof(float));
		neglectedVectorsCnt=0;
		for (i=0;i<negNumVectors;i++) {
			mixtureNumber = DecideWhichMixture(negData[i], mixtureMeans,
									mixtureVars, numMixtures,
									mixtureElemCnt, numVectors,
									probScaleFactor);
			tempDistortion = ComputeProbability(mixtureMeans[mixtureNumber],mixtureVars[mixtureNumber],
					 mixtureElemCnt[mixtureNumber]/numVectors, negData[i],probScaleFactor);
			if (tempDistortion >= maxDistortion[mixtureNumber]) {
				tempMixtureNegElemCnt[mixtureNumber] = tempMixtureNegElemCnt[mixtureNumber]+1;
				mixtureNegNumberCache[i] = mixtureNumber;
			}
			else {
				mixtureNegNumberCache[i] = -1;
				neglectedVectorsCnt++;
			}
			
		}
		
                tempNk=0.0;
//		if (neglectedVectorsCnt == negNumVectors)
			minBeta=-1.0;
//		else
//			minBeta=(float)numVectors/(negNumVectors-neglectedVectorsCnt);

		for (i=0;i<numMixtures;i++) {	 
		  if (tempMixtureNegElemCnt[i] != 0.0) {
		  alphaVector[i] = (tempMixtureNegElemCnt[i])/(tempMixtureElemCnt[i]+tempMixtureNegElemCnt[i]);
		  alphaVector[i] = (1.0-alphaVector[i])/alphaVector[i];
		  }
		  else 
			alphaVector[i] = 0.0;
		  beta = (tempMixtureNegElemCnt[i]+numMixtures) / (tempMixtureElemCnt[i]+tempMixtureNegElemCnt[i]);
	          beta = (1.0-beta)/beta;
		  if (beta < 0.0 || tempMixtureElemCnt[i] == 0.0) {
		    beta=0.0;	
		  }			
//		  if (alphaVector[i]!=0.0)		   
//		  beta = (alphaVector[i] + beta)/2;
		  if (beta>0.0) 
			if (minBeta==-1.0 || minBeta > beta)
				minBeta = beta;
			
		  alphaVector[i]=beta;		
	        }
                for (i=0;i<numMixtures;i++)
			tempNk += (minBeta*tempMixtureNegElemCnt[i]);

		for (i=0;i<negNumVectors;i++) {
/***			mixtureNumber = DecideWhichMixture(negData[i], mixtureMeans,
									mixtureVars, numMixtures,
									mixtureElemCnt, negNumVectors,
									probScaleFactor);***/
			mixtureNumber = mixtureNegNumberCache[i];			
                        if (mixtureNumber==-1 || alphaVector[mixtureNumber] == 0.0)
				continue;
			for (j=0;j<featLength;j++)
			tempMeans[mixtureNumber]->array[j] -= (minBeta*negData[i]->array[j]);
		}
		for (j=0;j<numMixtures;j++) {
			printf("negative %d is %f\t positive  is %f\n",j,tempMixtureNegElemCnt[j],tempMixtureElemCnt[j]);
			fflush(stdout);
		}

      for (i = 0; i < numMixtures; i++) {
	for (j = 0; j < featLength; j++){	  
	  if (tempMixtureElemCnt[i]-tempMixtureNegElemCnt[i] != 0)
	  tempMeans[i]->array[j] = 
	    tempMeans[i]->array[j]/((tempMixtureElemCnt[i])-(minBeta*tempMixtureNegElemCnt[i]));
          else
            tempMeans[i]->array[j] = mixtureMeans[i]->array[j];
	}
	mixtureElemCnt[i] = tempMixtureElemCnt[i];
      }
      
      for (i = 0; i < numVectors; i++) {
/***	mixtureNumber = DecideWhichMixture(vfv[i], mixtureMeans, 
					   mixtureVars, numMixtures, 
					   mixtureElemCnt, numVectors, 
					   probScaleFactor);	***/
	mixtureNumber = mixtureNumberCache[i];
	for (j = 0; j < featLength; j++){
	  tempVars[mixtureNumber]->array[j] = 
	    tempVars[mixtureNumber]->array[j] + 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]);
	}
      }
      for (i = 0; i < negNumVectors; i++) {
/***	mixtureNumber = DecideWhichMixture(negData[i], mixtureMeans, 
					   mixtureVars, numMixtures, 
					   mixtureElemCnt, negNumVectors, 
					   probScaleFactor);***/
	mixtureNumber = mixtureNegNumberCache[i];
	if (mixtureNumber == -1 || alphaVector[mixtureNumber] == 0.0)
		continue;
	for (j = 0; j < featLength; j++){
	  tempVars[mixtureNumber]->array[j] = 
	    tempVars[mixtureNumber]->array[j] -
	    (negData[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	    (negData[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * minBeta;
	}
      }
	printf("tempNk is %f\n",tempNk);
      for (i = 0; i < numMixtures; i++) {		
	  mixtureElemCnt[i] = (tempMixtureElemCnt[i] - (minBeta*tempMixtureNegElemCnt[i])) / (numVectors - (tempNk));
	  mixtureElemCnt[i] = mixtureElemCnt[i] * numVectors;
	}

      for (i = 0; i < numMixtures; i++){
	for (j = 0; j < featLength; j++) {
//	  if ((tempMixtureElemCnt[i] > 1) && (mixtureVars[i]->array[j] != 0.0)){
	    mixtureMeans[i]->array[j] = tempMeans[i]->array[j];
	    if (tempMixtureElemCnt[i] != 0.0 && tempVars[i]->array[j]>0.0 && alphaVector[i]!=0.0)
	    mixtureVars[i]->array[j] = tempVars[i]->array[j]
	      /(tempMixtureElemCnt[i]-minBeta*tempMixtureNegElemCnt[i]);	
	   else if (tempMixtureElemCnt[i] != 0.0 && tempVars[i]->array[j]>0.0 && alphaVector[i]==0.0)
	    mixtureVars[i]->array[j] = tempVars[i]->array[j]
	      /(tempMixtureElemCnt[i]);	
//	  } else {
/*	  if (tempVars[i]->array[j] <= 0.0) 
	  	    mixtureVars[i]->array[j] = 0.00001;
			if (tempVars[i]->array[j] < 0.0)
				printf("less than 0\n");

	  }*/
//	  }
	}
      }
    }
printf ("minBeta is %f\n",minBeta);
free(alphaVector);
free(mixtureNegNumberCache);
free(mixtureNumberCache);
}

import sys
save_dir = sys.argv[1]
test_only = int(sys.argv[2])

# Seed value
# Apparently you may use different seed values at each stage
seed_value= int(sys.argv[3])

# 1. Set `PYTHONHASHSEED` environment variable at a fixed value
import os
os.environ['PYTHONHASHSEED']=str(seed_value)

# 2. Set `python` built-in pseudo-random generator at a fixed value
import random
random.seed(seed_value)

# 3. Set `numpy` pseudo-random generator at a fixed value
import numpy as np
np.random.seed(seed_value)

# 4. Set `tensorflow` pseudo-random generator at a fixed value
import tensorflow as tf
tf.set_random_seed(seed_value)

# 5. Configure a new global `tensorflow` session
from keras import backend as K
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
session_conf.gpu_options.allow_growth = True
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)
import os
import shutil
from x_vector_models import get_x_vector_model

from keras import losses
from keras.utils.np_utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.utils.generic_utils import get_custom_objects
from keras.optimizers import Adam
from keras.metrics import top_k_categorical_accuracy

import scipy.io as sio

import hdf5storage

# fix random seed for reproducibility
from tqdm import tqdm

dataset = hdf5storage.loadmat('tmp/subject_id_dataset_for_keras.mat')


# split into input (X) and output (Y) variables
train_data = dataset['train_data']
train_label = dataset['train_label']
val_data = dataset['val_data']
val_label = dataset['val_label']
test_data = dataset['test_data']
test_label = dataset['test_label']
hiddenLayerConfig = dataset['hidden_layer_size']


train_data = np.moveaxis(train_data, 1, -1) #example * channel * feat * time - >  example * feat * time *  channel
shape = train_data.shape
train_data = np.reshape(train_data,(shape[0],shape[1], shape[2] * shape[3]) ) #example * feat * time * channel - >  example * feat * channel-time
train_data = np.moveaxis(train_data, 1, -1) #example * feat * channel-time -> example * channel-time * feat
no_of_examples = train_label.shape[1]
randperm = np.random.permutation(no_of_examples)
train_label = train_label[:, randperm]
train_data = train_data[randperm,:,:]
train_label = to_categorical(np.squeeze(train_label - 1), num_classes=np.unique(train_label).shape[0])





val_data = np.moveaxis(val_data, 1, -1)
shape = val_data.shape
val_data = np.reshape(val_data,(shape[0],shape[1], shape[2]*shape[3]) )
val_data = np.moveaxis(val_data, 1, -1)
val_label =  to_categorical(np.squeeze(val_label - 1), num_classes=train_label.shape[1])



test_data = np.moveaxis(test_data, 1, -1)
shape = test_data.shape
test_data = np.reshape(test_data,(shape[0],shape[1], shape[2]*shape[3]) )
test_data = np.moveaxis(test_data, 1, -1)
test_label =  to_categorical(np.squeeze(test_label - 1), num_classes=train_label.shape[1])




early_stopping = EarlyStopping(patience=10, verbose=1)
model_checkpoint = ModelCheckpoint("./keras.model", save_best_only=True, verbose=1)
reduce_lr = ReduceLROnPlateau(factor=0.5, patience=3, min_lr=0.00000000001, verbose=1)

def mixed_mse_cross_entropy_loss(y_true, y_pred):
    return 0.8 * losses.categorical_crossentropy(y_true, y_pred) + 0.2 * losses.mean_squared_error(y_true,y_pred)


get_custom_objects().update({"mixed_loss": mixed_mse_cross_entropy_loss})
model = get_x_vector_model(train_data, train_label, hiddenLayerConfig)
adam_opt = Adam(lr=0.001, clipvalue=1)
model.compile(loss=losses.categorical_crossentropy, optimizer=adam_opt,
              metrics=['accuracy', top_k_categorical_accuracy])
model.summary()

if test_only == 0 :
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    os.makedirs(save_dir)
    early_stopping = EarlyStopping(patience=10, verbose=1)
    model_checkpoint = ModelCheckpoint(save_dir+'/keras.model', save_best_only=True, verbose=1)
    reduce_lr = ReduceLROnPlateau(factor=0.5, patience=3, min_lr=0.00000000001, verbose=1)

    #Fit the model
    model.fit(train_data, train_label,validation_data=(val_data, val_label), epochs=3000, batch_size=16, verbose=2,
          callbacks=[early_stopping, model_checkpoint, reduce_lr])
    model.load_weights(save_dir+'/keras.model')
else:
    model.load_weights(save_dir+'/keras.model')

data_score = model.predict(val_data, verbose=0, batch_size=32)
predicted_val_labels = np.argmax(data_score, axis=1) + 1

data_score = model.predict(test_data, verbose=0, batch_size=32)
predicted_labels = np.argmax(data_score, axis=1) + 1


sio.savemat('tmp/predicted_labels.mat',
            {'predicted_labels': predicted_labels, 'predicted_val_labels': predicted_val_labels})
print('done')


// Does GMM training, with and without init from file.

#include "CommonFunctions.h"

void Usage() {
	printf("\n TrainModelsIncremental ctrlFile speakerTablePart codeBookSize ");
   printf("featureName numVQIter numGMMiter outputCodebookFile ergGaussianFileName \
fileInitFlag(0=initRandomVQ, 1=initFrmFile)\n\n"); 
}

int main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*inOutFile=NULL, *ergSpidModelFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *speakerTable =NULL, 
                        *featureName = NULL, *inOutFileName = NULL;
  char                  *string1=NULL, *string2=NULL, *ergGaussianFileName = NULL;
  char						line[500], speakerModel[500];
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  VECTOR_OF_F_VECTORS   *ergMeans, *ergVars;
  float                 *clusterWts, thresholdScale = 0.0;
  float						*ergWts, tempProbabilities[3];
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv, *energyVector, *newVfv;
  int                   i,j,k, numSpeechVectors;
  int                   silenceMixtureNumber, speechMixtureNumber, noiseMixtureNumber;
  unsigned long numVectors;
  int                   VQIter, GMMIter, featLength;
  float                 probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  F_VECTOR					*meanVector, *varVector;
  int                   seed;
  int fileInitFlag;
  // some constants
int numGaussiansErg = 2; // bi-Gaussian energy modelling  
  
  if (argc != 10 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  speakerTable = argv[2];
  string1 = argv[3];
  featureName = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&VQIter);
  string2 = argv[6];
  sscanf(string2,"%d",&GMMIter);
  inOutFileName = argv[7];
  cFile = fopen(cname,"r");
  speakerFile = fopen(speakerTable,"r");
  ergGaussianFileName = argv[8];
  string2 = argv[9];
  sscanf(string2,"%d",&fileInitFlag);
  
  printf("We are here now\n");
  
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  varianceFloor = (float) GetFAttribute (asdf, "varianceFloor");
  Cstore(asdf->fftSize);
  
  energyVector = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
	"frameEnergy", &numVectors, thresholdScale);

  for (j=0;j<numVectors;j++) energyVector[j]->array[0] = log (energyVector[j]->array[0]); 
  MakeZeroMeanUnitVar(energyVector,numVectors);

  meanVector = ComputeMeanVfv (energyVector, numVectors);
  varVector  = ComputeVarVfv  (energyVector, numVectors);

  ergMeans = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
  ergVars  = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
  ergWts = (float *) calloc (numGaussiansErg,sizeof(float));
  for (i=0;i<numGaussiansErg;i++)
    {
	   ergMeans[i] = (F_VECTOR *) AllocFVector (energyVector[0]->numElements);
		ergVars[i]  = (F_VECTOR *) AllocFVector (energyVector[0]->numElements);
	 }

  ergSpidModelFile = fopen (ergGaussianFileName,"r");
  if (ergSpidModelFile == NULL)
    { 
	   printf ("Could not open energy-Spid Models file..\n");
		fflush(stdout);
		exit(1);
	 }
  featLength = energyVector[0]->numElements;
  printf ("numGaussiansErg is %d and featLength is %d\n", numGaussiansErg, featLength); 
  for (j=0; j<numGaussiansErg && !feof(ergSpidModelFile);j++)
    {
	   fscanf (ergSpidModelFile,"%e",&ergWts[j]);
		printf ("%e\n", ergWts[j]);
		for (k=0;k<featLength;k++)
		  {
		    fscanf(ergSpidModelFile," %e %e", &ergMeans[j]->array[k], &ergVars[j]->array[k]);
			 printf (" %e %e\n", ergMeans[j]->array[k],ergVars[j]->array[k]);
		  }
		  fscanf(ergSpidModelFile,"\n");
		  printf("\n");
	 }

	printf("Computing features\n");
		fflush(stdout);

  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf,speakerFile,
  	featureName, &numVectors, thresholdScale);
	
  numSpeechVectors=0;
  newVfv=(VECTOR_OF_F_VECTORS *) calloc (numVectors, sizeof(VECTOR_OF_F_VECTORS));

  silenceMixtureNumber = 0;
  speechMixtureNumber  = 1;

  for (i=0; i<numVectors; i++)
    {
      tempProbabilities[silenceMixtureNumber] = ComputeProbability(ergMeans[silenceMixtureNumber],
																						 ergVars[silenceMixtureNumber],
																						 ergWts[silenceMixtureNumber],
																						 energyVector[i],probScaleFactor);
      tempProbabilities[speechMixtureNumber] = ComputeProbability(ergMeans[speechMixtureNumber],
																						 ergVars[speechMixtureNumber],
																						 ergWts[speechMixtureNumber],
																						 energyVector[i],probScaleFactor);
		if (tempProbabilities[silenceMixtureNumber] < tempProbabilities[speechMixtureNumber])		    		  
			  	 newVfv[numSpeechVectors++] = vfv[i];
		else if (energyVector[i]->array[0] > ergMeans[silenceMixtureNumber]->array[0])
			  	 newVfv[numSpeechVectors++] = vfv[i];		  
	 }
  printf ("numSpeechVectors is %d while numVectors is %d\n", numSpeechVectors, numVectors);
  numVectors = numSpeechVectors;
  
  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));

  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %d numGMM = %d\n",numVectors,GMMIter);

if (fileInitFlag == 0) {
	// do GMM with VQ init (only the first time)
	printf("Compute GMM init with VQ\n");
	ComputeGMM(newVfv, numVectors, clusterMeans, clusterVars,
		clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
		ditherMean, varianceNormalize, varianceFloor, seed);
	} else {
		// do GMM with file init
		printf("Compute GMM init from file\n");
		inOutFile = fopen(inOutFileName,"r");
		for (j = 0; j < numClusters; j++) {
			fscanf(inOutFile,"%f",&clusterWts[j]);
			for (k = 0; k < vfv[0]->numElements; k++) {
				fscanf(inOutFile,"  %f %f",&clusterMeans[j]->array[k],
					&clusterVars[j]->array[k]);
			}
		}
		fclose(inOutFile);
		ComputeGMMwithInit(newVfv, numVectors, clusterMeans, clusterVars,
			clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
			ditherMean, varianceNormalize, varianceFloor, seed);
	}

// write to file
  inOutFile = fopen(inOutFileName,"w");
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(inOutFile,"%e \n", clusterWts[i]);
 //     printf("%e \n", clusterWts[i]/numVectors);
    } else {
      fprintf(inOutFile, "%e\n", 1.0E-6);
 //     printf("%e\n", 1.0E-8);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
//      printf("mean=%e var=%e \n",
//	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(inOutFile," %e %e ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(inOutFile,"\n");
  }
  
  fclose(inOutFile);
//  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);

} // end main()



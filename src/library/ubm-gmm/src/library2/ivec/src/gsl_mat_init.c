/*
    This file initializes the T matrix

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"

void
printUsage() 
{
    printf("./InitTMat ubm M D R tMatOutFile (baseMatrix)\n");
    return;
}

int
main(int argc, char *argv[]) 
{
    FILE *ubmfile = NULL, *opfile = NULL, *ipfile = NULL;
    int M,D,R;
    int nrow, ncol;
    int i,j,k, phase = 0;
    char *ubmname = NULL;
    gsl_matrix *tmat = NULL, *normfactor = NULL;
    const gsl_rng_type * T;
    gsl_rng * r;
    float tempf, tempf1, tempf2;

    if (argc < 6) {
        printUsage();
        exit(1);
    }

    ubmname = argv[1];
    sscanf(argv[2],"%d",&M);
    sscanf(argv[3],"%d",&D);
    sscanf(argv[4],"%d",&R);
    nrow = M * D;
    ncol = R;

    tmat = gsl_matrix_alloc(M*D,R);
    if(tmat == NULL) {
        fprintf(stderr,"Unable to allocate matrix\n");
        exit(2);
    }
    if(argc == 7) {
        ipfile = fopen(argv[6],"r");
        if(gsl_matrix_fscanf(ipfile,tmat) == GSL_EFAILED) {
            fprintf(stderr,"Unable to load base matrix. Exiting\n");
            exit(2);
        }
        fclose(ipfile);
    }
    else {
        fprintf(stderr,"randomly initializing matrix using gsl\n");

        gsl_rng_env_setup();

        T = gsl_rng_default;
        r = gsl_rng_alloc (T);
        gsl_rng_set(r,1331);
        phase = 0;
        for(i = 0; i < M*D; i++) {
            for(k = 0; k < R; k++) {
                if(phase) {
                  gsl_matrix_set(tmat, i, k, 
                                 sqrt(-2 * log(gsl_rng_uniform(r))) * 
                                      sin(2 * M_PI * gsl_rng_uniform(r)));
                }
                else {
                  gsl_matrix_set(tmat, i, k, 
                                 sqrt(-2 * log(gsl_rng_uniform(r))) * 
                                      cos(2 * M_PI * gsl_rng_uniform(r)));
                }
                phase = 1 - phase;
            }
        }
    }

    ubmfile = fopen(ubmname,"r");
    normfactor = gsl_matrix_alloc(M*D,1);
    gsl_matrix_set_zero(normfactor);
    if(normfactor == NULL) {
        fprintf(stderr,"Unable to allocate memory for normfactor\n");
        exit(2);
    }
    for(i=0;i<M;i++) {
        fscanf(ubmfile,"%e\n", &tempf);
        for(j=0;j<D;j++) {
             fscanf(ubmfile," %e %e", &tempf1, &tempf);
             gsl_matrix_set(normfactor,i*D+j,0,(double) sqrt(tempf));
        }        
        fscanf(ubmfile,"\n");
    }
    fclose(ubmfile);

    for(k=0;k<R;k++) 
        for(i=0;i<M*D;i++) 
            gsl_matrix_set(tmat,i,k,
                           gsl_matrix_get(tmat,i,k)/
                           gsl_matrix_get(normfactor,i,0)
                           );
    
    write_matrix(argv[5],tmat);
    gsl_matrix_free(tmat);
    gsl_matrix_free(normfactor);
}

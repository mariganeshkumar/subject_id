function [] = make_data_from_tuh()
subject_cell={'00000255', '00003033', '00005766', '00000509', '00001068', '00000314', '00007780', '00004326', '00001474', '00002398', '00002274', '00005605', '00001937', '00002307', '00005607', '00002786', '00005440', '00003673', '00001927', '00003946', '00000567', '00002940', '00001773', '00003279', '00006819', '00004773', '00000571', '00005436', '00005835', '00000649', '00002123', '00002333', '00003546', '00001642', '00006825', '00006253', '00003461', '00000745', '00000392', '00006071', '00002427', '00001796', '00004087', '00004466', '00003098', '00001414', '00003508', '00005995', '00004667', '00000175', '00001896', '00003062', '00000306', '00002158', '00004584', '00002795', '00001501', '00001813', '00001310', '00004614', '00000678', '00000597', '00000005', '00002344', '00000403', '00002321', '00000087', '00004676', '00000539', '00000783', '00002826', '00004468', '00001050', '00000826', '00005378', '00002507', '00005426', '00003894', '00003466', '00004049', '00005265', '00001593', '00001770', '00003402', '00007889', '00000666', '00000216', '00000992', '00004286', '00000148', '00007541', '00002143', '00005527', '00003468', '00004490', '00002194', '00005874', '00002189', '00001763', '00000592'};
path=genpath('library');
addpath(path);
config = configuration;
tuh_data_dir = config.tuh_data_dir;
tuh_channels = config.tuh_channels;
val_split = config.val_split;
splited_dir = config.splited_dir;
montages = {'02_tcp_le'}; %get_all_sub_dir([tuh_data_dir, '/edf/']);
for m = 1:length(montages)
    sub_dirs =  get_all_sub_dir([tuh_data_dir, '/mat/', montages{m}]);
    parfor d = 1:length(sub_dirs)
        subjects = get_all_sub_dir([tuh_data_dir,...
            '/mat/', montages{m},'/'...
            sub_dirs{d},'/']);
        for s = 1:length(subjects)
            subject = subjects{s};
            
            if sum(strcmp(subject_cell, subject))==0
                continue;
            end
            sessions = get_all_sub_dir([tuh_data_dir, '/mat/', montages{m},'/'...
                sub_dirs{d},'/',...
                subject,'/']);
            for sess = 1:length(sessions)
                mat_files = dir([tuh_data_dir,'/mat/',montages{m},'/'...
                    sub_dirs{d},'/',...
                    subject,'/',...
                    sessions{sess},'/*.mat']);
                for e = 1:length(mat_files)
                    filename = [tuh_data_dir,'/mat/', montages{m},'/'...
                        sub_dirs{d},'/',...
                        subject,'/',...
                        sessions{sess},'/',...
                        mat_files(e).name];
                    split_save_data(filename,tuh_channels,val_split,splited_dir, subject, sessions{sess});
                end
            end
        end
        disp("done");
    end
end
end


function [] = split_save_data(filename,tuh_channels, val_split, splited_dir, subject_id, session_id)
data_struct = load(filename);
data_fields = fieldnames(data_struct);
all_chan_data = data_struct.(data_fields{1});
sampling_rate = 250;
c_id=zeros([length(tuh_channels),1]);
ind=1;
for c = 1:length(tuh_channels)
    try
        c_id(ind) = find(strcmp(data_struct.cleanchanlocs,tuh_channels{c}));
        current_data(ind,:)=all_chan_data(c_id(ind),:);
        ind=ind+1;
    catch
        return;
    end
end
current_data = current_data(:,250*5:end-250*5); %% Sampling rate hardcoded
current_data = current_data - mean(current_data,2);  %DC shift
std_data = std(current_data,[],2);
tot_chunks = floor(size(current_data,2)/(val_split*250)); %%todo: ignore intial and final 5 secs
mkdir([splited_dir,'/', num2str(val_split),'/',subject_id,'/',session_id,'/']);
for i=0:tot_chunks-1
    data = current_data(:,1+i*250*val_split:(i+1)*250*val_split);
    data = data - mean(data,2);
    save([splited_dir,'/', num2str(val_split),'/',subject_id,'/',session_id,'/',num2str(i),'.tmp.mat'],'data','sampling_rate'); %% tmp files will be ignored in sync and git
end

end


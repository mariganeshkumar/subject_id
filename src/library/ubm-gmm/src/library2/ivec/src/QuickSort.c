/*
    This file does Quicksort on N integers

    Copyright (C) 2001-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Hema A Murthy <hema@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "stdio.h"
#include "string.h"

/*-------------------------------------------------------------------------
 *  Swap -- Swaps two int elements
 *    Args:	int, int
 *    Returns:	int
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
int Swap (int *a, int *b) {
  int temp; 
  temp = *a;
  *a = *b;
  *b = temp;
  
}	/*  End of Swap		End of Swap   */

/*-------------------------------------------------------------------------
 *  QuickSort -- Recursive sort using CAR Hoare's Algorithm
 *    Args:	int*, int, int
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/

void QuickSort(int *v, int left, int right) {
  int   i,j,k;
  int x, w;
  i = left;
  j = right;
  x = v[(left+right)/2];
  do {
    while (v[i] < x)
      i++;
    while (x < v[j]) 
      j--;
     if (i <= j) {
       Swap(&v[i],&v[j]);
       i++;
       j--;
     }
 }   while (i < j);

  if (left < j) 
    QuickSort(v,left,j);
  if (i < right)
    QuickSort(v,i,right);
  

}	/*  End of QuickSort		End of QuickSort   */


/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of QuickSort.c
 -------------------------------------------------------------------------*/








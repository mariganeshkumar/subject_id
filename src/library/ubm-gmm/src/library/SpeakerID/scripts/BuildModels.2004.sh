#!/bin/bash

CTRLFILE=../cfiles/fe-ctrl.base
BINARY=../bin/AdaptOnlyMeans_2004_d
TRAINLIST=../flists/2004/spkrList.m
DUMPFOLDER=../feat/2004.m.fcd/
UBMFILE=../ubm/n2003.ubm.m
UBMSIZE=1024
numSpeakers=`wc -l $TRAINLIST | cut -d ' ' -f1`
RFACTOR=16
FEAT=frameCepstrum+frameDeltaCepstrum
TGMM=../models/tgmm

MODELDIR=/home/srikanth/Research/Experiments/STGauss/models/2004.m.fcd/
SPIDMODEL=$MODELDIR/SpidModel
mkdir -p $MODELDIR
rm $SPIDMODEL

for lineNo in `seq 1 $numSpeakers`; do
	line=`head -$lineNo $TRAINLIST | tail -1`  
  wavname=`cat $line`
  wavname=`basename $wavname | cut -d '.' -f1`
  wavname=`echo $DUMPFOLDER/$wavname`
	#Usage : ../bin.pad/AdaptOnlyMeans_2004_fromDumpedFeatures \
	#	 controlFile ubmFile ubmSize featureFile \
	#	 outputModelFile 
	$BINARY $CTRLFILE $UBMFILE $UBMSIZE $wavname $MODELDIR/spid_$lineNo.gmm $RFACTOR
	echo "$BINARY $CTRLFILE $UBMFILE $UBMSIZE $wavname $MODELDIR/spid_$lineNo.gmm $RFACTOR"
  echo $MODELDIR/spid_$lineNo.gmm $UBMSIZE >> $SPIDMODEL
done

./BatchTest2004.sh

/*-------------------------------------------------------------------------
 *  SigInstants.c - Find the significant instants of excitation
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@localhost.localdomain)
 *
 *  Created:        Thu 25-Jan-2001 16:28:13
 *  Last modified:  Thu 25-Jan-2001 16:28:41 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of ZeroCrossings.c
 -------------------------------------------------------------------------*/

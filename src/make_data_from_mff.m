function [] = make_data_from_mff()
    path=genpath('library');
    addpath(path);
    rmpath('eeglab/');
    eeglab_toolbox = dir([matlabroot,'/toolbox/eeglab*']); %remove all other eeglab toolboxes
    for i = 1:length(eeglab_toolbox)
        rmpath([matlabroot,'/toolbox/',eeglab_toolbox(i).name]);
    end
    config = configuration;
    data_dir =  config.data_dir;
    splited_dir = config.splited_dir;
    val_split = config.val_split;
    subject_id_map_file = config.subject_id_map_file;
    experiments = dir(data_dir);
    is_dir = [experiments(:).isdir];
    experiments = {experiments(is_dir).name};
    experiments(ismember(experiments,{'.','..'})) = [];

    if exist(subject_id_map_file, 'file') == 2
        subject_map=readtable(subject_id_map_file);
    else
        subject_name = {'dummy'};
        subject_ind = 0;
        subject_map = table(subject_name,[subject_ind]);
    end
    
    for i = 1:length(experiments)
        recordings = dir([data_dir,'/',experiments{i},'/*.mat']);
        recordings = {recordings(:).name};
        for j = 1:length(recordings)
             subject_split = strsplit(recordings{j},'_');
            subject_name = lower(subject_split{1});
            subject_ind = [];
            subject_ind = subject_map(strcmp(subject_map.subject_name,subject_name),2).subject_ind;
            if isempty(subject_ind)
                subject_name = {subject_name};
                subject_ind = max(subject_map.subject_ind)+1;
                subject_detial = table(subject_name,[subject_ind]);
                subject_map= [subject_map; subject_detial];
                writetable(subject_map,subject_id_map_file)
            end
        end
    end
    
    addpath('eeglab/');
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    parpool(3);
    for i = 1:length(experiments)
        recordings = dir([data_dir,'/',experiments{i},'/*.mff']);
        base_folder= {recordings(:).folder};
        recordings = {recordings(:).name};
        parfor j = 1:length(recordings)
            subject_split = strsplit(recordings{j},'_');
            subject_name = lower(subject_split{1});
            subject_ind = [];
            subject_ind = subject_map(strcmp(subject_map.subject_name,subject_name),2).subject_ind;
            subject_ind = subject_ind(1);
            session_identifier=[subject_split{end-6}];
            session_time = subject_split{end-5};
            
            clean_and_save([base_folder{j},'/',recordings{j}]);
            
            
        end
        disp('done');
        rmpath('eeglab/');
    end
end

function [] = clean_and_save(filename)
    EEG = pop_mffimport(filename, {'code'});
    clean_EEG = clean_rawdata(EEG, 5, [0.25 0.75], 0.8, 4, 5, 0.5);
    clean_EEG = pop_interp(clean_EEG, EEG.chanlocs, 'spherical');
    current_data = clean_EEG.data;
    cleanchanlocs = {clean_EEG.chanlocs.labels};
    save([filename,'.mat'],'current_data','cleanchanlocs');
end
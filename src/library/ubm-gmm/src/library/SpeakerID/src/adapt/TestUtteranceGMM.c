/*-------------------------------------------------------------------------
 *  TestUtteranceGMM.c - Tests a given utterance against speaker models
 *                       by loading features from a file
 -------------------------------------------------------------------------*/
 

#include "CommonFunctions.h"

void Usage() {
   printf (" Usage : TestUtteranceGMM ctrlFile modelFile numSpeakers wavName\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters;
  VFV   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VFV   *vfv;
  int                   i, j, k;
  int                   featLength;
  unsigned int         numVectors;
  int                   verify;
  float                 *Distortion;
  float                 thresholdScale;
  if (argc != 4) {
    Usage();
    exit(-1);
  }
  models = argv[1];
  modelFile = fopen(models,"r");
  sscanf(argv[2],"%d",&numSpeakers);
  wavname = argv[3];
  
  vfv = (VFV *) ReadVfvFromFile (wavname, &numVectors);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelVars = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelWts = (VFV *) calloc(numSpeakers,sizeof(VFV ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));

  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    //speakerModel[strlen(speakerModel)+1] = '\0';
    speakerFile = fopen(speakerModel,"r");
    /*printf("speakerFile = %s\n",speakerModel);
    fflush(stdout); */
    speakerModelMeans[i] = (VFV *) calloc(numClusters[i], 
    sizeof(VFV));
    speakerModelVars[i] = (VFV *) calloc(numClusters[i], 
    sizeof(VFV));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);
      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeLikelihood(vfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   1.0);
  printf("%s\n",wavname);
  for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

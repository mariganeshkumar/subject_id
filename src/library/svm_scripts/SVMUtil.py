import numpy as np
from sklearn.svm import SVC


def ovr_train(data, labels):
    labelSet = np.sort(np.unique(labels))
    models = []
    for label in labelSet:
        model = SVC(kernel='precomputed', probability=True)
        binaryLabels = np.array(labels == label, dtype=np.float32)
        model.fit(data, binaryLabels)
        models.append(model)
    model = {'models': models, 'labelSet': labelSet}
    return model


def ovr_predict(data, model):
    labelSet = model['labelSet']
    labelSetSize = len(labelSet)
    models = model['models']
    scores = []
    for label in range(labelSetSize):
        score = models[label].predict_log_proba(data)
        score = score[:, 1]  # true class score
        scores.append(np.expand_dims(score,axis=1))
    scores = np.concatenate(scores, axis=1)
    print(scores.shape)
    return scores
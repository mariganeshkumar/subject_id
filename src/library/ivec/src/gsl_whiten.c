/*
    This file computes computes whiten matrix using i vectors

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"

int main(int argc, char *argv[])
    {
    FILE *ipfile, *whmatfile = NULL;
    char *whitenname, *wxlistname = NULL;
    flist *wxlist = NULL;
    int R, num_files,i ,j;
    double tempf;
    gsl_matrix *dataMat, *dataMeanSubMat, *ataMat, *evecMat, *evalMat, *tempMat, *whVecMat;
    gsl_vector *eval, *meanVec, *tempVec, *temp1Vec;
    gsl_eigen_symmv_workspace * w;

    if(argc!=4) {
	printf("Usage: gsl_whiten wlist R whiteningMat\n");
	return 0;
    }
    wxlistname=argv[1];
    wxlist = read_list_of_files(wxlistname);
    num_files = wxlist->num_lines;
    sscanf(argv[2], "%d", &R);
    whitenname=argv[3];

    dataMat = gsl_matrix_alloc(num_files, R);
    dataMeanSubMat = gsl_matrix_alloc(num_files,R);
    ataMat = gsl_matrix_alloc(R, R);
    evecMat = gsl_matrix_alloc(R, R);
    gsl_matrix_set_zero(ataMat);
    evalMat = gsl_matrix_alloc(R, R);
    gsl_matrix_set_zero(evalMat);
    tempMat=gsl_matrix_alloc(R, num_files);  
    gsl_matrix_set_zero(tempMat);
    whVecMat=gsl_matrix_alloc(num_files,R);
    gsl_matrix_set_zero(whVecMat);  
    eval = gsl_vector_alloc(R);
    meanVec = gsl_vector_alloc(R);
    gsl_vector_set_zero(meanVec);
    tempVec = gsl_vector_alloc(R);
    temp1Vec = gsl_vector_alloc(num_files);

    if( dataMat == NULL) {
	fprintf(stderr,"Unable to allocate memory\n");
    }
    for(i=0;i<num_files;i++) {
	ipfile=fopen(wxlist->line[i],"r");
	if(ipfile == NULL) {
		fprintf(stderr,"Read failed\n");
		exit(0);
	}
	for(j=0;j<R;j++) {
		fscanf(ipfile,"%lf ", &tempf);
		gsl_matrix_set(dataMat,i,j,tempf);
	}
	fclose(ipfile);
    }
    for(i=0;i<num_files;i++) {
	for(j=0;j<R;j++) {
	    gsl_vector_set(tempVec,j,gsl_matrix_get(dataMat,i,j));
	}
    gsl_vector_add(meanVec,tempVec);
    }
    gsl_vector_scale(meanVec,(1.0/num_files));

    for(i=0;i<num_files;i++) {
	for(j=0;j<R;j++) {
		gsl_matrix_set(dataMeanSubMat,i,j,( gsl_matrix_get(dataMat,i,j) - gsl_vector_get(meanVec,j)));
	}
    }

    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,dataMeanSubMat,dataMeanSubMat,1.0,ataMat);
    gsl_matrix_scale(ataMat,(1.0/num_files));
   
  w = gsl_eigen_symmv_alloc (R);
    gsl_eigen_symmv(ataMat,eval,evecMat,w);
    gsl_eigen_symmv_free (w);
    gsl_eigen_symmv_sort(eval,evecMat,GSL_EIGEN_SORT_ABS_ASC);

    write_matrix(whitenname,evecMat);

    gsl_matrix_free(dataMeanSubMat);
    gsl_matrix_free(ataMat);
    gsl_matrix_free(dataMat);
    gsl_matrix_free(evecMat);
    gsl_matrix_free(evalMat);
    gsl_matrix_free(whVecMat);
    gsl_matrix_free(tempMat);
    gsl_vector_free(tempVec);
    gsl_vector_free(eval);
    gsl_vector_free(meanVec);
    gsl_vector_free(temp1Vec);

    }

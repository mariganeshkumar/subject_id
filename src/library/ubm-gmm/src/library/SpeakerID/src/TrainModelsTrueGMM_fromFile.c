/*-------------------------------------------------------------------------
 *  TrainModelsGMM.c - Trains models for each speaker given a list of files
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:58:37
 *  Last modified:  Wed 07-May-2003 15:52:21 by hema
 *  $Id: TrainModelsGMM.c,v 1.1 2002/04/30 09:34:53 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

/************************************************************************
  Function            : train_models- computes feature vectors from a 
                        list of speech files, generates the codebook
			and writes the codebook to a file.

  Input args          :  ctrl_file speaker_table codebook_size
                         feature_name output_codebook_file 

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-2002
*******************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "SphereInterface.h"
#include "BatchProcessWaveform.h"
#include "VQ_Modified.h"
#include "GMMNew.h"
#include "math.h"

void Usage() {
   printf(" TrainModelsGMM featureFile codeBookSize ");
   printf(" numVQIter numGMMIter outputCodebookFile\n"); 
}



main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i,j;
  int                   numVectors;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize;
  int                   seed = 1331;
  if (argc != 6 ) {
    Usage();
    exit(-1);
  }

  speakerTable = argv[1];
  sscanf(argv[2],"%d",&numClusters);
  sscanf(argv[3],"%d",&VQIter);
  sscanf(argv[4], "%d", &GMMIter);
  string2 = argv[5];
  outputFile = fopen(string2,"w");
  
  printf("We are here now\n");

  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
  varianceNormalize = 1.0;
  probScaleFactor =  1.0;

  vfv = (VECTOR_OF_F_VECTORS *) ReadVfvFromFile(speakerTable,							   
						      &numVectors);
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %d numGMM = %d\n",numVectors,GMMIter);
  ComputeGMM(vfv, numVectors, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             3.0, 0.0, 1E-5, seed);
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] > 0.) {
      fprintf(outputFile,"%e \n", clusterWts[i]);
      printf("%f \n", clusterWts[i]);
    } else {
      fprintf(outputFile, "%e\n", 1.0E-6);
      printf("%f\n", 1.0E-8);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
      printf("mean=%e var=%e \n",
	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(outputFile," %e %e ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  fclose(outputFile);
  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
}


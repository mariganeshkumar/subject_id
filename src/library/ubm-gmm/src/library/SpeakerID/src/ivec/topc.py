import sys
import numpy as np
import getopt
import re
from Utility import *

ipislist = 0
(gmm_file_name, dump_file_name, topc_str) = sys.argv[1:4]

cval = int (topc_str)
(nmix, dim, wts, mu, sigma) = ReadGMM (gmm_file_name)
cval_indices = range(nmix-cval,nmix)
vfv = read_ascii_vfv(dump_file_name)
nvec, dim = vfv.shape
lwts = np.log(wts) - np.sum(np.log(sigma),1) * 0.5 #- np.log(2*np.pi)


for i in range (0, nvec):
    indices = np.argsort(lwts - 
                         0.5 * 
                         np.sum((pow((vfv[i] - mu), 2) / sigma), 1)
                        )[cval_indices]
    indices.tofile(sys.stdout, sep=" ", format="%d")
    sys.stdout.write('\n')

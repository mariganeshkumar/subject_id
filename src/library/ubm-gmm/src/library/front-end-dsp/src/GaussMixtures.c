/****************************************************************************
 *   Function             : A collection of procedures for VQ
 *   Uses                 : DspLibrary.c, init-asdf.c
 *   Author               : Hema A Murthy
 *   Last Updated         : Dec 18 1998
 *   Source               : Rabiner and Juang, Fundamentals of Speech Recogn.
 *   Bugs                 : none known to date
 *****************************************************************************/

#include "fe/front-end-defs.h"
#include "fe/front-end-types.h"
#include "fe/init-asdf.h"
#include "fe/DspLibrary.h"
#include "math.h"

/****************************************************************************
 *   Function             : ComputeDiscriminant - computes euclidean distance
 *                        : distance between two vectors
 *   Input args           : clustVector, fvect : input vectors 
 *   Outputs              : ComputeDiscrimnant - distance      	  
 *****************************************************************************/

float ComputeDiscriminant(F_VECTOR *clusterMeans, 
			  F_VECTOR *clusterVars,
			  F_VECTOR *fvect) {

  int                     i;
  float                   sum = 0, prod = 0;


for (i = 0; i < fvect->numElements; i++) {
  sum = sum + ln((clusterMeans->array[i] - fvect->array[i])*
    (clusterMeans->array[i] - fvect->array[i])/
    (clusterVars->array[i]*clusterVars->array[i]));
} 
 prod = 0;
 for (i = 0; i < fvect->numElements; i++)
   prod = prod*clustVectorVars->array[i];
 sum = 0.5*sum  -0.5*fvect->numElements*ln(2*PI);
 sum = sum - ln(prod);
return(sum);
}

/****************************************************************************
 *   Function             : DecideWhichCluster - determines index of mixture
 *                        : to which a given vector belongs
 *   Input args           : fvect : input vector, clusterMeans, clusterVars, 
 *                        : numClusters
 *   Outputs              : DecideWhichCluster - codebook index      	  
 *****************************************************************************/

int DecideWhichCluster(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *clusterMeans, 
			 VECTOR_OF_VECTORS *clusterVars, int numClusters) {
  int                     i, j;
  float                   tempDesc;
  int                     index;
  float                   Descriminant;


  Descriminant  = ComputeDiscriminant(clusterMeans[0], clusterVars[0], fvect);
  index = 0;
  for (i = 1; i < numClusters; i++) {
    tempDesc = ComputeDiscriminant(clusterMeans[i], clusterVars[i],fvect);
    if (tempDesc < Descriminant) {
      index = i;
      Descriminant = tempDesc;
    }
  }
  return(index);
}

/****************************************************************************
 *   Function             : ComputeVq - compute codebook
 *                        : for the given set of vectors
 *   Input args           : vfv - input vectors,
 *                        : numVectors - number of input vectors
 *                        : numClusters - codebook size 
 *                        : numIterations - number of iterations
 *   Outputs		  : clusterMeans - array of cluster means
 *			  : clusterVars - array of cluster vars
 *			  : clusterElemCnt - number of elements in
 *			    each cluster
 *****************************************************************************/



void ComputeVq(ASDF *asdf, VECTOR_OF_F_VECTORS *vfv, int numVectors, 
		VECTOR_OF_F_VECTORS *clusterMeans, 
		VECTOR_OF_F_VECTORS *clusterVars, 
		float *clusterElemCnt, int numClusters, int iterations) {
 
  int                            i,j,k;
  static VECTOR_OF_F_VECTORS     *tempClusters;
  int                            clusterNumber;
  int                            featLength;
  int                            total;
  int                            minIndex, maxIndex;
  int                            minClusterSize, maxClusterSize;
  float                          meanValue;
  int                            noTimes;



  featLength = vfv[0]->numElements;
  tempClusters = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < numClusters; i++) 
    tempClusters[i] = (F_VECTOR *) AllocFVector(featLength);
  printf("temp clusters allocated\n");
  fflush(stdout);
  minClusterSize = 0;
  noTimes = 0;
  while((minClusterSize < 10) && (noTimes < 5)){
    noTimes++;
    for ( k = 0; k < iterations; k++) {
      printf(" iteration number = %d\n", k);
      fflush(stdout);
      for (i = 0; i < numClusters; i++)
	for (j = 0; j < featLength; j++)
	  if(k != 0)
	    clusterMeans[i]->array[j] = 
	      tempClusters[i]->array[j]/clusterElemCnt[i];    
      for (i = 0; i < numClusters; i++) {
	for (j = 0; j < featLength; j++)
	  tempClusters[i]->array[j] = 0;
	clusterElemCnt[i] = 0;
      }
      for (i = 0; i < numVectors; i++) {
	clusterNumber = DecideWhichCluster(vfv[i], clusterMeans, numClusters);
	clusterElemCnt[clusterNumber] = clusterElemCnt[clusterNumber]+1;
	for (j = 0; j < featLength; j++){
	  tempClusters[clusterNumber]->array[j] = 
	    tempClusters[clusterNumber]->array[j] + vfv[i]->array[j];
	}
      }
    }
    for (i = 0; i < numClusters; i++){
      for (j = 0; j < featLength; j++)
	tempClusters[i]->array[j] = 0;
      clusterElemCnt[i] = 0;
    }
    for (i = 0; i < numVectors; i++) {
      clusterNumber = DecideWhichCluster(vfv[i], clusterMeans, numClusters);
      for (j = 0; j < featLength; j++)
	tempClusters[clusterNumber]->array[j] = 
	  tempClusters[clusterNumber]->array[j] + 
	  (vfv[i]->array[j] - clusterMeans[clusterNumber]->array[j]) * 
	  (vfv[i]->array[j] - clusterMeans[clusterNumber]->array[j]);
      clusterElemCnt[clusterNumber]+=1.0;
    }
    for (i = 0; i < numClusters; i++){
      for (j = 0; j < featLength; j++) {
	tempClusters[i]->array[j] = sqrt(tempClusters[i]->array[j])/clusterElemCnt[i];
	printf("mean %d %d = %f var %d %d = %f\n",i,j, 
	       clusterMeans[i]->array[j], i,j,tempClusters[i]->array[j]);
	fflush(stdout);
      }
      printf("ClusCnt %d = %f\n",i,clusterElemCnt[i]);
    }
    minIndex = Imin0(clusterElemCnt,numClusters);
    maxIndex = Imax0(clusterElemCnt,numClusters);
    printf("minIndex = %d maxIndex = %d\n",minIndex, maxIndex);
    minClusterSize = clusterElemCnt[minIndex];
    maxClusterSize = clusterElemCnt[maxIndex];
    printf("minSize = %d maxSize = %d\n",minClusterSize, maxClusterSize);
    fflush(stdout);
    for (j = 0; j < featLength; j++) {
      meanValue = clusterMeans[maxIndex]->array[j];
      clusterMeans[minIndex]->array[j] = meanValue +
	3*tempClusters[maxIndex]->array[j];
      clusterMeans[maxIndex]->array[j] = meanValue -
	3*tempClusters[maxIndex]->array[j];
    }
  }
  for (i = 0; i < numClusters; i++)
    for (j = 0; j < featLength; j++) 
      clusterVars[i]->array[j] = tempClusters[i]->array[j];

}














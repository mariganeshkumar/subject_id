#include "TestUtteranceGMM.MapAdapted.ver3_2004_d.h"

float  *ComputeTopCLikelihood (VECTOR_OF_F_VECTORS *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	       if (mixProbValue > LOG_ZERO )	 
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    Distortion[i] = Distortion[i]/numVectors;
  }
  return(Distortion);
}


void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }


    return mixtureIds;  
  }

void Usage() {
   printf (" Usage : TestUtteranceGMM modelFile wavName ubm topC\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL, *ubmFile=NULL, *ergSpidModelFile;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, *ubmFileName = NULL,
                        *featureName = NULL, *ergSpidModelFileName;
  char                  *string1=NULL, *string2=NULL;
  char                  *speakerModel,line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue, tempint;
  unsigned int         numVectors;
  int                   verify, numGaussiansErg=3;
  float                 *Distortion, *ubmWts;
  float                 thresholdScale;

  if (argc != 5) {
    Usage();
    exit(-1);
  }
  models = argv[1];
  modelFile = fopen(models,"r");
  wavname = argv[2];
  ubmFileName = argv[3];
  sscanf (argv[4], "%d", &cValue);

  numVectors = 0;
  vfv = ReadVfvFromFile (wavname, &numVectors);
  featLength = vfv[0]->numElements;
  numSpeakers = 0;
  while (fgets(line,500,modelFile)) {
    speakerModel = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %d\n",speakerModel, &tempint);
    free (speakerModel);
    numSpeakers++;
  }
  printf ("numSpeakers = %d\n", numSpeakers);
  rewind(modelFile);

  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  i = 0;
  while (fgets(line,500,modelFile)) {
    speakerModel = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %d\n",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%e",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %e %e",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++; 
    free(speakerModel);
    fclose(speakerFile);
  }

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters[0], sizeof (float));

  for (i = 0; i < numClusters[0]; i++)
    {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);

  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters[0], vfv, numVectors, cValue, 1.0); 
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeTopCLikelihood(vfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   1.0, cValue, mixIds);
  printf("%s\n",wavname);
    for (i = 0; i < numSpeakers; i++)
    printf("Distortion:: %d %f\n", i, Distortion[i]);;
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }






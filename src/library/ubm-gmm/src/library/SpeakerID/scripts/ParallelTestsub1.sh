#!/bin/bash

TESTBINARY=../bin/test2004.d
CBSIZE=1024
MFOLDER=/home/srikanth/Research/Experiments/STGauss/models/2004.m.fcd.allubm
SPIDMODELS=$MFOLDER/SpidModel
CTRLFILE=../cfiles/fe-ctrl.base
FEATURE=frameCepstrum
TESTLIST=../flists/2004/male.test
GROUNDTRUTH=../flists/2004/GroundTruth
RESULTDIR=$MFOLDER/AllResults
SPKRLIST=../flists/2004/spkrList.m
FINALRESULT=$MFOLDER/finalResult
TEMPFILE=temp
TEMPDIR=../temp.1/
UBMFILE=../ubm/n2004.m.ubm.new
UBMSIZE=1024
CVAL=10
dumpbin=../bin/DumpFeat.1
numSpeakers=`wc -l $SPIDMODELS | cut -d ' ' -f1`
TGMM=../models/tgmm

	testName=`basename $1 .sph`
	testPath=$1
  LOOPTEMPFILE=$RESULTDIR/$testName.result
  echo "$dumpbin $CTRLFILE $testPath $FEATURE out.$1 $TGMM 1"
  $dumpbin $CTRLFILE $testPath $FEATURE $TEMPDIR/$testName.temp $TGMM 1 > /dev/null
	echo "$TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers out.$1 0.0 0 $UBMFILE $CVAL $TGMM"
  $TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $TEMPDIR/$testName.temp 0.0 0 $UBMFILE $CVAL $TGMM> $LOOPTEMPFILE 
	hypothesized=`grep speaker $LOOPTEMPFILE | awk '{print $5}'`
	speaker=`head -$hypothesized $SPKRLIST | tail -1`
	speaker=`basename $speaker | cut -d '.' -f1`
	echo "$1 $speaker" >> $FINALRESULT
	
  grep Distortion $RESULTDIR/$testName.result | cut -d ' ' -f4 > $RESULTDIR/$testName.result.dist

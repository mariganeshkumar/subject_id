/*-------------------------------------------------------------------------
 *  LPSyn.c - Program to Synthesis speech using LP Synthesis
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Mon 11-Jun-2007 11:58:25
 *  Last modified:  Wed 21-Dec-2011 11:44:16 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include <sys/types.h>
#include <unistd.h>
#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "malloc.h"
#include "string.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/SphereInterface.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
//#define MALLOC_CHECK_ 1

/*-------------------------------------------------------------------------
 *  ARMAsignal -- Generates an ARMA signal
 *    Args:	residual signal,  ArSignal, numSamples
 *              coefZero, coefPole, orderZ, orderP 
 *    Returns:	returns the signal generated in arSignal 
 *    Bugs:	
 * -------------------------------------------------------------------------*/

	void ARMAsignal(float *residual,float *arSignal,int numSamples,
			float *coefZero,int orderZ, float *coefPole, int orderP) {
        int             i, j;
        float           sum1, sum2;
	printf("OrderZ= %d OrderP= %d\n", orderZ, orderP);
	//      for(i=1; i<=orderP; i++)
	//printf("arSignal %d = %f\n",i, arSignal[i]); 
	for (i = 1; i <= numSamples; i++){
	  arSignal[i+orderP] = residual[i+orderZ]; 
	  sum1 = 0.0;
          if (orderZ != 0)
	    for (j = 1; j <= orderZ; j++)
	      sum1 = sum1 +coefZero[j]*residual[i+orderZ-j];
	  sum2 = 0.0;
	  for (j = 1; j <= orderP; j++)
	    sum2 = sum2 + coefPole[j]*arSignal[i+orderP-j];
	  arSignal[i+orderP] = arSignal[i+orderP]+sum1-sum2;
	  //printf("residual %d=%f arSignal %d= %f\n",i+orderZ,residual[i+orderZ], 
	  // i+orderP, arSignal[i+orderP]); 
	}
	}	/*  End of Armasignal		End of Armasignal   */

void Usage() {
  printf(" LPSyn ctrlFile excitationType outputSpeechFile outputResFile PoleData ZeroData (residualData) || (pitchData gainData) n1 n2 || \n");
  printf("LPSyn ctrlFile excitationType outputSpeechFile outputResFile PoleData (residualData) || (pitchData gainData) n1 n2 \n");
  fflush(stdout);
}

int main(int argc, char *argv[]){

  int         	n1, n2;  
  int           pitch, pitchPulsesPerFrame;
  long           numSamples;
  int		frameSize, numFrames = 0, frameLimit, frameNum;
  int 	        i, j, iloc;
  int           orderP,orderZ;
  float		*coefPole=NULL, *coefZero=NULL;
  float		samplingFrequency, samplingInterval;
  float 	gain;
  short 	*utterance=NULL;
  float 	 *residual=NULL,*arSignal=NULL,
                *arSignalTemp=NULL, *residualTemp=NULL;
  float 	exitn[201], tempext;
  float		rmax, average;
  FILE 	        *fCoefZero=NULL,*fCoefPole=NULL,
                *fPitch=NULL,*fGain=NULL, *controlFile,
                *fResidual=NULL, *fp=NULL,*fpResidual=NULL;
  char          *textFileName=NULL, *riffFileName=NULL;
  ASDF          *asdf;
  char 	        ans = 'i', line[200];


  if (argc < 7) {
    Usage();
    exit(-1);
  } 

  controlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  frameSize = (int) GetIAttribute(asdf, "frameAdvanceSamples");
  samplingFrequency = (int) GetIAttribute(asdf, "samplingRate");
  orderP       = (int) GetIAttribute(asdf, "lpOrder");
  orderZ       = (int) GetIAttribute(asdf, "zeroOrder");
  arSignalTemp = (float *) AllocFloatArray(arSignalTemp,frameSize+orderP+1);
  residualTemp = (float *) AllocFloatArray(residualTemp,frameSize+orderZ+1);
  coefPole     = (float *)  AllocFloatArray(coefPole, orderP+1);
  coefZero     = (float *)  AllocFloatArray(coefZero, orderZ+1);
  samplingInterval = 1.0/samplingFrequency;
  //  strcat (textFileName, argv[3]);
  textFileName = (char *)malloc((strlen(argv[3])+5)*sizeof(char));
  strcat(textFileName, argv[3]);
  riffFileName = (char *)malloc((strlen(argv[3])+5)*sizeof(char));
  strcat (textFileName, ".txt"); 
  strcat(riffFileName, argv[3]);
  strcat (riffFileName, ".wav"); 
  printf("samplingFrequency = %f sampling Interval = %f\n", 
	 samplingFrequency, samplingInterval); 
  sscanf(argv[2], "%c", &ans);
  fp = fopen(textFileName,"w");                   //output speech file 
  fpResidual = fopen(argv[4],"w");             //output residual file


  if (orderP != 0) {
      fCoefPole = fopen(argv[5],"r");
  }
  if (orderZ != 0) {
    if (fCoefPole != NULL) 
      fCoefZero = fopen(argv[6],"r");
    else
      fCoefZero = fopen(argv[5],"r");
  }
  putchar(ans);
  if (fCoefPole != NULL)
    while (fgets (line,200,fCoefPole) != NULL)
      numFrames++;
  else if (fCoefZero != NULL)
    while (fgets (line,200,fCoefZero) != NULL)
      numFrames++;
    numSamples = (numFrames)*frameSize+1;
  printf("numFrames=%d numSamples = %ld\n",numFrames, numSamples);
  //scanf("%*c");
  if (fCoefPole != NULL)
    rewind(fCoefPole);
  else if (fCoefZero != NULL)
    rewind(fCoefZero);
  
  if (ans != 'e') {
    if (fCoefPole != NULL){
      if (fCoefZero != NULL){
	fPitch = fopen(argv[7],"r");
	fGain = fopen(argv[8],"r");
      } else if ((fCoefPole == NULL) || (fCoefZero == NULL)) {
	fPitch = fopen(argv[6],"r");
	fGain = fopen(argv[7],"r");
      }
    }
    frameLimit = frameSize;
    /* Begin Creation of pitch pulse  for each pitch period based on ExcitationType */
    /* Initialise the excitation array.  Assumes that the pitch period can
       be a maximum of 200 samples */
    for (i = 1; i <= 200; i++) 
      exitn[i] = 0;
    if ((ans  ==  'g') || (ans  ==  'r')) {
      if ((fCoefPole != NULL) && (fCoefZero != NULL)) {
	sscanf(argv[6],"%d", &n1);
	sscanf(argv[7],"%d", &n2);
      } else if ((fCoefPole == NULL) || (fCoefZero == NULL)) {
	sscanf(argv[5],"%d", &n1);
	sscanf(argv[6],"%d", &n2);
      }
      for (i = 1; i <= n1; i++) 
	exitn[i] = 0.5 - 0.5*cos(PI*(i-1)/n1);
      for (i = n1+1; i <= n1+n2; i++) 
	exitn[i] = cos(PI*(i-1-n1)/2/n2);
    } else if (ans  ==  'i') 
      exitn[1] = 1;
    else if (ans == 'r') {
      tempext = exitn[1];
      for (i = 2; i <= n1+n2; i++) {
	exitn[i]=exitn[i]-tempext;
	tempext = exitn[i]+tempext;
      }
    }
    
    /* End of Creation of Pitch pulse */
    arSignal = (float *) AllocFloatArray(arSignal, numSamples+frameSize+1+orderP+orderZ);
    residual = (float *) AllocFloatArray(residual, numSamples+frameSize+1);
    iloc = 0;
    frameLimit = frameSize;
    printf("numSamples= %d fLim= %d\n",numSamples, frameLimit);
    while (fgets (line,200,fPitch) != NULL){
      sscanf(line,"%d", &pitch);
      fgets(line, 200, fGain);
      sscanf(line,"%f", &gain);
      if (pitch  !=  0) {
        pitchPulsesPerFrame = frameLimit/pitch;
	if (pitchPulsesPerFrame*pitch != frameLimit) 
          pitchPulsesPerFrame++;
	frameLimit = frameSize - pitchPulsesPerFrame*pitch % frameLimit;
	for (i = 1; i <= pitchPulsesPerFrame; i++){
	  for (j = 1; j <= pitch; j++)
	    residual[j+iloc] = exitn[j]*gain;
	  iloc = iloc+pitch;
	  }
      } else {
	for (i = 1; i <= frameLimit; i++)
	  residual[i+iloc] = rand()/RAND_MAX*gain;
	iloc= iloc + frameLimit;
      }
    }
    fclose(fPitch);
    fclose(fGain);
  } /* End of generation of artificial residual using impulse 
       train/glottal pulse/random noise */
    /* If the option is not i/g/r, read from file */
  else {
    if ((fCoefPole != NULL) && (fCoefZero != NULL))
      fResidual = fopen(argv[7],"r");
    else if ((fCoefPole == NULL ) || (fCoefZero == NULL))
      fResidual = fopen(argv[6],"r");
    iloc = 0;
    while (fgets(line,200,fResidual) != NULL) 
      iloc++;
    numSamples = (iloc+1);
    arSignal = (float *) AllocFloatArray(arSignal, numSamples+1);
    residual = (float *) AllocFloatArray(residual, numSamples+1);
    iloc = 0;
    rewind(fResidual);
    while (fgets(line,200,fResidual) != NULL) {
      iloc++;
      sscanf(line,"%f",&residual[iloc]);
    }
    fclose(fResidual);
  } /* End Read residual from file */
  frameNum = 0;	
  while (frameNum < numFrames) {
    printf("frameNum= %d\n",frameNum);
    if (fCoefPole != NULL) {
      for (i = 1; i <= orderP; i++) {
	fscanf(fCoefPole,"%f", &coefPole[i]);
	//printf("CoefPole [%d] = %f\n", i, coefPole[i]);
      }
      lseek((int) fCoefPole,(long) frameNum,0);
    } else orderP = 0;
    if (fCoefZero != NULL) { 
      for (i = 1; i <= orderZ; i++) {
	fscanf(fCoefZero,"%f",&coefZero[i]);
	printf("CoefZero[%d]= %f\n",i,coefZero[i]);
      } 
      lseek((int) fCoefZero, (long) frameNum, 0);
    } else orderZ = 0;
    if (frameNum == 0) {
      for (i = 1; i <= orderZ; i++)
	residualTemp[i] = 0;
      for (i = 1; i <= orderP; i++)
	arSignalTemp[i] = 0;
    } else {
      for (i = 1; i <= orderZ; i++)
	residualTemp[i] = residualTemp[frameSize+i];
      for (i = 1; i <= orderP; i++)
	arSignalTemp[i] = arSignalTemp[frameSize+i];
    }
    i = 1;
    while ((i <= frameSize) && (i+frameNum*frameSize <= numSamples)) {
      //    for (i = 1; i <= frameSize; i++)
      residualTemp[i+orderZ] = residual[i+frameNum*frameSize];
      i++;
    }
    for (j = i; j <= frameSize; j++)
      residualTemp[i+orderZ] = 0;
    ARMAsignal(residualTemp,arSignalTemp,frameSize,
	       coefZero,orderZ,coefPole,orderP);
    rmax = fabs(arSignalTemp[Imax(arSignalTemp,frameSize)]);
    printf("frameno= %d rmax= %f",frameNum,rmax);
    i = 1;
    while ((i <= frameSize) && (i+frameNum*frameSize <= numSamples)) {
      //    for (i = 1; i <= frameSize; i++)
      arSignal[i+frameNum*frameSize] = arSignalTemp[i+orderP];
      fprintf(fp,"%f\n",arSignalTemp[i+orderP]);
      fprintf(fpResidual,"%f\n",residualTemp[i+orderZ]);
      i++;
    }
    frameNum++;
    printf("frameNum=%d\n",frameNum);
  }
 fclose(fp);
  fclose(fpResidual);
  if(fCoefPole != NULL)
    fclose(fCoefPole);
  if(fCoefZero != NULL)
    fclose(fCoefZero);
  utterance = (short *) malloc ((numSamples+frameSize)*sizeof(short));
  average = 0.0;
  for (i = 1; i <= numSamples; i++)
    average = average + arSignal[i];
  average = average/numSamples;
  for (i = 1; i <= numSamples; i++)
    arSignal[i] =arSignal[i] -average;
  rmax = fabs(arSignal[Imax(arSignal,numSamples)]);
   if (utterance == NULL) {
    printf ("Unable to allocate space for waveform\n");
    exit(-1);
    } 
  for (i = 1; i < numSamples; i++)
  utterance[i-1] = (short) (arSignal[i]/rmax*pow(2,15)); 
  WriteRIFF(riffFileName, utterance, numSamples-1, samplingFrequency, 2); 
  printf("rmax= %f numSamples = %ld\n",rmax, numSamples); 
  free(arSignalTemp);
  free(residualTemp);
  free(residual);
  free(utterance);
  // free(arSignal);
  return(0);
}






/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSyn.c
 -------------------------------------------------------------------------*/

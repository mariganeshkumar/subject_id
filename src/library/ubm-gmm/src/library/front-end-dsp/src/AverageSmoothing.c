/**************************************************************************
 *  $Id: AverageSmoothing.c,v 1.1 2000/04/23 02:36:13 hema Exp hema $
 *  Release $Name:  $
 *
 *  File:	AverageSmoothing.c
 *             
 *  Purpose:	Average Smoothes an input signal and writes to a file
 *              
 *
 *  Author:	Hema A Murthy,BSB 307,8342,9342
 *
 *  Created:    Thu 16-Mar-2000 21:27:58
 *
 *  Last modified:  Tue 1-Nov-2011 20:40:07 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "string.h"
#include "stdlib.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program smoothes a given signal using Average
        Smoothing
*	Inputs :
*	Input Data File
*       Average Order
*	Output :
*       Average Smoothed  data written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : AverageSmoothing inFile avgOrder windowType outFile \n");
}
/*****************************************************************************/

        int main (int argc, char *argv[])
{ 
 	float           *sigIn=NULL, *sigOut=NULL;
	int  	        avgOrder;
	float 		*avgArray=NULL;
        FILE            *inFile=NULL, *outFile=NULL, *temp=NULL;
	int             i,j, numSamples;
        char            str[254], line[200], windowType;

       if (argc != 5) {
         Usage();
         exit(-1);
       }
       str[0] = '\0';
       strcat(str,"wc -l ");
       strcat(str, argv[1]);
       strcat(str," | cut -c1-8 > temp");
       system(str);
       temp = fopen("temp","r");
       fscanf(temp,"%d", &numSamples);
       fclose(temp);
       str[0] = '\0';
       strcat(str,argv[2]);
       sscanf(str,"%d",&avgOrder);
       printf("Number of samples = %d Average Order = %d\n",numSamples, avgOrder);
       sscanf(argv[3], "%c", &windowType);
       sigIn = (float *) AllocFloatArray(sigIn,numSamples+1);
       sigOut = (float *) AllocFloatArray(sigOut,numSamples+1);
       avgArray = (float *) AllocFloatArray(avgArray,avgOrder+1); 
       inFile = fopen(argv[1],"r");
       outFile = fopen(argv[4],"w");
       sscanf(argv[2],"%d",&avgOrder);
       if ((inFile == NULL) || (outFile == NULL)){
	 printf("cannot open files \n");
	 exit(-1);
       }
       i = 0;
       while (fgets(line, 200, inFile) != NULL) {
	 i++;
         sscanf(line,"%f",&sigIn[i]);
       }
       for (i = 1; i <= numSamples; i++) {
	 printf ("i=%d\n",i);
	 if ((i > avgOrder/2) && (i < numSamples-avgOrder/2))
	   for (j = 1; j <= avgOrder; j++)
	     avgArray[j] = sigIn[i-avgOrder/2+j];
	 else if( i <= avgOrder/2){ 
	   for (j = 1; j <= avgOrder/2; j++)
	     avgArray[j] = sigIn[1];
	   for (j = avgOrder/2+1; j <= avgOrder; j++)
	     avgArray[j] = sigIn[i-avgOrder/2+j];
	 } else {
	   for (j = 1; j <= avgOrder/2; j++)
	     avgArray[j] = sigIn[i-avgOrder/2+j];
	   for (j = avgOrder/2+1; j <= avgOrder; j++)
	     avgArray[j] = sigIn[numSamples];
	 }
	 if (windowType != 'R')
	   Window(avgArray, avgOrder, windowType, 'D', 1.0);
	 ComputeAverage (avgArray,avgOrder, &sigOut[i]);
       }
       for (i = 1; i <= numSamples; i++)
       fprintf(outFile,"%f\n",sigOut[i]);
       fclose(inFile);
       fclose(outFile);
       return 0;
}
/**************************************************************************
 * $Log: AverageSmoothing.c,v $
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of AverageSmoothing.c
 **************************************************************************/




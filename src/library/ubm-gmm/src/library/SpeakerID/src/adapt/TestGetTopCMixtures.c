#include "CommonFunctions.h"
#include "TestUtteranceGMM.MapAdapted.ver3_2004_d.h"


void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }
/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  
int **GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }


    return mixtureIds;  
  }


int main(int argc, char *argv[])
{

  FILE                  *ubmFile=NULL,
                        *opFileHnd = NULL;
  char                  *wavname =NULL, *ubmFileName = NULL;
  int                   numClusters;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue;
  unsigned int         numVectors;
  float                 *ubmWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  VECTOR_OF_F_VECTORS   *vfv;

  if (argc < 5) {
    printf ("Usage: %s featureFile ubm numMix cVal [outputFile]\n",argv[0]);
    exit(-1);
  }
  
  wavname = argv[1];
  ubmFileName = argv[2];
  sscanf (argv[3], "%d", &numClusters);
  sscanf (argv[4], "%d", &cValue);
  if (argc == 5) {
    opFileHnd = stdout;
  }
  else {
    opFileHnd = fopen (argv[5], "w");
  }

  numVectors = 0;
  vfv = ReadVfvFromFile (wavname, &numVectors);
  if (vfv == NULL) {
    fprintf (stderr, "Invalid input file %s\n", wavname);
    exit(2);
  }
  featLength = vfv[0]->numElements;
  i = 0;

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters, sizeof (float));

  for (i = 0; i < numClusters; i++)
  {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
      {
        fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],
                                 &ubmModelVars[i]->array[j]);
      }
      fscanf (ubmFile,"\n");
  }
  fclose (ubmFile);

  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters, vfv, numVectors, cValue, 1.0); 
  finc(j,numVectors,1) {
    finc(i,cValue,1) {
      fprintf (opFileHnd, "%d ", mixIds[j][i]-1);
    }
    fprintf (opFileHnd, "\n");
  }
  FreeVfv (ubmModelMeans, numClusters);
  FreeVfv (ubmModelVars, numClusters);
  free (ubmWts);
  if (argc >= 6) {
      fclose (opFileHnd);
  }
}

function scores = TopCGMMScoring(uBMModel,adaptedModels,data,topC)
lhoodNK=zeros([size(data,1) uBMModel.NumComponents]);
for i=1:uBMModel.NumComponents
    lhoodNK(:,i) = uBMModel.ComponentProportion(i) * mvnpdf(data,uBMModel.mu(i,:),uBMModel.Sigma(:,:,i));
end
[topUBMLhood,topCMixtureIDs]=maxk(lhoodNK,topC,2);


topUBMLhood(isnan(topUBMLhood)) = realmin;
topUBMLhood=sum(topUBMLhood,2);
topUBMLhood(topUBMLhood==0) = realmin;


scores=zeros([1,length(adaptedModels)]);

for model =1:length(adaptedModels)
    
    if isempty(adaptedModels{model})
        scores(model)=min(model);
        continue;
    end

    adaptedLhood=zeros([size(data,1) uBMModel.NumComponents]);
    for i=1:uBMModel.NumComponents
        adaptedLhood(:,i) = adaptedModels{model}.ComponentProportion(i) * mvnpdf(data,adaptedModels{model}.mu(i,:),adaptedModels{model}.Sigma(:,:,i));
    end
    %t = cputime;
    %topAdaptedLhood=zeros([size(data,1) topC]);
    %for i =1:size(data,1)  
    %        topAdaptedLhood(i,:) = adaptedLhood(i,topCMixtureIDs(i,:));
    %end
    %e = cputime-t;
    t = cputime;
    topAdaptedLhood=zeros([size(data,1) topC]);
    for i =1:topC
            topAdaptedLhood(:,i) = adaptedLhood(sub2ind(size(adaptedLhood),1:size(data,1), topCMixtureIDs(:,i)'));
    end
    e2 = cputime-t;

    topAdaptedLhood(isnan(topAdaptedLhood)) = realmin;
    topAdaptedLhood= sum(topAdaptedLhood,2);
    topAdaptedLhood(topAdaptedLhood==0) = realmin;


    scores(model) = sum(log(topAdaptedLhood) - log(topUBMLhood));
end


function [ newData, meanData] = PCA( data, reductionCount )
    dim = size(data, 2);
    meanData = mean(data);
    meanData = meanData';
    covData = cov(data);
    [projMat, ~] = eig(double(covData));
    %[projMat, ~] = eigs(double(covData), dim - reductionCount);
    %newData = projMat.' * data.';
    %newData = newData.';
    newData = projMat;
end
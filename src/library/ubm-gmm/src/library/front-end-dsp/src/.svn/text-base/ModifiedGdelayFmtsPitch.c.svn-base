/*-------------------------------------------------------------------------
 *  ModifiedGdelayFmtsPitch.c - Estimates Formants and Pitch from Speech File
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Thu 24-May-2007 14:25:13
 *  Last modified:  Thu 18-Apr-2013 16:42:30 by hema
 *  $Id: ModifiedGdelayFmtsPitch.c,v 1.3 2007/05/24 12:06:06 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"
#include "fe/FmtsLibrary.h"
#include "fe/PthLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program extracts formant and pitch data from a given speech
*       using the modified group delay function 
**	Inputs :
*	Speech data file, control file, output files for formant and pitch
*	Output :
*	Formant and Pitch aredata are written to specific files
*	
*******************************************************************************/       
void Usage() {
           printf("Usage : ModifiedGdelayFormants controlFile wavefile outputFmtFile  outputPthFile startFrame, endFrame\n");
}
int   main (int argc, char *argv[])
{ 
	float           *signal = NULL, *f = NULL;
        float           minFrequency, 
	                maxFrequency,gdelayPitch, 
	                gamma, winScaleFactor, gdPosScale, gdNegScale;
	int  	        i,k,fftSize, fftSizeBy2, fftOrder,frameSize,
	                numFmtObtd, frameNum, iloc, 
	                winLen, gdSmthWinSize, minPitch, maxPitch,
	                medianOrder, frameShift, numFormants, numFrames,  
	                startFrame, endFrame, samplingRate;
        long            nSamp;
	float 		*gdelayFormants,*gdelayFmtCopy;
        ASDF            *asdf;
        FILE            *fmtFile,*pthFile, *controlFile;
/******************************************************************************/
       if (argc != 7) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       winScaleFactor = (int) GetFAttribute(asdf, "winScaleFactor");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       numFormants = (int) GetIAttribute(asdf, "numFormants");
       minPitch = (int) GetIAttribute(asdf, "minPitch");
       maxPitch = (int) GetIAttribute(asdf, "maxPitch");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       winLen = (int) GetIAttribute(asdf, "numCepstrum");
       medianOrder = (int) GetIAttribute(asdf, "medianOrder");
       gdSmthWinSize = (int) GetIAttribute(asdf, "gdSmthWinSize");
       minFrequency = (float) GetFAttribute(asdf, "minFrequency");
       maxFrequency = (float) GetFAttribute(asdf, "maxFrequency");
       gdPosScale = (float) GetFAttribute(asdf, "gdPosScale");
       gdNegScale = (float) GetFAttribute(asdf, "gdNegScale");
       gamma = (float) GetFAttribute(asdf, "gamma");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	       frameSize, fftOrder, fftSize, frameShift);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d numFrames=%d\n", frameSize, fftOrder, fftSize, frameShift, numFrames);
       Cstore(fftSize);
       fmtFile = fopen(argv[3], "w");
       pthFile = fopen(argv[4], "w");
       sscanf(argv[5],"%d", &startFrame);
       sscanf(argv[6],"%d", &endFrame);
       signal = (float *) calloc(fftSize+1, sizeof(float));
       f  = (float *) calloc(frameSize+1, sizeof(float));
       gdelayFormants  = (float *) calloc(numFormants+1, sizeof(float));
       gdelayFmtCopy  = (float *) calloc(numFormants+1, sizeof(float));
       if ((startFrame == -1) && (endFrame == -1)) {
         startFrame = 0;
         endFrame = numFrames;
       }
       iloc = startFrame*frameShift;
       frameNum = startFrame;
       while ((iloc < nSamp) && (frameNum < endFrame)) {	
	 frameNum++;
	 for (i = 1; i <= frameSize; i++)
	   if ((iloc+i) <= nSamp) 
	     f[i] = asdf->waveform[iloc+i-1];
	   else
	     f[i] = 0.0;
	 for (i = 1; i <=  frameSize; i++){
	   signal[i] = f[i]*HamDw(i,frameSize);
	 }
	 for (i = frameSize+1; i <= fftSize; i++)
	   signal[i] = 0.0;
	 gdelayFormants = (float *) FmtsModGd(signal, frameSize,
					      fftSize, fftOrder,
					      winLen, gdSmthWinSize, gamma,
                                              gdPosScale, gdNegScale,
                                              gdelayFormants, 
					      &numFmtObtd);
         printf("FrameNumber = %d ", frameNum);
	 for(i = 1; i <= numFmtObtd; i++) 
	   printf("F %d = %f ", i, gdelayFormants[i]);
	 printf("\n");   
	 fflush(stdout);
	 gdelayPitch = (float ) PitchModifiedGd(signal,frameSize,
						fftSize,fftOrder,
						minPitch, maxPitch, 
						winLen, winScaleFactor, gdSmthWinSize, medianOrder, 
                                                gamma, gdPosScale, gdNegScale);
	 
	 printf("Pitch = %f\n",gdelayPitch);
	 k = 0;
	 for (i = 1; i <= numFormants; i++)
	   gdelayFmtCopy[i] = 0;
	 for (i = 1; i <= numFmtObtd; i++)       
   if (gdelayFormants[i]/fftSize*samplingRate >= minFrequency &&
	       gdelayFormants[i]/fftSize*samplingRate <= maxFrequency) {   
	     k = k + 1;         
	     gdelayFmtCopy[k] = gdelayFormants[i]/fftSize*samplingRate;     
	   }
	 printf("k = %d\n",k);
         fprintf(fmtFile, "%d ", iloc);
	 for (i = 1; i <= k; i++)
	   fprintf(fmtFile,"%f ",gdelayFmtCopy[i]);
	 for (i = k+1; i <= numFormants; i++)
	   fprintf(fmtFile, "%f ",0.0);
	 fprintf(fmtFile,"\n");
	 fprintf(pthFile,"%d %f\n",iloc, gdelayPitch/samplingRate);
	 for (i = 1; i <= 10; i++)
	   gdelayFormants[i] = 0;
	 gdelayPitch = 0;
	 iloc = iloc+frameShift;
       }
       fclose(fmtFile);
       fclose(pthFile);
       return(0);
}

/*-------------------------------------------------------------------------
 * $Log: ModifiedGdelayFmtsPitch.c,v $
 * Revision 1.3  2007/05/24 12:06:06  hema
 * Removed unnecessary variables
 * modified to support new version of FmtsModGd, PitchModifiedGd
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of ModifiedGdelayFmtsPitch.c
 -------------------------------------------------------------------------*/

/*
 * Purpose: takes the vfv dumps that are originally ascii files
 * and converts codes them into bytes. That way, the size of these
 * files are much lower.
 *
 * The format of i/pi file is as follows: the first line contains
 * dim numvfv
 *
 * Subsequent numvfv lines are feature vectors of dimensionality
 * dim. They are readable numbers separated with spaces
 *
 * The format of o/p is as follows: the first 4 bytes is a number
 * representing dim, the next 4 bytes represent numVfv (all in little
 * endian). Next dim*4bytes constitue 1 feature vect. Likewise, there
 * are numVfv such arrays
 *
 */

#include<stdio.h>
#include<string.h>
#include "unistd.h"
#include<stdlib.h>

#define READBYTESTOCHAR(a,b,c,d) for(a = 0; a < b; a++) c[a] = fgetc(d); 
#define finc(i,j) for(i=0;i<j;i++)

void
Usage () {
    printf ("shrinkvfv -i inputfile -o outputfile\n");
    printf ("-h: to print this help\n");
    return;
}
int
main (int argc, char *argv) {
  float f,f2;
  int dim, nvec, i, j, k, bytes_read;
  FILE *ipf, *opf;
  char oc, *c, *ipfname, *opfname;
  unsigned char *d;

  size_t sizeofint = sizeof (int),
         sizeoffloat = sizeof (float);
    
  while ((oc = getopt (argc, argv, "i:o:")) != -1) {
      switch (oc) {
            case 'i':
                ipfname = optarg;
                break;
            case 'o':
                opfname = optarg;
                break;
            case 'h':
                Usage ();
                exit (1);
                break;
            default:
                Usage ();
                exit (1);
                break;
      }


  
  }
  if (sizeofint != 4) {
    printf ("int size is not 4. Incompatible system\n");
    exit (2);
  }
  if (sizeoffloat != 4) {
      printf ("float size is not 4. Incompatible system\n");
      exit (2);
  }

  ipf = fopen (ipfname, "r");
  opf = fopen (opfname, "wb");

  d = (unsigned char *) malloc (sizeof(unsigned char) * sizeofint);
  // read dim value
  bytes_read = fscanf (ipf, "%d %d\n", &dim, &nvec);
  if (bytes_read <= 0) {
     printf("File is empty\n");
     exit(1);
  }
  finc(k,sizeofint) { 
     // putc (((unsigned char *) &dim) + k, opf);
     //d[k] = (unsigned char) (((unsigned char *) &dim)[k]);
     //putc (d[k], opf);
     putc (((unsigned char *) &dim)[k], opf);
  }
  
  finc(k,sizeofint)  {
     // putc (((unsigned char *) &nvec) + k, opf);
     //d[k] = (((unsigned char *) &nvec)[k]);
     //putc (d[k], opf);
     putc (((unsigned char *) &nvec)[k], opf);
  }
  free (d);
  
  for (j = 0; j < nvec; j++) {
      for (i = 0; i < dim; i++) {        
          fscanf (ipf, " %f", &f);          
          finc(k,sizeoffloat)                         // write each byte to file
              putc (*(((unsigned char *) &f) + k), opf);
      }
      fscanf (ipf,"\n");

  }

  fclose (ipf);
  fclose (opf);
}

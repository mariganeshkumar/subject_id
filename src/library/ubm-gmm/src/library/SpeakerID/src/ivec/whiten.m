function [whitemat wmat] = whiten (datamat)
  temp_cov = cov(datamat');
  dim = size(datamat,1);
  [wmat d] = eig(temp_cov);
  whitemat = wmat' * datamat;
  whitemat = whitemat ./ repmat(sqrt(sum(datamat.*datamat,1)),dim,1);

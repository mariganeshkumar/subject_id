/*-------------------------------------------------------------------------
 *  NormaliseHistogram.c - Normalises a given histogram to create a PDF.
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi)
 *
 *  Created:        Tue 19-Feb-2013 09:51:21
 *  Last modified:  Tue 19-Feb-2013 09:58:53 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/


#include "stdio.h"
#include "stdlib.h"
#include "fe/DspLibrary.h"
#include "malloc.h"

int main (int argc, char *argv[])
{
  char             line[500];
  FILE             *fpin, *fpout;
  int              numBins, i;
  float            sum, rMax;
  float            *histogram, *PDF;
  if (argc != 3) {
    printf("Usage: NormaliseHistogram InputHistogram OutputPDF\n");
    exit(-1);
  }
  fpout = fopen(argv[2],"w");
  fpin = fopen(argv[1], "r");
  i = 0;
  while (fgets (line, 200, fpin) != NULL) 
    i++;
  numBins = i+1;
  histogram = (float *) malloc(numBins*sizeof(float));
  PDF = (float *) malloc(numBins*sizeof(float));
  rewind(fpin);
  i = 0;
  while (fgets (line, 200, fpin) != NULL)  {
    sscanf(line, "%f", &histogram[i]);
    i++;
  }
  rMax = histogram[Imax0(histogram, numBins)];
  for (i = 0; i < numBins; i++)  {
    sum = sum + histogram[i];
  }
  for (i = 0; i < numBins; i++)  {
    PDF[i] = histogram[i]/sum;
    fprintf(fpout, "%d %f\n", i, PDF[i]);
  }
  free(histogram);
  free(PDF);
  fclose(fpin);
  fclose(fpout);
  return(0);
}















/**************************************************************************
 * $Log$
 *
 *                        End of ReadSpherePcm.c
 **************************************************************************/


/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of NormaliseHistogram.c
 -------------------------------------------------------------------------*/

function [X,N] = filter_development_data(Xall, Nall, min_nfiles, spk_ids)

idx = Nall >= min_nfiles;
N = Nall(idx);
nfiles = sum(N);
nspk_all = size(Nall,1);
%Initialize
X = zeros(size(Xall,1),nfiles,'single');
ie2 = 0;
for i = 1:nspk_all
    if idx(i)
        speakers_sessions = find(spk_ids == i);
        ib2 = ie2 + 1;
        ie2 = ib2 + Nall(i) -1;
        X(:,ib2:ie2) = Xall(:, speakers_sessions);
    end
end

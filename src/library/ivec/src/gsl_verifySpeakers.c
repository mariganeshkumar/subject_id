/*
    This file computes the cosine similarity between the test i vector
    to all the train i vectors and assigns the best

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/



#include "gsl_helper.h"
#include<string.h>
#define CHAR_ALLOC(k) (char*)malloc(sizeof(char)*k)


int main(int argc, char *argv[]) {
	if(argc!=7) {
		printf("Usage : %s train-test_model_pair wccnMat whitenMat R threshold resultFile\n",argv[0]);
		exit(0);
	}
	FILE *train_testListFile, *resultFile;
	char *train_testListName, *resultFileName, **trainUtterList, 
		**trainModelsList, *wccnMatName, *wMatName, line[256];
	int i,j,R,nUtter=0;
	double tempf, tempf1, tempf2;
	float th;
	gsl_matrix *wccnMat, *wMat, *tempMat, *trainUtterProjMat, 
		*trainModelsProjMat, *resultMat;
	gsl_vector *tempVect, *tempVect1;

	train_testListName=argv[1];
	wccnMatName=argv[2];
	wMatName=argv[3];
	R=atoi(argv[4]);
	th=atof(argv[5]);
	resultFileName=argv[6];
	if((train_testListFile = fopen(train_testListName,"r")) == NULL) {
		fprintf(stderr,"train-test list file %s cannot be opened\n",train_testListName);
        	exit(1);
		}

	resultFile=fopen(resultFileName,"w+");
	nUtter=0;
	while(fgets(line,sizeof(line),train_testListFile)!=NULL) nUtter++;
	rewind(train_testListFile);
	trainUtterList=(char **)malloc(sizeof(char *)*nUtter);
	trainModelsList=(char **)malloc(sizeof(char *)*nUtter);

	 i = 0;
	while (fgets(line,1000,train_testListFile)) {
		trainUtterList[i]= CHAR_ALLOC(256);
		trainModelsList[i]= CHAR_ALLOC(256);
		sscanf(line,"%s %s\n",trainUtterList[i], trainModelsList[i]); 
    		i++;
  	}
	if(i!=nUtter) {
		fprintf(stderr,"Error while reading train-test list\n");
		exit(1);
	}

	wccnMat = gsl_matrix_alloc(R,R);
	wMat = gsl_matrix_alloc(R,R);
	tempVect = gsl_vector_alloc(R);
	tempVect1 = gsl_vector_alloc(R);
	trainUtterProjMat = gsl_matrix_alloc(R,nUtter);
	trainModelsProjMat = gsl_matrix_alloc(R,nUtter);
	resultMat = gsl_matrix_alloc(nUtter,nUtter);
	gsl_matrix_set_zero(trainUtterProjMat);
	gsl_matrix_set_zero(trainModelsProjMat);
	gsl_matrix_set_zero(resultMat);
	wccnMat = read_matrix(wccnMatName,R,R);
	wMat = read_matrix(wMatName,R,R);
	gsl_matrix_transpose(wccnMat);

/*      Differs from gsl_testCosine2.c in the following step. Normalization after 
        each projection  */

	tempMat = gsl_matrix_alloc(R,nUtter);
	for(i=0;i<nUtter;i++) {
		tempVect1=read_vector(trainUtterList[i],R);
		gsl_blas_dgemv(CblasTrans, 1.0, wMat, tempVect1, 0.0, tempVect);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(tempMat,i,tempVect);
	}
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat,tempMat,0.0,trainUtterProjMat);
       for(i=0;i<nUtter;i++) {
		gsl_matrix_get_col(tempVect,trainUtterProjMat,i);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(trainUtterProjMat,i,tempVect);
	}

	tempMat = gsl_matrix_alloc(R,nUtter);
	for(i=0;i<nUtter;i++) {
		tempVect1=read_vector(trainModelsList[i],R);
		gsl_blas_dgemv(CblasTrans, 1.0, wMat, tempVect1, 0.0, tempVect);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(tempMat,i,tempVect);
	}
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat,tempMat,0.0,trainModelsProjMat);
	for(i=0;i<nUtter;i++) {
		gsl_matrix_get_col(tempVect,trainModelsProjMat,i);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(trainModelsProjMat,i,tempVect);
	}

	 
	for(i=0;i<nUtter;i++) {
		gsl_matrix_get_col(tempVect,trainUtterProjMat,i);
                for(j=0;j<nUtter;j++) {              
			gsl_matrix_get_col(tempVect1,trainModelsProjMat,j);
			gsl_blas_ddot(tempVect,tempVect1,&tempf2);
			gsl_matrix_set(resultMat,i,j,tempf2);
		}
        }
	for(i=0;i<nUtter;i++) {
		tempf=gsl_matrix_get(resultMat,i,i);
		if(tempf > th) {
			printf("%s : %s : score : %f : accept\n",trainUtterList[i],trainModelsList[i],tempf);
			fprintf(resultFile,"%s : %s : score : %f : accept\n",trainUtterList[i],trainModelsList[i],tempf);
		}
		else {
			printf("%s : %s : score : %f : reject\n",trainUtterList[i],trainModelsList[i],tempf);
			fprintf(resultFile,"%s : %s : score : %f : reject\n",trainUtterList[i],trainModelsList[i],tempf);
		}
	}
	gsl_matrix_free(wccnMat);
	gsl_matrix_free(wMat);
	gsl_matrix_free(tempMat);
	gsl_matrix_free(trainUtterProjMat);
	gsl_matrix_free(trainModelsProjMat);
	gsl_matrix_free(resultMat);
	gsl_vector_free(tempVect);
	gsl_vector_free(tempVect1);
}

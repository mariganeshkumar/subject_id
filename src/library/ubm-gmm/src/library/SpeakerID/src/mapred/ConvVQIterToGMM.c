#include "CommonFunctions.h"

int
main (int argc, char *argv[])
  {
	  int oc, i, nvec, flen, key;
		int ipsupplied = 0, opsupplied = 0;
		VECTOR_OF_F_VECTORS *vfv = NULL;
		char *opname, *ipname;
		FILE *ipfile = NULL, *opfile = NULL;

		while ((oc = getopt(argc, argv, "i:o:")) != -1) {		
				switch (oc) {
						case 'i':
						  ipsupplied = 1;
							ipname = optarg;
							break;
						case 'o':
						  opsupplied = 1;
							opname = optarg;
							break;
						default:
						  fprintf (stderr, "Wrong option %c\n", oc);
							return (1);
							break;
				}
		}

		if (!ipsupplied || !opsupplied) {
		  fprintf (stderr, "Input/Output not supplied\n");
		  return 1;
		}

		ipfile = fopen (ipname, "r");
		if (ipfile == NULL)
		  {
			  fprintf (stderr, "couldn't open i/p file\n");
				return  2;
			}

		opfile = fopen (opname, "w");
		if (opfile == NULL)
		  {
			  fprintf (stderr, "couldn't open o/p file\n");
				return 3;
			}
	  
		while (!feof (ipfile)) {
				fscanf (ipfile, "%d\t%d,%d", &key, &nvec, &flen);
				if (vfv == NULL) {
						vfv = (VECTOR_OF_F_VECTORS *)
						      calloc (2, sizeof (VECTOR_OF_F_VECTORS));									
						vfv[0] = (F_VECTOR *) AllocFVector (flen);
						vfv[1] = (F_VECTOR *) AllocFVector (flen);
				}
				else if (flen != vfv[0]->numElements) {
				    fprintf (stderr, "Jagged arrays in ipfile\n");
						return 4;
				}

				for (i = 0; i < flen; i++)				  
					  fscanf (ipfile, ",%e", &vfv[0]->array[i]);
				for (i = 0; i < flen; i++)				  
					  fscanf (ipfile, ",%e", &vfv[1]->array[i]);
			  
				fprintf (opfile, "%f\n", (float) nvec);
				for (i = 0; i < flen; i++)				  
					  fprintf (opfile, " %e %e", 
						               vfv[0]->array[i],
													 vfv[1]->array[i]);
				fscanf (ipfile,"\n");													 
		}
		fclose (ipfile);
		fclose (opfile);
  }

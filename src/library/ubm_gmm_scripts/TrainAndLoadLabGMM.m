function gmmDist= TrainAndLoadLabGMM(scriptFile,numMixtures, seedValue, modelDir, tempDir)
    if exist([modelDir,'/UBM_GMM.txt'], 'file') == 2
            model=load([modelDir,'/UBM_GMM.txt']);
        mixturePrior=model(:,1)';
        mixtureMean=model(:,2:1:(end-1)/2+1);
        mixtureCovariance=model(:,(end-1)/2+2:1:end)';
        mixtureCovariance = reshape(mixtureCovariance, [1, size(mixtureCovariance)]);
        gmmDist=gmdistribution(mixtureMean,mixtureCovariance,mixturePrior);
    else
        system(['sh library/classifiers/ubm_gmm_classifier/SupervisedGMM.sh ' ...
           scriptFile ' '...
           modelDir '/UBM_GMM.txt '...
           num2str(numMixtures) ' ' ...
           tempDir ' ' num2str(seedValue)])
        model=load([modelDir,'/UBM_GMM.txt']);
        mixturePrior=model(:,1)';
        mixtureMean=model(:,2:1:(end-1)/2+1);
        mixtureCovariance=model(:,(end-1)/2+2:1:end)';
        mixtureCovariance = reshape(mixtureCovariance, [1, size(mixtureCovariance)]);
        gmmDist=gmdistribution(mixtureMean,mixtureCovariance,mixturePrior);

    end
end
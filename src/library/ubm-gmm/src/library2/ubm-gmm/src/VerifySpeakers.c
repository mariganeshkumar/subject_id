/*
    This file extracts target and non-target scores from the 
    train data

    Copyright (C) 2002-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Hema A Murthy <hema@cse.iitm.ac.in>,
    Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "CommonFunctions.h"

float  *ComputeTopCLikelihood (VFV *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VFV **speakerMeans, 
			   VFV **speakerVars, 
			   VFV *speakerWts, 
			   int numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;

  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	       if (mixProbValue > LOG_ZERO )	 
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    Distortion[i] = Distortion[i]/numVectors;
  }
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue;
};

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VFV *ubmModelMeans, VFV *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
        mixtureIds[j][k] = bestCMixtures[k].id;
      }
    return mixtureIds;  
  }


int main(int argc, char *argv[])
  {

  FILE                  *feature_modelListFile=NULL,*speakerFile=NULL, 
                        *ubmFile=NULL,*modelListFile=NULL,*resultFile=NULL;
  char                  *feature_modelListName =NULL, *modelListName =NULL,
                        *ubmFileName = NULL,*featureName = NULL,*resultFileName=NULL; 
  char  		tempchar, *speakerModel,line[500],**modelName,
			**testModelName, **featureFileNames, 
			*featureFileName;
  int                   numSpeakers, nUtter, nTest, numClusters;
  VFV   **speakerModelMeans, **speakerModelVars, *speakerModelWts,
	**testSpeakerModelMeans, **testSpeakerModelVars, *testSpeakerModelWts;

  VFV   *ubmModelMeans, *ubmModelVars;
  VFV   **featureVectors;
  int                   i, j, k, **mixIds, **mixIds1, mixNumber;
  int                   featLength, cValue, tempint, startFrom, endAt;
  unsigned int          *numVectors;
  float                 *Distortion, *Distortion1, mixProbValue, *ubmWts,mean,sd,th;
  char 			c, **argv_mand;
  if (argc != 11) {
	printf (" Usage : %s [options] feature-modelPairList modelList(the one used for estimating threshold) ubm topC scoreThreshold resultFile\n",argv[0]);
	printf("options:\n");
	printf("--start: index of the start file\n");
	printf("--end: index of the end file\n"); 
	exit(-1);
	}

  static struct option long_options[] = {
    {"start", required_argument,NULL, 's'},
    {"end", required_argument,NULL, 'e'},
  };
  while ((c = getopt_long (argc, argv, "s:e:", long_options, NULL)) != -1) {
    switch(c) {
      case 's': 
        sscanf(optarg,"%d", &startFrom);
        break;
      case 'e':
        sscanf(optarg,"%d",&endAt);
        break;
      default:
        printf("Option %c not recognized\n", c);
        break;
      }
  }

  argv_mand = &argv[optind-1];

  feature_modelListName = argv_mand[1];
  if((feature_modelListFile = fopen(feature_modelListName,"r")) == NULL) {
	fprintf(stderr,"Feature-model list file %s cannot be opened\n",feature_modelListName);
        exit(1);
	}
  modelListName = argv_mand[2];
  if((modelListFile = fopen(modelListName,"r")) == NULL) {
	fprintf(stderr,"Model list file %s cannot be opened\n",modelListName);
        exit(1);
	}

  ubmFileName = argv_mand[3];
  sscanf (argv_mand[4], "%d", &cValue);
  sscanf (argv_mand[5], "%f", &th);

  resultFileName = argv_mand[6];
  if((resultFile = fopen(resultFileName,"a+")) == NULL) {
	fprintf(stderr,"result file %s cannot be opened\n",resultFileName);
        exit(1);
	}


 nUtter = 0;
  while (fgets(line,1000,feature_modelListFile)) {
    nUtter++;
    speakerModel = (char *) calloc (500,sizeof(char));
    featureFileName = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %s\n",featureFileName, speakerModel); 
    if(featureFileName == NULL || speakerModel == NULL) {
	fprintf(stderr,"Problem while reading line  %d in file %s\n",nUtter,feature_modelListName);
       	exit(1);
	}
    free (speakerModel);
    free (featureFileName);
  }
  rewind(feature_modelListFile);
	nTest=endAt-startFrom+1;

 numSpeakers = 0;
  while (fgets(line,500,modelListFile)) {
    numSpeakers++;
    speakerModel = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %d\n",speakerModel,&tempint); 
    if(speakerModel == NULL) {
	fprintf(stderr,"Problem while reading line  %d in file %s\n",numSpeakers,modelListName);
       	exit(1);
	}
    free (speakerModel);
  }
  rewind(modelListFile);
  numClusters=tempint;

  printf ("number of utterances = %d\n", nUtter);
  printf ("number of models used to normalize scores = %d\n", numSpeakers);

  featureFileNames = (char **) calloc(nUtter,sizeof(char *));
  numVectors = (unsigned int *) calloc(nUtter,sizeof(int *));
  featureVectors = (VFV **) calloc(nUtter,sizeof(VFV *));

  modelName = (char **) calloc(numSpeakers,sizeof(char *));
  testModelName = (char **) calloc(nUtter,sizeof(char *));
  speakerModelMeans = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelVars = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelWts = (VFV *) calloc(numSpeakers,sizeof(VFV ));
  testSpeakerModelMeans = (VFV **) calloc(nUtter,sizeof(VFV *));
  testSpeakerModelVars = (VFV **) calloc(nUtter,sizeof(VFV *));
  testSpeakerModelWts = (VFV *) calloc(nUtter,sizeof(VFV ));
 
  i = 0;
  while (fgets(line,1000,feature_modelListFile)) {
    speakerModel = (char *) calloc (500,sizeof(char));
    featureFileName = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %s\n",featureFileName, speakerModel); 
    featureFileNames[i]=(char *) calloc (100, sizeof(char));
    strcpy(featureFileNames[i], featureFileName);
    featureVectors[i] = ReadVfvFromFile (featureFileName, &numVectors[i]);

  featLength = featureVectors[0][0]->numElements;
    testModelName[i]=(char *) calloc (100,sizeof(char));
    strcpy(testModelName[i],speakerModel);
    speakerFile = fopen(speakerModel,"r");
    testSpeakerModelMeans[i] = (VFV *) calloc(numClusters,
    sizeof(VFV));
    testSpeakerModelVars[i] = (VFV *) calloc(numClusters,
    sizeof(VFV));
    testSpeakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters);
    for (j = 0; j < numClusters; j++) {
      testSpeakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      testSpeakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters; j++) {
      fscanf(speakerFile,"%e",&testSpeakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %e %e",&testSpeakerModelMeans[i][j]->array[k],
               &testSpeakerModelVars[i][j]->array[k]);
      }
    }
    i++;
    fclose(speakerFile);
    free (speakerModel);
    free (featureFileName);
  }

  i = 0;
  while (fgets(line,500,modelListFile)) {
    speakerModel = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %d\n",speakerModel, &tempint);
    modelName[i]=(char *) calloc (100,sizeof(char));
    strcpy(modelName[i],speakerModel);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VFV *) calloc(numClusters,
    sizeof(VFV));
    speakerModelVars[i] = (VFV *) calloc(numClusters,
    sizeof(VFV));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters);
    for (j = 0; j < numClusters; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters; j++) {
      fscanf(speakerFile,"%e",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %e %e",&speakerModelMeans[i][j]->array[k],
               &speakerModelVars[i][j]->array[k]);
      }
    }
    i++;
    fclose(speakerFile);
  }

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VFV *) calloc (numClusters, sizeof (VFV));
  ubmModelVars  = (VFV *) calloc (numClusters, sizeof (VFV));
  ubmWts        = (float *) calloc (numClusters, sizeof (float));

  for (i = 0; i < numClusters; i++)
    {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);

  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion1 = (float *)calloc(nUtter, sizeof(float));
    printf("Computing raw trial scores ...\n");
    for(i=startFrom-1;i<endAt;i++) {
	printProgress((i-startFrom+1)/(float)(endAt-startFrom));
	mixIds1 = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters, featureVectors[i], numVectors[i], cValue, 1.0); 

	Distortion1[i] = 0;
	for (j = 0; j < numVectors[i]; j++) {
        	for (k = 0; k < cValue; k++) {
			mixNumber = mixIds1[j][k]-1;
		         if (!k)
			         mixProbValue = ComputeProbability(testSpeakerModelMeans[i][mixNumber],
					testSpeakerModelVars[i][mixNumber], testSpeakerModelWts[i]->array[mixNumber],featureVectors[i][j],
					1.0);
		         else
			         mixProbValue = LogAdd (mixProbValue,
                        	 	ComputeProbability(testSpeakerModelMeans[i][mixNumber],
	                                testSpeakerModelVars[i][mixNumber], testSpeakerModelWts[i]->array[mixNumber],featureVectors[i][j],
        	                        1.0));
          	}
               if (mixProbValue > LOG_ZERO )
	       Distortion1[i] = Distortion1[i] + mixProbValue;
    	}
	Distortion1[i] = Distortion1[i]/numVectors[i];
    }

    printf("\nNormalizing scores ...\n");
    for(i=startFrom-1;i<endAt;i++) {
	printProgress((i-startFrom+1)/(float)(endAt-startFrom));
	mean=0;sd=0;
    	mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters, featureVectors[i], numVectors[i], cValue, 1.0); 
	Distortion = (float *) ComputeTopCLikelihood(featureVectors[i], 
               		numVectors[i], numSpeakers, speakerModelMeans,
   			speakerModelVars, speakerModelWts, numClusters,
			Distortion, 1.0, cValue, mixIds);
        for(j=0;j<numSpeakers;j++) 
		mean+=Distortion[j];
        mean/=numSpeakers;
        for(j=0;j<numSpeakers;j++)
                sd+=pow((Distortion[j]-mean),2);
        sd=sqrt(sd/numSpeakers);

	Distortion1[i]=(Distortion1[i]-mean)/sd;

	if(Distortion1[i] > th) {
		//printf("%s : %s : score : %f : accept\n",featureFileNames[i],testModelName[i],Distortion1[i]);
		fprintf(resultFile,"%s : %s : score : %f : accept\n",featureFileNames[i],testModelName[i],Distortion1[i]);
		}
	else {
		//printf("%s : %s : score : %f : reject\n",featureFileNames[i],testModelName[i],Distortion1[i]);
		fprintf(resultFile,"%s : %s : score : %f : reject\n",featureFileNames[i],testModelName[i],Distortion1[i]);
		}
  	}
	
  fclose(resultFile);
  free(Distortion);
  free(Distortion1);
  for (i=0;i<nUtter;i++) {
    free(featureVectors[i]);
  }
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
  }

  free (speakerModelWts);
  free (testSpeakerModelWts);
  return(0);
  }







/****************************************************************************
*	The following subroutine computes the Group delay Cepstrum
*       of a given signal
*
*	inputs : signal npts 
*	mfft : fft order nfft = 2**mfft
*	winlen : number of cepstral coeffts
*
*	Outputs :
*	groupDelay(nfft) - Group delay function
*****************************************************************************
*
*/
float *GroupDelayCepstrum(float *signal,int npts, int nfft, int mfft, 
			  int winlen, float *groupDelayCepstrum){
  static float		*sigTemp;
  static float		*ax, *ay,*amag,*phase, *groupDelay;
  static int            nfBy2,flag = 0;
  static complex 	*cSig,*cfSig;
  static complex	*cfTemp1,*cfTemp2,u;
  static float          epsilon = 0.0001;
  int		        i;
  float                 Ave, c0;

  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) AllocFloatArray(ax,nfft+1);
    ay = (float *) AllocFloatArray(ay,nfft+1);
    amag = (float *) AllocFloatArray(amag,nfft+1);
    phase = (float *) AllocFloatArray(phase,nfft+1);
    groupDelay = (float *) AllocFloatArray(groupDelay,nfft+1);
    sigTemp = (float *) AllocFloatArray(sigTemp,nfft+1);
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    if ((cSig == NULL) || (cfSig == NULL) || (cfTemp1 == NULL) ||
	(cfTemp2 == NULL)) {
      printf("unable to allocate complex array in Group Delay\n");
      fflush(stdout);
      exit(-1);
    } 
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i <= npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  c0 = 0;
  for (i = 1; i <= nfft; i++)
    c0 = c0 + amag[i]*amag[i];
  c0 = sqrt(c0/nfft);
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  for (i = 1; i <= nfft; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    if (amag[i] > epsilon) {
      cfTemp2[i].re = u.re/(amag[i]*amag[i]);
      cfTemp2[i].im = u.im/(amag[i]*amag[i]);
    } else 
      cfTemp2[i] = cfTemp2[i-1];
    groupDelay[i] = cfTemp2[i].re;
  }
  RemoveAverage(groupDelay,nfft,&Ave); 
  Rfft(groupDelay,ax,ay,mfft,nfft,1);
  for (i = 1; i <= winlen; i++) 
    groupDelayCepstrum[i-1] = ax[i+1]*HanW(i,winlen+1);
  return(groupDelayCepstrum);
}
/****************************************************************************
*	The following subroutine computes the smoothed group delay function of signal
*
*	inputs : signal(npts) 
*	mfft : fft order  = 2**mfft
*	winlen : window length for smoothing
*
*	Outputs :
*	groupDelay(nfft) - Group delay function
*****************************************************************************
*
*/
float *GroupDelay(float *signal,int npts, int nfft, int mfft, int winlen,
		float *groupDelay){
  static float		*ax, *ay,*amag,*phase, *gdCepstrum;
  static int            nfBy2,flag = 0;
  int		        i;
  float                 Ave;

  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) AllocFloatArray(ax,nfft+1);
    ay = (float *) AllocFloatArray(ay,nfft+1);
    amag = (float *) AllocFloatArray(amag,nfft+1);
    phase = (float *) AllocFloatArray(phase,nfft+1);
    gdCepstrum = (float *) AllocFloatArray(gdCepstrum,winlen);
    flag = 1;
  }
  gdCepstrum = (float *) GroupDelayCepstrum(signal, npts, nfft, mfft,
					    winlen, gdCepstrum);
  ax[1] = 0.0;
  for (i = 2; i <= winlen; i++) {
    ax[i] = gdCepstrum[i-2];
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+1; i <= nfft-(winlen-1); i++)
    ax[i] = 0.0;
  Rfft(ax,groupDelay,ay,mfft,nfft,-1);
  return(groupDelay);
}


/****************************************************************************
*	The following function computes the minimum phase 
*	group delay cepstrum for the given signal 
*
*	inputs : signal(npts)
*	mfft : fft order  nfft = 2**mfft
*	winlen : window length for minimum phase signal
*	gamma : time compression of minimum phase signal
*
*       Outputs:
*       MinGdCepstrum(winlen) - float *
*       Comment - mean removal not done as MinGd already is zero mean
*****************************************************************************
*
*/
float *MinGdCepstrum(float *signal,int npts, int nfft, int mfft, 
		int winlen, float *minGdCepstrum, float gamma){
  static float	  *sigTemp;
  static int      flag = 0;
  static float	  *ax, *ay,*amag,*phase, *minGd;
  static int      nfBy2;
  int		  i,npeaks;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    sigTemp = (float *) AllocFloatArray(sigTemp,nfft+1);
    ax =  (float *) AllocFloatArray(ax,nfft+1);
    ay = (float *) AllocFloatArray(ay,nfft+1);
    amag = (float *) AllocFloatArray(amag,nfft+1);
    phase = (float *) AllocFloatArray(phase,nfft+1);
    minGd = (float *) AllocFloatArray(minGd,nfft+1);
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for (i = 1; i <= nfft; i++){
    amag[i] = exp(log(amag[i])*gamma);
  }
  Rfft(amag,sigTemp,ay,mfft,nfft,1);
  for (i = 1; i <= winlen; i++)
    minGdCepstrum[i-1] = sigTemp[i]*HanW(i,winlen+1);
  return(minGdCepstrum);
}

/****************************************************************************
*	The following function computes the Minimum phase 
*	group delay function from the given signal 
*
*	inputs : signal(npts)
*	mfft : fft order  nfft = 2**mfft
*	winlen : window length for minimum phase signal
*	alfa : compression required for magnitude spectrum
*
*       Outputs:
*       MinGd - float *
*****************************************************************************
*
*/
float *MinGd(float *signal,int npts, int nfft, int mfft, 
		int winlen, float *minGd, float gamma){

  static float	  *ax, *ay,*amag,*phase; 
  static float    *sigTemp, *minGdCepstrum;
  static int      flag = 0;
  static int      nfBy2;
  int		  i;
  float           Ave;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    sigTemp = (float *) AllocFloatArray(sigTemp,nfft+1);
    ax =  (float *) AllocFloatArray(ax,nfft+1);
    ay = (float *) AllocFloatArray(ay,nfft+1);
    amag = (float *) AllocFloatArray(amag,nfft+1);
    phase = (float *) AllocFloatArray(phase,nfft+1);
    minGdCepstrum = (float *) AllocFloatArray(minGdCepstrum, winlen);
    flag = 1;
  }
  minGdCepstrum = MinGdCepstrum(signal, npts, nfft, mfft,
				winlen, minGdCepstrum, gamma);
  for (i = 1; i <= winlen; i++) 
    sigTemp[i] = minGdCepstrum[i-1];
  for (i = winlen+1; i <= nfft; i++)
    sigTemp[i] = 0.0;
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for(i = 1; i <=  nfBy2-1; i++) {
    minGd[i] = phase[i] - phase[i+1];
    minGd[nfft-i+1] = minGd[i];
  }
  minGd[nfBy2] = minGd[nfBy2-1];
  minGd[nfBy2+1] = minGd[nfBy2];
  return(minGd);
}

/****************************************************************************
*	The following subroutine extracts the pitch from the given signal
*	using modified group delay functions
*
*	inputs : DaTa npts points long
*	mfft : FFT stages nfft = 2**mfft
*	smthWinSize : window length for zero Spectrum
*       pthLow, pthHgh : expected range of pitch values
*
*	Outputs :
*	pitchModgd - value of the pitch picked
*****************************************************************************
*
*/
	float *PitchModGd(float *signal,int npts,int nfft,int mfft, int pthLow,
			 int pthHgh, int winlen, int smthWinSize, float *pitchGd){
	static float		*sigTemp, *ax, *ay, *cepAmag, *amag, *phase, *modGd;
	static int		nfBy2;
        float		        c0, ave;
        static int              flag = 0;
        int                     i;
        if (flag == 0) {
	  nfBy2 = nfft/2;
          modGd = (float *) calloc(nfft+1, sizeof(float));
          sigTemp = (float *) calloc(nfft+1, sizeof(float));
	  ax = (float *) calloc(nfft+1, sizeof(float));
	  ay = (float *) calloc(nfft+1, sizeof(float));
	  amag = (float *) calloc(nfft+1, sizeof(float));
	  phase = (float *) calloc(nfft+1, sizeof(float));
	  cepAmag = (float *) calloc(nfft+1, sizeof(float));
	  modGd = (float *) calloc(nfft+1, sizeof(float));
	}
	for (i = 1; i <= npts; i++)
	  sigTemp[i] = signal[i];
	for(i = npts+1; i<= nfft; i++)
	  sigTemp[i] = 0.0;
	Rfft(sigTemp,ax,ay,mfft,nfft,-1);
	SpectrumReal(nfft,ax,ay,amag,phase);
  	for( i = 1; i <= nfft; i++)
	  ax[i] = amag[i];
	CepSmooth(ax,cepAmag,mfft,nfft,smthWinSize, &c0, 1.0);
        for (i = 1; i <= npts; i++)
          sigTemp[i] = amag[i]/cepAmag[i];
	for(i = npts+1; i<= nfft; i++)
	  sigTemp[i] = 0.0;
	modGd = (float *) SmoothModGd(sigTemp, npts, nfft, mfft, winlen, 
					smthWinSize, modGd);
        for (i = 0; i <= pthLow; i++)
          modGd[i] = 0;
	for (i = pthHgh+1; i < nfBy2; i++)
          modGd[i] = 0; 
	RemoveAverage(modGd,nfBy2,&ave);
        for (i = 0; i < nfBy2; i++)
	  pitchGd[i] = modGd[i];
        return(pitchGd);
	}


/****************************************************************************
*	The following function computes  the
*	smoothed magnitude spectrum of the given signal
*       Cepstral smoothing is used.
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*/
float *SmoothMagSpectrum(float *signal, int npts, int nfft, int mfft, int smthWinSize,
                         float *smthMag) {
  static float		*ax, *ay, *amag, *phase, *modGd, *sigTemp;
  static float		*cepAmag, c0;
  static int            nfBy2, flag = 0, i;
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }

 
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
 Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, 1.0);
  for (i = 0; i <= nfBy2; i++)
    smthMag[i] = cepAmag[i+1];
  return(smthMag);
  }
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*/
float *ModGdCepstrumNcN(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex) { 
  static float		*sigTemp;
  static float		*ax, *ay, *amag, *phase, *modGd;
  static float		*cepAmag;
  static int            nfBy2, flag = 0;
  static complex        *cSig, *cfSig;
  static complex	*cfTemp1, *cfTemp2, u;
  int		        i, sign;
  float		        c0, Ave, rmax, abscfTemp, logcfTemp, alfaLogTemp;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, gamma);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = (u.re/(cepAmag[i]*cepAmag[i]));
    cfTemp2[i].im = (u.im/(cepAmag[i]*cepAmag[i]));
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /* RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }  
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }
    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	if (i > 1)
	  modGd[nfft - i+ 2] = 0;
      }
    }

  Rfft(modGd,ax,ay,mfft,nfft,1);
  for (i = 1; i <= winlen; i++) 
    modGdCepstrum[i-1] = ax[i];
  return(modGdCepstrum);  
}

/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum (ncn) for the given signal
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*
*/
float *ModGdCepstrum(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex) { 

static  float                     *modGdCepTemp;
static  int                       flag = 0;
int                               i;
if (flag == 0) {
  modGdCepTemp = calloc(winlen+1, sizeof(float));
  flag = 1;
}

modGdCepTemp = ModGdCepstrumNcN (signal, npts, nfft, mfft, winlen+1, smthWinSize,
				 modGdCepTemp, alfaP, alfaN, gamma, gdsign, removeLPhase,
				 startIndex, endIndex);
for (i = 0; i < winlen; i++)
  modGdCepstrum[i] = modGdCepTemp[i+1]/(float)(i+1);
return(modGdCepstrum);
}
/****************************************************************************
*	The following function computes  the
*	modified group delay cepstrum  for the given signal using DCT
*
*	inputs : signal - npts
*	mfft : fft order nfft = 2**mfft
*	winlen : window length for zero spectrum
*
*	Outputs :
*	modGdCepstrum(winlen) - modified group delay cepstrum
*****************************************************************************
*
*/
float *ModGdCepstrum_DCT(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex, float scaleDCT) { 
  static float		     *sigTemp;
  static float		     *ax, *ay, *amag, *phase, *modGd;
  static float		     *cepAmag;
  static int                 nfBy2, flag = 0;
  static complex             *cSig, *cfSig;
  static complex	     *cfTemp1, *cfTemp2, u;
  static VECTOR_OF_F_VECTORS *discreteCosineTransform;
  static F_VECTOR            *fvect, *modGdFvect;
  int		              i, sign;
  float		              c0, Ave, rmax, abscfTemp, 
                              logcfTemp, alfaLogTemp;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    modGd = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    discreteCosineTransform = (VECTOR_OF_F_VECTORS *) generate_pseudo_dct(1, winlen, nfBy2);
    fvect = alloc_f_vector(nfBy2);
    modGdFvect = alloc_f_vector(winlen);
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, gamma);
  for (i = 1; i <= nfBy2+1; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = (u.re/(cepAmag[i]*cepAmag[i]));
    cfTemp2[i].im = (u.im/(cepAmag[i]*cepAmag[i]));
    modGd[i] = cfTemp2[i].re;
    if (i > 1)
      modGd[nfft-i+2] = modGd[i];
  }
  /* Remove DC component of modGd */

  /*RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 1; i <= nfft; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }   
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }

    if ((startIndex > 1) || (endIndex < nfBy2)) {
      for (i = 1; i < startIndex; i++) { 
	modGd[i] = 0.0;
	if (i > 1)
	  modGd[nfft - i+2] = modGd[i];
      }
      for (i = endIndex+1; i <= nfBy2; i++) {
	modGd[i] = 0;
	if (i > 1)
	  modGd[nfft - i+ 2] = 0;
      }
    }
  for (i = 1; i <= nfBy2;i++)
    fvect->array[i-1] = modGd[i];
  linear_transformation_of_f_vector(fvect, discreteCosineTransform, modGdFvect);
  for (i = 1; i <= winlen; i++) 
    modGdCepstrum[i-1] = modGdFvect->array[i-1]/(float)i/(float)scaleDCT;
  return(modGdCepstrum);  
}

/****************************************************************************
*	The following function computes  the
*	modified group delay functions from ModGdCepstrum using DCT
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - smoothed modified group delay function
*****************************************************************************
*
*/
float *ModGd_DCT(float *signal,int npts,int nfft,int mfft, 
	     int winlen, int smthWinSize, float *modGd, float alfaP, float alfaN, float gamma, int gdsign, int removeLPhase,
	     int startIndex, int endIndex, float scaleDCT) {
  static float		*ax, *ay, *modGdCepstrum;
  static int              flag = 0;
  int		        i;
  float		        c0,Ave;
  if (flag == 0) {
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    modGdCepstrum = (float *) calloc(winlen, sizeof(float));
    flag = 1;
  }
  modGdCepstrum = (float *) ModGdCepstrum_DCT(signal, npts, nfft, 
					  mfft, winlen, smthWinSize, modGdCepstrum, alfaP, alfaN, gamma, 
					      gdsign, removeLPhase, startIndex, endIndex, scaleDCT);
  ax[1] = 0.0;
  for (i = 2; i <= winlen; i++) {
    ax[i] = modGdCepstrum[i-2]*(i-1);
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+1; i <= nfft-(winlen-1); i++)
    ax[i] = 0.0;
  Rfft(ax,modGd,ay,mfft,nfft,-1);
  return(modGd);
}

/****************************************************************************
*	The following function computes  the
*	modified group delay functions for the given signal
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - smoothed modified group delay function
*****************************************************************************
*
*/
float *ModGd(float *signal,int npts,int nfft,int mfft, 
	     int winlen, int smthWinSize, float *modGd, 
	     float alfaP, float alfaN, float gamma, int gdsign, int removeLPhase,
	     int startIndex, int endIndex) {
  static float		*ax, *ay, *modGdCepstrum;
  static int              flag = 0;
  int		        i;
  float		        c0,Ave;
  if (flag == 0) {
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    modGdCepstrum = (float *) calloc(winlen, sizeof(float));
    flag = 1;
  }
  modGdCepstrum = (float *) ModGdCepstrum(signal, npts, nfft, 
					  mfft, winlen, smthWinSize,
					  modGdCepstrum, alfaP, alfaN, gamma, 
					  gdsign, removeLPhase, startIndex, endIndex);
  ax[1] = modGdCepstrum[0];
  for (i = 2; i <= winlen; i++) {
    ax[i] = modGdCepstrum[i-1]*(i-1);
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+1; i <= nfft-(winlen-1); i++)
    ax[i] = 0.0;
  Rfft(ax,modGd,ay,mfft,nfft,-1);
  return(modGd);
}


/****************************************************************************
*	The following function computes  the standard
*	modified group delay functions for the given signal
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - smoothed modified group delay function
*****************************************************************************
*
*/
float *SmoothModGd(float *signal,int npts,int nfft,int mfft, int winlen, int smthWinSize, float *modGd) {
  static float		      *ax, *ay, *modGdTemp, *modGdCepstrum;
  static int                  flag = 0;
  /*static VECTOR_OF_F_VECTORS  *discreteInvCosineTransform;*/
  static F_VECTOR             *fvect, *modGdFvect;
  static int                  nfBy2;
  int		              i;

   if (flag == 0) {
     nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    modGdCepstrum = (float *) calloc(winlen, sizeof(float));
    modGdTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
modGdCepstrum = (float *) ModGdCepstrumNcN(signal, npts, nfft, 
					  mfft, winlen, smthWinSize, modGdCepstrum, 
					  1.0, 1.0, 1.0, 1, 0, 1, nfft);
  ax[1] = modGdCepstrum[0];
  for (i = 2; i <= winlen; i++) {
    ax[i] = modGdCepstrum[i-1];
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+1; i <= nfft-(winlen-1); i++)
    ax[i] = 0.0;
  Rfft(ax,modGdTemp,ay,mfft,nfft,-1);
  for (i = 0; i < nfBy2; i++)
    modGd[i] = modGdTemp[i+1];
  return(modGd);
}


/****************************************************************************
*	The following function computes  the standard
*	modified group delay functions for the given signal
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - smoothed modified group delay function
*****************************************************************************
*
*/
float *StandardModGd(float *signal,int npts,int nfft,int mfft, int smthWinSize, float *modGd) {
  static float		*sigTemp;
  static float		*ax, *ay, *amag, *phase;
  static float		*cepAmag;
  static int            nfBy2, flag = 0;
  static complex        *cSig, *cfSig;
  static complex	*cfTemp1, *cfTemp2, u;
  int		        i, sign;
  float		        c0, Ave, rmax, abscfTemp, logcfTemp, alfaLogTemp;
  
  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    amag = (float *) calloc(nfft+1, sizeof(float));
    cepAmag = (float *) calloc(nfft+1, sizeof(float));
    phase = (float *) calloc(nfft+1, sizeof(float));
    cSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfSig = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp1 = (complex *) calloc (nfft+1, sizeof(complex));
    cfTemp2 = (complex *) calloc (nfft+1, sizeof(complex));
    sigTemp = (float *) calloc(nfft+1, sizeof(float));
    flag = 1;
  }
  for (i = 1; i <= npts; i++)
    sigTemp[i] = signal[i];
  for(i = npts+1; i<= nfft; i++)
    sigTemp[i] = 0.0;
  for (i = 1; i < npts; i++){
    u.re = sigTemp[i];
    u.im = 0.0;
    cSig[i] = u;
    u.re = (float)(i-1);
    u.im = 0.0;
    cmul(cfSig[i],u,cSig[i]);
  }
  for (i = npts+1; i <= nfft; i++) {
    cSig[i].re = 0.0;
    cSig[i].im = 0.0;
    cfSig[i].re = 0.0;
    cfSig[i].im = 0.0;
  }
  Cfft(cSig,cfTemp1,mfft,nfft,-1);
  Cfft(cfSig,cfTemp2,mfft,nfft,-1);
  Rfft(sigTemp,ax,ay,mfft,nfft,-1);
  SpectrumReal(nfft,ax,ay,amag,phase);
  for( i = 1; i <= nfft; i++)
    ax[i] = amag[i];
  CepSmooth(ax,cepAmag,mfft,nfft, smthWinSize, &c0, 1.0);
  for (i = 1; i <= nfBy2; i++){
    conjg(u,cfTemp1[i]);
    cmul(cfTemp2[i],cfTemp2[i],u);
    u.re = cfTemp2[i].re;
    u.im = cfTemp2[i].im;
    cfTemp2[i].re = (u.re/(cepAmag[i]*cepAmag[i]));
    cfTemp2[i].im = (u.im/(cepAmag[i]*cepAmag[i]));
    modGd[i-1] = cfTemp2[i].re;
  }
return(modGd);
}


/****************************************************************************
*	The following function computes  the ModGdCepstrum from 
*       the group delay function
*
*	inputs : signal - npts
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - ModGd Cepstra
*****************************************************************************
*
*/
float *ModGdCepstrumFromGD(float *modGd,int nfft,
	     int winlen, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex, float scaleDCT) {
  static float		     *ax, *ay;
  static int                 flag = 0, nfBy2;
  int		             i;
  int                        sign;
  float                      abscfTemp, logcfTemp;
  static VECTOR_OF_F_VECTORS *discreteCosineTransform;
  static F_VECTOR            *fvect, *modGdFvect;

  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    discreteCosineTransform = (VECTOR_OF_F_VECTORS *) generate_pseudo_dct(1, winlen, nfBy2);
    fvect = alloc_f_vector(nfBy2);
    modGdFvect = alloc_f_vector(winlen);
    flag = 1;
  }

  /* RemoveAverage(modGd,nfft,&Ave); */


  /* Reduce dynamic range of modGd if necessary */
    for (i = 0; i < nfBy2; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }   
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }

    if ((startIndex > 0) || (endIndex < nfBy2-1)) {
      for (i = 1; i < startIndex; i++)  
	modGd[i] = 0.0;
      for (i = endIndex+1; i <= nfBy2; i++) 
	modGd[i] = 0;
    }
  for (i = 0; i < nfBy2;i++)
    fvect->array[i] = modGd[i];
  linear_transformation_of_f_vector(fvect, discreteCosineTransform, modGdFvect);
  for (i = 0; i < winlen; i++) 
    modGdCepstrum[i] = modGdFvect->array[i]/(float)(i+1)/(float)scaleDCT;
  return(modGdCepstrum);  
}


/****************************************************************************
*	The following function computes  the ModGdCepstrum from 
*       the Standard modgroupdelay cepstra
*
*	inputs : signal - winlen
*	mfft : fft stages nfft = 2**mfft
*	winlen : window length for zero spectrum and smoothing
*
*	Outputs :
*	modGd(nfft) - ModGd Cepstra
*****************************************************************************
*
*/
float *ModGdCepstrumFromStdCepstra(float *inModGdCepstrum,int nfft, int mfft, 
	     int winlen, float *modGdCepstrum, float alfaP, float alfaN,  
		     float gamma, int gdsign, int removeLPhase, int startIndex, int endIndex, float scaleDCT) {
  static float		     *ax, *ay, *modGdTemp, *modGd;
  static int                 flag = 0, nfBy2;
  int		             i;
  int                        sign;
  float                      abscfTemp, logcfTemp;
  static VECTOR_OF_F_VECTORS *discreteCosineTransform;
  static F_VECTOR            *fvect, *modGdFvect;

  if (flag == 0) {
    nfBy2 = nfft/2;
    ax = (float *) calloc(nfft+1, sizeof(float));
    ay = (float *) calloc(nfft+1, sizeof(float));
    discreteCosineTransform = (VECTOR_OF_F_VECTORS *) generate_pseudo_dct(1, winlen, nfBy2);
    modGd = (float *) calloc(nfft+1, sizeof(float));
    modGdTemp = (float *) calloc(nfft+1, sizeof(float));
    fvect = alloc_f_vector(nfBy2);
    modGdFvect = alloc_f_vector(winlen);
    
    flag = 1;
  }

  ax[1] = inModGdCepstrum[0];
  for (i = 2; i <= winlen+1; i++) {
    ax[i] = inModGdCepstrum[i-1];
    ax[nfft-i+2] = ax[i];
  }
  for (i = winlen+2; i <= nfft-(winlen); i++)
    ax[i] = 0.0;
  Rfft(ax,modGdTemp,ay,mfft,nfft,-1);
  for (i = 0; i < nfBy2; i++)
    modGd[i] = modGdTemp[i+1];

  /* RemoveAverage(modGd,nfft,&Ave); */

  /* Reduce dynamic range of modGd if necessary */
    for (i = 0; i < nfBy2; i++) {
      if (modGd[i] < 0) 
	sign = -1;
      else
	sign = 1;
      abscfTemp = fabs(modGd[i]);
      if (abscfTemp == 0)
	modGd[i] = 0;
      else {
	logcfTemp = log(abscfTemp);
	if ((alfaN == 0) && (alfaP == 0))
	  if (gdsign == 1)
	    modGd[i] = sign*logcfTemp;
	  else
	    modGd[i] = logcfTemp;
	else {
	  if (sign > 0.0)
	    modGd[i] = exp(alfaP*logcfTemp);
	  else 
	    modGd[i] = exp(alfaN*logcfTemp);
	  if (gdsign == 1)
	    modGd[i] = sign*modGd[i];
	}
      }
    }   
    /* Bandlimit modGd */
    if (removeLPhase) {
      for (i = 2; i < nfBy2; i++) {
	modGd[i] = modGd[i] - modGd[1];
	modGd[nfft - i +2] = modGd[i];
      }
      modGd[1] = 0;
    }

    if ((startIndex > 0) || (endIndex < nfBy2-1)) {
      for (i = 1; i < startIndex; i++)  
	modGd[i] = 0.0;
      for (i = endIndex+1; i <= nfBy2; i++) 
	modGd[i] = 0;
    }
  for (i = 0; i < nfBy2;i++)
    fvect->array[i] = modGd[i];
  linear_transformation_of_f_vector(fvect, discreteCosineTransform, modGdFvect);
  for (i = 0; i < winlen; i++) 
    modGdCepstrum[i] = modGdFvect->array[i]/(float)(i+1)/(float)scaleDCT;
  return(modGdCepstrum);  
}




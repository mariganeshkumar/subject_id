/*
 * =====================================================================================
 *
 *       Filename:  DiscAdapt.h
 *
 *    Description:  Header file for DiscAdapt.c 
 *
 *
 * =====================================================================================
 */

#ifndef __DISCGMM_DISCADAPT__
#define __DISCGMM_DISCADAPT__

#include "CommonFunctions.h"
#include<limits.h>

VFV* 
DiscAdaptOnlyMeans (VFV *ubmMeans, VFV *ubmVars, float *ubmWts,
                       uint numClusters, 
                       VFV *fvec, uint nvec, 
                       VFV *negfvec, uint negnvec, 
                       float rfac, float nper);

#endif

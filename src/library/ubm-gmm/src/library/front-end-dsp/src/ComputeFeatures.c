#include "fe/CommonFunctions.h"
#define UCHAR unsigned char
#define UCAHRP unsigned char*

main (int argc, char *argv[])
{

  char *controlFileName, *featureName, *ergSpidModelFileName, *wavName;
  char *outputFileName;
  FILE *controlFile, *ergSpidModelFile;
  FILE *outputFile;
  VECTOR_OF_F_VECTORS *newVfv;
  VECTOR_OF_F_VECTORS *deltaVfv = NULL, *deltaDeltaVfv = NULL;
  VECTOR_OF_F_VECTORS *concatDeltaVfv, *concatAcclVfv;
  ASDF *asdf;
  unsigned long numVectors;
  unsigned int featLength, i, j, k;
  unsigned int velocity = 0, acceleration = 0;
  unsigned int deltaDifference, deltaDeltaDifference;
  int numGaussiansErg = 3, numSpeechVectors, writeAsBin = 0;
  char channel;
  float probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  float thresholdScale;

  if (argc < 7) {
      printf ("Usage : %s controlFile wavName featureName outputFile "
              "thresholdScale channel [writeAsBin]\n", argv[0]);
      exit (0);
  }

  controlFileName = argv[1];
  wavName = argv[2];
  featureName = argv[3];
  outputFileName = argv[4];
  sscanf (argv[5], "%f", &thresholdScale);
  if (set_select_channel(argv[6][0]) != 0) return 1;
  if (argc == 8) 
    sscanf (argv[7], "%d", &writeAsBin);

  

  controlFile = fopen (controlFileName, "r");
  asdf = (ASDF *) calloc (1, sizeof (ASDF));
  InitializeStandardFrontEnd (asdf, controlFile);
  Cstore (asdf->fftSize);

  printf ("Threshold is %f\n", thresholdScale);
  newVfv = ExtractFeatureVectors (asdf, wavName,
				     featureName, &numVectors, thresholdScale);

  if (!writeAsBin) {
        WriteVfvToFile (newVfv, numVectors, outputFileName);      
  }
  else {
        WriteVfvToBinFile (newVfv, numVectors, outputFileName);
  }  
  FreeVfv (newVfv, numVectors);
}

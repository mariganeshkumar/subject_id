# Prepares data matrix given a list of supervectors
# the matrix is stored in ascii format. First line 
# contains the number of cols and rows. This is
# followed by nrows lines each containing the 
# row of the matrix

import GMM as GMM
import os
from tables import *
from svecdef import *
import numpy as np


optlist, args = getopt.getopt (sys.argv[1:], "i:o:h")
helpmsg  = "Usage: ./prepdatamat.py -i listOfSVec -o matname -h help"

temppytablename = "temp.hdf5"

def printhelp (close = 0):
  print helpmsg
  if close == 1:
    quit ()

ipname, opname = "", ""

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-i":
      ipname = value
    elif option == "-o":
      opname = value
    else:
      printhelp (1)

if ipname == "" or opname == "":
  printhelp (1)
ipf = open (ipname, "r")
iplist = [ ln.strip () for ln in ipf ]
ipf.close ()

ncols = len (iplist)
if ncols == 0:
  print "empty list file"
  quit ()


# get M and D values
temp_gmm= GMM.ReadGMM (iplist[0])
M,D = temp_gmm.GetNumMix (), temp_gmm.GetDim ()
csize = M * D

# write the supervectors in a hdf5 file
# temporarily. here each row contains
# 
tempPyTab, mtab, dtab = hdf5open (temppytablename, M, D)
row = mtab.row 
for i in range (0, ncols):
# read the mean supervector
  if i == 0:
    svec = temp_gmm.GetMeans ().reshape ((csize,))
  else:
    svec = GMM.ReadGMM (iplist[i]).GetMeans (). reshape ((csize,))
  row['colvec'] = svec
  row.append ()
mtab.flush ()

row = dtab.row 
row['dim'] = [ncols, csize]
row.append ()
dtab.flush ()

tempPyTab.close ()

# Once a temp file has been written, read it back.
# Save it a data matrix with column vectors as 
# data

tempPyTab = openFile (temppytablename, mode = "r")
ophnd, mtab, dtab = hdf5open (opname, ncols, 1)
# to write each row
row = mtab.row
step=1000
svec = np.zeros ((ncols,step))
jumps = range (0, csize, step)
jlen = len (jumps)
for i in jumps:
# writing "step" rows to the table
  start  = i
  end = i + step
  if end >= csize:
    end = csize-1
  print "%d %d" % (start, end)
  wnd = end - start
  j = 0
  for oldrow in tempPyTab.root.matrix.iterrows ():
    tempvec = oldrow['colvec'][start:end]
    svec[j,0:wnd] = tempvec
    j+1
  for k in range (0,wnd):
    row['colvec'] = svec[:,k]
    row.append ()  
mtab.flush ()

row = dtab.row 
row['dim'] = [csize, ncols]
row.append ()
dtab.flush ()

ophnd.close ()

os.remove (temppytablename)

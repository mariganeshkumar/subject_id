% combile all sub Ax matrices

function computeA_red (amatlist, amatname, exflag, M, R)
  al = readlistascell (amatlist);
  ll = length (al);

  amatname
    
  for m=1:M
      Afl = zeros (R,R);
      for i = 1:ll
        load (strcat (al,'.',num2str(m)), '-mat');
        Afl = Afl + Ax;
      end      
      Ax = Afl;

      newstr = strcat(amatname,'.',num2str(m));
      save(newstr, '-mat','Ax');
  end
  if exflag == 1
    exit
  end

# performs test utterance. a readable replacement to test2004.db

import sys
import numpy as np
import getopt
import os 
sys.path.append('/home/srikanth/SpeakerID/src/ivec')
from Utility import *

def print_help():
    print("\n".join(["python testutterance_map.py [options] " +
                     "SpidModels testlist ubm cval",
                     "Options are",
                     "h: print this help",
                     "b: feature is binary",
                     "r val: val is result folder",
                     "t : perform tnorm",
                     "--start: start tests at this index",
                     "--end: end tests at this index",
                     "--debug : increase verbosity",                     
                     "--true-claims : true claims file",
                     "--imp-claims : impostor claims file",
                     "--tnorm-start : start tnorm at this index",
                     "--tnorm-end : end tnorm at this index"]))

optlist, args = getopt.getopt(sys.argv[1:], "tr:bh", 
                              ["start=","end=", "debug", "tnorm-start=",
                               "tnorm-end=", "true-claims=", "imp-claims="])

try:
    (spid_models, testlist, ubm_filename, cval) = args
    cval = int(cval)
except:
    print("need more mandatory arguments")
    print_help()
    print args
    quit(2)

(true_claims, imp_claims, resultdir, feat_is_bin) = (None, None, "./", 0)
(start, end, verbose, do_tnorm) = (-1, -1, False, False)
(tnorm_start, tnorm_end) = (-1, -1) 
read_feat = read_ascii_vfv
 
def read_claims_file(filename):
    claims_dict = {}
    for ln in open(filename):
      lns = ln.strip().split()
      tc = lns[0]
      indices = [ int(x)-1 for x in lns[1:] ]
      claims_dict[tc] = indices
    return claims_dict
      
for opt,val in optlist:
    if opt == "--true-claims":        
        true_claims = read_claims_file(val)
    elif opt == "--imp-claims":
        imp_claims = read_claims_file(val)
    elif opt == "-r":
        resultdir = val
        try:
            os.mkdir(val)
        except OSError:
            print("Unable to make ", resultdir, "\n Exiting")
        except:
            print("Unable to make ", resultdir, "\n Exiting")
            quit(1)
    elif opt == "-b":
        feat_is_bin = 1
        read_feat = read_bin_file
    elif opt == "-t":
        do_tnorm = True
    elif opt == "-h":        
        print_help()
        quit(0)
    elif opt == "--debug":
        verbose = True
    elif opt == "--start":
        try:
            start = int(val)
        except:
            print ("Invalid input for start")
            quit(2)
    elif opt == "--end":
        try:
            end = int(val)
        except:
            print("Invalid input for end")
            quit(2)        
    elif opt == "--tnorm-start":
        tnorm_start = int(val)
    elif opt == "--tnorm-end":
        tnorm_end = int(val)
    else:
        print("Invalid option.")
        print_help()
        quit(2)
try:
    (nmix, dim, wts, mvec, vvec) = ReadGMM(ubm_filename)  
except:
    print("Unable to read UBM")
    quit(3)

spkrlist = []
for ln in open(spid_models):
      try:
        model = ReadGMM(ln.strip().split()[0])[3]
      except:
        model = mvec
      spkrlist.append(model)

if verbose:
    print("Finished reading all models")

cval_indices = range(nmix-cval,nmix)
spkr_idx = range(0, len(spkrlist))
if tnorm_start == -1 or tnorm_end == -1:  
    if verbose:
        print("Warning tnorm start or end not set")
    tnorm_start = len(spkrlist)-251
    tnorm_end = len(spkr_idx)-1
tnorm_spkrs = range(tnorm_start, min(tnorm_end+1,len(spkrlist)))

for idx, testcase in enumerate(open(testlist),1):
    testcase = testcase.strip()
    if start != -1 and idx < start:
        continue
    if end != -1 and idx > end:
        break

    vfv = read_feat(testcase)
    testname = '.'.join(testcase.split('/')[-1].split('.')[0:2])
    # get topc
    (nvec, dim) = np.shape(vfv)
    
    opf = open(resultdir + "/" + str(idx) + '.out', 'w')
    if true_claims != None:
        try:
            spkr_idx = true_claims[testname]
            spkr_idx += imp_claims[testname]
            spkr_idx += tnorm_spkrs
        except KeyError:
            continue
    

    distortion = np.zeros((len(spkr_idx),))
    for i in xrange(0,nvec):
        fvec = -0.5 * (vfv[i] - mvec) ** 2 / vvec 
        dist = np.sum (fvec,axis=1) 
        indices = np.argsort(dist)[cval_indices]
        for j,spkr in enumerate(spkr_idx):
            distortion[j] += np.sum(-0.5 * pow(vfv[i] - spkrlist[spkr][indices,:],2) / vvec[indices,:])
        
    if do_tnorm:
        tc_len = len(tnorm_spkrs)
        if tc_len > 0:
            m = np.mean(distortion[-1:-1-tc_len:-1])
            v = np.std(distortion[-1:-1-tc_len:-1])
            distortion = (distortion - m)/v
    for d in distortion:
        opf.write("%e\n" % (d/nvec))

    opf.close()            
        

/*
    This file does GMM adaptation using a set of feature vectors

    Copyright (C) 2009-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "AdaptGMM.h"

VFV* AdaptOnlyMeans (VFV *ubmMeans, 
                     VFV *ubmVars,
                     float *ubmWeights,
                     unsigned int numClusters,
                     VFV *vfv,
                     unsigned long numVectors,
                     float relevanceFactor,
                     float probScaleFactor)
{
    VFV	*tempMeans;
    float	alpha, beta, normFactorI, maxProb, logProb, *eeta;
    unsigned int dimensions, i, j, k, maxProbIdx, tempMaxProbIdx;
    VFV *adaptedMeans;

    dimensions = ubmMeans[0]->numElements;

    tempMeans = (VFV *) calloc (numClusters, sizeof(VFV));
    for (j = 0; j < numClusters; j++)
    {
        tempMeans[j] = AllocFVector(dimensions);
        InitFVector (tempMeans[j]);
    }

    // eeta is the effective number of examples belonging to a cluster
    // In this implementation, an example is assumed to belong to only
    // one cluster -- hard assignment. soft assignment did not improve 
    // the results

    eeta = (float *) malloc (numClusters*sizeof(float));
    for (j=0;j<numClusters;j++) eeta[j] = 0.;
    for (i=0; i<numVectors; i++)
    {
        for (j=0; j<numClusters; j++)
        {
            logProb = ComputeProbability(ubmMeans[j], 
                                         ubmVars[j], 
                                         ubmWeights[j], 
                                         vfv[i], 
                                         probScaleFactor);
            if (j==0 || logProb>maxProb)
            { 
                maxProb= logProb; 
                maxProbIdx= j; 
            }

        }

        eeta[maxProbIdx]+=1.0;
        for (k = 0; k < dimensions; k++)
            tempMeans[maxProbIdx]->array[k] += vfv[i]->array[k];	
      
    }


    for (j = 0; j < numClusters; j++)
        for (k = 0; k < dimensions; k++) 
        { 
            if (eeta[j] != 0) 
                tempMeans[j]->array[k] /= eeta[j];
        }
      
    adaptedMeans = (VFV *) calloc (numClusters, sizeof (VFV));
    for (j = 0; j < numClusters; j++)
    {
        adaptedMeans[j] = (F_VECTOR *) AllocFVector(dimensions);
        
        alpha = (eeta[j] / (eeta[j] + relevanceFactor));
        for (k = 0; k < dimensions; k++)
            adaptedMeans[j]->array[k] = (alpha * tempMeans[j]->array[k])
                                      + ((1.0 - alpha) *ubmMeans[j]->array[k]);
    }
    free (eeta);
    return adaptedMeans;
}

void
printHelp () {
      printf ("Usage : adapt ubmFile ubmSize featureFile outputModelFile "
              "relevanceFactor \n");
      return;
}

main (int argc, char *argv[])
{
  char *ubmFileName, *modelFileName,
       *trainFileName, *featureFileName;
  FILE *ubmFile, *modelFile, *ergSpidModelFile;
  VFV *vfv, *ubmMeans, *ubmVars, *adaptedMeans; 
  F_VECTOR *ubmWeights;
  ASDF *asdf;
  unsigned int numVectors;
  unsigned int ubmSize, featLength, i, j, k;
  int oc;
  F_VECTOR *meanVector, *varVector;
  int numGaussiansErg = 3, numSpeechVectors, isDump = 0;
  int silenceMixtureNumber, speechMixtureNumber, noiseMixtureNumber;
  int VQIter, GMMIter;
  float probScaleFactor = 1.0, varianceNormalize = 0, varianceFloor = 1E-5, 
        relevanceFactor = 16.;

  while((oc = getopt (argc,argv,":bh")) != -1) {
      switch (oc) {
          case 'b' :
            isDump = 1;
            break;
          case 'h' :
            printHelp ();
            exit(0);
            break;
      }
  }

  argv = &argv[optind-1];
  if (argc - optind < 5) {
      printHelp ();
      exit(1);
  }
  ubmFileName = argv[1];
  sscanf (argv[2], "%u", &ubmSize);
  featureFileName = argv[3];
  modelFileName = argv[4];
  sscanf (argv[5], "%f", &relevanceFactor);

  if (!isDump)
      vfv = ReadVfvFromFile (featureFileName, &numVectors);
  else
      vfv = ReadVfvFromBinFile (featureFileName, &numVectors);

  featLength = vfv[0]->numElements;
  ubmMeans = (VFV *) calloc (ubmSize, sizeof (VFV));
  ubmVars = (VFV *) calloc (ubmSize, sizeof (VFV));

  ubmWeights = (F_VECTOR *) AllocFVector (ubmSize);
  ubmFile = fopen (ubmFileName, "r");
  for (i = 0; i < ubmSize; i++)
  {
      ubmMeans[i] =  AllocFVector (vfv[0]->numElements);
      ubmVars[i] = AllocFVector (vfv[0]->numElements);
      fscanf (ubmFile, "%e\n", &ubmWeights->array[i]);
      for (j = 0; j < featLength; j++)
      {
          fscanf (ubmFile, " %e %e", &ubmMeans[i]->array[j],
                                     &ubmVars[i]->array[j]);
      }
      fscanf (ubmFile, "\n");
  }
  fclose (ubmFile);

  adaptedMeans = AdaptOnlyMeans(ubmMeans, ubmVars,
                                ubmWeights->array, ubmSize,
                                vfv, numVectors, 
                                relevanceFactor,
                                probScaleFactor);

  modelFile = fopen (modelFileName, "w");
  for (i = 0; i < ubmSize; i++)
  {
      fprintf (modelFile, "%e\n", ubmWeights->array[i]);
      for (j = 0; j < featLength; j++)
          fprintf (modelFile, " %e %e", adaptedMeans[i]->array[j],
                                        ubmVars[i]->array[j]);
      
      fprintf (modelFile, "\n");
  }
  fclose (modelFile);

  FreeVfv (ubmMeans, ubmSize);
  FreeVfv (ubmVars, ubmSize);
  FreeFVector (ubmWeights);

}				// end main()

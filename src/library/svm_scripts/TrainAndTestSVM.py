
import argparse
from tqdm import tqdm
import os
import shutil
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

import sys
sys.path.insert(0, 'library/svm_scripts')
from libsvmUtil import *

parser = argparse.ArgumentParser()
parser.add_argument("--trainDataScriptFile", help="Location of script file for train data.",
                    type=str, )
parser.add_argument("--testDataScriptFile", help="Location of script file for test files.",
                    type=str, )

parser.add_argument("--valDataScriptFile", help="Location of script file for script files.",
                    type=str, )
parser.add_argument("--testResultFolder", help="Location of the folder to write test result.",
                    type=str, )
parser.add_argument("--valResultFolder", help="Location of the folder to write test result.",
                    type=str, )


args = parser.parse_args()
tmpDir = "tmp/svd"

if os.path.exists(tmpDir):
    shutil.rmtree(tmpDir)
os.makedirs(tmpDir)


trainSuperVectorMatrixFile = tmpDir + '/TrainSuperVectorMatrix.txt'
testSuperVectorMatrixFile = tmpDir + '/testSuperVectorMatrix.txt'
valSuperVectorMatrixFile = tmpDir + '/valSuperVectorMatrix.txt'

trainSuperVectorMatrix = []
testSuperVectorMatrix = []
valSuperVectorMatrix = []

trainLabelFile = tmpDir + '/trainLabels.txt'
testLabelFile = tmpDir + '/testLabels.txt'
valLabelFile = tmpDir + '/valLabels.txt'


with open(args.trainDataScriptFile) as f:
    trainDataScript = f.readlines()
trainDataScript = [x.strip() for x in trainDataScript]
trainFiles = [x.split(' ')[0] for x in trainDataScript]
trainLabel = [int(x.split(' ')[1]) for x in trainDataScript]

for i in tqdm(range(len(trainFiles))):
    filename = trainFiles[i]
    iVec = np.genfromtxt(filename);
    trainSuperVectorMatrix.append(iVec)


with open(args.valDataScriptFile) as f:
    valDataScript = f.readlines()
valDataScript = [x.strip() for x in valDataScript]
valFiles = [x.split(' ')[0] for x in valDataScript]
valLabel = [int(x.split(' ')[1]) for x in valDataScript]

for i in tqdm(range(len(valFiles))):
    filename = valFiles[i]
    iVec = np.genfromtxt(filename);
    valSuperVectorMatrix.append(iVec)


with open(args.testDataScriptFile) as f:
    testDataScript = f.readlines()
testDataScript = [x.strip() for x in testDataScript]
testFiles = [x.split(' ')[0] for x in testDataScript]
testLabel = [int(x.split(' ')[1]) for x in testDataScript]

for i in tqdm(range(len(testFiles))):
    filename = testFiles[i]
    iVec = np.genfromtxt(filename);
    testSuperVectorMatrix.append(iVec)


trainSuperVectorMatrix = np.array(trainSuperVectorMatrix)
valSuperVectorMatrix = np.array(valSuperVectorMatrix)
testSuperVectorMatrix = np.array(testSuperVectorMatrix)


clf = LinearDiscriminantAnalysis(solver='svd')
clf.fit_transform(trainSuperVectorMatrix, trainLabel)
trainSuperVectorMatrix = clf.transform(trainSuperVectorMatrix)
valSuperVectorMatrix = clf.transform(valSuperVectorMatrix)
testSuperVectorMatrix = clf.transform(testSuperVectorMatrix)

#evLabel = np.array(devLabel)
trainLabel = np.array(trainLabel)
valLabel = np.array(valLabel)
testLabel = np.array(testLabel)

np.savetxt(trainSuperVectorMatrixFile, trainSuperVectorMatrix, fmt="%f", footer='\n', comments='')
np.savetxt(valSuperVectorMatrixFile, valSuperVectorMatrix, fmt="%f", footer='\n', comments='')
np.savetxt(testSuperVectorMatrixFile, testSuperVectorMatrix, fmt="%f", footer='\n', comments='')

np.savetxt(trainLabelFile, trainLabel, fmt="%d", footer='\n', comments='')
np.savetxt(testLabelFile, valLabel, fmt="%d", footer='\n', comments='')
np.savetxt(testLabelFile, testLabel, fmt="%d", footer='\n', comments='')


print("Computing Cosine Kernal")
TrainDataKernel = cosine_similarity(trainSuperVectorMatrix,trainSuperVectorMatrix )
ValDataKernel = cosine_similarity(valSuperVectorMatrix,trainSuperVectorMatrix)
TestDataKernel = cosine_similarity(testSuperVectorMatrix,trainSuperVectorMatrix)

# Changing kernal to LIBSVM format
trainIndex = np.expand_dims(np.array(range(1, TrainDataKernel.shape[0] + 1)), axis=1)
valIndex = np.expand_dims(np.array(range(1, ValDataKernel.shape[0] + 1)), axis=1)
testIndex = np.expand_dims(np.array(range(1, TestDataKernel.shape[0] + 1)), axis=1)

TrainDataKernel = np.concatenate((trainIndex, TrainDataKernel), axis=1)
valDataKernel = np.concatenate((valIndex, ValDataKernel), axis=1)
TestDataKernel = np.concatenate((testIndex, TestDataKernel), axis=1)

print("LIBSVM SVM Training and testing")
model = ovotrain(trainLabel, TrainDataKernel, '-s 0  -t 4 -q')
pred, ac, _ = ovopredict(testLabel, TestDataKernel, model)
print(ac)
pred = pred.tolist()

for i in range(len(pred)):
    filename = args.testResultFolder + str(i+1) + '.out'
    with open(filename, 'w') as f:
        f.write("label "+ str(pred[i]) + " \n")
        f.close()

pred, ac, _ = ovopredict(valLabel, valDataKernel, model)

pred = pred.tolist()
print(ac)
for i in range(len(pred)):
    filename = args.valResultFolder + str(i+1) + '.out'
    with open(filename, 'w') as f:
        f.write("label "+ str(pred[i]) + " \n")
        f.close()
/********************************************************************
 * This function is a variant of NewWeightedGMM.c - it will take
 * only closest negative data assigned to that cluster. That is,
 * the algo will look for, say, 10 -25 % quantile for that cluster
 * wrt the negative data.
*********************************************************************/





#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "InitAsdf.h"
#include "DspLibrary.h"
#include "stdlib.h"
#include "math.h"
#include "GMM.h"
#include "VQ_Modified.h"



void
NewWeightedGMMTopN_Mean (VECTOR_OF_F_VECTORS * vfv,
		int numVectors,
		VECTOR_OF_F_VECTORS * mixtureMeans,
		VECTOR_OF_F_VECTORS * mixtureVars,
		float *mixtureElemCnt,
		int numMixtures,
		int VQIter,
		int GMMIter,
		float probScaleFactor,
		int ditherMean,
		float varianceNormalize,
		int seed,
		VECTOR_OF_F_VECTORS * negData,
		int negNumVectors, float npercent)
{
  int i, j, k;
  static VECTOR_OF_F_VECTORS *tempMeans, *tempVars, *tempNegMeans,
    *tempNegVars;
  static float *tempMixtureElemCnt;
  float *tempMixtureNegElemCnt;
  int mixtureNumber;
  int *mixtureNegNumberCache, *mixtureNumberCache;
  int *topNVal;
  int featLength;
  float *alphaVector, *maxDistortion;
  float beta, tempNk, minBeta, errThresh, pminBeta = 1.0;
	float maxllp = 1.0, maxlln = 1.0, minllp = 1.0, minlln = 1.0;
	float neglogLklihood, pneglogLklihood = 1.0;
	float offset = numMixtures, itratio;
  float tempDistortion, logLklihood, plogLklihood = 1.0;
	
  float alpha;
  int neglectedVectorsCnt = 0, numNegPtsUsed = 0;


	// errThreshold depends on the number of vectors. We assume that
  // numVectors is large. So allow, a 1000 unit deviation in 
  // distortion value. This is divided by numVectors to get a 
  // stopping criterion
  errThresh = 2000.0 / (float) numVectors;
  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures,
					      sizeof (VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures,
					     sizeof (VECTOR_OF_F_VECTORS));
  for (i = 0; i < numMixtures; i++)
    {
      tempMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      tempVars[i] = (F_VECTOR *) AllocFVector (featLength);
    }
  tempMixtureElemCnt = (float *) AllocFloatArray (tempMixtureElemCnt,
						  numMixtures);
  printf ("temp mixtures allocated\n");
  fflush (stdout);

/* ComputeVQ (vfv, numVectors, mixtureMeans, mixtureVars,
	     mixtureElemCnt, numMixtures, varianceNormalize,
	     ditherMean, VQIter, seed);
  if (GMMIter == 0)
    return;*/

  ComputeGMM(vfv, numVectors, mixtureMeans, mixtureVars,
	     mixtureElemCnt ,numMixtures,VQIter, 2, probScaleFactor,
             ditherMean, varianceNormalize, seed);
  alphaVector = (float *) calloc (numMixtures, sizeof (float));
	printf ("\n");
	for (i = 0; i< numMixtures; i++) {
					printf ("%e ",mixtureElemCnt[i]);
			}
	printf ("\n");

  /* right now, all elements in alphaVector are equal,
   * but there is a scope for make them different
   * based on the effective number of positive (and negative
   * samples?) in a mixutre
   * UPDATE: this is only init'n. later on, alphaVector elmts
   *         are different for diff mixtures based on
   *         the lower bound discussed in the paper
   * UPDATE: actually, this vector is not used at all

   */

  for (i = 0; i < numMixtures; i++)
    alphaVector[i] = alpha;

  /* this is a very poor way to cache the mixture id for each sample
   */

  mixtureNumberCache = (int *) calloc (numVectors, sizeof (int));
  mixtureNegNumberCache = (int *) calloc (negNumVectors, sizeof (int));
  // the purpose of topNVal array is to hold, for each iteration, the actual
  // value of negative elements to be considered for the mixture referenced
  // by the index of the array
  topNVal = (int *) calloc (numMixtures, sizeof(int));
  // maxDistortion was in the intial implementation for a diff reason.
  // Here it is used to filter out unnecessary neg vectors even though
  // we could have only topN of them
  maxDistortion = (float *) calloc (numMixtures, sizeof(float));
  if (topNVal == NULL || maxDistortion == NULL)
    {
	   fprintf (stderr, "Unable to allocate vector for topNVal\n");
		return;
	 }

  // setting maxDistortion values for each mixture to 1.0 as we
  // know that only log probs are being calculated and the actual
  // vals cannot be >0.0, This way any positive value would mean
  // that the cell is uninitialized
  for (i = 0; i < numMixtures; i++)
    maxDistortion[i] = 1.0;

  // Training starts here. It would run for GMMIter iterations.
  // In each iteration, a set computations to find out
  // alpha value is computed and max of all of the is taken.
  // minBeta used in this loop actually refers to 1-alpha/alpha
  // expression's min value which corresponds to the max alpha.
  // From the proof of convergence, it would become clear, why
  // max alpha is required. The sooner alpha reaches 1, sooner
  // the algorithm converges.
  
  for (k = 0; k < GMMIter; k++)
    {
      printf (" GMM iteration number = %d\n", k);
      fflush (stdout);
		/* Initialize all values to zero*/
      for (i = 0; i < numMixtures; i++)
	     {
		    InitFVector (tempMeans[i]);
			 InitFVector (tempVars[i]);
	   	 tempMixtureElemCnt[i] = 0;
		  }

      /* *******************************************************
		 *
		 * For each positive example, find the closest mixture,
       * accumulate statistics for that mixture
		 * 
       ********************************************************/
      logLklihood = 0.0;
      for (i = 0; i < numVectors; i++)
	{
	  mixtureNumber = DecideWhichMixture (vfv[i], mixtureMeans,
					      mixtureVars, numMixtures,
					      mixtureElemCnt, numVectors,
					      probScaleFactor);

		/***************************************************
		 *
		 * 	the next few lines tries to add a distortion 
		 * 	threshold for data selection
		 *
 		 **************************************************/
	  tempDistortion = ComputeProbability (mixtureMeans[mixtureNumber],
					       mixtureVars[mixtureNumber],
					       mixtureElemCnt[mixtureNumber]
					       / numVectors,
					       vfv[i], probScaleFactor);	  							 
	  logLklihood   += tempDistortion;
	/*	if (i == 0)
		{
						maxllp = tempDistortion;
						minllp = tempDistortion;
		}
		else
		{
						if (maxllp < tempDistortion)
										maxllp = tempDistortion;
						else if (minllp > tempDistortion)
										minllp = tempDistortion;
		}*/

	  if (maxDistortion[mixtureNumber] > 1.0 || 
	      maxDistortion[mixtureNumber] > tempDistortion)
	  		maxDistortion[mixtureNumber] = tempDistortion;	  

	  mixtureNumberCache[i] = mixtureNumber;
	  tempMixtureElemCnt[mixtureNumber] =
	    tempMixtureElemCnt[mixtureNumber] + 1;
	  for (j = 0; j < featLength; j++)
	    {
	      tempMeans[mixtureNumber]->array[j] =
		tempMeans[mixtureNumber]->array[j] + (vfv[i]->array[j]);
	    }
	}
	// normalizing logLklihood by numVectors
	logLklihood = logLklihood/numVectors;
	printf ("ll = %f while pll = %f\n", logLklihood, plogLklihood);
	// increase iterations if likelihood decrease
	if ( fabs(logLklihood - plogLklihood) >= errThresh && plogLklihood <= 0.0)
	  {
						printf ("The log likelihood just decreased. Increasing no.of itns. %f\n",errThresh);
					GMMIter++;
		}
		// set the number of negative elements to conside for each 
		// mixture.
		for (i = 0; i < numMixtures; i++)
		  {
		    topNVal[i] = (int) ceil(npercent * ((float) tempMixtureElemCnt[i]) / 100.0);
			 maxDistortion[i] *= 0.5;
		  }
		  
      tempMixtureNegElemCnt = (float *) calloc (numMixtures, sizeof (float));
      neglectedVectorsCnt = 0;
		neglogLklihood = 0.0;
      numNegPtsUsed = 0;
      for (i = 0; i < negNumVectors && numNegPtsUsed < npercent * numVectors; i++)
	{

	  /* this is a new addition (22/12/10) to the logic.
	   * here I think it'll be beneficial to check
	   * during the 2nd or further iterations if the
	   * sample has been previously discarded. If so, I think
	   * it should be ignored
	   */
	  if (k && mixtureNegNumberCache[i] == -1)
	    continue;

	  mixtureNumber = DecideWhichMixture (negData[i], mixtureMeans,
					      mixtureVars, numMixtures,
					      mixtureElemCnt, numVectors,
					      probScaleFactor);
	 // This call is a bit redundant. Try to define a new function to
	 // alias DecideWhichMixture which would also take in a ptr to
	 // some storage where tempDistortion could be stored and returned
	  tempDistortion =
	    ComputeProbability (mixtureMeans[mixtureNumber],
				mixtureVars[mixtureNumber],
				mixtureElemCnt[mixtureNumber] / numVectors,
				negData[i], probScaleFactor);

	  // if the closest mixture is full, ignore
	  // or if, its distance is too far, say mean+/-2sigma ignore
	  // else include it and update the value of the most distant
	  // vector
	  if (tempMixtureNegElemCnt[mixtureNumber] >= topNVal[mixtureNumber]
	    || tempDistortion >= maxDistortion[mixtureNumber])
	    {
		   
	      mixtureNegNumberCache[i] = -1;
	      neglectedVectorsCnt++;
		 }
	  else 
	    {
	      tempMixtureNegElemCnt[mixtureNumber]++;
	      ++numNegPtsUsed;
	      mixtureNegNumberCache[i] = mixtureNumber;
			/*	if (numNegPtsUsed == 1)
				{
								minlln = tempDistortion;
								maxlln = tempDistortion;
				}
				else
				{
								if (maxlln < tempDistortion)
												maxlln = tempDistortion;
								else if (minlln > tempDistortion)
												minlln = tempDistortion;								
				}*/
		
			neglogLklihood += tempDistortion;

	    }
	}

   neglogLklihood = neglogLklihood / numNegPtsUsed; 

	
	

	printf
	("At iter=%d, neglectedVectorsCnt=%d numNegPtsUsed=%d logLklihood=%f ratio=%f\n",
	 k + 1, neglectedVectorsCnt, numNegPtsUsed, logLklihood, itratio);
      tempNk = 0.0;
      minBeta = -1.0;
	if (k == 0) {
      for (i = 0; i < numMixtures; i++)
					{
						beta =
							(tempMixtureNegElemCnt[i] +
							 offset) / (tempMixtureElemCnt[i] +
									 tempMixtureNegElemCnt[i]);
						beta = (1.0 - beta) / beta;
						if (beta < 0.0 || tempMixtureElemCnt[i] == 0.0)
								beta = 0.0;

						if (beta > 0.0)
							if (minBeta == -1.0 || minBeta > beta)
								minBeta = beta;
					}
	}
	else {
			printf ("plogLklihood = %f pneglogLklihood = %f ", plogLklihood, pneglogLklihood);
			printf ("logLklihood = %f neglogLklihood = %f\n", logLklihood, neglogLklihood);
			itratio =  
						(plogLklihood + pneglogLklihood) / 
						(logLklihood + neglogLklihood);	
			itratio = itratio / ( 1.0 + pminBeta);
			itratio += ((neglogLklihood - pneglogLklihood) 
						/ (logLklihood + neglogLklihood));
			if (itratio > 1.0) minBeta = 1.0;
			else
				minBeta = itratio + ((1.0 - itratio) / 2.0);
			minBeta = (1.0 - minBeta) / minBeta;
	}
	
	printf ("iteartion %d minBeta %f\n", k+1, minBeta);
      for (i = 0; i < numMixtures; i++)
							tempNk += (minBeta * tempMixtureNegElemCnt[i]);

      for (i = 0; i < negNumVectors; i++)
					{
						mixtureNumber = mixtureNegNumberCache[i];
						if (mixtureNumber == -1 || alphaVector[mixtureNumber] == 0.0)
							continue;
						for (j = 0; j < featLength; j++)
							tempMeans[mixtureNumber]->array[j] -=
								(minBeta * negData[i]->array[j]);
					}

      for (i = 0; i < numMixtures; i++)
					{
						for (j = 0; j < featLength; j++)
							{
								if (tempMixtureElemCnt[i] - tempMixtureNegElemCnt[i] != 0)
										tempMeans[i]->array[j] =
											tempMeans[i]->array[j] / ((tempMixtureElemCnt[i]) -
															(minBeta *
															 tempMixtureNegElemCnt[i]));
												else
										tempMeans[i]->array[j] = mixtureMeans[i]->array[j];
							}
						mixtureElemCnt[i] = tempMixtureElemCnt[i];
					}
      
			if (plogLklihood == 1.0 || 
					fabs (plogLklihood - logLklihood) 
								<= errThresh)
				{
							plogLklihood = logLklihood;			

				}
		
			pneglogLklihood = neglogLklihood;
			pminBeta = minBeta;			
    }
  free (alphaVector);
  free (mixtureNegNumberCache);
  free (mixtureNumberCache);
  free (topNVal);
  free (maxDistortion);
}

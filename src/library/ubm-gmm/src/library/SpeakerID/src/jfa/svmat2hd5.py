################################################################################
##  This channel adaptation code uses numpy & scipy effectively
##  to take a list of model filese to calc eigen channel directions,
##  another file containing a list indicating number of svectors
##  for each speaker. Also give M,D,R as inputs
##  
##  It outputs a MD x R matrix to a file in the argument.
##  
##
##  Reference for implementation: Analysis of feature extraction and
##  channel compensation in a GMM speaker recognition system - Burget, et al
##
################################################################################

import math
import sys
import numpy as np
import scipy as sp
from tables import *

if len(sys.argv) < 5:
  quit()
  
file1 = open (sys.argv[1], 'r') # contains list of supervector files
file2 = open (sys.argv[2], 'r') # contains no.of indices for each spkr
M,D = map(int,sys.argv[3:5])  # for convenience

############# CLASS DEFINITION BEGINS #############
# Description class for hdf5 files
class svector (IsDescription):
  colvec = Float32Col (M*D)
############## END OF CLASS DEFINITION ##############

from GMM import *


idx, snames = [], []

file1_rl = file1.readlines()
l = len (file1_rl)
snames = [rl.strip() for rl in file1_rl]
file1.close()

file2_rl = file2.readlines()
nidx = len (file2_rl)
indices = [ int(rl.strip()) for rl in file2_rl]
total = sum (indices)
file2.close()
# stack it as a whole matrix and save it to a hdf5 file

tabfile = openFile ("svector.h5",mode = "w",title="Base matrix")
cvtable = tabfile.createTable ("/", 'vmatrix', svector, "Cols are supervectors",expectedrows=total)
rtab = cvtable.row
for s in snames:
  gmm = ReadGMM (s)
  rtab['colvec'] = ConvGMMToSV(gmm).transpose()
  rtab.append()

cvtable.flush()

class indexTableLayout(IsDescription):
  start = UInt16Col ()
  stop = UInt16Col ()

idtab = tabfile.createTable ("/", 'indices', indexTableLayout, "IndexTable")
rtab = idtab.row
i = 1
for j in indices:
  rtab['start'] = i
  st = i
  i = i + j - 1
  rtab['stop'] = i
  en = i
  print (st,en)
  i = i + 1
  rtab.append()

idtab.flush()

tabfile.close()
print "File saved"

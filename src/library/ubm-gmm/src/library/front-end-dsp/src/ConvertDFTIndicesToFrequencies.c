#include "stdio.h"
#include "stdlib.h"
void Usage () {
  printf ("ConvertDFTIndicesToFrequencies inFile outFile samplingRate fftOrder numFormants numFrames windowSize\n");
}
main (int argc, char **argv) {
  FILE  *fpin = NULL, *fpout = NULL;
  int   samplingRate;
  int   fftOrder, numFmts, frameNum, numFrames, windowSize;
  float freqPoles[20];
  int   i;
  char  line[256];
  if (argc != 8) {
    Usage();
    exit(-1);
  }
  fpin = fopen (argv[1], "r");
  fpout = fopen (argv[2], "w");
  sscanf (argv[3], "%d", &samplingRate);
  sscanf (argv[4], "%d", &fftOrder);
  sscanf (argv[5], "%d", &numFmts);
  sscanf (argv[6], "%d",  &numFrames);
  sscanf (argv[7], "%d", &windowSize);
  frameNum = 0;
  while (frameNum < numFrames) {
    printf("FrameNum = %d \n", frameNum);
   for (i = 1; i <= numFmts; i++) {
	fscanf(fpin,"%f",&freqPoles[i]); 
	printf("freqPoles %d = %f\n", i, freqPoles[i]);
      }
   lseek((int) fpin,(long) frameNum+1,0);
   fprintf(fpout, "%d ", frameNum*windowSize);
   for (i = 1; i <= numFmts; i++) {
     fprintf(fpout,"%f ",freqPoles[i]/fftOrder*samplingRate); 
     printf("freqPoles %d = %f\n", i, freqPoles[i]);
   }
   fprintf(fpout, "\n");
   frameNum++;    
  }
  fclose(fpin);
  fclose(fpout);
}

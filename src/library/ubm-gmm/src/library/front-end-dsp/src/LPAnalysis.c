#include "stdio.h"
#include <math.h>
#include "malloc.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/SphereInterface.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
/*******************************************************************************/       void Usage() {
           printf("Usage : LPAnalysis ctrlFile dataFile coefFile outputdataFile residualFile gainFile \n");
}
/*****************************************************************************/
int main (int argc, char *argv[])
{ 
        float           *signal=NULL, *residual=NULL;
	int  	        frameSize, frameAdvanceSamples, numSamples,
	                orderP, numFrames;
        float           *coefPole=NULL;
        FILE            *coefFile=NULL, *gainFile=NULL, *residualFile=NULL, *signalFile=NULL;
        FILE            *controlFile=NULL;
        float           gain, rmax;
        int             i, j; 
        F_VECTOR        *waveForm=NULL;
        ASDF            *asdf;

       if (argc != 7) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       coefFile = fopen(argv[3], "w");
       signalFile = fopen(argv[4], "w");
       residualFile = fopen(argv[5], "w");
       gainFile = fopen(argv[6], "w");
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       frameAdvanceSamples = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute (asdf, "numFrames");
       numSamples = (int) GetIAttribute (asdf, "numSamples");
       signal = (float *) AllocFloatArray(signal, frameSize+1);
       residual = (float *) AllocFloatArray(residual, frameSize+1);
       orderP       = (int) GetIAttribute(asdf, "lpOrder");
       coefPole     = (float *)  AllocFloatArray(coefPole, orderP+2);
       rmax = asdf->waveform[ImaxShort0(asdf->waveform, numSamples)];
       for (i = 0; i < numFrames; i++) {
         waveForm = (F_VECTOR *) GsfRead (asdf, i, "frameWaveform");
         for (j = 0; j < frameSize; j++)
           signal[j+1] = waveForm->array[j];
	 LpAnal(signal,residual, frameSize, frameAdvanceSamples, coefPole, orderP, &gain);
       for (j = 1; j <= orderP; j++)
	 fprintf(coefFile, "%f ", coefPole[j]);
       fprintf(coefFile, "\n");
       for (j = 1; j <= frameAdvanceSamples; j++) {
         fprintf(residualFile, "%f\n", residual[j]);
	 fprintf(signalFile, "%f\n", signal[j]/rmax);
       }
       fprintf(gainFile, "%f\n", gain);
       }
       fclose(coefFile);
       fclose(residualFile);
       fclose(gainFile);
       return(0);
}

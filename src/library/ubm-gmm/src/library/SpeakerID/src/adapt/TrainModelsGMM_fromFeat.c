/*-------------------------------------------------------------------------
 *  TrainModelsGMM.c - Trains models for each speaker given a list of files
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:58:37
 *  Last modified:  Wed 07-May-2003 15:52:21 by hema
 *  $Id: TrainModelsGMM.c,v 1.2 2008/03/12 17:49:10 hema Exp $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

#include "CommonFunctions.h"



int main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char			*featFileName;
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv, *oldVfv, *newVfv;
  int                   i,j;
  unsigned int         numVectors, numVectorsNew, numVectorsOld;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  int                   seed;
  if (argc !=  7) {
   printf("\nTrainModelsGMM ctrFile speakerTable codeBookSize");
   printf(" numVQIter numGMMIter outputCodebookFile\n"); 
   printf ("-------------------------------------------------------------------\n"); 
   printf ("The speaker table consists of feature dumps in a particular format:\n");
   printf ("Each file has information about feature dimension and");
   printf ("numVectors in the first line\n Subsequent lines are feature vectors");
   printf ("itself\n");
   printf ("-------------------------------------------------------------------\n");
   exit(-1);
  }

  cname = argv[1];
  cFile = fopen (cname, "r");
  speakerTable = argv[2];
  speakerFile = fopen(speakerTable,"r");
  string1 = argv[3];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[4];
  
  sscanf(string2,"%d",&VQIter);
  string2 = argv[5];
  sscanf(string2, "%d", &GMMIter);
  string2 = argv[6];
  outputFile = fopen(string2,"w");
  string1 = argv[7];
  
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  varianceFloor = (float) GetFAttribute (asdf, "varianceFloor");
  Cstore(asdf->fftSize);

  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
 
  oldVfv = NULL; 
  while (!feof(speakerFile))	
    {
	featFileName = (char *) calloc (256, sizeof (char));
	fscanf (speakerFile, "%s\n", featFileName);
	newVfv = ReadVfvFromFile (featFileName, &numVectorsNew);
	if (oldVfv == NULL)
	  {
		oldVfv = newVfv;
		numVectorsOld = numVectorsNew;
		free (featFileName);	
		continue;
	  }		
	vfv = JoinVfv (oldVfv, numVectorsOld, 
				newVfv, numVectorsNew,
				&numVectors, 3);	// 3 : Cleanup value
	oldVfv = vfv;
	numVectorsOld = numVectors;
	printf ("In main: Finished reading %s\n", featFileName);
	free (featFileName);	
    }
  numVectors = numVectorsOld ;
  vfv = oldVfv;
  fclose (speakerFile);
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %ld numGMM = %d\n", numVectors, GMMIter);
  ComputeGMM(vfv, numVectors, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             ditherMean, varianceNormalize,  varianceFloor, seed);
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(outputFile,"%e \n", clusterWts[i]);
      printf("%e \n", clusterWts[i]);
    } else {
      fprintf(outputFile, "%f\n", 1.0E-10);
      printf("%e\n", 1.0E-10);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
      printf("mean=%e var=%e \n",
	     clusterMeans[i]->array[j],clusterVars[i]->array[j]);
      fprintf(outputFile," %e %e ",clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  fclose(outputFile);
  free(vfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
  return(0);
}

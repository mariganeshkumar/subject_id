#!/usr/local/bin/perl

# Program:	mu_ec.perl
# Written by:	David Graff, Linguistic Data Consortium
# Usage:	mu_ec.perl [-s] infile outfile
# Purpose:	run 2-channel echo cancellation on sphere-header files

# The echo cancellation program (ec.exe, v2.3), supplied by the
# Institute for Signal and Information Processing (at Mississippi
# State University), assumes headerless, 2-channel interleaved 16-bit
# PCM data as input (8KHz sample frequency), and produces equivalent
# output with echo cancellation applied to both channels.

# The following script provides a simple wrapper to make it easier to
# apply echo cancellation on files that contain NIST SPHERE headers
# and 2-channel mu-law telephone speech (e.g. Switchboard and Call
# Home speech files).

# This script makes use of the NIST SPHERE utility programs "h_edit"
# and "w_edit", as well as the echo-cancellation program itself.
# These programs should be in the user's current execution path.

$Usage =
    "Usage: mu_ec.perl [-s] infile outfile\n" .
    "  use -s if your system's native byte order is 01\n" .
    "  (default byte order is 10)\n";

$byte_order = "10";

# First, check usage and input data:

if ( $ARGV[0] =~ /^\-/ ) {
    $option = shift;
    if ( $option eq '-s' ) {
	$byte_order = "01";
    } else {
	die $Usage;
    }
}
die $Usage if ( $#ARGV != 1 );

$infile = shift;
$outfile = shift;
die "Input file $infile not found\n" if ( !-e $infile );

$orig_hdr = `h_read $infile`;
die "mu_ec.perl: Input file $infile is not a 2-chan 8KHz mu-law sphere file\n"
    if ($orig_hdr !~ /channel_count 2/ ||
	$orig_hdr !~ /sample_rate 8000/ ||
	$orig_hdr !~ /sample_coding m?u\-?law/ );


# Create a version of the original sphere header for use with pcm data

$tmp_hdr = "linear_hdr.$$";	# file name for a modified sphere header

open( INP, "<$infile" );
open( OUT, ">$tmp_hdr" );
sysread( INP, $hdr, 1024 );
die "mu_ec.perl: Unable to write header file $tmp_hdr\n"
    if ( syswrite( OUT, $hdr, 1024 ) != 1024 );
close( INP );
close( OUT );

$hed_args =
    "-Ssample_coding=pcm -Isample_n_bytes=2 " .
    "-Ssample_byte_format=$byte_order -Secho_cancellation=ec-v2.3";
`h_edit $hed_args $tmp_hdr`;


# Now create and run the pipeline that does the echo cancellation

$cmd =
    "w_edit -o pcm_$byte_order $infile - | h_strip - - | " .
    "ec.exe | cat $tmp_hdr - | w_edit -o ulaw - $outfile";

$status = system( $cmd );

die "mu_ec.perl: echo cancellation pipeline failed for $infile\n"
    if ( $status/256 );

unlink $tmp_hdr;

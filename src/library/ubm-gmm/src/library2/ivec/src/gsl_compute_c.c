/*
    This file computes C matrix and writes it to a file

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"

int
main (int argc, char *argv[])
{
    if (argc != 7) 
    {
        printf( "usage: %s fostatList ivectList M D R cMatOutFile\n", argv[0] );
	exit(0);
    }

    char *fostatname = NULL,
         *iveclistname = NULL,
	 *CZero = NULL,
         *cmatfilename = NULL;
    char *cmattxtfilename = NULL;
    int  i, ii, M, D, R, md;
    gsl_matrix *C = NULL, *w = NULL;
    fostat *fx;
    flist *iveclist, *folist;

    fostatname = argv[1];
    iveclistname = argv[2];
    sscanf (argv[3], "%d", &M);
    sscanf (argv[4], "%d", &D);
    sscanf (argv[5], "%d", &R);
    cmattxtfilename = argv[6];

    folist = read_list_of_files (fostatname);
    iveclist = read_list_of_files (iveclistname);

    ii = folist->num_lines;
    printf("\n Number of stat files : %d", ii);
    md = M * D;

    C = gsl_matrix_alloc (md, R);
    gsl_matrix_set_zero(C);

    for (i = 0; i < ii; i++) {
        w = read_matrix (iveclist->line[i], R, 1);
        fx = read_fostat (folist->line[i]);
        if (fx == NULL || w == NULL) continue;
        gsl_blas_dgemm(CblasNoTrans,CblasTrans,
                       1.0, fx->f, w, 1.0, C);
        free_fostat(fx);
        gsl_matrix_free (w);
    }

    write_matrix (cmattxtfilename,C);
    free_flist (folist);
    free_flist (iveclist);
}


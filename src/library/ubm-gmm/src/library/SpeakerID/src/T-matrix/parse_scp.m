function [logical physical weights] = parse_scp(filename)

rec = textread(filename, '%s', 'delimiter', '\n');

% decide wheather we have *=* pattern
pat=regexp(rec(1), '(?<logical>.*)=(?<physical>.*)$', 'match');

if isempty(pat{1})
  nrec = size(rec,1);
  physical = rec;
  logical  = cell(nrec,1);
else
  n = regexp(rec,'(?<logical>.*)=(?<physical>.*)$', 'names');

  nrec = size(n,1);
  logical   = cell(nrec,1);
  physical  = cell(nrec,1);

  for ii = 1:nrec
    logical{ii,1}   = n{ii}.logical;
    physical{ii,1}  = n{ii}.physical;
  end
end

weights = ones(size(physical,1),1);

% check for weight in the name
pat=regexp(physical(1), '(?<physical>\S*)\s*{\s*(?<weight>[\.0-9]*)\s*}', 'match');

if (~isempty(pat{1}) )
  n=regexp(physical, '(?<physical>\S*)\s*{\s*(?<weight>[\.0-9]*)\s*}', 'names');

  nrec = size(n,1);
  physical   = cell(nrec,1);
  weights    = zeros(nrec,1);

  for ii = 1:nrec
    physical{ii,1}  = n{ii}.physical;
    weights(ii,1)   = str2num(n{ii}.weight);
  end
end



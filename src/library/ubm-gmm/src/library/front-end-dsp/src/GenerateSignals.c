
/**************************************************************************
 *  $Id: GenerateSignals.c,v 1.1 2000/04/23 02:36:13 hema Exp hema $
 *  File:	GenerateSignals.c - Generation of test data
 *
 *  Purpose:	Generate Sinusoids and AR Signals
 *
 *  Author:	Hema A Murthy
 *
 *  Created:    Wed 19-Apr-2000 07:41:20
 *
 *  Last modified:  Wed 19-Apr-2000 13:44:0 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"

/*-------------------------------------------------------------------------
 *  Usage -- Usage for the program
 *    Args:	None
 *    Returns:	None
 *    Bugs:	
 * -------------------------------------------------------------------------*/

void Usage()
{
  printf("For Sinosoids in noise:\n");
  printf("GenerateSignals s Y outFile numSamples dB seed freq1 freq2 samplingFreq amp1 amp2\n"); 
  printf("For Sinosoids without noise:\n");
  printf("GenerateSignals s N outFile numSamples freq1 freq2 samplingFreq amp1 amp2\n"); 
  printf("For ARSignal in noise:\n");
  printf("GenerateSignals a Y outFile numSamples dB seed srcType band\n"); 
  printf("For ARSignals without noise:\n");
  printf("GenerateSignals a N outFile numSamples srcType band\n"); 
}	/*  End of Usage		End of Usage   */

/*-------------------------------------------------------------------------
 *  Gauss -- Generates a Gaussian random number
 *    Args:  stDev, mean, seed	
 *    Returns:	Gauss
 *    Bugs:	
 * -------------------------------------------------------------------------*/

float Gauss(float stDev, float mean)
{

  float             sum;
  int               i;
  float             random, rmax;
  sum = 0.0;
  for (i = 1; i <= 12; i++) {
    random = rand();
    rmax = RAND_MAX;
    sum = sum + random/rmax;
}
    sum = (sum-6.0)*stDev+mean;
  
  return(sum);
}	/*  End of Gauss		End of Gauss   */


/*-------------------------------------------------------------------------
 *  GenerateSinuSoid -- Generates a sum of two Sinusoids
 *    Args:	
 *    Returns:	
 *    Bugs:	
 *    Ref : S.M.Kay,Modern Spectral Estimation, pp.82
 * -------------------------------------------------------------------------*/

void GenerateSinuSoid(float *signal,int npts,char addNoise, float dB,int seed, 
		      float freq1, float freq2, float samplingFreq, 
		      float amp1, float amp2)
{
  float                           stDev, energy;
  float                           root10,root20;
  int                             i;

  root10 = sqrt(amp1);
  root20 = sqrt(amp2);
  srand(seed);
  for (i = 1; i <= npts; i++)
    signal[i] = root10*cos(2*PI*freq1/samplingFreq*(i-1)) +
      root20*cos(2*PI*freq2/samplingFreq*(i-1));

  if (addNoise == 'Y') {
    energy = 0.0;
    for (i = 1; i <= npts; i++) 
      energy = energy + signal[i]*signal[i];
    energy = energy/npts;
    stDev = sqrt(energy/exp(dB/20.0*log(10)));
      for (i = 1; i <= npts; i++) 
	signal[i] = signal[i] + Gauss(stDev,0.0);
  }
}	/*  End of GenerateSinuSoid		End of GenerateSinuSoid   */


/*-------------------------------------------------------------------------
 *  GenerateARSignal --  Generates a broadband or narrow band AR Signal
 *    Args:	signal, npts, band, srcType, addNoise, dB, seed
 *    Returns:	signal array as a float
 *    Bugs:	
 *    Ref : S.M. Kay, Modern Spectral Estimation, pp.147,242
 * -------------------------------------------------------------------------*/

void GenerateARSignal(float *signal, int npts, char band, 
		      char srcType, char addNoise, float dB,int seed)
{
  float                        stDev,energy;
  float                        coef[5], *source;
  int                          i, j, lim;
  float                        sum;
  int                          newSeed = seed+50;
  float                        tempY, powerY, Rmax, gaussRand;
  if (band == 'B') {
    coef[1] = -1.352;
    coef[2] = 1.338;
    coef[3] = -0.662;
    coef[4] = 0.240;
  } else if (band == 'N') {
    coef[1] = -2.760;
    coef[2] = 3.809;
    coef[3] = -2.654;
    coef[4] = 0.924;
  }
  source =  (float *) AllocFloatArray (source, npts+1);
  if (srcType ==  'G') 
    for (i = 1; i <= npts; i++)
      source[i] = Gauss(1.0,0.0);
  else {
    source[1] = 1.0;
    for (i = 2; i <= npts; i++)
      source[i] = 0.0;
  }
  for (i = 1; i <= npts; i++) {
    signal[i] = source[i];
    sum = 0.0;
    lim = i-1;
    if (lim > 4) lim = 4;
    for (j = 1; j <= lim; j++) 
      sum = sum + coef[j]*signal[i-j];
    signal[i] = signal[i] - sum;
  }
  Rmax = signal[Imax(signal,npts)];
  for (i = 1; i <= npts; i++)
    signal[i] = signal[i]/Rmax;
  if (addNoise == 'Y') {
    energy = 0.0;
    for (i = 1; i <= npts; i++) 
      energy = energy + signal[i]*signal[i];
    energy = energy/npts;
    tempY = dB/10.0;
    powerY = powf(10.0, tempY);
    stDev = sqrt(energy/powerY);
    for (i = 1; i <= npts; i++) {
      gaussRand = Gauss(stDev, 0.0);
      signal[i] = signal[i] + gaussRand;
    }
  }
  free(source);
}	/*  End of GenerateARSignal		End of GenerateARSignal   */




  main(int argc, char**argv)
    {
      float                   *signal;
      int                     numSamples;
      char                    sigType = 's', addNoise ='N', 
	                      srcType = 'G', band = 'N';
      float                   dB;
      int                     seed;
      float                   freq1, freq2;
      int                     samplingFreq;
      int                     i;
      float                   amp1, amp2;
      FILE                    *sigFile;

      if (argc < 7) { 
        Usage();
	exit(-1);
      }
      sscanf(argv[1],"%c", &sigType);
      sscanf(argv[2],"%c", &addNoise);
      sigFile = fopen(argv[3],"w");
      sscanf(argv[4],"%d",&numSamples);
      signal = (float *) AllocFloatArray(signal, numSamples+1);

      if (addNoise == 'Y') {
	sscanf(argv[5],"%f", &dB);
	sscanf(argv[6],"%d", &seed);
        srand(seed);
      }
      if (sigType == 's') {
	if (argc < 10) {
	  Usage();
	  exit(-1);
	}
	if (addNoise == 'Y') {
	  sscanf(argv[7],"%f", &freq1);
	  sscanf(argv[8],"%f", &freq2);
	  sscanf(argv[9],"%d", &samplingFreq);
	  sscanf(argv[10],"%f", &amp1);
	  sscanf(argv[11],"%f", &amp2);
	} else {
	  sscanf(argv[5],"%f", &freq1);
	  sscanf(argv[6],"%f", &freq2);
	  sscanf(argv[7],"%d", &samplingFreq);
	  sscanf(argv[8],"%f", &amp1);
	  sscanf(argv[9],"%f", &amp2);
	}
	GenerateSinuSoid(signal, numSamples, addNoise, dB, seed, 
		      freq1, freq2, samplingFreq, 
		       amp1, amp2);
      } else {     
        if (argc < 7) {
	  Usage();
	  exit(-1);
	}
	if (addNoise == 'Y') {
	  if (argc < 9) {
	    Usage();
	    exit(-1);
	  }
	  sscanf(argv[7],"%c", &srcType);
	  sscanf(argv[8],"%c", &band);
	} else {
	  sscanf(argv[5],"%c", &srcType);
	  sscanf(argv[6],"%c", &band);
	}
	GenerateARSignal(signal, numSamples, band, 
			 srcType, addNoise, dB, seed);      
      }
      for (i = 1; i <= numSamples; i++)
	fprintf(sigFile, "%f\n", signal[i]);
    }
/**************************************************************************
 * $Log: GenerateSignals.c,v $
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of GenerateSignals.c
 **************************************************************************/










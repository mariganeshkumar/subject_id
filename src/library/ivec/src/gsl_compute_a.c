/*
    This file computes A matrix and writes it to a file

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/


#include "gsl_helper.h"
#include<getopt.h>

void
printUsage() 
{  
    printf("Usage: ComputeA [options] amatprefix "
           "fxlist "
           "lxlist M D R\n"
           "The options are\n"
           "--start/-s : start index in the fxlist [first by default]\n"
           "--end/-e : end index in the fxlist [last line by default]\n"
           "--fb : the fxlists are in binary mode\n"
           "--lb : the lxlists are in binary mode\n"
           );
    exit(0);
}

int
computeA (gsl_matrix *A, char *fostatname, 
          char *lxmatname,
          int M,
          int D,
          int R)
{
    int i,j,k, md = M * D,
        m1, m2, sf;
    float scale_factor;
    fostat *spkr_stats = NULL;
    gsl_matrix *lxmat = NULL;
    gsl_matrix_view submat;    

    spkr_stats = read_fostat (fostatname);
    if(spkr_stats == NULL) return FILE_OPEN_FAILED;

    lxmat = read_matrix(lxmatname,R,R);
    if(lxmat == NULL) return FILE_OPEN_FAILED | MEM_ALLOC_FAILED;
    
    scale_factor = 1.0;
    for(m1 = 0; m1 < M; m1++) {
        sf = spkr_stats->eeta[m1];
        if (sf <= 0) continue;
        gsl_matrix_scale (lxmat, (double)sf / scale_factor);
        scale_factor = (double) sf;
        submat = gsl_matrix_submatrix(A,m1*R,0,R,R);
        gsl_matrix_add(&submat.matrix,lxmat);
    }
    gsl_matrix_free(lxmat);
    free_fostat(spkr_stats);
    return 0;
} 
int 
main(int argc, char *argv[]) 
{
    FILE *amatfile = NULL;
    char  *fxlistname = NULL,
          *lxlistname = NULL;
    int   i, numRead;
    int   buff_size = 1024, num_files = 0,
          M, D, R;
    int   oc;
    int   start_idx = -1, end_idx = -1, idx;
    char  *amatprefix = NULL;
    gsl_matrix *A = NULL;
    flist *lxlist = NULL, *fxlist = NULL;
    struct option longopts[] = {
        { "--start", required_argument, NULL, 's' },
        { "--end", required_argument, NULL, 'e' },
        { "--help", no_argument, NULL, 'h'},
        { 0,  0,  0,  0}
    };


    while((oc = getopt_long(argc, argv, ":he:s", longopts, NULL)) != -1) {
        switch(oc) {
            case 's':
                sscanf(optarg, "%d", &start_idx);
                break;
            case 'e':
                sscanf(optarg, "%d", &end_idx);
                break;
            case 'h':
            default:
                printUsage();
                return 1;
        }
    }

    if(argc-optind != 6) {
        printf ("too few/many args\n");
        printUsage();
    }
    argv = &argv[optind];
   
    amatprefix = argv[0];
    fxlistname = argv[1];
    lxlistname = argv[2];
    fxlist = read_list_of_files(fxlistname);
    lxlist = read_list_of_files(lxlistname);
    if(fxlist->num_lines <= 0 || 
       lxlist->num_lines <= 0 || 
       fxlist->num_lines != lxlist->num_lines) {
        fprintf (stderr,"num of lines is incorrect\n");
        return 4;
    }
    num_files = lxlist->num_lines;
    sscanf(argv[3], "%d", &M);
    sscanf(argv[4], "%d", &D);
    sscanf(argv[5], "%d", &R);
    
    A = gsl_matrix_alloc(M*R,R);
    if(A == NULL) {
        fprintf(stderr, "Unable to allocate such a large matrix\n");
        return 3;
    }

    if (start_idx == -1) start_idx = 1;
    if (end_idx == -1) end_idx = num_files;

    for (idx = start_idx - 1; idx < end_idx; idx++) {
        fflush(stdout);
        computeA(A, fxlist->line[idx],
                 lxlist->line[idx],
                 M, D, R);
    }

    for (idx = 0; idx < 10; idx++) 
      printf("%e ", gsl_matrix_get(A,0,idx)); 

    write_matrix(amatprefix, A);
    gsl_matrix_free(A);
    free_flist(fxlist);
}

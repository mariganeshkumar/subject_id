#!/usr/bin/python
# Estimate the ivector given extractor matrix (T)
# and first order stats
import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *
import GMM

# option description
# T: extractor matrix
# f: first order stat
# o: output file
# h: print help
optlist, args = getopt.getopt (sys.argv[1:], "T:f:o:h")

def printhelp (close = 0):
  print "Usage: python ivecest.py -T tmat.hdf5 -f firstOrderStats"
  if close == 1:
      quit ()

hdfname, fstatname = "", ""
opfname = ""

for arg in optlist:
    option = arg[0]
    value = arg[1]

    if option == "-T":
        hdfname = value
    elif option == "-f":
        fstatname = value
    elif option == "-o":
        opfname = value
    else:
        printhelp (1)

if hdfname == "" or fstatname == "":
    printhelp (1)

hdf = openFile (hdfname, mode = "r")
dimmat = hdf.root.dim
# there should be a better way to read a single row
tempv = (dimmat.read (start = 0, stop = 1))['dim']
R = tempv[0][1]

# read the f.o.stats
M,D,Nx,fx = ReadFirstOrderStats (fstatname)
tmat = hdf.root.matrix
# first compute T' f_x. this produces a smaller
# matrix to handle with
tempmat = np.zeros ((R,1))
r = 0
for row in tmat.iterrows ():
    tvec = row['colvec']
    tempmat[r,] = np.dot (tvec, fx)
    r+=1
tm = np.zeros ((D,R))
lx = np.identity (R)
for m in range (0,M):
    r = 0
    for row in tmat.iterrows ():
        tm[:,r] = row['colvec'][m*D:(m+1)*D]
        r += 1
    lx += (np.dot (tm.transpose (),tm) * Nx[m])
lx = np.linalg.inv(lx)   
wx = np.dot (lx , tempmat)   

# store lx + wx * wx(t)
lx += np.dot (wx,wx.transpose ())
opf = open (opfname, "w")
# write to the file in following format
# first line contains M D R
# next line contains R value representing wx 
# subsequent R lines contain R values separated by spaces representing
# Lx value
opf.write ("%d %d %d\n" % (M,D,R))
for r in range (0,R):
    opf.write(" %e" % wx[r])
opf.write ("\n")
for r1 in range (0,R):
    for r2 in range (0,R):
        opf.write (" %e" % lx[r1,r2])
    opf.write ("\n")
opf.close ()

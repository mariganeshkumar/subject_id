
function recomputeT (tmatfname, cmatfname, amatname, M, D, R, exflag)
        load (cmatfname, '-mat');
        W = zeros(M*D,R);
        l = ([1:M]-1)*D+1;
        r = [1:M]*D;
        amatname = strcat(amatname,'.');
        for m=1:M
          load (strcat(amatname,int2str(m)), '-mat');
          W(l(m):r(m),:) =  C(l(m):r(m),:) * inv(Ax);
        end
        save (tmatfname, 'W', '-mat');
        if exflag ~= 0
            exit
        end

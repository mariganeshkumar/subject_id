#ifndef __TUMAP_2004_d_
#define __TUMAP_2004_d_

#include "stdio.h"
#include "stdlib.h"
#include "CommonFunctions.h"

struct topCMixtures_ {
      int id;
      float DistortionValue;
};

typedef struct topCMixtures_ topCMixtures;
float  *ComputeTopCLikelihood (VECTOR_OF_F_VECTORS *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds);
void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue);
int **GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor);
void Usage();
#endif

#!/usr/bin/python
# estimate C and store it in a file
# for T matrix

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *

# arguments description
# C: (flat) file to store C matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "C:F:w:h")

def printhelp (close = 0):
  print "Usage: ./hype.py -C Cmatrix.file -F fo.file -w wx.file"
  if close == 1:
    quit ()

cmatfname, wxlistname, folist = "", "", "" 

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-F":
      folist = value
    elif option == "-C":
      cmatfname = value
    elif option == "-w":
      wxlistname = value
    elif option == "-h":
      printhelp (1)
    else:
      print "Invalid argument..."
      printhelp (1)

# check if args are empty
if wxlistname == "" or folist == "" or cmatfname == "":
    printhelp (1)


M,D,nx,fx = ReadFirstOrderStats (folist)
fx = fx.reshape ((M*D,1))
wx, Lx = ReadIVecEst (wxlistname)
C = np.dot (fx, wx.transpose ())
nrows, ncols = np.shape (C)

# store C in a file
# the structure of the file is as follows. the first line
# contains the number of cols and number of rows (like dim numVec
# in vfv dumps). this is followed by each row of the matrix
np.save (cmatfname, C)
#cmatf = open (cmatfname, "w")
#cmatf.write ("%d %d\n" % (ncols, nrows))
#for rno in range (0, nrows):
#  for cno in range (0, ncols):
#    cmatf.write (" %e" % C[rno,cno])
#  cmatf.write ("\n")
#cmatf.close ()  

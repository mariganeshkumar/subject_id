/*-------------------------------------------------------------------------
 *  SigInstants.c - Find the significant instants of excitation
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@localhost.localdomain)
 *
 *  Created:        Thu 25-Jan-2001 16:28:13
 *  Last modified:  Thu 20-Feb-2014 11:47:37 by hema
 *  $Id: SigInstants.c,v 1.1 2001/10/31 17:33:18 hema Exp $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
/*******************************************************************************
* 	the Following program computes the significant instants of 
        excitation by processing the phase slope function 
*	a file. 
*	Inputs :
*	text file of the phase slope function
*	Output :
        text file containing the Significant Instants

*******************************************************************************/       void Usage() {
           printf("Usage : SigInstants inputFile outputFile numSamples\n");
}
/*****************************************************************************/

main (int argc, char *argv[])
{ 
        float           *signal, *sigInstants;
	long            i, numSamples;
        FILE            *signalFile, *sigInstFile;
        char            line[100];
        float           *waveform;
/******************************************************************************/
       if (argc != 4) {
         Usage();
         _exit(-1);
       }
       signalFile = fopen(argv[1], "r");
       sigInstFile = fopen(argv[2],"w");
       sscanf(argv[3], "%ld", &numSamples);
       printf("%ld\n", numSamples);
       waveform = (float *) malloc(numSamples*sizeof(float));
       if ((signalFile == NULL) || (sigInstFile == NULL)){
	 printf("cannot open files \n");
	 exit(-1);
       }
       i = 0;
       while (fgets(line, 200, signalFile) != NULL) {
	 printf("line = %s\n", line);
         sscanf(line, "%f", &waveform[i]);
	 if (waveform[i] > 0) 
	   waveform[i] = 1;
	 else
	   waveform[i] = -1;
	 i++;
       }
       for (i = 1; i < numSamples-1; i++)
	 if ((waveform[i]> 0) &&(waveform[i-1] < 0))
	   fprintf(sigInstFile, "%d %d\n", i,10);
         else
	   fprintf(sigInstFile, "%d %d\n", i,0);
}
/*-------------------------------------------------------------------------
 * $Log: SigInstants.c,v $
 * Revision 1.1  2001/10/31 17:33:18  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of SigInstants.c
 -------------------------------------------------------------------------*/
 

function [] = train_and_test_intersession(config)
path=genpath('library');
addpath(path);
exp_name = 'Eye-Open-Results';
if ~exist('config','var')
    config = configuration;
end
disp(config)
feature_dir = config.features_dir;

feature_code = [num2str(config.val_split),...
    '_',num2str(config.feature)];

val_predicted_label =  cell([1 config.feature]);
val_gt_label = cell([1 config.feature]);

val_accuracy = zeros([1 config.feature]);
val_eer = zeros([1 config.feature]);
val_norm_eer = zeros([1 config.feature]);

test_predicted_label = cell([1 config.feature]);
test_gt_label = cell([1 config.feature]);

test_accuracy = zeros([1 config.feature]);
test_eer = zeros([1 config.feature]);
test_norm_eer = zeros([1 config.feature]);

experiments_cell = {'Ssvep', 'Oddball-Visual', 'Passive-Visual'};
subject_cell = {'41','42','43','45','46','50','51','52','55', '44'}; %'44'

freq_dim = 0;

for t=1:config.trials
    
    %get all the subjects in the feature directory
    subjects = get_all_sub_dir([feature_dir,'/',feature_code]);
    intersession_subjects=0;
    
    subjectsInfo={};
    sessionInfo=[];
    for i = 1:length(subjects)
        if sum(strcmp(subject_cell, subjects{i}))==0
             continue;
        end
        %get all the session for a given subject
        session = get_all_sub_dir([...
            feature_dir,...
            '/',feature_code,...
            '/',num2str(subjects{i})
            ]);
        no_of_sessions=length(session);
        random_splits_folder = [config.history_save,...
            '/',num2str(config.feature),...
            '/random_sessions_ordering',...
            '/',num2str(subjects{i}),...
            '/'
            ];
        if exist(random_splits_folder,'dir') ~= 7
            mkdir(random_splits_folder)
        end
        random_splits_filename = [random_splits_folder,'/',...
            num2str(t),'.txt'];
        if exist(random_splits_filename, 'file') == 2
            random_permutation = dlmread(random_splits_filename, ' ');
        else
            random_permutation = 1:no_of_sessions;
            dlmwrite(random_splits_filename,random_permutation,' ');
        end
        
        is_ubm_data =0;
        if no_of_sessions <= 1
            is_ubm_data = 1;
            continue;
        elseif no_of_sessions == 2
            intersession_subjects = intersession_subjects+1;
            train_sessions = 1;
            subjectsInfo{intersession_subjects}=subjects{i};
            sessionInfo(intersession_subjects) = no_of_sessions;
        else
            intersession_subjects = intersession_subjects+1;
            train_sessions = floor(no_of_sessions * 0.7);
            subjectsInfo{intersession_subjects}=subjects{i};
            sessionInfo(intersession_subjects) = no_of_sessions;
        end
        
        disp(['Loading data for subject: ',num2str(subjects{i}),';  ',num2str(intersession_subjects),'/',num2str(no_of_sessions)])
        
        for j = 1:no_of_sessions
            recordings = dir([...
                feature_dir,...
                '/',feature_code,...
                '/',num2str(subjects{i}),...
                '/',session{j},...
                '/*.mat'
                ]);
            recordings = {recordings(:).name};
            rind=1;
            for l = 1:length(recordings)
                filename=split(recordings{l},'.');
                filename = filename{1};
                experiment_name = split(filename,'_');
                experiment_name = experiment_name{end};
                data = load([...
                    feature_dir,...
                    '/',feature_code,...
                    '/',num2str(subjects{i}),...
                    '/',session{j},...
                    '/',recordings{l}
                    ]);
                if size(data.feature,1) ~= length(config.lobe_map{config.lobe})
                    disp('wrong logic')
                end
                data = data.feature;
                freq_dim = size(data,2);
                if is_ubm_data
                    %allData.ubm_data{i}{j}{rind} = data;
                elseif ~is_ubm_data && random_permutation(j) <= train_sessions && sum(strcmp(experiments_cell, experiment_name)) ==0
                    allData.ubm_data{intersession_subjects}{j}{rind} =  data;
                    allData.train_data{intersession_subjects}{j}{rind} = data;
                    rind = rind + 1;
                elseif ~is_ubm_data && random_permutation(j) > train_sessions &&sum(strcmp(experiments_cell, experiment_name)) ==0
                    allData.test_data{intersession_subjects}{j}{rind} = data;
                    rind = rind + 1;
                end
            end
        end
        allData.test_data{intersession_subjects}{:}
    end
    
    allData.freq_dim = freq_dim;
    allData.num_channels = length(config.lobe_map{config.lobe});
    modelSaveDir = [config.history_save,...
        '/',config.feature_name{config.feature},...
        '/',config.classifier_name{config.classifier},...
        '/', num2str(t)];
    allData.modelSaveDir = modelSaveDir;
    plotSaveDir = [config.history_save,...
        '/',config.feature_name{config.feature},...
        '/',config.classifier_name{config.classifier},...
        '/',exp_name,...
        '/', num2str(t)];
    allData.plotSaveDir = plotSaveDir;
    mkdir(plotSaveDir);
    allData.sessionInfo = sessionInfo;
    disp(['Total Number Of Intersession Subjects:', num2str(intersession_subjects)]);
    disp(['Total Number Of Sessions:', num2str(sum(sessionInfo))]);
    [results]=config.classifier_function{config.classifier}(config,allData);
    save([modelSaveDir,'/',num2str(config.val_split),'_score.mat'],'results');
    val_predicted_label{t}=results.val_predicted_label;
    val_gt_label{t}=results.val_gt_label;
    test_predicted_label{t} = results.test_predicted_label;
    test_gt_label{t} = results.test_gt_label;
    
    clear allData;
    cm = confusionmat(test_predicted_label{t}, test_gt_label{t});
    test_accuracy(t) = sum(diag(cm))/sum(sum(cm)) * 100;
    
    disp(['Trail Accuracy ', num2str(test_accuracy(t))]);
    cm = confusionmat(val_predicted_label{t}, val_gt_label{t});
    val_accuracy(t) = sum(diag(cm))/sum(sum(cm)) * 100;
    save_dir=[modelSaveDir,'/',num2str(config.val_split),'_'];
    
    
    test_eer(t) = plot_score_distribution([save_dir,'test'],results.target_test_scores, results.non_target_test_scores);
    val_eer(t) = plot_score_distribution([save_dir,'val'],results.target_val_scores, results.non_target_val_scores);
    fprintf('EER is %.2f\n', test_eer(t));
    
    
    test_norm_eer(t) = plot_score_distribution([save_dir,'test_norm'],results.target_test_norm_scores, results.non_target_test_norm_scores);
    val_norm_eer(t) = plot_score_distribution([[save_dir,'val_norm']],results.target_test_norm_scores, results.non_target_test_norm_scores);
    fprintf('Normalised EER is %.2f\n', test_eer(t));
    
end
val_predicted_label;
val_predicted_label = cell2mat(val_predicted_label);
val_gt_label = cell2mat(val_gt_label);

test_predicted_label = cell2mat(test_predicted_label);
test_gt_label = cell2mat(test_gt_label);

result_str = strcat(config.history_save,...
    '/',config.feature_name{config.feature},...
    '/',config.classifier_name{config.classifier},...
    '/',exp_name,...
    '/report_val_segment',num2str(config.val_split),'_Fe_',...
    config.feature_name{config.feature},'_Clas_',...
    config.classifier_name{config.classifier},'_inter.txt');
WriteReport(result_str, val_accuracy, val_predicted_label,...
    val_gt_label,val_eer,val_norm_eer,subjectsInfo);

result_str = strcat(config.history_save,...
    '/',config.feature_name{config.feature},...
    '/',config.classifier_name{config.classifier},...
    '/',exp_name,...
    '/report_test_segment',num2str(config.val_split),'_Fe_',...
    config.feature_name{config.feature},'_Clas_',...
    config.classifier_name{config.classifier},'.txt');
WriteReport(result_str, test_accuracy, test_predicted_label,...
    test_gt_label,test_eer,test_norm_eer,subjectsInfo);
end
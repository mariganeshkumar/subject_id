import sys
import getopt
import math
import numpy
import scipy
from tables import *

# import this code only when M and D are already define.

# import this every time M and D are changed so that the
# changes are reflected

# call this hdf5open () wrapper so that file description
# is common to all. every hdf5 file contains a matrix.
# the table "matrix" containing the matrix has rows that are 
# cols of the matrix. the number of rows and cosl in 
# the table are stored in a separate table "dim"

def hdf5open (fname, m, d):
  hnd = openFile (fname, mode = "w", title = "matrix file")
  # Description class for hdf5 files
  class svec (IsDescription):
    colvec = Float32Col (m*d)
  # dim description
  class dimdesc (IsDescription):
    dim = Int32Col (2)  # first entry is the no.of rows in the matrix
                        # second entry is the no.of cols
  class cid (IsDescription):
    cid = Int32Col (1)  # class id of each row
  
  # add the "matrix" table
  
  mtable = hnd.createTable ("/", 'matrix',svec,"stack of col vectors")
  dtable = hnd.createTable ("/", 'dim', dimdesc, "two col table")
  return (hnd, mtable, dtable) 

def hdf5close (hnd):
  hnd.close ()


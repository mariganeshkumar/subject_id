/* SlopeStg.c: functions takes asdf params along with wavname/
 *             log filter bank energy dump. it performs
 *             stg on that stream and then computes slope
 */
 
#include "SlopeStg.h"

VFV* 
ComputeSlopeStg (VFV *lfbstream, uint nvec, int nrc, int gwnd) {
    int n,i,j,dim, numSlope;
    VFV *slopestg= NULL, *tempvfv;
    float intercept;

    if (nvec == 0) return NULL;
    dim = (int) lfbstream[0]->numElements;
    numSlope = dim - 1;
    tempvfv = Gaussianization (lfbstream, nvec, gwnd);

    slopestg = (VFV *) calloc (nvec, sizeof (VFV));
    finc(n,nvec,1) {
         slopestg[n] = (F_VECTOR*) AllocFVector (numSlope);
         finc(i,numSlope,1)
                LinReg (tempvfv[i], i, 
                        nrc, &intercept, 
                        &slopestg[n]->array[i]
                       ); 
    }
    return slopestg;
}

#!/bin/bash
# Info: Script to extract features. Was written for NIST 2004 files
flist=../flists/2004/ubm.m
dumpbin=../bin/DumpFeat
cfile=../cfiles/fe-ctrl.base
feat=frameCepstrum+frameDeltaCepstrum
tgmmfile=../models/tgmm
if [ $# -ne 1 ]; then
  echo "USage: scriptName outfolder"
  exit
fi
outfolder=$1
mkdir -p $outfolder
for filename in `cat $flist`; do
  outname=`basename $filename | cut -d '.' -f1`  
  $dumpbin $cfile $filename $feat  $tgmmfile $outfolder/$outname
  echo "$dumpbin $cfile $filename $feat  $tgmmfile $outfolder/$outname"
done  

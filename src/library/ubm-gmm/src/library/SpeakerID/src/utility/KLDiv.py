import GMM as GMM
import math
import numpy as np
import scipy
import getopt
import sys

def KLDistance (gmm1, gmm2):
    return (KLDivKernel (gmm1, gmm1) +
           KLDivKernel (gmm2, gmm2) -
           (2 * KLDivKernel (gmm1, gmm2)))

# works only when the gmms are mean adapted
def KLDivKernel (gmm1, gmm2):
  means1, means2 = gmm1.GetMeans (), gmm2.GetMeans ()
  vars1 = gmm1.GetVars ()
  wts1 = gmm1.GetWts ()
  nmix, kval, dim = len (wts1), 0., len (means1 [0,:])
  
  kval = np.sum ( [ sum ((means1[n,:] * means2[n,:]) / vars1 [n,:]) * wts1[n]  
                for n in range (0,nmix)
         ])
#  sqvars, sqwts = np.sqrt (vars1), np.sqrt (wts1)
#  lval, rval = np.ndarray ((dim,1)),np.ndarray ((dim,1))
#  for n in range(0,nmix):
#    lval = sqwts[n] * means1[n,:] / sqvars[n,:] 
#    rval = sqwts[n] * means2[n,:] / sqvars[n,:]
#    tval = (np.dot (np.transpose (lval) , rval))
#    kval += tval
  return kval

optlist, args = getopt.getopt (sys.argv[1:], "l:r:ohd")
printDist = 0
for arg in optlist:
  option = arg[0]
  if option == "-l":
    gmm1 = GMM.ReadGMM (arg[1])
  elif option == "-r":
    gmm2 = GMM.ReadGMM (arg[1])
  elif option == "-d":
    printDist = 1
  elif option == "-h":
    print "Usage: python KLDiv.py [options]"
    print "l: Gmm1's file name"
    print "r: Gmm2's file name"
    print "h: print this help"
    print "d: print distance rather than kernel value"
    quit ()

if printDist == 1:
  print KLDistance (gmm1, gmm2)
else:
  print KLDivKernel (gmm1, gmm2)


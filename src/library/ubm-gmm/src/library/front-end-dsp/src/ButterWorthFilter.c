/*
 *                            COPYRIGHT
 *
 *  bwlp - Butterworth lowpass filter coefficient calculator
 *  Copyright (C) 2003, 2004, 2005 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  info(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"

main (int argc, char **argv) {
  float minFrequency, maxFrequency;
  int samplingRate;
  int filterOrder;
  float *numerator, *denominator;
  FILE *fPole, *fZero;
  int i;

  if( argc < 7 )
  {
    printf("\nButterWorthFilter calculates Butterworth bandpass filter coefficients\n");
    printf("\nUsage: bwbp n fc1 fc2 samplingRate poleFile zeroFile\n");
    printf("  n = order of the filter\n");
    printf("  fc1 = lower cutoff frequency as a fraction of Pi [0,1]\n");
    printf("  fc2 = upper cutoff frequency as a fraction of Pi [0,1]\n");
    return(-1);
  }
  sscanf(argv[1], "%d", &filterOrder);
  sscanf(argv[2], "%f", &minFrequency);
  sscanf(argv[3], "%f", &maxFrequency);
  sscanf(argv[4], "%d", &samplingRate);
  fPole = fopen (argv[5], "w");
  fZero = fopen (argv[6], "w");
  numerator = (float *) malloc((2*filterOrder+1)*sizeof(float));
  denominator = (float *) malloc((2*filterOrder+1)*sizeof(float));
  ButterWorthFilter(minFrequency, maxFrequency, samplingRate, filterOrder,
		    numerator, denominator);
  for (i = 0; i < 2*filterOrder+1; i++)
    fprintf (fZero, "%f ", numerator[i]);
  fprintf(fZero, "\n");
  fclose(fZero);
  for (i = 1; i < 2*filterOrder+1; i++)
    fprintf (fPole, "%f ", denominator[i]);
  fprintf(fPole, "\n");
}

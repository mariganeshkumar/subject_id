function cca_coeff = cca (data1, data2)
	N1 = size (data1,1);
	N2 = size(data2,1);
 	c_xx  = corr (data1,data1);
	c_yy = corr (data2,data2);
	c_xy = corr (data1,data2);
	c_yx = corr (data2,data1);

	r_xx = (chol (c_xx))';	
	inv_r_xx = inv(r_xx);

	r_yy = (chol (c_yy))';
	inv_r_yy = inv(r_yy);

%	[v_x d_x] = eig (inv_r_xx * c_xy * inv(c_yy) * c_yx * inv_r_xx);
%	[v_y d_y] = eig (inv_r_yy * c_yx * inv(c_xx) * c_xy * inv_r_yy);
%	w_x = inv(r_xx') * v_x(:,1);
	
	M_x = inv(c_xx) * c_xy * inv(c_yy) * c_yx;
	M_y = inv(c_yy) * c_yx * inv (c_xx) * c_xy;
	[v_x d_x] = eig(M_x);
	[v_y d_y] = eig(M_y);
	w_x = v_x(:,1);
	w_y = v_y(:,1);
	cca_coeff = (w_x' * c_xy * w_y)/(sqrt(w_x' * c_xx * w_x)*sqrt(w_y' * c_yy * w_y));

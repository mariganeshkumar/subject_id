#!/usr/bin/python
# estimate C and A matrices - E step in hyperparameter estimation
# for T matrix

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *

# arguments description
# T: hdf5 file to store T matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "T:F:w:h")

def printhelp (close = 0):
  print "Usage: ./hype.py -T Tmat.hd5 -F firstorderlist -w wxlist"
  if close == 1:
    quit ()

tmatfname, wxlistname, folist = "", "", "" 

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-F":
      folist = value
    elif option == "-T":
      tmatfname = value
    elif option == "-w":
      wxlistname = value
    elif option == "-h":
      printhelp (1)
    else:
      print "Invalid argument..."
      printhelp (1)

# check if args are empty
if wxlistname == "" or folist == "" or tmatfname == "":
    printhelp (1)

# prepare meta list
wxlistf = open (wxlistname, "r")
wxlist = [ fname.strip () for fname in wxlistf.readlines ()]
wxlistf.close ()

folistf = open (folist, "r")
folist = [ fname.strip () for fname in folistf.readlines ()]
folistf.close ()

listlen = len (folist)
# check if list lengths are same
if listlen != len (wxlist):
    print "Incoherent lengths for input lists. Exiting"
    quit ()

# pair lists
meta = [ (folist[i],wxlist[i]) for i in range (0, listlen)]

tempf = open (wxlist[0], "r")
M,D,R = [ int (i) for i in tempf.readline ().strip ().split ()]
tempf.close ()
# there should be a way to reduce the amount of memory used here for C
C = np.zeros ((M*D,R))
N = np.zeros ((M,listlen))
l = 0
for foname, wxname in meta:
  M,D,nx,fx = ReadFirstOrderStats (foname)
  
  N[:,l] = nx.reshape ((M,))
  l += 1
  fx = fx.reshape ((M*D,1))
  wx, Lx = ReadIVecEst (wxname)
  C += np.dot (fx, wx.transpose ())
tmatfile,mtab,dtab = hdf5open (tmatfname, R,1)   

row = dtab.row
row['dim'] = [M*D,R]
row.append ()
dtab.flush ()

row = mtab.row
for m in range (0,M):
   print m
   A = np.zeros ((R,R))
   # a flag to avoid singularity issues when inverting
   nopts = 1
   for l in range (0,listlen):
       wx, Lx = ReadIVecEst (wxlist[l])
       if N[m,l] > 0. :
           A += (N[m,l] * Lx)
           nopts = 0
   if nopts == 0:           
       A = np.dot (C[m*D:(m+1)*D,:], np.linalg.inv (A))
   # write A to file
   for d in range (0,D):
       row['colvec'] = A[d,:]
       row.append ()
mtab.flush ()
tmatfile.close ()
      

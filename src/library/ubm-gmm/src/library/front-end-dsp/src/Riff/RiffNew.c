/*----------------------------------------------------------------------
* FileName : Riff.c
* Purpose: To get samples from wave file 
* Author: P.G.Deivapalan <deivapalan@lantan.TeNeT.res.in>
* Bugs: Currently supports only WAVE file format
* Created on: 7th July 2005.  
-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Riff.h"

//=================== WriteWaveFile ===================//
int WriteWaveform(RIFFWaveFile *myRIFF, short int *waveform, short int numsamples, FILE *file)
{
	int i = 0;
	int size = 0;
	int result = SUCCESS;
	
	strcpy(myRIFF->header->riffChunkID, "RIFF");
	myRIFF->header->riffChunkDataSize = (numsamples + 36);
	strcpy(myRIFF->header->riffType, "WAVE");
	

	strcpy(myRIFF->header->fmtChunkID,"fmt ");
	myRIFF->header->fmtChunkDataSize = 16;
	myRIFF->header->compressionCode = 1;
	myRIFF->header->numberOfChannel = 1;
	myRIFF->header->sampleRate = 16000;
	myRIFF->header->averageBytesPerSecond = 32000;
	myRIFF->header->blockAlign = 2;
	myRIFF->header->sigBytesPerSample = 16;

	strcpy(myRIFF->header->dataChunkID,"data");
	myRIFF->header->dataChunkDataSize = numsamples;
	
	size = myRIFF->header->dataChunkDataSize;
	myRIFF->data = (short int*)malloc( size );
	for(i=0; i<size/2; i++)
	{
		myRIFF->data[i] = waveform[i];
	}

	result = WriteRIFFWaveFile(myRIFF, size, file);

	return result;
}

//=================== WriteRIFFWaveFile ===================//
int WriteRIFFWaveFile( RIFFWaveFile *riff, int size, FILE *file)
	{
	int result = SUCCESS;

	if( fwrite( riff->header, 44, 1, file) != 1) 
	{
		result = FAILURE;
	}

	if( fwrite( riff->data, size, 1, file ) != 1)
	{
		result = FAILURE;
	}

	return result;
	}

//=================== ReadRIFFWaveFile ===================//
int ReadRIFFWaveFile( FILE *file, RIFFWaveFile *riff )
	{
	int result = FAILURE;
	int size = 0;
	char temp[4];
	long temp1;

	fseek( file, 0L, SEEK_SET );

	//-------- Read RIFF Header Info ---------------//
	
	if( fread( riff->header, 44, 1, file ) != 1 )
		{
  		fprintf( stderr,"Error Reading the File 1\n" );	
		exit(1);
		}
	
	if ( memcmp( riff->header->riffChunkID, "RIFF", 4 ) != 0 ) 
		{
  		return result;
		}

	if ( memcmp( riff->header->dataChunkID, "data", 4 ) != 0 ) 
		{
 		return result;
		}
		
	//-------- Read Data --------------------------//
	size = riff->header->dataChunkDataSize;
	riff->data = (short int*)malloc( size );
	
	if( fread( riff->data, size, 1, file ) != 1 ) 
		{
 		fprintf( stderr,"Error Reading the File\n" );
		exit(1);
		}

	result = SUCCESS;
	return result;
	}

//=================== PrintRIFFWaveFile ==================//
void PrintRIFFWaveFileInfo( FILE *oFile, RIFFWaveFile *riff )
	{
	int i = 0;
	
	printf("----------------------------------------\n");
	printf("RIFF Chunk\n");
	printf("----------------------------------------\n");
	printf("ID\t\t\t\t");
	for( i = 0; i < 4; i++ )
		printf("%c",riff->header->riffChunkID[i]);
	printf("\n");	
	printf("Data Size\t\t\t");
	printf("%ld\n",riff->header->riffChunkDataSize);
	printf("Type\t\t\t\t");
	for( i = 0; i < 4; i++ )
		printf("%c",riff->header->riffType[i]);
	printf("\n");
	
	printf("----------------------------------------\n");
	printf("Format Chunk\n");
	printf("----------------------------------------\n");
	printf("ID\t\t\t\t");
	for( i = 0; i < 4; i++ )
		printf("%c",riff->header->fmtChunkID[i]);
	printf("\n");
	printf("Data Size\t\t\t");
	printf("%ld\n",riff->header->fmtChunkDataSize);
	printf("Compression Code\t\t");
	printf("%d\n",riff->header->compressionCode);
	printf("Number Of Channels\t\t");
	printf("%d\n",riff->header->numberOfChannel);
	printf("Sample Rate\t\t\t");
	printf("%ld\n",riff->header->sampleRate);
	printf("Average Bytes Per Second\t");
	printf("%ld\n",riff->header->averageBytesPerSecond);
	printf("Block Align\t\t\t");	
	printf("%d\n",riff->header->blockAlign);
	printf("Significant Bytes Per Sample\t");
	printf("%d\n",riff->header->sigBytesPerSample);
	
	printf("----------------------------------------\n");
	printf("Data Chunk\n");
	printf("----------------------------------------\n");
	printf("ID\t\t\t\t");
	for( i = 0; i < 4; i++ )
		printf("%c",riff->header->dataChunkID[i]);
	printf("\n");
	printf("Data Size\t\t\t");
	printf("%ld\n",riff->header->dataChunkDataSize);

	for( i = 0; i < riff->header->dataChunkDataSize/2;)
		{
		fprintf( oFile,"%d\n",riff->data[i] );
		i++;
		}
	return;
	}

//xxxxxxxxxxxxxxxxxxxxxxxx CUT HERE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

//Appendix A

//****************** RIFF FORMAT CHUNK *********************

//Chunk Id
//Always end with the space character (0x20).

//Compression Code Description
//Code 		Description
//0 (0x0000)	UnKnown
//1 (0x0001)	PCM/Uncompressed
//2 (0x0002)	Microsoft ADPCM
//6 (0x0006)	ITU G.711 a-law
//7 (0x0007)	ITU G.711 mu-law
//17 (0x0011)	IMA ADPCM
//20 (0x0016)	ITU G.723 ADPCM (Yamaha)
//49 (0x0031)	GSM 6.10
//64 (0x0040)	ITU G.721 ADPCM
//80 (0x0050)	MPEG
//   (0xFFFF)	Experimental 

//Number of Channels
//1 - mono signal, 2 - stereo signal.

//Average Bytes Per Second
//AverageBytesPerSecond = SampleRate * BlockAlign.

//BlockAlign
//BlockAlign = SignificantBitsPerSample / 8 * NumChannels.

//Significant Bits Per Sample
//Its usually 8,16,24, or 32. Must be aligned to nearest byte (multiples of 8).

//ExtraFormatBytes
//Its not there for Uncompressed PCM file. If present, must be word aligned (multiples of 2).


//Appendix B

//****************** RIFF DATA CHUNK ************************

//If the compression code is 1, then the wave data contains raw sample values.

//xxxxxxxxxxxxxxxxxxxxxxxx CUT TILL HERE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


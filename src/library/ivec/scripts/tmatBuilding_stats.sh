#!/bin/bash


if [ $# != 8 ]; then
	echo "USAGE: bash tmatBuilding.sh FeatureList LabelFile UBMFileName CodebookSize FeatDimension reducedDimension dumpDirectory tmatDirectory"
	exit
fi

############################    INPUT   #################################
devFeatList=$1
keyFile=$2
ubmDev=$3
ubmMix=$4
D=$5
R=$6
dumpDir=$7
tmatDevDir=$8
trainFeatList=$9
valFeatList=${10}
testFeatList=${11}

############################### Initialization ##############################
listDumpDir=$dumpDir/lists
tmatDumpDir=$dumpDir/tmat


binDir=library/ivec/bin
scriptsDir=library/ivec/scripts



devStatList=$listDumpDir/devStatList
devWList=$listDumpDir/devWList
devLxList=$listDumpDir/devLxList

trainStatList=$listDumpDir/trainStatList
trainWList=$listDumpDir/trainWList
trainLxList=$listDumpDir/trainLxList


valStatList=$listDumpDir/valStatList
valWList=$listDumpDir/valWList
valLxList=$listDumpDir/valLxList


testStatList=$listDumpDir/testStatList
testWList=$listDumpDir/testWList
testLxList=$listDumpDir/testLxList



ComputeStats=$scriptsDir/topc_stats_chan.py
InitTMat=$binDir/InitTMat
IVectEst=$binDir/IVectEst
ComputeA=$binDir/ComputeA
ComputeC=$binDir/ComputeC
RecomputeT=$binDir/RecomputeT
Whiten=$binDir/Whiten
LDAPlusWCCN=$binDir/LDAPlusWCCN

nCores=15
nPerClass=5

rm -rf $dumpDir 
mkdir -p $listDumpDir $tmatDumpDir  

########################        Stats computation      ########################

echo "Statistics computation ..."
less $devFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

sed 's!.lfcc$!.lfcc.stat!' $devFeatList > $devStatList
sed 's!.stat$!.w!' $devStatList > $devWList
sed 's!.stat$!.lx!' $devStatList > $devLxList


$InitTMat $ubmDev $ubmMix $D $R $tmatDumpDir/TMAT0.txt



count=`less $devStatList |wc -l`
let count1=$count/$nCores
let count1=$count1+1
let var=1
for i in `seq $count1 $count1 $count`
       do
       less $devStatList |head -$i |tail -$count1 >>$listDumpDir/$var.statList
       less $devWList |head -$i |tail -$count1 >>$listDumpDir/$var.wList
       less $devLxList |head -$i |tail -$count1 >>$listDumpDir/$var.lxList
       let var=$var+1
done
let count1=$count-$i
less $devStatList |tail -$count1 >>$listDumpDir/$var.statList
less $devWList |tail -$count1 >>$listDumpDir/$var.wList
less $devLxList |tail -$count1 >>$listDumpDir/$var.lxList


for i in `seq 1 1 10`
do
echo "Iteration $i"
let j=$i-1
echo "Computing i vectors"
seq 1 $nCores |parallel -k -j$nCores --no-notice --bar $IVectEst -l $listDumpDir/{1}.statList $ubmMix $D $R $tmatDumpDir/TMAT$j.txt $listDumpDir/{1}.wList $listDumpDir/{1}.lxList
count=`less $devStatList | wc -l`
echo "Computing A matrix"
#$ComputeA -s 1 -e $count $tmatDumpDir/AMAT$i.txt $devStatList $devLxList $ubmMix $D $R
$ComputeA $tmatDumpDir/AMAT$i.txt $devStatList $devLxList $ubmMix $D $R
echo "Computing C matrix"
$ComputeC $devStatList $devWList $ubmMix $D $R $tmatDumpDir/CMAT$i.txt 
echo "T matrix recomputation"
$RecomputeT $tmatDumpDir/AMAT$i.txt $tmatDumpDir/CMAT$i.txt $tmatDumpDir/TMAT$i.txt $R $ubmMix $D
done
let j=$i-1
cp $tmatDumpDir/TMAT$j.txt $tmatDevDir/TMAT.txt

$IVectEst -l $devStatList $ubmMix $D $R $tmatDevDir/TMAT.txt $devWList -s

$Whiten $devWList $R $tmatDevDir/WMAT.txt

ind=0

[ -f $keyFile ] && echo "KeyFile       : $keyFile"  ||   ind=1

if [ $ind -eq 1 ]; then
        echo "KeyFile not found"
		echo "Skipping WCCN matrix computation"
        exit
fi

echo $LDAPlusWCCN $keyFile $R $nPerClass $tmatDevDir/WMAT.txt $tmatDevDir/WCCNMAT.txt
$LDAPlusWCCN $keyFile $R $nPerClass $tmatDevDir/WMAT.txt $tmatDevDir/WCCNMAT.txt



# run t-matrix estimation based on the em algorithm

# steps to run
initflag=0                          # run initialization step
iterflag=1                              # run iterations
iterno=2                        # start from this iteration number 
                                        # assuming previous iterations are 
                                        # over
numiter=10                                      
  
# the following flags are to help continue an iteration
# from where we previously left off

skipinitivec=0                          # skip i-vector estimation 
copystats=0
copyivecs=0
splitsize=8                             # split size for map reduce operations 
                                        # for computeA and computeC


# required system level programs
matlab=matlab

# related matlab code
inticode=tmatinit.m

# ubm and other parameters 
ubm=/dirac/ubms/f.mgd
M=1024
D=48
R=500
remote_mc=":,n115/8,n116/8,n117/8,n118/8"
remote_dir=".,/home/srikanth/SpeakerID/src/alt.fa,/home/srikanth/SpeakerID/src/alt.fa,/home/srikanth/SpeakerID/src/alt.fa,/home/srikanth/SpeakerID/src/alt.fa"
num_proc=40
ncorelist="8"
wrkdirlist="."
nmcs=`echo $hostmachines | wc -w`
username=`whoami`

# relevant file names
dumpfolder=eval.matrices            # CHANGE this for every new run
bkupfolder=/poisson/srikanth/matrices.bkup

tmatrixprefix=tmat.mfcc.em
sigma_prefix=sigma.mfcc.em
map_prefix=map.mfcc.em
tmatrixfull=$dumpfolder/$tmatrixprefix
sigma_full=$dumpfolder/$sigma_prefix
fostatlist=lists/statlist.mgd

if [ $initflag -eq 1 ]; then
  echo $matlab -nojvm -nodesktop -nosplash -r "tmatinit ('$tmatrixfull.0', '$ubm', '$sigma_full.0', $M,$D,$R,1)"
  $matlab -nojvm -nodesktop -nosplash -r "tmatinit ('$tmatrixfull.0', '$ubm', '$sigma_full.0', $M,$D,$R,1)"
fi
ncores=0
for item in $ncorelist; do
  ((ncores+=item))
done  

# iteration () is a function that encapsulates one entire
# EM iteration. first, ivectors are estimated. estimation
# involves computing i-vectors. side-effects of this
# estimation involve L matrix. this is also stored in the
# same folder.
#
# input : one argument is sufficient and necessary - iteration number
#
# 

iteration () {
    curriterno=$1
    ((baseno=curriterno-1))

    # check if required files exist
    oldtmatfile=$tmatrixfull.$baseno
    newtmatfile=$tmatrixfull.$curriterno
    new_sigma_file=$sigma_full.$curriterno

    # compute i-vectors with given T-matrix (in parallel)
    if [ $skipinitivec -eq 0 ]; then
      echo bash metaivecest.sh $fostatlist $oldtmatfile $map_prefix.$curriterno $sigma_full.$baseno $remote_mc $remote_dir
    else 
        skipinitivec=0
    fi
    exit

    # recompute T-matrix
    echo $newtmatfile| parallel --dry-run matlab -nojvm -nodesktop -nosplash -r '"em_pca_red ('\'$map_prefix.$curriterno.\', $num_proc, \'{}\',  $M, $D, $R ')"'
    echo $newtmatfile| parallel matlab -nojvm -nodesktop -nosplash -r '"em_pca_red ('\'$map_prefix.$curriterno.\', $num_proc, \'{}\', $M, $D, $R ')"'
    cp $map_prefix.$curriterno.* $newtmatfile $bkupfolder
    rm -f $map_prefix.$curriterno.*
}

if [ $iterflag -ne 1 ]; then
  exit
fi

for i in `seq $iterno $numiter`;do
    iteration $i
done


/**************************************************************************
 *  $Id: SignalReconstruction.c,v 1.1 2000/07/24 02:07:25 hema Exp hema $
 *  File:       SignalReconstruction.c - Computes the FFT Spectrum
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 28-Jan-2003 17:31:58 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	The following program attempts to iterative reconstruct a clipped
signal.
*******************************************************************************/       void Usage() {
           printf("Usage : SignalReconstruction ctrlFile clipWaveFile clipInfoFile reconsFile frameStart frameEnd scale numIter\n");
}

/**************************************************************************
 * Find the closest unclipped value in a waveform to the index k.

 **************************************************************************/
int ClosestUnclipped (float *array, int k) {
  int index = k - 1;
  int flag = 0;
  while (index >= 0 && !flag)
    if (array[index] == 0.0)
      flag = 1;
    else
      index--;
  return(index);
}
/*****************************************************************************/

int        main (int argc, char *argv[])
{ 

  float           *signal=NULL, *orgSignal=NULL;
  int  	          i, k, fftOrder, fftSize, frameSize, numFrames, samplingRate,
                  frameNum, frameAdvanceSamples, frameStart, frameEnd, lowerLimit, upperLimit,
                  fftSizeBy2, numIter, fftBandLimit, fftBandLimitBy2, flag;
  long            nSamp;
  float           *FFTSpecOrg=NULL, *FFTPhaseOrg=NULL, *FFTPhase=NULL, *FFTMag=NULL, 
    *ax=NULL, *ay=NULL;
  F_VECTOR        *waveform=NULL, *clipWaveform=NULL;
  complex           *FFTIter=NULL, *FFTIterInverse=NULL;
  float           bandwidth, *energyContour=NULL, *energySmthContour=NULL;
  FILE            *reconsFile=NULL, *controlFile=NULL;
  static ASDF     *asdf1, *asdf2;
  float           scale, energyMax;
/******************************************************************************/
  if (argc != 9) {
    Usage();
    exit(-1); 
  }
  
  controlFile = fopen(argv[1], "r");
  asdf1 = (ASDF *) malloc(1*sizeof(ASDF));
  asdf2 = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf1, controlFile);
  sscanf(argv[5],"%d", &frameStart);
  sscanf(argv[6],"%d", &frameEnd);
  sscanf(argv[7], "%f", &scale);
  sscanf(argv[8], "%d", &numIter);
  //  sscanf(argv[8],"%f",  &bandwidth);
  GsfOpen(asdf1, argv[2]);
  rewind(controlFile);
  InitializeStandardFrontEnd(asdf2, controlFile);
  GsfOpen(asdf2, argv[3]);
  reconsFile = fopen(argv[4],"w");
  frameSize = (int) GetIAttribute(asdf1, "windowSize");
  frameAdvanceSamples = (int) GetIAttribute(asdf1, "frameAdvanceSamples");
  fftOrder  = (int) GetIAttribute(asdf1, "fftOrder");
  fftSize  = (int) GetIAttribute(asdf1, "fftSize");
  samplingRate  = (int) GetIAttribute(asdf1, "samplingRate");
  nSamp = (int) GetIAttribute (asdf1,"numSamples");
  numFrames = (int) GetIAttribute(asdf1, "numFrames");
  bandwidth = (float) GetFAttribute(asdf1,"maxFrequency");
  fftSizeBy2 = fftSize/2;
  Cstore(fftSize);
  printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	  frameSize, fftOrder, fftSize, frameAdvanceSamples);
  signal = (float *) AllocFloatArray(signal, fftSize+1);
  orgSignal = (float *) AllocFloatArray(orgSignal, fftSize+1);
  FFTSpecOrg  = (float *) AllocFloatArray(FFTSpecOrg, fftSize+1);
  FFTPhaseOrg  = (float *) AllocFloatArray(FFTPhaseOrg, fftSize+1);
  FFTMag  = (float *) AllocFloatArray(FFTMag, fftSize+1);
  FFTPhase  = (float *) AllocFloatArray(FFTPhase, fftSize+1);
  ax       = (float *) AllocFloatArray(ax, fftSize+1);
  ay       = (float *) AllocFloatArray(ay, fftSize+1);
  FFTIter  = (complex *) calloc((fftSize+1), sizeof(complex));
  FFTIterInverse  = (complex *) calloc((fftSize+1), sizeof(complex));
  energyContour = (float *) AllocFloatArray(energyContour, numFrames+1);
  energySmthContour = (float *) AllocFloatArray(energySmthContour, numFrames+1);
  fftBandLimit = (int) (ceilf ((float) bandwidth*2/(float) samplingRate * fftSize));
  fftBandLimitBy2 = fftBandLimit/2;
  if ((frameStart == -1) && (frameEnd == -1 )){
    frameStart = 0;
    frameEnd = numFrames - 1;
  }
  for (frameNum = frameStart; frameNum <= frameEnd; frameNum++) 
    energyContour[frameNum] = sqrt(((F_VECTOR *)GsfRead(asdf1, frameNum, "frameEnergy"))->array[0]/2.0);
  energyMax = energyContour[Imax0Actual(&energyContour[frameStart], frameEnd-frameStart)];
  for (frameNum = frameStart; frameNum <= frameEnd; frameNum++) 
    energyContour[frameNum] = energyContour[frameNum]/energyMax;
  MedianSmoothArray (&energyContour[frameStart-1], frameEnd-frameStart+1, 5, energySmthContour);
  for (frameNum = frameStart; frameNum <= frameEnd; frameNum++) {
    printf (" frameNum = %d\n", frameNum);
    waveform = (F_VECTOR *) GsfRead(asdf1, frameNum, "frameWaveform");
    clipWaveform = (F_VECTOR *)GsfRead(asdf2, frameNum, "frameWaveform");
    flag = 0;
    i = 0;
    while ((i < frameSize) && !flag) {
      flag = clipWaveform->array[i] == 1.0;
      i++;
    }
    if (flag) {
      for (i = 1; i <= frameSize; i++){
	signal[i] = waveform->array[i-1];
	orgSignal[i] = waveform->array[i-1];
	printf("waveform %d = %f\n", i-1, waveform->array[i-1]);
        if ((i > 1) && (i < frameSize)) {
	  if ((clipWaveform->array[i-2] == 0) && (clipWaveform->array[i-1] == 1)) {
	    orgSignal[i] = waveform->array[i-1]*energySmthContour[frameNum]*scale;
	    lowerLimit = i-1;
	  }
	  if ((clipWaveform->array[i-2] == 1) && (clipWaveform->array[i-1] == 0)) {
	    orgSignal[i] =waveform->array[i-1]*energySmthContour[frameNum]*scale;
	    upperLimit = i-1;
	    signal[(lowerLimit+upperLimit)/2] = (signal[lowerLimit] + signal[upperLimit])/2.0*
	      energySmthContour[(lowerLimit+upperLimit)/2]*scale;
	    
	/*else
	  signal[i] = 0.0;*/
	  }
    
	}
      }
	  
      for (i = frameSize+1; i <= fftSize; i++) {
	orgSignal[i] = 0;
	signal[i] = 0;
      }
      Rfft(orgSignal,ax, ay, fftOrder, fftSize, -1);
      SpectrumReal(fftSize, ax, ay, FFTSpecOrg, FFTPhaseOrg);
      Rfft(signal,ax, ay, fftOrder, fftSize, -1);
      SpectrumReal(fftSize, ax, ay, FFTMag, FFTPhase);
      for (i = 1; i <= numIter; i++) { 
	for (k = 1; k <= fftBandLimitBy2; k++) {
	  FFTIter[k].re = FFTSpecOrg[k]*cos(FFTPhase[k]);
	  FFTIter[k].im  = FFTSpecOrg[k]*sin(FFTPhase[k]);
	  FFTIter[fftSize-k+1].re = FFTIter[k].re;
	  FFTIter[fftSize-k+1].im = -FFTIter[k].im;
	}
	for (k = fftBandLimitBy2+1; k <= fftSize-fftBandLimitBy2; k++) {
	  FFTIter[k].re = 0.0;
	  FFTIter[k].im  = 0.0;
	}
	Cfft(FFTIter, FFTIterInverse, fftOrder, fftSize, 1);
	for (k = 1; k <= frameSize; k++)
	  if (clipWaveform->array[k-1] == 0) {
	    FFTIterInverse[k].re = orgSignal[k];
	    FFTIterInverse[k].im = 0.0;
	  } else
	    FFTIterInverse[k].im = 0.0;
	for (k = frameSize+1; k <= fftSize; k++) {
	  FFTIterInverse[k].re = 0.0;
	  FFTIterInverse[k].im = 0.0;
	}
	Cfft(FFTIterInverse, FFTIter, fftOrder, fftSize, -1);
	SpectrumComplex (fftSize, FFTIter, FFTMag, FFTPhase);
      } 
      for (k = 1; k <= frameAdvanceSamples; k++) {
	if (clipWaveform->array[k-1] == 1.0) {
	  fprintf(reconsFile, "%d %d %f\n", frameNum, k, FFTIterInverse[k].re);
	}
	else
	  fprintf(reconsFile, "%d %d %f\n", frameNum, k, waveform->array[k-1]);
      } 
    } else
      for (k = 0; k < frameAdvanceSamples; k++)
	fprintf(reconsFile, "%d %d %f\n", frameNum, k, waveform->array[k]);
  }
  free(clipWaveform->array);
  free(waveform->array);
  free(ax);
  free(ay);
  free(FFTMag);
  free(FFTPhase);
  free(FFTSpecOrg);
  free(FFTPhaseOrg);
  free(FFTIterInverse);
  free(FFTIter);
  free(signal);
  free(orgSignal);
  free(energyContour);
  free(energySmthContour);
  GsfClose(asdf1);
  GsfClose(asdf2);
  fclose(reconsFile);
  fclose(controlFile);
  return 0;
}
/**************************************************************************
 * $Log: SignalReconstruction.c,v $
 * Revision 1.1  2000/07/24 02:07:25  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of SignalReconstruction.c
 **************************************************************************/




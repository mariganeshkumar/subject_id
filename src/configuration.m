
classdef configuration
    
    properties ( Constant = false )
        
        %% Random seed value.
        seed=1337;
        
        %% Directory in which the continues data are stored; Data needs to be present for the program to run.
        data_dir='../data';
        tuh_data_dir='~/datasets/tuh_eeg/www.isip.piconepress.com/projects/tuh_eeg/downloads/tuh_eeg/v1.1.0/';
        
        %% channels to load TUH data
        tuh_channels={'EEG FZ-LE','EEG F7-LE', 'EEG F8-LE', 'EEG C3-LE','EEG C4-LE', 'EEG T5-LE', 'EEG T6-LE', 'EEG O1-LE', 'EEG O2-LE'};
        
        %% Duration in sec for each task independent trail.
        val_split = 15;
        
        %% number of times the classification should be repeated.
        trials = 1;
        
        %% test only. if set to one training part will be skipped.
        test_only = 1;
        
        %% Directory in which the splitted task independent data are stored; will be created by the program.
        splited_dir='../splited_tuh';
        
        
        %% Directory in which the models will be saved for training and testing will be stored; will be created by the program.
        history_save='../history_tuh';
        
        %% Filename to rename the subjects. Will be created if not present
        subject_id_map_file = '../data/map.csv';
        
        %% number of channels in EEG
        num_channels = 128;
        
        %% Name of the feature to be used
        feature_name = {...
        'spectral_power',...            %1
        'LFCC',...                      %2
        'multitaper_spectrogram',...    %3
        };
        feature = 1;
        feature_function={@average_power_spectrogram, @LFCC_from_front_end_dsp, @multitaper_spectrum};
        
        
        %% Name of the classifier to be used
        classifier_name={...
        'K-NN_Channel_Concatnated',...      %1
        'UBM-GMM',...                       %2
        'UBN-GMM_Concatnated',...           %3
        'Super-Vector_SVM ',...             %4
        'lamda-Vector_SVM_99',...           %5
        'lamda-Vector_SVM_Best',...         %6
        'i-vector_SVM',...                  %7
        'i-vector_Cosine',...               %8
        'i-vector_PLDA',...                 %9
        'modified-i-vector_SVM',...         %10
        'modified-i-vector_Cosine',...      %11
        'con-i-vector_Cosine',...           %12
        'con-i-vector_SVM',...           	%13
        'score-fusion-i-vector_Cosine',...  %14
        'score-fusion-i-vector_SVM',...     %15
        'modified-x-vector-classifier',...  %16
        'modified-x-vector_SVM',...         %17
        'modified-x-vector_Cosine',...      %18
        'con-x-vector_Cosine',...           %19
        'con-x-vector_SVM',...              %20
        'score-fusion-x-vector_Cosine',...  %21
        'score-fusion-x-vector_SVM',...     %22
        'x-vector-Classifer',...            %23
        'x-vector_Cosine',...               %24
        'x-vector_SVM',...                  %25
        };
        classifier = 7;
        
        classifier_function={
            @classify_using_knn,...
            @classify_using_ubm_gmm,...
            @classify_using_ubm_gmm_con,...
            @classify_using_super_vector,...
            @classify_using_lamda_vector,...
            @classify_using_lamda_vector_best,...
            @classify_using_i_vector,...
            @classify_using_i_vector_cosine,...
            @classify_using_i_vector_PLDA,...
            @classify_using_i_vector_chan_stat,...
            @classify_using_i_vector_chan_stat_cos,...
            @classify_using_con_i_vector_cosine,...
            @classify_using_con_i_vector_svm,...
            @classify_using_score_fusion_i_vector_cosine,...
            @classify_using_score_fusion_i_vector_svm,...
            @classify_using_mod_x_vector,...
            @classify_using_mod_x_vector_svm,...
            @classify_using_mod_x_vector_cosine,...
            @classify_using_con_x_vector_cosine,...
            @classify_using_con_x_vector_svm,...  
            @classify_using_score_fusion_x_vector_cosine,...
            @classify_using_score_fusion_x_vector_svm,...
            @classify_using_x_vector_naive,...
            @classify_using_x_vector_naive_cosine,...
            @classify_using_x_vector_naive_svm,...
            };
        
        
        %% Name of the lobe to be used
        lobe_name = {...
        'All_Channels',...      %1
        'Frontal',...           %2
        'Parietal',...          %3
        'Temporal',....         %4
        'Occipital',...         %5
        '8-Channel',...         %6
        '9-Channel-tuh',...     %7
        };
        
        lobe = 7;
        lobe_map = {...
            1:128,...
            [1 2 3 4 8 9 10 11 14 15 16 18 19 21 22 23 24 25 26 27 32 33 38 121 122 123 124],...
            [52 53 54 55 58 59 60 61 62 63 64 72 77 78 79 85 86 91 92 95 96 99 100 107],...
            [43 44 48 49 50 56 57 101 113 114 119 120],...
            [65 66 68 69 70 71 73 74 75 76 81 82 83 84 88 89 90 94],...
            [11, 70, 83, 41, 103, 122, 33, 58, 96],...
            1:9,...
            };
        
        
        %% Sampling rate used to record data
        samp_rate =250;
        
        %% window size in ms
        win_size = 360;
        
        
        %% overlap size in ms
        overlap = 0;
        
        %% no of fft points
        nfft = 256
        
        %% low frequency limit in Hz
        %todo: replace this with bands
        lfreq = 3
        
        %% high frequency limit in Hz
        %todo: replace this with bands
        hfreq = 30
        
        %% Dir for saving the features
        features_dir='../computed_features_tuh'
        
        
        %% FFT order (log_{2} of fft size) --> need only for LFCC
        fftorder = 10;
        
        %% Number of filters --> need only for LFCC
        numfilters = 10;
        
        %% Number of Ceps --> need only for LFCC
        numceps = 4;
        
        %% Delta Needed ?--> need only for LFCC
        delta_1 = 1;
        
        %% Delta^2 needed? --> needed only for LFCC
        delta_2 = 1;
        
        %% hidden layer config for ANN
        hiddenlayers = [512 256 100];
        
        %% UBM Parameters
        mixtures=128;
        ivec_dim=50;
        initsize=50;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Parameters that
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% doesn't change
        
        %% Directory to save models
        ubm_model_dir = 'ubm_model';
        ivec_prog_mat_dir = 'ivec_prog_mat_files';
        dnn_model_files = 'dnn_models';
        
        %% tmp dir to save ubm data
        tmp_dir = 'tmp';
        ubm_data_dir = 'tmp/ubm_data';
        ubm_temp_dir = 'tmp/ubm_tmp_files';
        ubm_train_data_dir = 'tmp/ubm_train';
        ubm_val_data_dir = 'tmp/ubm_val';
        ubm_test_data_dir = 'tmp/ubm_test';
        ivec_temp_dir = 'tmp/ivec_tmp_files';
        
        %% Percent data to be used for Validation
        val_per = 0.25;
        
        
        
        
        
    end
end






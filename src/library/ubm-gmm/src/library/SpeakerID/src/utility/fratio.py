import numpy as np
import sys
sys.path.append ('../ivec')
from Utility import read_ascii_vfv,read_bin_file
import pylab

# data_list is a list of numpy arrays. each numpy array is Nxd matrix
def fratio (data_list, ids):
    len_data_list = len (data_list)
    if ids == None:               
        # assume members of data_list are from diff classes
        ids = range (1, len_data_list + 1)

    uniq_ids = set (ids) 
    num_classes = len (uniq_ids) 
    dim = np.shape(data_list[0])[1]
    mu_perclass = np.zeros ((1, dim))  
    data_list = [ np.mean(d,0) for d in data_list ]
    mu_overall = np.mean (np.vstack (data_list), 0)
    dr = np.zeros ((1,dim))
    nr = np.zeros ((1,dim))
    for id_val in uniq_ids:
        indices = [ i for i in range(0,len_data_list) if ids[i] == id_val ]
        li = len(indices)
        mu_perclass = np.mean (np.vstack ([data_list[i] for i in indices]), 0)   
        nr = nr + li * (mu_perclass - mu_overall)**2
        for i in indices:
            dr = dr + (data_list[i] - mu_perclass) ** 2
    nr = nr / (num_classes-1)
    dr = dr / (len(data_list) - num_classes)
    return  nr / dr 

if __name__ == '__main__':
  num_plots = len (sys.argv[1:])
  plot_idx = 0
  fno = 2
  for ip_file_name in sys.argv[1:]:
    ip_file_hnd = open (ip_file_name, "r")
    spkr_id_set = set([])

    curr_spkr_id = 1
    def get_next_spkr_id ():
        global curr_spkr_id
        while curr_spkr_id in spkr_id_set:
            curr_spkr_id += 1
        return curr_spkr_id

    data_list = []
    id_list = []
    for ln in ip_file_hnd:
        ln_split = ln.split ()
        if len (ln_split) == 1:
            spkr_id = get_next_spkr_id ()
            feat_file_name = ln[0]
        elif len (ln_split) >= 2:
            feat_file_name, spkr_id = ln.split()[0:2]
            if not spkr_id.isdigit():
                print ("Warning: spkr_ids must be integers")
                print ("All spkrs with this id will unfortunately be considered " +
                      "diff")
                spkr_id = get_next_spkr_id            
        else:
            continue
        if fno == 2:
            data_list.append (read_ascii_vfv(feat_file_name))
        else:
            data_list.append (read_bin_file (feat_file_name))
        if data_list[-1] == None:
            print ("Warning: error occured while reading feature file %s " %
                   feat_file_name)
        id_list.append (spkr_id)
    
    F = fratio (data_list, id_list)
    plot_idx += 1
    #pylab.subplot (num_plots,1,plot_idx)
    pylab.plot (np.log(F[0,:]),'-o', linestyle = '--')
        
    ip_file_hnd.close ()
    print "done argument"
  pylab.show ()

function scores = FAST_SPLDA_verification(F,E,train_data,test_data)

nspk = size(train_data,2);
tot_files = size(test_data,2);

B = F*F';
A = B + E;
[VA,DA] = eig(A);
iA=VA*bsxfun(@rdivide,VA',diag(DA));
iA = (iA + iA')/2;

T = A - B*iA*B;
[VT,DT] = eig(T);
iT=VT*bsxfun(@rdivide,VT',diag(DT));
iT = (iT + iT')/2;

Q1 = iA - iT;
Q2 = iA*B*iT;
Q2 = (Q2+Q2')/2;

%Monitor the eigenvalues
% e1  = eig(Q1);
% e2 = eig(Q2);
% nrm_max_e1 = max(-e1);
% nrm_max_e2 = max(e2);
% nrm_max = max(nrm_max_e1,nrm_max_e2);
% e1 = e1/nrm_max;
% e2 = e2/nrm_max;
% Q1 = Q1/nrm_max;
% Q2 = Q2/nrm_max;
% figure;
% plot(sort(-e1,'descend'),'r.'); hold on;
% plot(sort(e2,'descend'),'g.');
% legend('Q1','Q2');
% fprintf(1,'Rank Q1: %i\t',rank(Q1,1e-6));
% fprintf(1,'Rank Q2: %i\n',rank(Q2,1e-6));
% figure;

%Project data
% cF = chol(F'*F);
% Q1 = F*(cF\(cF'\(F'*Q1)));
% Q2 = F*(cF\(cF'\(F'*Q2)));

% [dim,nF] = size(F);
% nG = size(G,2);
% if nF + nG < dim || nF == dim
    nrm2_train = sum(train_data.*(Q1*train_data))';
    nrm2_test = sum(test_data.*(Q1*test_data));

    scores = repmat(nrm2_train,1,tot_files) + repmat(nrm2_test,nspk,1) + ((2*Q2)*train_data)'*test_data;   
% else
% 
%     [UQ2,DQ2] = eig(Q2);
%     D = diag(DQ2);
%     [Ds,idx_eigv] = sort(D,'descend');
%     U = UQ2(:,idx_eigv(1:nF));
%     D2 = Ds(1:nF);
%     Q1 = (U'*Q1)*U;
%     %Q1 = diag(diag(Q1)); fprintf(1,'Assuming jointly diagonal\n');%Check joint diagonal
% 
%     ptrain_data = U'*train_data;
%     ptest_data = U'*test_data;
% 
%     nrm2_train = sum(ptrain_data.*(Q1*ptrain_data))';
%     nrm2_test = sum(ptest_data.*(Q1*ptest_data));
% 
%     scores = repmat(nrm2_train,1,tot_files) + repmat(nrm2_test,nspk,1) + (2*bsxfun(@times,ptrain_data,D2))'*ptest_data;
% end

end

#!/bin/bash

# run ivector estimation by splitting input list
# based on the number of cores available

# currently no remote m/cs are used
matlab=matlab
username=`whoami`

if [ $# -lt 4 ]; then
  echo "Usage: ./metaivecest.sh iplist \
  em_prefix tmatfname \
  [comma_list_of_remote/numcores comma_list_of_wrkdirs]"
  exit
fi
iplist=$1
map_prefix=$3
tmatname=$2
sigma_file=$4

# flags
transferTmatrix=1
copystats=0


numcores=0
if [ $# -gt 4 ]; then
  opt3=$5
  opt4=$6
else
  opt3=":"
  opt4="."
fi

ipfoldername=`head -1 $iplist`
ipfoldername=`dirname $ipfoldername` # assume the given path is relative

echo Input folder name is $ipfoldername

itemno=0
corelist=
remotelist=
wrkdirlist=
lwrkdirlist=
lremotelist=
numremotes=0

# compute the number of total number of cores,
# prepare a list of host and remotes,

for item in `echo $opt3 | sed 's/,/\ /g'`; do
   i1=`echo $item | cut -d '/' -f1`
   i2=`echo $item | cut -d '/' -f2`
   if [ "$i1" == ":" ]; then
     hostidxno=$itemno
     corelist[$itemno]=`parallel --number-of-cores`
   else
     corelist[$itemno]=$i2
   fi
   remotelist[$itemno]=$i1
   echo $i1 $i2
   numcores=`echo ${corelist[$itemno]}+$numcores | bc`
   ((itemno++))
done
numremotes=$itemno
numitems=$numremotes
itemno=0

echo Number of remotes is $numremotes

# load working directories in a list
for item in `echo $opt4 | sed 's/,/\ /g'`; do
  wrkdirlist[$itemno]=$item
  if [ "${remotelist[$itemno]}" != ":" -a $transferTmatrix -ne 0 ]; then
    tmatdir=`dirname $tmatname`
    ssh ${remotelist[$itemno]} mkdir ${wrkdirlist[$itemno]}/$tmatdir/
#   changing the scp command to rsync. should save a lot of time
    rsync -av $tmatname $username@${remotelist[$itemno]}:${wrkdirlist[$itemno]}/$tmatdir/
  fi
  ((itemno++))
done
# create list of launchers and i/o dirs in remote

itemno=0
((numitems--))
for i in `seq 0 $numitems`; do
  if [ "${remotelist[$i]}" == ":" ]; then
    sshprefix=""
  else
    sshprefix="ssh ${remotelist[$i]}"
    echo "$sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$ipfoldername"
    $sshprefix 'mkdir -p' ${wrkdirlist[$i]}/$ipfoldername
  fi
  for j in `seq ${corelist[$i]}`; do
      if [ ${remotelist[$i]} == ":" ]; then
        launchlist[$itemno]=""
      else
        launchlist[$itemno]="$sshprefix ${remotelist[$i]}"
      fi
      lremotelist[$itemno]=${remotelist[i]}
      lwrkdirlist[$itemno]=${wrkdirlist[i]}
      ((itemno++))
  done
done
ll=`wc -l $iplist | cut -d ' ' -f1`
((sl=ll/numcores))
((tl=sl*numcores))
if [ $tl -lt $ll ]; then
  ((sl++))
fi  
i=0

rm -f meta.iplist
# create sub lists and copy files to relevant
# folders on the remote


for startno in `seq 1 $sl $ll`; do
  remotemc=${lremotelist[i]}
  tail -n +$startno $iplist | head -$sl > $iplist.$i
    echo CREATING $i $iplist.$i

  if [ "$remotemc" != ":" ]; then    
      echo remote is not local host
      rsync -av --files-from=$iplist.$i . $username@$remotemc:${lwrkdirlist[$i]}/
      scp $iplist.$i $username@$remotemc:${lwrkdirlist[$i]}
      echo "Launching process"
      iplist_new=`basename $iplist.$i`
      echo $tmatname | parallel -S 1/$remotemc --wd ${lwrkdirlist[$i]} 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "em_pca_map('\'{}\',\'$map_prefix.$i\', \'$iplist_new\' ')" > /dev/null' &
      echo $tmatname | parallel --dry-run -S 1/$remotemc --wd ${lwrkdirlist[$i]} 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "em_pca_map('\'{}\',\'$map_prefix.$i\', \'$iplist_new\' ')" > /dev/null' &
  else
      echo $tmatname | parallel -j 1 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "em_pca_map('\'{}\',\'$map_prefix.$i\', \'$iplist.$i\' ')" > /dev/null' &
      echo $tmatname | parallel --dry-run -j 1 'matlab -singleCompThread -nojvm -nosplash -nodesktop -r "em_pca_map('\'{}\',\'$map_prefix.$i\', \'$iplist.$i\' ')" > /dev/null' &
  fi
  ((i++))
done
wait;
# copy all the result folders and delete input folders
for i in `seq 0 $numitems`; do
  if [ "${remotelist[$i]}" != ":" ]; then
    scp -r $username@${remotelist[$i]}:${wrkdirlist[$i]}/$map_prefix.$i .
  fi  
done  

#rm $iplist.[0-9+]

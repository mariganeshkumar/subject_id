#include "stdio.h"
#include "string.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"


void Usage() {
  printf ("Usage: ../bin/ComputeCuSum controlFile waveFile inEntropyFile inThreshold outCuSumFile startFrame endFrame \n");
}

void main (int argc, char *argv[])
{
  char            line[500];
  FILE            *controlFile=NULL;
  FILE            *inEntropyFile=NULL, *outCusumFile=NULL;
  float           timeIndex, entropy, averageEntropy, averageDervEntropy, threshold;
  float           *highEntropy=NULL, *smthHighEntropy=NULL, *derivativeEntropy=NULL, *smthDervEntropy = NULL;
  int             startFrame, endFrame, numFrames, medianOrder;
  int             frameShift, samplingRate;
  float           cuSumEntropy, cuSumDervEntropy;
  int             i;
  static ASDF     *asdf;

 if (argc != 8) {
   Usage();
   exit(-1);
 }
 controlFile = fopen(argv[1], "r");
 inEntropyFile = fopen (argv[3], "r");
 sscanf(argv[4], "%f", &threshold);
 asdf = (ASDF *) malloc(1*sizeof(ASDF));
 InitializeASDF(asdf);
 InitializeStandardFrontEnd(asdf,controlFile);
 GsfOpen(asdf, argv[2]);
 outCusumFile = fopen (argv[5],"w");
 sscanf(argv[6],"%d",  &startFrame);
 sscanf(argv[7], "%d", &endFrame);
 frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
 samplingRate = (int) GetIAttribute(asdf, "samplingRate");
 medianOrder = (int) GetIAttribute(asdf, "medianOrder");
 numFrames = (int) GetIAttribute(asdf, "numFrames");
 highEntropy = (float *) AllocFloatArray (smthHighEntropy, numFrames);
 derivativeEntropy = (float *) AllocFloatArray (derivativeEntropy, numFrames);
 smthHighEntropy = (float *) AllocFloatArray (smthHighEntropy, numFrames);
 smthDervEntropy = (float *) AllocFloatArray (smthDervEntropy, numFrames);
 printf ("frameShift = %d numFrames = %ld\n", frameShift, numFrames+medianOrder);
 if ((startFrame == -1) && (endFrame == -1)) {
     startFrame = 0;
     endFrame = numFrames;
   }
 i = startFrame;
 averageEntropy = 0;
 while (fgets (line, 200, inEntropyFile) != NULL)  {
   sscanf(line, "%f %f %f", &timeIndex, &highEntropy[i], &entropy);
   averageEntropy = averageEntropy+highEntropy[i];
   if (i != 0) {
     derivativeEntropy[i] = highEntropy[i] - highEntropy[i-1];
   averageDervEntropy = averageDervEntropy + derivativeEntropy[i];
   }
   i++;
 }
 derivativeEntropy[0] = derivativeEntropy[1];
 averageDervEntropy = averageDervEntropy + derivativeEntropy[1];
 MedianSmoothArray(highEntropy, i-1, medianOrder, smthHighEntropy);
 MedianSmoothArray(derivativeEntropy, i-1, medianOrder, smthDervEntropy);
 averageEntropy = averageEntropy/(endFrame-startFrame);
 averageDervEntropy = averageDervEntropy/(endFrame-startFrame);
 for (i = startFrame; i < endFrame; i++) {
   smthHighEntropy[i] = smthHighEntropy[i] - averageEntropy*threshold;
   smthDervEntropy[i] = smthDervEntropy[i] - averageDervEntropy*threshold;
 }
  i = startFrame;
 cuSumEntropy = 0;
 cuSumDervEntropy = 0;
 while (i < endFrame){
   if (smthHighEntropy[i]  < 0) 
     cuSumEntropy = 0;
   else
     cuSumEntropy = cuSumEntropy + smthHighEntropy[i];
   if (smthDervEntropy[i]  < 0) 
     cuSumDervEntropy = 0;
   else
     cuSumDervEntropy = cuSumDervEntropy + smthDervEntropy[i];
   fprintf(outCusumFile, "%f %f %f %f %f\n", (float) (i*frameShift)/samplingRate, cuSumEntropy, cuSumDervEntropy, smthHighEntropy[i], smthDervEntropy[i]);
   i++;
  

 }
 fclose(inEntropyFile);
 fclose(controlFile);
 fclose(outCusumFile);
 // free(highEntropy);
 //free (smthHighEntropy);
 //free(derivativeEntropy);
 //free(smthDervEntropy);
 printf("files closed\n");
 fflush(stdout);
}














/*
 * =====================================================================================
 *
 *       Filename:  DumpFeatures_2004_2.c
 *
 *    Description:  Dumps features for nist 2004 type of waveforms into a file with
 *                  proper header structure - dim, numVectors
 *
 *        Version:  1.0
 *        Created:  Sunday 29 May 2011 12:04:02  IST
 *       Compiler:  gcc
 *
 * =====================================================================================
 */
#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {
	char		*controlFileName, *ubmFileName, *featureName, *modelFileName, 
          *trainFileName, *tgname, *outFileName;
	FILE		*controlFile, *ubmFile, *modelFile, *tgFile, *opf, *trainFile;
	VFV *vfv, *ubmMeans, *ubmVars, *adaptedMeans;
  VFV *ergMeans, *ergVars;
  float *ergWts;
  int numGaussErg;
	F_VECTOR	    *ubmWeights;
	ASDF 		    *asdf;
	unsigned int	numVectors, ubmSize, featLength, i, j;
	float 		thresholdScale;
	if (argc != 6)
	  {
		printf ("Usage : %s controlFile spkrList featureName outFile thresholdScale\n", argv[0]);
		exit (0);
	  }
	
  controlFileName = argv[1];
	trainFileName   = argv[2];
	featureName     = argv[3];
  outFileName     = argv[4];
  sscanf (argv[5], "%f", &thresholdScale);

	
	controlFile = fopen (controlFileName, "r");
	asdf = (ASDF *) calloc (1, sizeof (ASDF));
	InitializeStandardFrontEnd (asdf, controlFile);		
	Cstore (asdf->fftSize);
  
  trainFile = fopen(trainFileName,"r");
	vfv = (VFV *) ComputeFeatureVectors 
    (asdf,trainFile, featureName, &numVectors, thresholdScale);

	featLength = vfv[0]->numElements;
  printf ("featLength is %d\n", featLength);
  opf = fopen (outFileName,"w");
  fprintf (opf, "%d %d\n", featLength, numVectors);
  finc(i,numVectors,1) {
    finc(j,featLength,1) {
      fprintf (opf," %f",vfv[i]->array[j]);
    }
    fprintf (opf,"\n");
  }
  fclose(opf);
}

#!/usr/bin/python
# Purpose: the program tries to compute first order statistics
#          "given" the top scoring mixtures for an utterance
#          It takes a file and outputs the 
#          statistics - N^(c) and f^(c) - for it. 
#          To compute f^(c) the feature vectors are required

import sys
import getopt
import numpy as np
from Utility import *

# Description of arguments
# d: input vfv dump file 
# o: output file that will contain first order stats 
# u: ubm file
# m: file containing mix ids
optlist, args = getopt.getopt (sys.argv[1:], "o:d:u:m:lbh")

ipislist = 0
ipname, opname = "", ""
dumpfname, ubmfname, tmatfname = "", "", ""
mixfname = ""
ipisbin = 0

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-h":
        print "python fostat.py -d dumpFile -o outFile -u ubm -m topCfile"
        quit ()
    elif option == "-o":
        opname = value
    elif option == "-d":
        dumpfname = value
    elif option == "-m":
        mixfname = value
    elif option == "-u":
        ubmfname = value
        (M, dim, ubm_wts, ubm_means, ubm_vars) = ReadGMM (ubmfname)
    elif option == "-l":
        ipislist = 1
    elif option == "-b":
        ipisbin = 1
    else:
        print "Invalid option %s. Continuing program..." % option



#----------------------------------------------------
#                                                   |
#                 estimate N_x                      |
#                 and f_x                           |
#----------------------------------------------------

if ipislist == 0:
  metalist = [(dumpfname, mixfname, opname)]
  
else:
  tempf = open (dumpfname,"r")
  ipnames = [ fname.strip () for fname in tempf.readlines ()]
  tempf.close ()
  tempf = open (mixfname,  "r")
  mixnames = [ fname.strip () for fname in tempf.readlines ()]
  tempf.close ()
  tempf = open (opname, "r")
  opnames = [ fname.strip () for fname in tempf.readlines ()]
  tempf.close ()
  # TODO: check if the lengths are the same. skipping that for now
  l = len (ipnames)
  metalist = [ (ipnames[i], mixnames[i], opnames[i]) for i in range(0,l)]

for dumpfname, mixfname, opname in metalist:
    idxarray = ReadClosestMix (mixfname) 
    if ipisbin == 0:
      vfv = read_ascii_vfv(dumpfname)
      nvec = np.shape(vfv)[0] 
    else:
      vfv = read_bin_file (dumpfname)
      nvec = np.shape(vfv)[0]
    N = np.zeros ((M,1), dtype = "uint16")  
    fx= np.zeros ((M,dim), dtype = "float")
    for j in range (0, nvec):
          idx = idxarray[j][0]
          N[idx] += 1
          fx[idx,] += vfv[j,]

    for i in range(0, M):
        fx[i,] -= (N[i] * ubm_means[i,])
    for i in range (0,M):
        fx[i,] /= np.sqrt(ubm_vars[i,])

# writing results to output
    opf = open (opname, "w")
# first line contains M and dim value
    opf.write ("%d %d\n" % (M,dim))
# next M lines contain 'N(c)' values for each c
    for m in range (0,M):
      opf.write("%d\n" % N[m])
# next M lines contain fx values  
    for m in range (0,M):
      for d in range (0,dim):
        opf.write (" %e" % fx[m,d])
      opf.write ("\n")
    opf.close ()



/*-------------------------------------------------------------------------
 *  VerifyUtterance.c - Verifies the claim of a speaker
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	 (hema@localhost.localdomain)
 *
 *  Created:        Wed 13-Feb-2002 19:53:50
 *  Last modified:  Wed 06-Mar-2002 10:46:56 by hema
 *  $Id: VerifyUtteranceBG.c,v 1.1 2002/04/19 10:53:19 hema Exp $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/


/************************************************************************
  Function            : VerifyUtterance- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numVq numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 19-Dec-98
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/front-end-defs.h"
#include "fe/front-end-types.h"
#include "fe/DspLibrary.h"
#include "fe/init-asdf.h"
#include "fe/batch-process-waveform.h"
#include "fe/sphere_interface.h"
#include "fe/vq.h"
/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *featureName, int *numVectors) {

F_VECTOR *fvect;
VECTOR_OF_F_VECTORS *vfv;
int i;
int frameNo;

/*printf("enter compute feat vect \n");
  fflush(stdout);*/

  vfv = (VECTOR_OF_F_VECTORS *) calloc(asdf->numFrames, 
				       sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < asdf->numFrames; i++) {
    fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
      if(fvect == NULL) {
      printf("problems fvect\n");
      fflush(stdout);
      exit(-1);
    }
    vfv[i] = fvect;
  }
  GsfClose(asdf);
*numVectors = asdf->numFrames;
printf("total number of frame processed = %d\n",*numVectors);
fflush(stdout); 
return(vfv);
}
/************************************************************************
  Function            : BestCluster - computes the
                        minimum error for a given feature vector

  Inputs              : fvect - feature vector
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
  Outputs             : smallest distance
            
  **************************************************************************/

float BestCluster(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *speakerMean,
		   VECTOR_OF_F_VECTORS *speakerVar, F_VECTOR *speakerWts, int numClusters){
int                          i, j;
float                        *error, minDistortion;
int                          featLength; 
            
/*  printf("enter BestCluster\n");
  fflush(stdout);*/
  featLength = fvect->numElements;
  /* printf("numClusters = %d featLength = %d\n",numClusters,featLength);
     fflush(stdout); */
  error = (float *) calloc (numClusters, sizeof(float)); 
  for (i = 0; i < numClusters; i++) {
    error[i] = 0;
     for (j = 0; j < featLength; j++)
       error[i]  = error[i]  
	           + (fvect->array[j] - speakerMean[i]->array[j])* 
                     (fvect->array[j] - speakerMean[i]->array[j]);
	 /*(speakerVar[i]->array[j]*speakerVar[i]->array[j]) */;
     /*    printf("error %d = %f\n",i,error[i]);
    fflush(stdout);*/
  }
  minDistortion = error[Imin0(error,numClusters)];
  free(error); 
  return(minDistortion);
}

/************************************************************************

  Function            : ComputeDistortion - computes the
                        total distortion for the entire utterance data.

  Inputs              : vfv - feature vectors for an utterance
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
                        claim - claim of utterance
			backGround - index of backGround
  Outputs             : Distortion

  **************************************************************************/

float  ComputeDistortion(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
			 VECTOR_OF_F_VECTORS **speakerMeans, 
			 VECTOR_OF_F_VECTORS **speakerVars, 
			 VECTOR_OF_F_VECTORS *speakerWts, 
			 int numClusters, int claim) {

  
  int                     j;
  float                   distortion;

  /*printf("enter comp dist\n");
    fflush(stdout);*/
    distortion = 0;
    for (j = 0; j < numVectors; j++) {
      distortion = distortion + BestCluster(vfv[j],
					    speakerMeans[claim], 
					    speakerVars[claim], 
					    speakerWts[claim],
					    numClusters);
    }
  return(distortion);
}

/*-------------------------------------------------------------------------
 *  ReadInModels -- Read in the models for Speakers from file
 *    Args:	    numClusters, modelFile, speakerMeans, speakerVars, 
 *                   speakerWts
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
void ReadInModels(FILE *modelFile, int numVqSpk, 
		  int numVqBg, int featLength, int numSpeakers,
		  VECTOR_OF_F_VECTORS **speakerMeans, 
		  VECTOR_OF_F_VECTORS **speakerVars, 
		  VECTOR_OF_F_VECTORS *speakerWts)

{
  int              i, j, k;
  int              numVq;
  char             speakerModel[256],line[256];
  FILE             *speakerFile=NULL;
   
  i = 0;
 while ((fgets(line, 200, modelFile)) && (i <= numSpeakers)) {
    sscanf(line,"%s",speakerModel);
    speakerFile = fopen(speakerModel,"r");
    /*    printf("speakerFile = %s\n",speakerModel);
	  fflush(stdout); */
    if ( i == numSpeakers) {
      for (j = 0; j < numVqBg; j++) {
	fscanf(speakerFile,"%f",&speakerWts[i]->array[j]);
	/*	printf("%d %d %f\n", i, j, speakerWts[i]->array[j]);
	fflush(stdout);
	scanf("%*c"); */
	for(k = 0; k < featLength; k++) {
	  fscanf(speakerFile," %f %f",&speakerMeans[i][j]->array[k],
		 &speakerVars[i][j]->array[k]);       
	  /*printf(" %d %d %d %f %f\n", i, j, k, speakerMeans[i][j]->array[k],
		 speakerVars[i][j]->array[k]);  
		 fflush(stdout); */
	}
      }
    } else {
 
      for (j = 0; j < numVqSpk; j++) {
	fscanf(speakerFile,"%f",&speakerWts[i]->array[j]);
	/*  printf("%f\n",speakerWts[i]->array[j]);
	    fflush(stdout); */
	for(k = 0; k < featLength; k++) {
	  fscanf(speakerFile," %f %f",&speakerMeans[i][j]->array[k],
		 &speakerVars[i][j]->array[k]);       
	  /*	printf(" %f %f\n",speakerMeans[i][j]->array[k],
		speakerVars[i][j]->array[k]);  
		fflush(stdout); */ 
	}      
      }
    }
      i++;  
  }
 printf (" numSpeaker Read = %d \n", i);
  fclose(speakerFile);

}	/*  End of ReadInModels		End of ReadInModels   */



/*-------------------------------------------------------------------------
 *  Usage --  Prints the Usage of the program
 *    Args:	
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/

void Usage() {
   printf (" Usage : VerifyUtteranceBG ctrlFile modelFile numVqSpk numVqBack featureName numSpeakers wavname claim backGround threshold\n");

}	/*  End of Usage		End of Usage   */


/*-------------------------------------------------------------------------
 *  main -- Main function to verify a given utterance
 *    Args:	
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/

main(int argc, char *argv[])
{

  FILE                  *cFile=NULL,*modelFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  int                   numSpeakers, numVqSpk, numVqBg;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, 
                        *speakerModelWts;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k;
  int                   featLength, numVectors;
  float                 distSpeaker, distBackGround, 
                        distortion, threshold;
  int                   claim, backGround;

  if (argc != 11) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  sscanf(argv[3],"%d",&numVqSpk);
  sscanf(argv[4],"%d",&numVqBg);
  featureName = argv[5];
  sscanf(argv[6],"%d",&numSpeakers);
  wavname = argv[7];
  cFile = fopen(cname,"r");
  sscanf(argv[8],"%d", &claim);
  sscanf(argv[9],"%d", &backGround);
  sscanf(argv[10], "%f", &threshold);
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  printf("testfile name = %s\n",wavname);
  fflush(stdout);
  GsfOpen(asdf,wavname);
  vfv = (VECTOR_OF_F_VECTORS *) 
    ComputeFeatureVectors(asdf,featureName, &numVectors);
  featLength = vfv[0]->numElements;


  speakerModelMeans = (VECTOR_OF_F_VECTORS **) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) 
    calloc(numSpeakers+1, sizeof(VECTOR_OF_F_VECTORS ));
  for (i = 0; i < numSpeakers; i++) {
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numVqSpk, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numVqSpk, 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numVqSpk);
    for (j = 0; j < numVqSpk; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);
      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
  }
  speakerModelMeans[numSpeakers] = (VECTOR_OF_F_VECTORS *) 
    calloc(numVqBg, sizeof(VECTOR_OF_F_VECTORS));
  speakerModelVars[numSpeakers] = (VECTOR_OF_F_VECTORS *) 
    calloc(numVqBg, sizeof(VECTOR_OF_F_VECTORS));
  speakerModelWts[numSpeakers] = (F_VECTOR *) AllocFVector(numVqBg);
  for (j = 0; j < numVqBg; j++) {
    speakerModelMeans[numSpeakers][j] = (F_VECTOR *) 
      AllocFVector(featLength);
    speakerModelVars[numSpeakers][j] = (F_VECTOR *) AllocFVector(featLength);
  }
 
 ReadInModels (modelFile, numVqSpk, numVqBg, featLength, numSpeakers, 
		 speakerModelMeans, speakerModelVars, speakerModelWts);
 /*for (i = 0; i <= numSpeakers; i++) {  
    for (j = 0; j < numVqSpk; j++) { 
      if (speakerModelWts != NULL)
	printf("In main %f\n",speakerModelWts[i]->array[j]);
	  fflush(stdout); 
      for(k = 0; k < featLength; k++) {
	if (speakerModelWts != NULL)
	  printf(" In Main %f %f\n", speakerModelMeans[i][j]->array[k],
		 speakerModelVars[i][j]->array[k]);  
	fflush(stdout);  
      }
    }
    }*/
   distSpeaker = (float) ComputeDistortion (vfv, asdf->numFrames, 
					 speakerModelMeans,
					 speakerModelVars, speakerModelWts,
					 numVqSpk,claim);
  distBackGround = (float) ComputeDistortion (vfv, asdf->numFrames, 
					 speakerModelMeans,
					 speakerModelVars, speakerModelWts,
					 numVqBg, backGround);
   distortion = (distBackGround - distSpeaker)/distSpeaker;
  printf("wavefile = %s\n",wavname);
 printf("distSpeaker = %f distBackGround = %f distortion = %f\n", 
	 distSpeaker, distBackGround, distortion);
 printf("distortion = %f threshold = %f\n", distortion, threshold);
  if (distortion > 0.0) 
    printf("%50s Claim as speaker %5d accepted\n",wavname, claim);
  else
    printf("%50s Claim as speaker %5d rejected \n",wavname, claim);

}	/*  End of main		End of main   */


/*-------------------------------------------------------------------------
 * $Log: VerifyUtteranceBG.c,v $
 * Revision 1.1  2002/04/19 10:53:19  hema
 * Initial revision
 *
 * Revision 1.1  2002/04/19 10:28:48  hema
 * Initial revision
 *
 * Revision 1.1  2002/02/18 11:21:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of VerifyUtterance.c
 -------------------------------------------------------------------------*/

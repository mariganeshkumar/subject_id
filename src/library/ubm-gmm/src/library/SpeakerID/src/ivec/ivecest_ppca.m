function dmat = ivecest_ppca(T, iplist)
	W = inv(T' * T) * T';
	list = readlistascell(iplist);
	ll = length(list);
	R = size(T,2);
	dmat = zeros(R,ll);
	for i = 1:ll
		try
			[N fx] = ReadFoStat(list{i});
			dmat(:,i) = W * fx;
		catch
			continue
		end
	end

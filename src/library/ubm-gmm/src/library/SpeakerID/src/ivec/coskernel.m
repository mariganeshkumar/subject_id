function val = coskernel (A, w1, w2)
   A = A';
   x1 = A * w1;
   x2 = A * w2;
   val = x1' * x2 ;
   val = val / (sqrt (x1' * x1) * sqrt (x2' * x2));

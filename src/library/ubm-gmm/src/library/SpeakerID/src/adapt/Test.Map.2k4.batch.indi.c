/****************
 * MAJOR DIFFERENCE: It does batch testing - loads all models and tests.
 * MINOR DIFFERENCE: from TestUtteranceGMM.MAP.2k4.batch is that
 *                   it extracts features on its own
 * This src file is to Test an utterance for adapted models - version. It does top C mixture testing.
 * The user needs to provide normal testutterancegmm params and also append to it
 * ubm file path and value for C. The main function reads all spkr models, reads the
 * ubm model and computes the top C mixtures with ubm's help. With these topC, the
 * the ComputeLikelihood function in the original code has been changed to ComputeTopCLikelihood
 * to take C and corresponding mixture Ids and compute likelihood accordingly
 * This was earlier tested on mean-only adapted models and it worked (for 32 mixtures)! 

The implementational doubts in the 2nd version will be cleared in this version by compute top C
values for every feature vector in the test utterance
*************************/
/************************************************************************
  Function            : TestUtteranceGMM- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numGMM numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, InitAsdf.c,BatchProcessWaveform.c
                         SphereInterface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-02
*******************************************************************/
#include "CommonFunctions.h"

float  *ComputeTopCLikelihood (VECTOR_OF_F_VECTORS *vfv, unsigned long  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence = 0.0;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	 //      if (mixProbValue != 0.0)
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    Distortion[i] = Distortion[i]/numVectors;
    fflush(stdout); 
  }
  evidence = Distortion[0];
  for (i = 1; i < numSpeakers; i++){
    evidence = LogAdd(evidence, Distortion[i]);
  }
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = Distortion[i] - evidence;
  }
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue
};

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }


    return mixtureIds;  
  }


int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL, *outFile = NULL,
                        *speakerFile=NULL, *ubmFile=NULL, *testListFile=NULL,
								*ergSpidModelFile = NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, *ubmFileName = NULL;
  char			*testListFileName = NULL, *outFileName = NULL, *ergSpidModelFileName=NULL;
  char						*featureName = NULL;
  char                  *string1=NULL, *string2=NULL, *resultFolderName;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters, testCaseNo;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  float *ergWts;
  VECTOR_OF_F_VECTORS   *ergMeans, *ergVars;
  int numGaussiansErg = 3;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv,*newVfv;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue;
  unsigned long         numVectors;
  int                   verify, startFrom;
  float                 *Distortion, *ubmWts;
  float                 thresholdScale = 0.0;
  int lastSelectedVectorIdx;
  unsigned long  numSpeechVectors;
  
  if (argc != 12) {
   printf (" Usage : %s ctrlFile modelFile ", argv[0]);
   printf ("numSpeakers testList test/verify(0/1) ubm topC startFrom resultFolder ");
	printf ("triGauss featureName\n");
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  string2 = argv[3];
  sscanf(string2,"%d",&numSpeakers);
  testListFileName = argv[4];
  string1 = argv[5];
  sscanf(string1, "%d", &verify);
  ubmFileName = argv[6];
  sscanf (argv[7], "%d", &cValue);
  sscanf (argv[8], "%d", &startFrom);
  resultFolderName = argv[9];
  ergSpidModelFileName = argv[10];
  featureName = argv[11];

  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);

   ergMeans = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
   ergVars  = (VECTOR_OF_F_VECTORS *) calloc (numGaussiansErg,sizeof (VECTOR_OF_F_VECTORS));
   ergWts = (float *) calloc (numGaussiansErg,sizeof(float));
   for (i=0;i<numGaussiansErg;i++)
     {
	     ergMeans[i] = (F_VECTOR *) AllocFVector (1);
		  ergVars[i]  = (F_VECTOR *) AllocFVector (1);
	  }

   ergSpidModelFile = fopen (ergSpidModelFileName,"r");
   if (ergSpidModelFile == NULL)
     {
	    printf ("Could not open energy-Spid Models file..\n");
		 fflush(stdout);
		 exit(1);
	  }
   featLength = 1;
   for (j=0; j<numGaussiansErg && !feof(ergSpidModelFile);j++)
     {
	    fscanf (ergSpidModelFile,"%e",&ergWts[j]);
		 printf ("%e\n", ergWts[j]);
		 for (k=0;k<featLength;k++)
		   {
		     fscanf(ergSpidModelFile," %e %e", &ergMeans[j]->array[k], &ergVars[j]->array[k]);
			  printf (" %e %e\n", ergMeans[j]->array[k],ergVars[j]->array[k]);
		   }
		 fscanf(ergSpidModelFile,"\n");
		 printf("\n");
	  }
   if (ergSpidModelFileName != NULL)
       fclose (ergSpidModelFile);
  testListFile = fopen (testListFileName,"r");
  if (testListFile == NULL)
    {
	printf ("Could not open testList\n");
	exit(1);
    }
   wavname = (char *) calloc (256, sizeof(char));
   fscanf (testListFile, "%s\n", wavname);
   newVfv = ExtractFeatureVectorsVAD (
	          asdf, wavname, 
				 featureName, &numVectors,
				 ergMeans, ergVars,
				 ergWts);
   featLength = newVfv[0]->numElements;
   rewind(testListFile);
   FreeVfv (newVfv, numVectors);
  

  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    //speakerModel[strlen(speakerModel)+1] = '\0';
    speakerFile = fopen(speakerModel,"r");
    /*printf("speakerFile = %s\n",speakerModel);
    fflush(stdout); */
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  printf ("Read all Models\n");
  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters[0], sizeof (float));

  for (i = 0; i < numClusters[0]; i++)
    {
      fscanf (ubmFile, "%f\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
   	{
	  fscanf (ubmFile," %f %f",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);
  printf ("Read ubm model\n");
  testCaseNo = 0;
  while (!feof (testListFile))
    {
  testCaseNo++;
  fscanf (testListFile,"%s\n", wavname);
  if (testCaseNo < startFrom)
	continue;
  newVfv = ExtractFeatureVectorsVAD (
	          asdf, wavname, 
				 featureName, &numVectors,
				 ergMeans, ergVars,
				 ergWts);
  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, 
           numClusters[0], newVfv, numVectors, 
			  cValue, asdf->probScaleFactor); 
  
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeTopCLikelihood(newVfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   asdf->probScaleFactor, cValue, mixIds);
  outFileName = (char *) calloc (256, sizeof(char));
  sprintf (outFileName, "%s/%d.out",  resultFolderName, testCaseNo);
  outFile = fopen(outFileName, "w");
  if (outFile == NULL)
    {
	printf ("unable to create file %s ... \n", outFileName);
	continue;
    }
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    fprintf(outFile, "Distortion:: %d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    fprintf(outFile, "%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  printf ("Processed file No:%d %s\n", testCaseNo, wavname);
  free(newVfv);
  free(Distortion);
  free (outFileName);
  fclose (outFile);
  }
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  FreeVfv (ergMeans, numGaussiansErg);
  FreeVfv (ergVars, numGaussiansErg);
  free (ergWts);
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

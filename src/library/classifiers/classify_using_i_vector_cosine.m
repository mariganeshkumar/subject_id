function [result] = classify_using_i_vector_cosine(config,allData)

train_data = allData.train_data;
test_data = allData.test_data;
num_subjects=length(train_data);

models_dir =  [allData.modelSaveDir,'/',config.ivec_prog_mat_dir];


system(['rm -rf ', config.tmp_dir]);
mkdir(config.ubm_data_dir);
mkdir(config.ubm_temp_dir);
mkdir([config.ubm_val_data_dir,'/result']);
mkdir([config.ubm_test_data_dir,'/result']);

data_dir=[config.ubm_data_dir,'/'];
ubm_data=allData.ubm_data;
filenames = cell([1, num_subjects]);
subject_data = cell([1, num_subjects]);
clear allData.ubm_data;
if (~config.test_only)
    system(['rm -rf ', models_dir]);
    mkdir(models_dir);
    disp('Preparing UBM Data');
    parfor i=1:length(ubm_data)
        disp(['Subject : ', num2str(i)]);
        subject_data{i}=[];
        filenames{i}={};
        for j=1:length(ubm_data{i})
            for k=1:length(ubm_data{i}{j})
                recording_data=ubm_data{i}{j}{k};
                recording_data=permute(recording_data,[2,3,1]);
                reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
                recording_filename=strcat(data_dir,num2str(i),'_',num2str(j),'_',num2str(k),'.lfcc');
                [dim, no_of_frames] = size(reshaped_recoding_data);
                dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(recording_filename,reshaped_recoding_data','-append','delimiter',' ');
                filenames{i} = {filenames{i}{:},recording_filename}
                subject_data{i}=[subject_data{i} reshaped_recoding_data];
            end
        end
        filenames{i}=SplitAndWriteData(transpose(subject_data{i}),data_dir,num2str(i),2500)
    end

    combined_ubm_data =[];
    for i=1:length(ubm_data)
        combined_ubm_data = [combined_ubm_data subject_data{i}];
    end
    rng(1331);
    rp = randperm(size(combined_ubm_data,2));
    combined_ubm_data = combined_ubm_data(:,rp);

    ubm_filenames=SplitAndWriteData(combined_ubm_data',data_dir,'Shuffled_UBM_Data',2000);
    
    scriptFileID = fopen([config.ubm_data_dir  '/scriptFile.scp'],'w');
    for j =1:length(ubm_filenames)
        fprintf(scriptFileID,'%s \n',ubm_filenames{j});
    end
    fclose(scriptFileID);

    listFileID = fopen([config.ubm_data_dir  '/devDataScriptFile.scp'],'w');
    labelFileID = fopen([config.ubm_data_dir  '/devDataLabelFile.scp'],'w');
    for i =1:length(ubm_data)
        for j =1:length(filenames{i})
            recording_filename=strcat(filenames{i}{j});
            fprintf(listFileID,'%s\n',recording_filename);
            ivector_filename=strcat(filenames{i}{j},'.w');
            fprintf(labelFileID,'%s %d\n',ivector_filename, i);
        end
    end
    fclose(listFileID);
    fclose(labelFileID);
    
    
    
    clear ubm_data allData.ubm_data
    dim = allData.freq_dim;
    system(['sh library/ubm_gmm_scripts/SupervisedGMM.sh ',...
        config.ubm_data_dir,'/scriptFile.scp ' ...
        models_dir,'/UBM_GMM.txt ',...
        num2str(config.mixtures),' ',...
        config.ubm_temp_dir,' ',num2str(1331)]);
    system(['bash library/ivec/scripts/tmatBuilding.sh ',...
        config.ubm_data_dir  '/devDataScriptFile.scp ' ,...
        config.ubm_data_dir  '/devDataLabelFile.scp ',...
        models_dir, '/UBM_GMM.txt_py ',...
        num2str(config.mixtures),' ',...
        num2str(dim),' ',...
        num2str(config.ivec_dim),' ',...
        config.ivec_temp_dir,' ',models_dir, ' ']);
    system(['python2.7 library/ivec/scripts/ComputeLDA.py ',...
        '--devDataScriptFile ',config.ubm_data_dir  '/devDataLabelFile.scp ',...
        '--devMat ',models_dir,'/dev.mat ',...
        '--ldaMat ',models_dir,'/LDA.pkl ',...
        ]);
else
    if exist(models_dir,'dir')~=7
        disp('trained models not found')
        msgID = 'Models:NotFound';
        msg = 'Unable to find trained models.';
        baseException = MException(msgID,msg);
        throw(baseException);
    end
end

disp(['Adapting Models']);
data_dir=config.ubm_train_data_dir;
mkdir(data_dir);
parfor i=1:length(train_data)
    subject_data=[];
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_data=train_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            subject_data = cat(2,subject_data,reshaped_recoding_data)
        end
    end
    [dim, no_of_frames] = size(subject_data);
    recording_filename=strcat(data_dir,'/',num2str(i),'.lfcc');
    dlmwrite(recording_filename,[dim no_of_frames],'delimiter',' ','precision',10);
    dlmwrite(recording_filename,subject_data','-append','delimiter',' ');
end


listFileID = fopen([config.ubm_train_data_dir  '/trainDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_train_data_dir  '/trainDataLabelFile.scp'],'w');
for i=1:length(train_data)
    recording_filename=strcat(data_dir,'/',num2str(i),'.lfcc');
    fprintf(listFileID,'%s\n',recording_filename);
    ivector_filename=strcat(data_dir,'/',num2str(i),'.lfcc.w');
    fprintf(labelFileID,'%s %d\n',ivector_filename, i);
end


clear train_data allData.train_data
disp(['Testing Models']);
test_filenames = cell([1, num_subjects]);
val_filenames = cell([1, num_subjects]);
val_data_dir = [config.ubm_val_data_dir];
mkdir(val_data_dir);
test_data_dir = [config.ubm_test_data_dir];
mkdir(test_data_dir);


val_per = config.val_per;

parfor i=1:length(test_data)
    t=cputime;
    val_ind=1;
    test_ind=1;
    test_filenames{i}={};
    val_filenames{i}={};
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            recording_data=permute(recording_data,[2,3,1]);
            reshaped_recoding_data=reshape(recording_data,[size(recording_data,1), size(recording_data,2)*size(recording_data,3)]);
            [dim, no_of_frames]=size(reshaped_recoding_data);
            if k <= length(test_data{i}{j}) * val_per
                subject_filename=strcat(val_data_dir,'/',num2str(i),'_',num2str(val_ind),'.lfcc');
                val_filenames{i}{val_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                val_ind=val_ind+1;
            else
                subject_filename=strcat(test_data_dir,'/',num2str(i),'_',num2str(test_ind),'.lfcc');
                test_filenames{i}{test_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                test_ind=test_ind+1;
            end
        end
    end
    disp(['Tested subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end

listFileID = fopen([config.ubm_val_data_dir  '/valDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_val_data_dir  '/valDataLabelFile.scp'],'w');
val_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(val_filenames{i})
        temp_name=split(val_filenames{i}{j},'/');
        adapted_filename = [config.ubm_val_data_dir,'/',temp_name{end}];
        fprintf(listFileID,'%s\n',adapted_filename);
        fprintf(labelFileID,'%s %d\n',[adapted_filename,'.w'], i);
        val_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(labelFileID);
fclose(listFileID);


listFileID = fopen([config.ubm_test_data_dir  '/testDataScriptFile.scp'],'w');
labelFileID = fopen([config.ubm_test_data_dir  '/testDataLabelFile.scp'],'w');

test_gt_label=[];
ind=1;
for i =1:num_subjects
    for j =1:length(test_filenames{i})
        temp_name=split(test_filenames{i}{j},'/');
        adapted_filename = [config.ubm_test_data_dir,'/',temp_name{end}];
        fprintf(listFileID,'%s\n',adapted_filename);
        fprintf(labelFileID,'%s %d\n',[adapted_filename,'.w'], i);
        test_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(labelFileID);
fclose(listFileID);


dim = allData.freq_dim;
system(['bash library/ivec/scripts/extractiVec.sh ',...
    models_dir, '/UBM_GMM.txt_py ',...
    num2str(config.mixtures),' ',...
    num2str(dim),' ',...
    num2str(config.ivec_dim),' ',...
    config.ivec_temp_dir,' ',models_dir , ' ', ...
    config.ubm_train_data_dir,'/trainDataScriptFile.scp ',...
    config.ubm_val_data_dir,'/valDataScriptFile.scp ',...
    config.ubm_test_data_dir,'/testDataScriptFile.scp ']);




system(['python2.7 library/ivec/scripts/TrainAndTestCosine.py ',...
    '--trainDataScriptFile ',config.ubm_train_data_dir,'/trainDataLabelFile.scp ',...
    '--valDataScriptFile ',config.ubm_val_data_dir,'/valDataLabelFile.scp ',...
    '--testDataScriptFile ',config.ubm_test_data_dir,'/testDataLabelFile.scp ',...
    '--testResultFolder ',config.ubm_test_data_dir,'/result/ ',...
    '--valResultFolder ',config.ubm_val_data_dir,'/result/ ',...
    '--ldaMat ',models_dir,'/LDA.pkl ',...
    '--scoreMat ',config.tmp_dir,'/scores.mat ',...
    '--saveDir ',config.tmp_dir, ' ']);

make_plots_from_sub_space(config.tmp_dir,[allData.plotSaveDir,'/',num2str(config.val_split)], allData.sessionInfo)



val_predicted_label=[];
for i=1:length(val_gt_label)
    file_ID=fopen([config.ubm_val_data_dir,'/result/', num2str(i),'.out'],'r');
    val_predicted_label(i) = fscanf(file_ID,'label %d \n');
    fclose(file_ID);
end


test_predicted_label=[];
for i=1:length(test_gt_label)
    file_ID=fopen([config.ubm_test_data_dir,'/result/', num2str(i),'.out'],'r');
    test_predicted_label(i) = fscanf(file_ID,'label %d \n');
    fclose(file_ID);
end

result.val_predicted_label = val_predicted_label;
result.val_gt_label = val_gt_label;
result.test_predicted_label = test_predicted_label;
result.test_gt_label = test_gt_label;


data = load([config.tmp_dir,'/scores.mat']);
result.target_test_scores = double(data.target_test_scores);
result.non_target_test_scores = double(data.non_target_test_scores);
result.target_val_scores = double(data.target_val_scores);
result.non_target_val_scores = double(data.non_target_val_scores);
result.target_test_norm_scores = double(data.target_test_norm_scores);
result.non_target_test_norm_scores = double(data.non_target_test_norm_scores);
result.target_val_norm_scores = double(data.target_val_norm_scores);
result.non_target_val_norm_scores = double(data.non_target_val_norm_scores);

end


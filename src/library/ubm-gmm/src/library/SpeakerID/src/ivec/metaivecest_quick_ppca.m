% calls the ivecest function and saves the result
% in a file. just didn't want to change the
% existing function.

% exflag is required because the function need not
% be called from command line

function metaivecest_quick_ppca (tfname, ffname, wfname,exflag, listflag)

% accumulate A and C matrixexflag, listflag)
    if nargin < 5
      'Usage : metaivecest (tfname, ffname, wfname, exflag, listflag)'
      return
    end
    load (tfname,'-mat');
    T = W;
    clear W;
    T = inv(T' * T) * T';
    if listflag == 0
        ii = 1;
        ffl = [ffname];
        wxl = [wfname];
    else
        ffl = readlistascell (ffname);
        wxl = readlistascell (wfname);
        ii = length (wxl);
    end
    [MD R] = size (T);
    eyeR = eye(R);
    
    for i = 1:ii
        % converting functions to inline
        % [Nx fx] = ReadFoStat  (ffl{i});
        % DELETE THIS BLOCK AND UNCOMMENT PREV LINE
        % TO REVERT TO ORIGINAL CODE
        try
            f_hnd = fopen (ffl{i});
            M = fscanf (f_hnd, '%d', 1);
            D = fscanf (f_hnd, '%d', 1);
            N = fscanf (f_hnd, '%d', M);
            fx = fscanf (f_hnd, '%e', M*D);
            fclose (f_hnd);
            w = T * fx;
            save (wxl{i}, 'w', '-ascii');
        catch
            continue
        end

    end
    if exflag ~= 0
        exit
    end


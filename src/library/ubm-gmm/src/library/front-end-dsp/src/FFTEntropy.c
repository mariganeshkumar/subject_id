#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: FFTEntropy.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	FFTEntropy.c - Computes the Entropy of the LPSpectrum (framewise)
 *              of a given signal.
 *
 *  Purpose:	To compute FFTEntropy for music processing
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Mon 19-Aug-2013 17:14:59 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average LP Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the LP Spectrum  of data are written to a file 

*******************************************************************************/       
void Usage() {
           printf("Usage : FFTEntropy ctrlFile waveFile SpecFile entropyChangeFile Avsmth/MedSmth(0/1) windowType entropyThreshold (%% average) frameStart frameEnd sigOutput (optional)\n");
}
/*****************************************************************************/

int  main (int argc, char *argv[])
{ 

        float           *signal=NULL;
	int  	        i, k, fftOrder,fftSize,fftSizeBy2, 
	                frameSize, numFrames,
	                frameNum, medianOrder, frameShift, 
                        frameStart, frameEnd;
        int             samplingRate, smthType;
        float           *ax=NULL, *ay=NULL, *psd=NULL,*FFTSpec=NULL,*FFTPhase=NULL;
        float           *entropy=NULL, *entropySmooth=NULL, entropyThreshold, 
	                minValue, entropyAverage, entropyMax, timeStart, timeEnd,value;
        char            windowType;
    	F_VECTOR        *waveform=NULL;
        FILE            *specFile=NULL, *entropyChangeFile=NULL, *controlFile=NULL, *fpOut=NULL;
        static ASDF     *asdf;
        int             changeDet, flag;
        char            line[500];

/******************************************************************************/
	if ((int)(argc) < 10) {
	  Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       specFile = fopen(argv[3],"w");
       entropyChangeFile = fopen(argv[4],"w");
       sscanf(argv[5], "%d", &smthType);
       sscanf(argv[6], "%c", &windowType);
       sscanf(argv[7], "%f", &entropyThreshold);
       sscanf(argv[8], "%d", &frameStart);
       sscanf(argv[9], "%d", &frameEnd);
       if (argc == 11) 
	 fpOut = fopen(argv[10], "w");
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       medianOrder   = (int) GetIAttribute(asdf, "medianOrder");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d numFrames=%d\n", 
	       frameSize, fftOrder, fftSize, frameShift, numFrames);
       Cstore(fftSize);
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);

       signal = (float *) AllocFloatArray(signal, fftSize+1);
       FFTSpec  = (float *) AllocFloatArray(FFTSpec, fftSize+1);
       ax       = (float *) AllocFloatArray(ax, fftSize+1);
       ay       = (float *) AllocFloatArray(ay, fftSize+1);
       FFTPhase  = (float *) AllocFloatArray(FFTPhase, fftSize+1);
       psd  = (float *) AllocFloatArray(psd, fftSize+1);
       entropy  = (float *) AllocFloatArray(entropy, numFrames);
       entropySmooth  = (float *) AllocFloatArray(entropySmooth, numFrames);
       minValue = 100;
       if ((frameStart == -1) && (frameEnd == -1 )){
         frameStart = 0;
	 frameEnd = numFrames - 1;
       }
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	 for (i = 1; i <= frameSize; i++){
	   //	   printf("waveform %d = %f\n", i-1, waveform->array[i-1]);
	   signal[i] = waveform->array[i-1]*HamDw(i,frameSize);
	 }
	   for (k = 0; k < frameShift; k++)
	     fprintf(fpOut, "%e %e\n", (float) (frameNum*frameShift+k)/samplingRate, signal[k+1]);
	   //printf("signal %d = %f\n", i, signal[i]);
	 free(waveform->array);
	 free(waveform);
         for (i = frameSize+1; i <= fftSize; i++)
           signal[i] = 0;
	 Rfft(signal,ax, ay, fftOrder, fftSize, -1);
	 SpectrumReal(fftSize, ax, ay, FFTSpec, FFTPhase);
	 entropy[frameNum-frameStart] = 0;
	 for (i = 1; i <= fftSizeBy2; i++)
	   entropy[frameNum-frameStart] = entropy[frameNum-frameStart] + FFTSpec[i]*FFTSpec[i];	   
	 for (i = 1; i <= fftSizeBy2; i++) {
	   psd[i] = FFTSpec[i]*FFTSpec[i]/entropy[frameNum-frameStart];
	   if (psd[i] <= 0.0)
	     exit(-1);
	 }
	 entropy[frameNum-frameStart] = 0;
	 for (i = 1; i <= fftSizeBy2; i++)
           if (psd[i] != 0.0)
	     entropy[frameNum-frameStart] = entropy[frameNum-frameStart] - psd[i] * logf(psd[i]);
	 if (!isnan(entropy[frameNum-frameStart])) {
	   entropyAverage = entropyAverage + entropy[frameNum-frameStart];
	   //	   if(entropy[frameNum-frameStart] < minValue)
	   //  minValue = entropy[frameNum-frameStart];
	 }
	 else entropy[frameNum-frameStart] = 0;
	 if ((entropy[frameNum-frameStart] != 0) && (entropy[frameNum-frameStart] < minValue))
	   minValue = entropy[frameNum-frameStart];
	 printf("entropyAverage = %f entropy %d = %f \n", entropyAverage, frameNum-frameStart, entropy[frameNum-frameStart]);
	 fflush(stdout);
       }
       entropyMax = entropy[Imax(entropy, frameEnd-frameStart+1)];
       for (i = 0; i <= frameEnd-frameStart; i++)
	 entropy[i] = entropy[i]/entropyMax;

       entropyAverage = entropyAverage/(frameEnd-frameStart+1)/entropyMax;
       minValue = minValue/entropyMax;
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++)
         if (entropy[frameNum] == 0) entropy[frameNum] = minValue;
       printf ("entropyAverage = %f entropyThreshold = %f\n", entropyAverage, entropyThreshold*entropyAverage);
       if (smthType == 0) {
	   AverageSmoothArray(&entropy[-1], frameEnd-frameStart, medianOrder, windowType, &entropySmooth[-1]);
	   AverageSmoothArray(&entropySmooth[-1], frameEnd-frameStart, medianOrder, windowType, &entropy[-1]);
	   AverageSmoothArray(&entropy[-1], frameEnd-frameStart, medianOrder, windowType, &entropySmooth[-1]);
	 } else
	 MedianSmoothArray(&entropy[-1], frameEnd-frameStart, medianOrder, &entropySmooth[-1]);
       for (i = 0; i < frameEnd-frameStart; i++)
	 if (entropySmooth[i] >= entropyAverage*entropyThreshold) 
	   fprintf(specFile, "%f %f 1 \n", (float)i*frameShift/(float) samplingRate, entropySmooth[i]);
	 else
	   fprintf(specFile, "%f %f 0 \n", (float) i*frameShift/(float) samplingRate, entropySmooth[i]);
       fclose(specFile);
       specFile = fopen (argv[3], "r");
       fgets (line, 500, specFile);
       sscanf(line, "%e %e %d", &timeStart, &value, &changeDet);
       if (changeDet == 0) {
	 fprintf (entropyChangeFile, "%e ", timeStart);
	 flag = 1;
       }
       timeEnd = timeStart;
       while (!(feof(specFile))) {
	 while ((fgets (line, 500, specFile) != NULL) && (changeDet == 0)) {
		timeEnd = timeStart;
		sscanf(line, "%e %e %d", &timeStart, &value, &changeDet);
	 }
	 if (flag) {
	   fprintf (entropyChangeFile,"%e \n", timeEnd);
	   flag = 0;
	 }
	 while ((fgets (line, 500, specFile) != NULL) && (changeDet == 1)) {
	     sscanf(line, "%e %e %d", &timeStart, &value, &changeDet);
	 }
	 if (!flag) {
	   fprintf (entropyChangeFile,"%e ", timeStart);
	   flag = 1;
	 }
       }
       if (flag) 
	 fprintf(entropyChangeFile, "%e ", timeStart);
       fclose(specFile);
       fclose(entropyChangeFile);
       free(ax);
       free(ay);
       free(FFTPhase);
       free(signal);
       free(FFTSpec);
       free(entropy);
       free(psd);
       free(entropySmooth);
       if (argc == 10)
         fclose(fpOut);
       return(0);
}
/**************************************************************************
 * $Log: FFTEntropy.c,v $
 * Revision 1.1  2000/07/24 02:07:57  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of FFTEntropy.c
 **************************************************************************/




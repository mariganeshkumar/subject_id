#ifndef __AdaptMeans__
#define __AdaptMeans__
#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "BatchProcessWaveform.h"
#include "SphereInterface.h"
#include "math.h"
#include "GMM.h"
#include "CommonFunctions.h"

VECTOR_OF_F_VECTORS * AdaptOnlyMeans (VECTOR_OF_F_VECTORS *ubmMeans, 
				    VECTOR_OF_F_VECTORS *ubmVars,
				    float *ubmWeights,
				    unsigned int numClusters,
			            VECTOR_OF_F_VECTORS *featureVectors,
				    unsigned int numVectors,
				    unsigned int relevanceFactor,
				    float probScaleFactor);
#endif

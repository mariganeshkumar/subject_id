function [val_predicted_label, val_gt_label, test_predicted_label, test_gt_label] = classify_using_ubm_gmm_con(config,allData)


train_data = allData.train_data;
test_data = allData.test_data;
num_subjects=length(train_data);
%isDeleteTMPDir=input('Do you want to retrain the UBM? 1.yes 2. no');

%if isDeleteTMPDir==1
system(['rm -rf ', config.tmp_dir]);
mkdir(config.ubm_data_dir);
mkdir(config.ubm_temp_dir);
mkdir(config.ubm_model_dir);
mkdir([config.ubm_val_data_dir,'/result']);
mkdir([config.ubm_test_data_dir,'/result']);

data_dir=[config.ubm_data_dir,'/'];
ubm_data=allData.ubm_data;
filenames = cell([1, num_subjects]);
clear allData.ubm_data;
disp('Preparing UBM Data');
parfor i=1:length(ubm_data)
    disp(['Subject : ', num2str(i)]);
    subject_data=[];
    for j=1:length(ubm_data{i})
        for k=1:length(ubm_data{i}{j})
            recording_data=ubm_data{i}{j}{k};
            reshaped_recoding_data = reshape(...
                recording_data,...
                [...
                size(recording_data,1)*...
                size(recording_data,2),...
                size(recording_data,3)...
                ]...
                );
            subject_data=[subject_data reshaped_recoding_data];
        end
    end
    filenames{i}=SplitAndWriteData(subject_data',data_dir,num2str(i),30000)
end
scriptFileID = fopen([config.ubm_data_dir  '/scriptFile.scp'],'w');
for i =1:length(ubm_data)
    for j =1:length(filenames{i})
        fprintf(scriptFileID,'%s \n',filenames{i}{j});
    end
end
fclose(scriptFileID);
%end
%gmmDist = TrainAndLoadLabGMM([config.ubm_data_dir  '/scriptFile.scp'], config.mixtures,1331,config.ubm_model_dir,config.ubm_temp_dir);
clear ubm_data allData.ubm_data  ubm_cell
system(['sh library/ubm_gmm_scripts/SupervisedGMM.sh ',...
    config.ubm_data_dir,'/scriptFile.scp ' ...
    config.ubm_model_dir,'/UBM_GMM.txt ',...
    num2str(config.mixtures),' ',...
    config.ubm_temp_dir,' ',num2str(1331)]);


disp(['Adapting Models']);
%subjectGMMs=cell([1 num_subjects]);
parfor i=1:length(train_data)
    t=cputime;
    subject_data=[];
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_data=train_data{i}{j}{k};
            reshaped_recoding_data = reshape(...
                recording_data,...
                [...
                size(recording_data,1)*...
                size(recording_data,2),...
                size(recording_data,3)...
                ]...
                );
            subject_data=[subject_data, reshaped_recoding_data];
        end
    end
    [dim, no_of_frames] = size(subject_data);
    subject_filename=strcat(config.ubm_data_dir,'/',num2str(i),'.lfcc');
    dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
    dlmwrite(subject_filename,subject_data','-append','delimiter',' ');
    disp(['Wrote subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end
scriptFileID = fopen([config.ubm_model_dir  '/adaptationScript.sh'],'w');
listFileID = fopen([config.ubm_model_dir  '/modelsScriptFile.scp'],'w');
for i=1:length(train_data)
    subject_filename=strcat(config.ubm_data_dir,'/',num2str(i),'.lfcc');
    fprintf(scriptFileID, ['library/ubm-gmm/bin/OldAdaptGMM ',...
        config.ubm_model_dir,'/UBM_GMM.txt_py ',...
        num2str(config.mixtures),' ',...
        subject_filename,' ',...
        config.ubm_model_dir,'/',num2str(i),'.model ',...
        num2str(16), '\n']);
    %subjectGMMs{i}=AdaptGMM(subject_data',gmmDist,16);
    %writeLabGMM(subjectGMMs{i},[config.ubm_model_dir,'/',num2str(i),'.model'])
    fprintf(listFileID,'%s %d\n',[config.ubm_model_dir,'/',num2str(i),'.model'],config.mixtures);
    
end
system(['less ', config.ubm_model_dir,'/adaptationScript.sh | parallel -v -j12'])
fclose(scriptFileID);
fclose(listFileID);
clear train_data allData.train_data
disp(['Testing Models']);
test_filenames = cell([1, num_subjects]);
val_filenames = cell([1, num_subjects]);
val_per = config.val_per;

parfor i=1:length(test_data)
    t=cputime;
    val_ind=1;
    test_ind=1;
    test_filenames{i}={};
    val_filenames{i}={};
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            reshaped_recoding_data = reshape(...
                recording_data,...
                [...
                size(recording_data,1)*...
                size(recording_data,2),...
                size(recording_data,3)...
                ]...
                );
            [dim, no_of_frames]=size(reshaped_recoding_data);
            if k <= length(test_data{i}{j}) * val_per
                subject_filename=strcat(config.ubm_val_data_dir,'/',num2str(i),'_',num2str(val_ind),'.lfcc');
                val_filenames{i}{val_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                val_ind=val_ind+1;
            else
                subject_filename=strcat(config.ubm_test_data_dir,'/',num2str(i),'_',num2str(test_ind),'.lfcc');
                test_filenames{i}{test_ind}=subject_filename;
                dlmwrite(subject_filename,[dim no_of_frames],'delimiter',' ','precision',10);
                dlmwrite(subject_filename,reshaped_recoding_data','-append','delimiter',' ');
                test_ind=test_ind+1;
            end
        end
    end
    disp(['Tested subject : ', num2str(i),' Time Taken:', num2str(cputime-t)]);
end

scriptFileID = fopen([config.ubm_val_data_dir  '/valScriptFile.scp'],'w');
v_filenames={};
val_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(val_filenames{i})
        fprintf(scriptFileID,'%s \n',val_filenames{i}{j});
        v_filenames{ind}=val_filenames{i}{j};
        val_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(scriptFileID);

scriptFileID = fopen([config.ubm_val_data_dir  '/valTestScript.sh'],'w');
for i=1:100:length(val_gt_label)
    endtest=i+100-1;
    if endtest>length(val_gt_label)
        endtest=length(val_gt_label);
    end
    command=sprintf(['library/ubm-gmm/bin/TestGMM ',...
        '--resultFolder %s --start %d ',...
        '--end %d %s %d %s %s %d'],....
        [config.ubm_val_data_dir,'/result/'],...
        i,endtest,...
        [config.ubm_model_dir  '/modelsScriptFile.scp'],...
        num_subjects,...
        [config.ubm_val_data_dir  '/valScriptFile.scp'],...
        [config.ubm_model_dir  '/UBM_GMM.txt_py'],10);
    fprintf(scriptFileID,command);
    fprintf(scriptFileID,'\n');
end
fclose(scriptFileID);

system(['less ',config.ubm_val_data_dir,'/valTestScript.sh | parallel -v -j12']);
val_predicted_label=[];
for i=1:length(val_gt_label)
    file_ID=fopen([config.ubm_val_data_dir,'/result/', num2str(i),'.out'],'r');
    val_predicted_label(i) = fscanf(file_ID,'label %d \n');
    fclose(file_ID);
end

scriptFileID = fopen([config.ubm_test_data_dir  '/testScriptFile.scp'],'w');
t_filenames={};
test_gt_label=[];
ind=1;
for i =1:length(test_data)
    for j =1:length(test_filenames{i})
        fprintf(scriptFileID,'%s \n',test_filenames{i}{j});
        t_filenames{ind}=test_filenames{i}{j};
        test_gt_label(ind)=i;
        ind=ind+1;
    end
end
fclose(scriptFileID);

scriptFileID = fopen([config.ubm_test_data_dir  '/testScript.sh'],'w');
for i=1:100:length(test_gt_label)
    endtest=i+100-1;
    if endtest>length(test_gt_label)
        endtest=length(test_gt_label);
    end
    command=sprintf(['library/ubm-gmm/bin/TestGMM ',...
        '--resultFolder %s --start %d ',...
        '--end %d %s %d %s %s %d'],....
        [config.ubm_test_data_dir,'/result/'],...
        i,endtest,...
        [config.ubm_model_dir  '/modelsScriptFile.scp'],...
        num_subjects,...
        [config.ubm_test_data_dir  '/testScriptFile.scp'],...
        [config.ubm_model_dir  '/UBM_GMM.txt_py'],10);
    fprintf(scriptFileID,command);
    fprintf(scriptFileID,'\n');
end
fclose(scriptFileID);

system(['less ',config.ubm_test_data_dir,'/testScript.sh | parallel -v -j12']);
test_predicted_label=[];
for i=1:length(test_gt_label)
    file_ID=fopen([config.ubm_test_data_dir,'/result/', num2str(i),'.out'],'r');
    test_predicted_label(i) = fscanf(file_ID,['label %d \n']);
    fclose(file_ID);
end
end


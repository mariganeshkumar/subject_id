IN_LIST='/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/plda/tools/compute_parallel/female/lists/outlist/tel_mic/eigenvoices/outList_eigenchannels.scp';

nx =450;
addpath('/home/hltcoe/sgarimella/software/modified_tools/JFA/matlab');
addpath('/export/hynek/sganapathy/speech/speakerid/jfa/scripts/train_ivec_tel'); % for parse_scp.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%%%%%%%%%%%%%% PLDA %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Params
min_nfiles = 6;
flag_LDA = 1;
flag_norm_dev = 1; % makes norm of each ivect 1
flag_monitor_LL=1;
flag_MD=1;
flag_diag=1; % 1 %Make the noise cov diagonal if set to 1.


nF = 250;
nA = nx ;
niter=20;

%Display information
fid = 1;
%%%%
%%%%%%%%%%%%%%%%%
%HARD CODED CONFIGURATION
SMALL_INC = 0.1; % Small increment in log Marginal Likelihood to stop EM
var_floor = 1e-5; % Floor for noise variance
%%%%%%%%%%%%%%%%%


%% load data
[spk_logical spk_physical] = parse_scp(IN_LIST);
for ii=1:size(spk_physical,1)
          spk_physicalx{ii,1} = [spk_physical{ii} '.x'];
end

x= load_vectors_from_files(spk_physicalx);
x=x';

% get speaker/session information
auxm  = regexp(spk_logical, '^[^_]+_[^_]+', 'match');
auxm  = [auxm{:}]';
[auxa auxb spk_ids] = unique(auxm);
n_speakers=max(spk_ids);
n_sessions=size(spk_ids,1);

N_sessions = zeros(n_speakers,1); % Variable indicating total sessions per speaker
for i = 1: n_speakers
   N_sessions(i) = sum(spk_ids == i);
end
flag_gender_dependent = 0;

%% drop speakers with less sessions
fprintf(1,'number of speakers before trimming - %d\n', n_speakers);
[x,N_sessions] = filter_development_data(x, N_sessions, min_nfiles, spk_ids); % spk_ids is not valid anymore, sessions in the returned x is continuous
[dim,ntot] = size(x);
n_speakers = size(N_sessions, 1);
fprintf(1,'number of speakers after trimming - %d\n', n_speakers);



%% compute the mean from development set and remove
fprintf(1,'Removing mean from development data ...\n');
meanX=mean(x,2);           
x = bsxfun(@minus,x,meanX);


%% Use LDA to rotate the space (tends to facilitate convergence of EM)
if flag_LDA
    fprintf(1,'Computing LDA mtx ...\n');
    A = train_LDA_projection(x, N_sessions, nA);
    x = A'*x;
    dim = size(x,1);
    if nF > nA
        nF = nA;
    end
else
    A=0;
end

if flag_norm_dev
    fprintf(1,'UNIT CIRCLE Normalizing development data ...\n');    
    x_nrm = sqrt(sum(x .* x));
    x = bsxfun(@rdivide,x,x_nrm);
    LML_correction_term = 0;        
end


fprintf(1,'******************      DEVELOPMENT       ********************************************\n');
fprintf(1,' dim: %i F: %i;   n_speakers: %i ntot: %i; min_nfiles: %i\n',dim,nF,n_speakers,ntot,min_nfiles);
fprintf(1,'**************************************************************************************\n');
fprintf(1,'\n Training F,  E from development data...\n');



tic;
[Fest,Eest] = EM_train_SPLDA_model(x, N_sessions, nF, niter, flag_monitor_LL, flag_MD, SMALL_INC, fid, var_floor, flag_diag, LML_correction_term);
toc;

save('/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/models/mfcc/gauss/female/F250_female_D.mat','Fest', 'Eest', 'A', 'meanX'); 


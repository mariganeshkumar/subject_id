/*-------------------------------------------------------------------------
 *  TrainMLLRModels_mod.c - Trains models for each speaker given a list of files.
 *  			 Requires negative examples to facilitate better discrimination.
 *  			 Include a regularization parameter alpha that balances data between
 *  			 speaker and anti-speaker data
 *
 *  			 This modified version takes a meta list of all spkrs. Each entry
 *  			 in this meta list is the file name of a spkr list (i.e list of
 *  			 training files for that spkr). All features are computed at once 
 *  			 in the beginning and they are combined appropriately for each spkr
 *  			 to form the +ve and -ve data sets.
 *
 *  			 Additional Feature : startfrom - enables to pause and start later (manually)
 *
 *  			 
 *  			 
 *  				
 -------------------------------------------------------------------------*/

/************************************************************************
  Function            : train_models- computes feature vectors from a 
                        list of speech files, generates the codebook
			and writes the codebook to a file.

  Input args          :  ctrl_file speaker_table codebook_size
                         feature_name output_codebook_file 

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-2002
*******************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "SphereInterface.h"
#include "BatchProcessWaveform.h"
#include "VQ_Modified.h"
#include "GMM.h"
#include "math.h"
#include "NewWeightedGMM.h"
/*-------------------------------------------------------------------------
 *  ComputeThreshold -- Computes the threshold on energy for a waveform
 *    Args:	
 *    Returns:	float
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeThreshold(ASDF *asdf, float threshold)
{
  int i;
  float ave = 0;
  for (i = 0; i < asdf->numFrames; i++)
    ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
  ave = ave/asdf->numFrames;
  return (ave*threshold);
}	/*  End of ComputeThreshold		End of ComputeThreshold   */

/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, FILE *speakerFile, 
					   char *featureName, 
					   unsigned long *numVectors,
					   float thresholdScale) {

  F_VECTOR                       *fvect;
  VECTOR_OF_F_VECTORS            *vfv;
  char                           line[500];
  unsigned long                  totalFrames;
  int                            i;
  unsigned long                  frameNo;
  char                           wavname[500];
  float                          energy, threshold;

  totalFrames = 0;
  while (fgets(line,500,speakerFile) != NULL) {
    sscanf(line,"%s",wavname);
    printf("file read :%s\n",wavname); fflush(stdout);
    GsfOpen(asdf,wavname);
    totalFrames = totalFrames + asdf->numFrames;
    printf("wavname = %s %ld %d numFrames = %d\n",wavname,totalFrames,asdf->numSamples, asdf->numFrames); fflush(stdout);
    GsfClose(asdf);
  }
  printf("total no frames = %ld\n", totalFrames);
  fflush(stdout);
  vfv  = (VECTOR_OF_F_VECTORS *) malloc(totalFrames*sizeof(VECTOR_OF_F_VECTORS));
  if (vfv == NULL) {
    printf ("unable to allocate space for vfv \n");
    exit(-1);
  }
  rewind(speakerFile);
  frameNo =0; 
  while (EOF != fscanf(speakerFile,"%s",wavname)) {
    GsfOpen(asdf,wavname);
    printf("numFrames = %d\n", asdf->numFrames);
    threshold = (float) ComputeThreshold(asdf, thresholdScale);
    for (i = 0; i < asdf->numFrames; i++) {
      energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
      if (energy >= threshold) {
	fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
	if(fvect == NULL) {
	  printf("problems fvect\n");
	  fflush(stdout);
	  exit(-1);
	} 
	vfv[frameNo] = fvect;
	frameNo++; 
      }
    }
    GsfClose(asdf);
  }
  *numVectors = frameNo;
  printf("total no of frame processed = %ld\n",*numVectors);
  return(vfv);
}

void Usage() {
   printf(" TrainModelsGMM ctrlFile numSpeakers codeBookSize ");
   printf(" FeatureFolder numVQIter numGMMIter modelFolder thresholdScale distThreshold startFrom\n"); 
}



int main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  int                   numClusters;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv, *nvfv;
  VECTOR_OF_F_VECTORS 	**metaVfv;
  int                   i,j;
  unsigned int          startFrom, numElements;
  unsigned long         *numVectors, *negNumVectors, totalVectors;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize,alpha, npercent;
  int                   seed,featLength;
  FILE			 *negSpeakerFile, *tempSpeakerListFile, *featureFile, *cohortMetaFile, *speakerCohortFile;
  int							numSpeakers, spkId, tempSpkId, idx;
  int                   cohortSize=50, tempCohortId, endAt;
  char                  *featureFileName, *speakerCohortList, *cohortMetaList;
  char						*modelName;
  char						*modelPrefix, *featureFolder;
  if (argc < 12 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  sscanf(argv[2],"%d",&numSpeakers);
  string1 = argv[3];
  featureFolder = argv[4];
  sscanf(string1,"%d",&numClusters);
  string2 = argv[5];
  sscanf(string2,"%d",&VQIter);
  string2 = argv[6];
  sscanf(string2, "%d", &GMMIter);
  modelPrefix = argv[7];
//  outputFile = fopen(string2,"w");
  cFile = fopen(cname,"r");
  sscanf(argv[9],"%f",&alpha);
  string1 = argv[8];
  sscanf(string1, "%f", &thresholdScale);
  sscanf(argv[10], "%u", &startFrom);
	sscanf(argv[11], "%f", &npercent);
//  cohortMetaList = argv[11]; 
//  cohortMetaFile = fopen(cohortMetaList,"r");
 
  if (argc == 13)
  		sscanf (argv[12],"%d",&endAt);
	else
		endAt = numSpeakers;
  if (startFrom > numSpeakers)
  {
     printf ("Invalid startFrom argument...\n");
     exit(0);
  }

  metaVfv = (VECTOR_OF_F_VECTORS**) calloc (numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  numVectors = (int *) calloc (numSpeakers, sizeof(int));
  negNumVectors = (int *) calloc (numSpeakers, sizeof(int));
  totalVectors = 0;

  if (metaVfv != NULL) {
  	printf ("... allocated memory for metaVfv ...\n");
  	fflush(stdout);
	}
  
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  seed = (int) GetIAttribute(asdf, "seed");
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  Cstore(asdf->fftSize);

  for (spkId = 0; spkId < numSpeakers; spkId++) {
/*  	fscanf (speakerFile,"%s\n",perSpeakerList);
	tempSpeakerListFile = fopen (perSpeakerList,"r");
  	metaVfv[spkId] = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors (asdf, 
						       	tempSpeakerListFile,
						       	featureName,
	  								&numVectors[spkId],
									thresholdScale
							      );*/
   featureFileName = (char *) calloc(200,sizeof(char));
   strcpy(featureFileName,featureFolder);
   strcat(featureFileName,"/Speaker");
   sprintf(&featureFileName[strlen(featureFileName)],"%d",spkId+1);
   strcat(featureFileName,".feat");
//   printf(".....Reading file %s\n",featureFileName);
   featureFile = fopen(featureFileName,"r");
   fscanf(featureFile,"%u %u\n",&numElements,&numVectors[spkId]);
   metaVfv[spkId] = (VECTOR_OF_F_VECTORS*) calloc(numVectors[spkId],sizeof(VECTOR_OF_F_VECTORS));
   for (i=0;i<numVectors[spkId];i++) {
      metaVfv[spkId][i]=(F_VECTOR*) AllocFVector(numElements);
      for (j=0;j<numElements;j++) { 
           fscanf(featureFile,"%f ",&metaVfv[spkId][i]->array[j]);
      }
//      fscanf(featureFileName,"\n");
   }
   totalVectors += numVectors[spkId];			
   free(featureFileName);
   fclose(featureFile);
  }
  for (spkId = 0; spkId < numSpeakers; spkId++)
  	negNumVectors[spkId] = totalVectors - numVectors[spkId];

 /* speakerCohortList = (char *) calloc (200,sizeof(char));
  if (startFrom > 1) {
     for (spkId = 1; spkId < startFrom; spkId++)
        fscanf (cohortMetaFile,"%s\n",speakerCohortList);
  }*/
   
//  free (speakerCohortList);
  printf("We are here now\n"); fflush (stdout);
  for (spkId = startFrom - 1; spkId < endAt; spkId++) {

  printf ("Building models for speaker with id = %d\n",spkId+1); fflush (stdout);

  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
  

  vfv = metaVfv[spkId];
/*  speakerCohortList = (char*) calloc (200,sizeof(char));
  fscanf (cohortMetaFile,"%s\n", speakerCohortList);
  speakerCohortFile = fopen(speakerCohortList,"r");*/
/*  for(i=1;i<=cohortSize && !feof(speakerCohortFile);i++) { 
     fscanf(speakerCohortFile,"%d",&tempCohortId);
     negNumVectors[spkId] += numVectors[tempCohortId-1];
     if (feof(speakerCohortFile)) break;
  }
  rewind(speakerCohortFile);*/
  
  nvfv = (VECTOR_OF_F_VECTORS *) calloc (negNumVectors[spkId], sizeof (VECTOR_OF_F_VECTORS));
  idx=0;
  for (tempSpkId = 0; tempSpkId < numSpeakers; tempSpkId++) {
  	 if (tempSpkId == spkId)
	 	continue;
	 
	 for (i = 0; i < numVectors[tempSpkId]; i++) {
	 	nvfv[idx++] = metaVfv[tempSpkId][i];
	 }
  }

  
  printf ("\n num Vectors allocated is %d\n",numVectors[spkId]);
  printf ("neg numvectors is %d and it should be %d\n",idx, negNumVectors[spkId]);	
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(vfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %ld numGMM = %d\n", numVectors[spkId], GMMIter);
  
  NewWeightedGMMTopN(vfv, numVectors[spkId], clusterMeans, clusterVars,
	     	 clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             	 ditherMean, varianceNormalize, seed, nvfv, negNumVectors[spkId],
		 npercent);
  modelName = (char *) calloc (200,sizeof(char));
  strcpy (modelName, modelPrefix);
  strcat (modelName, "spid_");
  sprintf (&modelName[strlen(modelName)],"%d",spkId+1);
  strcat (modelName, ".gmm");
  printf ("opening file %s\n",modelName);
  outputFile = fopen (modelName, "w");
  for (i = 0; i < numClusters; i++){
    if (clusterWts[i] != 0) {
      fprintf(outputFile,"%e \n", clusterWts[i]/(float)numVectors[spkId]);
    } else {
      fprintf(outputFile, "%e\n", 1.0E-6);
    }
    for (j = 0; j < vfv[0]->numElements; j++) {
	  if (clusterVars[i]->array[j] == 0.000000) clusterVars[i]->array[j] = 1.0E-6;
      fprintf(outputFile," %e %e ",
	      clusterMeans[i]->array[j],
	      clusterVars[i]->array[j]);
    }
    fprintf(outputFile,"\n");
  }

  fclose(outputFile);
//  fclose (negSpeakerFile);
  free(nvfv);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
  free (modelName);
//  fclose(speakerCohortFile);
  printf ("... Completed building model for speaker %d\n",spkId+1); fflush(stdout);
  }
  free (numVectors);
  free (negNumVectors);
  return(0);
}


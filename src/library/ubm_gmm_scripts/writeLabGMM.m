function [] = writeLabGMM(model,filename)
fileID = fopen(filename,'w');
for i=1:model.NumComponents
    weight_str = sprintf('%e \n',model.ComponentProportion(i));
    fprintf(fileID, weight_str);
    mean =  squeeze(model.mu(i,:));
    var =  squeeze(model.Sigma(:,:,i));
    mean_var_str='';
    for j = 1:length(mean)
        mean_var_str=[mean_var_str,sprintf('%e %e', mean(j),var(j))];
    end
    fprintf(fileID, mean_var_str);
    fprintf(fileID, '\n');
end
end

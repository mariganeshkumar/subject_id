/*
 * =====================================================================================
 *
 *       Filename:  GetMixtureScores.c
 *
 *    Description:  Prints the topc scoring probability values
 *                  after normalization
 *
 *
 * =====================================================================================
 */


#include "topclib.h"

int main(int argc, char *argv[])
{

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL, *ubmFile=NULL, *ergSpidModelFile;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, *ubmFileName = NULL,
                        *featureName = NULL, *ergSpidModelFileName;
  char                  *string1=NULL, *string2=NULL;
  char                  *speakerModel,line[500];
  int                   numSpeakers, numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue, mixno;
  unsigned int         numVectors;
  int                   verify, numGaussiansErg=3;
  float                 *Distortion, *ubmWts;
  float                 thresholdScale;
	float                 *ergWts;
  float                 *prob_arr, norm_factor;
	VECTOR_OF_F_VECTORS   *ergMeans, *ergVars;

  if (argc != 5) {
    printf ("Usage: %s featureFile ubm numMix cVal\n",argv[0]);
    exit(-1);
  }
  wavname = argv[1];
  ubmFileName = argv[2];
  sscanf (argv[3], "%d", &numClusters);
  sscanf (argv[4], "%d", &cValue);

  numVectors = 0;
  vfv = ReadVfvFromFile (wavname, &numVectors);
  featLength = vfv[0]->numElements;
  i = 0;

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters, sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters, sizeof (float));

  for (i = 0; i < numClusters; i++)
    {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);

  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters, 
                            vfv, numVectors, cValue, 1.0); 
  prob_arr = (float *) calloc (numClusters, sizeof(float));
  finc(j,numVectors,1) {
    norm_factor = 0.;
    finc(i,cValue,1) {
      mixno = mixIds[j][i] - 1;
      prob_arr[mixno] = ComputeProbability(ubmModelMeans[mixno], 
                                           ubmModelVars[mixno],
                                           ubmWts[mixno],
                                           vfv[j],
                                           1.0);
      if (i > 0) norm_factor = LogAdd (prob_arr[mixno],norm_factor);
      else norm_factor = prob_arr[mixno];
    }
    finc(i,cValue,1) {
      mixno = mixIds[j][i] - 1;
      prob_arr[mixno] = expf(prob_arr[mixno] - norm_factor);
    }    
    finc(i,numClusters,1)
        printf("%e ", prob_arr[i]);
    printf ("\n");
    finc(i,cValue,1) 
        prob_arr[mixIds[j][i]-1] = 0.;
  }
  FreeVfv (ubmModelMeans, numClusters);
  FreeVfv (ubmModelVars, numClusters);
  free (ubmWts);
  }

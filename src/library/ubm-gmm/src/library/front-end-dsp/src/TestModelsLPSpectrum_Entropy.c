/*-------------------------------------------------------------------------
 *  TrainModelsGMM.c - Trains models for each speaker given a list of files
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:58:37
 *  Last modified:  Wed 07-May-2003 15:52:21 by hema
 *  $Id: TrainModelsGMM.c,v 1.2 2008/03/12 17:49:10 hema Exp $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/

/************************************************************************
  Function            : train_models- computes feature vectors from a 
                        list of speech files, generates the codebook
			and writes the codebook to a file.

  Input args          :  ctrl_file speaker_table codebook_size
                         feature_name output_codebook_file 

  Uses                :  DspLibrary.c, init-asdf.c,batch-process-waveform.c
                         sphere_interface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-2002
*******************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "Compute_CorrCoef.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "SphereInterface.h"
#include "BatchProcessWaveform.h"

	

void LP_ComputeFeatureVectors(ASDF *asdf,char *string1)
{


  float           *signal, *f, *residual;
  long int  	  i,k,fftOrder,fftSize,frameSize, numFrames,count,
                  frameNum,iloc,lpOrder, frameShift;
  char            line[500],*speakerTable=NULL,*outfile=NULL,*outfile1=NULL,*ptr,*prev;                
  int             samplingRate,flag,increment_value,Diff,status;
  long            nSamp;
  unsigned long   frameStart,frameEnd,totalFrames=0;
  char            ansTemp,fNameSpec[100],*ftemp=NULL,wavname[500],test[500],test1[500];
  float           *LPSpec, *LPPhase, *LPSpecAvg, *LPGdSpec,  
                  gain, *coef;
  double          mul,sum,prob,value;
  F_VECTOR        *waveform;
  FILE            *specFile, *controlFile,*ProbValue,*OutCorr;
  FILE            *speakerFile=NULL;
/******************************************************************************/
  printf("speakerfile=%s\n",string1);
  
  
  speakerFile=fopen(string1,"r");
  
  k=0;
  while (EOF != fscanf(speakerFile,"%s",wavname))
    {
      k++;
      printf("wavname=%s\n",wavname);
      
      strcpy(test,wavname);
      ptr=strtok(test,"/");
      while(ptr != NULL)
	{
	  fflush(stdout);
	  prev=ptr;
	  printf("\n[%s]",prev);			
	  ptr=strtok(NULL,"/");
	  
	  fflush(stdout);
	}
      
      outfile=prev;
      fflush(stdout);
      printf("prev=%s\n",prev);
      fflush(stdout);
      sprintf(outfile,"%s.Prob",outfile);
      /*fflush(stdout);
	printf("\n outfile=%s",outfile);
	fflush(stdout);
	sprintf(test1,"test_%d_Spec",k);
	fflush(stdout);
	printf("\ntest1=%s",test1);
	strcpy(test,test1);
	fflush(stdout);
	printf("\ntest=%s",test);*/
      
      GsfOpen(asdf,wavname);
      printf("numFrames = %d\n", asdf->numFrames);
      frameStart=0;frameEnd=asdf->numFrames;
      frameSize = (int) GetIAttribute(asdf, "windowSize");
      fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
      fftSize  = (int) GetIAttribute(asdf, "fftSize");
      lpOrder   = (int) GetIAttribute(asdf, "lpOrder");
      frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
      numFrames = (int) GetIAttribute(asdf, "numFrames");
      samplingRate = (int) GetIAttribute(asdf, "samplingRate");
      printf ("frameSize = %ld fftOrder = %ld fftSize = %ld lpOrder = %ld frameShift = %ld numFrames=%ld\n", frameSize, fftOrder, fftSize, 			 lpOrder,  frameShift, numFrames);
      Cstore(fftSize);
      
      signal 	 = (float *) AllocFloatArray(signal, frameSize+1);
      f 		 = (float *) AllocFloatArray(f, frameSize+1);
      residual	 = (float *) AllocFloatArray(residual, frameSize+1);
      LPSpec 	 = (float *) AllocFloatArray(LPSpec, fftSize+1);
      LPSpecAvg 	 = (float *) AllocFloatArray(LPSpecAvg, fftSize+1);
      LPPhase 	 = (float *) AllocFloatArray(LPPhase, fftSize+1);
      LPGdSpec 	 = (float *) AllocFloatArray(LPGdSpec, fftSize+1);
      coef 		 = (float *) AllocFloatArray(coef, lpOrder+1);
      
      iloc = frameStart*frameShift;
      flag=0;increment_value=1;
      OutCorr=fopen(outfile,"w");
      
      frameNum = 0;
      while(frameStart<=frameEnd)
	{
	  printf (" frameNum = %d startLoc = %d\n", frameStart, iloc);
	  waveform = (F_VECTOR *) GsfRead(asdf, frameStart, "frameWaveform");
	  for (i = 1; i <= frameSize; i++)
	    signal[i] = waveform->array[i-1];
	  
	  LpAnal(signal,residual, frameSize, frameShift, coef, lpOrder,&gain);
	  LPSpectrum(coef, lpOrder, LPSpec, LPPhase , fftSize, fftOrder, gain);
	  
	  for (i = 1; i <= fftSizeBy2; i++)
	    specAvg  = specAvg + LPSpec[i]*LPSpec[i];
	  
	  for (i = 1; i < fftSizeBy2; i++) {
	    psd[i] = LPSpec[i]*LPSpec[i]/specAvg;
	    entropy = - psd[i]*log (psd[i]) + entropy; 
	  }				
	  fprintf(specFile, "%f\n", entropy);
	  iloc = iloc+frameShift;
	}
    }
  fclose(specFile);
}
				/*				for (i = 2; i <= fftSize - 1; i++) 
       				{
        				 LPGdSpec[i] = LPPhase[i] - LPPhase[i-1];
					 if (fabs(LPGdSpec[i]) > PI)
	 				  LPGdSpec[i] = LPGdSpec[i-1];
     				}
      				 LPGdSpec[1] = LPGdSpec[2];
      		 		for (i = 1; i <= fftSize; i++) 
				{
					fflush(stdout);
					//printf("\n i=%d",i);
					 if (i <= frameSize)
					{
						
							
							fprintf(specFile, "%f\n",   20*(LPSpecAvg[i])/(frameEnd-frameStart+1));
					}
		 	  	
	 				else
					{
						fprintf(specFile, "%f\n",   20*(LPSpecAvg[i])/(frameEnd-frameStart+1));
					}
					} */
			fclose(specFile);
			fflush(stdout);		  		

			specFile=fopen("out.spec","r");count=0;

			ProbValue=fopen("out.prob","w");sum=0;
			while(1)
			{

				status=fscanf(specFile,"%lf",&value);
				count++;
				if(status==-1)
					break;
				if(count==512)
					break;
				if(value>=0)
				
				sum=sum+value;
				else
				;
			}
			//printf("\n framenum=%d\t sum=%lf",frameStart,sum);
			rewind(specFile);
			count=0;
			while(1)
			{

				status=fscanf(specFile,"%lf",&value);
				count++;
				if(status==-1)
					break;
				if(count==512)
					break;
				if(value>0)
				prob=value/sum;
				else
				prob=0;
				fprintf(ProbValue,"%lf\n",prob);
			}
			fclose(ProbValue);
			fclose(specFile);
			ProbValue=fopen("out.prob","r");sum=0;
			while(1)
			{
				status=fscanf(ProbValue,"%lf",&value);
				if(status==-1)
					break;
				if(value>0)
				{
					mul=-value*log2(value);
					sum=sum+mul;
				}
				else
				;
				//printf("prob=%f\tmul%lf\t sum=%lf\n",value,mul,sum);
			}
			fprintf(OutCorr,"%d\t%lf\n",frameStart,sum);				

			frameStart++;

		}
		fclose(OutCorr);


		GsfClose(asdf);
	}
	
       fclose(speakerFile);
    
	
     
}

		

void Usage() 
{
 	  printf(" TestModelsLPSpectrum ctrlFile Testfile ");
	   printf("\n"); 
}
 
int main(int argc, char *argv[])
{

  	 FILE                  *cFile=NULL;
	 char                  *cname=NULL ;     		            
 	 char                  *string1=NULL;
	 int  	       	  	i,j,k;
	    	
         static  ASDF           *asdf;
 	
 	
 	 if (argc != 3 )
	 {
  		  Usage();
  		  exit(-1);
  	}

 		cname = argv[1];	    
		string1 = argv[2];
 			  cFile = fopen(cname,"r");
			  
		asdf = (ASDF *) malloc(sizeof(ASDF));
       		InitializeStandardFrontEnd(asdf,cFile);
       		
		//LP_ComputeFeatureVectors(asdf,speakerFile, modelFile);
		
        
  		LP_ComputeFeatureVectors(asdf,string1);
	  return(0);
}


		/*frameNum	= frameStart;flag=0;increment_value=40;
		while(flag!=1)
		{
			
			frameEnd   = frameStart+increment_value;
			
			if(frameEnd>numFrames)
			{
				Diff=numFrames-frameStart;
				frameEnd=frameStart+Diff;
				flag=1;
			}*/













/*-------------------------------------------------------------------------
 * $Log: TrainModelsGMM.c,v $
 * Revision 1.2  2008/03/12 17:49:10  hema
 * Fixed the length of filenames
 *
 * Revision 1.1  2002/04/30 09:34:53  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:30:22  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TrainModelsGMM.c
 -------------------------------------------------------------------------*/

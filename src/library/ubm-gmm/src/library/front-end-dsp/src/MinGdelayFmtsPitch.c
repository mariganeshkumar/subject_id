/**************************************************************************
 *  $Id$
 *  File:	MinGdelayFmtsPitch.c - Extracts formants and pitch using Minimum Phase 
 *              Group Delay function
 *  Purpose:	Formant and Pitch Extraction
 *
 *  Author:	Hema Murthy
 *
 *  Date:	Thu Feb 21 21:25:13 IST 2008
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"
#include "fe/FmtsLibrary.h"
#include "fe/PthLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program extracts formant and pitch data from a given speech
*       using the min group delay function 
**	Inputs :
*	Control File, Speech data file, startFrame, endFrame
*	Output :
*	Formant and pitch are written on to different files, outputFmtFile, outputPthFile
*       If only formants are required -- set vad in ctrlFile:
        vad int 0
*       For pitch with voiced and unvoiced decision set vad in ctrlFile:
        vad int 1
*******************************************************************************/      
 void Usage() {
           printf("Usage : MinGdelayFmtsPitch  controlFile wavefile outputFmtFile  outputPthFile startFrame, endFrame\n");
}
        int main (int argc, char *argv[])
{ 
        float           *signal = NULL, *f = NULL;
        float           minFrequency, 
	                maxFrequency, gamma;
	int  	        i,k,fftSize, fftSizeBy2, fftOrder,frameSize,
      	                numFmtObtd, frameNum, iloc,
	                winLen, minPitch, maxPitch, 
                 	frameShift, numFormants, numFrames,  
	                startFrame, endFrame, samplingRate;
        long            nSamp;
	float 		minGdelayPitch,*minGdelayFormants=NULL,*minGdelayFmtCopy=NULL;
        ASDF            *asdf;
	static  F_VECTOR *waveForm=NULL;
        FILE            *fmtFile=NULL,*pthFile=NULL, *controlFile=NULL;
/******************************************************************************/
       if (argc != 7) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       numFormants = (int) GetIAttribute(asdf, "numFormants");
       minPitch = (int) GetIAttribute(asdf, "minPitch");
       maxPitch = (int) GetIAttribute(asdf, "maxPitch");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       winLen = (int) GetIAttribute(asdf, "numCepstrum");
       minFrequency = (float) GetFAttribute(asdf, "minFrequency");
       maxFrequency = (float) GetFAttribute(asdf, "maxFrequency");
       gamma = (float) GetFAttribute(asdf, "gamma");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	       frameSize, fftOrder, fftSize, frameShift);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d numFrames=%d\n", frameSize, fftOrder, fftSize, frameShift, numFrames);
       Cstore(fftSize);
       fmtFile = fopen(argv[3], "w");
       pthFile = fopen(argv[4], "w");
       sscanf(argv[5],"%d", &startFrame);
       sscanf(argv[6],"%d", &endFrame);
       signal = (float *) calloc(fftSize+1, sizeof(float));
       f  = (float *) calloc(frameSize+1, sizeof(float));
       minGdelayFormants  = (float *) calloc(numFormants+1, sizeof(float));
       minGdelayFmtCopy  = (float *) calloc(numFormants+1, sizeof(float));
       if ((startFrame == -1) && (endFrame == -1)) {
	 startFrame = 0;
         endFrame = numFrames;

       }
      frameNum = startFrame;
       iloc = startFrame*frameShift;
       for (frameNum = startFrame; frameNum < endFrame;  frameNum++) {	
	  waveForm = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	  if (waveForm != NULL) {
	    for (i = 1; i <= frameSize; i++)
	      f[i] = waveForm->array[i-1];
	    for (i = 1; i <=  frameSize; i++){
	      signal[i] = f[i]*HamDw(i,frameSize);
	    }
	    for (i = frameSize+1; i <= fftSize; i++)
	      signal[i] = 0.0;
	    minGdelayFormants = (float *) FmtsMinGd(signal, frameSize,
						     fftSize, fftOrder,
						     winLen, gamma, 
						     minGdelayFormants, 
						     &numFmtObtd);
	    printf("Frame Number = %d ", frameNum);
	    if (numFmtObtd > numFormants)
	      numFmtObtd = numFormants;
            printf("numFormants = %d numFmtsObtd = %d\n", numFormants, numFmtObtd);
	    for(i = 1; i <= numFmtObtd; i++) 
	      printf("F %d = %f ", i, minGdelayFormants[i]);
	    printf("\n");   
	    fflush(stdout);
	    minGdelayPitch = (float ) PitchMinGd(signal,frameSize,
					   fftSize, fftOrder,
					      minPitch, maxPitch, gamma);
	 
	    printf("FrameNumber %d Pitch =  %f\n",frameNum, minGdelayPitch);
	    k = 0;
	    for (i = 1; i <= numFormants; i++)
	      minGdelayFmtCopy[i] = 0;
	    for (i = 1; i <= numFmtObtd; i++)       
	      if (minGdelayFormants[i]/fftSize*samplingRate >= minFrequency &&
		  minGdelayFormants[i]/fftSize*samplingRate <= maxFrequency) {   
		k = k + 1;         
		minGdelayFmtCopy[k] = minGdelayFormants[i]/fftSize*samplingRate;     
	      }
	    fprintf(fmtFile, "%d ", iloc);
	    for (i = 1; i <= k; i++)
	      fprintf(fmtFile,"%f ",minGdelayFmtCopy[i]);
	    for (i = k+1; i <= numFormants; i++)
	      fprintf(fmtFile, "%f ",0.0);
	    fprintf(fmtFile,"\n");
	    fprintf(pthFile,"%d %f %d\n",iloc, minGdelayPitch/samplingRate, (int) minGdelayPitch);
	    free(waveForm->array);
	    free(waveForm);
	  } else {
	    fprintf (fmtFile, "%d", iloc);
	    for (i = 1; i <= numFormants; i++)
	      fprintf(fmtFile, "%f ", 0.0);
	    fprintf(fmtFile, "\n");
	    fprintf(pthFile,"%d %f %d\n",iloc, 0.0, 0);
	  }
	  for (i = 1; i <= 10; i++)
	    minGdelayFormants[i] = 0;
	  minGdelayPitch = 0;
	  iloc = iloc+frameShift;
	}
       fclose(fmtFile);
       fclose(pthFile);
       free(f);
       free(signal);
       free(minGdelayFormants);
       free(minGdelayFmtCopy);
       return(0);
}














/**************************************************************************
 * $Log$
 *
 *                        End of MinGdelayFmtsPitch.c
 **************************************************************************/

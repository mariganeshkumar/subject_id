/**************************************************************************
 *  $Id: DifferentSpectra.c,v 1.1 2000/07/24 02:07:25 hema Exp hema $
 *  File:       DifferentSpectra.c - Computes different spectra
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Mon 10-Dec-2012 22:24:24 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average FFT Spectrum 
        from the startFrame to endFrame for a given speech utterance and 
        saves it in 
*	a file. 
*	Inputs :
*	Input data : controlFile, waveFile, SpecFile, startFrame, endFrame
*	Output :
*       Average of the FFT Spectrum  of data are written to a file 

*******************************************************************************/   
void Usage() {
           printf("Usage : DifferentSpectra ctrlFile waveFile SpecFile nonorm startFrame endFrame startDftIndex endDftIndex\n");
}
/*****************************************************************************/

int        main (int argc, char *argv[])
{ 
	int  	        i, fftSize, samplingRate,
	                frameNum, frameStart, frameEnd, startDftIndex, endDftIndex,
	                fftSizeBy2, nonorm;
	long            numFrames, numberOfFrames;
        float           *ModGDAvg=NULL, *MinGDAvg=NULL,
	                *FFTSpecAvg=NULL, *FFTAvgEntropy=NULL, 
	                avgSpec=0, avgModgd=0, avgMingd=0;
	static F_VECTOR        *fvectFFT=NULL, *fvectModGd=NULL, *fvectMinGd=NULL;
        FILE            *specFile=NULL, *controlFile=NULL;
	float           minMinGd, maxMinGd, minModGd, maxModGd, 
	                rangeModGd, rangeMinGd;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 9) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       specFile = fopen(argv[3],"w");
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       sscanf(argv[4],"%d", &nonorm);
       sscanf(argv[5],"%d", &frameStart);
       sscanf(argv[6],"%d", &frameEnd);
       sscanf(argv[7],"%d", &startDftIndex);
       sscanf(argv[8],"%d", &endDftIndex);
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       samplingRate  = (int) GetIAttribute(asdf, "samplingRate");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       fftSizeBy2 = fftSize/2;
       FFTSpecAvg  = (float *) AllocFloatArray(FFTSpecAvg, fftSizeBy2+1);
       FFTAvgEntropy  = (float *) AllocFloatArray(FFTAvgEntropy, fftSizeBy2+1);
       ModGDAvg  = (float *) AllocFloatArray(ModGDAvg, fftSizeBy2+1);
       MinGDAvg  = (float *) AllocFloatArray(MinGDAvg, fftSizeBy2+1);
       fvectFFT = (F_VECTOR*)AllocFVector(fftSizeBy2);
       fvectModGd = (F_VECTOR*)AllocFVector(fftSizeBy2);
       fvectMinGd = (F_VECTOR*)AllocFVector(fftSizeBy2);
       if (frameStart == -1) 
	 frameStart = 0;
       if (frameEnd == -1)
	 frameEnd = numFrames;
       if (frameEnd > numFrames)
	 frameEnd = numFrames;
       if (startDftIndex == -1)
	 startDftIndex = 1;
       if (endDftIndex == -1)
	 endDftIndex = fftSize;
       if (endDftIndex > fftSize)
	 endDftIndex = fftSize;
       printf("startFrame %d endFrame %d startDftIndex %d endDftIndex %d numFrames=%ld\n", frameStart, frameEnd, startDftIndex, endDftIndex, numFrames);
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 //	 printf (" frameNum = %d\n", frameNum);
         //fvectFFT = (F_VECTOR *) GsfRead(asdf, frameNum, "frameFFTSpectrum");
	 fvectFFT = (F_VECTOR *) FrameComputeFFTSpectrum(asdf, frameNum, fvectFFT);
	 //fvectModGd = (F_VECTOR *) GsfRead (asdf, frameNum, "frameModGDelaySmooth");
	 fvectModGd = (F_VECTOR *) FrameComputeModGDelaySmooth(asdf, frameNum, fvectModGd);
	 //fvectMinGd = (F_VECTOR *) GsfRead (asdf, frameNum, "frameMinGDelay");
	 fvectMinGd = (F_VECTOR *) FrameComputeMinGDelay(asdf, frameNum, fvectMinGd);
	 minModGd = fvectModGd->array[Imin0Actual(fvectModGd->array, fftSizeBy2)];
	 maxModGd = fvectModGd->array[Imax0Actual(fvectModGd->array, fftSizeBy2)];
	 minMinGd = fvectMinGd->array[Imin0Actual(fvectMinGd->array, fftSizeBy2)];
	 maxMinGd = fvectMinGd->array[Imax0Actual(fvectMinGd->array, fftSizeBy2)];
	 rangeModGd = fabs(maxModGd - minModGd);
	 rangeMinGd = fabs (maxMinGd - minMinGd);
	 for (i = 0; i < fftSizeBy2; i++) {
	   if (rangeModGd != 0)
	     fvectModGd->array[i] = (fvectModGd->array[i] - minModGd)/rangeModGd;
	   else
	     fvectModGd->array[i] = 0.0;
	   if (rangeMinGd != 0)
	     fvectMinGd->array[i] = (fvectMinGd->array[i] - minMinGd)/rangeMinGd;
	   else
	     fvectMinGd->array[i] = 0.0;
	 }
	 if (nonorm == 0) {
	   for (i = startDftIndex; i < endDftIndex; i++) {
	     avgSpec = avgSpec + fvectFFT->array[i-1];
	     avgModgd = avgModgd + fvectModGd->array[i-1];
	     avgMingd = avgMingd + fvectMinGd->array[i-1];
	   }
	   
	   for (i = startDftIndex; i < endDftIndex; i++) {
	     if (avgSpec != 0)
	       fvectFFT->array[i] = fvectFFT->array[i]/avgSpec;
	     if (avgModgd != 0)
	       fvectModGd->array[i] = fvectModGd->array[i]/avgModgd;
	     if (avgMingd != 0)
	       fvectMinGd->array[i] = fvectMinGd->array[i]/avgMingd;
	   }
	 }
	 for (i = startDftIndex; i <= endDftIndex; i++) {
	   FFTSpecAvg[i] = FFTSpecAvg[i] + fvectFFT->array[i-1];
	   ModGDAvg[i] = ModGDAvg[i] + fvectModGd->array[i-1];
	   MinGDAvg[i] = MinGDAvg[i] + fvectMinGd->array[i-1];
	 }
	 /*       free(fvectModGd->array);
       free(fvectModGd);
       free(fvectMinGd->array);
       free(fvectMinGd);
       free(fvectFFT->array);
       free(fvectFFT);*/
       }
       numberOfFrames = frameEnd - frameStart + 1;
       for (i = startDftIndex; i <= endDftIndex; i++) 
	 fprintf(specFile, "%e %e %e %e %e %e %e\n",
		 (float) (i-1)/(float)samplingRate, 
		 (float) (i-1) / (float) fftSize * samplingRate, 
		 FFTSpecAvg[i]/numberOfFrames,20*log10(FFTSpecAvg[i])/numberOfFrames, 
		 FFTSpecAvg[i]*logf(FFTSpecAvg[i])/numberOfFrames, ModGDAvg[i]/numberOfFrames, MinGDAvg[i]/numberOfFrames);
       fclose(specFile);
       return(0);
       free(ModGDAvg);
       free(MinGDAvg);
       free(FFTSpecAvg);
       }
/************************************************************************** 
 * $Log: DifferentSpectra.c,v $
 * Revision 1.1  2000/07/24 02:07:25  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




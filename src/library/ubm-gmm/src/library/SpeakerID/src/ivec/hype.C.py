#!/usr/bin/python
# estimate C and store it in a file
# for T matrix

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *

# arguments description
# C: (flat) file to store C matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "C:F:w:h")

def printhelp (close = 0):
  print "Usage: ./hype.py -C Cmatrix.file -F firstorderlist -w wxlist"
  if close == 1:
    quit ()

cmatfname, wxlistname, folist = "", "", "" 

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-F":
      folist = value
    elif option == "-C":
      cmatfname = value
    elif option == "-w":
      wxlistname = value
    elif option == "-h":
      printhelp (1)
    else:
      print "Invalid argument..."
      printhelp (1)

# check if args are empty
if wxlistname == "" or folist == "" or cmatfname == "":
    printhelp (1)

# prepare meta list
wxlistf = open (wxlistname, "r")
wxlist = [ fname.strip () for fname in wxlistf.readlines ()]
wxlistf.close ()

folistf = open (folist, "r")
folist = [ fname.strip () for fname in folistf.readlines ()]
folistf.close ()

listlen = len (folist)
# check if list lengths are same
if listlen != len (wxlist):
    print "Incoherent lengths for input lists. Exiting"
    quit ()

# pair lists
meta = [ (folist[i],wxlist[i]) for i in range (0, listlen)]

tempf = open (wxlist[0], "r")
M,D,R = [ int (i) for i in tempf.readline ().strip ().split ()]
tempf.close ()
# there should be a way to reduce the amount of memory used here for C
nrows, ncols = M*D, R
C = np.zeros ((nrows, ncols))
N = np.zeros ((M,listlen))
l = 0
for foname, wxname in meta:
  M,D,nx,fx = ReadFirstOrderStats (foname)
  fx = fx.reshape ((M*D,1))
  wx, Lx = ReadIVecEst (wxname)
  C += np.dot (fx, wx.transpose ())
np.save (cmatfname, C)
quit ()




if [ $# != 7 ]; then
       echo "USAGE: bash extractiVec.sh  UBMFileName CodebookSize FeatDimension reducedDimension dumpDirectory tmatDirectory featList"
       exit
fi

############################    INPUT   #################################
ubmDev=$1
ubmMix=$2
D=$3
R=$4
dumpDir=$5
tmatDevDir=$6
FeatList=$7

############################### Initialization ##############################

listDumpDir=$dumpDir/lists
tmatDumpDir=$dumpDir/tmat


binDir=library/ivec/bin
scriptsDir=library/ivec/scripts

StatList=$listDumpDir/StatList
WList=$listDumpDir/WList
LxList=$listDumpDir/LxList




ComputeStats=$scriptsDir/topc_stats_chan.py
InitTMat=$binDir/InitTMat
IVectEst=$binDir/IVectEst
ComputeA=$binDir/ComputeA
ComputeC=$binDir/ComputeC
RecomputeT=$binDir/RecomputeT
Whiten=$binDir/Whiten
LDAPlusWCCN=$binDir/LDAPlusWCCN

nCores=15
nPerClass=5

rm -rf $dumpDir 
mkdir -p $listDumpDir $tmatDumpDir  

############################# i-vector vector extraction #####################################

echo "Statistics computation ..."
less $FeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

sed 's!.lfcc$!.lfcc.stat!' $FeatList > $StatList
sed 's!.stat$!.w!' $StatList > $WList
sed 's!.stat$!.lx!' $StatList > $LxList


count=`less $StatList |wc -l`
let count1=$count/$nCores
let count1=$count1+1
let var=1
for i in `seq $count1 $count1 $count`
       do
       less $StatList |head -$i |tail -$count1 >>$listDumpDir/$var.StatList
       less $WList |head -$i |tail -$count1 >>$listDumpDir/$var.WList
       less $LxList |head -$i |tail -$count1 >>$listDumpDir/$var.LxList
       let var=$var+1
done
let count1=$count-$i
less $StatList |tail -$count1 >>$listDumpDir/$var.StatList
less $WList |tail -$count1 >>$listDumpDir/$var.WList
less $LxList |tail -$count1 >>$listDumpDir/$var.LxList

echo "Computing i vectors"
seq 1 $nCores |parallel -k -j$nCores --no-notice --bar $IVectEst -l $listDumpDir/{1}.StatList $ubmMix $D $R $tmatDevDir/TMAT.txt $listDumpDir/{1}.WList $listDumpDir/{1}.LxList

import math
import sys
import numpy as np
import scipy as sp
from tables import *
from GMM import *

fname = sys.argv[1]

fvar = openFile (fname,mode="r")

objs = dir (fvar.root)
if 'vvtmatrix' in objs:
  tab = fvar.root.vvtmatrix
elif 'vmatrix' in objs:
  tab = fvar.root.vmatrix
elif 'amatrix' in objs:
  tab = fvar.root.amatrix
else:
  print "Unable to find table"
  fvar.close ()
  quit()

tot=0
rowsize = -1

for svec in tab.iterrows():
  rowsize = (np.shape(svec['colvec']))[0]
  print svec['colvec']
  tot+=1
fvar.close()
print tot
print rowsize


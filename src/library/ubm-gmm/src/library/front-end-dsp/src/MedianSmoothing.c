/**************************************************************************
 *  $Id: MedianSmoothing.c,v 1.1 2000/04/23 02:36:13 hema Exp hema $
 *  Release $Name:  $
 *
 *  File:	MedianSmoothing.c
 *             
 *  Purpose:	Median Smoothes an input signal and writes to a file
 *              
 *
 *  Author:	Hema A Murthy,BSB 307,8342,9342
 *
 *  Created:    Thu 16-Mar-2000 21:27:58
 *
 *  Last modified:  Tue 1-Nov-2011 20:41:13 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "string.h"
#include "math.h"
#include "malloc.h"
#include "stdlib.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program smoothes a given signal using Median
        Smoothing
*	Inputs :
*	Input Data File
*       Median Order
*	Output :
*       Median Smoothed  data written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : MedianSmoothing inFile medianOrder outFile \n");
}
/*****************************************************************************/

        int main (int argc, char *argv[])
{ 
 	float           *sigIn=NULL, *sigOut=NULL;
	int  	        medianOrder;
	float 		*medArray=NULL;
        FILE            *inFile=NULL, *outFile=NULL, *temp=NULL;
	int             i,j, numSamples;
        char            str[254], line[200];

       if (argc != 4) {
         Usage();
         exit(-1);
       }
       str[0] = '\0';
       strcat(str,"wc -l ");
       strcat(str, argv[1]);
       strcat(str," | cut -c1-8 > temp");
       system(str);
       temp = fopen("temp","r");
       fscanf(temp,"%d", &numSamples);
       fclose(temp);
       str[0] = '\0';
       strcat(str,argv[2]);
       sscanf(str,"%d",&medianOrder);
       printf("Number of samples = %d Median Order = %d\n",numSamples, medianOrder);
       sigIn = (float *) AllocFloatArray(sigIn,numSamples+1);
       sigOut = (float *) AllocFloatArray(sigOut,numSamples+1);
       medArray = (float *) AllocFloatArray(medArray,medianOrder+1); 
       inFile = fopen(argv[1],"r");
       outFile = fopen(argv[3],"w");
       sscanf(argv[2],"%d",&medianOrder);
       if ((inFile == NULL) || (outFile == NULL)){
	 printf("cannot open files \n");
	 exit(-1);
       }
       i = 0;
       while (fgets(line, 200, inFile) != NULL) {
         sscanf(line,"%f",&sigIn[i]);
	 i++;
       }

       /*       for (i = 1; i <= numSamples; i++) {
	 if ((i > medianOrder/2) && (i < numSamples-medianOrder/2))
	   for (j = 1; j <= medianOrder; j++)
	     medArray[j] = sigIn[i-medianOrder/2+j];
	 else if( i <= medianOrder/2){ 
	   for (j = 1; j <= medianOrder/2; j++)
	     medArray[j] = sigIn[1];
	   for (j = medianOrder/2+1; j <= medianOrder; j++)
	     medArray[j] = sigIn[i-medianOrder/2+j];
	 } else {
	   for (j = 1; j <= medianOrder/2; j++)
	     medArray[j] = sigIn[i-medianOrder/2+j];
	   for (j = medianOrder/2+1; j <= medianOrder; j++)
	     medArray[j] = sigIn[numSamples];
	 }
	 sigOut[i] = (float) Median (medArray,medianOrder);
	 }*/
       MedianSmoothArray(&sigIn[-1], numSamples, medianOrder, &sigOut[-1]);
       printf("numSamples = %d\n", i);
       for (i = 0; i < numSamples; i++)
       fprintf(outFile,"%f\n",sigOut[i]);
       fclose(inFile);
       fclose(outFile);
       free(sigIn);
       free(sigOut);
       free(medArray);
       return 0;
}
/**************************************************************************
 * $Log: MedianSmoothing.c,v $
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of MedianSmoothing.c
 **************************************************************************/









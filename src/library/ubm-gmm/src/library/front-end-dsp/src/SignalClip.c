
/**************************************************************************
 *  $Id: SignalClip.c,v 1.1 2000/07/24 02:07:25 hema Exp hema $
 *  File:       SignalClip.c - Generate a clipped version of 
 *              a given signal.
 *
 *  Purpose:	To study the effect of clipfix algorithms
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 28-Jan-2003 17:31:58 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average FFT Spectrum 
        from the startFrame to endFrame for a given speech utterance and 
        saves it in 
*	a file. 
*	Inputs :
*	Input data : controlFile, waveFile, SpecFile, startFrame, endFrame
*	Output :
*       Average of the FFT Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : SignalClip ctrlFile waveFile clipOutputFixAmplitude clipFile clipInfoFile clipValue waveformScale\n");
}
/*****************************************************************************/

int        main (int argc, char *argv[])
{ 

  float           *signal=NULL, *clipSignal=NULL;
  int             *clipInfoSignal=NULL;
  int              clipValue;
  F_VECTOR        *waveform=NULL;
  FILE            *controlFile=NULL, *fpClip=NULL, *fpOrgClip=NULL, *fpClipInfo=NULL;
  static ASDF     *asdf;
  int             i, numSamples, lowerLimit, upperLimit;
  float           scale;
/******************************************************************************/
  if (argc != 8) {
    Usage();
    exit(-1);
  }
  
  controlFile = fopen(argv[1], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  GsfOpen(asdf, argv[2]);
  fpClip = fopen (argv[3], "w");
  fpOrgClip = fopen(argv[4], "w");
  fpClipInfo = fopen (argv[5], "w");
  sscanf(argv[6], "%d", &clipValue);
  sscanf(argv[7], "%f", &scale);
  numSamples = (int) GetIAttribute (asdf,"numSamples");
  signal = (float *) AllocFloatArray(signal, numSamples);
  clipSignal = (float *) AllocFloatArray(clipSignal, numSamples);
  clipInfoSignal = (int *) AllocIntArray(clipInfoSignal, numSamples);
  for (i = 0; i < numSamples; i++){
    if (fabsf(asdf->waveform[i]) > clipValue) {
      if (asdf->waveform[i] < 0) 
	signal[i] = -clipValue;
      else  
	signal[i] = clipValue;
      clipInfoSignal[i] = 1;
    } else {
      signal[i] = asdf->waveform[i];
      clipInfoSignal[i] = 0;
    }
    clipSignal[i] = signal[i];
  }
  for (i = 0; i < numSamples; i++)  
    if ((i > 0) && (i < numSamples)){
      if ((clipInfoSignal[i] == 1) && (clipInfoSignal[i-1] == 0)) {
	signal[i-1] = signal[i-1]*scale;
	lowerLimit = i-1;
      }
      if ((clipInfoSignal[i] == 1) && (clipInfoSignal[i+1] == 0)) {
	signal[i+1] = signal[i+1]*scale;
	upperLimit = i+1;
        signal[(lowerLimit+upperLimit)/2] = (int) ((signal[lowerLimit] + signal[upperLimit])/2.0);
      }
    }
  for (i = 0; i < numSamples; i++) 
    fprintf(fpOrgClip, "%d\n", (int) ceilf(clipSignal[i]));
  for (i = 0; i < numSamples; i++) 
    fprintf(fpClip, "%d\n", (int) ceilf(signal[i]));
  for (i = 0; i < numSamples; i++)
    fprintf(fpClipInfo, "%d\n", clipInfoSignal[i]);
  fclose(fpClip);
  fclose(fpOrgClip);
  fclose(fpClipInfo);
  free(signal);
  free(clipInfoSignal);
  fclose(controlFile);
  return 0;
}
/**************************************************************************
 * $Log: SignalClip.c,v $
 * Revision 1.1  2000/07/24 02:07:25  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




// Calculate global means
//
#include "CommonFunctions.h"

int
main (int argc, char *argv[]) {

    char *infilename = NULL, *opfilename = NULL;
    FILE *ifile = NULL, *ofile = NULL;
    VFV *vfv = NULL;
    F_VECTOR *meanVfv = NULL;
    uint numVectors;
    int i,j,k,dim;

    if (argc != 3) {
        printf ("usage: %s iplist opfil\n", argv[0]);
    }

    vfv = BatchReadVfvFromFile (argv[1], &numVectors);
    meanVfv = ComputeMeanVfv (vfv, numVectors);
    dim = vfv[0]->numElements;
    ofile = fopen (argv[2],"w");
    finc(i,dim,1)
      fprintf (ofile,"%e ", meanVfv->array[i]);
    fclose(ofile);
    FreeFVector (meanVfv);
    finc(j,numVectors,1)
        FreeFVector (vfv[j]);
}

clear all;
config = configuration;
computed_feature=[config.features_dir,'/',num2str(config.feature)];
sdir=dir(computed_feature);
num_subj=size(sdir,1)-2;
fullmat={};
count = 0;
for i=1:num_subj
   disp(i);
   sub = strcat(computed_feature,'/',num2str(i));
   subdir=dir(sub);
   num_sess=size(subdir,1)-2;
   for j=1:num_sess
       sess=subdir(j+2).name;
       train=strcat(sub,'/',sess,'/train');
       test=strcat(sub,'/',sess,'/test');
       trainf=dir(train);
       testf=dir(test);
       
       for k=3:size(trainf)
           matn=strcat(train,'/',trainf(k).name);
           load(matn);
           permutedFeature = permute(feature,[2,3,1]);
           clear feature;
           [freqBins, time, channel] = size(permutedFeature);
           reshapedFeature = reshape(permutedFeature,[freqBins, time*channel]);
           count=count+1;
           fullmat{count} = reshapedFeature;   
       end
       for k=3:size(testf)
           matn=strcat(test,'/',testf(k).name);
           load(matn);              
           permutedFeature = permute(feature,[2,3,1]);
           clear feature;
           [freqBins, time, channel] = size(permutedFeature);
           reshapedFeature = reshape(permutedFeature,[freqBins, time*channel]);
           count=count+1;
           fullmat{count} = reshapedFeature;              
       end
   end
end
finalData = cell2mat(fullmat);
[proj_mat, meanData] =PCA(finalData',0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% num_subject = size(sdir,1)-2;
mkf1=[config.reduced_dim_dir,'/',num2str(config.feature)];
mkdir(mkf1);
% mkf9=strcat(mkf1,'/','projection','.mat'); 
% save(mkf9,'proj_mat');
count=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:num_subj
   sub=strcat(computed_feature,'/',num2str(i));
   subdir=dir(sub);
   num_sess=size(subdir,1)-2;
   mkf3=strcat(mkf1,'/',num2str(i));
   mkdir(mkf3);
   for j=1:num_sess
       sess=subdir(j+2).name;
       train=strcat(sub,'/',sess,'/train');
       test=strcat(sub,'/',sess,'/test');
       trainf=dir(train);
       testf=dir(test);
       mkf4=strcat(mkf3,'/',sess);
       mkdir(mkf4);
       mkf5=strcat(mkf4,'/train');
       mkdir(mkf5);
       mkf6=strcat(mkf4,'/test');
       mkdir(mkf6);
       for k=3:size(trainf)
           matn=strcat(train,'/',trainf(k).name);
           load(matn);
           for x=1:128
               abc = feature(x,:,:);
               [p, q, r] = size(abc);  
               abc = reshape(abc, q, r);
               abc = abc - repmat(meanData, [1, r]);
               feature(x,:,:) = proj_mat*abc;
           end
           mkf7=strcat(mkf5,'/',trainf(k).name); 
           save(mkf7,'feature');
       end
       for k=3:size(testf)
           matn=strcat(test,'/',testf(k).name);
           load(matn);
           for x=1:128
               abc = feature(x,:,:);
               [p, q, r] = size(abc);  
               abc = reshape(abc, q, r);
               abc = abc - repmat(meanData, [1, r]);
               feature(x,:,:) = proj_mat*abc;
           end
           mkf8=strcat(mkf6,'/',testf(k).name); 
           save(mkf8,'feature');
       end
   end
end  

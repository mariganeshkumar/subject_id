/*
 *                            COPYRIGHT
 *
 *  bwlp - Butterworth lowpass filter coefficient calculator
 *  Copyright (C) 2003, 2004, 2005 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  info(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"

/*-------------------------------------------------------------------------
 *  ARMASignal -- Generates an ARMA signal
 *    Args:	residual signal,  ArSignal, numSamples
 *              coefZero, coefPole, orderZ, orderP 
 *    Returns:	returns the signal generated in arSignal 
 *    Bugs:	
 * -------------------------------------------------------------------------*/

	void ARMASignal(float *residual,float *arSignal,int numSamples,
			float *coefZero,int orderZ, float *coefPole, int orderP) {
        int             i, j;
        float           sum1, sum2;
	printf("OrderZ= %d OrderP= %d\n", orderZ, orderP);
	//      for(i=1; i<=orderP; i++)
	//printf("arSignal %d = %f\n",i, arSignal[i]); 
	for (i = 0; i < numSamples; i++){
	  arSignal[i+orderP] = coefZero[0]*residual[i+orderZ]; 
	  sum1 = 0.0;
          if (orderZ != 0)
	    for (j = 1; j < orderZ; j++)
	      sum1 = sum1 +coefZero[j]*residual[i+orderZ-j];
	  sum2 = 0.0;
	  for (j = 1; j < orderP; j++)
	    sum2 = sum2 + coefPole[j]*arSignal[i+orderP-j];
	  arSignal[i+orderP] = arSignal[i+orderP] + sum1 - sum2;
	  printf("residual %d=%f arSignal %d= %f\n",i+orderZ,residual[i+orderZ], 
	  i+orderP, arSignal[i+orderP]); 
	}
	}	/*  End of ARMAsignal		End of ARMAsignal   */


main (int argc, char **argv) {
  ASDF               *asdf;
  float              minFrequency, maxFrequency;
  int                windowSize, frameNum, numFrames, samplingRate;
  int                filterOrder;
  float              *numerator, *denominator;
  float              *filteredSignal = NULL, *signal = NULL;
  float              coefz[513], coefp[513], ax[513], ay[513],
                     amagz[513], phasez[513], amagp[513], phasep[513];
  FILE               *fp, *controlFile, *filtSpec;
  F_VECTOR           *waveform;
  int                i, j;

  if( argc < 8 )
  {
    printf("\nFilterSignal bandpass filters a signal using an nth order Butterworth filter\n");
    printf("\nUsage:FilterSignal filterOrder minFrequency MaxFrequency SamplingRate controlFile inFile outFile\n");
    return(-1);
  }
  sscanf(argv[1], "%d", &filterOrder);
  sscanf(argv[2], "%f", &minFrequency);
  sscanf(argv[3], "%f", &maxFrequency);
  sscanf(argv[4], "%d", &samplingRate);
  controlFile = fopen(argv[5], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  GsfOpen(asdf, argv[6]);
  fp = fopen (argv[7], "w");
  numerator = (float *) malloc((2*filterOrder+1)*sizeof(float));
  denominator = (float *) malloc((2*filterOrder+1)*sizeof(float));
  ButterWorthFilter(minFrequency, maxFrequency, samplingRate, filterOrder,
		    numerator, denominator);
  for (i = 0; i < 2*filterOrder+1; i++) {
    printf ("numerator [%d] = %f \n", i, numerator[i]);
    coefz[i+1] = numerator[i];
  }
  for (i = 0; i < 2*filterOrder+1; i++) {
    printf ("denominator [%d] = %f \n",i, denominator[i]);
    coefp[i] = denominator[i];
  }
  for (i = 2*filterOrder+2; i <= 512; i++) {
    coefz[i] = 0;
    coefp[i] = 0;
  }
  filtSpec = fopen("BW.spec", "w");
  Rfft(coefz, ax, ay, 9, 512, -1);
  SpectrumReal (512, ax, ay, amagz, phasez);
  Rfft(coefp, ax, ay, 9, 512, -1);
  SpectrumReal (512, ax, ay, amagp, phasep);
  for (i = 1; i <= 512; i++)
    fprintf(filtSpec, "%f\n", log(amagz[i]/amagp[i]));
  fclose(filtSpec);
  
  windowSize = GetIAttribute(asdf, "windowSize");
  numFrames = GetIAttribute(asdf, "numFrames");
  signal = (float *)malloc((windowSize+2*filterOrder+1)*sizeof(float));
  filteredSignal = (float *)malloc((windowSize+2*filterOrder+1)*sizeof(float));
  for (frameNum = 0; frameNum < numFrames; frameNum++) {
    waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
    if (frameNum == 0) {
      for (i = 0; i < 2*filterOrder; i++)
	signal[i] = 0;
      for (i = 0; i < 2*filterOrder; i++)
	filteredSignal[i] = 0;
    } else {
      for (i = 0; i < 2*filterOrder; i++)
	signal[i] = signal[windowSize+i];
      for (i = 0; i < 2*filterOrder; i++)
	filteredSignal[i] = filteredSignal[windowSize+i];
    }
    for (i = 0; i < windowSize; i++)
      signal[i+2*filterOrder] = waveform->array[i];
    
    ARMASignal (signal, filteredSignal, windowSize, numerator, 2*filterOrder,  denominator, 2*filterOrder);
    for (i = 0; i < windowSize; i++) {
      fprintf(fp, "%f\n", filteredSignal[i+2*filterOrder]);
    }
  }
  fclose(fp);

}

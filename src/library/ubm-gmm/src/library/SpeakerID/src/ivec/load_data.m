% loads data from a cell-list or a file name containing the
% the list
function [dmat,oplist,missed] = load_data (flist,listiscell)
  if nargin == 1
    listiscell = iscell (flist);
  end
  if listiscell == 0
    try
      oplist = readlistascell(flist);
    catch 
      'Unable to open file'
      return
    end
  else
    oplist = flist;
  end

  ii = length (oplist);
  if ii >= 1
    tempw = load(oplist{1});
    iveclen = length(tempw);
    dmat = zeros(iveclen,ii);
    dmat(:,1) = tempw;
    if ii == 1
      return;
    end
  else
    return;
  end
  j = 2;
  missed = [];
  for i=2:ii
    try
        dmat(:,j) = load(oplist{i});
        j = j + 1;        
    catch
        strcat('Skipping',oplist{i})
        missed = [missed i];
    end
  end

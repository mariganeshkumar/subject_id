import math
import getopt
import sys
import numpy as np
import scipy as sp
from tables import *
#from GMM import *
import GMM as GMM


def Usage ():
  print "python prg.py options"
  print "-M num_of_mixtures"
  print "-D dimensionality of feature"
  print "-i inputFile"
  print "-u UtteranceDump"
  print "-g Ubmfile"
  print "-o Output file"
  print "-n occupation list output file"
  return

def ReadUtterance (fname):
  fptr = open (fname, "r")
  print fname
  if fptr == None:
    return None
  ln = fptr.readline().strip()
  dim, nvec= map(int,ln.split())
  fvecs = np.zeros ((nvec,dim))
  for i in range (0,nvec):
    fvecs[i,:] = [ float(f) for f in fptr.readline ().strip ().split ()]
  fptr.close()
  return fvecs

optlist, args = getopt.getopt (sys.argv[1:], "M:D:i:u:g:o:n:")

M, D, ipfname, utt = -1, -1, "", ""
ubmfname, opfname, idfilename = "", "", ""

for arg in optlist:
  option = arg[0]
  if option == "-M":
    M = int (arg[1])
  elif option == "-D":
    D = int (arg[1])
  elif option == "-i":
    ipfname = arg[1]
  elif option == "-u":
    utt = arg[1]
    fvec = ReadUtterance (utt)
    nvec, dim = np.shape (fvec)
  elif option == "-g":
    gmm = GMM.ReadGMM (arg[1])
  elif option == "-o":
    opfname = arg[1]
  elif option == "-n":
    idfilename = arg[1]
  else:
    Usage()
    quit()

if M == -1 or D == -1 or ipfname == "":
  Usage ()
  quit()


ipf = openFile (ipfname, mode = "r")
tab = ipf.root.vmatrix
dimtab = ipf.root.desc
ncols, R= map (int, ((dimtab.read (start = 0, stop = 1))[0][0]).split())
R += 1
ncols = (np.shape (((tab.read (start = 0, stop = 1))[0])['colvec']))[0] # woosh!
Vmat = np.zeros ((ncols,R))
i = -1
for svec in tab.iterrows ():
  i += 1
  Vmat[:,i] = svec['colvec']

ipf.close ()
if ncols / M != D:
  print "Incompatible inputs for M and D"
  quit ()

st = 0
idfile = open (idfilename, "r")
indices = [ [ int (f) for f in ln.strip().split() ]  for ln in idfile.readlines ()]
cval = len (indices[0])
idfile.close ()
print "Read index file"

print "Computed indices" 
mixcnt = np.zeros ((M,1))
for m in range(0,len(indices)):
  for j in range(0,cval):
    mixcnt[indices[m][j]] += 1.

mixfreq = np.zeros ((M,1))
mixfreq = [ float(mixcnt[m]) / float(nvec) for m in range(0,M) ]

A = np.zeros ((R,R))
for m in range (0,M):
  vsub = Vmat [m*D:m*(D-1)+D,:]
  temp = np.dot (vsub.transpose() , vsub) * mixfreq[m]
  #if mixcnt[m] > 0.:
    #print (m,mixcnt[m])
  A = A + temp
for r in range(0,R):
  A[r,r] += 1.
A = np.linalg.inv (A)  
opf = openFile (opfname, mode = "w", title = "matrix A")
class tempClassSvec (IsDescription):
  colvec = Float32Col (R)

tab = opf.createTable ("/", 'amatrix', tempClassSvec, "foobar");
row = tab.row 
for r in range(0,R):
  row['colvec'] = A[:,r]
  row.append ()
tab.flush ()

opf.close ()


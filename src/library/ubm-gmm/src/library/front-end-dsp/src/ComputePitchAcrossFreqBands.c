/**************************************************************************
 *  $Id: ComputePitchAcrossFreqBands.c,v 1.1 2000/05/26 10:00:58 hema Exp hema $
 *  Release $Name:  $
 *
 *  File:	ComputePitchAcrossFreqBands.c - Computes features from a given
 *              utterance and writes it to a file.
 *
 *  Purpose:	Feature Extraction
 *
 *  Author:	Hema A Murthy,BSB 307,8342,9342
 *
 *  Created:    Wed 08-Mar-2000 21:51:19
 *
 *  Last modified:  Fri 25-Mar-2011 11:26:43 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "math.h"
void Usage () {
  printf ("ComputePitchAcrossFreqBands controlFile waveFile featureFile featureName startMinFrequency EndMinFrequency freqStep Bandwidth sum startFrame endFrame\n");
  fflush(stdout);
}
int main(int argc, char *argv[])
{
  FILE                  *c_file = NULL, *out_file = NULL;
  char                  *wavname =NULL, 
                        *cname = NULL, *cepname = NULL, *featureName = NULL;
  ASDF                  *asdf1, *asdf2;
  int                   i, j, k;
  int                   startFrame, endFrame, numVoicedFrames =0, samplingRate, frameShift;
  float                 minStartFrequency, maxStartFrequency, freqStep, bandWidth; 
  float                 lowFrequencyLimit, maxFrequencyLimit, rMax;
  int                   sum, numSpectra, frameNum;
  F_VECTOR              *fvect=NULL, *avgFVect=NULL;
  VECTOR_OF_F_VECTORS   *vfv;
  if(argc != 12) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  wavname = argv[2];
  cepname = argv[3];
  out_file = fopen(cepname ,"w");
  featureName = argv[4];
  sscanf(argv[5], "%f", &minStartFrequency);
  sscanf(argv[6], "%f", &maxStartFrequency);
  sscanf(argv[7], "%f", &freqStep);
  sscanf(argv[8], "%f", &bandWidth);
  sscanf(argv[9], "%d", &sum);
  sscanf(argv[10], "%d", &startFrame);
  sscanf(argv[11], "%d", &endFrame);
  c_file = fopen(cname,"r");
  asdf1 = (ASDF *) malloc(1*sizeof(ASDF));
  asdf2 = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeASDF(asdf1);
  InitializeStandardFrontEnd(asdf1,c_file);
  rewind(c_file);
  InitializeASDF(asdf2);
  InitializeStandardFrontEnd(asdf2,c_file);
  //    Cstore(asdf->fftSize);
  printf("testfile name = %s\n",wavname);
  fflush(stdout);
  GsfOpen(asdf1,wavname);
  GsfOpen(asdf2,wavname);
  printf("Total Number of frames = %d\n", asdf1->numFrames);
  samplingRate = (int) GetIAttribute(asdf1, "samplingRate");
  frameShift = (int) GetIAttribute(asdf1, "frameAdvanceSamples");
  if (startFrame == -1) { 
    startFrame = 0;
    endFrame = asdf1->numFrames;
  }
  if (endFrame > asdf1->numFrames) 
    endFrame = asdf1->numFrames;
  lowFrequencyLimit = minStartFrequency;
  maxFrequencyLimit = lowFrequencyLimit + bandWidth;
  numSpectra = ceilf((maxStartFrequency - minStartFrequency)/freqStep)+1;
  vfv = (VECTOR_OF_F_VECTORS *) malloc(numSpectra*sizeof(VECTOR_OF_F_VECTORS));  
  i = 0;
  while (avgFVect == NULL) {
    avgFVect = (F_VECTOR *) GsfRead(asdf1, i, featureName);
    i++;
  }
  for (frameNum = startFrame; frameNum < endFrame; frameNum++) {
    k = 0;
    lowFrequencyLimit = minStartFrequency;
    maxFrequencyLimit = lowFrequencyLimit + bandWidth;
    for (j = 0; j < numSpectra; j++) {
      if ( (j % 2) == 0) {
	asdf2->minPitch = lowFrequencyLimit;
	asdf2->maxPitch = maxFrequencyLimit;
	fvect = (F_VECTOR *) GsfRead(asdf2, frameNum, featureName);
      } 
      else {
	asdf1->minPitch = lowFrequencyLimit;
	asdf1->maxPitch = maxFrequencyLimit;
	fvect = (F_VECTOR *) GsfRead(asdf1, frameNum, featureName); 
      }
      if(fvect != NULL) {
	if (sum == 0) {
	  rMax = fvect->array[Imax0(fvect->array, fvect->numElements)];
	  for (i = 0; i < fvect->numElements; i++)
	    fvect->array[i] = fvect->array[i]/rMax;
	}
	vfv[k] = fvect;
	lowFrequencyLimit = lowFrequencyLimit + freqStep;
	maxFrequencyLimit = lowFrequencyLimit + bandWidth;
	k++;
      } //else break;
    }
    if (fvect != NULL)
      numVoicedFrames++;
    for (i = 0; i < avgFVect->numElements; i++)
      if (sum == 1)
	avgFVect->array[i] = 0;
      else
	avgFVect->array[i] = 1;
    for (j = 0; j < k; j++)
      for (i = 0; i < avgFVect->numElements; i++)
	if (sum == 1) 
	  avgFVect->array[i] = vfv[j]->array[i] + avgFVect->array[i];
	else
	  avgFVect->array[i] = vfv[j]->array[i]*avgFVect->array[i];
    for (i = 0; i < avgFVect->numElements; i++)
      avgFVect->array[i] = avgFVect->array[i]/numSpectra;
        for (k = 0; k < avgFVect->numElements; k++)
	  fprintf(out_file,"%f %f %d %e \n", (float)(frameNum*(frameShift))/samplingRate, (float) ((float) (k) /samplingRate), k, avgFVect->array[k]);
  }
  GsfClose(asdf1);
  GsfClose(asdf2);
  printf(" %d number frames processed \n", numVoicedFrames);
  fclose(out_file);
  return(0);
}














/**************************************************************************
 * $Log: ComputePitchAcrossFreqBands.c,v $
 * Revision 1.1  2000/05/26 10:00:58  hema
 * Initial revision
 *
 * Revision 1.1  2000/04/25 11:43:29  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of ComputePitchAcrossFreqBands.c
 **************************************************************************/

import math
import sys
import numpy as np
import scipy as sp
from tables import *
from GMM import *

def MatMult (fname1, fname2, trans1=False, trans2=False):
# now, read vmat.h5 to compute VV{t}
  ntabfile1 = openFile (fname1, mode="r")
  rtab1 = ntabfile1.root.vmatrix

  ntabfile2 = openFile (fname2, mode="r")
  rtab2 = ntabfile2.root.vmatrix
  
  # read first matrix
  temparr = rtab1.read (start=1,end=1)
  Md = (np.shape(temparr))[0]
  ncols = 0
  for row1 in rtab1.iterrows():
    ncols+=1

  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INIT THE EXPERIMENT
nx = 450;           % number of eigenvoices

% initial eigenvoices and eigenchannels matrices
%eigen_voices_mat_file   = '/export/hynek/sganapathy/speech/speakerid/jfa/models/plp2_lin_v2/gauss_models/ivec_tel/u_it002.mat';

% load the data
TRAINING_LIST='/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/scripts/train_ivec/female/fa_train_eigenchannels.scp';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN
% load the training data matrix
% N ... zero order sufficient stats
% F ... first order sufficient stats
% spk_ids ... numerical speaker identification (easy for matlab to do uniqe on
%             these)

disp('Reading list')
[spk_logical spk_physical] = parse_scp(TRAINING_LIST);

disp('Loading stats')
[N F] = load_stats_from_files(spk_physical);

% extract the spk_ids
disp('Creating speaker ids')
auxm  = regexp(spk_logical, '^[^_]+_[^_]+', 'match');
auxm  = [auxm{:}]';
[auxa auxb spk_ids] = unique(auxm);
clear auxa auxb auxm

% load initial u and v matrices
disp(['Setting v matrix to 0 ' ])
%load(eigen_voices_mat_file);

% we need the ubm params
% m ... supervector of means
% E ... supervector of variances
m = load(['/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/models/plp2_lin_v2/gauss/female/ubm_means'], '-ascii');
E = load(['/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/models/plp2_lin_v2/gauss/female/ubm_variances'], '-ascii');

% or do not load initial U matrix, but rather initialize it randomly
disp(['Initializing U matrix (randomly)'])

% Running from 8th iteration **********************************
u = randn(nx, size(F, 2)) * sum(E,2) * 0.001;
%load('/export/hynek/sthomas/speaker_id/BEST/sre10_evals/gender_dependent/t_matrix/tel_mic/female/mic_tel_t_matrix/u_it008.mat');

% we don't use the second order stats - set 'em to empty matrix
S = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_speakers=max(spk_ids);
n_sessions=size(spk_ids,1);

disp('Estimating factors');
% estimate speaker factors. these will be fixed througout the u estimation
%[y]=estimate_y_and_v_onestepMD(F, N, S, m, E, 0, v, 0, zeros(n_speakers,1), 0, zeros(n_sessions,1), spk_ids);
y = 0;
v=0;

% iteratively retrain u
for ii=1:10
  disp(' ')
  disp(['Starting iteration: ' num2str(ii)])

  ct=cputime;
  et=clock;

  % go baby...
  % note, that we don't use the reestimated y vectors (we want to keep the spk
  % direction constant and concentrate on the channel subspace)
  [x u]=estimate_x_and_u_onestepMD(F, N, S, m, E, 0, v, u, zeros(n_speakers,1), y, zeros(n_sessions,1), spk_ids);

  disp(['Iteration: ' num2str(ii) ' CPU time:' num2str(cputime-ct) ' Elapsed time: ' num2str(etime(clock,et))])

  out_mat_file = ['/export/hynek/sganapathy/speech/speakerid/sre10/gender_dependent/models/plp2_lin_v2/gauss/female/t_matrix/u_it' num2str(ii, '%03d')];
  disp(['Saving u to ' out_mat_file])
  save(out_mat_file, 'u');
end

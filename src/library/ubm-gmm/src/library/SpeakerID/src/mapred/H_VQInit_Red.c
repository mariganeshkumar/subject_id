/**********************************************************
*	This is the reducer function for H_VQInit.c. It expects
* input in a certain format: each line of input will be
* a key-value pair. the key and value is separated by a 
* tab. the values are comma separated lists of feature
* vectors that are preceded by the dimensionality of the
* feature. It sums up all feature vectors and outputs their
* mean for that particular key
***********************************************************/
#include "CommonFunctions.h"

int
main (int argc, char *argv[])
{
		int featLength;

		int key, prevKey = -1;
		int numVectors=0, i;
    F_VECTOR *vec1 = NULL, *vec2 = NULL;

		while (!feof (stdin))
		{
		    scanf ("%d\t%d",&key, &featLength);
			 fprintf (stderr, "key is %d and featLength is %d\n", key, featLength);
			  if (vec1 == NULL) vec1 = AllocFVector (featLength);							
				if (vec2 != NULL && featLength != vec2->numElements) return 1;
			  if (prevKey == key || prevKey == -1)
				{
							numVectors++;
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &vec1->array[i]);
							if (numVectors == 1) 
							{
											if (vec2 == NULL)
															vec2 = AllocFVector (featLength);
											for (i = 0; i < featLength; i++)
															vec2->array[i] = vec1->array[i];
											
							}				
							else
							    LinearVectorAddition (vec2, vec2, vec1); 
				      prevKey = key;
				}
				else
				{
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &vec1->array[i]);
						printf("%d\t%d",prevKey,featLength);
						for (i = 0; i < featLength; i++)
								printf (",%e", vec2->array[i]/((float)numVectors));
						printf ("\n");
						fflush (stdout);
						numVectors = 1;
						prevKey = key;
						for (i = 0; i < featLength; i++)
															vec2->array[i] = vec1->array[i];
				}
				scanf("\n");
		}
						printf("%d\t%d",prevKey,featLength);
						for (i = 0; i < featLength; i++)
								printf (",%e", vec2->array[i]/((float)numVectors));
						printf ("\n");
						fflush (stdout);
		return 0;
}

#include "sp/sphere.h"
/*****************************************************************************
  Function : sphereInterface.c reads sphere headered speech files
  Author   : Hema A Murthy
  Date     : Dec 10 1997
  Uses     : sphere library
  Known Bugs : none to date
  ****************************************************************************/



/*****************************************************************************
  function : ReadSpherePcm - reads sphere headered pcm files
  Inputs   : wavefilename
  Outputs  : short waveform, numSamples
  **************************************************************************/
  


short *ReadSpherePcm(char *wavname, long *numSamples) {
  SP_FILE             *waveFile;
  SP_INTEGER          samples = 0;
  long                numSamp = 0;
  short               *waveform;
  int                 success;
  int                 i;

  waveFile = sp_open(wavname,"r");
  success = sp_h_get_field(waveFile,"sample_count",T_INTEGER, &samples);
  printf("numSamp = %d\n",samples);
  if ((waveform = (short *) sp_data_alloc(waveFile,-1)) == NULL) {
    printf("unable to allocate space for waveform\n");
    exit(-1);
  }
  sp_rewind(waveFile);
  numSamp = sp_read_data(waveform, samples, waveFile);
  printf("numSamp = %d\n",numSamp);
  if (numSamp != samples) {  
    printf("numSamp = %d error reading waveform \n", numSamp);
    exit(-1);
  }
  *numSamples = numSamp;
/*for (i = 0; i < numSamp; i++)
  printf("waveform %d = %d \n", i, waveform[i]);*/
  sp_close(waveFile);
  return(waveform);
}
/*****************************************************************************
  function : ReadSphereUlaw - reads sphere headered ulaw files
  Inputs   : wavefilename
  Outputs  : short waveform, numSamples
  **************************************************************************/

short *ReadSphereUlaw(char *wavename,long *numSamples) {
  SP_FILE              *sp;
  SP_INTEGER            channelCount, sampleNBytes, sampleCount;
  int                   mode;
  long                  numSamp = 0;
  short                 *waveform;
  int                   success;
  int                   waveByteSize,
                        totalSamples,
                        samplesRead;

    sp = sp_open(wavename,"r");

    if (sp_set_data_mode(sp, "SE-PCM-2") > 0){
	sp_print_return_status(stdout);
	sp_close(sp);
	exit(-1);
    }
    if (sp_h_get_field(sp,"channel_count",T_INTEGER,&channelCount) > 0){
	fprintf(stderr,"Error: Unable to get the '%s' field\n",
		"channelCount");
	sp_close(sp);
	exit(-1);
    }
    /*  When the sd_set_data_mode() function is called to convert the file */
    /*  from ulaw to pcm, the sampleNBytes in the header is automatically*/
    /*  changed to 2                                                       */

if (sp_h_get_field(sp,"sample_n_bytes",T_INTEGER,&sampleNBytes) > 0){
	fprintf(stderr,"Error: Unable to get the '%s' field\n",
		"sampleNBytes");
	sp_close(sp);
	exit(-1);
    }
    if (sp_h_get_field(sp,"sample_count",T_INTEGER,&sampleCount) > 0){
	fprintf(stderr,"Error: Unable to get the '%s' field\n","sampleCount");
	sp_close(sp);
	exit(-1);
    }

    totalSamples=sampleCount * channelCount;
    waveByteSize=sampleNBytes * totalSamples;

    printf("---- Example 2: Expected channelCount=1,      Actually=%d\n",
		channelCount);
    printf("---- Example 2: Expected sampleNBytes=2,     Actually=%d\n",
		sampleNBytes);
    printf("---- Example 2: Expected sampleCount=16000,   Actually=%d\n",
		sampleCount);
    printf("---- Example 2: Expected totalSamples=16000,  Actually=%d\n",
		totalSamples);
    printf("---- Example 2: Expected waveByteSize=32000, Actually=%d\n",
	waveByteSize);

    if ((waveform=(short *)malloc(waveByteSize)) == (short *)0){
        fprintf(stderr,"Error: Unable to allocate %d bytes for the waveform\n",
                       waveByteSize);
	sp_close(sp);
	exit(-1);
    }
    
    if ((samplesRead=sp_read_data(waveform,totalSamples,sp)) !=
                     totalSamples){
        fprintf(stderr,"Error: Unable to read speech waveform, ");
	fprintf(stderr,"%d samples read\n",samplesRead);
	sp_print_return_status(stderr);
	sp_close(sp);
	exit(-1);
    }
printf("numSamp = %d\n",samplesRead);
fflush(stdout);
*numSamples=samplesRead;
sp_close(sp);
return(waveform);

}





/**********************************************************************

 ReadText reads speech data from a text_file

 Inputs :
        wavename : name of the text file
 Outputs : 
        waveform : array of short integers
	num_samples : number of samples read

***********************************************************************/
short *ReadText(char *wavename, long *numSamples)
{
  FILE *waveFile;
  long numSamp = 0;
  short *waveform;
  char line[254];
  waveFile = fopen(wavename,"r");
  while (fgets(line,254,waveFile) != NULL) {
     numSamp++;
  }
  waveform =(short *) malloc(numSamp*sizeof(short));
  rewind(waveFile);
  if (waveform == NULL) {
    printf("unable to allocate space for waveform \n");
    exit(-1);
  }
  numSamp=0;
  while (fgets(line,254,waveFile)!= NULL) {
    sscanf(line,"%d",&waveform[numSamp]);
    numSamp++;
  }
  *numSamples = numSamp;
  printf("number of samples read = %d\n",numSamp);
  fflush(stdout);
  fclose(waveFile);  
  return(waveform);
}























import numpy as np
def mel2hz(a):
    return 700 * (pow(10., a/2595) - 1.0)
def hz2mel(a):
    return 2595 * np.log10(1 + a/700)

def mel_filters(nf, fftSize = 256, minFreq = 0, maxFreq = 4000,
                srate = 8000, width = 1.0):
    fftfreqs = (np.array(range(0, fftSize)) * float(srate) /fftSize)
    minmel = hz2mel(float(minFreq))
    maxmel = hz2mel(float(maxFreq))
    cf = mel2hz(np.array(range(0, nf+2), dtype = float)/(nf+1) * (maxmel - minmel))
    mf = np.zeros((nf, fftSize))
    zz = np.zeros((fftSize,))
    for i in range(1, nf+1):
        fs = np.array(cf[range(i-1, i+2)])
        fs = fs[1] + width * (fs - fs[1])
        loslope = ((fftfreqs - fs[0])/(fs[1] - fs[0])).reshape((fftSize,))
        hislope = ((fs[2] - fftfreqs)/(fs[2] - fs[1])).reshape((fftSize,))
        pmin = np.min(np.array([loslope, hislope]), 0)
        mf[i-1,:] = np.max(np.array([zz, pmin]), 0)
    mf[:,((fftSize/2)+1):] = 0.
    return mf

def linear_filters(nf, fftSize = 256, minFreq = 0., maxFreq = 4000.,
                srate = 8000, width = 1.0):
    fftfreqs = (np.array(range(0, fftSize)) * float(srate) /fftSize)
    cf = np.array(range(0, nf+2), dtype = float)/(nf+1) * (maxFreq - minFreq)
    mf = np.zeros((nf, fftSize))
    zz = np.zeros((fftSize,))
    for i in range(1, nf+1):
        fs = np.array(cf[range(i-1, i+2)])
        fs = fs[1] + width * (fs - fs[1])
        loslope = ((fftfreqs - fs[0])/(fs[1] - fs[0])).reshape((fftSize,))
        hislope = ((fs[2] - fftfreqs)/(fs[2] - fs[1])).reshape((fftSize,))
        pmin = np.min(np.array([loslope, hislope]), 0)
        mf[i-1,:] = np.max(np.array([zz, pmin]), 0)
    mf[:,((fftSize/2)+1):] = 0.
    return mf

if __name__ == '__main__':
    from pylab import *
    mf = linear_filters(22, fftSize = 1024)
    for i in range(0, 22):
        a = mf[i, 1:522]
        a = a / max(a)
        plot(np.array(range(0, 521)) * 8000. / 1024 ,a ,  'black',
             linewidth = 1.4 ) 
        xlabel("Frequency", fontsize = 14)
        ylabel("Filter Co-efficients", fontsize = 14)
        title("Linear Filterbank", fontsize = 14)

    print(np.shape(a))
    show()

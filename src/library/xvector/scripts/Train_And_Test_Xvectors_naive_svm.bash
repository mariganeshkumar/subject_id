#!/usr/bin/env bash
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6
export LD_LIBRARY_PATH=/usr/local/cuda/lib64 
python library/xvector/scripts/Train_And_Test_Xvectors_naive_svm.py $1 $2 $3

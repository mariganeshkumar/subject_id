#!/bin/bash

NO_ARGS=4

if [ $# -lt $NO_ARGS ]; then
  echo "Usage: "
  echo "bash split_and_test.sh model_dir testlist ubm cval"
fi

mdir=$1
testlist=$2
ubm=$3
cval=$4

if [ ! -d $mdir ]; then
  echo "Model directory does not exist"
  exit 1
fi


nlists=`find $mdir -maxdepth 1 -iname spidmodel* | wc -l | cut -d ' ' -f1`
if [ $nlists -eq 0 ]; then
  echo "No model list found"
  exit 2
elif [ $nlists -eq 1 ]; then  
  spidmodels=`find $mdir -maxdepth 1 -iname spidmodel*`
else
  # use latest
  echo "Using latest spidmodels file"
  spidmodels=`ls -lrt $mdir/* | grep -i spidmodel* | tail -1 | awk '{print $(NF)}'`
fi  

if [ ! -f $testlist ]; then
  echo "Testlist does not exist"
  exit 1
fi

if [ ! -f $ubm ]; then
  echo "UBM does not exist"
  exit 1
fi


nt=`wc -l $testlist | cut -d ' ' -f1`
if [ `which parallel` = "" ]; then
  echo "Warning: parallel command not found"
  nc=`cat /proc/cpuinfo | grep -m 1 "cpu cores" | awk '{print $3}'`
else 
  nc=`parallel --number-of-cores`
fi

((nfiles=nt/nc))
((nfiles++))


mkdir $mdir/results

nspkrs=`wc -l $spidmodels | cut -d ' ' -f1`

start=1
for i in `seq 1 $nc`; do
    ((end=start+nfiles))
    echo test2004.db --resultFolder $mdir/results  --start $start \
    --end $end $spidmodels $nspkrs $testlist $ubm $cval &
    test2004.db --resultFolder $mdir/results  --start $start \
    --end $end $spidmodels $nspkrs $testlist $ubm $cval &
    ((start=end+1))
done
wait

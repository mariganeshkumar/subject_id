#ifndef __NewWeightedGMM_
#define __NewWeightedGMM_
void NewWeightedGMM(VECTOR_OF_F_VECTORS *vfv, int numVectors, 
		VECTOR_OF_F_VECTORS *mixtureMeans, 
		VECTOR_OF_F_VECTORS *mixtureVars, 
		float *mixtureElemCnt, int numMixtures, 
		int VQIter, int GMMIter, float probScaleFactor,
               int ditherMean, float varianceNormalize, int seed,
		VECTOR_OF_F_VECTORS *negData, int negNumVectors, float alpha);

void
NewWeightedGMMTopN (VECTOR_OF_F_VECTORS * vfv,
		int numVectors,
		VECTOR_OF_F_VECTORS * mixtureMeans,
		VECTOR_OF_F_VECTORS * mixtureVars,
		float *mixtureElemCnt,
		int numMixtures,
		int VQIter,
		int GMMIter,
		float probScaleFactor,
		int ditherMean,
		float varianceNormalize,
		int seed,
		VECTOR_OF_F_VECTORS * negData,
		int negNumVectors, float npercent);
void
NewWeightedGMMTopN_Mean (VECTOR_OF_F_VECTORS * vfv,
		int numVectors,
		VECTOR_OF_F_VECTORS * mixtureMeans,
		VECTOR_OF_F_VECTORS * mixtureVars,
		float *mixtureElemCnt,
		int numMixtures,
		int VQIter,
		int GMMIter,
		float probScaleFactor,
		int ditherMean,
		float varianceNormalize,
		int seed,
		VECTOR_OF_F_VECTORS * negData,
		int negNumVectors, float npercent);
void
NewWeightedGMMTopN_a (VECTOR_OF_F_VECTORS * vfv,
		int numVectors,
		VECTOR_OF_F_VECTORS * mixtureMeans,
		VECTOR_OF_F_VECTORS * mixtureVars,
		float *mixtureElemCnt,
		int numMixtures,
		int VQIter,
		int GMMIter,
		float probScaleFactor,
		int ditherMean,
		float varianceNormalize,
		int seed,
		VECTOR_OF_F_VECTORS * negData,
		int negNumVectors, float npercent);
#endif


#
# HISTORY
# 
# @(#)$RCSfile: .cshrc,v $ $Revision: 4.1.3.3 $ (DEC) $Date: 1992/05/11 09:13:09 $ 
# 

setenv PATH "${PATH}:/usr/local/bin"
setenv MAIL /usr/spool/mail/$USER
setenv NISTINC /usr/local/nist/include
setenv NISTLIB /usr/local/nist/lib
limit coredumpsize 100
if ($?HOSTNAME == 0) setenv HOSTNAME `hostname`
#
# Do the rest only for interactive shells
#
if ($?prompt) then
    if ($?history == 0) set history=30
    set filec
    set prompt="[$HOSTNAME]`dirs`> "

    alias cd 'cd \!*; set prompt="`dirs`>"'
    alias copy cp
    alias cls clear
    alias del 'rm -i'
    alias v 'ls -l'
    alias rm 'rm -i'
    alias dir 'ls'
    alias mail Mail
    alias m more
    alias f finger
    alias ffind "find \!:1 \( -name \!:2* \) -print"
    alias findx 'find \!:1 \( -name \!:2- \) -exec \!$ \{\} \;'
    alias hi history
    alias pushd 'pushd \!*; set prompt="[$HOSTNAME]`dirs|/usr/local/bin/trunc 30`> "'
    alias popd 'popd \!*; set prompt="[$HOSTNAME]`dirs|/usr/local/bin/trunc 30`> "'
    alias po popd
    alias pu pushd
    alias ren mv
    alias so source
    alias type 'more'
    alias v 'ls -l'
    alias javac '/usr/local/jdk1.2/bin/javac'
    alias ls 'ls -F'
    setenv CLASSPATH ~/cygnet/classes:/usr/cygnet/CygNet120R3/AdventNetSNMPv3/classes:/usr/local/jdk1.2/lib/dt.jar
    setenv CygSrc ~/cygnet/src
    alias emacs 'emacs -fn -adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1'
endif 
nfrm -S









/*
 *                            COPYRIGHT
 *
 *  bwlp - Butterworth lowpass filter coefficient calculator
 *  Copyright (C) 2003, 2004, 2005 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  info(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"

main (int argc, char **argv) {
  ASDF               *asdf;
  float              minFrequency, maxFrequency;
  int                windowSize, frameNum, numFrames, samplingRate;
  int                filterOrder;
  float              *numerator, *denominator;
  float              *filteredSignal = NULL, *signal = NULL;
  float              coefz[513], coefp[513], ax[513], ay[513],
                     amagz[513], phasez[513], amagp[513], phasep[513];
  FILE               *fp, *controlFile, *filtSpec;
  F_VECTOR           *waveform;
  int                i, j;

  if( argc < 8 )
  {
    printf("\nFilterSignal bandpass filters a signal using an nth order Butterworth filter\n");
    printf("\nUsage:FilterSignal filterOrder minFrequency MaxFrequency SamplingRate controlFile inFile outFile\n");
    return(-1);
  }
  sscanf(argv[1], "%d", &filterOrder);
  sscanf(argv[2], "%f", &minFrequency);
  sscanf(argv[3], "%f", &maxFrequency);
  sscanf(argv[4], "%d", &samplingRate);
  controlFile = fopen(argv[5], "r");
  asdf = (ASDF *) malloc(1*sizeof(ASDF));
  InitializeStandardFrontEnd(asdf, controlFile);
  GsfOpen(asdf, argv[6]);
  fp = fopen (argv[7], "w");
  //  numerator = (float *) malloc((filterOrder+1)*sizeof(float));
  numerator = (float *) BandPassFilter(minFrequency, maxFrequency, samplingRate, filterOrder);
  for (i = 0; i < filterOrder; i++) {
    printf ("numerator [%d] = %f \n", i, numerator[i]);
    coefz[i+1] = numerator[i];
  }
  for (i = filterOrder; i <= 512; i++) 
    coefz[i] = 0;

  filtSpec = fopen("BW.spec", "w");
  Rfft(coefz, ax, ay, 9, 512, -1);
  SpectrumReal (512, ax, ay, amagz, phasez);
  for (i = 1; i <= 512; i++)
    fprintf(filtSpec, "%f\n", log(amagz[i]));
  fclose(filtSpec);
  
  windowSize = GetIAttribute(asdf, "windowSize");
  numFrames = GetIAttribute(asdf, "numFrames");
  signal = (float *)malloc((windowSize+filterOrder)*sizeof(float));
  filteredSignal = (float *)malloc((windowSize+filterOrder)*sizeof(float));
  for (frameNum = 0; frameNum < numFrames; frameNum++) {
    waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
    if (frameNum == 0) {
      for (i = 0; i < filterOrder; i++)
	signal[i] = 0;
       } else {
      for (i = 0; i < filterOrder; i++)
	signal[i] = signal[windowSize+i];
    }
    for (i = 0; i < windowSize; i++)
      signal[i+filterOrder] = waveform->array[i];
    
    MASignal (signal, filteredSignal, windowSize, numerator, filterOrder);
    for (i = 0; i < windowSize; i++) {
      fprintf(fp, "%f\n", filteredSignal[i]);
    }
  }
  fclose(fp);

}

/*
    This file computes the cosine similarity between the test i vector
    to all the train i vectors and assigns the best

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/



#include "gsl_helper.h"
#include<string.h>
#define CHAR_ALLOC(k) (char*)malloc(sizeof(char)*k)


int main(int argc, char *argv[]) {
	if(argc!=6) {
		printf("Usage : %s ivecList wccnMat whitenMat outList R\n",argv[0]);
		exit(0);
	}
	FILE *ivecListFile, *outListFile;
	char *ivecListName, *outListName, **ivecList, **outList, *wccnMatName, *wMatName, line[256];
	int i,j,R,ivecCount=0;
	double tempf, tempf1, tempf2;
	gsl_matrix *wccnMat, *wMat, *tempMat, *ivecProjMat, *resultMat;
	gsl_vector *tempVect, *tempVect1;

	ivecListName=argv[1];
	wccnMatName=argv[2];
	wMatName=argv[3];
	outListName=argv[4];
	R=atoi(argv[5]);
	ivecListFile=fopen(ivecListName,"r");
	outListFile=fopen(outListName,"r");
	while(fgets(line,sizeof(line),ivecListFile)!=NULL) ivecCount++;
	rewind(ivecListFile);
	ivecList=(char **)malloc(sizeof(char *)*ivecCount);
	outList=(char **)malloc(sizeof(char *)*ivecCount);
	for(i=0;!feof(ivecListFile);i++) {
		ivecList[i]= CHAR_ALLOC(256);
		fscanf(ivecListFile,"%s\n",ivecList[i]);
	}
	if(i!=ivecCount) {
		fprintf(stderr,"Error while reading train list\n");
		exit(1);
	}
	for(i=0;!feof(outListFile);i++) {
		outList[i]=CHAR_ALLOC(256);
		fscanf(outListFile,"%s\n",outList[i]);
	}
	if(i!=ivecCount) {
		fprintf(stderr,"Error while reading out list\n");
		exit(1);
	}

	wccnMat = gsl_matrix_alloc(R,R);
	wMat = gsl_matrix_alloc(R,R);
	tempVect = gsl_vector_alloc(R);
	tempVect1 = gsl_vector_alloc(R);
	tempMat = gsl_matrix_alloc(R,ivecCount);
	ivecProjMat = gsl_matrix_alloc(R,ivecCount);
	gsl_matrix_set_zero(ivecProjMat);

	wccnMat = read_matrix(wccnMatName,R,R);
	wMat = read_matrix(wMatName,R,R);
	gsl_matrix_transpose(wccnMat);

/*      Differs from gsl_testCosine2.c in the following step. Normalization after 
        each projection  */

	for(i=0;i<ivecCount;i++) {
		tempVect1=read_vector(ivecList[i],R);
		gsl_blas_dgemv(CblasTrans, 1.0, wMat, tempVect1, 0.0, tempVect);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(tempMat,i,tempVect);
	}


	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat,tempMat,0.0,ivecProjMat);

       for(i=0;i<ivecCount;i++) {
		gsl_matrix_get_col(tempVect,ivecProjMat,i);
		tempf=gsl_blas_dnrm2(tempVect);
		gsl_vector_scale(tempVect, 1.0/tempf);
		gsl_matrix_set_col(ivecProjMat,i,tempVect);
	}


	for(i=0;i<ivecCount;i++) {
		gsl_matrix_get_col(tempVect,ivecProjMat,i);
		write_vector(outList[i],tempVect);
	}

	gsl_matrix_free(wccnMat);
	gsl_matrix_free(wMat);
	gsl_matrix_free(tempMat);
	gsl_matrix_free(ivecProjMat);
	gsl_matrix_free(resultMat);
	gsl_vector_free(tempVect);
	gsl_vector_free(tempVect1);
}

#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include<cuda.h>

#define finc(i,j,k) for(i=0;i<j;i+=k)
#define LOG_ZERO  (-1.0E20)
#define LOG_ONE  (0.0)
#define LOG_SMALL (-0.5E10)
#define PRINTLINE printf("\n");



typedef struct {
  int numElements;
  float *array;
} F_VECTOR;

typedef F_VECTOR* VFV;

struct topCMixtures_ {
      int id;
      float DistortionValue;
};

typedef struct topCMixtures_ topCMixtures;

F_VECTOR* AllocFVector (int n);
float ComputeProbability(F_VECTOR *mixtureMean, 
			  F_VECTOR *mixtureVar, float priorProb, 
			 F_VECTOR *fvect, float probScaleFactor, float scale) ;


float  *ComputeTopCLikelihood (VFV *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VFV **speakerMeans, 
			   VFV **speakerVars, 
			   VFV *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds, float *mixScales);
void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue);
int **GetTopCMixtures (VFV *ubmModelMeans, VFV *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, float probScaleFactor
                      );

float LogAdd(float x, float y);

float LogAdd(float x, float y)
{
	float temp, diff, z;
        float value;

	if (x < y) {
		temp = x;
		x = y;
		y = temp;
	}
	diff = y - x;

	if (diff < LOG_ZERO)
		return (x < LOG_SMALL) ? LOG_ZERO : x;
	else {
		z = expf(diff);
		return x + log(1.0 + z);
	}
return(value);
}
F_VECTOR *AllocFVector (int n) {
      F_VECTOR *f = NULL;
      f = (F_VECTOR *) calloc (1, sizeof (F_VECTOR));
      if (f == NULL) return f;
      f->array = (float *) calloc (n, sizeof(float));
      f->numElements = n;
      return f;

}

float ComputeProbability(F_VECTOR *mixtureMean, 
			  F_VECTOR *mixtureVar, float priorProb, 
			 F_VECTOR *fvect, float probScaleFactor, float scale) {

  int                     i;

  float                   sumProb = 0;
  float                   floorValue = 0.0;

  sumProb = scale;
  for (i = 0; i < fvect->numElements; i++) {
    floorValue =  floorValue +
    (mixtureMean->array[i] - fvect->array[i])*
    (mixtureMean->array[i] - fvect->array[i])
    /(2*mixtureVar->array[i]);
  }
  sumProb = sumProb - floorValue; 
  return(sumProb + log(probScaleFactor));
}  
float  *ComputeTopCLikelihood (VFV *vfv, unsigned int  numVectors, 
			   int numSpeakers, 
			   VFV **speakerMeans, 
			   VFV **speakerVars, 
			   VFV *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds,
        float *mixScales
        ) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0.0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor, mixScales[mixNumber]);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor, mixScales[mixNumber]));
	  }
	       if (mixProbValue > LOG_ZERO )	 
	Distortion[i] = Distortion[i] + mixProbValue;
    }
  }
/*  evidence = Distortion[0];
  for (i = 1; i < numSpeakers; i++)
    evidence = LogAdd(evidence, Distortion[i]);
  for (i = 0; i < numSpeakers; i++)
    Distortion[i] = Distortion[i] - evidence;*/
  return(Distortion);
}


void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }


__global__ void
GpuMixtureProbability (float *mean, float *var, float *wts, 
                       float *vfv, float *dist, int dim,
                       int nc) {
    int i,j,k,l,t,ii,jj;
    float f,g = 0.0;

    i = blockIdx.x; // cluster number
    j = blockIdx.y; // vector number
    ii = i * dim;
    jj = j * dim;

    f =  wts[i];
    for (t = 0; t < dim; t++) {
      k = jj+t;  //actual element in vfv
      l = ii+t; // actual element in model
      f += ((vfv[k] - mean[l]) * (vfv[k] - mean[l]))/ (var[l]);
    }
    dist[(j * nc) + i] = -(f * 0.5);
}


/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VFV *ubmModelMeans, VFV *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k, dim;
    float *Dist, *h_dist, *flatvec;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;
    dim3 grid(numClusters,numVectors);
    // pointers to pointers may not be supported in
    // CUDA. so, flattening all vfvs 
    float *gpu_ubm_m, *gpu_ubm_v, *gpu_vfv;
    float *gpu_dist, *gpu_ubm_w;
    cudaStream_t cstream;

    dim = vfv[0]->numElements;
    
    // first allocate memory in GPU
    // make sure the numVectors is not too large. how much is too many
    // depends on the memory available in the gpu

    cudaMalloc ((void **)&gpu_ubm_m, sizeof(float) * numClusters * dim);
    cudaMalloc ((void **)&gpu_ubm_v, sizeof(float) * numClusters * dim);
    cudaMalloc ((void **)&gpu_ubm_w, sizeof(float) * numClusters); 
    cudaMalloc ((void **)&gpu_vfv, sizeof(float) * numVectors * dim);
    cudaMalloc ((void **)&gpu_dist, sizeof(float) * numVectors * numClusters);
    // copy ubm means to gpu
    flatvec = (float *) calloc (dim*numClusters, sizeof(float));
    k = 0;
    finc(i,numClusters,1) 
      finc(j,dim,1) 
        flatvec[k++]= ubmModelMeans[i]->array[j];
    cudaMemcpy (gpu_ubm_m, flatvec, sizeof(float)*dim*numClusters,
                    cudaMemcpyHostToDevice);
    // copy ubm vars to gpu
    k = 0;
    finc(i,numClusters,1) 
      finc(j,dim,1) 
        flatvec[k++]= ubmModelVars[i]->array[j];
    finc(i,numClusters,1)
        cudaMemcpy (gpu_ubm_v, flatvec, sizeof(float)*dim*numClusters,
                    cudaMemcpyHostToDevice);
    free (flatvec);

    // copy ubm wts to gpu
    flatvec = (float *) calloc (numClusters, sizeof(float));
    finc(i,numClusters,1) flatvec[i] = log (ubmWts[i]);
    finc(i,numClusters,1) 
        finc(j,dim,1)
            flatvec[i] += log(ubmModelVars[i]->array[j]);
    cudaMemcpy (gpu_ubm_w, flatvec, sizeof(float)*numClusters,
                    cudaMemcpyHostToDevice);
    free (flatvec);
        
    // copy vfvs to gpu
    flatvec = (float *) calloc (dim*numVectors, sizeof(float));
    k = 0;
    finc(i,numVectors,1)
        finc(j,dim,1)
            flatvec[k++] = vfv[i]->array[j];
    cudaMemcpy (gpu_vfv, flatvec, sizeof(float)*dim*numVectors,
                    cudaMemcpyHostToDevice);
    free (flatvec);

    // call computeprob func         
    cudaStreamCreate (&cstream);
    GpuMixtureProbability <<<grid, 1,0, cstream>>> 
    (gpu_ubm_m, gpu_ubm_v, gpu_ubm_w, gpu_vfv, gpu_dist, dim, numClusters);
    cudaStreamSynchronize (cstream);
    
    
    // calls to gpu are non-blocking. not sure what to do about it
    h_dist = (float *) calloc (numClusters * numVectors, sizeof(float));
    cudaMemcpy (h_dist, gpu_dist, sizeof(float) * numVectors * numClusters,
                cudaMemcpyDeviceToHost);
    cudaFree (gpu_ubm_m);
    cudaFree (gpu_ubm_v);
    cudaFree (gpu_ubm_w);
    cudaFree (gpu_vfv);
    cudaFree (gpu_dist);
    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
        
      Dist = (float *)&h_dist[j*numClusters];  
    	for (k = 0; k < cValue; k++)
        {
          bestCMixtures[k].id = k+1;
          bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }
    free (bestCMixtures);
    free (h_dist);
    return mixtureIds;  
  }

void Usage() {
   printf ("Usage : gmmiter.gpu testList ubm ubmSize gmmfile featLength\n");
   return;
}



int 
main(int argc, char *argv[]) {
  FILE                  *ubmFile=NULL, *tempfptr, *waveFile = NULL;
  FILE                  *speakerFile = NULL, *modelFile = NULL, *outFile = NULL;
  char                  *wavname =NULL, *ubmFileName = NULL, *models = NULL;
  char                  *resultFolderName = NULL, *outFileName;
  char                  speakerModel[500],line[500], testname[500];
  VFV                   *ubmModelMeans, *ubmModelVars;
  VFV                   *vfv, *tempMeanClusters, *tempVarClusters;
  int                   i, j, k, numSpeakers, **mixIds, *numClusters;
  int                   thisMix;
  int                   featLength, cValue, ubmSize, totalVecLen;
  unsigned int          numVectors;
  int                   verify, numGaussiansErg=3, startNo, endNo;
  float                 *ubmWts, *Distortion, *mixScales, *tempWts;
  float                 thresholdScale, PI = 2.14, l2PI = log(2 * PI);

  if (argc != 6) {
    Usage();
    exit(-1);
  }
  wavname = argv[1];
  ubmFileName = argv[2];
  sscanf (argv[3], "%d", &ubmSize);
  outFileName = argv[4];
  sscanf (argv[5], "%d", &featLength);
  
  printf ("featLength = %d\n", featLength);
  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VFV *) calloc (ubmSize, sizeof (VFV));
  ubmModelVars  = (VFV *) calloc (ubmSize, sizeof (VFV));
  ubmWts        = (float *) calloc (ubmSize, sizeof (float));
  mixScales     = (float *) calloc (ubmSize, sizeof(float));
  for (i = 0; i < ubmSize; i++)
    {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      mixScales[i]= 0.0;
      for (j = 0; j < featLength; j++) {        
	        fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
          if (ubmModelVars[i]->array[j] != 0.) mixScales[i] += log (ubmModelVars[i]->array[j]);
	    }     
      mixScales[i] = log (ubmWts[i]) - 0.5 * (featLength * l2PI + mixScales[i]);
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);
  tempMeanClusters = (VFV *) calloc (ubmSize, sizeof(VFV));
  tempVarClusters = (VFV *) calloc (ubmSize, sizeof(VFV));
  tempWts = (float *) calloc (ubmSize, sizeof(float));
  for (i = 0; i < ubmSize; i++) {
      tempMeanClusters[i] = (F_VECTOR *) AllocFVector(featLength);
      tempVarClusters[i] = (F_VECTOR *) AllocFVector(featLength);
      for (j = 0; j < featLength; j++) {
        tempMeanClusters[i]->array[j] = 0.0;
        tempVarClusters[i]->array[j] = 0.0;
      }
      tempWts[i] = 0.;
  }
  
  waveFile = fopen (wavname, "r");
  outFile = fopen(outFileName, "w");
  if (outFile == NULL) {
      printf ("unable to create file %s ... \n", outFileName);
      exit(1);
  }
  while (!feof (waveFile)) {
      numVectors = 0;
      fscanf (waveFile, "%s\n", testname);
      tempfptr = NULL;
      tempfptr = fopen (testname, "r");
      fscanf (tempfptr, "%d %d", &featLength, &numVectors);
      vfv = (VFV *) calloc (numVectors, sizeof(VFV));
      finc(i,numVectors,1) {
          vfv[i] = (F_VECTOR *) calloc (1, sizeof(F_VECTOR));
          vfv[i]->array = (float *) calloc (featLength, sizeof(float));
          if (vfv[i]->array == NULL) {
             printf ("Unable to allocate mem\n");
             exit(1);
          }
          vfv[i]->numElements = featLength;
          finc(j,featLength,1)
              fscanf (tempfptr, " %e", &vfv[i]->array[j]);
      }
      fclose (tempfptr);

      mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, ubmSize, vfv, numVectors, 1, 1.0); 
      totalVecLen += numVectors;
      for (j = 0; j < numVectors; j++) {
          thisMix = mixIds[j][0]-1;
          tempWts[thisMix] += 1.0;
          for (i = 0; i < featLength; i++) {
              tempMeanClusters[thisMix]->array[i] += vfv[j]->array[i];
              tempVarClusters[thisMix]->array[i] += (vfv[j]->array[i] 
                                                  * vfv[j]->array[i]);
          }
      }
      
      printf ("Processed file:%s\n", wavname);
      for (j = 0; j < numVectors;j++) {
        free (vfv[j]->array);
        free (vfv[j]);
        free (mixIds[j]);
      }
      free(vfv);
      free (mixIds);
  }
  for (k = 0; k < ubmSize; k++) {
      for (j = 0; j < featLength; j++) 
          tempMeanClusters[k]->array[j] /= tempWts[k];
      
      for (j = 0; j < featLength; j++) 
          tempVarClusters[k]->array[j] = (tempVarClusters[k]->array[j]/tempWts[k]) -
                                         (tempMeanClusters[k]->array[j] *
                                          tempMeanClusters[k]->array[j]);
      tempWts[k] /= (float) totalVecLen; 
      fprintf (outFile, "%e\n", tempWts[k]);
      for (j = 0; j < featLength; j++) 
          fprintf (outFile, " %e %e", tempMeanClusters[k]->array[j],
                                      tempVarClusters[k]->array[j]);
      fprintf (outFile, "\n");
      free (tempMeanClusters[k]->array); 
      free (tempVarClusters[k]->array); 
      free (tempMeanClusters[k]);
      free (tempVarClusters[k]);
  }
  free (tempWts);
  fclose (outFile);  
  fclose (waveFile);
  free (mixScales);
}

function [val_p_labels, val_label, predicted_labels, test_label] = classify_using_x_vector_naive(config,allData)


models_dir =  [allData.modelSaveDir,'/',config.dnn_model_files];
system(['rm -rf ', config.tmp_dir]);
mkdir(config.tmp_dir);
train_data = allData.train_data;
test_data = allData.test_data;

disp('collectin train data');
train_data_cell={};
train_label={};
parfor i=1:length(train_data)
    ind=1;
    train_data_s=[];
    train_label_s=[];
    for j=1:length(train_data{i})
        for k=1:length(train_data{i}{j})
            recording_data=train_data{i}{j}{k};
            train_data_s(ind,:,:,:) = recording_data;
            train_label_s(ind)=i;
            ind = ind+1;
        end
    end
    train_data_cell{i}=train_data_s;
    train_label{i}=train_label_s;
end
train_data_mat=cat(1,train_data_cell{:});
train_label = cat(2,train_label{:});


disp('preparing val data')
val_data_cell = {};
val_label={};
test_data_cell = {};
test_label = {};

val_per = config.val_per;


parfor i=1:length(test_data)
    val_ind=1;
    test_ind=1;
    val_data_s=[];
    val_label_s=[];
    test_data_s=[];
    test_label_s=[];
    for j=1:length(test_data{i})
        for k=1:length(test_data{i}{j})
            recording_data=test_data{i}{j}{k};
            if k <= length(test_data{i}{j}) * val_per
                val_data_s(val_ind,:,:,:) = recording_data;
                val_label_s(val_ind)=i;
                val_ind = val_ind+1;
            else
                test_data_s(test_ind,:,:,:) = recording_data;
                test_label_s(test_ind)=i;
                test_ind = test_ind+1;
            end
        end
    end
    val_data_cell{i} = val_data_s;
    val_label{i} = val_label_s;
    test_data_cell{i} = test_data_s;
    test_label{i} = test_label_s;
end
val_data_mat = cat(1, val_data_cell{:});
val_label = cat(2, val_label{:});
test_data_mat = cat(1, test_data_cell{:});
test_label = cat(2, test_label{:});

[predicted_labels, val_p_labels] = run_ann(config.seed, config.hiddenlayers, config.tmp_dir, train_data_mat, train_label,val_data_mat, val_label, test_data_mat, test_label, models_dir, config.test_only);

end

function [predicted_labels, val_labels] = run_ann(seed, hidden_layer_size, tmp_dir, train_data, train_label ,val_data, val_label, test_data, test_label, save_dir, only_test)
disp('writing files for X-vector training')
save([tmp_dir,'/subject_id_dataset_for_keras.mat'],'train_data','train_label','val_data', 'val_label','test_data','test_label','hidden_layer_size','-v7.3')
clear 'train_data' 'train_label' 'test_data' 'test_data' 'val_label' 'val_data' 'test_label';
system(['bash library/xvector/scipts/Train_And_Test_Xvectors_naive.bash ', save_dir, ' ', num2str(only_test), ' ', num2str(seed)])
data = load([tmp_dir,'/predicted_labels.mat']);
predicted_labels = double(data.predicted_labels);
val_labels = double(data.predicted_val_labels);
end

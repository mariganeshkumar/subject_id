/*
 * =====================================================================================
 *
 *       Filename:  TestStg.c
 *
 *    Description:  run stg on a feature dump
 *
 *
 * =====================================================================================
 */
#include "CommonFunctions.h"
int
main(int argc, char *argv[])
{
    unsigned int      nvec;
    int 	      len;
    VFV               *vfv = NULL, *new_vfv = NULL;

    if (argc != 4) {
        fprintf (stderr, "Usage: teststg ipfile opfile len \n");
        return 1;
    }

    vfv = ReadVfvFromFile (argv[1], &nvec);
    sscanf(argv[3], "%d", &len);
    new_vfv = Gaussianization (vfv, nvec, len);
    WriteVfvToFile (new_vfv, nvec, argv[2]);

    FreeVfv (vfv,nvec);
    FreeVfv (new_vfv,nvec);
}

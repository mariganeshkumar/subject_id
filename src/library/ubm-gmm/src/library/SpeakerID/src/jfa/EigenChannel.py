import math
import sys
import numpy as np
import scipy as sp
from tables import *
from GMM import *

M,D,R = map(int,sys.argv[1:4])  # for convenience
colstart, colend = map(int, sys.argv[4:6])
opfname = sys.argv[6]

############# CLASS DEFINITION BEGINS #############
# Description class for hdf5 files
class svector (IsDescription):
  colvec = Float32Col (M*D)
############## END OF CLASS DEFINITION ##############

tabfile = openFile ("svector.h5",mode = "r")
cvtable = tabfile.root.vmatrix
idtable = tabfile.root.indices

ntabfile = openFile ("vmat.h5", mode="w", title="NewVMatrix")
rtab = ntabfile.createTable("/", 'vmatrix', svector, 'Mean centred rows')
nrow = rtab.row
tot=0
for ridx in idtable.iterrows(): 
  st = ridx['start']
  en = ridx['stop']
  l = en - st + 1
  S0 = np.zeros((M*D,l))
  
  i=0  
# row indices are zero-indexed
  for svec in cvtable.iterrows(start=st-1,stop=en):
    S0[:,i] = svec['colvec']
    i+=1
  meanVec = np.mean (S0,0)
  S0 = S0 - meanVec
  for j in range(0,i):
    nrow['colvec'] = S0[:,j]
    tot+=1
    nrow.append()
  

## clean up
rtab.flush()
ntabfile.close()
tabfile.close()

# now, read vmat.h5 to compute VV{t}
restabfile = openFile (opfname,mode="w")
tot = 1334
class tempVVTClass (IsDescription):
  colno = Int32Col()
  colvec = Float32Col (tot)

# create the table if it is not there already
#if 'vvtmatrix' not in dir(restabfile.root):
vvtab = restabfile.createTable ("/", 'vvtmatrix', tempVVTClass, "SSt matrix")
#else:
#  vvtab = restabfile.root.vvtmatrix


ntabfile = openFile ("vmat.h5", mode="r")
rtab = ntabfile.root.vmatrix


vvrow = vvtab.row

colstart -= 1
colend -= 1
collen = M * D
colno = -1

for svec1 in rtab.iterrows():
  colno += 1
  if colno < colstart:
    continue
  resvec = np.zeros((tot,)) 
  leftarg = svec1['colvec']
# always start with the 1st col in S matrix
  rowno = -1
  for svec2 in rtab.iterrows():
    rowno += 1
    if rowno < colno:
      resvec[rowno] = 0.
      continue
    rightarg = svec2['colvec']
# leftarg and rightarg need to be multiplied
    tempfval = 0.
    for i in range(0,collen):      
      tempfval += (leftarg[i] * rightarg[i])
    resvec[rowno] = tempfval
  vvrow['colvec'] = resvec
  vvrow['colno'] = colno
  vvrow.append()
  if colend <= colno:
    break
vvtab.flush()
ntabfile.close()
restabfile.close()

#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: GroupDelaySpectrum.c,v 1.1 2011/03/24 16:56:00 hema Exp hema $
 *  File:	GroupDelaySpectrum.c - Computes the Group Delay Spectrum
 *              of a given signal, averaged over a number of frames.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Fri 25-Mar-2011 11:28:51 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/QuickSort.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average LP Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the LP Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : GroupDelaySpectrum ctrlFile waveFile SpecFile frameStart frameEnd\n");
}
/*****************************************************************************/

int     main (int argc, char *argv[])
	{
	  int  	          i, fftSize, numFrames, numVoicedFrames = 0,
	                  frameNum, vad,
	                  frameStart, frameEnd, nfby2;
	  char            fNameSpec[100],*ftemp=NULL;
	  float           *ModGdSpecAvg, 
	                  *MinGdSpecAvg,  
	                  *ModGdLPSpecAvg, 
	                  *LPGdSpecAvg;
        FILE              *specFile, *controlFile;
        static ASDF       *asdf;
	F_VECTOR          *fvect;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       nfby2 = fftSize/2;
       vad = (int) GetIAttribute(asdf, "vad");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       printf ("fftSize = %d vad = %d numFrames=%d\n", fftSize, vad, numFrames);       
       sscanf(argv[4], "%d", &frameStart);
       sscanf(argv[5], "%d", &frameEnd);
       fNameSpec[0] = '\0';
       ftemp = argv[3];
       Cstore(fftSize);
       strcpy(fNameSpec,ftemp);
       specFile = fopen(fNameSpec, "w");
       MinGdSpecAvg  = (float *) AllocFloatArray(MinGdSpecAvg, fftSize+1);
       ModGdSpecAvg  = (float *) AllocFloatArray(ModGdSpecAvg, fftSize+1);
       ModGdLPSpecAvg  = (float *) AllocFloatArray(ModGdLPSpecAvg, fftSize+1);
       LPGdSpecAvg = (float *) AllocFloatArray(LPGdSpecAvg, fftSize+1);
       if ((frameStart == -1)  && (frameEnd == -1)) {
	 frameStart = 0;
	 frameEnd = numFrames;
       }
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 if (asdf->vU[frameNum]) {
	   numVoicedFrames++;
	   fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameMinGDelay");
	   for (i = 0; i < nfby2; i++)
	     MinGdSpecAvg[i] = MinGdSpecAvg[i] + fvect->array[i];
	   free(fvect->array);
	   free(fvect);
	   fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameModGDelaySmooth");
	   for (i = 0; i < nfby2; i++)
	     ModGdSpecAvg[i] = ModGdSpecAvg[i] + fvect->array[i];
	   free(fvect->array);
	   free(fvect);
	   fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameModGDelayLPSmooth");
	   for (i = 0; i < nfby2; i++)
	   ModGdLPSpecAvg[i] = ModGdLPSpecAvg[i] + fvect->array[i];
	   free(fvect->array);
	   free(fvect);
	   fvect = (F_VECTOR *) GsfRead(asdf, frameNum, "frameLPGDelay");
	   for (i = 0; i < nfby2; i++)
	     LPGdSpecAvg[i] = LPGdSpecAvg[i] + fvect->array[i];
	   free(fvect->array);
	   free(fvect);
	 }
       }
       printf("Number of Voiced Frames = %d\n", numVoicedFrames);
       for (i = 0; i < nfby2; i++)
	 fprintf(specFile, "%d %f %f %f %f \n", i, 
		 ModGdSpecAvg[i]/(numVoicedFrames), 
		 MinGdSpecAvg[i]/(numVoicedFrames), 
		 LPGdSpecAvg[i]/(numVoicedFrames),
		 ModGdLPSpecAvg[i]/(numVoicedFrames));
       fclose(specFile);
       free(ModGdLPSpecAvg);
       free(ModGdSpecAvg);
       free(MinGdSpecAvg);
       free(LPGdSpecAvg);
       return(0);
       }
/**************************************************************************
 * $Log: GroupDelaySpectrum.c,v $
 * Revision 1.1  2011/03/24 16:56:00  hema
 * Initial revision
 *
 * Revision 1.1  2000/07/24 02:07:57  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




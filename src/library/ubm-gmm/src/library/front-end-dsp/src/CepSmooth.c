/**************************************************************************
 *  $Id$
 *  File:	CepSmooth.c - Computes the smooth spectrum using root cepstrum
 *
 *  Purpose:	Utility for comparing different spectral estimation techniques
 *
 *  Author:	Hema Murthy
 *
 *  Date:	Wed Jan  9 18:42:01 IST 2008
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average Cepstrally smoothed  Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the Cepstrally smoothed Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : CepSmooth ctrlFile waveFile SpecFile frameStart frameEnd\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

	float           *signal, *f, *residual;
	int  	        i,k, iloc;
        int             fftOrder,fftSize,frameSize, numFrames,
	                frameNum, frameShift, frameStart,
	                frameEnd;
        int             winLen;
        long            nSamp;
	char            ansTemp,fNameSpec[100],*ftemp=NULL;
        float           *amag, *phase, *ax, *ay;
        float           *cepSpec, *cepSpecAvg;
        float           c0;
	F_VECTOR        *waveform;
        FILE            *specFile, *controlFile;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       winLen   = (int) GetIAttribute(asdf, "gdSmthWinSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       printf ("frameSize = %d fftOrder = %d fftSize = %d winLen = %d frameShift = %d numFrames=%d\n", frameSize, fftOrder, fftSize, winLen, frameShift, numFrames);
       sscanf(argv[4], "%d", &frameStart);
       sscanf(argv[5], "%d", &frameEnd);
       fNameSpec[0] = '\0';
       ftemp = argv[3];
       Cstore(fftSize);
       strcpy(fNameSpec,ftemp);

       signal = (float *) AllocFloatArray(signal, frameSize+1);
       f  = (float *) AllocFloatArray(f, frameSize+1);
       residual  = (float *) AllocFloatArray(residual, frameSize+1);

       ax  = (float *) AllocFloatArray(ax, fftSize+1);
       ay  = (float *) AllocFloatArray(ay, fftSize+1);
       amag  = (float *) AllocFloatArray(amag, fftSize+1);
       phase  = (float *) AllocFloatArray(phase, fftSize+1);
       
       cepSpec  = (float *) AllocFloatArray(cepSpec, fftSize+1);
       cepSpecAvg  = (float *) AllocFloatArray(cepSpecAvg, fftSize+1);
       specFile = fopen(fNameSpec,"w");
       iloc = frameStart*frameShift;
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 printf (" frameNum = %d startLoc = %d\n", frameNum, iloc);
         waveform = (F_VECTOR *) GsfRead(asdf, frameNum, 
						"frameWaveform");
	 for (i = 1; i <= frameSize; i++)
	   signal[i] = waveform->array[i-1];
	 Rfft(signal, ax, ay, fftOrder, fftSize, -1);
         SpectrumReal(fftSize, ax, ay, amag, phase);
         CepSmooth(amag, cepSpec, fftOrder, fftSize, winLen, &c0, 1.0);
         for (i = 1; i <= fftSize; i++)
           cepSpecAvg[i] = cepSpecAvg[i] + log(cepSpec[i]);
	 iloc = iloc+frameShift;
       }
       for (i = 1; i <= fftSize; i++)
         fprintf(specFile, "%f\n",10*cepSpecAvg[i]/(frameEnd-frameStart));
       fclose(specFile);
}

/**************************************************************************
 * $Log$
 *
 *                        End of CepSmooth.c
 **************************************************************************/

/*
    This file computes LDA/WCCN matrix

    Copyright (C) 2011-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Srikanth Madikeri, Karthik Pandia <pandia@cse.iitm.ac.in>

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>. 
*/



#include "gsl_helper.h"

gsl_matrix* ldaTrain(gsl_matrix **dataMat, int totModel, int *modelsCount1, int R, int flag) {
	int i,j,s;
	float tempf;
	gsl_matrix **classMeanMat, *Sb, *Sw, *tempMat, *tempMat1, *tempMat2, *SwInv;
	gsl_matrix_complex *evectMat;
	gsl_vector **classMeanVect, *tempVect;
	gsl_vector_complex *evalVect ;
	gsl_permutation *perm;
	gsl_eigen_nonsymmv_workspace *w;

	tempMat = gsl_matrix_alloc(R,1);
	tempMat1 = gsl_matrix_alloc(R,R);
	tempMat2 = gsl_matrix_alloc(R,R);
	SwInv = gsl_matrix_alloc(R,R);
	tempVect = gsl_vector_alloc(R);
	perm = gsl_permutation_alloc(R);
	Sb = gsl_matrix_alloc(R,R);
	Sw = gsl_matrix_alloc(R,R);
	evalVect = gsl_vector_complex_alloc(R);
	evectMat = gsl_matrix_complex_alloc(R,R);
	w = gsl_eigen_nonsymmv_alloc(R);

	gsl_matrix_set_zero(tempMat);
	gsl_matrix_set_zero(tempMat1);
	gsl_matrix_set_zero(tempMat2);
	gsl_matrix_set_zero(SwInv);
	gsl_matrix_set_zero(Sb);
	gsl_matrix_set_zero(Sw);

	classMeanVect = (gsl_vector **)malloc(totModel*sizeof(gsl_vector *));
	classMeanMat = (gsl_matrix **)malloc(totModel*sizeof(gsl_matrix *)); 
	for(i=0;i<totModel;i++) {
		classMeanVect[i] = gsl_vector_alloc(R);
		classMeanMat[i] = gsl_matrix_alloc(R,modelsCount1[i]);
		gsl_vector_set_zero(classMeanVect[i]);
		gsl_matrix_set_zero(classMeanMat[i]);
	}

	for(i=0;i<totModel;i++) {
		for(j=0;j<modelsCount1[i];j++) {
			gsl_matrix_get_col(tempVect,dataMat[i],j);
			gsl_vector_add(classMeanVect[i],tempVect);
		}
	gsl_vector_scale(classMeanVect[i],1.0/modelsCount1[i]);
	}
 
	gsl_matrix_set_zero(tempMat1);
	for(i=0;i<totModel;i++) {
		gsl_vector_memcpy(tempVect,classMeanVect[i]);
		gsl_matrix_set_col(tempMat,0,tempVect);
		gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,tempMat,tempMat,0.0,tempMat1);
		gsl_matrix_add(Sb,tempMat1);
	}
	
	gsl_matrix_free(tempMat);
	for(i=0;i<totModel;i++) {
		tempMat=gsl_matrix_alloc(R,modelsCount1[i]);
		gsl_matrix_memcpy(tempMat,dataMat[i]);
		gsl_vector_memcpy(tempVect,classMeanVect[i]);
		for(j=0;j<modelsCount1[i];j++) {
			gsl_matrix_set_col(classMeanMat[i],j,tempVect);
		}
		gsl_matrix_sub(tempMat,classMeanMat[i]);
		gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,tempMat,tempMat,0.0,tempMat1);
		gsl_matrix_scale(tempMat1,1.0/modelsCount1[i]);
		gsl_matrix_add(Sw,tempMat1);
		gsl_matrix_free(tempMat);
	}

	gsl_matrix_memcpy(tempMat1,Sw);
	gsl_linalg_LU_decomp(tempMat1, perm, &s);
	gsl_linalg_LU_invert(tempMat1, perm, SwInv);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,SwInv,Sb,0.0,tempMat1);


	gsl_eigen_nonsymmv(tempMat1,evalVect,evectMat,w);
	gsl_eigen_nonsymmv_free(w);
	gsl_eigen_nonsymmv_sort(evalVect,evectMat,GSL_EIGEN_SORT_ABS_DESC);

	for(i=0;i<R;i++) {
	        gsl_vector_complex_view evec_i = gsl_matrix_complex_row (evectMat, i);
		for(j=0;j<R;j++) {
			gsl_complex z = gsl_vector_complex_get(&evec_i.vector,j);
			tempf=GSL_REAL(z);
			gsl_matrix_set(tempMat2,i,j,tempf);
		}
	}


	gsl_matrix_memcpy(tempMat1,SwInv);
	gsl_matrix_scale(tempMat1,totModel);
	gsl_linalg_cholesky_decomp(tempMat1);
	
	for(i=0;i<totModel;i++) {	
		gsl_vector_free(classMeanVect[i]);
		gsl_matrix_free(classMeanMat[i]);
	}

	gsl_matrix_free(Sw);
	gsl_matrix_free(Sb);
	gsl_vector_complex_free(evalVect);
	gsl_vector_free(tempVect);

	if(flag==0)
	return(tempMat2);
	else 
	return(tempMat1);
	gsl_matrix_complex_free(evectMat);
	gsl_matrix_free(tempMat1);
	gsl_matrix_free(tempMat2);
}


int main(int argc, char *argv[]) {
	printf("Arg Count = %d\n",argc);
	if(argc != 6) {
		printf("Usage : %s groundTurthFile R P(min_utterances_perclass) whitenMat ldawccnMat \n",argv[0]);
		exit(0);
	}

	FILE *ipfile;
	gt *gtptr;
	char *filename,*ldaMatName,*projMat,*wccnMatName, *whitenMatName;
	int i,j,k,n,R,P,totModel, *modelsCount,*modelIndex,*modelsCount1,*modelIndex1;
	float tempf;
	gsl_matrix **dataMat, **classMeanMat, *tempMat1, *ldaMat, **dataMatProj, *wccnMat, *ldaWccnMat;
	gsl_vector **classMeanVect, *tempVect;
	filename=argv[1];
	R=atoi(argv[2]);
	P=atoi(argv[3]);
	whitenMatName=argv[4];
	wccnMatName=argv[5];
	ldaMatName=argv[6];
	gtptr=read_gtfile(filename);
	gt_quickSort(gtptr,0,gtptr->num_lines-1);
	for(i=1,totModel=1;i<gtptr->num_lines;i++) {
		totModel = (gtptr->modelno[i]!=gtptr->modelno[i-1]) ? totModel+1:totModel;
	}
	modelIndex = (int *)calloc(totModel,sizeof(int));
	modelsCount = (int *)calloc(totModel,sizeof(int));

    for(i=1,j=0,modelsCount[0]=1;i<gtptr->num_lines;i++) {
		j = (gtptr->modelno[i]!=gtptr->modelno[i-1]) ? j+1:j;
		modelsCount[j]++;
	}
	for(i=1;i<totModel;i++) {
		modelIndex[i]=modelIndex[i-1]+modelsCount[i-1];
	}
	for(i=0,j=0;i<totModel;i++)
		if(modelsCount[i]>=P)
			j++;
	modelIndex1 = (int *)calloc(j,sizeof(int));
	modelsCount1 = (int *)calloc(j,sizeof(int));

	printf("total models = %d total unique models=%d\n",totModel,j);
	for(i=0,k=0;i<totModel;i++) {
		if(modelsCount[i] < P)
			continue;
		else {
			modelIndex1[k]=modelIndex[i];
			modelsCount1[k++]=modelsCount[i];
		} 
	}
	totModel=j;
	dataMat = (gsl_matrix **)malloc(totModel*sizeof(gsl_matrix *));
	dataMatProj = (gsl_matrix **)malloc(totModel*sizeof(gsl_matrix *));
	
	tempVect = gsl_vector_alloc(R);
	tempMat1 = gsl_matrix_alloc(R,R);
	ldaMat = gsl_matrix_alloc(R,R);
	wccnMat = gsl_matrix_alloc(R,R);
	ldaWccnMat = gsl_matrix_alloc(R,R);
	gsl_matrix_set_zero(tempMat1);
	gsl_matrix_set_zero(ldaMat);
	gsl_matrix_set_zero(wccnMat);
	gsl_matrix_set_zero(ldaWccnMat);
	gsl_vector_set_zero(tempVect);

	for(i=0;i<totModel;i++) {
		dataMat[i] = gsl_matrix_alloc(R,modelsCount1[i]);
		dataMatProj[i] = gsl_matrix_alloc(R,modelsCount1[i]);
		gsl_matrix_set_zero(dataMat[i]);
		gsl_matrix_set_zero(dataMatProj[i]);
	}
	
	for(i=0;i<totModel;i++) {
		gsl_vector_set_zero(tempVect);
	    for(j=0;j<modelsCount1[i];j++) {               
    	    	ipfile=fopen(gtptr->line[modelIndex1[i]+j],"r");   
        	if(ipfile == NULL)
                fprintf(stderr,"Read failed\n");      
	        for(k=0;k<R;k++) {
                	fscanf(ipfile,"%f ", &tempf);
	                gsl_matrix_set(dataMat[i],k,j,tempf);    
			gsl_vector_set(tempVect,k,tempf);
    	   	}
		fclose(ipfile);
    		}
	}

/*********************Whiten the vectors by projecting onto whiten matrix*****************/

	gsl_matrix *tempMat2;
	gsl_vector *tempVect1;
	tempMat1 = read_matrix(whitenMatName,R,R);
	for(i=0;i<totModel;i++) {
		tempMat2=gsl_matrix_alloc(R,modelsCount1[i]);
		tempVect1=gsl_vector_alloc(modelsCount1[i]);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,tempMat1,dataMat[i],0.0,tempMat2);
		for(j=0;j<modelsCount1[i];j++) {
			tempf=0;
			for(k=0;k<R;k++) {
				tempf=tempf+pow(gsl_matrix_get(dataMat[i],k,j),2);
			}
		gsl_vector_set(tempVect1,j,sqrt(tempf));
		}
		for(j=0;j<modelsCount1[i];j++) {
			for(k=0;k<R;k++) {
				tempf=gsl_matrix_get(tempMat2,k,j)/gsl_vector_get(tempVect1,j);
				gsl_matrix_set(dataMat[i],k,j,tempf);
			}
		}
		gsl_matrix_free(tempMat2);
		gsl_vector_free(tempVect1);
	}

/*************************Perform LDA on the projected vectors****************************/

	ldaMat=ldaTrain(dataMat, totModel, modelsCount1, R, 0);

	for(i=0;i<totModel;i++) {
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,ldaMat,dataMat[i],0.0,dataMatProj[i]);
	}

/*************************Perform class normalization on the LDA projected data***************/

	wccnMat=ldaTrain(dataMatProj, totModel, modelsCount1, R, 1);

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ldaMat,wccnMat,0.0,ldaWccnMat);	//new projection matrix is the product of lda matrix and wccn matrix on the lda projected data

	write_matrix(wccnMatName,ldaWccnMat);

	for(i=0;i<totModel;i++) {
		gsl_matrix_free(dataMat[i]);
		gsl_matrix_free(dataMatProj[i]);
	}

	free(modelIndex);
	free(modelsCount);
	free(modelIndex1);
	free(modelsCount1);
	free(dataMat);
	free(dataMatProj);
	gsl_vector_free(tempVect);
	gsl_matrix_free(tempMat1);
	gsl_matrix_free(ldaMat);
	gsl_matrix_free(wccnMat);
	gsl_matrix_free(ldaWccnMat);
}      

/*
    This file does the topC testing in the UBM-GMM framework

    Copyright (C) 2002-2016 Speech and Music Technology Lab,
    Indian Institute of Technology Madras
    
    Contributed by Hema A Murthy <hema@cse.iitm.ac.in>, 
    Srikanth Madikeri

    This file is part of SpeakerID-IITM.

    SpeakerID-IITM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SpeakerID-IITM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SpeakerID-IITM.  If not, see <http://www.gnu.org/licenses/>.

    Modified by Mari ganesh Kumar<mariganeshkumar@gmail.com> on 6/08/2018
    Modification Include:
        * Added UBM likelihood normalization when topc from file is not used
        * Changed the result file format
*/

#include "CommonFunctions.h"

float  *ComputeTopCLikelihood (VFV *vfv, unsigned int numVectors, 
			   int numSpeakers, 
			   VFV **speakerMeans, 
			   VFV **speakerVars, 
			   VFV *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j, k, mixNumber;
  float                   mixProbValue;
  float                   evidence = 0.0;
  for (i = 0; i < numSpeakers; i++) {
      Distortion[i] = 0.;
      for (j = 0; j < numVectors; j++) {
          for (k = 0; k < cValue; k++) {
              mixNumber = mixIds[j][k]-1;
              if (!k)
                  mixProbValue = ComputeProbability(
                                      speakerMeans[i][mixNumber], 
                                      speakerVars[i][mixNumber], 
                                      speakerWts[i]->array[mixNumber],
                                      vfv[j], 
                                      probScaleFactor);
              else
                  mixProbValue = LogAdd (mixProbValue, 
                                         ComputeProbability(
                                             speakerMeans[i][mixNumber], 
                                             speakerVars[i][mixNumber], 
                                             speakerWts[i]->array[mixNumber],
                                             vfv[j], 
                                             probScaleFactor));
          }
          Distortion[i] = Distortion[i] + mixProbValue;
      }
      Distortion[i] /= numVectors;
  }
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue;
};

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
{
    int           i,j, tempMixId;
    float         tempDist;
    for (i=cValue-1; i>0; i--) {
        if (bestCMixtures[i].DistortionValue > 
              bestCMixtures[i-1].DistortionValue) {
            tempMixId = bestCMixtures[i].id;
                  tempDist  = bestCMixtures[i].DistortionValue;
            bestCMixtures[i].id = bestCMixtures[i-1].id;
            bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
            bestCMixtures[i-1].id = tempMixId;
            bestCMixtures[i-1].DistortionValue = tempDist;
        }
    }
}

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 

int **GetTopCMixtures (VFV *ubmModelMeans, VFV *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, float probScaleFactor, float *logLikelihood)
  {
    int  **mixtureIds, i, j, k;
    float *Dist, mixProbValue;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];
    *logLikelihood = 0;
    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++) {
    	  for (i = 0; i < numClusters; i++) {
            Dist[i] = ComputeProbability (
                        ubmModelMeans[i], 
                        ubmModelVars[i], 
                        ubmWts[i], 
                        vfv[j], 
                        probScaleFactor);
        }
        for (k = 0; k < cValue; k++) {
            bestCMixtures[k].id = k+1;
            bestCMixtures[k].DistortionValue = Dist[k];
            if (k)
                ReArrangeMixtureIds(bestCMixtures, k+1);
        }

        for (k = cValue; k < numClusters; k++) {
            if (Dist[k] > bestCMixtures[cValue-1].DistortionValue) {
                bestCMixtures[cValue-1].id = k +1;
                bestCMixtures[cValue-1].DistortionValue = Dist[k];
                ReArrangeMixtureIds(bestCMixtures, cValue);
            }
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++) {
            mixtureIds[j][k] = bestCMixtures[k].id;
            if (!k)
                mixProbValue = bestCMixtures[k].DistortionValue;
            else
                mixProbValue = LogAdd(mixProbValue,
                                      bestCMixtures[k].DistortionValue);
        }
        *logLikelihood = *logLikelihood + mixProbValue;

    }

    return mixtureIds;  
}

void Usage() 
{
   printf(" Usage : TestGMM [options] modelFile numSpkrs testList ubm ");
   printf("cval\n");
   printf("Options include:\n");
   printf("--help/-h: print this menu\n");
   printf("--featureIsBinary/-b: feature files are in binary format\n");
   printf("--resultFolder: folder in which results are to be stored\n");
   printf("--start: the index number of the test case to start from\n");
   printf("--end: the index number of the test case to end at\n");
   printf("--topc topc_file_list: use pre-computed topc values\n");
   return;
}



int 
main(int argc, char *argv[])
{

  FILE                  *cFile=NULL,*modelFile=NULL, *outFile = NULL,
                        *speakerFile=NULL, *ubmFile=NULL, *testListFile=NULL,
                        *topc_list_file = NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *ubmFileName = NULL, *topc_value_filename,
                        *topc_list_filename;
  char			            *testListFileName = NULL, *outFileName = NULL;
  char                  **argv_mand;
  char                  *string1=NULL, *string2=NULL, *resultFolderName = ".";
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters, testCaseNo;
  VFV   **speakerModelMeans, **speakerModelVars, 
                        *speakerModelWts;
  VFV   *ubmModelMeans, *ubmModelVars;
  VFV   *vfv,*newVfv;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue, endAt = 0, argc_mand_len,c;
  // flags
  int                   use_topc_values = 0, print_help = 0, featIsBin = 0;
  unsigned int          numVectors;
  int                   startFrom = 1;
  float                 *Distortion, *ubmWts,mean,sd, ubmDistortion;
  float                 thresholdScale = 0.0;
  int lastSelectedVectorIdx;
  unsigned long  numSpeechVectors;
  struct option longopts[] = {
      { "topc", required_argument, NULL , 't' },
      { "start", required_argument, NULL, 's' },
      { "end", required_argument, NULL, 'e'},
      { "resultFolder", required_argument, NULL, 'r'},
      { "featureIsBinary", no_argument, &featIsBin, 1},
      { "help", no_argument, &print_help, 1},
      { 0, 0, 0, 0}
  };

  while((c = getopt_long(argc,argv,":bh", longopts, NULL)) != -1) {
      switch(c) {
          case 't':
              topc_list_filename = optarg;
              use_topc_values = 1;
              break;
          case 's':
              sscanf(optarg,"%d", &startFrom);
              break;
          case 'e':
              sscanf(optarg,"%d", &endAt);
              break;
          case 'r':
              resultFolderName = optarg;
              break;          
          case 'b':
              featIsBin = 1;
              break;
          case 'h':
              print_help = 1;
              break;
          default:
              printf("Option %c not recognized\n", c);
              break;
      }
  }
 
  argv_mand = &argv[optind-1];
  argc_mand_len = argc - optind;
   
  if (argc_mand_len < 1 || print_help) {
    Usage();
    exit(-1);
  }

  models = argv_mand[1];
  modelFile = fopen(models,"r");
  sscanf(argv_mand[2],"%d",&numSpeakers);
  testListFileName = argv_mand[3];
  ubmFileName = argv_mand[4];
  sscanf (argv_mand[5], "%d", &cValue);

  testListFile = fopen (testListFileName,"r");
  if (testListFile == NULL) {
      printf ("Could not open testList\n");
      exit(1);
  }
  if (use_topc_values) 
      topc_list_file = fopen(topc_list_filename,"r");  
  if (use_topc_values && topc_list_file == NULL) {
      fprintf(stderr,"Unable to open topc file");
      return 1;
  }

   wavname = (char *) calloc (MAX_FILE_NAME_LENGTH, sizeof(char));
   printf("Preparing to read %s\n", wavname);
   fscanf (testListFile, "%s\n", wavname);
   if (featIsBin) {
       printf ("MESSAGE: featIsBin is set\n");
       newVfv = ReadVfvFromBinFile (wavname, &numVectors);
   }
   else {
       newVfv = ReadVfvFromFile (wavname,&numVectors);       
   }
   featLength = newVfv[0]->numElements;
   rewind(testListFile);
   FreeVfv (newVfv, numVectors);

  speakerModelMeans = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelVars = (VFV **) calloc(numSpeakers,sizeof(VFV *));
  speakerModelWts = (VFV *) calloc(numSpeakers,sizeof(VFV));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VFV *) calloc(numClusters[i], 
    sizeof(VFV));
    speakerModelVars[i] = (VFV *) calloc(numClusters[i], 
    sizeof(VFV));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  printf ("Read all Models\n");
  ubmFile = fopen (ubmFileName, "r");
  if (ubmFile == NULL) {
      printf ("Unable to open ubm file\n");
      exit(1);
  }
  ubmModelMeans = (VFV *) calloc (numClusters[0], sizeof (VFV));
  ubmModelVars  = (VFV *) calloc (numClusters[0], sizeof (VFV));
  ubmWts        = (float *) calloc (numClusters[0], sizeof (float));

  for (i = 0; i < numClusters[0]; i++)
    {
      fscanf (ubmFile, "%f\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
   	{
	  fscanf (ubmFile," %f %f",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);
  printf ("Read ubm model\n");
  testCaseNo = 0;
  while (!feof (testListFile))
  {
    mean=0;
    sd=0;
    ubmDistortion = 0;
    testCaseNo++;

    fscanf (testListFile,"%s\n", wavname);

    if (testCaseNo < startFrom)
      continue;
    if (endAt != 0 && testCaseNo > endAt)
      break;

    if (featIsBin)
      newVfv = ReadVfvFromBinFile (wavname, &numVectors);
    else
      newVfv = ReadVfvFromFile (wavname, &numVectors);

    if (use_topc_values) {
      topc_value_filename = (char*) malloc(sizeof(char)
                                          *MAX_FILE_NAME_LENGTH);
      if (topc_value_filename == NULL) {
          fprintf(stderr,"Mem allocation failed\n");
          return 1;
      }
      fscanf(topc_list_file,"%s\n",topc_value_filename);
      mixIds = ReadTopCValuesFromFile(topc_value_filename,cValue,
                                      numVectors);
      free(topc_value_filename);
    }
    else {

      mixIds = GetTopCMixtures(ubmModelMeans, ubmModelVars, ubmWts,
                               numClusters[0], newVfv, numVectors,
                               cValue, 1.0, &ubmDistortion);
    }
    Distortion = (float *) calloc(numSpeakers, sizeof(float));
    Distortion = (float *) ComputeTopCLikelihood(newVfv, numVectors,
             numSpeakers, speakerModelMeans,
             speakerModelVars, speakerModelWts,
             numClusters, Distortion,
             1.0, cValue, mixIds);
    if(! use_topc_values )
     for(int speaker =0; speaker<numSpeakers; speaker++)
          Distortion[speaker]=Distortion[speaker]-ubmDistortion;
    finc(i,1,numVectors)
      free(mixIds[i]);
    free (mixIds);

    outFileName = (char *) calloc (MAX_FILE_NAME_LENGTH, sizeof(char));
    sprintf (outFileName, "%s/%d.out",  resultFolderName, testCaseNo);
    outFile = fopen(outFileName, "w");
    if (outFile == NULL) {
      printf ("unable to create file %s ... \n", outFileName);
      continue;
    }
    for(i=0;i<numSpeakers;i++)
        mean+=Distortion[i];
    mean/=numSpeakers;
    for(i=0;i<numSpeakers;i++)
        sd+=pow((Distortion[i]-mean),2);
    sd=sqrt(sd/numSpeakers);
    for(i=0;i<numSpeakers;i++)
        Distortion[i]=(Distortion[i]-mean)/sd;
    for (i = 0; i < numSpeakers; i++)
        fprintf(outFile, "Score_%d %f\n", i, Distortion[i]);
    //fprintf(outFile, "label %5d\n",
    //              Imax0Actual(Distortion,numSpeakers)+1);
    //fprintf(outFile, "Likelihood %f\n",
    //       Distortion[Imax0Actual(Distortion,numSpeakers)]);
    printf ("Processed file No:%d %s\n", testCaseNo, wavname);
    free(newVfv);
    free(Distortion);
    free (outFileName);
    fclose (outFile);
  }
  for (i = 0; i < numSpeakers; i++) {
      free (speakerModelMeans[i]);
      free (speakerModelVars[i]);
      free (speakerModelWts[i]);
  }
  if (use_topc_values && topc_list_file != NULL) 
      fclose(topc_list_file);
  return(0);
}


/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

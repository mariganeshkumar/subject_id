/*
 * =====================================================================================
 *
 *       Filename:  UtilityFunctions.c
 *
 *    Description:  Some specific functions to be applied on feature 
 *                  vector streams
 *
 *
 * =====================================================================================
 */

#include "UtilityFunctions.h"

VFV*
computeDCTOnStream (VFV *ip, uint nvec, uint nfilts, uint nceps) {
  int i, dim;
  VFV *cosTx, *op;
  dim = ip[0]->numElements;
  op = (VFV *) calloc (nvec, sizeof (VFV));
  cosTx = (VFV *) 
      GeneratePseudoDct(2, nceps, nfilts);
  finc(i,nvec,1) {
      op[i] = (F_VECTOR*) AllocFVector (nceps);
      LinearTransformationOfFVector (ip[i], cosTx, 
                                             nceps, nfilts, 
                                             op[i]);
  }
  FreeVfv (cosTx, nceps);
  return op;
}

#include "stdio.h"
#include "sp/sphere.h"
#include "fe/SphereInterface.h"
/*short *read_sphere_waveform(SP_FILE *wave_file,long *num_samples)
{
SP_INTEGER samples = 0;
int mode;
long num_samp = 0;
short *waveform;
int success;
success = sp_h_get_field(wave_file,"sample_count",T_INTEGER, &samples);
printf("samples = %ld\n",samples); 
if ((waveform = (short *) sp_data_alloc(wave_file,-1)) == NULL) {
   printf("unable to allocate space for waveform\n");
   exit(-1);
}
*num_samples = samples;
sp_rewind(wave_file);
num_samp = sp_read_data(waveform, *num_samples, wave_file);
printf("num_samp = %d\n",num_samp);
fflush(stdout);
if (num_samp != samples) {  
  printf("num_samp = %d error reading waveform \n", num_samp);
  exit(-1);
}
return(waveform);
}*/


main (int argc, char *argv[])
{
short *waveform;
char *name;
SP_FILE *wave_file;
FILE *fpout;
int i;
float value;
long num_samples;
name = argv[1];
fpout = fopen(argv[2],"w");

wave_file = sp_open (name, "r");
waveform = ReadSpherePcm(argv[1], &num_samples);
printf("num_samples in main = %d\n",num_samples);
for (i = 0; i < num_samples; i++){
  value = waveform[i];
     if (i % 10000 == 0)
       printf("output waveform[%d]= %d\n",i,waveform[i]);
  fflush(stdout);
  fprintf(fpout,"%d\n ",(waveform[i]));
}
fclose(fpout);
printf("file closed\n");
fflush(stdout);
sp_close(wave_file);
}














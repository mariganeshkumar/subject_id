% computes lda when supervectors are large. assume block diagonal format

function A = blocklda (flist, idlist, ubmname, M, D, R)
  idvec = load (idlist); 
  ids = unique (idvec);
  S = length (ids);
  spkrlist = readlistascell (flist);
  % scatter matrices are now cells
  for m = 1:M
      S_b {m} = zeros (D);
      S_w {m} = zeros (D);
  end

  MD = M * D;
  
  [ubm_wts ubm_means ubm_vars] = ReadGMM (ubmname, M, D);

  clear ubm_wts ubm_vars;
  ubm_means = reshape (ubm_means,MD,1); 

  % for each speaker
  for s = 1:S
      % collect the index values
      j = ids (s);
      idx = find (idvec == j);
      ns = length (idx);

      if ns == 1
        continue
      end

      % construct data matrix
      k = 1;
      dmat = zeros (MD,ns);
      for k = 1:ns        
        i = idx (k);
        [spkr_wts spkr_means spkr_vars] = ReadGMM (spkrlist{i},M,D);
        clear spkr_wts spkr_vars;
        dmat(:,k) = reshape (spkr_means, MD, 1) - ubm_means;
      end      

      mvec = (mean (dmat'))';

      % for each supervector, find cov accumulators
      for m = 1:M
        C_m{m} = zeros(D);
      end

      for k = 1:ns
          svec = dmat(:,k) - mvec;      
          C = blockcov (svec, M, D);      

          % accumulate
          for m = 1:M
            C_m{m} = C_m{m} + C{m};
          end
      end    
      % normalize and add things up
      for m = 1:M
          S_w{m} = S_w{m} + (C_m{m} / ns);
      end
      clear C_m
      C = blockcov(mvec,M,D);
      for m = 1:M
          S_b{m} = S_b{m} + C{m};
      end
  end

  % do eigen analysis block-wise
  A = zeros (M*D, R);
  for m = 1:M
      s = (m-1)*D+1:m*D;
      S_w{m} = S_{m} / S;
      [V Dmat] = eig (inv (S_w{m}) * S_b{m});
      Dmat = diag (Dmat);
      [Ds Didx] = sort (Dmat,'descend');
      for i = 1:R
          idx = Didx (i);
          A(s,i) = V(:,idx);
      end    
  end
  
  % do variance normalization on projection matrix
  %[ubm_wts ubm_means ubm_vars] = ReadGMM (ubmname, M, D);
  %clear ubm_wts ubm_means;
  %ubm_vars = reshape (ubm_vars, MD, 1);

  % do variance normalization on projection matrix
  %for r = 1:R
  %  A (:,r) = A(:,r) ./ ubm_vars;
  %end

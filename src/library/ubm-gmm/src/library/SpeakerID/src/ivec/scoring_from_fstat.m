function scoring_from_fstat (testname, trainlist, ldamatrix, opname, exflag)
  [N_test f_test] = ReadFoStat (testname);
  tl = readlistascell (trainlist);
  ll = length (tl);
  A = load (ldamatrix);
  s = zeros (ll,1);
  [L w_test T] = ivecest (A,N_test,f_test);
  for i = 1:ll
      [N_tgt f_tgt] = ReadFoStat (tl {i});      
      [L w_tgt T] = ivecest (A, N_tgt, f_tgt);
      s (i) = coskernel (A', w_test, w_tgt);
  end
%  mu = mean (s(1:125));
%  sigma = std (s(1:125));
%  s = s - repmat (mu,ll,1);
%  s = s ./ repmat (sigma,ll,1);  
  save (opname, 's', '-ascii')
  id = find (min (s) == s);  
  sprintf ('R35ul7 %s %d', testname, id)
  if exflag ~= 0
    exit
  end

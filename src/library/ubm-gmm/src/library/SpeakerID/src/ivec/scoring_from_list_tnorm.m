function res = scoring_from_list_tnorm (testname, trainlist, ldamatrix, opname, t_is_list,exflag, tnorm_list)
  if t_is_list == 1
    testlist = readlistascell (testname);
    sl = length (testlist);
    oplist = readlistascell (opname);
  else
    testlist{1} = testname;
    sl = 1;
    oplist{1} = opname;
  end

  tl = readlistascell (trainlist);
  ll = length (tl);
  tnorm_files= readlistascell (tnorm_list);
  tnorm_ll = length (tnorm_files);
  A = load (ldamatrix);  
  [R psize] = size (A);
  A = A';
  wmat = zeros (R,ll);
  for i = 1:ll
      wmat(:,i) = load (tl{i});
  end
  wmat = (A * wmat)';
  tnmat = zeros(R,tnorm_ll);
  for i = 1:tnorm_ll
      tnmat(:,i) = load (tnorm_files{i});
  end
  tnmat = (A * tnmat)';

  n1 = sqrt (sum ((wmat .* wmat),2));  
  nt = sqrt (sum ((tnmat .* tnmat),2));
  size(wmat)
  size(tnmat)
  %s = zeros (ll,1);
  res = zeros(ll,1);
  for tno = 1:sl
      w_test = load (testlist{tno});
      w_test = A * w_test;
      n2 = sqrt (w_test' * w_test);
      w_test = w_test / n2;
      s = (wmat * w_test) ./ n1;
      stn = (tnmat * w_test) ./ nt;
      mu = mean (stn);
      sigma = std (stn);
      s = (s - mu) / sigma;
      save (oplist{tno}, 's', '-ascii')
      [mval id] = max (s);
%      sprintf ('R35ul7 %s %d', testlist{tno}, id)
      res (tno) = id;
  end
  if exflag ~= 0
    exit
  end

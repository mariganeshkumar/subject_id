/****************
 * This src file is to Test an utterance for adapted models - version. It does top C mixture testing.
 * The user needs to provide normal testutterancegmm params and also append to it
 * ubm file path and value for C. The main function reads all spkr models, reads the
 * ubm model and computes the top C mixtures with ubm's help. With these topC, the
 * the ComputeLikelihood function in the original code has been changed to ComputeTopCLikelihood
 * to take C and corresponding mixture Ids and compute likelihood accordingly
 * This was earlier tested on mean-only adapted models and it worked (for 32 mixtures)! 

The implementational doubts in the 2nd version will be cleared in this version by compute top C
values for every feature vector in the test utterance
*************************/
/************************************************************************
  Function            : TestUtteranceGMM- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numGMM numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, InitAsdf.c,BatchProcessWaveform.c
                         SphereInterface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-02
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "CommonFunctions.h"

float  *ComputeTopCLikelihood (VECTOR_OF_F_VECTORS *vfv, unsigned long  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int **mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[j][k]-1;
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	       if (mixProbValue > LOG_ZERO )	 
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    printf("Distortion %d = %f\n",i,Distortion[i]);
    fflush(stdout); 
    Distortion[i] = Distortion[i]/numVectors;
  }
  evidence = Distortion[0];
  for (i = 1; i < numSpeakers; i++)
    evidence = LogAdd(evidence, Distortion[i]);
  for (i = 0; i < numSpeakers; i++)
    Distortion[i] = Distortion[i] - evidence;
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue
};

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
	  {
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
	  }
    	for (k = 0; k < cValue; k++)
        {
	bestCMixtures[k].id = k+1;
	bestCMixtures[k].DistortionValue = Dist[k];
	
	if (k)
		ReArrangeMixtureIds(bestCMixtures, k+1);
        }

     	for (k = cValue; k < numClusters; k++)
        {
		if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
		{
		bestCMixtures[cValue-1].id = k +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[k];
		ReArrangeMixtureIds(bestCMixtures, cValue);
		}
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }


    return mixtureIds;  
  }

void Usage() {
   printf (" Usage : TestUtteranceGMM ctrlFile modelFile featureName numSpeakers wavName threshold test/verify(0/1) ubm topC\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL, *ubmFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, *ubmFileName = NULL,
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  *speakerModel,line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k, **mixIds;
  int                   featLength, cValue;
  unsigned long         numVectors;
  int                   verify;
  float                 *Distortion, *ubmWts;
  float                 thresholdScale;
  if (argc != 10) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  featureName = argv[3];
  string2 = argv[4];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[5];
  string1 = argv[6];
  sscanf(string1, "%f", &thresholdScale);
  string1 = argv[7];
  sscanf(string1, "%d", &verify);
  ubmFileName = argv[8];
  sscanf (argv[9], "%d", &cValue);

  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  vfv = (VECTOR_OF_F_VECTORS *) ExtractFeatureVectors
          (asdf, wavname, featureName, &numVectors, thresholdScale);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  i = 0;
  while (fgets(line,500,modelFile)) {
    speakerModel = (char *) calloc (500,sizeof(char));
    sscanf(line,"%s %d\n",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%e",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %e %e",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++; 
    free(speakerModel);
    fclose(speakerFile);
  }

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters[0], sizeof (float));

  for (i = 0; i < numClusters[0]; i++)
    {
      fscanf (ubmFile, "%e\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
        {
	  fscanf (ubmFile," %e %e",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);

  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters[0], vfv, numVectors, cValue, asdf->probScaleFactor); 
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeTopCLikelihood(vfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   asdf->probScaleFactor, cValue, mixIds);
  printf("%s\n",wavname);
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

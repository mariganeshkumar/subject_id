#!/usr/bin/python
# at this stage, it is assumed that the C
# matrix has already been estimated. but we
# are still in the E step. for each c in [0..M]
# we need to estimate A_c

import numpy as np
import scipy as sp
from svecdef import *
import getopt
from tables import *
from Utility import *

# arguments description
# C: (flat) file to store C matrix
# F: list of all first order stats
# w: list of all ivec estimates

optlist, args = getopt.getopt (sys.argv[1:], "N:F:h")

def printhelp (close = 0):
  print "Usage: ./hype.py -N Nmatrix.file -F firstorderlist"
  if close == 1:
    quit ()

nmatfname, folist = "", ""

for arg in optlist:
    option = arg[0]
    value = arg[1]
    if option == "-F":
      folist = value
    elif option == "-N":
      nmatfname = value
    elif option == "-h":
      printhelp (1)    
    else:
      print "Invalid argument..."
      printhelp (1)

# check if args are empty
if folist == "" or nmatfname == "" :   
    printhelp (1)

tempf = open (folist, "r")
listlen = len (tempf.readlines ())
tempf.close ()
tempf = open (folist, "r")
  
for i,foname in enumerate (tempf):
  # using GetNFromFO gives a significant speed-up
  M,nx = GetNFromFO (foname.strip ())
  if i == 0:
    N = np.zeros ((M,listlen))
  N[:,i] = nx.reshape ((M,))

np.save (nmatfname, N)

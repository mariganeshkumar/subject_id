#include "gsl_helper.h"
#include<string.h>
#define CHAR_ALLOC(k) (char*)malloc(sizeof(char)*k)

int main(int argc, char *argv[]) {
        if(argc!=9) {
                printf("Usage : %s enrollModelList_MFCC enrollModelList_MGD wccnMat_MFCC whitenMat_MFCC wccnMat_MGD whitenMat_MGD  R resultFile\n",argv[0]);
                exit(0);
        }
	FILE *enrollModelListFile1, *enrollModelListFile2, *resultFile;
	char *enrollModelListName1, *enrollModelListName2, *resultFileName, 
		**enrollModelList1, **enrollModelList2, *wccnMat1Name, 
		*wMat1Name, *wccnMat2Name, *wMat2Name, line[256];
	int i,j,R,nModels=0;
	double tempf,tempf1,tempf2;
	gsl_matrix *wccnMat1, *wccnMat2, *wMat1, *wMat2, *tempMat, *enrollProjMat1, 
			*resultMat1, *enrollProjMat2, *resultMat2;
	gsl_vector *tempVect, *tempVect1;

	enrollModelListName1=argv[1];
	enrollModelListName2=argv[2];
	wccnMat1Name=argv[3];
	wMat1Name=argv[4];
	wccnMat2Name=argv[5];
	wMat2Name=argv[6];
	R=atoi(argv[7]);
	resultFileName=argv[8];

	if((enrollModelListFile1 = fopen(enrollModelListName1,"r")) == NULL) {
                fprintf(stderr,"enroll list file %s cannot be opened\n",enrollModelListName1);
                exit(1);
                }
	if((enrollModelListFile2 = fopen(enrollModelListName2,"r")) == NULL) {
                fprintf(stderr,"enroll list file %s cannot be opened\n",enrollModelListName2);
                exit(1);
                }

        resultFile=fopen(resultFileName,"w+");
        while(fgets(line,sizeof(line),enrollModelListFile1)!=NULL) nModels++;
	rewind(enrollModelListFile1);
        enrollModelList1=(char **)malloc(sizeof(char *)*nModels);
        enrollModelList2=(char **)malloc(sizeof(char *)*nModels);

        i = 0;
        while (fgets(line,1000,enrollModelListFile1)) {
                enrollModelList1[i]= CHAR_ALLOC(256);
                sscanf(line,"%s\n",enrollModelList1[i]); 
                i++;
        }
        if(i!=nModels) {
                fprintf(stderr,"One or more models from MFCC list are faulty\n");
                exit(1);
        }

        i = 0;
        while (fgets(line,1000,enrollModelListFile2)) {
                enrollModelList2[i]= CHAR_ALLOC(256);
                sscanf(line,"%s\n",enrollModelList2[i]); 
                i++;
        }
        if(i!=nModels) {
                fprintf(stderr,"One or more models from MGD list are faulty\n");
                exit(1);
        }

        wccnMat1 = gsl_matrix_alloc(R,R);
        wccnMat2 = gsl_matrix_alloc(R,R);
        wMat1 = gsl_matrix_alloc(R,R);
        wMat2 = gsl_matrix_alloc(R,R);
	enrollProjMat1 = gsl_matrix_alloc(R,nModels);
	enrollProjMat2 = gsl_matrix_alloc(R,nModels);
	resultMat1 = gsl_matrix_alloc(nModels,nModels);
	resultMat2 = gsl_matrix_alloc(nModels,nModels);
        tempMat = gsl_matrix_alloc(R,nModels);
        tempVect = gsl_vector_alloc(R);
        tempVect1 = gsl_vector_alloc(R);
        gsl_matrix_set_zero(enrollProjMat1);
        gsl_matrix_set_zero(enrollProjMat2);
        gsl_matrix_set_zero(resultMat1);
        gsl_matrix_set_zero(resultMat2);
        gsl_matrix_set_zero(tempMat);

        wMat1 = read_matrix(wMat1Name,R,R);
	wccnMat1 = read_matrix(wccnMat1Name,R,R);
        wMat2 = read_matrix(wMat2Name,R,R);
	wccnMat2 = read_matrix(wccnMat2Name,R,R);

        for(i=0;i<nModels;i++) {
                tempVect1=read_vector(enrollModelList1[i],R);
                gsl_blas_dgemv(CblasTrans, 1.0, wMat1, tempVect1, 0.0, tempVect);
                tempf=gsl_blas_dnrm2(tempVect);
                gsl_vector_scale(tempVect, 1.0/tempf);
                gsl_matrix_set_col(tempMat,i,tempVect);
        }
        gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat1,tempMat,0.0,enrollProjMat1);
        for(i=0;i<nModels;i++) {
                gsl_matrix_get_col(tempVect,enrollProjMat1,i);
                tempf=gsl_blas_dnrm2(tempVect);
                gsl_vector_scale(tempVect, 1.0/tempf);
                gsl_matrix_set_col(enrollProjMat1,i,tempVect);
        }

        for(i=0;i<nModels;i++) {
                tempVect1=read_vector(enrollModelList2[i],R);
                gsl_blas_dgemv(CblasTrans, 1.0, wMat2, tempVect1, 0.0, tempVect);
                tempf=gsl_blas_dnrm2(tempVect);
                gsl_vector_scale(tempVect, 1.0/tempf);
                gsl_matrix_set_col(tempMat,i,tempVect);
        }
        gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,wccnMat1,tempMat,0.0,enrollProjMat2);
        for(i=0;i<nModels;i++) {
                gsl_matrix_get_col(tempVect,enrollProjMat2,i);
                tempf=gsl_blas_dnrm2(tempVect);
                gsl_vector_scale(tempVect, 1.0/tempf);
                gsl_matrix_set_col(enrollProjMat2,i,tempVect);
        }

        for(i=0;i<nModels;i++) {
                gsl_matrix_get_col(tempVect,enrollProjMat1,i);
                for(j=0;j<nModels;j++) {              
                        gsl_matrix_get_col(tempVect1,enrollProjMat1,j);
                        gsl_blas_ddot(tempVect,tempVect1,&tempf);
                        gsl_matrix_set(resultMat1,i,j,tempf);
                }
        }
        for(i=0;i<nModels;i++) {
                gsl_matrix_get_col(tempVect,enrollProjMat2,i);
                for(j=0;j<nModels;j++) {              
                        gsl_matrix_get_col(tempVect1,enrollProjMat2,j);
                        gsl_blas_ddot(tempVect,tempVect1,&tempf);
                        gsl_matrix_set(resultMat2,i,j,tempf);
                }
        }

        tempVect=gsl_vector_alloc(nModels);
        tempVect1=gsl_vector_alloc(nModels);
	for(i=0;i<nModels;i++) {
		gsl_matrix_get_row(tempVect,resultMat1,i);
		gsl_matrix_get_row(tempVect1,resultMat2,i);
		tempf1=gsl_blas_dasum (tempVect);
		tempf2=gsl_blas_dasum (tempVect1);

		if(tempf1 >tempf2)
			fprintf(resultFile,"%s %d\n",enrollModelList1[i],0);
		else
			fprintf(resultFile,"%s %d\n",enrollModelList1[i],1);
	}
	
	fclose(resultFile);
        gsl_matrix_free(wccnMat1);
        gsl_matrix_free(wccnMat2);
        gsl_matrix_free(wMat1);
        gsl_matrix_free(wMat2);
        gsl_matrix_free(tempMat);
        gsl_matrix_free(enrollProjMat1);
        gsl_matrix_free(enrollProjMat2);
        gsl_matrix_free(resultMat1);
        gsl_matrix_free(resultMat2);
        gsl_vector_free(tempVect);
        gsl_vector_free(tempVect1);

}

/*----------------------------------------------------------------------
* FileName : Riff.h
* Purpose: Structure of RIFF File 
* Author: P.G.Deivapalan <deivapalan@lantan.TeNeT.res.in>
* Bugs: Currently supports only WAVE file format
* Created on: 7th July 2005.  
-----------------------------------------------------------------------*/

#ifndef RIFF_H
#define RIFF_H

#define SUCCESS 1
#define FAILURE 0

typedef unsigned char UCHAR;
typedef short int INT;
typedef long DWORD;

typedef struct RIFFHeader
{
	UCHAR riffChunkID[4];		//0x00	 	
	DWORD riffChunkDataSize;	//0x04
	UCHAR riffType[4];		//0x08
	
	UCHAR fmtChunkID[4];		//0x0C
	DWORD fmtChunkDataSize;		//0x10
	INT compressionCode;		//0x14
	INT numberOfChannel;		//0x16
	DWORD sampleRate;		//0x18
	DWORD averageBytesPerSecond;	//0x1C
	INT blockAlign;			//0x20
	INT sigBytesPerSample;		//0x22
	
	//@ This Element is not present for Uncompressed/PCM format
	//INT numberOfExtraBytes  __attribute__ ((packed)); 	//0x24
	
	UCHAR dataChunkID[4]; 		//0x24
	DWORD dataChunkDataSize;	//0x28
		
}RIFFHeader __attribute__ ((packed));	
	
typedef struct RIFFWaveFile
{
	RIFFHeader *header;				//0x00 to 0x2B
	short int *data __attribute__ ((packed));	//0x2C to dataSize
	
}RIFFWaveFile;


//Function Prototypes
int ReadRIFFWaveFile( FILE*, RIFFWaveFile* );
void PrintRIFFWaveFileInfo( FILE*, RIFFWaveFile* );
int WriteRIFFWaveFile( RIFFWaveFile*, int size, FILE* );
int WriteWaveform(short int *, long int, FILE*);

#endif

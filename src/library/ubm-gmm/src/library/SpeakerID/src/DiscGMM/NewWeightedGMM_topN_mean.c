/********************************************************************
 * This function is a variant of NewWeightedGMM.c - it will take
 * only closest negative data assigned to that cluster. That is,
 * the algo will look for, say, 10 -25 % quantile for that cluster
 * wrt the negative data.
*********************************************************************/





#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "InitAsdf.h"
#include "DspLibrary.h"
#include "stdlib.h"
#include "math.h"
#include "GMM.h"
#include "NewWeightedGMM.h"
#include "VQ_Modified.h"



void
NewWeightedGMMTopN_MeanOnly (VECTOR_OF_F_VECTORS * vfv,
		int numVectors,
		VECTOR_OF_F_VECTORS * mixtureMeans,
		VECTOR_OF_F_VECTORS * mixtureVars,
		float *mixtureElemCnt,
		int numMixtures,
		int VQIter,
		int GMMIter,
		float probScaleFactor,
		int ditherMean,
		float varianceNormalize,
		int seed,
		VECTOR_OF_F_VECTORS * negData,
		int negNumVectors, float npercent)
{
  int i, j, k;
  static VECTOR_OF_F_VECTORS *tempMeans, *tempVars, *tempNegMeans,
    *tempNegVars;
  static float *tempMixtureElemCnt;
  float *tempMixtureNegElemCnt;
  int mixtureNumber;
  int *mixtureNegNumberCache, *mixtureNumberCache;
  int *topNVal;
  int featLength;
  float *alphaVector, *maxDistortion;
  float beta, tempNk, minBeta;
  float tempDistortion, logLklihood;
  float alpha;
  int neglectedVectorsCnt = 0, numNegPtsUsed = 0;

  /* alpha is hardcoded. it, for now, does not depend
   * on anything. this was done due to desperation
   * to get things right
   * UPDATE: this hardcoded alpha is not used anywhere
   */
  alpha = 0.95;
  alpha = (1.0 - alpha) / alpha;

  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures,
					      sizeof (VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures,
					     sizeof (VECTOR_OF_F_VECTORS));
  for (i = 0; i < numMixtures; i++)
    {
      tempMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      tempVars[i] = (F_VECTOR *) AllocFVector (featLength);
    }
  tempMixtureElemCnt = (float *) AllocFloatArray (tempMixtureElemCnt,
						  numMixtures);
  printf ("temp mixtures allocated\n");
  fflush (stdout);

  ComputeGMM(vfv, numVectors, mixtureMeans, mixtureVars,
	     mixtureElemCnt,numMixtures,VQIter, 2, 1.0,
             3.0, 1.0, seed);

  alphaVector = (float *) calloc (numMixtures, sizeof (float));

  /* right now, all elements in alphaVector are equal,
   * but there is a scope for make them different
   * based on the effective number of positive (and negative
   * samples?) in a mixutre
   * UPDATE: this is only init'n. later on, alphaVector elmts
   *         are different for diff mixtures based on
   *         the lower bound discussed in the paper
   * UPDATE: actually, this vector is not used at all

   */

  for (i = 0; i < numMixtures; i++)
    alphaVector[i] = alpha;

  /* this is a very poor way to cache the mixture id for each sample
   */

  mixtureNumberCache = (int *) calloc (numVectors, sizeof (int));
  mixtureNegNumberCache = (int *) calloc (negNumVectors, sizeof (int));
  // the purpose of topNVal array is to hold, for each iteration, the actual
  // value of negative elements to be considered for the mixture referenced
  // by the index of the array
  topNVal = (int *) calloc (numMixtures, sizeof(int));
  // maxDistortion was in the intial implementation for a diff reason.
  // Here it is used to filter out unnecessary neg vectors even though
  // we could have only topN of them
  maxDistortion = (float *) calloc (numMixtures, sizeof(float));
  if (topNVal == NULL || maxDistortion == NULL)
    {
	   fprintf (stderr, "Unable to allocate vector for topNVal\n");
		return;
	 }

  // setting maxDistortion values for each mixture to 1.0 as we
  // know that only log probs are being calculated and the actual
  // vals cannot be >0.0, This way any positive value would mean
  // that the cell is uninitialized
  for (i = 0; i < numMixtures; i++)
    maxDistortion[i] = 1.0;

  // Training starts here. It would run for GMMIter iterations.
  // In each iteration, a set computations to find out
  // alpha value is computed and max of all of the is taken.
  // minBeta used in this loop actually refers to 1-alpha/alpha
  // expression's min value which corresponds to the max alpha.
  // From the proof of convergence, it would become clear, why
  // max alpha is required. The sooner alpha reaches 1, sooner
  // the algorithm converges.
  
  for (k = 0; k < GMMIter; k++)
    {
      printf (" GMM iteration number = %d\n", k);
      fflush (stdout);
		/* Initialize all values to zero*/
      for (i = 0; i < numMixtures; i++)
	     {
		    InitFVector (tempMeans[i]);
			 InitFVector (tempVars[i]);
	   	 tempMixtureElemCnt[i] = 0;
		  }

      /* *******************************************************
		 *
		 * For each positive example, find the closest mixture,
       * accumulate statistics for that mixture
		 * 
       ********************************************************/
      logLklihood = 0.0;
      for (i = 0; i < numVectors; i++)
	{
	  mixtureNumber = DecideWhichMixture (vfv[i], mixtureMeans,
					      mixtureVars, numMixtures,
					      mixtureElemCnt, numVectors,
					      probScaleFactor);

		/***************************************************
		 *
		 * 	the next few lines tries to add a distortion 
		 * 	threshold for data selection
		 *
 		 **************************************************/
	  tempDistortion = ComputeProbability (mixtureMeans[mixtureNumber],
					       mixtureVars[mixtureNumber],
					       mixtureElemCnt[mixtureNumber]
					       / numVectors,
					       vfv[i], probScaleFactor);	  							 
	  logLklihood   += tempDistortion;

	  if (maxDistortion[mixtureNumber] > 1.0 || 
	      maxDistortion[mixtureNumber] > tempDistortion)
	  		maxDistortion[mixtureNumber] = tempDistortion;	  

	  mixtureNumberCache[i] = mixtureNumber;
	  tempMixtureElemCnt[mixtureNumber] =
	    tempMixtureElemCnt[mixtureNumber] + 1;
	  for (j = 0; j < featLength; j++)
	    {
	      tempMeans[mixtureNumber]->array[j] =
		tempMeans[mixtureNumber]->array[j] + (vfv[i]->array[j]);
	    }
	}

		// set the number of negative elements to conside for each 
		// mixture.
		for (i = 0; i < numMixtures; i++)
		  {
		    topNVal[i] = (int) ceil(npercent * ((float) tempMixtureElemCnt[i]));
			 maxDistortion[i] *= 0.5;
		  }
		  
      tempMixtureNegElemCnt = (float *) calloc (numMixtures, sizeof (float));
      neglectedVectorsCnt = 0;
      numNegPtsUsed = 0;
      for (i = 0; i < negNumVectors; i++)
	{

	  /* this is a new addition (22/12/10) to the logic.
	   * here I think it'll be beneficial to check
	   * during the 2nd or further iterations if the
	   * sample has been previously discarded. If so, I think
	   * it should be ignored
	   */
	  if (k && mixtureNegNumberCache[i] == -1)
	    continue;

	  mixtureNumber = DecideWhichMixture (negData[i], mixtureMeans,
					      mixtureVars, numMixtures,
					      mixtureElemCnt, numVectors,
					      probScaleFactor);
	 // This call is a bit redundant. Try to define a new function to
	 // alias DecideWhichMixture which would also take in a ptr to
	 // some storage where tempDistortion could be stored and returned
	  tempDistortion =
	    ComputeProbability (mixtureMeans[mixtureNumber],
				mixtureVars[mixtureNumber],
				mixtureElemCnt[mixtureNumber] / numVectors,
				negData[i], probScaleFactor);

	  // if the closest mixture is full, ignore
	  // or if, its distance is too far, say mean+/-2sigma ignore
	  // else include it and update the value of the most distant
	  // vector
	  if (tempMixtureNegElemCnt[mixtureNumber] >= topNVal[mixtureNumber]
	    || tempDistortion >= maxDistortion[mixtureNumber])
	    {
		   
	      mixtureNegNumberCache[i] = -1;
	      neglectedVectorsCnt++;
		 }
	  else 
	    {
	      tempMixtureNegElemCnt[mixtureNumber]++;
	      ++numNegPtsUsed;
	      mixtureNegNumberCache[i] = mixtureNumber;
	    }
	}
   
	printf
	("At iter=%d, neglectedVectorsCnt=%d numNegPtsUsed=%d logLklihood=%f\n",
	 k + 1, neglectedVectorsCnt, numNegPtsUsed, logLklihood);
      tempNk = 0.0;
      minBeta = -1.0;

      for (i = 0; i < numMixtures; i++)
	{
	  beta =
	    (tempMixtureNegElemCnt[i] +
	     numMixtures) / (tempMixtureElemCnt[i] +
			     tempMixtureNegElemCnt[i]);
	  beta = (1.0 - beta) / beta;
	  if (beta < 0.0 || tempMixtureElemCnt[i] == 0.0)
	    {
	      beta = 0.0;
	    }

	  if (beta > 0.0)
	    if (minBeta == -1.0 || minBeta > beta)
	      minBeta = beta;
	}
      for (i = 0; i < numMixtures; i++)
			tempNk += (minBeta * tempMixtureNegElemCnt[i]);

      for (i = 0; i < negNumVectors; i++)
	{
	  mixtureNumber = mixtureNegNumberCache[i];
	  if (mixtureNumber == -1 || alphaVector[mixtureNumber] == 0.0)
	    continue;
	  for (j = 0; j < featLength; j++)
	    tempMeans[mixtureNumber]->array[j] -=
	      (minBeta * negData[i]->array[j]);
	}

      for (i = 0; i < numMixtures; i++)
	{
	  for (j = 0; j < featLength; j++)
	    {
	      if (tempMixtureElemCnt[i] - tempMixtureNegElemCnt[i] != 0)
		tempMeans[i]->array[j] =
		  tempMeans[i]->array[j] / ((tempMixtureElemCnt[i]) -
					    (minBeta *
					     tempMixtureNegElemCnt[i]));
	      else
		tempMeans[i]->array[j] = mixtureMeans[i]->array[j];
	    }
	  mixtureElemCnt[i] = tempMixtureElemCnt[i];	 
	}

      for (i = 0; i < numMixtures; i++)
	  		for (j = 0; j < featLength; j++)
	      tempVars[i]->array[j] = mixtureVars[i]->array[j];
      for (i = 0; i < negNumVectors; i++)
	{
	  mixtureNumber = mixtureNegNumberCache[i];
	  if (mixtureNumber == -1 || alphaVector[mixtureNumber] == 0.0)
	    continue;
	  for (j = 0; j < featLength; j++)
	    {
	      tempVars[mixtureNumber]->array[j] =
		tempVars[mixtureNumber]->array[j] -
		(negData[i]->array[j] - tempMeans[mixtureNumber]->array[j]) *
		(negData[i]->array[j] -
		 tempMeans[mixtureNumber]->array[j]) * minBeta;
	    }
	}
      
    }
  free (alphaVector);
  free (mixtureNegNumberCache);
  free (mixtureNumberCache);
  free (topNVal);
  free (maxDistortion);
}

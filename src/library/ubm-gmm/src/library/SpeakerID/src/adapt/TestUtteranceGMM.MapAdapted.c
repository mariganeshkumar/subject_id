/*-------------------------------------------------------------------------
 *  TestUtteranceGMM.c - Tests a given utterance against speaker models
 *  Version:	$Name:  $
 *  Module:	
 *
 *  Purpose:	
 *  See:	
 *
 *  Author:	Hema A Murthy (hema@bhairavi.iitm.ernet.in)
 *
 *  Created:        Tue 06-Nov-2001 18:57:03
 *  Last modified:  Mon 30-Jun-2008 09:26:59 by hema
 *  $Id: TestUtteranceGMM.c,v 1.2 2008/03/12 17:48:43 hema Exp hema $
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/
 

/************************************************************************
  Function            : TestUtteranceGMM- computes feature vectors from a 
                        test utterance, compares with trained models,
			determines the identity of an utterance.

  Input args          :  ctrlFile modelFile numGMM numSpeakers 
                         featureName wavFileName

  Uses                :  DspLibrary.c, InitAsdf.c,BatchProcessWaveform.c
                         SphereInterface.c

  Author              : Hema A Murthy

  Last Modified       : 25-May-02
*******************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "sp/sphere.h"
#include "constants.h"
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
#include "DspLibrary.h"
#include "InitAsdf.h"
#include "BatchProcessWaveform.h"
#include "SphereInterface.h"
#include "math.h"
#include "GMM.h"

/*-------------------------------------------------------------------------
 *  ComputeThreshold -- Computes the threshold on energy for a waveform
 *    Args:	
 *    Returns:	float
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeThreshold(ASDF *asdf, float thresholdScale)
{
  int i;
  float ave = 0;
  for (i = 0; i < asdf->numFrames; i++)
    ave = ave + ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
  ave = ave/asdf->numFrames;
  return (ave*thresholdScale);
}	/*  End of ComputeThreshold		End of ComputeThreshold   */

/************************************************************************
  Function            : ComputeFeatureVectors - computes the
                        feature vectors from a list of speech data file 
			writes the vectors to the array vfv

  Inputs              : asdf - control structure, 
                        speaker_File - a text file  containing a list of 
			speech data files
			feature_Name - name of the feature

  Outputs             : vfv - array of feature vectors
            
  **************************************************************************/

VECTOR_OF_F_VECTORS *ComputeFeatureVectors(ASDF *asdf, char *featureName,
					   unsigned long  *numVectors, float thresholdScale) {

  F_VECTOR                  *fvect;
 VECTOR_OF_F_VECTORS        *vfv;
 int                        i;
 unsigned long              frameNum;
 float                      threshold, energy;
/*printf("enter compute feat vect \n");
fflush(stdout);*/

  vfv = (VECTOR_OF_F_VECTORS *) calloc(asdf->numFrames, sizeof(VECTOR_OF_F_VECTORS));
  threshold = (float) ComputeThreshold(asdf, thresholdScale);
  frameNum = 0;
  for (i = 0; i < asdf->numFrames; i++) {
    energy = ((F_VECTOR *) GsfRead(asdf, i, "frameEnergy"))->array[0];
    if (energy > threshold) {
      fvect = (F_VECTOR *) GsfRead(asdf,i,featureName);
      if(fvect == NULL) {
	printf("problems fvect\n");
	fflush(stdout);
	exit(-1);
      }
      vfv[frameNum] = fvect;
      frameNum++;
    }
  }

  GsfClose(asdf);
*numVectors = frameNum;
/*printf("total no of frame processed = %d\n",*numVectors);
fflush(stdout); */
return(vfv);
}
/************************************************************************
  Function            : MixtureProbability - computes the
                        maximum prob for a given feature vector

  Inputs              : fvect - feature vector
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
			probScaleFactor - scalefactor for probability
  Outputs             : smallest distance
            
  **************************************************************************/

float MixtureProbability(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *speakerMean,
		   VECTOR_OF_F_VECTORS *speakerVar, F_VECTOR *speakerWts, 
		  int numClusters, float probScaleFactor){
int                          i;
float                        maxProb = 0, probValue;
int                          featLength; 
            
/*  printf("enter MixtureProbability\n");
  fflush(stdout);*/
  featLength = fvect->numElements;
  /*  printf("numClusters = %d featLength = %d\n",numClusters,featLength);
  fflush(stdout);*/
   maxProb = ComputeProbability(speakerMean[0], 
				   speakerVar[0], 
				   speakerWts->array[0], 
				   fvect, probScaleFactor); //fvect->numElements;
  for (i = 1; i < numClusters; i++) {
    probValue = ComputeProbability(speakerMean[i], 
				   speakerVar[i], 
				   speakerWts->array[i], 
				   fvect, probScaleFactor); //fvect->numElements;
    // if (fabs(probValue) < 10000)
    maxProb  = LogAdd(maxProb, probValue);
  }
  //  printf("probs %f\n",maxProb);
  return(maxProb);
}

/************************************************************************
  Function            : ComputeLikelihood - computes the
                        likelihood for the entire utterance data.

  Inputs              : vfv - feature vectors for an utterance
                        speakerMean - speaker_means 
			speakerVar - speaker_variances
			speakerWts - weights for different clusters
                        numClusters - number of clusters/speaker
  Outputs             : Distortion
            
  **************************************************************************/

float  *ComputeTopCLikelihood (VECTOR_OF_F_VECTORS *vfv, unsigned long  numVectors, 
			   int numSpeakers, 
			   VECTOR_OF_F_VECTORS **speakerMeans, 
			   VECTOR_OF_F_VECTORS **speakerVars, 
			   VECTOR_OF_F_VECTORS *speakerWts, 
			   int *numClusters, float *Distortion, 
			  float probScaleFactor, int cValue, int *mixIds) {
  
  int                     i, j,k, mixNumber;
  float                   mixProbValue;
  float                   evidence;
  /*  printf("enter comp dist\n");
      fflush(stdout);*/
  for (i = 0; i < numSpeakers; i++) {
    Distortion[i] = 0;
    for (j = 0; j < numVectors; j++) {
	for (k = 0; k < cValue; k++)
	 {
	 mixNumber = mixIds[k];
	 if (!k)
	 mixProbValue = ComputeProbability(speakerMeans[i][mixNumber], 
			       speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			       probScaleFactor);
	 else
	 mixProbValue = LogAdd (mixProbValue, 
				ComputeProbability(speakerMeans[i][mixNumber], 
			        speakerVars[i][mixNumber], speakerWts[i]->array[mixNumber],vfv[j], 
			        probScaleFactor));
	  }
	 //      if (mixProbValue != 0.0)
	Distortion[i] = Distortion[i] + mixProbValue;
      //     else
      //Distortion[i] = Distortion[i] +LOG_ZERO;
    }
    printf("Distortion %d = %f\n",i,Distortion[i]);
    fflush(stdout); 
    Distortion[i] = Distortion[i]/numVectors;
  }
  evidence = Distortion[0];
  for (i = 1; i < numSpeakers; i++)
    evidence = LogAdd(evidence, Distortion[i]);
  for (i = 0; i < numSpeakers; i++)
    Distortion[i] = Distortion[i] - evidence;
  return(Distortion);
}

struct topCMixtures_ {
      int id;
      float DistortionValue
} ;

typedef struct topCMixtures_ topCMixtures;

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
       {
	 if (bestCMixtures[i].DistortionValue > bestCMixtures[i-1].DistortionValue)
	   {
		tempMixId = bestCMixtures[i].id;
	        tempDist  = bestCMixtures[i].DistortionValue;
		bestCMixtures[i].id = bestCMixtures[i-1].id;
		bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
		bestCMixtures[i-1].id = tempMixId;
		bestCMixtures[i-1].DistortionValue = tempDist;
	   }
       }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int *GetTopCMixtures (VECTOR_OF_F_VECTORS *ubmModelMeans, VECTOR_OF_F_VECTORS *ubmModelVars,
		      float *ubmWts, unsigned int numClusters, 
                      VECTOR_OF_F_VECTORS *vfv, unsigned int numVectors, int cValue, float probScaleFactor)
  {
    int  *mixtureIds, i, j;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int *) calloc (cValue, sizeof (int));
    for (i = 0; i < numClusters; i++)
      {
	for (j = 0; j < numVectors; j++)
	  {
		if (!j)
		Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor);
		else
		Dist[i] = LogAdd (Dist[i], ComputeProbability (ubmModelMeans[i], ubmModelVars[i], ubmWts[i], vfv[j], probScaleFactor));
	  }
      }

    for (i = 0; i < cValue; i++)
      {
	bestCMixtures[i].id = i+1;
	bestCMixtures[i].DistortionValue = Dist[i];
	
	if (i)
		ReArrangeMixtureIds(bestCMixtures, i+1);
      }

     for (i = cValue; i < numClusters; i++)
       {
	if (Dist[i] > bestCMixtures[cValue-1].DistortionValue)
	{
		bestCMixtures[cValue-1].id = i +1;
		bestCMixtures[cValue-1].DistortionValue = Dist[i];
		ReArrangeMixtureIds(bestCMixtures, cValue);
	}
       }

    for (i = 0; i < cValue; i++)
	mixtureIds[i] = bestCMixtures[i].id;
    return mixtureIds;  
  }

void Usage() {
   printf (" Usage : TestUtteranceGMM ctrlFile modelFile featureName numSpeakers wavName threshold test/verify(0/1) ubm topC\n");
}



int main(int argc, char *argv[])
  {

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL, *ubmFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, *ubmFileName = NULL,
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  VECTOR_OF_F_VECTORS   *ubmModelMeans, *ubmModelVars;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k, *mixIds;
  int                   featLength, cValue;
  unsigned long         numVectors;
  int                   verify;
  float                 *Distortion, *ubmWts;
  float                 thresholdScale;
  if (argc != 10) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  //  string1 = argv[3];
  //sscanf(string1,"%d",&numClusters);
  featureName = argv[3];
  string2 = argv[4];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[5];
  string1 = argv[6];
  sscanf(string1, "%f", &thresholdScale);
  string1 = argv[7];
  sscanf(string1, "%d", &verify);
  ubmFileName = argv[8];
  sscanf (argv[9], "%d", &cValue);

  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  /*printf("testfile name = %s\n",wavname);
  fflush(stdout);*/
  GsfOpen(asdf,wavname);
  vfv = (VECTOR_OF_F_VECTORS *) ComputeFeatureVectors(asdf, featureName, &numVectors, thresholdScale);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  /*  for (i = 0; i < numSpeakers; i++) {
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    }*/
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    //speakerModel[strlen(speakerModel)+1] = '\0';
    speakerFile = fopen(speakerModel,"r");
    /*printf("speakerFile = %s\n",speakerModel);
    fflush(stdout); */
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      /*      printf("%f\n",speakerModelWts[i]->array[j]);
      fflush(stdout);*/
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
	/*	        printf(" %f %f\n",speakerModelMeans[i][j]->array[k],
			speakerModelVars[i][j]->array[k]);  
			fflush(stdout);*/
      }      
    }
    i++;  
    fclose(speakerFile);
  }

  ubmFile = fopen (ubmFileName, "r");
  ubmModelMeans = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmModelVars  = (VECTOR_OF_F_VECTORS *) calloc (numClusters[0], sizeof (VECTOR_OF_F_VECTORS));
  ubmWts        = (float *) calloc (numClusters[0], sizeof (float));

  for (i = 0; i < numClusters[0]; i++)
    {
      fscanf (ubmFile, "%f\n", &ubmWts[i]);
      ubmModelMeans[i] = (F_VECTOR *) AllocFVector (featLength);
      ubmModelVars[i]  = (F_VECTOR *) AllocFVector (featLength);
      for (j = 0; j < featLength; j++)
   	{
	  fscanf (ubmFile," %f %f",&ubmModelMeans[i]->array[j],&ubmModelVars[i]->array[j]);
	}
      fscanf (ubmFile,"\n");
    }
  fclose (ubmFile);

  mixIds = GetTopCMixtures (ubmModelMeans, ubmModelVars, ubmWts, numClusters[0], vfv, numVectors, cValue, asdf->probScaleFactor); 
  for (i = 0 ; i < cValue; i++)
	printf ("mixId of %d is %d\n", i+1, mixIds[i]);
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeTopCLikelihood(vfv, numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   asdf->probScaleFactor, cValue, mixIds);
  printf("%s\n",wavname);
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import argparse
from tqdm import tqdm
import numpy as np
import pickle
import scipy.io as sio

dev_iVectorMatrix = []

parser = argparse.ArgumentParser()
parser.add_argument("--devDataScriptFile", help="Location of script file for dev data for computing LDA.",
                    type=str, )
#parser.add_argument("--wMat", help="Location of whiting transform matrix.",
#                    type=str, )
parser.add_argument("--devMat", help="Location of to save modelfiles.",
                    type=str)
parser.add_argument("--ldaMat", help="Location of to save modelfiles.",
                    type=str)
args = parser.parse_args()


with open(args.devDataScriptFile) as f:
    devDataScript = f.readlines()
devDataScript = [x.strip() for x in devDataScript]
devFiles = [x.split(' ')[0] for x in devDataScript]
devLabel = [int(x.split(' ')[1]) for x in devDataScript]
for i in tqdm(range(len(devFiles))):
    filename = devFiles[i]
    with open(filename, 'r') as f:
        lines = f.readlines()
        lines = [x.split(' \n')[0] for x in lines]
        iVec = np.asarray(lines, dtype=np.float32)
        f.close()
        dev_iVectorMatrix.append(iVec)

dev_iVectorMatrix = np.array(dev_iVectorMatrix)
devLabel = np.array(devLabel)

#wMAT = np.genfromtxt(args.wMat, delimiter=' ')
#dev_iVectorMatrix = np.matmul(dev_iVectorMatrix, wMAT)

clf = LinearDiscriminantAnalysis(solver='svd')
clf.fit_transform(dev_iVectorMatrix, devLabel)
pickle.dump(clf, open(args.ldaMat, 'wb'))
dev_iVectorMatrix=clf.transform(dev_iVectorMatrix)

sio.savemat(args.devMat,{
    'dev_iVectorMatrix': dev_iVectorMatrix,
    'devLabel': devLabel,
    })
function K = KernelGramMatrix (data, kernelType, param)
	N = size (data,1);
	K = zeros (N,N);
	if kernelType == 2
		K = (data * data').^param;
	elseif kernelType == 3
		for i = 1 : N
			x = data - repmat(data(i,:),N,1);
			K(i,:) = exp(-param * sum((x .* x)'));
		end
	end

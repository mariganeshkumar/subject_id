classifier_name={...
    'K-NN_Channel_Concatnated',...      %1
    'UBM-GMM',...                       %2
    'UBN-GMM_Concatnated',...           %3
    'Super-Vector_SVM ',...             %4
    'lamda-Vector_SVM_99',...           %5
    'lamda-Vector_SVM_Best',...         %6
    'i-vector_SVM',...                  %7
    'i-vector_Cosine',...               %8
    'i-vector_PLDA',...                 %9
    'modified-i-vector_SVM',...         %10
    'modified-i-vector_Cosine',...      %11
    'con-i-vector_Cosine',...           %12
    'score-fusion-i-vector_Cosine',...  %13
    'modified-x-vector-classifier',...  %14
    'modified-x-vector_SVM',...         %15
    'modified-x-vector_Cosine',...      %16
    'con-x-vector_Cosine',...           %17
    'score-fusion-x-vector_Cosine',...  %18
    'x-vector-Classifer',...            %19
    'x-vector_Cosine',...               %20
    };
close all;
classifiers = [20,17,18,16];
h=figure();
f=figure();
for i = 1:length(classifiers)
    close(f);
    f = openfig([classifier_name{classifiers(i)},'/1/60_test.fig'],'reuse');
    figure(f);
    ax1 = gca;
    fig1 = get(ax1,'children');
    figure(h);
    b = subplot_tight(6,8,[(i-1)*2+1,(i-1)*2+2],[0.06,0.06]);
    copyobj(fig1,b);
    title(ax1.Title.String)
    clear ax1 fig1;
    close(f);
    f = openfig([classifier_name{classifiers(i)},'/1/60_top_inter.fig'],'reuse');
    figure(f);
    ax1 = gca;
    fig1 = get(ax1,'children');
    figure(h);
    b = subplot_tight(6,8,[(i-1)*2+8+1,(i-1)*2+8+2,(i-1)*2+16+1,(i-1)*2+16+2],[0.06,0.06]);
    copyobj(fig1,b);
end

classifiers = [8,12,13,11];

for i = 1:length(classifiers)
    close(f);
    f = openfig([classifier_name{classifiers(i)},'/1/60_test.fig'],'reuse');
    figure(f);
    ax1 = gca;
    fig1 = get(ax1,'children');
    figure(h);
    b = subplot_tight(6,8,[(i-1)*2+24+1,(i-1)*2+24+2],[0.06,0.06]);
    copyobj(fig1,b);
    title(ax1.Title.String)
    clear ax1 fig1;
    close(f);
    f = openfig([classifier_name{classifiers(i)},'/1/60_top_inter.fig'],'reuse');
    figure(f);
    ax1 = gca;
    fig1 = get(ax1,'children');
    figure(h);
    b = subplot_tight(6,8,[(i-1)*2+32+1,(i-1)*2+32+2,(i-1)*2+40+1,(i-1)*2+40+2],[0.06,0.06]);
    copyobj(fig1,b);
end
legend(ax1.Legend.String)


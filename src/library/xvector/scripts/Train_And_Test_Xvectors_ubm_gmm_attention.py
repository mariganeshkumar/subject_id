import numpy as np

np.random.seed(1337)

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
import keras
from TDNN_layer import TDNN
from keras.models import Sequential
from keras.layers import Dense
from keras import losses
from keras.utils.np_utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.utils.generic_utils import get_custom_objects
import keras.backend as K

import scipy.io as sio

import hdf5storage

# fix random seed for reproducibility
from tqdm import tqdm

dataset = hdf5storage.loadmat('tmp/subject_id_dataset_for_keras.mat')




# split into input (X) and output (Y) variables
train_data = dataset['train_data']
train_label = dataset['train_label']
val_data = dataset['val_data']
val_label = dataset['val_label']
test_data = dataset['test_data']
test_label = dataset['test_label']

train_data = np.moveaxis(train_data, 1, -1)
shape = train_data.shape
train_data = np.reshape(train_data,(shape[0],shape[1],shape[2]*shape[3]))
train_data = np.moveaxis(train_data, 1, -1)
no_of_examples = train_label.shape[1]
randperm = np.random.permutation(no_of_examples)
train_label = train_label[:, randperm]
train_data = train_data[randperm,:,:]
train_label = to_categorical(np.squeeze(train_label - 1), num_classes=np.unique(train_label).shape[0])




val_data = np.moveaxis(val_data, 1, -1)
shape = val_data.shape
val_data = np.reshape(val_data,(shape[0],shape[1],shape[2]*shape[3]))
val_data = np.moveaxis(val_data, 1, -1)
#no_of_examples = val_label.shape[1]
#randperm = np.random.permutation(no_of_examples)
#val_label = val_label[:, randperm]
#val_data = val_data[randperm,:,:]
val_label =  to_categorical(np.squeeze(val_label - 1), num_classes=train_label.shape[1])


test_data = np.moveaxis(test_data, 1, -1)
shape = test_data.shape
test_data = np.reshape(test_data,(shape[0],shape[1],shape[2]*shape[3]))
test_data = np.moveaxis(test_data, 1, -1)
#no_of_examples = test_label.shape[1]
#randperm = np.random.permutation(no_of_examples)
#test_label = test_label[:, randperm]
#test_data = test_data[randperm,:,:]
test_label =  to_categorical(np.squeeze(test_label - 1), num_classes=train_label.shape[1])




early_stopping = EarlyStopping(patience=10, verbose=1)
model_checkpoint = ModelCheckpoint("./keras.model", save_best_only=True, verbose=1)
reduce_lr = ReduceLROnPlateau(factor=0.5, patience=3, min_lr=0.00000000001, verbose=1)

def mixed_mse_cross_entropy_loss(y_true, y_pred):
    return 0.8 * losses.categorical_crossentropy(y_true, y_pred) + 0.2 * losses.mean_squared_error(y_true,y_pred)


get_custom_objects().update({"mixed_loss": mixed_mse_cross_entropy_loss})


num_mix = 8
inputs = keras.Input(shape=(None, train_data.shape[-1],))
t1 = TDNN(512,(0,),padding='same', activation="sigmoid", name="TDNN1")(inputs)
t2 = TDNN(256,input_context=(0,),padding='same', activation="sigmoid", name="TDNN2")(t1)
t3 = TDNN(num_mix,input_context=(0,),padding='same', activation="softmax", name="TDNN3")(t2)




def split(x) : 
    splits  = tf.split(x, num_mix, axis=-1)
    return splits
mask_repeat = keras.layers.Lambda(lambda xin:K.repeat_elements(xin,256,axis=-1), output_shape=(None,256))
mask_multiply = keras.layers.Multiply()
average = keras.layers.Lambda(lambda xin: K.mean(xin, axis=1), output_shape=(256,))
variance = keras.layers.Lambda(lambda xin: keras.backend.std(xin, axis=1), output_shape=(256,))


splitted_masks = keras.layers.Lambda(split)(t3)

means=[]
for mask in splitted_masks:
	mask_r = mask_repeat(mask)
	weighted_t2 = mask_multiply([t2,mask])
	means.append(average(weighted_t2))

k1 = keras.layers.Concatenate()(means)
d1 =Dense(500, activation='sigmoid', name='dense_1')(k1)
d2 =Dense(100, activation='sigmoid', name='dense_2')(d1)
output = Dense(train_label.shape[1], activation='softmax', name='dense_' + str(train_label.shape[1]))(d2)

model = keras.Model(inputs=inputs, outputs=output)

adam_opt = keras.optimizers.Adam(lr=0.001, clipvalue=1)
model.compile(loss= losses.categorical_crossentropy, optimizer=adam_opt, metrics=['accuracy',keras.metrics.top_k_categorical_accuracy])
model.summary()
# model.summary()
# Fit the model

#
model.fit(train_data, train_label,validation_data=(val_data, val_label), epochs=3000, batch_size=16, verbose=2,
          callbacks=[early_stopping, model_checkpoint, reduce_lr])

data_score = model.predict(val_data, verbose=0, batch_size=32)
predicted_val_labels = np.argmax(data_score, axis=1) + 1

data_score = model.predict(test_data, verbose=0, batch_size=32)
predicted_labels = np.argmax(data_score, axis=1) + 1


sio.savemat('tmp/predicted_labels.mat',
            {'predicted_labels': predicted_labels, 'predicted_val_labels': predicted_val_labels})
print('done')

/*
 * =====================================================================================
 *
 *       Filename:  DiscAdapt.c
 *
 *    Description:  Functions to adapt models discriminatively
 *
 * * =====================================================================================
 */

#include "DiscAdapt.h"

/**************************************************************
 * Function : DiscAdaptOnlyMeans
 *
 * Description: Only the UBM means are adapted. First few args
 *              are same as those in generative modelling
 *
 * Arguments:
 *    ubmMeans, ubmVars, ubmWts: ubm parameters
 *    numClusters: no.of mixtures in the UBM. this is going
 *                 to be the no.of mixtures for the model
 *    fvec       : feature vectors used to adapt the model
 *    nvec       : number of vectors
 *    rfactor    : relavence factor
 *    psfactor   : probability scale factor. usually = 1.0
 *                 but only sticking to the convetions in adding 
 *                 this parameter to proto
 *    negfvec    : anti-spkr data
 *    negnvec    : amount of anti-spkr data available
 *    nper       : percentage of neg data to be used during
 *                 discriminative training. Percentage is
 *                 the fraction w.r.t to nvec for impostor
 *                 data selection
 *                 
 * Algorithm design decisions:                 
 *    i)  Takes only a percentage of the anti-spkr data so
 *        that training shouldn't take long
 *
 * TODO: uniformity in using variables i,j,k need to be 
 *       taken care of. currently, they could mean anything.
 *       indexing in an uniform way would mean a lot
 *************************************************************/ 

VFV* 
DiscAdaptOnlyMeans (VFV *ubmMeans, VFV *ubmVars, float *ubmWeights,
                       uint numClusters, 
                       VFV *fvec, uint nvec, 
                       VFV *negfvec, uint negnvec, 
                       float rfactor,
                       float nper) {
    
    VFV                 *tempMeans = NULL,
                        *adaptedMeans = NULL,
                        *negMeans = NULL, *vfv = fvec;

    float			    alpha, beta, normFactorI, minBeta,
                        maxProb, logProb, offset = numClusters;
    float               psf = 1.0, admissible;

    int                 dim, i, j, k, maxProbIdx, 
                        tempMaxProbIdx;
    float               *eeta = NULL, *negEeta = NULL;
    
    dim = ubmMeans[0]->numElements;
    if (nvec == 0 || vfv[0]->numElements != dim)  return NULL;
    

    /* The function of eeta is to store responsibility factors */
    eeta    = (float*) calloc (numClusters, sizeof(float));
    negEeta = (float*) calloc (numClusters, sizeof(float));
    finc(i,numClusters,1) {
      eeta[i] = 0;
      negEeta[i] = 0;
    }

    printf ("Finished initialization %d %d\n",nvec,negnvec); fflush(stdout);

    /*  for each vector, find the closest mixture and update
     *  the mixture accumulate prob (eeta) */
    tempMeans = (VFV *) calloc (numClusters, sizeof(VFV));
    finc(j,numClusters,1)  {
      tempMeans[j] = (FVec *) AllocFVector (dim);
      InitFVector (tempMeans[j]);
    }
    finc(i,nvec,1) {
      finc(j,numClusters,1) {
			  logProb = ComputeProbability (
                      ubmMeans[j], ubmVars[j], ubmWeights[j], 
								      fvec[i], psf);
        /*  checking if the recent one is the close
         *  is a way to avoid sorting               */
			  if (j == 0 || logProb > maxProb) { 
			    maxProb= logProb; 
          maxProbIdx= j; 
        }
      }
      eeta[maxProbIdx]++;
      finc(k,dim,1)
			  tempMeans[maxProbIdx]->array[k] += vfv[i]->array[k];	
    }

    
    /*  adapt the means from positive samples */

    /*  move the adapted means so that 
     *  there is better discriminability */
    admissible = nper * nvec / 100.0;
    /*  the idea behind using accumulating negMeans
     *  for each cluster is to avoid calling 
     *  ComputeProbability() again */
    negMeans = (VFV *) calloc (numClusters, sizeof(VFV));
    finc(j,numClusters,1) {
        negMeans[j] = (F_VECTOR *) AllocFVector (dim);
        InitFVector (negMeans[j]);
    }

    
    finc(i,negnvec,1) {
        maxProb = -INFINITY;
        maxProbIdx = -1;
        finc(j,numClusters,1) {
             /* currently, only the mixtures that are 
              * adapted are moved */
            if (eeta[j] == 0) continue;
            
            logProb = ComputeProbability (
                        ubmMeans[j], ubmVars[j], ubmWeights[j], 
                        negfvec[i], psf);
            
            if (logProb > maxProb) { 
                maxProb= logProb; 
                maxProbIdx= j; 
            }
        }
        if (maxProbIdx >= 0) {
            negEeta[maxProbIdx]++;
            finc (k,dim,1) 
                negMeans[maxProbIdx]->array[k] += negfvec[i]->array[k];
            if (--admissible <= 0) break;            
        }  
    }
/*      finc(k,numClusters,1) 
        finc(j,dim,1) 
            if (negEeta[k] > 0)
                negMeans[k]->array[j] /= negEeta[k];*/
    

    /* compute regularization factor such that
     * the negative fvecs get some weightage but
     * not too much. here, weightage is given such
     * that not N - \beta N' is positive => means
     * move in the right direction. 
     *
     * TODO: Perhaps, it will be worthwhile to experiment 
     * if variance reshaping is required
     */

    minBeta = 0.0;
    finc(k,numClusters,1) {
        if (eeta[k] == 0.){
            continue;
        }
        beta = (negEeta[k]) / (eeta[k] + negEeta[k]);
        beta += 1E-03; // adding a tiny offset
        if (beta > 1.0)
            beta = 1.0;

        if (minBeta < beta)
                minBeta = beta;        
    }    

    printf ("minBeta = %e\n", minBeta);
	  
    adaptedMeans = (VFV*) calloc (numClusters, sizeof (VFV));

    finc(k,numClusters,1) {
      adaptedMeans[k] = (FVec *) AllocFVector (dim);
      if (eeta[k] == 0.) {
        finc(j,dim,1)
          adaptedMeans[k]->array[j] = ubmMeans[k]->array[j];
        continue;
      }
	    alpha 
            = (
                eeta[k] 
                / 
                (eeta[k]  
                + rfactor
                )
              );

        finc(j,dim,1) {
                adaptedMeans[k]->array[j] 
                  = (alpha * 
                      (minBeta * tempMeans[k]->array[j] 
                      - ((1.0 - minBeta) * negMeans[k]->array[j])
                      )
                    / 
                     ( 
                      minBeta *  eeta[k] 
                      - 
                      (1.0 - minBeta) *  negEeta[k]
                     )
                    )
                    +
                    (1.0 - alpha) * (ubmMeans[k]->array[j]);
        }
    }

  /* clean up and return the means */
    
  free (eeta);  
  free (negEeta);
  finc(k,numClusters,1) {
      FreeFVector (tempMeans[k]);
      FreeFVector (negMeans[k]);
  } 
  return adaptedMeans;
}


/*
 * =====================================================================================
 *
 *       Filename:  TNorm.c
 *
 *    Description:  Computes tnorm based on given score value and cohort scores
 *
 *
 * =====================================================================================
 */

#include<stdio.h>
#include<math.h>

int
main (int argc, char *argv[]) {
    FILE *sf = NULL, *cf = NULL;
    float score, tempf, mean = 0.0, var = 0.;
    int nl;
    if (argc < 3) {
        printf ("Usage: TNorm scorefile cohortfile\n");
        exit (1);
    }

    sf = fopen (argv[1], "r");
    fscanf (sf, "%e", &score);
    fclose (sf);

    cf = fopen (argv[2], "r");
    nl = 0;
    while (!feof (cf)) {
        fscanf (cf, "%e\n", &tempf);
        if (isnan (tempf) || isinf(tempf)) continue;
        mean += tempf;
        nl++;
    }    
    mean = mean / nl;
    rewind (cf);
    var = 0.0;
    while (!feof (cf)) {
        fscanf (cf, "%e\n", &tempf);
        if (isnan (tempf) || isinf(tempf)) continue;
        var = var + ((tempf - mean)*(tempf-mean));
    }    
    var = sqrt(var) / nl;
    fclose (cf);
    tempf = (score - mean) / var;
    printf ("%e\n", tempf);
}

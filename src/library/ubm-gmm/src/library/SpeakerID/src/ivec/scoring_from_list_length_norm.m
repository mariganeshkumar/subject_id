function res = scoring_from_list_length_norm (testname, trainlist, ldamatrix, opname, t_is_list,exflag, whitematrix)
  if t_is_list == 1
    testlist = readlistascell (testname);
    sl = length (testlist);
    oplist = readlistascell (opname);
  else
    testlist{1} = testname;
    sl = 1;
    oplist{1} = opname;
  end

  tl = readlistascell (trainlist);
  ll = length (tl);
  A = load (ldamatrix);  
  [R psize] = size (A);
  A = A';
  white_mat = load(whitematrix, '-ascii');

  wmat = zeros (R,ll);
  for i = 1:ll
      x = load (tl{i});
      x = white_mat' * x;
      x = x / sqrt(x'*x);
      wmat(:,i) = x;
  end
  
  wmat = (A * wmat)';

  n1 = sqrt (sum ((wmat .* wmat),2));  
  s = zeros (ll,1);
  tnorm = ll - 300;
  res = zeros(sl,1);
  for tno = 1:sl
      y = load (testlist{tno});
      y = white_mat' * y; 
      w_test = y / sqrt (y'*y);
      w_test = A * w_test;
      n2 = sqrt (w_test' * w_test);
      w_test = w_test / n2;
      s = (wmat * w_test) ./ n1;
      mu = mean (s(tnorm:ll));
      sigma = std (s(tnorm:ll));
      s = (s - mu) / sigma;
      save (oplist{tno}, 's', '-ascii')
      [mval id] = max (s);
      res (tno) = id;
  end
  if exflag ~= 0
    exit
  end

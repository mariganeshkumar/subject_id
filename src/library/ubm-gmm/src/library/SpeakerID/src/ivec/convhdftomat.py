import math
import getopt
import sys
import numpy as np
from tables import *

ipn,opn = sys.argv[1], sys.argv[2]
ipf = openFile (ipn, "r")
mtab = ipf.root.matrix
dtab = ipf.root.dim

for row in dtab.iterrows ():
  nrows, ncols = row['dim']
  break

opf = open (opn, "w")
for row in mtab.iterrows ():
  svec = row['colvec']
  for i in range (0, ncols):
    opf.write ("%e "% svec[i])
  opf.write ("\n")
opf.close ()
ipf.close ()

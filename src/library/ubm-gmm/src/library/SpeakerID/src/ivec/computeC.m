% compute C matrix 

function computeC (cmatname, fxlist, wxlist, exflag)
    fxl = readlistascell (fxlist);
    wxl = readlistascell (wxlist);

    ii = length (wxl);    
    Cinit = 0;

    for i=1:ii        
        try
          [N fx] = ReadFoStat (fxl {i});
          wx = load (wxl {i});
          if Cinit == 0
              R = length (wx);
              MD = size (fx,1);
              C = zeros (MD,R);
              Cinit = 1;
          end
          C = C + fx * wx';
        catch
          continue
        end
    end

    save (cmatname, 'C', '-mat')
    if exflag ~= 0
      exit
    end

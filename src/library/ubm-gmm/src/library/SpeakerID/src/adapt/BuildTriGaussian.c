/********************************************************************
* This program is used to build a tri gaussian that is required
* for VAD during feat extraction stages in some programs.
* It extracts energy, computes log and builds 3 clusters. An
* IMPORTANT NOTE: is that it doesnt sort the clusters according
* to the weights or means.
********************************************************************/




#include "CommonFunctions.h"

void Usage() {
   printf(" BuildTriGaussian ctrlFile trainFileList outFile\n ");
}





main(int argc, char *argv[])
{

  FILE                  *cFile=NULL, *speakerFile=NULL,*outputFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *speakerTable =NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL, line[500];
  int                   numClusters, idx[3], num_smaller;
  VECTOR_OF_F_VECTORS   *clusterMeans,*clusterVars;
  float                 *clusterWts, thresholdScale;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv, *allVfv;
  int                   i,j, k, numFiles, numVectorsProcessed,totalVectors;
  unsigned long                   numVectors;
  int                   VQIter, GMMIter;
  float                 probScaleFactor, ditherMean, varianceNormalize, varianceFloor;
  int                   seed = 1331;
  if (argc != 4 ) {
    Usage();
    exit(-1);
  }

  cname = argv[1];
  speakerTable = argv[2];
  outputFile = fopen(argv[3],"w");
  cFile = fopen(cname,"r");
  speakerFile = fopen(speakerTable,"r");
  thresholdScale = 0.0; // hard coded

  featureName = "frameLogEnergy";
  VQIter=10;
  GMMIter=5;
  numClusters=3;

  printf("We are here now\n");
  clusterMeans = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						 sizeof(VECTOR_OF_F_VECTORS));
  clusterVars = (VECTOR_OF_F_VECTORS *) calloc(numClusters,
						sizeof(VECTOR_OF_F_VECTORS));
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  varianceNormalize = (int) GetIAttribute(asdf, "varianceNormalize");
  ditherMean = (float) GetFAttribute (asdf, "ditherMean");
  probScaleFactor =  (float) GetFAttribute (asdf, "probScaleFactor");
  varianceFloor = (float) GetFAttribute (asdf, "varianceFloor");
  Cstore(asdf->fftSize);

	
  PutIAttribute (asdf,"stGauss",0);
  PutIAttribute (asdf, "zeroMean", 0);
  
  numVectorsProcessed=0;
  rewind(speakerFile);
  allVfv = ComputeFeatureVectors (asdf,speakerFile,featureName,&numVectorsProcessed,0.0);
	
  for (i = 0; i < numClusters; i++) {
    clusterMeans[i] = (F_VECTOR *) AllocFVector(allVfv[0]->numElements);
    clusterVars[i] = (F_VECTOR *) AllocFVector(allVfv[0]->numElements);
  }
  clusterWts = (float *) calloc(numClusters, sizeof(float));

  printf("number of frames = %d numGMM = %d\n",numVectorsProcessed,GMMIter);
  // We dont use ComputeGMM from TrueGMM
  
  ComputeGMM(allVfv, numVectorsProcessed, clusterMeans, clusterVars,
	     clusterWts,numClusters,VQIter, GMMIter, probScaleFactor,
             ditherMean, varianceNormalize, varianceFloor, seed);
  for (i = 0; i < numClusters; i++) {
    num_smaller = 0;
    for (j = 0; j < numClusters; j++) {
      if (j == i) continue;
      if (clusterMeans[i]->array[0] > clusterMeans[j]->array[0]) 
        num_smaller++;
    }
    idx[num_smaller] = i;
  }
  
  for (i = 0; i < numClusters; i++){
    k = idx[i];
    if (clusterWts[k] != 0) {
      fprintf(outputFile,"%e \n", clusterWts[k]);
      printf("%e \n", clusterWts[k]);
    } else {
      fprintf(outputFile, "%e\n", 1.0E-6);
      printf("%e\n", 1.0E-8);
    }
    for (j = 0; j < allVfv[0]->numElements; j++) {
      printf("mean=%e var=%e \n",
	     clusterMeans[k]->array[j],clusterVars[k]->array[j]);
      fprintf(outputFile," %e %e ",clusterMeans[k]->array[j],
	      clusterVars[k]->array[j]);
    }
    fprintf(outputFile,"\n");
  }
  fclose(outputFile);
  FreeVfv (allVfv, numVectorsProcessed);
  free(clusterMeans);
  free(clusterVars);
  free(clusterWts);
}

















/*-------------------------------------------------------------------------
 * $Log: TrainModelsGMM.c,v $
 * Revision 1.1  2002/04/30 09:34:53  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:30:22  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TrainModelsGMM.c
 -------------------------------------------------------------------------*/

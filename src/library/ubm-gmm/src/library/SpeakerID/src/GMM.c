m/****************************************************************************
 *   Function             : A collection of procedures for Gaussian Mixture
 *                        : Modeling
 *   Uses                 : DspLibrary.c, InitAsdf.c
 *   Author               : Hema A Murthy
 *   Last Updated         : May 23 2002
 *   Source               : Rabiner and Juang, Fundamentals of Speech Recogn.
 *   Bugs                 : none known to date
 *****************************************************************************/

#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/InitAsdf.h"
#include "fe/DspLibrary.h"
#include "stdlib.h"
#include "math.h"
/****************************************************************************
 *   Function             : InitGMM - initialises the GMMs with a set
 *                        : of mean vectors and variance vectors
 *   Input args           : asdf (front-end structure), 
 *                        : vfv - vector of featureVectors
 *                        : numMixtures : number of Vectors
 *   Output args          : mixtureMeans - vector of GMM mean vectors
 *                        : mixtureVars  - vector of GMM variance vectors 
 *****************************************************************************/

void InitGMM (ASDF *asdf, VECTOR_OF_F_VECTORS *vfv,int numVectors, 
              VECTOR_OF_F_VECTORS *mixtureMeans,               
	      VECTOR_OF_F_VECTORS *mixtureVars, 
	     int numMixtures) {
  int                          seed;
  int                          index;
  int                          i, j;
  int                          random;
  float                        rmax;


  seed = GetIAttribute(asdf,"seed");
  srand(seed);
  printf("seed = %d\n",seed);
  fflush(stdout);
  for (i = 0; i < numMixtures; i++) {
      random = rand();
      rmax = RAND_MAX;
      index = (int) ((float) (random/rmax*numVectors));
     for (j = 0; j <vfv[0]->numElements; j++)
      mixtureMeans[i]->array[j] = vfv[index]->array[j];
    for (j = 0; j <vfv[0]->numElements; j++)
      mixtureVars[i]->array[j] = 1.0;
  }
}

/****************************************************************************
 *   Function             : ComputeDiscriminant - computes euclidean distance
 *                        : distance between two vectors
 *   Input args           : mixtureMean, mixtureVar, fvect : input vectors 
 *   Outputs              : ComputeDiscrimnant - distance      	  
 *****************************************************************************/

float ComputeDiscriminant(F_VECTOR *mixtureMean, 
			  F_VECTOR *mixtureVar, F_VECTOR *fvect) {

  int                     i;
  float                   sum = 0;
  static float            scale = 0.0;
  float                   floorValue = 0.0;
  static F_VECTOR         *prevMean = NULL;

  if (prevMean != mixtureMean) {
    for (i = 0; i < fvect->numElements; i++)
      {
	scale = scale+ log(mixtureVar->array[i]);
      } /*  for (..i < fvect->numElements..)  */
  
    scale = scale + log(2.0) + log(PI);
    scale = 0.5*scale;
    prevMean = mixtureMean;
    /*    printf("scale = %f \n", scale);
	  scanf("%*c"); */
  }
    sum = -scale;
  for (i = 0; i < fvect->numElements; i++) 
    floorValue =  floorValue +(mixtureMean->array[i] - fvect->array[i])*
      (mixtureMean->array[i] - fvect->array[i])/
      (2*mixtureVar->array[i]*mixtureVar->array[i]);
  
  sum = sum - floorValue;
 /*  printf("sum = %f\n", sum); */
  return(sum/fvect->numElements);
} 

/****************************************************************************
 *   Function             : DecideWhichMixture - determines index of Mixture
 *                        : to which a given vector belongs
 *   Input args           : fvect : input vector, 
 *                                  mixtureMeans,  mixtureVars
 *   Outputs              : DecideWhichMixture - Mixture index      	  
 *****************************************************************************/

int DecideWhichMixture(F_VECTOR *fvect, VECTOR_OF_F_VECTORS *mixtureMeans, 
		       VECTOR_OF_F_VECTORS *mixtureVars, 
			 int numMixtures) {
  int                     i, j;
  float                   tempDesc;
  int                     index;
  float                   Discriminant;


  Discriminant  = ComputeDiscriminant(mixtureMeans[0], 
				      mixtureVars[0], fvect);
  index = 0;
  for (i = 1; i < numMixtures; i++) {
    tempDesc = ComputeDiscriminant(mixtureMeans[i], 
				   mixtureVars[i], fvect);

    if (tempDesc > Discriminant) {
      index = i;
       Discriminant = tempDesc;
    }
  }
  return(index);
}

/****************************************************************************
 *   Function             : ComputeGMM - compute GMMs
 *                        : for the given set of vectors
 *   Input args           : vfv - input vectors,
 *                        : numVectors - number of input vectors
 *                        : numMixtures - codebook size 
 *                        : numIterations - number of iterations
 *   Outputs		  : mixtureMeans - array of mixture means
 *			  : mixtureVars - array of mixture vars
 *			  : mixtureElemCnt - number of elements in
 *			    each mixture
 *****************************************************************************/



void ComputeGMM(ASDF *asdf, VECTOR_OF_F_VECTORS *vfv, int numVectors, 
		VECTOR_OF_F_VECTORS *mixtureMeans, 
		VECTOR_OF_F_VECTORS *mixtureVars, 
		float *mixtureElemCnt, int numMixtures, int iterations) {
 
  int                            i,j,k;
  static VECTOR_OF_F_VECTORS     *tempMeans, *tempVars;
  int                            mixtureNumber;
  int                            featLength;
  int                            total;
  int                            minIndex, maxIndex;
  int                            minMixtureSize, maxMixtureSize;
  float                          meanValue;
  int                            noTimes;



  featLength = vfv[0]->numElements;
  tempMeans = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  tempVars = (VECTOR_OF_F_VECTORS *) calloc (numMixtures, 
					      sizeof(VECTOR_OF_F_VECTORS));
  for (i = 0; i < numMixtures; i++) { 
    tempMeans[i] = (F_VECTOR *) AllocFVector(featLength);
    tempVars[i] = (F_VECTOR *) AllocFVector(featLength);
  }
  for (i = 0; i < numMixtures; i++) {
    for (j = 0; j < featLength; j++) {
      tempMeans[i]->array[j] = 0;
      tempVars[i]->array[j] = 0;
    }
    mixtureElemCnt[i] = 0;
  }
  printf("temp mixtures allocated\n");
  fflush(stdout);
  InitGMM (asdf, vfv, numVectors, mixtureMeans, mixtureVars, numMixtures);
  minMixtureSize = 0;
  noTimes = 0;
  /*  while((minMixtureSize < 10) && (noTimes < 5)){
      noTimes++; */
    for ( k = 0; k < iterations; k++) {
      printf(" iteration number = %d\n", k);
      fflush(stdout);
      for (i = 0; i < numVectors; i++) {
	mixtureNumber = DecideWhichMixture(vfv[i], mixtureMeans, 
					   mixtureVars, numMixtures);
	mixtureElemCnt[mixtureNumber] = mixtureElemCnt[mixtureNumber]+1;
	for (j = 0; j < featLength; j++){
	  tempMeans[mixtureNumber]->array[j] = 
	    tempMeans[mixtureNumber]->array[j] + vfv[i]->array[j];
	}
      }  
      for (i = 0; i < numMixtures; i++)
	printf("mixElemCnt %d = %f\n", i, mixtureElemCnt[i]);
      scanf("%*c");
      for (i = 0; i < numMixtures; i++) {
	for (j = 0; j < featLength; j++){
	  tempMeans[mixtureNumber]->array[j] = 
	    tempMeans[mixtureNumber]->array[j]/mixtureElemCnt[i];
	}
	mixtureElemCnt[i] = 0;
      }

      
      for (i = 0; i < numVectors; i++) {
	mixtureNumber = DecideWhichMixture(vfv[i], mixtureMeans, 
					   mixtureVars, numMixtures);
	mixtureElemCnt[mixtureNumber] = mixtureElemCnt[mixtureNumber]+1;
	for (j = 0; j < featLength; j++){
	  tempVars[mixtureNumber]->array[j] = 
	    tempVars[mixtureNumber]->array[j] + 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]) * 
	    (vfv[i]->array[j] - tempMeans[mixtureNumber]->array[j]);
	}
      }
	for (i = 0; i < numMixtures; i++)
	  printf("mixElemCnt %d = %f\n", i, mixtureElemCnt[i]);
	scanf("%*c");

      for (i = 0; i < numMixtures; i++){
	for (j = 0; j < featLength; j++) {
	  mixtureMeans[i]->array[j] = tempMeans[i]->array[j];
	  if (tempVars[i]->array[j] == 0) 
	    mixtureVars[i]->array[j] = 1.0;
	  else mixtureVars[i]->array[j] = tempVars[i]->array[j]
	      /mixtureElemCnt[i];	
	  tempMeans[i]->array[j] = 0;
	  tempVars[i]->array[j] = 0;
	  printf("mean %d %d = %f var %d %d = %f\n",i,j, 
		 mixtureMeans[i]->array[j], i, j, mixtureVars[i]->array[j]);
	  fflush(stdout);
	}
	mixtureElemCnt[i] =0;
      }
    }
  for (i = 0; i < numMixtures; i++)
    for (j = 0; j < featLength; j++) {
	printf("mean %d %d = %f var %d %d = %f\n",i,j, 
	       mixtureMeans[i]->array[j], i, j, mixtureVars[i]->array[j]);
	fflush(stdout);
    }

  }

    /*    minIndex = Imin0(mixtureElemCnt, numMixtures);
    maxIndex = Imax0(mixtureElemCnt, numMixtures);
    printf("minIndex = %d maxIndex = %d\n",minIndex, maxIndex);
    minMixtureSize = mixtureElemCnt[minIndex];
    maxMixtureSize = mixtureElemCnt[maxIndex];
    printf("minSize = %d maxSize = %d\n",minMixtureSize, maxMixtureSize);
    fflush(stdout);
    for (j = 0; j < featLength; j++) {
      meanValue = mixtureMeans[maxIndex]->array[j];
      mixtureMeans[minIndex]->array[j] = meanValue +
	3*tempMixtures[maxIndex]->array[j];
      mixtureMeans[maxIndex]->array[j] = meanValue -
	3*tempMixtures[maxIndex]->array[j];
	} */
    /* } */
 










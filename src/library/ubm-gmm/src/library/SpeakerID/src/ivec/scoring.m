function scoring (testname, trainlist, ldamatrix, opname, ubmname, exflag)
  w_test = load (testname);
  tl = readlistascell (trainlist);
  ll = length (tl);
  A = load (ldamatrix);
  ubm = load (ubmname);
  s = zeros (ll,1);
  w_test = w_test - ubm;
  for i = 1:ll
      w_tgt = load (tl {i}) - ubm;
      s (i) = coskernel (A, w_test, w_tgt);
  end
%  mu = mean (s(1:125));
%  sigma = std (s(1:125));
%  s = s - repmat (mu,ll,1);
%  s = s ./ repmat (sigma,ll,1);  
  save (opname, 's', '-ascii')
  id = find (min (s) == s);  
  sprintf ('R35ul7 %s %d', testname, id)
  if exflag ~= 0
    exit
  end

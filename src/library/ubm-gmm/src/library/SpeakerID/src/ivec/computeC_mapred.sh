cmatname=$1
iplist=$2
iveclist=$3
M=$4
D=$5
R=$6
remote=$7
wrkdir=$8
username=`whoami`

corelist=
remotelist=
wrkdirlist=

itemno=0
for item in `echo $remote | sed 's/,/\ /g'`; do
   i1=`echo $item | cut -d '/' -f1`
   i2=`echo $item | cut -d '/' -f2`
   if [ "$i1" == ":" ]; then
     corelist[$itemno]=`parallel --number-of-cores`
   elif [ "$i1" == ":" -a "$i2" != ":" ]; then
     corelist[$itemno]=$i2
   else
     corelist[$itemno]=$i2
   fi
   remotelist[$itemno]=$i1
   ((itemno++))
done
((numremotes=itemno-1))

i=0
j=0
for item in `echo $wrkdir | sed 's/,/\ /g'`; do
  wrkdirlist[$i]=$item
  if [ ${remotelist[$i]} == ":" ]; then
    remotename=localhost
  else 
    remotename=${remotelist[$i]}
  fi
  catlistname_iplist=concatlist.$remotename.iplist
  catlistname_iveclist=concatlist.$remotename.iveclist
  rm -f $catlistname_iplist $catlistname_iveclist
# creating a concatenated list 
  for core_no in `seq 1 ${corelist[$i]}`; do
    cat $iplist.$j >> $catlistname_iplist   
    cat $iveclist.$j >> $catlistname_iveclist
    ((j++))
  done
# transferring the lists
  if [ ${remotelist[$i]} != ":"} ]; then
    scp $catlistname_iplist $catlistname_iveclist $username@${remotelist[$i]}:$item/
  fi
  echo $cmatname |  parallel -W $item -S 1/${remotelist[$i]} 'matlab -nojvm -nodesktop -nosplash -r "computeC ('\'{}.$i\', \'$catlistname_iplist\', \'$catlistname_iveclist\', 1')"' &
  echo $cmatname |  parallel --dry-run -W $item -S 1/${remotelist[$i]} 'matlab -nojvm -nodesktop -nosplash -r "computeC ('\'{}.$i\', \'$catlistname_iplist\', \'$catlistname_iveclist\', 1')"' &
  ((i++))
done
wait

cmatdir=`dirname $cmatname`
rm -f cmatlist
for i in `seq 0 $numremotes`; do
    if [ ${remotelist[$i]} != ":" ]; then
      echo "scp -r $username@${remotelist[$i]}:${wrkdirlist[$i]}/$cmatname.$i $cmatdir/"
      scp -r $username@${remotelist[$i]}:${wrkdirlist[$i]}/$cmatname.$i $cmatdir/
    fi
    echo $cmatname.$i >> cmatlist
done    

seq 1 | parallel --dry-run -j 1 'matlab -nojvm  -nodesktop -nosplash -r "computeC_red ('\'$cmatname\',\'cmatlist\',{}')"'
seq 1 | parallel -j 1 'matlab -nojvm  -nodesktop -nosplash -r "computeC_red ('\'$cmatname\',\'cmatlist\',{}')"'

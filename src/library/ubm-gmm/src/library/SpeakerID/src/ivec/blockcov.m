
function C = blockcov (svec, M, D)
    for m = 1:M
      C{m} = zeros (D);
      s = svec((m-1)*D+1:m*D,1);
      C{m} = s * s';
    end    

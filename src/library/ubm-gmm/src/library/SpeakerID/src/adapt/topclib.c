/*
 * =====================================================================================
 *
 *       Filename:  topclib.c
 *
 *    Description:  library of functions required to compute 
 *                  and order topc scoring mixtures
 *
 *
 * =====================================================================================
 */

#include "topclib.h"

void ReArrangeMixtureIds (topCMixtures *bestCMixtures, int cValue)
  {
     int i,j, tempMixId;
     float tempDist;
     for (i = cValue-1; i>0; i--)
     {
         if (bestCMixtures[i].DistortionValue > 
             bestCMixtures[i-1].DistortionValue)
         {
            tempMixId = bestCMixtures[i].id;
            tempDist  = bestCMixtures[i].DistortionValue;
            bestCMixtures[i].id = bestCMixtures[i-1].id;
            bestCMixtures[i].DistortionValue = bestCMixtures[i-1].DistortionValue;
            bestCMixtures[i-1].id = tempMixId;
            bestCMixtures[i-1].DistortionValue = tempDist;
         }
     }
  }

/******************************************************************************
 * GetTopCMixtures : Given UBM model and feature vectors,this function computes 
 * 		     the id's of mixtures with high likelihoods.
 * Returns	   : It returns top C ids
 ******************************************************************************/ 		  

int **GetTopCMixtures (VFV *ubmModelMeans, VFV *ubmModelVars,
            		      float *ubmWts, unsigned int numClusters, 
                      VFV *vfv, unsigned int numVectors, int cValue, 
                      float probScaleFactor)
{
    int  **mixtureIds, i, j,k;
    float *Dist;
    topCMixtures *bestCMixtures;
    topCMixtures *top ;

    bestCMixtures = (topCMixtures *) calloc (cValue, sizeof(topCMixtures));
    Dist = (float *) calloc (numClusters, sizeof (float));
    top = &bestCMixtures[cValue];

    mixtureIds = (int **) calloc (numVectors, sizeof (int*));
    for (j = 0; j < numVectors; j++)
      {
    	  for (i = 0; i < numClusters; i++)
		      Dist[i] = ComputeProbability (ubmModelMeans[i], ubmModelVars[i], 
                                        ubmWts[i], 
                                        vfv[j], 
                                        probScaleFactor);
        for (k = 0; k < cValue; k++)
        {
          bestCMixtures[k].id = k+1;
          bestCMixtures[k].DistortionValue = Dist[k];
	
          if (k)
            ReArrangeMixtureIds(bestCMixtures, k+1);
        }

        for (k = cValue; k < numClusters; k++)
        {
            if (Dist[k] > bestCMixtures[cValue-1].DistortionValue)
            {
                bestCMixtures[cValue-1].id = k +1;
                bestCMixtures[cValue-1].DistortionValue = Dist[k];
                ReArrangeMixtureIds(bestCMixtures, cValue);
            }
        }
        mixtureIds[j] = (int *) calloc (cValue, sizeof(int));
        for (k = 0; k < cValue; k++)
	        mixtureIds[j][k] = bestCMixtures[k].id;
      }


    return mixtureIds;  
}

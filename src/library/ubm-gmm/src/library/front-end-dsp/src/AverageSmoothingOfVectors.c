/**************************************************************************
 *  $Id: AverageSmoothOfVectors.c,v 1.1 2000/04/23 02:36:13 hema Exp hema $
 *  Release $Name:  $
 *
 *  File:	AverageSmoothOfVectors.c
 *             
 *  Purpose:	Average Smoothes an input signal and writes to a file
 *              
 *
 *  Author:	Hema A Murthy,BSB 307,4364
 *
 *  Created:    Thu Sept 26 2013
 *
 *  Last modified:  Thu 09-Sep-2013 14:35:07 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "string.h"
#include "stdlib.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
/*******************************************************************************
* 	the Following program smoothes a given signal using Average
        Smoothing
*	Inputs :
*	Input Data File -- each corresponds to one Feature Vector
*       numberOfElements -- number of elements in the feature vector
*	Output :
*       Average Smoothed Vector written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : AverageSmoothingOfVector inFile numberOfElements outFile \n");
}
/*****************************************************************************/

        int main (int argc, char *argv[])
{ 
	int  	        numberOfElements;
	float 		*avgVector=NULL, *featureVector=NULL;
        FILE            *inFile=NULL, *outFile=NULL, *temp=NULL;
	int             i,j, numSamples;
        char            str[254], line[200];

       if (argc != 4) {
         Usage();
         exit(-1);
       }
       str[0] = '\0';
       strcat(str,"wc -l ");
       strcat(str, argv[1]);
       strcat(str," | cut -c1-8 > temp");
       system(str);
       temp = fopen("temp","r");
       fscanf(temp,"%d", &numSamples);
       fclose(temp);
       str[0] = '\0';
       strcat(str,argv[2]);
       sscanf(str,"%d",&numberOfElements);
       printf("Number of samples = %d Number of Elements = %d\n",numSamples, numberOfElements);
       avgVector = (float *) AllocFloatArray(avgVector,numberOfElements);
       featureVector = (float *) AllocFloatArray(featureVector,numberOfElements);
        inFile = fopen(argv[1],"r");
       sscanf(argv[2],"%d",&numberOfElements);
       outFile = fopen(argv[3],"w");
         if ((inFile == NULL) || (outFile == NULL)){
	 printf("cannot open files \n");
	 exit(-1);
       }
       for (j = 0; j < numberOfElements; j++)
         avgVector[j] = 0;
       i = 0;
       while (fgets(line, 200, inFile) != NULL) {
	 for (j = 0; j < numberOfElements; j++)
	   fscanf(inFile,"%f",&featureVector[j]);
	 for (j = 0; j < numberOfElements; j++)
	   avgVector[j] = avgVector[j] + featureVector[j];
	 i++;
       }
       for (j = 0; j < numberOfElements; j++)
       fprintf(outFile,"%f\n",avgVector[j]/numSamples);
       fclose(inFile);
       fclose(outFile);
       return 0;
}
/**************************************************************************
 * $Log: AverageSmoothOfVectors.c,v $
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of AverageSmoothOfVectors.c
 **************************************************************************/




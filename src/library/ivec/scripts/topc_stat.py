import sys
import numpy as np
import getopt
import re
#import *
from Utility import *

ipislist = 0
optlist, args = getopt.getopt (sys.argv[1:], "o:d:u:m:lh")
(gmm_file_name, dump_file_name) = sys.argv[1:3]

(nmix, dim, wts, mvec, vvec) = ReadGMM (gmm_file_name)
fvarr = fstream (dump_file_name)

gamma_Nks=[]
non_contibuting_fvecs_indices=[];
for i in range (0,fvarr.nvec):
	meanVarNorm = -0.5 * (fvarr.vfv[i] - mvec) ** 2 / vvec
	l2Norm = np.sum (meanVarNorm,axis=1) #- lvar
	numerator = wts*np.exp(l2Norm)
	if np.sum(numerator) == 0 :
		non_contibuting_fvecs_indices.append(i)
		continue
	gamma_Nk = numerator/np.sum(numerator)
	term1 = np.expand_dims(gamma_Nk,axis=1)
	term2 = np.transpose(np.expand_dims(fvarr.vfv[i],axis=1))
	a = np.matmul( term1, term2 )
#	print a.shape
	gamma_Nks.append(gamma_Nk)

fVectors = np.asarray(fvarr.vfv)
fVectors = np.delete(fVectors,np.asarray(non_contibuting_fvecs_indices), axis=0) 
gamma_Nks = np.asarray(gamma_Nks)
gamma_Nks = np.round(gamma_Nks, decimals=2)
foStats = np.matmul( np.transpose(gamma_Nks), fVectors )

Nks = np.sum(gamma_Nks,axis=0)
aligned_foStats=[]
for i in range(0,nmix):
	aligned_foStat = foStats[i,:] - (Nks[i] * mvec[i,])
	aligned_foStat /= np.sqrt(vvec[i,])
	aligned_foStats.append(aligned_foStat);

aligned_foStats = np.asarray(aligned_foStats)


print str(nmix)+" "+str(dim)
#Nks = np.round(Nks, decimals = 0)
np.savetxt(sys.stdout, Nks, fmt="%1.6e")
np.savetxt(sys.stdout, aligned_foStats, fmt="%1.6e")



#ifndef DSP_LIBRARY_H
#define DSP_LIBRARY_H
#include "FrontEndDefs.h"
#include "FrontEndTypes.h"
float HanW(int i,int npts);
float HanDw(int i,int npts); 
float HamW(int i,int npts);
float HamDw(int i,int npts) ;
float BartW(int i,int npts) ;
float LogAdd (float x, float y);
void Window(float *sig,int npts,char hw,char sw,float gausmin);
void TrapeziumWindow(float sig[],int npts,char taperWin,int winSep);
int Imin(float sig[],int npts);
int Imax(float sig[],int npts);
int FindIndex(int *array, int npts, int index);
int FindMatch(VECTOR_OF_F_VECTORS *vfv, int numVectors, int *array, int npts, int index);
float Median(float *array, int npts);
int Imin0(float sig[],int npts);
int Imax0(float sig[],int npts);
void FindPeaks(float *spectr,int *randPhs,int nfft);
float *FrameCompCepstrum(float *signal, int numPts, float *cepstrum, int numCepstrum, int mfft, int nfft);
void Durbin(double *R,int M,float *A,float *AK,float *E);
void LpAnal(float *Signal,float *E,int Npts,float *A,int Order,float *Gain);
void LogSpectrum(float spectrum[],int npts);
float warp (float fin, float minFrequency, float maxFrequency, float warpConstant);
void Cstore(int n);
float *AllocFloatArray(float *array, int npts);
int *AllocIntArray(int *array, int npts);
void Cfft(complex a[],complex b[],int m,int ,int nsign);
void RCfft(complex a[],complex b[],int m,int ,int nsign);
void Rfft(float *sig,float *ax,float *ay,int mfft,int nfft,int nsign);
void SpectrumReal(int nfft,float *ax,float *ay,float *amag,float *phase);
void SpectrumComplex(int nfft,complex csig[],float *amag,float *phase);
 void RemoveAverage(float *derv,int nfft,float *ave);
void LPSpectrum(float *a,int order,float *mag,float *phase,int npts,int mexp,float gain);
void LogSpectrum(float spectrum[],int npts);
void CepSmooth(float *amag,float *smthAmag,int mfft,int nfft,int winlen,float *co, float gamma);
void ComputeCepstrum (float *amag, float *cepstrum, int mfft, int nfft);
VECTOR_OF_F_VECTORS *GeneratePseudoDct(int offset, int numRows, int numColumns);
float Warp (float fin, float minFrequency, float maxFrequency, float warpConstant); 
float TrapezoidalFilter(float startFrequency, float centerFrequency,float fin, float trapRatio);
F_VECTOR *AllocFVector( int npts);
F_VECTOR *FilterbankEnergyIntegration(ASDF *asdf, float *Spectrum,
				      F_VECTOR *fvect);

float *FrameCompWaveform (short *waveform,float *array, int frameIndex,int frameShift, int frameSize, long samples);
float ComputeEnergy (float *array, int frameSize);
void VoicedUnvoiced(short *waveform, long samples, short *vU, int frameShift, int frameSize); 
float ComputeZeroCrossing(float *array, int frameSize);
float ComputeSpectralFlatness(float *array, float *residual, int frameSize);
void GenerateMelFilters(ASDF *asdf);
void LinearTransformationOfFVector (F_VECTOR *inVect, VECTOR_OF_F_VECTORS *melCepstrumCosineTransform, int numRows, int numCols, F_VECTOR *outVect);
void LinearVectorDifference (F_VECTOR *fvect1, F_VECTOR *fvect2, F_VECTOR *fvect); 
void LinearVectorAddition (F_VECTOR *fvect1, F_VECTOR *fvect2, F_VECTOR *fvect);
void LinearVectorScalarDivide (float scalar, F_VECTOR *fvect1, F_VECTOR *fvect); 
void LinearVectorScalarMultiply (float scalar, F_VECTOR *fvect1, F_VECTOR *fvect); 
float *GroupDelayCepstrum(float *signal,int npts, int nfft, int mfft, 
			  int winlen, float *groupDelayCepstrum);
float *GroupDelay(float *signal,int npts, int nfft, int mfft, int winlen, float *groupDelay);
float *MinGdCepstrum(float *signal,int npts, int nfft, int mfft, 
		     int winlen, float *minGdCepstrum, float gamma);
float *MinGd(float *signal,int npts, int nfft, int mfft, 
	     int winlen, float *minGd, float gamma);
void StdGroupDelay(float *signal,int npts, int nfft, int mfft, 
		   float *groupDelay);
void LPResGroupDelay(float *signal,int npts, int nfft, int mfft,  
		     int order, float *groupDelay);
float *SmoothMagSpectrum(float *signal, int npts, int nfft, int mfft, 
			 int smthWinSize, float *smthMag);
float *ModGdCepstrumNcN(float *signal,int npts,int nfft,int mfft, 
			int winlen, int smthWinSize, float *modGdCepstrum, 
			float alfaP, float alfaN,  
			float gamma, int gdsign, int removeLPhase, 
                        int removeMin,
			int startIndex, int endIndex);
float *ModGdCepstrum(float *signal,int npts,int nfft,int mfft, 
		     int winlen, int smthWinSize, float *modGdCepstrum, 
		     float alfaP, float alfaN, float gamma, int gdsign, 
		     int removeLPhase, int removeMin, int startIndex, int endIndex);
float *ModGdCepstrum_DCT(float *signal,int npts,int nfft,int mfft, 
			 int winlen, int smthWinSize, float *modGdCepstrum, 
			 float alfaP, float alfaN, float gamma, int gdsign, 
			 int removeLPhase, int startIndex, int endIndex, 
			 float scaleDCT);
float *ModGd_DCT(float *signal,int npts,int nfft,int mfft, 
		 int winlen, int smthWinSize, float *modGd, float alfaP, 
		 float alfaN, float gamma, int gdsign, int removeLPhase,
		 int startIndex, int endIndex, float scaleDCT);
float *ModGd(float *signal,int npts,int nfft,int mfft, 
	     int winlen, int smthWinSize, float *modGd, 
	     float alfaP, float alfaN, float gamma, int gdsign, 
	     int removeLPhase, int removeMin, int startIndex, int endIndex);
float *SmoothModGd(float *signal,int npts,int nfft,int mfft, 
		   int winlen, int smthWinSize, float *modGd);
float *StandardModGd(float *signal,int npts,int nfft,int mfft, 
		     int smthWinSize, float *modGd);
float *ModGdCepstrumFromGD(float *modGd,int nfft, int winlen, 
			   float *modGdCepstrum, float alfaP, float alfaN,  
			   float gamma, int gdsign, int removeLPhase, 
			   int startIndex, int endIndex, float scaleDCT);
float *ModGdCepstrumFromStdCepstra(float *inModGdCepstrum,int nfft, int mfft, 
				   int winlen, float *modGdCepstrum, 
				   float alfaP, float alfaN,  
				   float gamma, int gdsign, int removeLPhase, 
				   int startIndex, int endIndex, float scaleDCT);
#endif










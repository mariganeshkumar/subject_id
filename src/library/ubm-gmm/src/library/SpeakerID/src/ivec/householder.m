function [Q,R] = householder(A)

[m, n] = size(A);
if m < n,  error('We need at least as many rows as columns'); end

R = A; Q = zeros(m,n);

for k = 1:n,
  
  x = R(k:m,k);
  e = zeros(length(x),1); e(1) = 1;
  u = sign(x(1))*norm(x)*e + x;
  u = u./norm(u);
  R(k:m, k:n) = R(k:m, k:n) - 2*u*u'*R(k:m, k:n);
  
  if k == 1, 
      Q(:,k) = A(:,k)/R(1,1); 
  else
      Q(:,k) = (A(:,k) - Q(:,1:k-1)*R(1:k-1,k))/R(k,k);
  end
end

R = R(1:n,:); % Reduced QR





  


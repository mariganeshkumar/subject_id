function em_pca_map(oldtmat, map_prefix,Xlist)
%EMPCA Run an EM-based implementation of (probabilistic) PCA
%
%   [mappedX, mapping] = em_pca(X, no_dims)
%
% Performs probabilistic PCA on dataset X in order to reduce its
% dimensionality to no_dims. The dimensionality reduction is performed by
% means of an EM algorithm. The resulting low-dimensional counterpart of X
% is returned in mappedX. Information on the applied mapping (allowing for,
% e.g., out-of-sample extension) is returned in mapping.
%
%

% This file is part of the Matlab Toolbox for Dimensionality Reduction v0.7.2b.
% The toolbox can be obtained from http://homepage.tudelft.nl/19j49
% You are free to use, change, or redistribute this code in any way you
% want for non-commercial purposes. However, it is appreciated if you 
% maintain the name of the original author.
%
% (C) Laurens van der Maaten, 2010
% University California, San Diego / Delft University of Technology


    path (path,'../ivec')
    % Initialize some variables
    iplist = readlistascell (Xlist);
    n = length (iplist);

    % RANDOM FACT:
    % The covariance of the Gaussian is: C = W * W' + sigma2 * eye(D);
    %
    
    % Perform EM iterations
    load (oldtmat, '-mat');                 % loads sigma2 and W
    [D no_dims] = size(W);
    inW = W' * W;                           % (R x R)
            
    % Perform E-step
    invM = inv(inW + sigma2 * eye(no_dims));         % (R x R)
    
    % Perform M-step (maximize mapping W)
    Wp1 = zeros(D, no_dims);                        % (MD x R)
    Wp2 = zeros(no_dims, no_dims);                  % (R  x R)
    Ez_pre = invM * W';
    Ezz_pre = sigma2 * invM;
    sigma2 = 0;                                     % (1  x 1)
    for i=1:n
        try
        [Nx X] = ReadFoStat (iplist{i});            
        Ez = Ez_pre * X;                         % (R x 1)
        Ezz = Ezz_pre + Ez * Ez';             % (R x R)
        Wp1 = Wp1 + X * Ez';
        Wp2 = Wp2 + Ezz;
        sigma2 = sigma2 + (sum(X.*X) - 2 * Ez' * W' * X + trace(Ezz* inW));
        catch
            continue
        end 
    end
    save(map_prefix,'-mat','Wp1','Wp2','sigma2','n');

    exit

function [] = estimate_statistics()
path=genpath('library');
addpath(path);
%eeglab_toolbox = dir([matlabroot,'/toolbox/eeglab*']); %remove all other eeglab toolboxes
%for i = 1:length(eeglab_toolbox)
%    rmpath([matlabroot,'/toolbox/',eeglab_toolbox(i).name]);
%end
%addpath('eeglab/');
%[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
total_subject_count=0;
total_multi_session_count=0;
total_seziure_multi=0;
total_seziure_abn_multi=0;
clean_multi_subject={};
config = configuration;
tuh_data_dir= config.tuh_data_dir;
montages = {'02_tcp_le'}; %get_all_sub_dir([tuh_data_dir, '/edf/']);
for m = 1:length(montages)
    sub_dirs =  get_all_sub_dir([tuh_data_dir, '/edf/', montages{m}]);
    parfor d = 1:length(sub_dirs)
        c_mul_sub={};
        li=0;
        %subjects = dir([tuh_data_dir,...
        %              '/edf/', montages{m},'/'...
        %               sub_dirs{d},'/*.tar.gz']);
        subjects = dir([tuh_data_dir,...
            '/edf/', montages{m},'/'...
            sub_dirs{d},'/*.tar.gz']);
        
        for s = 1:length(subjects)
            try
                total_subject_count = total_subject_count + 1;
                subject_tar=subjects(s).name;
                subject= split(subject_tar,'.');
                subject = subject{1};
                
                %if sum(strcmp(subject_cell, subject))==0
                %    continue;
                %end
                
                mkdir(['/tmp/',subject]);
                untar([tuh_data_dir,...
                    '/edf/', montages{m},'/'...
                    sub_dirs{d},'/',subject_tar...
                    ],['/tmp/',subject])
                
                sessions = get_all_sub_dir(['/tmp/',subject,...
                    '/v1.1.0/',...
                    '/edf/', montages{m},'/'...
                    sub_dirs{d},'/',...
                    subject,'/']);
                if length(sessions) <= 1
                    disp("Skiping Single Session Subject")
                    rmdir(['/tmp/',subject],'s');
                    continue;
                end
                total_multi_session_count = total_multi_session_count+1;
                seizure_sessions = 0;
                abnormal_sessions =0;
                for sess = 1:length(sessions)
                    edf_files = dir(['/tmp/',subject,...
                        '/v1.1.0/',...
                        '/edf/', montages{m},'/'...
                        sub_dirs{d},'/',...
                        subject,'/',...
                        sessions{sess},'/*.edf']);
                    seizure_edf = 0;
                    abnormal_edf =0;
                    for e = 1:length(edf_files)
                        try
                            filename = split(edf_files(e).name, '.');
                            filename = filename{1};
                            seizure_filename = [tuh_data_dir,...
                                '/auto_annotations/seizure/tse/', montages{m},'/'...
                                sub_dirs{d},'/',...
                                subject,'/',...
                                sessions{sess},'/',...
                                filename, '.tse'];
                            try
                                seizure_detials = fileread(seizure_filename);
                                is_seizure = strfind(seizure_detials,'seiz');
                                if ~ isempty(is_seizure)
                                    seizure_edf = seizure_edf+1;
                                    continue;
                                end
                            catch
                            end
                            abnormal_filename = [tuh_data_dir,...
                                '/auto_annotations/abnormal/tse/', montages{m},'/'...
                                sub_dirs{d},'/',...
                                subject,'/',...
                                sessions{sess},'/',...
                                filename, '.tse'];
                            try
                                abnormal_detials = fileread(abnormal_filename);
                                is_abnormal = strfind(abnormal_detials,'abnm');
                                if ~ isempty(is_abnormal)
                                    abnormal_edf = abnormal_edf+1;
                                    continue;
                                end
                            catch
                            end
                        catch ME
                            disp(ME);
                            fId = fopen([tuh_data_dir,'stat_error_files'],'a');
                            fprintf(fId,[tuh_data_dir,...
                                '/edf/', montages{m},'/'...
                                sub_dirs{d},'/',subject,'/',...
                                sessions{sess},'/',...
                                edf_files(e).name, ' ', getReport(ME), '\n']);
                            fclose(fId);
                        end
                    end
                    %disp('edf Stats')
                    %disp([length(edf_files),seizure_edf,abnormal_edf]);
                    if (length(edf_files) - seizure_edf)  == 0
                        
                        seizure_sessions = seizure_sessions +1;
                    end
                    if (length(edf_files) - seizure_edf - abnormal_edf)  == 0
                        
                        abnormal_sessions = abnormal_sessions +1;
                    end
                end
                if length(sessions) - seizure_sessions >1
                    total_seziure_multi = total_seziure_multi+1;
                else
                    disp('Skiping Seizure Data')
                end
                if (length(sessions) - seizure_sessions - abnormal_sessions) > 1
                    total_seziure_abn_multi = total_seziure_abn_multi +1;
                    li = li +1;
                    c_mul_sub{li} = subject;
                else
                    disp('Skiping Abnormal Data')
                end
            catch ME
                disp(ME);
                fId = fopen([tuh_data_dir,'start_error_tar_files'],'a');
                fprintf(fId,[tuh_data_dir,...
                    '/edf/', montages{m},'/'...
                    sub_dirs{d},'/',subject, getReport(ME), '\n']);
                fclose(fId);
            end
            %             disp('Session Stats')
            %             disp([length(sessions),seizure_sessions,abnormal_sessions]);
            %             disp(['Total number of subjects: ', num2str(total_subject_count)]);
            %             disp(['Total number of multi session subjects: ', num2str(total_multi_session_count)]);
            %             disp(['Total number of multi session subjects after SZR: ', num2str(total_seziure_multi)]);
            %             disp(['Total number of multi session subjects after SZR+ABNL: ', num2str(total_seziure_abn_multi)]);
            rmdir(['/tmp/',subject],'s');
        end
        clean_multi_subject{d}=c_mul_sub;
    end
    
end
for d = 1:length(sub_dirs)
    disp(clean_multi_subject{d})
end
disp(['Total number of subjects: ', num2str(total_subject_count)]);
disp(['Total number of multi session subjects: ', num2str(total_multi_session_count)]);
disp(['Total number of multi session subjects after SZR: ', num2str(total_seziure_multi)]);
disp(['Total number of multi session subjects after SZR+ABNL: ', num2str(total_seziure_abn_multi)]);

end


function [] = save_clean_data(filename, current_data, cleanchanlocs)
save(filename,'current_data','cleanchanlocs');
end


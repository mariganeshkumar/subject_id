% Runs em algorithm as mentioned in the paper
% by Rubin and Thayer

function em_fa (tmatprefix, folist, ubmname, M,D,R, numIter, exflag, doInit, startIter)
  path (path,'../ivec/');

  fxlist = readlistascell (folist);
  fx_len = length (fxlist);

  % initialization 
  MD = M * D;
  nrows = MD;
  ncols = R;
  if doInit == 1
      [ws ms vs] = ReadGMM (ubmname, M, D);
      clear ws ms
      vs = sqrt (reshape (vs, nrows,1));
      T = randn (nrows , ncols);
      for r = 1:R
          T (:,r) = T(:,r) ./ vs;
      end
      save(strcat (tmatprefix,int2str(0)),'-mat','T');
      message = 'finished initialization'
      % finished - initialization
  else
      load(strcat (tmatprefix,int2str(0)),'-mat');
  end
  
  tau = ones (MD,1);
  eye_R = eye (R);
  if startIter > 1
      load(strcat (tmatprefix,int2str(startIter-1)),'-mat');
  end
  for iter = startIter:numIter
      message = strcat ('starting iter no ', int2str(iter))
      T_tau = T;
      for r = 1:R
          T_tau(:,r) = T(:,r) ./ tau;
      end
      Tprod = T_tau' * T;
      % E - step
      A = inv (eye_R + Tprod);
      
      C = T - T_tau * A * Tprod;
      A = A * fx_len;
      C1 = zeros(MD,R);
      message = 'into the loop'
      residue = zeros(MD,1);
      parfor i = 1:fx_len
            [Nx fx] = ReadFoStat (fxlist{i});
            y = C' * fx;
            residue = residue + fx .* fx;
            C1 = C1 + (fx * y');
            A = A + y*y';
      end
      message = 'finished looping'
      
      % M - step
      T = C1 * inv (A);
      residue = residue / fx_len;
      C1 = C1 / fx_len;
      tau =1 ./ (residue - (sum(C1 .* T,2)));
      sum (tau)
      message = 'new T-matrix computed'
      save(strcat (tmatprefix,int2str(iter)),'-mat','T');
  end

/*
 *                            COPYRIGHT
 *
 *  bwlp - Butterworth lowpass filter coefficient calculator
 *  Copyright (C) 2003, 2004, 2005 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  info(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

double *trinomial_mult( int n, double *b, double *c );

int main( int argc, char *argv[] )
{
  FILE *fp;
  int i, n;
  double fcd, fca;
  double fc1, fc2;
  double *p;
  double *d;
  double pmr, pmi;
  double ppr, ppi;
  double ppmr, ppmi;
  double d1, d2;
  double ad, bd;
  double *r, *q;
  double bpr, bpi;
  double pbpr, pbpi;
  double sf;

  if( argc < 4 )
  {
    printf("\nbwbp calculates Butterworth bandpass filter coefficients\n");
    printf("\nUsage: bwbp n fc1 fc2 outfile\n");
    printf("  n = order of the filter\n");
    printf("  fc1 = lower cutoff frequency as a fraction of Pi [0,1]\n");
    printf("  fc2 = upper cutoff frequency as a fraction of Pi [0,1]\n");
    printf("  outfile = output file name\n");
    return(-1);
  }

  n = atoi( argv[1] );
  fc1 = M_PI * atof( argv[2] );
  fc2 = M_PI * atof( argv[3] );

  fcd = 0.5 * M_PI;
  fca = tan( fcd / 2.0);
  p = (double *)calloc( 2 * n, sizeof(double) );

  /* Calculate the analog Butterworth coefficients */
  for( i = 0; i < n; ++i )
  {
    p[2*i] = -fca * sin(M_PI*((double)i+0.5)/(double)n);
    p[2*i+1] = fca * cos(M_PI*((double)i+0.5)/(double)n);
  }

  ppmr = 1.0;
  ppmi = 0.0;

  for( i = 0; i < n; ++i )
  {
    pmr = p[2*i] - 1.0;
    pmi = p[2*i+1];
    ppr = p[2*i] + 1.0;
    ppi = p[2*i+1];
    
    d1 = pmr * pmr + pmi * pmi;
    p[2*i] = (ppr * pmr + ppi * pmi) / d1;
    p[2*i+1] = (ppi * pmr - ppr * pmi) / d1;

    d1 = ppmr;
    d2 = ppmi;
    ppmr = -( d1 * pmr - d2 * pmi );
    ppmi = -( d1 * pmi + d2 * pmr );
  }

  ppmr = pow( fca, (double)(2*n) ) / ppmr; /* scaling factor */

  d1 = cos( (fc2 + fc1)/2.0 ) / cos( (fc2 - fc1)/2.0 );
  d2 = tan( fcd / 2.0 ) * cos( (fc2 - fc1)/2.0 ) / sin( (fc2 - fc1)/2.0 );
  ad = 2.0 * d1 * d2 / (d2 + 1.0);
  bd = (d2 - 1.0)/(d2 + 1.0);

  r = (double *)calloc( 2 * n, sizeof(double) );
  q = (double *)calloc( 2 * n, sizeof(double) );  

  pbpr = 1.0;
  pbpi = 0.0;

  for( i = 0; i < n; ++i )
  {
    bpr = 1.0 - bd * p[2*i];
    bpi = -bd * p[2*i+1];
    pmr = bd - p[2*i];
    pmi = -p[2*i+1];
    d1 = bpr * bpr + bpi * bpi;
    r[2*i] = (pmr * bpr + pmi * bpi) / d1;
    r[2*i+1] = (pmi * bpr - pmr * bpi) / d1;
    pmr = ad * (p[2*i] - 1.0);
    pmi = ad * p[2*i+1];
    q[2*i] = (pmr * bpr + pmi * bpi) / d1;
    q[2*i+1] = (pmi * bpr - pmr * bpi) / d1;

    d1 = pbpr;
    d2 = pbpi;
    pbpr = d1 * bpr - d2 * bpi;
    pbpi = d1 * bpi + d2 * bpr;
  }

  pbpr = pow( (bd - 1.0), (double)n ) / pbpr; /* scaling factor */
  sf = ppmr * pbpr;

  d = trinomial_mult( n, q, r );

  fp = fopen(argv[4], "w");
  if( fp == 0 )
  {
    perror( "Unable to open output file" );
    return(-1);
  }

  fprintf( fp, "# Butterworth bandpass filter coefficients.\n" );
  fprintf( fp, "# Produced by bwbp with parameters:\n" );
  fprintf( fp, "# %d  :order of the filter\n", n );
  fprintf( fp, "# %1.15lf  :lower cutoff frequency\n", fc1 );
  fprintf( fp, "# %1.15lf  :upper cutoff frequency\n", fc2 );

  fprintf( fp, "%d\n", 2*n+1 );    /* number of ck coefficient */
  fprintf( fp, "%1.15lf\n", sf );  /* c[0] coefficient */
  fprintf( fp, "%1.15lf\n", 0.0 ); /* c[1] coefficient */

  d1 = -(double)n;
  d2 = 1.0;

  for( i = 1; i <= n; ++i )
  {
    fprintf( fp, "%1.15lf\n", sf*d1/d2 );
    if( i < n ) fprintf( fp, "%1.15lf\n", 0.0 );
    d1 *= -(double)(n - i);
    d2 *= (double)(i + 1);
  }

  fprintf( fp, "%d\n", 2*n+1 );  /* number of dk coefficients */
  fprintf( fp, "1.0\n" );        /* d[0] coefficient */

  for( i = 0; i < 2*n; ++i )
  {
    fprintf( fp, "%1.15lf\n", d[2*i] );
  }

  fclose( fp );
  free( p );
  free( r );
  free( q );
  free( d );

}

/**********************************************************************
  trinomial_mult - multiplies a series of trinomials together and returns
  the coefficients of the resulting polynomial.
  
  The multiplication has the following form:

  (x^2 + b[0]x + c[0])*(x^2 + b[1]x + c[1])*...*(x^2 + b[n-1]x + c[n-1])

  The b[i] and c[i] coefficients are assumed to be complex and are passed
  to the function as a pointers to arrays of doubles of length 2n. The real
  part of the coefficients are stored in the even numbered elements of the
  array and the imaginary parts are stored in the odd numbered elements.

  The resulting polynomial has the following form:
  
  x^2n + a[0]*x^2n-1 + a[1]*x^2n-2 + ... +a[2n-2]*x + a[2n-1]
  
  The a[i] coefficients can in general be complex but should in most cases
  turn out to be real. The a[i] coefficients are returned by the function as
  a pointer to an array of doubles of length 4n. The real and imaginary
  parts are stored, respectively, in the even and odd elements of the array.
  Storage for the array is allocated by the function and should be freed by
  the calling program when no longer needed.
  
  Function arguments:
  
  n  -  The number of trinomials to multiply
  b  -  Pointer to an array of doubles of length 2n.
  c  -  Pointer to an array of doubles of length 2n.
*/

double *trinomial_mult( int n, double *b, double *c )
{
  int i, j;
  double *a;

  a = (double *)calloc( 4 * n, sizeof(double) );

  a[2] = c[0];
  a[3] = c[1];
  a[0] = b[0];
  a[1] = b[1];
  
  for( i = 1; i < n; ++i )
  {
    a[2*(2*i+1)]   += c[2*i]*a[2*(2*i-1)]   - c[2*i+1]*a[2*(2*i-1)+1];
    a[2*(2*i+1)+1] += c[2*i]*a[2*(2*i-1)+1] + c[2*i+1]*a[2*(2*i-1)];

    for( j = 2*i; j > 1; --j )
    {
      a[2*j]   += b[2*i] * a[2*(j-1)]   - b[2*i+1] * a[2*(j-1)+1] + 
                  c[2*i] * a[2*(j-2)]   - c[2*i+1] * a[2*(j-2)+1];
      a[2*j+1] += b[2*i] * a[2*(j-1)+1] + b[2*i+1] * a[2*(j-1)] +
                  c[2*i] * a[2*(j-2)+1] + c[2*i+1] * a[2*(j-2)];
    }

    a[2] += b[2*i] * a[0] - b[2*i+1] * a[1] + c[2*i];
    a[3] += b[2*i] * a[1] + b[2*i+1] * a[0] + c[2*i+1];
    a[0] += b[2*i];
    a[1] += b[2*i+1];
  }

  return( a );
}

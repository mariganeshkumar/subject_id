/* 
 * Purpose: to check if the dumped binary file obtained from
 * ShrinkVfvDump is correct
 *
 * I/P: takes the dump file
 * O/P: o/ps vfv dump file in ascii format
 */


#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>

#define READBYTESTOCHAR(a,b,c,d) for(a = 0; a < b; a++) c[a] = fgetc(d); 
#define finc(i,j) for(i=0;i<j;i++)

void
Usage () {
    printf ("testshrinkvfv -i inputfile -o outputfile\n");
    printf ("-h: to print this help\n");
    return;
}

int
main (int argc, char *argv) {
  float f;
  int dim, nvec,i,j,k;
  FILE *ipf, *opf;
  char oc, *ipfname, *opfname;
  unsigned char *c , *d;

  size_t sizeofint = sizeof (int),
         sizeoffloat = sizeof (float);
    
  while ((oc = getopt (argc, argv, "i:o:")) != -1) {
      switch (oc) {
            case 'i':
                ipfname = optarg;
                break;
            case 'o':
                opfname = optarg;
                break;
            case 'h':
                Usage ();
                exit (1);
                break;
            default:
                Usage ();
                exit (1);
                break;
      }


  
  }
  if (sizeofint != 4) {
    printf ("int size is not 4. Incompatible system\n");
    exit (2);
  }
  if (sizeoffloat != 4) {
      printf ("float size is not 4. Incompatible system\n");
      exit (2);
  }

  ipf = fopen (ipfname, "rb");
  opf = fopen (opfname, "w");

  d = (unsigned char *) &dim;
  finc(k,sizeofint) d[k] = (unsigned char) fgetc (ipf);

  d = (unsigned char *) &nvec;
  finc(k,sizeofint) d[k] = (unsigned char) fgetc (ipf);
  
  
  fprintf (opf, "%d %d\n", dim, nvec);

  d = (unsigned char *) &f;
  for (j = 0; j < nvec; j++) {
      for (i = 0; i < dim; i++) {        
          finc(k,sizeofint)    d[k] = (unsigned char) fgetc (ipf);
          fprintf (opf, " %f", f);
      }
      fprintf (opf,"\n");
  }
  fclose (ipf);
  fclose (opf);
  return 0;
}

# prints dot product between two adapted gmms
import sys
import numpy as np
from Utility import ReadGMM

if len(sys.argv) < 3:
    print "Usage: python dotproduct.py gmm1 gmm2"
    quit()

(n1,d1,w1,m1,v1) = ReadGMM (sys.argv[1])
(n2,d2,w2,m2,v2) = ReadGMM (sys.argv[2])

print np.sum (m1 * m2)

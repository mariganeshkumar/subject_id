% calls the ivecest function and saves the result
% in a file. just didn't want to change the
% existing function.

% exflag is required because the function need not
% be called from command line

function metaivecest (tfname, ffname, wfname, Afname, exflag, listflag)

% accumulate A and C matrixexflag, listflag)
    if nargin < 6
      'Usage : metaivecest (tfname, ffname, wfname, Afname, exflag, listflag)'
      return
    end
    load (tfname,'-mat');
    T = W;
    clear W;
    if listflag == 0
        ii = 1;
        ffl = [ffname];
        wxl = [wfname];
        axl = [Afname];
    else
        ffl = readlistascell (ffname);
        wxl = readlistascell (wfname);
        axl = readlistascell (Afname);
        ii = length (wxl);
    end
    [MD R] = size (T);
    eyeR = eye(R);
    
    for i = 1:ii
        % converting functions to inline
        % [Nx fx] = ReadFoStat  (ffl{i});
        % DELETE THIS BLOCK AND UNCOMMENT PREV LINE
        % TO REVERT TO ORIGINAL CODE
        try
            f_hnd = fopen (ffl{i});
            M = fscanf (f_hnd, '%d', 1);
            D = fscanf (f_hnd, '%d', 1);
            N = fscanf (f_hnd, '%d', M);
            if i==1
              l = ([1:M]-1)*D+1;
              r = [1:M]*D;
            end
            fx = fscanf (f_hnd, '%e', M*D);
            fclose (f_hnd);
            L = eyeR;
            Nidx = find(N>0);
            for j = 1:length(Nidx)             
                  m = Nidx(j);
                  L = L + (N(m) * T(l(m):r(m),:)' * T(l(m):r(m),:));
            end
            
            % converting ivec
            L = inv (L);
            if rcond(L) < 1E-15
                delete(wxl{i},axl{i});
                wxl{i}                
                continue
            end
            w = L * T' * fx;
            save (wxl{i}, 'w', '-ascii');
            L = L + w*w';
            save (axl{i}, 'L', '-mat');
        catch
            continue
        end

    end
    if exflag ~= 0
        exit
    end

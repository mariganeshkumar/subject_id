STATS="/home/hltcoe/sthomas/speaker_id/jfa/features/stats/fdlp"
list="fa_train_eigenchannels.scp"

rm -rf $list

while read line
do

	log=`echo $line | cut -f 1 -d '='`
	phy=`echo $line | cut -f 2 -d '='`

	echo ${log}.fea=${STATS}/$phy.mat >> $list

done < /home/hltcoe/sthomas/speaker_id/jfa/lists/hp_lists/$list


/**************************************************************************
 *  $Id: FFTSpectrum.c,v 1.1 2000/07/24 02:07:25 hema Exp hema $
 *  File:       FFTSpectrum.c - Computes the FFT Spectrum
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 28-Jan-2003 17:31:58 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/BatchProcessWaveform.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average FFT Spectrum 
        from the startFrame to endFrame for a given speech utterance and 
        saves it in 
*	a file. 
*	Inputs :
*	Input data : controlFile, waveFile, SpecFile, startFrame, endFrame
*	Output :
*       Average of the FFT Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : ComputeSpectralFlux ctrlFile waveFile SpecFile Fluxthreshold startFrame endFrame \n");
}
/*****************************************************************************/

int        main (int argc, char *argv[])
{ 

	float           *signal,value,FFTSpecFlux,diff,*Flux,*FluxSmooth,FluxAvg,FluxThreshold;
	int  	        fftOrder, fftSize, frameSize, numFrames, samplingRate,FFTSpecMax,
	                frameNum, frameShift, frameStart, frameEnd,
			fftSizeBy2,medianOrder;
        long            nSamp,i;
        float           *FFTSpec,*PrevFFTSpec, *FFTPhase,sum,prevsum, 
	                *FFTSpecAvg, *FFTPhaseAvg,  
	                *ax, *ay;
	F_VECTOR        *waveform;
        FILE            *specFile=NULL,*controlFile=NULL;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 7) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       specFile = fopen(argv[3],"w");
	
	sscanf(argv[4], "%f", &FluxThreshold);
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       sscanf(argv[5],"%d", &frameStart);
       sscanf(argv[6],"%d", &frameEnd);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       samplingRate  = (int) GetIAttribute(asdf, "samplingRate");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       medianOrder   = (int) GetIAttribute(asdf, "medianOrder");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\t numframes=%d",frameSize, fftOrder, fftSize, frameShift,numFrames);
       signal 	= (float *) AllocFloatArray(signal, fftSize+1);
       FFTSpec  = (float *) AllocFloatArray(FFTSpec, fftSize+1);
       PrevFFTSpec  = (float *) AllocFloatArray(FFTSpec, fftSize+1);
       ax       = (float *) AllocFloatArray(ax, fftSize+1);
       ay       = (float *) AllocFloatArray(ay, fftSize+1);
       FFTSpecAvg  = (float *) AllocFloatArray(FFTSpecAvg, fftSize+1);
       FFTPhase  = (float *) AllocFloatArray(FFTPhase, fftSize+1);
       Flux = (float *) AllocFloatArray(Flux, numFrames);
       FluxSmooth  = (float *) AllocFloatArray(FluxSmooth, numFrames);
       if ((frameStart == -1) && (frameEnd == -1 )){
         frameStart = 0;
	 frameEnd = numFrames - 1;
       }
	FluxAvg=0;
	printf("framestart=%d,frameend=%d",frameStart,frameEnd);
	//fprintf(specFile,"%d %d %d\n",asdf->numSamples,asdf->numFrames,asdf->samplingRate);
       for (frameNum = frameStart; frameNum <= frameEnd; frameNum++)
       {
		// printf (" frameNum = %d\n", frameNum);
	         waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
		 for (i = 1; i <= frameSize; i++)
		{
	   		//printf("waveform %d = %f\n", i-1, waveform->array[i-1]);
		        signal[i] = waveform->array[i-1]*HamDw(i,frameSize);
		       // printf("signal %d = %f\n", i, signal[i]);
		}
	         for (i = frameSize+1; i <= fftSize; i++)
	           signal[i] = 0;
		 Rfft(signal,ax, ay, fftOrder, fftSize, -1);
		 SpectrumReal(fftSize, ax, ay, FFTSpec, FFTPhase);
	
		FFTSpecMax=ImaxActual(FFTSpec,fftSizeBy2);
		FFTSpecMax=FFTSpec[FFTSpecMax];
		/* for (i = 1; i <= fftSize; i++)
		{
	          	 FFTSpec[i] = (FFTSpec[i])/(FFTSpecMax);
			 
		}*/
		
		if(frameNum>0)
		{
			for(i=1,sum=0;i<=fftSizeBy2;i++)
			{
				diff=FFTSpec[i]-PrevFFTSpec[i];
				sum=sum+pow(diff,2);
			}
				FFTSpecFlux=sum;
				Flux[frameNum]=sum;
				FluxAvg=FluxAvg+sum;
				fprintf(specFile,"%d\t%f\n",frameNum,FFTSpecFlux);
		
		}
		else
		{
			FFTSpecFlux=0;
			Flux[frameNum]=0;
			fprintf(specFile,"%d\t%f\n",frameNum,FFTSpecFlux);
		}
	 	for (i = 1; i <= fftSize; i++)
		{
	          	 PrevFFTSpec[i]=FFTSpec[i];
			 
		}
		
	}
	/*FluxAvg= FluxAvg/(frameEnd-frameStart+1);
	 MedianSmoothArray(Flux, frameEnd-frameStart, medianOrder, FluxSmooth);
       for (i = 0; i <= frameEnd-frameStart; i++)
	 
	   fprintf(specFile, "%ld %f\n", i, FluxSmooth[i]);
	 else
	   fprintf(specFile, "%ld %f\n", i, FluxSmooth[i]);*/
       fclose(specFile);

	fclose(controlFile);     
      free(ax);
       free(ay);
       free(FFTPhase);
       free(signal);
       free(FFTSpec);
       free(Flux);
       
       free(FluxSmooth);
       return(0);
}
/**************************************************************************
 * $Log: FFTSpectrum.c,v $
 * Revision 1.1  2000/07/24 02:07:25  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




function filenames=SplitAndWriteData3D(rdTrainDataAreaWise,dir,fileSuffix,noOfFeaturePerFile)
    ind=1;
    filenames = {};
    i=1;
    while ind < size(rdTrainDataAreaWise,3)
         filenames{i}=[dir num2str(ind) '_' fileSuffix '.lfcc'];
         if ind+noOfFeaturePerFile> size(rdTrainDataAreaWise,3)
             noOfFeaturePerFile=size(rdTrainDataAreaWise,3)-ind;
         end
         indices = ind+1:ind+noOfFeaturePerFile;
         mat_file = rdTrainDataAreaWise(:,:,indices);
         save(filenames{i},'mat_file');
         ind=ind+noOfFeaturePerFile+1;
         i=i+1;
    end
end
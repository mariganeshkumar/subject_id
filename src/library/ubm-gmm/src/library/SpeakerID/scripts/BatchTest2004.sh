#!/bin/bash

TESTBINARY=../bin/test2004
CBSIZE=1024
MFOLDER=../models/2004.m.fcd
SPIDMODELS=$MFOLDER/SpidModel
CTRLFILE=../cfiles/fe-ctrl.base
FEATURE=frameCepstrum+frameDeltaCepstrum
TESTLIST=../flists/2004/male.test
GROUNDTRUTH=../flists/2004/GroundTruth
RESULTDIR=$MFOLDER/AllResults
SPKRLIST=../flists/2004/spkrList.m
FINALRESULT=$MFOLDER/finalResult
TEMPFILE=temp
UBMFILE=../ubm/n2003.ubm.m
UBMSIZE=1024
CVAL=15
numSpeakers=`wc -l $SPIDMODELS | cut -d ' ' -f1`
TGMM=../models/tgmm
testUtterance() {
	LOOPTEMPFILE=temp.$1
	testName=`basename $2 | cut -d '.' -f1`
	testPath=$2
	echo "$TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testPath 0.0 0 $UBMFILE $CVAL $TGMM"
  $TESTBINARY $CTRLFILE $SPIDMODELS $FEATURE $numSpeakers $testPath 0.0 0 $UBMFILE $CVAL $TGMM> $LOOPTEMPFILE 
	hypothesized=`grep speaker $LOOPTEMPFILE | awk '{print $5}'`
	speaker=`head -$hypothesized $SPKRLIST | tail -1`
	speaker=`basename $speaker | cut -d '.' -f1`
	echo "$2 $speaker" >> $FINALRESULT
	
	cp $LOOPTEMPFILE $RESULTDIR/$testName.result
  grep Distortion $RESULTDIR/$testName.result | cut -d ' ' -f4 > $RESULTDIR/$testName.result.dist
}

#rm -f $FINALRESULT
#touch $FINALRESULT
mkdir -p $RESULTDIR
numCasesParallel=4
numRunning=0
for testCase in `cat $TESTLIST`; do
  tn=`basename $testCase | cut -d '.' -f1`
  nl=`grep $tn $FINALRESULT | wc -l | cut -d ' ' -f1`
  if [ $nl -gt 0 ]; then
    continue
  fi
	if [ $numRunning -eq $numCasesParallel ]; then
		wait
		numRunning=0
	fi
	if [ $numRunning -lt $numCasesParallel ]; then
		testUtterance $numRunning $testCase &
		((numRunning++))
	fi
done


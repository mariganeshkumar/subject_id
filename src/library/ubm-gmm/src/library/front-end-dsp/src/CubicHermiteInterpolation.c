/*-------------------------------------------------------------------------
 *  CubicHermiteInterpolation.c - Cubic Hermite Interpolation using Berstein polynomials
 *  Version:	$Name$
 *  Module:	
 *
 *  Purpose: Interpolation of undulation points in a Raga	
 *  See:	
 *
 *  Author:	Hema Murthy (hema@bhairavi)
 *
 *  Created:        Mon 10-Mar-2014 15:09:57
 *  Last modified:  Wed 12-Mar-2014 11:19:08 by hema
 *  $Id$
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 -------------------------------------------------------------------------*/
#include "stdio.h"
#include "math.h"

/*
 p(x) = h_{00}(t) p_k = h_10(t) (x_k+1 - x_k) m_k + h_01(t)p_k+1 + h_11(t) (x_k+1 - x_k) m_k+1

t = (x - x_k)/(x_k+1 - x_k) 

h(t) is given by:

h_00(t) = B_0(t) + B_1(t) || 2t^3 -3t^2 +1
h_10(t) = 1/3 B_1(t)      || t^3 - 2t^2 +t
h_01(t) = B_3(t) + B_2(t) || -2t^3 + 3t^2
h_11(t) = -1/3 B_2(t)     || t^3 - t^2

m_k = (p_k+1 - p_k)/2(t_k+1 - t_k) + (p_k - p_k-1)/2(t_k-t_k-1)

*/

/*-------------------------------------------------------------------------
 *  ComputeDerivatives -- Computes the derivatives at each point for Hermite interpolation
 *    Args:the array x and array px, number of points n	
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
void ComputeDerivatives(float *x, float *px, int n, float *dervPx)
{
  int i;
  for (i = 1; i < n - 1; i++)
    dervPx[i] = (px[i] - px[i-1])/(2*(x[i]-x[i-1])) +  (px[i+1] - px[i])/(2*(x[i+1]-x[i]));
  dervPx[0] =  (px[i+1] - px[i])/(2*(x[i+1]-x[i]));
  dervPx[n-1] =  (px[n-1] - px[n-2])/(2*(x[n-1]-x[n-2]));

}	/*  End of ComputeDerivatives		End of ComputeDerivatives   */


/*-------------------------------------------------------------------------
 *  ComputeNCR --  Computes NC_R
 *    Args: n, r	
 *    Returns:	int
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
int ComputeNCR(int n, int r)
{
  float ncr;
  if ((r == 0)||(n == r))
    ncr = 1;
  else if (n < r) 
    ncr = 0;
  else 
    ncr = ComputeNCR(n-1,r-1)+ ComputeNCR(n-1,r);
  return(ncr);

}	/*  End of ComputeNCR		End of ComputeNCR   */

/*-------------------------------------------------------------------------
 *  ComputeBernSteinPolynomials -- Compute Berstein polynomial values for a given value of t and k
 *    Args: t and k	
 *    Returns:	float
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeBernSteinPolynomials(float t, int k)
{
  float bernsteinValue;
  bernsteinValue = ComputeNCR(3, k) *powf(t,k)*powf(1-t, 3-k);
return bernsteinValue;

}	/*  End of ComputeBernSteinPolynomials		End of ComputeBernSteinPolynomials   */


/*-------------------------------------------------------------------------
 *  ComputeInterpolatedValue -- For a given value of xvalue
 *    Args:	
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
float ComputeInterpolatedValue(float *x, float *px, float *dervPx, int n, float xvalue)
{
  int flag=0;
  int i = 0;
  float t;
  while ((!flag) && (i < n)) 
    if (x[i] >= xvalue)
      flag = 1;
    else
      i++;
  if ((i > 0) && (i < n))
    t = (xvalue - x[i])/(x[i+1]-x[i]);
  else if (i == 0)
    t = 0;
  else
    t = 1;
  return((ComputeBernSteinPolynomials(t,0)+ComputeBernSteinPolynomials(t,1))*px[i]+
	 1/3.0*ComputeBernSteinPolynomials(t,1)*dervPx[i]*(x[i+1] -x[i]) +
	 (ComputeBernSteinPolynomials(t,3)+ComputeBernSteinPolynomials(t,2))*px[i+1] +	 
	 -1/3.0*ComputeBernSteinPolynomials(t,2)*(x[i+1]-x[i])*dervPx[i+1]);
}	/*  End of ComputeInterpolatedValue		End of ComputeInterpolatedValue   */

/*-------------------------------------------------------------------------
 *  Interpolate -- For a given set of x values in a interval, return the interpolated values.
 *    Args:x1, x2, y1, y2, yp1, yp2 (yps are the derivatives at the 
      endpoints of the interval)	
 *    Returns:	array y
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/

void Interpolate (float x1, float x2, float y1, float y2, float yp1, float yp2, float *x, float *y, int n) {
  float a,b,c,d;
  float deltax, deltay;
  int i;
  a = y1;
  b = yp1;
  deltax = x2 - x1;
  deltay = y2 - y1;
  c = (deltay/deltax - yp1)/deltax;
  d = (yp1+yp2-2*deltay/deltax)/(deltax*deltax);
  for (i = 0; i < n; i++)
    y[i] = a + (x[i] - x1)*(b +(x[i] - x1)*(c + d*(x[i] - x2)));
}			 
/*-------------------------------------------------------------------------
 *  main -- Main Program
 *    Args:	
 *    Returns:	Nothing
 *    Throws:	
 *    See:	
 *    Bugs:	
 -------------------------------------------------------------------------*/
void main()
{
  float x[10], x1[20], y1[20];
  float px[10];
  float dervPx[10], dervDervPx[10];
  float xvalue, yvalue;
  int i, k;
  FILE *outFile, *inputFile;
  outFile = fopen ("IntValues.txt", "w");
  inputFile = fopen("Input.txt", "w");
  /*for (i = 0; i < 10; i++) {
    x[i] = i;
    if ((i % 2) == 0)
      px[i] = 1;
    else
      px[i] = -1;
      }*/
  for (i = 0; i < 10; i++) {
    x[i] = i;
    px[i] = sin(i);
  }
  ComputeDerivatives(x, px, 10, dervPx);
  ComputeDerivatives(x, dervPx, 10, dervDervPx);
  for (i = 0; i < 10; i++)
    fprintf(inputFile, "%f %f %f %f\n", x[i], px[i], dervPx[i], dervDervPx[i]);
  
  for (k = 0; k < 9; k++) {
    for (i = 0; i < 20; i++)
      x1[i] = x[k] + (x[k+1] - x[k])/20*i;
  /*    xvalue = 0;
    while (xvalue <= 3.5) {
      yvalue = ComputeInterpolatedValue(x, px, dervPx, 10, xvalue);
      fprintf(outFile, "%f %f\n", xvalue, yvalue);
      xvalue = xvalue+0.01;
      }*/
    Interpolate (x[k], x[k+1], px[k], px[k+1], dervPx[k], dervPx[k+1], x1, y1, 20);
    for (i = 0; i < 20; i++)
      fprintf(outFile, "%f %f\n", x1[i], y1[i]);
  }
  fclose(inputFile);
  fclose (outFile);
}	/*  End of main		End of main   */


/*-------------------------------------------------------------------------
 * $Log$
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of CubicHermiteInterpolation.c
 -------------------------------------------------------------------------*/

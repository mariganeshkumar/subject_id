#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Riff.h"

//===================== Main Function =====================//

int main( int argc, char *argv[] )
	{
	//============== Declarations ==================//
	RIFFWaveFile *myRIFF = NULL;
	FILE *iFile = NULL;
	FILE *oFile = NULL;
	FILE *outFile = NULL;

	if( argc < 3 ) 
		{
   		fprintf(stderr,"Usage:RIFFReader <Input Wave File> <Text File>\n");
		exit(1);
		}
	if( (iFile = fopen( argv[1],"rb" )) == NULL ) 
		{
		fprintf( stderr, "Error Opening the File\n" );
		exit(1);
		}
	if( (oFile = fopen( argv[2],"w" )) == NULL ) 
		{
		fprintf( stderr, "Error Opening the File\n" );
		exit(1);
		}

	myRIFF = (RIFFWaveFile *)malloc(sizeof(RIFFWaveFile));
	myRIFF->header = (RIFFHeader *)malloc(sizeof(RIFFHeader));

	//============== Process ====================//
	if ( ReadRIFFWaveFile( iFile, myRIFF ) == SUCCESS ) 
		{
  		//PrintRIFFWaveFileInfo( oFile, myRIFF );	
		}
  		PrintRIFFWaveFileInfo( oFile, myRIFF );	

	fclose( oFile );
	fclose( iFile );

	if( (outFile = fopen("temp.wav","wb")) == NULL)
		{
		fprintf(stderr, "Error writing the file\n");
		exit(1);
		}
	
	if( WriteWaveform(myRIFF->data, myRIFF->header->dataChunkDataSize, outFile) == SUCCESS )
		{
		fprintf(stdout, "Temporary wavefile temp.wav is created\n");
		}

	//============ write file ===================//
	
	
	return 0;
	}
//=================== End of Main ========================//


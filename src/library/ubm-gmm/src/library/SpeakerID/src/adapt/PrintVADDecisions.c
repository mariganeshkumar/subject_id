#include "CommonFunctions.h"


main (int argc, char *argv[])
  {
    FILE *controlFile;
    ASDF *asdf;
    int i,numSpeechVectors;
    unsigned int *idx;
    float *ergWts = NULL;
    VFV *ergMeans = NULL, *ergVars = NULL;

    if (argc != 4) 
      {
        printf ("Usage: %s ctrlfile wavfile trigaussian\n", argv[0]);
        exit(1);
      }

    controlFile = fopen (argv[1], "r");
    asdf = (ASDF *) calloc (1, sizeof (ASDF));
    InitializeStandardFrontEnd (asdf, controlFile);
    Cstore (asdf->fftSize);
    fclose(controlFile);
    idx = GetVADDecisionsTGMM(asdf, argv[2], &numSpeechVectors, 
                             argv[3]);
    printf ("numSpeechVectors = %d\n", numSpeechVectors);
    finc(i,numSpeechVectors,1) 
      {
        printf ("%d\n", idx[i]);
      }
    return 0;
  }

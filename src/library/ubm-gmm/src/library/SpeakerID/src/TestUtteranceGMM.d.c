#include "CommonFunctions.h"


void Usage() {
   printf (" Usage : TestUtteranceGMM ctrlFile modelFile numSpeakers wavName\n");
}



int main(int argc, char *argv[]) {
  

  FILE                  *cFile=NULL,*modelFile=NULL,
                        *speakerFile=NULL;
  char                  *cname=NULL, *wavname =NULL, *models = NULL, 
                        *featureName = NULL;
  char                  *string1=NULL, *string2=NULL;
  char                  speakerModel[500],line[500];
  int                   numSpeakers, *numClusters;
  VECTOR_OF_F_VECTORS   **speakerModelMeans, **speakerModelVars, *speakerModelWts;
  ASDF                  *asdf;
  VECTOR_OF_F_VECTORS   *vfv;
  int                   i, j, k;
  int                   featLength;
  unsigned long         numVectors;
  int                   verify = 0;
  float                 *Distortion;
  float                 thresholdScale;
  if (argc != 5) {
    Usage();
    exit(-1);
  }
  cname = argv[1];
  models = argv[2];
  modelFile = fopen(models,"r");
  //  string1 = argv[3];
  //sscanf(string1,"%d",&numClusters);
  string2 = argv[3];
  sscanf(string2,"%d",&numSpeakers);
  wavname = argv[4];
  cFile = fopen(cname,"r");
  asdf = (ASDF *) malloc(sizeof(ASDF));
  InitializeStandardFrontEnd(asdf,cFile);
  Cstore(asdf->fftSize);
  vfv = (VECTOR_OF_F_VECTORS *) ReadVfvFromFile(wavname, &numVectors);
  printf ("numVectors = %d\n", numVectors);
  featLength = vfv[0]->numElements;
  speakerModelMeans = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelVars = (VECTOR_OF_F_VECTORS **) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS *));
  speakerModelWts = (VECTOR_OF_F_VECTORS *) calloc(numSpeakers,sizeof(VECTOR_OF_F_VECTORS ));
  numClusters = (int *) calloc (numSpeakers, sizeof(int));
  i = 0;
  while (fgets(line,500,modelFile)) {
    sscanf(line,"%s %d",speakerModel, &numClusters[i]);
    speakerFile = fopen(speakerModel,"r");
    speakerModelMeans[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelVars[i] = (VECTOR_OF_F_VECTORS *) calloc(numClusters[i], 
    sizeof(VECTOR_OF_F_VECTORS));
    speakerModelWts[i] = (F_VECTOR *) AllocFVector(numClusters[i]);
    for (j = 0; j < numClusters[i]; j++) {
      speakerModelMeans[i][j] = (F_VECTOR *) AllocFVector(featLength);

      speakerModelVars[i][j] = (F_VECTOR *) AllocFVector(featLength);
    }
    for (j = 0; j < numClusters[i]; j++) {
      fscanf(speakerFile,"%f",&speakerModelWts[i]->array[j]);
      for(k = 0; k < featLength; k++) {
        fscanf(speakerFile,"  %f %f",&speakerModelMeans[i][j]->array[k],
	       &speakerModelVars[i][j]->array[k]);       
      }      
    }
    i++;  
    fclose(speakerFile);
  }
  Distortion = (float *)calloc(numSpeakers, sizeof(float));
  Distortion = (float *) ComputeLikelihood(vfv, (unsigned int) numVectors,
					   numSpeakers, speakerModelMeans,
					   speakerModelVars, speakerModelWts,
					   numClusters, Distortion,
					   asdf->probScaleFactor);
  printf("%s\n",wavname);
  if (!verify) 
    for (i = 0; i < numSpeakers; i++)
    printf("%d %f\n", i, Distortion[i]);
  else 
    for (i = 0; i < numSpeakers-1; i++)
      printf("%d %f  %f\n",i, Distortion[i], Distortion[i] - Distortion[numSpeakers-1]);
  if (!verify)
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers)+1);
  else
    printf("%200s identified as speaker %5d\n",wavname,
                Imax0Actual(Distortion,numSpeakers-1)+1);
  free(vfv);
  free(Distortion);
  for (i = 0; i < numSpeakers; i++) {
    free (speakerModelMeans[i]);
    free (speakerModelVars[i]);
    free (speakerModelWts[i]);
  }
  return(0);
  }












/*-------------------------------------------------------------------------
 * $Log: TestUtteranceGMM.c,v $
 * Revision 1.2  2008/03/12 17:48:43  hema
 * Fixed the length of filename
 *
 * Revision 1.1  2002/04/30 09:36:19  hema
 * Initial revision
 *
 * Revision 1.1  2001/11/06 13:28:03  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %02d-%3b-%:y %02H:%02M:%02S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of TestUtterance.c
 -------------------------------------------------------------------------*/

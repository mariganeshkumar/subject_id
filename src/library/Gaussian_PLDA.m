function [scores,m,W,Phi,Sigma] = Gaussian_PLDA(dev,dev_spk_idx,mod,tst,nPhi,flag_lnorm)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function implements a simplified PLDA system to model i-vectors: 
%
%               i-vector = m + Phi*beta + epsilon
%
% Length normalization is applied to all i-vectors (unless flag_lnorm = 0).
% The two steps of the length normalization are whitening and then
% projection into unit sphere. We always apply the whitening but the
% projection is controled by the optional input parameter: "flag_lnorm".
% The noise term has full-covariance.
%
% USAGE:
% [scores,m,W,Phi,Sigma] = Gaussian_PLDA(dev,dev_spk_idx,mod,tst,nPhi[,flag_lnorm = 1]); 
%
% INPUT: 
%
%   - dev: A matrix of dimension dim x ndev (i.e., one column per i-vector)
%          with the development i-vectors to train the PLDA model
%          parameters {m,W,Phi,Sigma}. It assumes that all the i-vectors
%          from the same speaker are consecutive. This is very important
%          because the input variable "dev_spk_idx" only specifies the
%          number of i-vectors per speaker.
%
%   - dev_spk_idx: Vector of dimension nspk x 1. This vector provides the
%                  labels for the development data. For example, if there 
%                  are three speakers in the dev. data and each has 2, 3 and
%                  5 i-vectors respectively, then the variable is a column
%                  vector: dev_spk_idx = [2 3 5]';
%                  Note that: sum(dev_spk_idx) == ndev;
%
%   - mod: A matrix of dimensions dim x nmod (i.e., one column per i-vector)
%          This matrix contains the i-vectors of the model speech segments
%
%   - tst: A matrix of dimensions dim x ntst (i.e., one column per i-vector)
%          This matrix contains the i-vectors of the test speech segments.
%
%   - nPhi: Number of columns of the speaker-specific subspace.
%
%   - flag_lnorm: Flag variable (either set to 0 or 1) indicating if we 
%                 want length normalization to be applied to all the 
%                 i-vectors. This parameter is optional and the default
%                 value is true (1).
%
% OUTPUT:
%
%   - scores: Matrix of scores between all mod i-vectors and tst i-vectors.
%             The dimensions of the matrix are nmod x ntst.
%
%   - m: global mean of the development data (i.e., mean(dev,2));
%
%   - W: Whitening transformation learned from development data such that
%        cov(W*dev) = I;
%
%   - Phi: Basis for speaker-specific subspace (i.e., eigenvoices)
%
%   - Sigma: Full-covariance of noise term in PLDA model
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programmed by Daniel Garcia-Romero (dgromero@jhu.edu). 
% Last modified 2/1/2012
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%
% Please reference the following paper if you use this software:
%
%   D. Garcia-Romero and C. Y. Espy-Wilson, "Analysis of I-vector Length
%   Normalization in Speaker Recognition Systems," in Proceedings of Interspeech,
%   Florence, Italy, August 2011
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%Initialize random seed
seed1 = RandStream.create('mt19937ar','seed',5489);
RandStream.setGlobalStream(seed1);

%Debugging flags
flag_monitor_LL = 1;

%HARD CODED CONFIGURATION
SMALL_INC = 0.1; % Small increment in log Marginal Likelihood to stop EM
var_floor = 1e-7; % Floor for noise variance
flag_diag = 0;
flag_MD = 1;
niter = 100;
LML_correction_term = 0;
fid = 1;


if nargin < 5
    error('USE: [scores,m,W,Phi,Sigma] = Gaussian_PLDA(dev,dev_spk_idx,mod,tst,nPhi[,flag_lnorm = 1]);');
end

if nargin < 6
    flag_lnorm = 1;
end

[dim,ntot] = size(dev);
nspk = size(dev_spk_idx,1);

if sum(dev_spk_idx) ~= ntot
    error('The number of columns of dev (ndev) must satisfy: sum(dev_spk_idx) == ndev');
end

%Compute the mean from development set and remove
fprintf(fid,'Whitening the development data ...\n');
m=mean(dev,2);           
dev = bsxfun(@minus,dev,m);
SX = cov(dev');
[V,D]=eig(SX);
W = diag(1./sqrt(diag(D)))*V';
%dev = W*dev;

if flag_lnorm
    fprintf(fid,'UNIT CIRCLE normalizing development data ...\n');    
    X_nrm = sqrt(sum(dev .* dev));
    dev = bsxfun(@rdivide,dev,X_nrm);   
    clear X_nrm;
end

fprintf(fid,'******************      DEVELOPMENT       ********************************************\n');
fprintf(fid,' dim: %i; nPhi: %i; nspk: %i; ntot_files: %i;\n',dim,nPhi,nspk,ntot);
fprintf(fid,'**************************************************************************************\n');    

fprintf(fid,'\n Training Phi and Sigma from development data...\n');
tic;
[Phi,Sigma] = EM_train_SPLDA_model(dev,dev_spk_idx,nPhi,niter,flag_monitor_LL,flag_MD,SMALL_INC,fid,var_floor,flag_diag,LML_correction_term);
toc;

%Removing development data
clear dev dev_spk_idx;

%Whitening eval data using dev set statistics
fprintf(fid,'Whitening evaluation data using dev set statistics ...\n');
mod = bsxfun(@minus,mod,m);
tst = bsxfun(@minus,tst,m);
%mod = W*mod;
%tst = W*tst;    

if flag_lnorm
    fprintf(fid,'UNIT CIRCLE normalizing evaluation data ...\n');
    m_nrm = sqrt(sum(mod .* mod));
    t_nrm = sqrt(sum(tst .* tst));        
    mod = bsxfun(@rdivide,mod,m_nrm);
    tst = bsxfun(@rdivide,tst,t_nrm);
    clear m_nrm;
    clear t_nrm;
end


%     %Verification
%     fprintf(fid,'Start computing PLDA scores...\n');        
%     tic;
%     scores2 = FAST_SPLDA_verification(Phi,Sigma, mod, tst);
%     toc;
%     fprintf(fid,'Finished computing scores...\n');
    
    
    %Verification
    fprintf(fid,'Start computing PLDA scores...\n');        
    tic;
    scores = FAST_SPLDA_verification_direct_Log_LR(Phi,Sigma,mod,tst);    
    toc;
    fprintf(fid,'Finished computing scores...\n');



end


%SUBS

function [Phi,Sigma] = EM_train_SPLDA_model(dev,dev_spk_idx,nPhi,niter,flag_monitor_LL,flag_MD,SMALL_INC,fid,var_floor,flag_diag,LML_correction_term)

%Smallest inc
%SMALL_INC = 1000;
%Get dimensions
[dim,ntot] = size(dev);
Nsum = sum(dev_spk_idx);
nspk = length(dev_spk_idx);
nlist = unique(dev_spk_idx); %Create the list of unique zeroth order stats
is_first= 1; %variable used to prevent error in first selected iter to compute LL

%Compute dev data energy
eX = (norm(dev,'fro')^2)/ntot;
fprintf(fid,'Average energy dev. data i-vectors: %f\n',eX);

% %Initialize results
% Phi = zeros(dim,nPhi);
% Sigma = zeros(dim,dim);

%Corrected likelihood to make fare comparison with the unit norm case. When
%we normalize to unit norm there is no correction. When we don't normalize
%I correct the LML as if we had done the scaling
total_LL = zeros(niter+1,1) + LML_correction_term;

Phi = (randn(dim,nPhi)/sqrt(dim*nPhi))*sqrt(1/4 * eX);

%Sigma = diag(abs(randn(dim,1)))/sqrt(dim*(2/eX));
%Sigma = diag(abs(randn(dim,1)).^2)/(dim*((4/3)/eX));
Sigma = (eye(dim)/dim)*(3/4 * eX);

%Preprocessing to speed up
%Xtilda (First order suf. stats.)
Xtilda = zeros(dim,nspk);

ie = 0;
for i = 1:nspk
    ib = ie + 1;
    ie = ib + dev_spk_idx(i) - 1;
    Xtilda(:,i) = sum(dev(:,ib:ie),2);
end

%Posterior means
H = zeros(nPhi,nspk);
eye_nF = eye(nPhi);

%Start EM iterations
for iter = 1:niter
    
    %Matrix for the M-step
    R2 = zeros(nPhi);
    R_MD = zeros(nPhi);
    
    %Update estimates from previous iteration
    F = Phi;    
    E = Sigma;
    
    %Precompute iPn=chol(I + n*F'*inv(E)*F) for each unique dev_spk_idx    
    [iPn,iE,log_det_Pn,log_det_E,iEF] = precompute_matrices(F,E,nlist);
       
    %Precompute F'*iE*Xtilda 
    FtiEXtilda = iEF'*Xtilda;
                
    %********************
    %E-step:
    %********************
    ie = 0;
    for i = 1:nspk
        ib = ie + 1;
        ie = ib + dev_spk_idx(i) - 1;
        %Select the correct matrix based on the number of vectors for
        %current speaker
        index_n = find(nlist == dev_spk_idx(i));        
        iP = iPn(:,:,index_n);
                
        H(:,i) = iP*FtiEXtilda(:,i); %Posterior means
                
        R2 = R2 + dev_spk_idx(i)* iP;%Weigthed sum of posterior covariances
        
        R_MD = R_MD + iP;%MD 
                        
        if ~mod(iter,flag_monitor_LL)
            log_det_P = log_det_Pn(index_n);            
            total_LL(iter) = total_LL(iter) + chol_PLDA_Log_Likelihood_precomputed_matrices(dev(:,ib:ie),Xtilda(:,i),iP,log_det_P,log_det_E,iEF,iE);                                               
        end
    end
    if ~mod(iter,flag_monitor_LL)
        total_LL(iter) = total_LL(iter) / nspk;
    end
    %Monitor the progress in the Log Marginal Likelihood and subspace
    %distance
    if ~mod(iter,flag_monitor_LL)       
        if is_first            
            tmp_inc = 0;
            is_first = 0;
        else
            tmp_inc = total_LL(iter) - total_LL(iter-flag_monitor_LL);
        end
      
        if tmp_inc < 0
            fprintf(fid,'Warning decrease in total Log Mariginal Likelihood\n');
        end
                
        eF = norm(F,'fro')^2;
        eE = trace(E);
                                
        fprintf(fid,'Iteration %i:\t LML:%8.2f\t INC:%8.2f\t energy Phi:%6.4f\t energy E:%6.4f\n',iter-1,total_LL(iter),tmp_inc,eF,eE);    
        %hold off; plot(vE); drawnow;

                
        if tmp_inc < SMALL_INC && iter > 10
            fprintf(fid,'Stopping the EM due to small increment in LML (< %i)\n',SMALL_INC);
            if tmp_inc > 0 %Only return the last update if it increases likelihood
                Phi = F;            
                Sigma = E;                                                     
            end
            
            break;
        end       
    end
                       
    %********************
    %M-step
    %********************            
    Hn= bsxfun(@times,H,dev_spk_idx'); 
    R2 = Hn*H' + R2;
    
    %Update F  
    Bt = Xtilda*H';
    Phi = Bt / R2;           
    
    %Update E
    Sigma = (dev*dev' - Phi*Bt')/ Nsum;
    
    if flag_diag        
        Sigma = diag(max(diag(Sigma),var_floor));        
    end
                   
    %********************
    %MD iteration
    %********************
    if flag_MD          
         R_MD = (R_MD + H*H')/nspk;
         Phi = Phi*chol(R_MD,'lower');             
    end
    
                              
end %iter

    if fid ~= 1
        %fprintf(fid,'Iteration %i:\t MLL:%g\n',iter-1,total_LL(iter)); 
        fprintf(fid,'Iteration %i:\t LML:%8.2f\t INC:%8.2f\t eF:%6.4f\t eE:%6.4f\n',iter-1,total_LL(iter),tmp_inc,eF,eE);    
    end
    
%     if flag_diag        
%         %Sigma = diag(max(diag(Sigma),var_floor));        
%         Sigma = diag(diag(Sigma));        
%     end                     
end

function [iPn,iE,log_det_Pn,log_det_E,iEF] = precompute_matrices(F,E,nlist)
    
    nPhi = size(F,2);
    num_unique = length(nlist);
    eye_nF = eye(nPhi);
    
    iPn = zeros(nPhi,nPhi,num_unique);    
    log_det_Pn = zeros(num_unique,1);
    
    %Invert the noise covariance matrix
    iE = chol(E);
    log_det_E = 2*sum(log(diag(iE)));
    
    %Compute iEF
    iE = iE\(iE'\eye(size(E,1)));
    iEF = iE*F;
    %iEF = iE\(iE'\F);
    FtiEF = F'*iEF;
    
    %For each value of n compute iPn
    for i = 1:num_unique
        n = nlist(i);       
        iPn(:,:,i) = chol(eye_nF + n*FtiEF);
        log_det_Pn(i) = 2*sum(log(diag(iPn(:,:,i))));
        iPn(:,:,i) = iPn(:,:,i)\(iPn(:,:,i)'\eye_nF);
    end

end

function chol_LL = chol_PLDA_Log_Likelihood_precomputed_matrices(data,sum_data,iPn,log_det_Pn,log_det_E,iEF,iE)

%This function is called for each speaker
%data contains all the ivectors for the spekaer
%sum_data contains the first order suff stat
[dim,n] = size(data);
t_dim = n*dim;

%Precompute intermediate variables
FtiEsum_data = iEF'*sum_data;

iEdata = iE*data;

chol_LL = -t_dim/2*log(2*pi) - n/2*log_det_E - 0.5*log_det_Pn - 0.5*sum(sum(data.*iEdata)) + 0.5*(FtiEsum_data'*(iPn*FtiEsum_data));

end

function scores = FAST_SPLDA_verification_direct_Log_LR(F,E,mod,tst)

    [dim,nspk] = size(mod);
    [dim,tot_files] = size(tst);
        
    %Precompute matrices    
    [iPn,iE,log_det_Pn,log_det_E,iEF] = precompute_matrices(F,E,[1 1 2]);
                       
    y_mod = iEF'*mod;                                                    
    y_tst = iEF'*tst;

    Q1 = (iPn(:,:,3)-iPn(:,:,2)) / 2;
    Q2 = (iPn(:,:,3)-iPn(:,:,1)) / 2;

    %Compute the full matrix of scores
    nrm2_train = sum(y_mod.*(Q1*y_mod))';
    nrm2_test = sum(y_tst.*(Q2*y_tst));
    scores = repmat(nrm2_train,1,tot_files) + repmat(nrm2_test,nspk,1) + (y_mod'*(iPn(:,:,3)))*y_tst; 

    %Add the normalization term of the determinants to produce log_LR
    log_det_term = (log_det_Pn(1) + log_det_Pn(2) - log_det_Pn(3))/2;            
    scores = scores +  log_det_term;


end


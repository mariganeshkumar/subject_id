#include "fe/constants.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
void Usage () {
  printf ("TestCentLinearScale ctrfile outputFile FBfile \n");
}
int main(int argc, char **argv) {
  FILE                  *cFile = NULL, *outFile = NULL, *FBfile = NULL;
  char                  *filterBankName =NULL, 
                        *cname = NULL, *outfileName = NULL;
  ASDF                  *asdf;
  int                   i, j, k;
  float                 centFrequency;

    if(argc != 4) {
      Usage();
      exit(-1);
    }
    cname = argv[1];
    outfileName = argv[2];
    filterBankName = argv[3];
    outFile = fopen(outfileName,"w");
    cFile = fopen(cname, "r");
    FBfile = fopen(filterBankName, "w");
    asdf = (ASDF *) malloc(1*sizeof(ASDF));
    InitializeASDF(asdf);
    InitializeStandardFrontEnd(asdf,cFile);
  if (asdf->zeroMean == 1) {
    for (i = (int) asdf->minFrequency; i <= (int) asdf->maxFrequency; i++) {
      centFrequency = ConvertFreqToCent ((float) i, asdf->tonic);
      fprintf (outFile, "%f %f\n", (float)(i), centFrequency);
    }
  } else if (asdf->zeroMean == 0) {
    for (i = (int) asdf->minFrequency; i <= (int) asdf->maxFrequency; i++) {
      centFrequency = ConvertCentToFreq ((float) i, asdf->tonic);
      fprintf (outFile, "%f %f\n", (float)(i), centFrequency);
    }
  }
  for (i = 0; i < asdf->numFilters; i++) {
    for (j = asdf->dftIndices[i]; j < asdf->filterbankWeights[i]->numElements+asdf->dftIndices[i]; j++)
      fprintf(FBfile, "%d %f\n", j, asdf->filterbankWeights[i]->array[j-asdf->dftIndices[i]]);
  }
  fclose(outFile);
  fclose(FBfile);
  return((int) (0));
}

#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: LPSpectrum.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	LPSpectrum.c - Computes the LP Spectrum
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 11-Oct-2011 10:58:55 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average LP Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the LP Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : LPSpectrum ctrlFile waveFile SpecFile frameStart frameEnd\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

	float           *signal=NULL, *residual=NULL;
	int  	        i, fftOrder, fftSize, fftSizeBy2, frameSize, numFrames, numVoicedFrames = 0,
	                frameNum,lpOrder, frameShift, 
                        frameStart, frameEnd;
        int             samplingRate;
        float           *LPSpec=NULL, *LPPhase=NULL, *LPSpecAvg=NULL, *LPGdSpec=NULL, *LPGdSpecAvg=NULL,  
	                gain, *coef=NULL;
	F_VECTOR        *waveform=NULL;
        FILE            *specFile=NULL, *controlFile=NULL;
        static ASDF     *asdf=NULL;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       lpOrder   = (int) GetIAttribute(asdf, "lpOrder");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       printf ("frameSize = %d fftOrder = %d fftSize = %d lpOrder = %d frameShift = %d numFrames=%d\n", 
	       frameSize, fftOrder, fftSize, lpOrder, frameShift, numFrames);
       sscanf(argv[4], "%d", &frameStart);
       sscanf(argv[5], "%d", &frameEnd);
       specFile = fopen(argv[3],"w");
       Cstore(fftSize);
       fftSizeBy2 = fftSize/2;
       signal = (float *) AllocFloatArray(signal, frameSize+1);
       residual  = (float *) AllocFloatArray(residual, frameSize+1);
       LPSpec  = (float *) AllocFloatArray(LPSpec, fftSize+1);
       LPSpecAvg  = (float *) AllocFloatArray(LPSpecAvg, fftSize+1);
       LPPhase  = (float *) AllocFloatArray(LPPhase, fftSize+1);
       LPGdSpec  = (float *) AllocFloatArray(LPGdSpec, fftSize+1);
       LPGdSpecAvg  = (float *) AllocFloatArray(LPGdSpecAvg, fftSize+1);
       coef  = (float *) AllocFloatArray(coef, lpOrder+1);
       if ((frameStart == -1) && (frameEnd == -1)) {
         frameStart = 0;
	 frameEnd = numFrames;
       }
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 if (asdf->vU[frameNum]) {
	   numVoicedFrames++;
	   waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	   for (i = 1; i <= frameSize; i++)
	     signal[i] = waveform->array[i-1];
	   LpAnal(signal,residual, frameSize, frameShift, coef, lpOrder,&gain);
	   LPSpectrum(coef, lpOrder, LPSpec, LPPhase, fftSize, fftOrder, gain);
	   for (i = 1; i <= fftSize; i++)
	     if (LPSpec[i] != 0)
	       LPSpecAvg[i] = LPSpecAvg[i] + log10(LPSpec[i]);
	 }
       }
	 for (i = 2; i <= fftSize - 1; i++) {
	   LPGdSpec[i] = LPPhase[i] - LPPhase[i-1];
	   if (fabs(LPGdSpec[i]) > PI)
	     LPGdSpec[i] = LPGdSpec[i-1];
	   LPGdSpecAvg[i] = LPGdSpecAvg[i] + LPGdSpec[i];
	 }
	 LPGdSpec[1] = LPGdSpec[2];
	 for (i = 1; i <= fftSizeBy2; i++) 
	   if (i <= frameSize)
	     fprintf(specFile, "%f %f %f %f %f %f %f %f\n",
		     (float) (i-1)/(float) samplingRate, 
		     (float) (i-1) / (float) fftSize *samplingRate, 
		     (float) waveform->array[i-1], 
		     LPSpec[i], LPPhase[i], LPGdSpec[i], 
		     LPGdSpecAvg[i]/(frameEnd-frameStart+1),
		   20*(LPSpecAvg[i])/(frameEnd-frameStart+1));	 
	   else
	     fprintf(specFile, "%f %f %f %f %f %f %f %f\n",
		     (float) (i-1)/(float) samplingRate, 
		     (float) (i-1) / (float) fftSize *samplingRate, 
		     0.0, 
		     LPSpec[i], LPPhase[i], LPGdSpec[i], 
		     LPGdSpecAvg[i]/(frameEnd-frameStart+1),
		   20*(LPSpecAvg[i])/(frameEnd-frameStart+1));	 
	 fclose(specFile);
	 return(0);
}
/**************************************************************************
 * $Log: LPSpectrum.c,v $
 * Revision 1.1  2000/07/24 02:07:57  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/




#define MaxN 20
#define MaxM 20
typedef double Vector;
typedef double Matrix;

typedef struct Cvector {
  Vector Re[MaxN], Im[MaxN];
} Cvector;

typedef struct Cmatrix {
  Matrix Re[MaxN][MaxM], Im[MaxN][MaxM];
} Cmatrix;

typedef long square;

#include "eigenr.c"

main()
{
  int i,j,n;
   int input[MaxM][MaxN];
 Cvector *EigenValue;
  Cmatrix *EigenVector;
  double Index;
  int *flag;
  printf("ENTER THE ORDER OF THE MATRIX\n");
  scanf("%d",&n);
   printf("ENTER THE ELEMENTS OF THE MATRIX\n");
  for(i=0;i<n;i++)
    for(j=0;j<n;j++)
      scanf("%d",&input[i][j]);
  for(i=0;i<n;i++)
    for(j=0;j<n;j++)
      printf("\t%d\t",input[i][j]);
  
  EigenR(n,input,EigenValue,EigenVector,Index,flag);
  //printf("\n");
  printf("Real of eigenvalue\n");
  for(i=0;i<n;i++)
    printf("%lf",EigenValue->Re[i]);
  printf("\nimaginary of eigenvalue\n");
  for(i=0;i<n;i++)
    printf("%lf",EigenValue->Im[i]);
  printf("\nReal of eigenvector\n");
  for(i=0;i<n;i++)
    {
      for(j=0;j<n;j++)
	printf("%lf\t",EigenVector->Re[i][j]);
      printf("\n"); 
    }
  printf("\nimaginary of eigenvector\n");
  for(i=0;i<n;i++)
    {
      for(j=0;j<n;j++)
	printf("%lf\t",EigenVector->Im[i][j]);
      printf("\n"); 
    }
  printf("\nflag=%d\n",*flag);
  printf("\n");
}







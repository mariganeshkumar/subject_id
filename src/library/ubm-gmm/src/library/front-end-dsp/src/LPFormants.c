#include "stdio.h"
#include "math.h"
#include "sp/sphere.h"
#include "fe/front-end-defs.h"
#include "fe/front-end-types.h"
#include "fe/DspLibrary.h"
#include "fe/FmtSLibrary.h"
#include "fe/PthLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program extracts formant from a given speech
*	utterance and saves it on FILEs for different LP orders and 
*	Inputs :
*	Speech data FILE
*	FFT order (nfft, nfft (No. of FFT Stages)
*	frameLength : length of data frame for processing
*	WinShft : SHift for sliding window
*	Window parameters : Range of window widths, Increment
*	nFormants : Number of formants to be picked
*	FOrmant FILEname : first 5 chars of formant FILE name(FTMP)
*	Output :
*	Formant data are written on to different FILEs for different 
*	LP orders
*       e.g : For the first value of LP order the following FILE
*	is created
*	concatenate(ftmp,"A")
*	The substring A changes for different LP orders (becomes WB,WC..)
*******************************************************************************/       void Usage() {
           printf("Usage : LPFormants waveFile output_formant_FILE \n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 
        int		maxpts=1024,maxptsby2=512;
	int  	        i,k,nfft,mfft,frameLength,
      	                num,frameNum,iloc,totalFrames,
     			lpLow,lpHgh,lpOrd,lpInc,
			winShft, nFormants, waveType;
        long            nSamp;
	char            fNameFmt[40], 
			*ftmp=NULL;
	char       	string[5],cwin;
	float 		*lpFormants,*lpCopy,lowLim,hghLim;
	short           *waveform;
        SP_FILE         *waveFile;     
        FILE            *fmtFile;
	float           *signal, *f;

/******************************************************************************/
       if (argc != 3) {
         Usage();
         exit(-1);
       }
       /*	waveFile = sp_open(argv[1],"r"); */
        printf("encoding scheme ( pcm - 0/ulaw -1/text -2) :");
        scanf("%d",&waveType);
        if (waveType == 0)
	  waveform = (short *) ReadSpherePcm(argv[1], &nSamp);
        else if (waveType == 1)
	  waveform = (short *) ReadSphereUlaw(argv[1], &nSamp);
	else
	  waveform = (short *) ReadText(argv[1], &nSamp);
	printf("nfft,mfft :"); 
        scanf("%d %d",&nfft, &mfft);
        Cstore(nfft);
	printf("frameLength :");
        scanf("%d",&frameLength);
	printf("winDow shift :");
	scanf("%d",&winShft);
        totalFrames = nSamp/winShft;
	printf("total number of frames = %d\n",totalFrames);
	printf("range of LP orders for signal :");
	scanf("%d %d",&lpLow, &lpHgh);
	printf("winDow increment :");
	scanf("%d",&lpInc);
	printf("number of formants :");
	scanf("%d", &nFormants);
	printf("lower limit and higher limits for formants:");
	scanf("%f %f",&lowLim,&hghLim);
        printf("starting sample :");
        scanf("%d", &iloc);
	/*       ftmp = (char *) malloc(strlen(argv[2])*sizeof(char));
        ftmp[0] = '\0';
        strcat(ftmp,argv[2]);*/
        signal = (float *) AllocFloatArray(signal,nfft+1);
        f  = (float *) AllocFloatArray(f,nfft+1);
	lpFormants  = (float *) AllocFloatArray(lpFormants,21);
	lpCopy  = (float *) AllocFloatArray(lpFormants,21);
        printf("space allocated\n");
	lpOrd = lpLow;
	cwin = 'A';
	while (lpOrd <= lpHgh){
	  printf("enter while\n");
	  /*	  fNameFmt[0] = '\0';
	  string[0] = '\0';
	  strcat(fNameFmt,ftmp);
	  printf("string catted %s\n",fNameFmt);
	  fflush(stdout);
	  string[0] = cwin;
	  string[1] = '\0';
	  printf("string = %s\n",string);
	  strcat(fNameFmt,string);
	  printf("string catted\n");
	  printf("Filename = %s\n",fNameFmt); */ 
	  fmtFile = fopen(argv[2],"w");
	  if (fmtFile == NULL) {
            printf("cannot open formant file \n");
            exit(-1);
	  }
	  frameNum = 0;
	  while (iloc < nSamp) {
            printf("FrameNumber = %d iloc = %d, nSamp =%d\n",
		   frameNum, iloc, nSamp);	
	    frameNum++;
	    for (i = 1; i <= frameLength; i++)
	      if ((iloc+i) <= nSamp) 
		f[i] = waveform[iloc+i];
	      else
		f[i] = 0.0;
	    for (i = 1; i <=  frameLength; i++){
	      signal[i] = f[i]*HamDw(i,frameLength);
	    }
            
	    lpFormants = (float *) FmtsLp(signal,frameLength, frameShift, nfft,mfft,
					  lpOrd,lpFormants,&num);
            printf("num = %d\n",num);
            fflush(stdout);
	    for(i = 1; i <=  num; i++) 
	      printf("f %d = %f", i, lpFormants[i]);
	    printf("\n"); 
            fflush(stdout);
	    k = 0;
	    for (i = 1; i <= nFormants; i++)
	      lpCopy[i] = 0;
	    for (i = 1; i <= num; i++)
	      if (lpFormants[i] >= lowLim &&
		  lpFormants[i] <= hghLim) {
		k = k + 1;
		lpCopy[k] = lpFormants[i];
	      }
	    printf("k = %d\n",k);
	    for (i = 1; i <= k; i++)
	      fprintf(fmtFile,"%f ",lpCopy[i]);
	      for (i = k+1; i <= nFormants; i++)
	      fprintf(fmtFile, "%f ",0.0);
	      fprintf(fmtFile,"\n");
	    for (i = 1; i <= 10; i++)
	      lpFormants[i] = 0;
	    iloc = iloc+winShft;
            printf("exit while \n");
            fflush(stdout);
	  }
	  lpOrd = lpOrd+lpInc;
	  ((int)cwin)++;
	  fclose(fmtFile);
	}
}











if [ $# != 9 ]; then
       echo "USAGE: bash extractiVec.sh  UBMFileName CodebookSize FeatDimension reducedDimension dumpDirectory tmatDirectory"
       exit
fi

############################    INPUT   #################################
ubmDev=$1
ubmMix=$2
D=$3
R=$4
dumpDir=$5
tmatDevDir=$6
trainFeatList=$7
valFeatList=$8
testFeatList=$9

############################### Initialization ##############################

listDumpDir=$dumpDir/lists
tmatDumpDir=$dumpDir/tmat


binDir=library/ivec/bin
scriptsDir=library/ivec/scripts

trainStatList=$listDumpDir/trainStatList
trainWList=$listDumpDir/trainWList
trainLxList=$listDumpDir/trainLxList


valStatList=$listDumpDir/valStatList
valWList=$listDumpDir/valWList
valLxList=$listDumpDir/valLxList


testStatList=$listDumpDir/testStatList
testWList=$listDumpDir/testWList
testLxList=$listDumpDir/testLxList



ComputeStats=$scriptsDir/topc_stat.py
InitTMat=$binDir/InitTMat
IVectEst=$binDir/IVectEst
ComputeA=$binDir/ComputeA
ComputeC=$binDir/ComputeC
RecomputeT=$binDir/RecomputeT
Whiten=$binDir/Whiten
LDAPlusWCCN=$binDir/LDAPlusWCCN

nCores=15
nPerClass=5

rm -rf $dumpDir 
mkdir -p $listDumpDir $tmatDumpDir  

############################# Train data i-vector#####################################

echo "Statistics computation ..."
less $trainFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

sed 's!.lfcc$!.lfcc.stat!' $trainFeatList > $trainStatList
sed 's!.stat$!.w!' $trainStatList > $trainWList
sed 's!.stat$!.lx!' $trainStatList > $trainLxList


count=`less $trainStatList |wc -l`
let count1=$count/$nCores
let count1=$count1+1
let var=1
for i in `seq $count1 $count1 $count`
       do
       less $trainStatList |head -$i |tail -$count1 >>$listDumpDir/$var.trainStatList
       less $trainWList |head -$i |tail -$count1 >>$listDumpDir/$var.trainWList
       less $trainLxList |head -$i |tail -$count1 >>$listDumpDir/$var.trainLxList
       let var=$var+1
done
let count1=$count-$i
less $trainStatList |tail -$count1 >>$listDumpDir/$var.trainStatList
less $trainWList |tail -$count1 >>$listDumpDir/$var.trainWList
less $trainLxList |tail -$count1 >>$listDumpDir/$var.trainLxList

echo "Computing i vectors"
seq 1 $nCores |parallel -k -j$nCores --no-notice --bar $IVectEst -l $listDumpDir/{1}.trainStatList $ubmMix $D $R $tmatDevDir/TMAT.txt $listDumpDir/{1}.trainWList $listDumpDir/{1}.trainLxList


############################# Val data i-vector#####################################

echo "Statistics computation ..."
less $valFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

sed 's!.lfcc$!.lfcc.stat!' $valFeatList > $valStatList
sed 's!.stat$!.w!' $valStatList > $valWList
sed 's!.stat$!.lx!' $valStatList > $valLxList


count=`less $valStatList |wc -l`
let count1=$count/$nCores
let count1=$count1+1
let var=1
for i in `seq $count1 $count1 $count`
       do
       less $valStatList |head -$i |tail -$count1 >>$listDumpDir/$var.valStatList
       less $valWList |head -$i |tail -$count1 >>$listDumpDir/$var.valWList
       less $valLxList |head -$i |tail -$count1 >>$listDumpDir/$var.valLxList
       let var=$var+1
done
let count1=$count-$i
less $valStatList |tail -$count1 >>$listDumpDir/$var.valStatList
less $valWList |tail -$count1 >>$listDumpDir/$var.valWList
less $valLxList |tail -$count1 >>$listDumpDir/$var.valLxList

echo "Computing i vectors"
seq 1 $nCores |parallel -k -j$nCores --no-notice --bar $IVectEst -l $listDumpDir/{1}.valStatList $ubmMix $D $R $tmatDevDir/TMAT.txt $listDumpDir/{1}.valWList $listDumpDir/{1}.valLxList



############################# Test data i-vector#####################################

echo "Statistics computation ..."
less $testFeatList | parallel -j$nCores --bar "python2.7  $ComputeStats $ubmDev {1}  > {1}.stat"

sed 's!.lfcc$!.lfcc.stat!' $testFeatList > $testStatList
sed 's!.stat$!.w!' $testStatList > $testWList
sed 's!.stat$!.lx!' $testStatList > $testLxList


count=`less $testStatList |wc -l`
let count1=$count/$nCores
let count1=$count1+1
let var=1
for i in `seq $count1 $count1 $count`
       do
       less $testStatList |head -$i |tail -$count1 >>$listDumpDir/$var.testStatList
       less $testWList |head -$i |tail -$count1 >>$listDumpDir/$var.testWList
       less $testLxList |head -$i |tail -$count1 >>$listDumpDir/$var.testLxList
       let var=$var+1
done
let count1=$count-$i
less $testStatList |tail -$count1 >>$listDumpDir/$var.testStatList
less $testWList |tail -$count1 >>$listDumpDir/$var.testWList
less $testLxList |tail -$count1 >>$listDumpDir/$var.testLxList

echo "Computing i vectors"
seq 1 $nCores |parallel -k -j$nCores --no-notice --bar $IVectEst -l $listDumpDir/{1}.testStatList $ubmMix $D $R $tmatDevDir/TMAT.txt $listDumpDir/{1}.testWList $listDumpDir/{1}.testLxList
echo $testFeatList
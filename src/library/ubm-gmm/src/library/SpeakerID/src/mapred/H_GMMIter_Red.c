#include "CommonFunctions.h"

int
main (int argc, char *argv[])
{
		int featLength;

		int key, prevKey = -1;
		int numVectors=0, i, totalVectors = 0;
		float wt, totalWt = 0.0;
	  // vec1 is for reading and vec2 for accumulation	
    F_VECTOR *mvec1 = NULL, *mvec2 = NULL;
		F_VECTOR *vvec1 = NULL, *vvec2 = NULL;
		
		while (!feof (stdin))
		{
		    scanf ("%d\t%d,%d",&key, &wt, &featLength);
				// if mvec1 unallocated, allocate
			  if (mvec1 == NULL) mvec1 = AllocFVector (featLength);							
				if (vvec1 == NULL) vvec1 = AllocFVector (featLength);
				// if mvec2 allocated but feat length is invalid exit
				if (mvec2 != NULL && featLength != mvec2->numElements) return 1;
				// if the record belongs to the same key or is the first record
				// read it and have a copy for accumulation
				fprintf (stderr, "%d %d %d\n", key, featLength, numVectors);
			  if (prevKey == key || prevKey == -1)
				{
							totalWt += wt;
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &mvec1->array[i]);
							fprintf (stderr, "ok tiull here\n");
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &vvec1->array[i]);
							if (mvec2 == NULL)
							{
															mvec2 = AllocFVector (featLength);
															for (i = 0; i < featLength; i++)
																			mvec2->array[i] = 0.0;
							}
							if (vvec2 == NULL)
							{
											vvec2 = AllocFVector (featLength);
															for (i = 0; i < featLength; i++)
																			mvec2->array[i] = 0.0;
							}
							for (i = 0; i < featLength; i++)
							{
											mvec2->array[i] += mvec1->array[i];
											vvec2->array[i] += vvec1->array[i];
							}
				      prevKey = key;
				}
				else
				{
						printf("%d\t%d,%d",prevKey,totalVectors,featLength);
						for (i = 0; i < featLength; i++)
						{
										if (totalVectors == 0)
												{
																printf (",%e,%e",0.0,0.0);
																continue;
												}
										mvec2->array[i] = mvec2->array[i]/totalVectors;
										vvec2->array[i] = vvec2->array[i]/totalVectors;
										vvec2->array[i] = vvec2->array[i] - 
														        (mvec2->array[i] * mvec2->array[i]);
										printf (",%e,%e", mvec2->array[i],vvec2->array[i]);
						}						
						printf ("\n");
						fflush (stdout);
						totalVectors = numVectors;
						prevKey = key;
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &mvec2->array[i]);
							for (i = 0; i < featLength; i++)
							    scanf(",%e", &vvec2->array[i]);
				}
				scanf("\n");
		}
						printf("%d\t%d,%d",prevKey,totalVectors,featLength);
						
						for (i = 0; i < featLength; i++)
						{
										if (totalVectors == 0)
												{
																printf (",%e,%e",0.0,0.0);
																continue;
												}
										mvec2->array[i] = mvec2->array[i]/totalVectors;
										vvec2->array[i] = vvec2->array[i]/totalVectors;
										vvec2->array[i] = vvec2->array[i] - 
														(mvec2->array[i] * mvec2->array[i]);
										printf (",%e,%e", mvec2->array[i],vvec2->array[i]);
						}						
						printf ("\n");
						fflush (stdout);
/*						FreeFVector (mvec1);
						FreeFVector (mvec2);
						FreeFVector (vvec1);
						FreeFVector (vvec2);						*/
	return 0;
}	

function scoring_direct_lda (testname, trainlist, ldamatrix, opname, D, ubmname, exflag)
  w_test = load (testname);
  tl = readlistascell (trainlist);
  ll = length (tl);
  load (ldamatrix, '-mat', 'A');
  MD = size(A,1);
  M = MD / D;
  [u_w u_m u_v] = ReadGMM (ubmname, M, D);
  clear u_w;
  u_m = reshape (u_m,MD,1);
  u_v = reshape (u_v,MD,1);
  w_test = (w_test - u_m);
  R = size (A,2);
  s = zeros (ll,1);
  for i = 1:ll
      w_tgt = (load (tl {i}) - u_m);
      s(i) = 0.0;
      for m = 1:M        
        idx = (m-1)*D+1:m*D;
        s (i) = s(i) +  coskernel (A(idx,:), w_test(idx), w_tgt(idx));        
      end
  end
%  mu = mean (s(1:125));
%  sigma = std (s(1:125));
%  s = s - repmat (mu,ll,1);
%  s = s ./ repmat (sigma,ll,1);  
  save (opname, 's', '-ascii')
  id = find (min (s) == s);  
  sprintf ('Result %s %d', testname, id)
  if exflag ~= 0
    exit
  end

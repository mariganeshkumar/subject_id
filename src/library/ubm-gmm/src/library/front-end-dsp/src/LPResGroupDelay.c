/**************************************************************************
 *  $Id: LPResGroupDelay.c,v 1.4 2001/01/25 11:53:03 hema Exp hema $
 *  File:	LPResGroupDelay.c - Computes the group delay function of
 *               the residual of a signal.
 *
 *  Purpose:	Used to compute the group delay function of the LP
 *               Residual
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Fri 8-Jun-2007 12:35:37 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "math.h"
#include "malloc.h"

#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average group delay function 
        from the residual of a given speech utterance and saves it in 
*	a file. 
*	Inputs :
*	Speech data FILE
*	FFT order (fftSize, fftOrder (No. of FFT Stages)
*	FrameLength : length of data frame for processing
*	nFormants : Number of formants to be picked
*	OutPut Filename 
*	Output :
*       GroupDelay  data are written to a file 

*******************************************************************************/       void Usage() {
  printf("Usage : LPResGroupDelay ctrlFile waveFile GroupDelayFile resFile\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 
 	float           *signal, *resEnergy, *residual;
	int  	        i,k,fftSize,fftOrder,frameSize,
      	                frameNum,iloc,lpOrder,frameShift,
			ans,waveType, fftSizeBy2, numFrames;
        long            numSamples;
	char            ansTemp;
	float 		*gDelay, *gDelaySmoothed, med[4], 
	                *coef, gdAverage, gain;
	F_VECTOR        *waveform;
        SP_FILE         *waveFile;     
        FILE            *gdFile, *resFile, *controlFile;
	static ASDF     *asdf;
        
/******************************************************************************/
       if (argc != 5) {
         Usage();
         exit(-1);
       }
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       lpOrder   = (int) GetIAttribute(asdf, "lpOrder");
       numSamples = (int) GetIAttribute(asdf, "numSamples");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize);
       printf ("frameSize = %d fftOrder = %d fftSize = %d lpOrder = %d frameShift = %d\n", frameSize, fftOrder, fftSize, lpOrder, frameShift);
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       residual = (float *) AllocFloatArray(residual,numSamples+1);
       signal = (float *) AllocFloatArray(signal, fftSize+1);
       resEnergy  = (float *) AllocFloatArray(resEnergy, fftSize+1);
       gDelay  = (float *) AllocFloatArray(gDelay, fftSize+1);
       gDelaySmoothed  = (float *) AllocFloatArray(gDelaySmoothed, fftSize+1);
       coef = (float *) AllocFloatArray(coef, lpOrder+2);
       gdFile = fopen(argv[3],"w");
       resFile = fopen(argv[4],"w");
       if ((gdFile == NULL) || (resFile == NULL)){
	 printf("cannot open files \n");
	 exit(-1);
       }
       printf ("numFrames = %d\n", numFrames);
       iloc = 0;
       for (frameNum = 0; frameNum < numFrames; frameNum++)
	 {	
	  waveform = (F_VECTOR *) GsfRead(asdf,frameNum,"frameWaveform");
	 for (i = 1; i <= frameSize; i++)
	   signal[i] = waveform->array[i-1];
	 /* *HamDw(i,frameSize); */
	 LpAnal(signal,resEnergy,frameSize, coef, lpOrder,&gain);
         for (i = 1; i <= frameShift; i++) {
           residual[iloc+i-1] = resEnergy[i];
	   fprintf(resFile, "%f\n",resEnergy[i]);
	 }
	 iloc = iloc+frameShift;
	 }
       iloc = 0;
       while (iloc < numSamples) {	
	 for (i = 1; i <= frameSize; i++)
	   if ((iloc+i-1) < numSamples) 
	     resEnergy[i] = residual[iloc+i-1];
	   else
	     resEnergy[i] = 0.0000001;
	 StdGroupDelay (resEnergy, frameSize, fftSize, fftOrder, gDelay);
	 for (i = 2; i <= fftSize-1; i++) {
	   med[1] = gDelay[i-1];
	   med[2] = gDelay[i];
	   med[3] = gDelay[i+1];
	   gDelaySmoothed[i] = (float) Median (med,3);
	   //printf("GDs=%f\n", gDelaySmoothed[i]);
           fflush(stdout);
	 }
	 gDelay[1] = gDelay[2];
	 gDelay[fftSize] = gDelay[fftSize-1];
	 gDelaySmoothed[1] = gDelaySmoothed[2];
         gDelaySmoothed[fftSize] = gDelaySmoothed[fftSize-1];
	 ComputeAverage(gDelaySmoothed, fftSize/2, &gdAverage);
	 //	 printf("Gd Average = %f\n",gdAverage-frameSize/2);
	 fprintf(gdFile,"%f\n",gdAverage);
	 iloc++;
       }
       fclose(gdFile);
       fclose(resFile);
}
/**************************************************************************
 * $Log: LPResGroupDelay.c,v $
 * Revision 1.4  2001/01/25 11:53:03  hema
 * Modified Usage
 *
 * Revision 1.3  2000/04/28 01:58:15  hema
 * fixed the bug in median filtering
 *
 * Revision 1.2  2000/04/27 01:59:30  hema
 * modified group delay averaging
 *
 * Revision 1.1  2000/04/23 02:36:13  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of GroupDelay.c
 **************************************************************************/




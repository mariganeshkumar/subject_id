function [A] = train_LDA_projection(X,N,nA)

[dim,ntot] = size(X);
nspk = length(N);

%Compute speaker means
Sb = zeros(dim,dim,'single');
St = double(X*X');
ie = 0;
for i = 1:nspk
    ib = ie + 1;
    ie = ib + N(i) - 1;
    spk_mean = sum(X(:,ib:ie),2);    
    Sb = Sb + (spk_mean * spk_mean')/N(i);    
end
Sb = double(Sb);
Sw = St - double(Sb);

if nA < dim
    [A,D]=eigs(Sb,Sw,nA);
else
    [A,D]=eig(Sb,Sw);
end




#include "SlopeStg.h"

int main (int argc, char *argv[]) {
    char *controlFileName, *trainFileName, *opfname, *wavname;
    char *filterbank = "frameFilterbankLogEgy";
    FILE *controlFile;
    ASDF *asdf;
    float thresholdScale;
    uint numVectors;
    int oc, gwnd;
    VFV *vfv, *newvfv;
    
	while ( (oc = getopt (argc, argv, "f:t:o:i:")) != -1) {
        switch (oc) {
          case 'f':
            controlFileName = optarg;
            controlFile = fopen (controlFileName, "r");
            asdf = (ASDF *) calloc (1, sizeof (ASDF));
            InitializeStandardFrontEnd (asdf, controlFile);		
            Cstore (asdf->fftSize);
            break;
          case 't':
            sscanf (optarg, "%e", &thresholdScale);
            break;
          case 'o':
            opfname = optarg;
            break;
          case 'i':
            wavname = optarg;
            break;
          default:
            printf("Usage: TestSlopeStg -f cfile -t threshold -i wavname -o opfile -d\n");
            exit (1);
            break;
        }	
    }
    PutIAttribute (asdf, "stGauss", 0);
	vfv = (VFV *) 
          ExtractFeatureVectors
                (asdf, wavname, filterbank, 
                 &numVectors, thresholdScale);
                
    gwnd = GetIAttribute (asdf,"stGaussWnd");
    newvfv = ComputeSlopeStg (vfv, numVectors, asdf->numRegressCoeffts, gwnd);                
    WriteVfvToFile (newvfv, numVectors, opfname);
    return 0;
}

#include "stdio.h"
#include "math.h"
#include "malloc.h"
/**************************************************************************
 *  $Id: LPEntropy.c,v 1.1 2000/07/24 02:07:57 hema Exp hema $
 *  File:	LPEntropy.c - Computes the Entropy of the LPSpectrum (framewise)
 *              of a given signal.
 *
 *  Purpose:	To compute LPEntropy for music processing
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 11-Oct-2011 10:58:55 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/

#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/InitAsdf.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average LP Spectrum
        from frameStart to frameEnd from a given utterance of speech 
*	a file. 
*	Inputs :
*	controlFile, waveFile, specFile, frameStart, frameEnd
*	Output :
*       Average of the LP Spectrum  of data are written to a file 

*******************************************************************************/       
void Usage() {
           printf("Usage : LPEntropy ctrlFile waveFile SpecFile frameStart frameEnd\n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

	float           *signal=NULL, *residual=NULL;
	int  	        i, fftOrder,fftSize,fftSizeBy2, 
	                frameSize, numFrames,
	                frameNum, lpOrder, frameShift, 
                        frameStart, frameEnd;
        int             samplingRate;
        float           *LPSpec=NULL, *LPPhase=NULL, *psd=NULL,
	                gain, *coef=NULL;
        float           entropy;
    	F_VECTOR        *waveform=NULL;
        FILE            *specFile=NULL, *controlFile=NULL;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf,argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       lpOrder   = (int) GetIAttribute(asdf, "lpOrder");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       samplingRate = (int) GetIAttribute(asdf, "samplingRate");
       printf ("frameSize = %d fftOrder = %d fftSize = %d lpOrder = %d frameShift = %d numFrames=%d\n", 
	       frameSize, fftOrder, fftSize, lpOrder, frameShift, numFrames);
       sscanf(argv[4], "%d", &frameStart);
       sscanf(argv[5], "%d", &frameEnd);
       Cstore(fftSize);
       fftSizeBy2 = fftSize/2;

       signal = (float *) AllocFloatArray(signal, frameSize+1);
       residual  = (float *) AllocFloatArray(residual, frameSize+1);
       LPSpec  = (float *) AllocFloatArray(LPSpec, fftSize+1);
       psd  = (float *) AllocFloatArray(psd, fftSize+1);
       LPPhase  = (float *) AllocFloatArray(LPPhase, fftSize+1);
       coef  = (float *) AllocFloatArray(coef, lpOrder+1);
       specFile = fopen(argv[3],"w");
       if ((frameStart == -1) && (frameEnd == -1)) {
         frameStart = 0;
	 frameEnd = numFrames;
       }
       for (frameNum = frameStart; frameNum < frameEnd; frameNum++) {
	 waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	 for (i = 1; i <= frameSize; i++)
	   signal[i] = waveform->array[i-1];
	 LpAnal(signal, residual, frameSize, frameShift, coef, lpOrder, &gain);
	 LPSpectrum(coef, lpOrder, LPSpec, LPPhase, fftSize, fftOrder, gain);
	 entropy = 0;
	 for (i = 1; i <= fftSizeBy2; i++)
	   entropy = entropy + LPSpec[i]*LPSpec[i];	   
	 for (i = 1; i <= fftSizeBy2; i++)
	   psd[i] = LPSpec[i]*LPSpec[i]/entropy;
	 entropy = 0;
	 for (i = 1; i <= fftSizeBy2; i++)
	   entropy = entropy - psd[i] * log10(psd[i]);
	 fprintf(specFile, "%f %f\n", frameNum*frameShift/(float) samplingRate, entropy);
       }
       fclose(specFile);
       return(0);
}
/**************************************************************************
 * $Log: LPEntropy.c,v $
 * Revision 1.1  2000/07/24 02:07:57  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPEntropy.c
 **************************************************************************/





/**************************************************************************
 *  $Id: FFTSpectrum.c,v 1.1 2000/07/24 02:07:25 hema Exp hema $
 *  File:       FFTSpectrum.c - Computes the FFT Spectrum
 *              of a given signal.
 *
 *  Purpose:	To compute spectra of IPTraffic
 *
 *  Author:	Hema A Murthy,BSB-307,445-8342,445-9342
 *
 *  Created:    Wed 09-Feb-2000 09:12:15
 *
 *  Last modified:  Tue 28-Jan-2003 17:31:58 by hema
 *
 *  Bugs:	
 *
 *  Change Log:	<Date> <Author>
 *  		<Changes>
 *
 **************************************************************************/
#include "stdio.h"
#include "math.h"
#include "malloc.h"
#include "sp/sphere.h"
#include "fe/FrontEndDefs.h"
#include "fe/FrontEndTypes.h"
#include "fe/DspLibrary.h"
#include "fe/SphereInterface.h"
/*******************************************************************************
* 	the Following program computes the average FFT Spectrum 
        from the startFrame to endFrame for a given speech utterance and 
        saves it in 
*	a file. 
*	Inputs :
*	Input data : controlFile, waveFile, SpecFile, startFrame, endFrame
*	Output :
*       Average of the FFT Spectrum  of data are written to a file 

*******************************************************************************/       void Usage() {
           printf("Usage : FFTSpectrum ctrlFile waveFile SpecFile startFrame endFrame \n");
}
/*****************************************************************************/

        main (int argc, char *argv[])
{ 

	float           *signal;
	int  	        i, fftOrder, fftSize, frameSize, numFrames,
	                frameNum,iloc, frameShift, frameStart, frameEnd,
			fftSizeBy2;
        long            nSamp;
        float           *FFTSpec, *FFTPhase, *FFTSpecAvg, *FFTPhaseAvg,  
	                *ax, *ay;
	F_VECTOR        *waveform;
        FILE            *specFile, *controlFile;
        static ASDF     *asdf;
/******************************************************************************/
       if (argc != 6) {
         Usage();
         exit(-1);
       }
       
       controlFile = fopen(argv[1], "r");
       asdf = (ASDF *) malloc(1*sizeof(ASDF));
       InitializeStandardFrontEnd(asdf, controlFile);
       GsfOpen(asdf, argv[2]);
       frameSize = (int) GetIAttribute(asdf, "windowSize");
       fftOrder  = (int) GetIAttribute(asdf, "fftOrder");
       fftSize  = (int) GetIAttribute(asdf, "fftSize");
       frameShift = (int) GetIAttribute(asdf, "frameAdvanceSamples");
       nSamp = (int) GetIAttribute (asdf,"numSamples");
       numFrames = (int) GetIAttribute(asdf, "numFrames");
       fftSizeBy2 = fftSize/2;
       Cstore(fftSize/2);
       printf ("frameSize = %d fftOrder = %d fftSize = %d frameShift = %d\n",
	       frameSize, fftOrder, fftSize, frameShift);
       sscanf(argv[4],"%d", &frameStart);
       sscanf(argv[5],"%d", &frameEnd);

       signal = (float *) AllocFloatArray(signal, frameSize+1);
       FFTSpec  = (float *) AllocFloatArray(FFTSpec, fftSize+1);
       ax       = (float *) AllocFloatArray(ax, fftSize+1);
       ay       = (float *) AllocFloatArray(ay, fftSize+1);
       FFTSpecAvg  = (float *) AllocFloatArray(FFTSpecAvg, fftSize+1);
       FFTPhase  = (float *) AllocFloatArray(FFTPhase, fftSize+1);
  
       specFile = fopen(argv[3],"w");
       iloc = frameStart*frameShift;
       if ((frameStart == -1) && (frameEnd == -1 )){
         frameStart = 0;
	 frameEnd = numFrames;
       }
       for (frameNum = frameStart; frameNum <= frameEnd; frameNum++) {
	 printf (" frameNum = %d startLoc = %d\n", frameNum, iloc);
         waveform = (F_VECTOR *) GsfRead(asdf, frameNum, "frameWaveform");
	 for (i = 1; i <= frameSize; i++){
	   printf("waveform %d = %f\n", i, waveform->array[i-1]);
	   signal[i] = waveform->array[i-1]*HamDw(i,frameSize);
	   printf("signal %d = %f\n", i, signal[i]);}
         for (i = frameSize+1; i <= fftSize; i++)
           signal[i] = 0;
	 Rfft(signal,ax, ay, fftOrder, fftSize, -1);
	 SpectrumReal(fftSize, ax, ay, FFTSpec, FFTPhase);
         for (i = 1; i <= fftSize; i++)
           FFTSpecAvg[i] = FFTSpecAvg[i] + (double)log(FFTSpec[i]);
	 iloc = iloc+frameShift;
       }
       for (i = 1; i <= fftSize; i++)
         fprintf(specFile, "%f %f %f %f\n",ax[i], ay[i], FFTSpec[i], 20*FFTSpecAvg[i]/(frameEnd-frameStart+1));
       fclose(specFile);
}
/**************************************************************************
 * $Log: FFTSpectrum.c,v $
 * Revision 1.1  2000/07/24 02:07:25  hema
 * Initial revision
 *
 *
 * Local Variables:
 * time-stamp-active: t
 * time-stamp-line-limit: 20
 * time-stamp-start: "Last modified:[ 	]+"
 * time-stamp-format: "%3a %:d-%3b-%:y %H:%M:%S by %u"
 * time-stamp-end: "$"
 * End:
 *                        End of LPSpectrum.c
 **************************************************************************/



